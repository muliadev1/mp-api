const Sequelize = require('sequelize');

const sequelize = new Sequelize(process.env.DB_DATABASE, process.env.DB_USERNAME, process.env.DB_PASSWORD, {
  host: process.env.DB_HOST,
  dialect: process.env.DB_CONNECTION,
  port: process.env.DB_PORT,
  dialect: 'mysql',
  logging: false,
  timezone: '+08:00', // Correctly set to UTC+8
});

module.exports = sequelize;