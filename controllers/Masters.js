const { Master, InitialPoint, Member, Point, EnrollsLog, sequelize, Setting } = require('../db/models/index');
const { Op } = require('sequelize');
const mysql = require('mysql');

const fs = require('fs');
const path = require('path');
const { Sequelize, DataTypes } = require('sequelize');
const csv = require('fast-csv');

const readline = require('readline');
const moment = require('moment');


const applyFilters = (baseQuery, filter) => {
    const baseWhere = {
        [Op.and]: [
            { email: { [Op.ne]: '' } },
            { arrival_date: { [Op.not]: '' } },
            { departure_date: { [Op.not]: '' } },
            { stay_date: { [Op.not]: '' } },
            { market_code: { [Op.in]: ['RAC', 'WEB', 'PACKDOM', 'PACKINT'] } },
            { resv_status: { [Op.eq]: 'CHECKED OUT' } },
            { flag: { [Op.eq]: 0 } },
        ],
    };

    const filterWhere = filter ? {
        [Op.or]: [
            { confirmation_no: { [Op.like]: `%${filter}%` } },
            { mp: { [Op.like]: `%${filter}%` } },
            { room_class: { [Op.like]: `%${filter}%` } },
            { firstname: { [Op.eq]: `${filter}` } },
            { lastname: { [Op.like]: `%${filter}%` } },
            { email: { [Op.like]: `%${filter}%` } },
            { resv_status: { [Op.like]: `%${filter}%` } },
        ],
    } : {};

    const additionalWhere = {
        // Kondisi untuk memfilter count mp dan confirmation_no
        [Op.and]: [
            Sequelize.literal('(SELECT COUNT(*) FROM Masters AS subquery WHERE subquery.mp = Master.mp) = 1'),
            Sequelize.literal('(SELECT COUNT(*) FROM Masters AS subquery WHERE subquery.confirmation_no = Master.confirmation_no) = 1'),
        ],
    };

    return {
        ...baseQuery,
        where: { ...baseWhere, ...filterWhere, ...additionalWhere },
    };
};


class MasterController {
    static async getMasters(req, res) {

        //! Extract pagination parameters from request query
        //const { page, pageSize, sort, filter } = req.query
        let { page, pageSize, filter } = req.query

        //! Calculate starting index and number of records to retrieve
        const startIndex = (Number(page) - 1) * Number(pageSize);
        const endIndex = Number(page) * Number(pageSize);

        try {
            const masters = await Master.findAndCountAll({
                attributes: ['stay_date', 'firstname', 'lastname', 'confirmation_no', 'market_code', 'market_group', 'rate_code', 'currency', 'rate_amount', 'share_amount', 'arrival_date', 'departure_date', 'nationality', 'country', 'city'],
                order: [['stay_date', "DESC"]],
                where: filter ? {
                    [Op.or]: [{
                        firstname: {
                            [Op.like]: `%${filter}%`
                        }
                    },
                    {
                        lastname: {
                            [Op.like]: `%${filter}%`
                        }
                    },
                    {
                        confirmation_no: {
                            [Op.like]: `%${filter}%`
                        }
                    },
                    {
                        market_code: {
                            [Op.eq]: `%${filter}%`
                        }
                    },
                    {
                        market_group: {
                            [Op.like]: `%${filter}%`
                        }
                    },
                    {
                        rate_code: {
                            [Op.like]: `%${filter}%`
                        }
                    },
                    {
                        nationality: {
                            [Op.like]: `%${filter}%`
                        }
                    },
                    {
                        country: {
                            [Op.like]: `%${filter}%`
                        }
                    },
                    {
                        city: {
                            [Op.like]: `%${filter}%`
                        }
                    }
                    ]
                } : {},
                //order: sort ? [sort.split(' ')] : [],
                offset: startIndex,
                limit: Number(pageSize)

            });
            //! Retrieve total count of records for pagination info
            const totalCount = masters.count;
            //! Calculate total number of pages
            const totalPages = Math.ceil(totalCount / Number(pageSize));

            res.status(200).json(
                {
                    masters: masters,
                    totalRecords: totalCount,
                    currentPage: Number(page),
                    totalPages: totalPages
                }
            );
        } catch (error) {
            console.log(error);
        }
    };

    static async xxxxgetListMaster(req, res, next) {
        const { page, per_page, filter } = req.query;
        const perPageInt = parseInt(per_page, 10);
        const offset = (page - 1) * per_page;

        const startDate = moment().subtract(12, 'months').startOf('day').format('YYYY-MM-DD');
        const endDate = moment().endOf('day').format('YYYY-MM-DD');

        try {

            // Lakukan query ke database menggunakan Sequelize
            const master = await Master.findAndCountAll({
                attributes: [
                    [Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y'), 'stay_date'],
                    'firstname', 'lastname', 'confirmation_no', 'market_code', 'market_group', 'rate_code',
                    'currency', 'rate_amount', 'share_amount', 'arrival_date', 'departure_date', 'nationality',
                    'country', 'city', 'email', 'mp', 'mpenroll_date'
                ],
                offset: offset,
                limit: perPageInt,
                where: filter ? {
                    [Op.or]: [{
                        confirmation_no: {
                            [Op.like]: `%${filter}%`
                        }
                    },
                    {
                        mp: {
                            [Op.like]: `%${filter}%`
                        }
                    },
                    {
                        market_code: {
                            [Op.like]: `%${filter}%`
                        }
                    },
                    {
                        market_group: {
                            [Op.like]: `%${filter}%`
                        }
                    },
                    {
                        rate_code: {
                            [Op.like]: `%${filter}%`
                        }
                    },
                    {
                        firstname: {
                            [Op.eq]: `${filter}`
                        }
                    },
                    {
                        lastname: {
                            [Op.like]: `%${filter}%`
                        }
                    },
                    {
                        country: {
                            [Op.like]: `%${filter}%`
                        }
                    },
                    {
                        city: {
                            [Op.like]: `%${filter}%`
                        }
                    },
                    {
                        email: {
                            [Op.like]: `%${filter}%`
                        }
                    }
                    ],
                    [Op.and]: [
                        { arrival_date: { [Op.not]: null } },
                        { departure_date: { [Op.not]: null } },
                        { room_rev_usd: { [Op.gt]: 0 } },
                        { fb_rev_usd: { [Op.gt]: 0 } },
                        Sequelize.literal("STR_TO_DATE(`insert_date`, '%d/%m/%Y') BETWEEN '" + startDate + "' AND '" + endDate + "'")
                    ],
                } : {
                    [Op.and]: [
                        { arrival_date: { [Op.not]: null } },
                        { departure_date: { [Op.not]: null } },
                        { room_rev_usd: { [Op.gt]: 0 } },
                        { fb_rev_usd: { [Op.gt]: 0 } },
                        Sequelize.literal("STR_TO_DATE(`insert_date`, '%d/%m/%Y') BETWEEN '" + startDate + "' AND '" + endDate + "'")
                    ],
                },
                // Anda dapat menambahkan pengurutan jika diperlukan
                order: [[Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y'), 'DESC']],
            });

            // Ekstrak data dan jumlah total dari hasil query
            const data = master.rows;
            const totalCount = master.count;


            res.status(200).json({
                data: data,
                totalData: totalCount,
            });
        } catch (error) {
            console.log(error);
        }

    }

    static async getListMaster(req, res) {
        const { page, per_page, filter, location } = req.query;
        const perPageInt = parseInt(per_page, 10);
        const offset = (page - 1) * per_page;

        const startDate = moment().subtract(12, 'months').startOf('day').format('YYYY-MM-DD');
        const endDate = moment().endOf('day').format('YYYY-MM-DD');

        const uppercaseLocation = location ? location.toUpperCase() : '';

        const whereCondition = {
            [Op.and]: [
                { arrival_date: { [Op.not]: '' } },
                { departure_date: { [Op.not]: '' } },
                { room_rev_usd: { [Op.gt]: 0 } },
                { fb_rev_usd: { [Op.gt]: 0 } },
                Sequelize.literal("STR_TO_DATE(`insert_date`, '%d/%m/%Y') BETWEEN '" + startDate + "' AND '" + endDate + "'")
            ],
            region: uppercaseLocation
        };

        if (filter) {
            whereCondition[Op.or] = [
                { confirmation_no: { [Op.like]: `%${filter}%` } },
                { mp: { [Op.like]: `%${filter}%` } },
                { market_code: { [Op.like]: `%${filter}%` } },
                { firstname: { [Op.eq]: `${filter}` } },
                { lastname: { [Op.like]: `%${filter}%` } },
                { country: { [Op.like]: `%${filter}%` } },
                { city: { [Op.like]: `%${filter}%` } },
                { email: { [Op.like]: `%${filter}%` } }
            ];
        }

        try {
            const master = await Master.findAndCountAll({
                attributes: [
                    [Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y'), 'stay_date'],
                    'firstname', 'lastname', 'confirmation_no', 'market_code', 'market_group', 'rate_code',
                    'currency', 'rate_amount', 'share_amount', 'arrival_date', 'departure_date', 'nationality',
                    'country', 'city', 'email', 'mp', 'mpenroll_date'
                ],
                where: whereCondition,
                order: [
                    [Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y'), 'DESC'],
                    ['firstname', 'ASC'],
                    ['lastname', 'ASC'],
                ],
                offset: offset,
                limit: perPageInt,
            });

            const data = master.rows;
            const totalCount = master.count;

            res.status(200).json({
                data: data,
                totalData: totalCount,
            });
        } catch (error) {
            console.log(error);
            res.status(500).json({ error: 'Internal Server Error' });
        }
    };

    static async getListMaster_OLD(req, res) {

        const { page, per_page, filter, location } = req.query;
        const perPageInt = parseInt(per_page, 10);
        const offset = (page - 1) * per_page;

        const startDate = moment().subtract(12, 'months').startOf('day').format('YYYY-MM-DD');
        const endDate = moment().endOf('day').format('YYYY-MM-DD');

        const uppercaseLocation = location ? location.toUpperCase() : '';

        try {

            const master = await Master.findAndCountAll({
                attributes: [
                    [Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y'), 'stay_date'],
                    'firstname', 'lastname', 'confirmation_no', 'market_code', 'market_group', 'rate_code',
                    'currency', 'rate_amount', 'share_amount', 'arrival_date', 'departure_date', 'nationality',
                    'country', 'city', 'email', 'mp', 'mpenroll_date'
                ],
                where: filter ? {
                    [Op.or]: [{
                        confirmation_no: {
                            [Op.like]: `%${filter}%`
                        }
                    },
                    {
                        mp: {
                            [Op.like]: `%${filter}%`
                        }
                    },
                    {
                        market_code: {
                            [Op.like]: `%${filter}%`
                        }
                    },
                    {
                        firstname: {
                            [Op.eq]: `${filter}`
                        }
                    },
                    {
                        lastname: {
                            [Op.like]: `%${filter}%`
                        }
                    },
                    {
                        country: {
                            [Op.like]: `%${filter}%`
                        }
                    },
                    {
                        city: {
                            [Op.like]: `%${filter}%`
                        }
                    },
                    {
                        email: {
                            [Op.like]: `%${filter}%`
                        }
                    }
                    ],
                    [Op.and]: [
                        { arrival_date: { [Op.not]: '' } },
                        { departure_date: { [Op.not]: '' } },
                        { room_rev_usd: { [Op.gt]: 0 } },
                        { fb_rev_usd: { [Op.gt]: 0 } },
                        Sequelize.literal("STR_TO_DATE(`insert_date`, '%d/%m/%Y') BETWEEN '" + startDate + "' AND '" + endDate + "'")
                    ],
                    region: uppercaseLocation,
                } : {
                    [Op.and]: [
                        { arrival_date: { [Op.not]: '' } },
                        { departure_date: { [Op.not]: '' } },
                        { room_rev_usd: { [Op.gt]: 0 } },
                        { fb_rev_usd: { [Op.gt]: 0 } },
                        Sequelize.literal("STR_TO_DATE(`insert_date`, '%d/%m/%Y') BETWEEN '" + startDate + "' AND '" + endDate + "'")
                    ],
                    region: uppercaseLocation,
                },
                order: [
                    [Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y'), 'DESC'],
                    ['firstname', 'ASC'],
                    ['lastname', 'ASC'],
                ],
                offset: offset,
                limit: perPageInt,
            });

            // Ekstrak data dan jumlah total dari hasil query
            const data = master.rows;
            const totalCount = master.count;


            res.status(200).json({
                data: data,
                totalData: totalCount,
            });
        } catch (error) {
            console.log(error);
        }

    };

    static async getEnrolls(req, res, next) {
        const { page = 1, per_page = 10, filter, location } = req.query;
        const perPageInt = parseInt(per_page, 10);
        const offset = (page - 1) * perPageInt;

        const startDate = moment().subtract(24, 'months').startOf('day').format('YYYY-MM-DD'); // 2 tahun ke belakang
        const uppercaseLocation = location ? location.toUpperCase() : '';


        try {
            const whereCondition = {
                [Op.and]: [
                    { mp: '' },
                    { mp_level: '' },
                    { email: { [Op.ne]: '' } },
                    { arrival_date: { [Op.not]: '' } },
                    { departure_date: { [Op.not]: '' } },
                    { stay_date: { [Op.not]: '' } },
                    { resv_status: { [Op.in]: ['CHECKED IN', 'CHECKED OUT'] } },
                    Sequelize.literal(`STR_TO_DATE(insert_date, '%d/%m/%Y') >= '${startDate}'`)
                ],
                region: uppercaseLocation,
            };

            if (filter) {
                whereCondition[Op.or] = [
                    { confirmation_no: { [Op.like]: `%${filter}%` } },
                    { mp: { [Op.like]: `%${filter}%` } },
                    { market_code: { [Op.like]: `%${filter}%` } },
                    { firstname: { [Op.eq]: `${filter}` } },
                    { lastname: { [Op.like]: `%${filter}%` } },
                    { country: { [Op.like]: `%${filter}%` } },
                    { city: { [Op.like]: `%${filter}%` } },
                    { email: { [Op.like]: `%${filter}%` } }
                ];
            }

            // Combined query for data and count
            const { count, rows } = await Master.findAndCountAll({
                attributes: [
                    [Sequelize.fn('STR_TO_DATE', Sequelize.col('insert_date'), '%d/%m/%Y'), 'rsv_date'],
                    'confirmation_no',
                    'mp',
                    'market_code',
                    'market_group',
                    'firstname',
                    'lastname',
                    'nationality',
                    'country',
                    'city',
                    'email',
                    'telephone',
                    'resv_status',
                    'agent_name',
                    'wcc_member_id',
                    'is_point_wcc',
                    'region'
                ],
                where: whereCondition,
                offset: offset,
                limit: perPageInt,
                group: [
                    'confirmation_no',
                    'mp',
                    'market_code',
                    'market_group',
                    'firstname',
                    'lastname',
                    'nationality',
                    'country',
                    'city',
                    'email',
                    'telephone',
                    'resv_status',
                    'agent_name',
                    'wcc_member_id',
                    'is_point_wcc',
                    'region',
                    Sequelize.fn('STR_TO_DATE', Sequelize.col('insert_date'), '%d/%m/%Y')
                ],
                order: [
                    [Sequelize.fn('STR_TO_DATE', Sequelize.col('insert_date'), '%d/%m/%Y'), 'DESC'],
                    ['firstname', 'ASC'],
                    ['lastname', 'ASC'],
                ],
            });

            res.status(200).json({
                data: rows,
                totalData: count.length,
            });
        } catch (error) {
            console.log(error);
            res.status(500).json({ error: 'Internal Server Error' });
        }
    }

    static async getEnrolls_LAMA(req, res, next) {
        const { page, per_page, filter, location } = req.query;
        const perPageInt = parseInt(per_page, 10);
        const offset = (page - 1) * per_page;

        const startDate = moment().subtract(24, 'months').startOf('day').format('YYYY-MM-DD'); // 2 tahun ke belakang
        const endDate = moment().endOf('day').format('YYYY-MM-DD');

        const uppercaseLocation = location ? location.toUpperCase() : '';

        console.log(req.query, "APAKAH MASUK ENROLLS DISINI ??????????????????")

        try {

            // Lakukan query ke database menggunakan Sequelize
            const result = await Master.findAll({
                attributes: [
                    [Sequelize.fn('STR_TO_DATE', Sequelize.col('insert_date'), '%d/%m/%Y'), 'rsv_date'],
                    'confirmation_no',
                    'mp',
                    'market_code',
                    'market_group',
                    'firstname',
                    'lastname',
                    'nationality',
                    'country',
                    'city',
                    'email',
                    'telephone',
                    'resv_status'
                ],
                offset: offset,
                limit: perPageInt,
                where: filter ? {
                    [Op.or]: [{
                        confirmation_no: {
                            [Op.like]: `%${filter}%`
                        }
                    },
                    {
                        mp: {
                            [Op.like]: `%${filter}%`
                        }
                    },
                    {
                        market_code: {
                            [Op.like]: `%${filter}%`
                        }
                    },
                    {
                        firstname: {
                            [Op.eq]: `${filter}`
                        }
                    },
                    {
                        lastname: {
                            [Op.like]: `%${filter}%`
                        }
                    },
                    {
                        country: {
                            [Op.like]: `%${filter}%`
                        }
                    },
                    {
                        city: {
                            [Op.like]: `%${filter}%`
                        }
                    },
                    {
                        email: {
                            [Op.like]: `%${filter}%`
                        }
                    }
                    ],
                    [Op.and]: [
                        { mp: '' },
                        { mp_level: '' },
                        { email: { [Op.ne]: '' } },
                        // { birthday: { [Op.not]: null } },
                        { arrival_date: { [Op.not]: '' } },
                        { departure_date: { [Op.not]: '' } },
                        { stay_date: { [Op.not]: '' } },
                        { resv_status: { [Op.in]: ['CHECKED IN', 'CHECKED OUT'] } },
                        Sequelize.literal("STR_TO_DATE(`insert_date`, '%d/%m/%Y') >= '" + startDate + "'")
                    ],
                    region: uppercaseLocation,
                } : {
                    [Op.and]: [
                        { mp: '' },
                        { mp_level: '' },
                        { email: { [Op.ne]: '' } },
                        // { birthday: { [Op.not]: null } },
                        { arrival_date: { [Op.not]: '' } },
                        { departure_date: { [Op.not]: '' } },
                        { stay_date: { [Op.not]: '' } },
                        { resv_status: { [Op.in]: ['CHECKED IN', 'CHECKED OUT'] } },
                        Sequelize.literal("STR_TO_DATE(`insert_date`, '%d/%m/%Y') >= '" + startDate + "'")
                    ],
                    region: uppercaseLocation,
                },
                group: [
                    'confirmation_no',
                    'mp',
                    'market_code',
                    'market_group',
                    'firstname',
                    'lastname',
                    'nationality',
                    'country',
                    'city',
                    'email',
                    'telephone',
                    'resv_status',
                    Sequelize.fn('STR_TO_DATE', Sequelize.col('insert_date'), '%d/%m/%Y')
                ],
                order: [
                    [Sequelize.fn('STR_TO_DATE', Sequelize.col('insert_date'), '%d/%m/%Y'), 'DESC'],
                    ['firstname', 'ASC'],
                    ['lastname', 'ASC'],
                ],
            });

            // Query untuk mendapatkan totalData
            const totalDataResult = await Master.findAll({
                attributes: [
                    [Sequelize.fn('STR_TO_DATE', Sequelize.col('insert_date'), '%d/%m/%Y'), 'rsv_date'],
                    'confirmation_no',
                    'mp',
                    'market_code',
                    'market_group',
                    'firstname',
                    'lastname',
                    'nationality',
                    'country',
                    'city',
                    'email',
                    'telephone',
                    'resv_status'
                ],
                where: filter ? {
                    [Op.or]: [{
                        confirmation_no: {
                            [Op.like]: `%${filter}%`
                        }
                    },
                    {
                        mp: {
                            [Op.like]: `%${filter}%`
                        }
                    },
                    {
                        market_code: {
                            [Op.like]: `%${filter}%`
                        }
                    },
                    {
                        firstname: {
                            [Op.eq]: `${filter}`
                        }
                    },
                    {
                        lastname: {
                            [Op.like]: `%${filter}%`
                        }
                    },
                    {
                        country: {
                            [Op.like]: `%${filter}%`
                        }
                    },
                    {
                        city: {
                            [Op.like]: `%${filter}%`
                        }
                    },
                    {
                        email: {
                            [Op.like]: `%${filter}%`
                        }
                    }
                    ],
                    [Op.and]: [
                        { mp: '' },
                        { mp_level: '' },
                        { email: { [Op.ne]: '' } },
                        // { birthday: { [Op.not]: null } },
                        { arrival_date: { [Op.not]: '' } },
                        { departure_date: { [Op.not]: '' } },
                        { stay_date: { [Op.not]: '' } },
                        { resv_status: { [Op.in]: ['CHECKED IN', 'CHECKED OUT'] } },
                        Sequelize.literal("STR_TO_DATE(`insert_date`, '%d/%m/%Y') >= STR_TO_DATE('" + startDate + "', '%Y-%m-%d')")
                    ],
                    region: uppercaseLocation,
                } : {
                    [Op.and]: [
                        { mp: '' },
                        { mp_level: '' },
                        { email: { [Op.ne]: '' } },
                        // { birthday: { [Op.not]: null } },
                        { arrival_date: { [Op.not]: '' } },
                        { departure_date: { [Op.not]: '' } },
                        { stay_date: { [Op.not]: '' } },
                        { resv_status: { [Op.in]: ['CHECKED IN', 'CHECKED OUT'] } },
                        Sequelize.literal("STR_TO_DATE(`insert_date`, '%d/%m/%Y') >= STR_TO_DATE('" + startDate + "', '%Y-%m-%d')")
                    ],
                    region: uppercaseLocation,
                },
                group: [
                    'confirmation_no',
                    'mp',
                    'market_code',
                    'market_group',
                    'firstname',
                    'lastname',
                    'nationality',
                    'country',
                    'city',
                    'email',
                    'telephone',
                    'resv_status',
                    Sequelize.fn('STR_TO_DATE', Sequelize.col('insert_date'), '%d/%m/%Y')
                ],
                order: [
                    [Sequelize.fn('STR_TO_DATE', Sequelize.col('insert_date'), '%d/%m/%Y'), 'DESC'],
                    ['firstname', 'ASC'],
                    ['lastname', 'ASC'],
                ],
            });

            const totalData = totalDataResult ? totalDataResult.length : 0;


            res.status(200).json({
                data: result,
                totalData: totalData,
            });
        } catch (error) {
            console.log(error);
        }

    }

    static async getEnrolls_OLD(req, res) {
        const startDate = moment().subtract(24, 'months').startOf('day').format('YYYY-MM-DD'); // 2 tahun ke belakang
        const endDate = moment().endOf('day').format('YYYY-MM-DD');

        try {

            const result = await Master.findAll({
                attributes: [
                    [Sequelize.fn('STR_TO_DATE', Sequelize.col('insert_date'), '%d/%m/%Y'), 'rsv_date'],
                    'confirmation_no',
                    'mp',
                    'market_code',
                    'firstname',
                    'lastname',
                    'nationality',
                    'country',
                    'city',
                    'email',
                    'telephone',
                    'resv_status'
                ],
                where: {
                    [Op.and]: [
                        { mp: '' },
                        { mp_level: '' },
                        { email: { [Op.ne]: '' } },
                        { birthday: { [Op.not]: null } },
                        { arrival_date: { [Op.not]: '' } },
                        { departure_date: { [Op.not]: '' } },
                        { stay_date: { [Op.not]: '' } },
                        //{ room_rev_usd: { [Op.gt]: 0 } },
                        //{ fb_rev_usd: { [Op.gt]: 0 } },
                        //{ market_code: { [Op.in]: ['RAC', 'WEB', 'PACKDOM', 'PACKINT']}},
                        { resv_status: { [Op.in]: ['CHECKED IN', 'CHECKED OUT'] } },
                        Sequelize.literal("STR_TO_DATE(`insert_date`, '%d/%m/%Y') BETWEEN '" + startDate + "' AND '" + endDate + "'")
                    ],
                },
                group: [
                    'confirmation_no',
                    'mp',
                    'market_code',
                    'firstname',
                    'lastname',
                    'nationality',
                    'country',
                    'city',
                    'email',
                    'telephone',
                    'resv_status',
                    Sequelize.fn('STR_TO_DATE', Sequelize.col('insert_date'), '%d/%m/%Y')
                ],
                order: [
                    [Sequelize.fn('STR_TO_DATE', Sequelize.col('insert_date'), '%d/%m/%Y'), 'DESC'],
                    ['firstname', 'ASC'],
                    ['lastname', 'ASC'],
                ],
            });

            res.status(200).json(result);
        } catch (error) {
            console.log(error);
        }

    };

    static async getOperas(req, res) {

        console.log(req.query, "AMBIL MEMBER OPERA ??????????????????")

        const startDate = moment().subtract(24, 'months').startOf('day').format('YYYY-MM-DD'); // 2 tahun ke belakang
        const endDate = moment().endOf('day').format('YYYY-MM-DD');

        try {

            // Step 1: Fetch multiplier values from Settings
            const settings = await Setting.findOne({ where: { id: 1 } });
            if (!settings) {
                return res.status(404).json({ error: 'Settings not found.' });
            }

            const mpsMultiplier = parseFloat(settings.room_point_silver) / parseFloat(settings.room_amount); // Default multiplier
            const mpgMultiplier = parseFloat(settings.room_point_gold) / parseFloat(settings.room_amount); // Default multiplier
            const mpdMultiplier = parseFloat(settings.room_point_diamond) / parseFloat(settings.room_amount); // Default multiplier
            const fbsMultiplier = parseFloat(settings.fb_point_silver) / parseFloat(settings.fb_amount); // Default multiplier
            const fbgMultiplier = parseFloat(settings.fb_point_gold) / parseFloat(settings.fb_amount); // Default multiplier
            const fbdMultiplier = parseFloat(settings.fb_point_diamond) / parseFloat(settings.fb_amount); // Default multiplier


            const result = await Master.findAll({
                attributes: [
                    [Sequelize.fn('STR_TO_DATE', Sequelize.col('insert_date'), '%d/%m/%Y'), 'rsv_date'],
                    'confirmation_no',
                    'mp',
                    'market_code',
                    'firstname',
                    'lastname',
                    [Sequelize.fn('SUM', Sequelize.col('room_rev_usd')), 'room_usd'], // Sum of room_rev_usd
                    [Sequelize.fn('SUM', Sequelize.col('fb_rev_usd')), 'fb_usd'], // Sum of fb_rev_usd    
                    [
                        Sequelize.fn(
                            'FLOOR',
                            Sequelize.fn(
                                'SUM',
                                Sequelize.literal(`
                                    CASE 
                                        WHEN mp_level = 'MPS' THEN (room_rev_usd * ${mpsMultiplier}) + (fb_rev_usd * ${fbsMultiplier})
                                        WHEN mp_level = 'MPG' THEN (room_rev_usd * ${mpgMultiplier}) + (fb_rev_usd * ${fbgMultiplier})
                                        WHEN mp_level = 'MPD' THEN (room_rev_usd * ${mpdMultiplier}) + (fb_rev_usd * ${fbdMultiplier})
                                        ELSE (room_rev_usd * ${mpsMultiplier}) + (fb_rev_usd * ${fbsMultiplier})
                                    END
                                `)
                            )
                        ),
                        'total_points'
                    ],
                    'arrival_date',
                    'departure_date',
                    'email',
                    'resv_status'
                ],
                include: [
                    {
                        model: Member,
                        as: "members",
                        attributes: [],
                        required: false,
                        where: {
                            member_id: { [Op.eq]: null },
                        },
                    },
                ],
                where: {
                    [Op.and]: [
                        { email: { [Op.not]: '' } },
                        { mp: { [Op.ne]: '' } },
                        { arrival_date: { [Op.not]: '' } },
                        { departure_date: { [Op.not]: '' } },
                        { stay_date: { [Op.not]: '' } },
                        // { market_code: { [Op.in]: ['RAC', 'WEB', 'PACKDOM', 'PACKINT'] } },
                        { resv_status: { [Op.eq]: 'CHECKED OUT' } },
                        { flag: { [Op.eq]: 0 } },
                    ],
                },
                group: [
                    'confirmation_no',
                    'mp',
                    'market_code',
                    'firstname',
                    'lastname',
                    'arrival_date',
                    'departure_date',
                    'email',
                    'resv_status',
                    Sequelize.fn('STR_TO_DATE', Sequelize.col('insert_date'), '%d/%m/%Y'),
                ],
                order: [
                    [Sequelize.fn('STR_TO_DATE', Sequelize.col('insert_date'), '%d/%m/%Y'), 'DESC'],
                    ['firstname', 'ASC'],
                    ['lastname', 'ASC'],
                ],
            });

            res.status(200).json(result);
        } catch (error) {
            console.log(error);
        }

    };


    static async getNeedPoint(req, res, next) {
        const { page, per_page, filter, location } = req.query;
        const perPageInt = parseInt(per_page, 10);
        const offset = (page - 1) * perPageInt;

        const startDate = moment().subtract(24, 'months').startOf('day').format('YYYY-MM-DD'); // 2 tahun ke belakang
        const endDate = moment().endOf('day').format('YYYY-MM-DD');

        const uppercaseLocation = location ? location.toUpperCase() : '';

        console.log(req.query, "APAKAH MASUK KE NEEDPOINTS ??????????????????")

        try {
            // Step 1: Fetch multiplier values from Settings
            const settings = await Setting.findOne({ where: { id: 1 } });
            if (!settings) {
                return res.status(404).json({ error: 'Settings not found.' });
            }

            const mpsMultiplier = parseFloat(settings.room_point_silver) / parseFloat(settings.room_amount); // Default multiplier
            const mpgMultiplier = parseFloat(settings.room_point_gold) / parseFloat(settings.room_amount); // Default multiplier
            const mpdMultiplier = parseFloat(settings.room_point_diamond) / parseFloat(settings.room_amount); // Default multiplier
            const fbsMultiplier = parseFloat(settings.fb_point_silver) / parseFloat(settings.fb_amount); // Default multiplier
            const fbgMultiplier = parseFloat(settings.fb_point_gold) / parseFloat(settings.fb_amount); // Default multiplier
            const fbdMultiplier = parseFloat(settings.fb_point_diamond) / parseFloat(settings.fb_amount); // Default multiplier

            // Query utama untuk mendapatkan data anggota
            const resultQuery = `
                SELECT
                    STR_TO_DATE(m.insert_date, '%d/%m/%Y') as rsv_date,
                    m.confirmation_no,
                    m.mp,
                    m.room_class,
                    m.firstname,
                    m.lastname,
                    SUM(m.room_rev_usd) as room_usd,
                    SUM(m.fb_rev_usd) as fb_usd,
                    FLOOR(SUM(
                    CASE
                        WHEN m.mp_level = 'MPS' THEN (m.room_rev_usd * ${mpsMultiplier}) + (m.fb_rev_usd * ${fbsMultiplier})
                        WHEN m.mp_level = 'MPG' THEN (m.room_rev_usd * ${mpgMultiplier}) + (m.fb_rev_usd * ${fbgMultiplier})
                        WHEN m.mp_level = 'MPD' THEN (m.room_rev_usd * ${mpdMultiplier}) + (m.fb_rev_usd * ${fbdMultiplier})
                        ELSE (m.room_rev_usd * ${mpsMultiplier}) + (m.fb_rev_usd * ${fbsMultiplier})
                    END
                )) as total_points,
                    MIN(STR_TO_DATE(m.arrival_date, '%d/%m/%Y')) as arrival_date,
                    MAX(STR_TO_DATE(m.departure_date, '%d/%m/%Y')) as departure_date,
                    m.email,
                    m.resv_status
                FROM
                    Masters m
                INNER JOIN
                    Members mem ON m.mp = mem.member_id AND mem.member_id != ''
                WHERE
                    m.email != ''
                    AND m.arrival_date IS NOT NULL
                    AND m.departure_date IS NOT NULL
                    AND m.stay_date IS NOT NULL
                    ${uppercaseLocation == 'BALI' ? `AND m.market_code IN ('RAC', 'WEB', 'PACKDOM', 'PACKINT')` : `AND m.market_group IN ('COT', 'PKG', 'RCK', 'VCO', 'LST')`}
                    AND m.resv_status = 'CHECKED OUT'
                    AND m.flag = 0
                    AND m.region = '${uppercaseLocation}'
                    ${filter ? `AND (
                        m.confirmation_no LIKE '%${filter}%' OR
                        m.mp LIKE '%${filter}%' OR
                        m.room_class LIKE '%${filter}%' OR
                        m.firstname LIKE '%${filter}%' OR
                        m.lastname LIKE '%${filter}%' OR
                        m.email LIKE '%${filter}%' OR
                        m.resv_status LIKE '%${filter}%'
                    )` : ''}
                GROUP BY
                    m.confirmation_no,
                    m.mp,
                    m.room_class,
                    m.firstname,
                    m.lastname,
                    m.email,
                    m.resv_status,
                    m.region,
                    STR_TO_DATE(m.insert_date, '%d/%m/%Y')
                HAVING
                    COUNT(m.mp) > 1 AND
                    COUNT(m.confirmation_no) > 1 AND
                    total_points > 0
                ORDER BY
                    STR_TO_DATE(m.insert_date, '%d/%m/%Y') DESC,
                    m.firstname ASC,
                    m.lastname ASC
                LIMIT :limit OFFSET :offset
            `;

            const result = await sequelize.query(resultQuery, {
                replacements: { limit: perPageInt, offset: offset },
                type: sequelize.QueryTypes.SELECT,
            });

            // Query untuk mendapatkan total data
            const totalDataQuery = `
                SELECT
                    COUNT(*) as total
                FROM (
                    SELECT
                        m.confirmation_no
                    FROM
                        Masters m
                    INNER JOIN
                        Members mem ON m.mp = mem.member_id AND mem.member_id != ''
                    WHERE
                        m.email != ''
                        AND m.arrival_date IS NOT NULL
                        AND m.departure_date IS NOT NULL
                        AND m.stay_date IS NOT NULL
                        ${uppercaseLocation == 'BALI' ? `AND m.market_code IN ('RAC', 'WEB', 'PACKDOM', 'PACKINT')` : `AND m.market_group IN ('COT', 'PKG', 'RCK', 'VCO', 'LST')`}
                        AND m.resv_status = 'CHECKED OUT'
                        AND m.flag = 0
                        AND m.region = '${uppercaseLocation}'
                        ${filter ? `AND (
                            m.confirmation_no LIKE '%${filter}%' OR
                            m.mp LIKE '%${filter}%' OR
                            m.room_class LIKE '%${filter}%' OR
                            m.firstname LIKE '%${filter}%' OR
                            m.lastname LIKE '%${filter}%' OR
                            m.email LIKE '%${filter}%' OR
                            m.resv_status LIKE '%${filter}%'
                        )` : ''}
                    GROUP BY
                        m.confirmation_no,
                        m.mp,
                        m.room_class,
                        m.firstname,
                        m.lastname,
                        m.email,
                        m.resv_status,
                        m.region,
                        STR_TO_DATE(m.insert_date, '%d/%m/%Y')
                    HAVING
                        COUNT(m.mp) > 1 AND
                        COUNT(m.confirmation_no) > 1 AND
                        FLOOR(SUM(
                        CASE
                            WHEN m.mp_level = 'MPS' THEN (m.room_rev_usd * ${mpsMultiplier}) + (m.fb_rev_usd * ${fbsMultiplier})
                            WHEN m.mp_level = 'MPG' THEN (m.room_rev_usd * ${mpgMultiplier}) + (m.fb_rev_usd * ${fbgMultiplier})
                            WHEN m.mp_level = 'MPD' THEN (m.room_rev_usd * ${mpdMultiplier}) + (m.fb_rev_usd * ${fbdMultiplier})
                            ELSE (m.room_rev_usd * ${mpsMultiplier}) + (m.fb_rev_usd * ${fbsMultiplier})
                        END
                    )) > 0
                ) as subquery
            `;

            const totalDataResult = await sequelize.query(totalDataQuery, {
                type: sequelize.QueryTypes.SELECT,
            });

            const totalData = totalDataResult[0].total;

            res.status(200).json({
                data: result,
                totalData: totalData,
            });
        } catch (error) {
            console.log(error);
            res.status(500).json({ error: 'An error occurred while fetching need points data.' });
        }
    }


    static async getNeedPointXXXOLD(req, res, next) {
        const { page, per_page, filter } = req.query;
        const perPageInt = parseInt(per_page, 10);
        const offset = (page - 1) * per_page;

        const startDate = moment().subtract(24, 'months').startOf('day').format('YYYY-MM-DD'); // 2 tahun ke belakang
        const endDate = moment().endOf('day').format('YYYY-MM-DD');

        console.log(req.query, "APAKAH MASUK KE NEEDPOINTS ??????????????????")

        try {
            // Count all records
            const result = await Master.findAll({
                attributes: [
                    [Sequelize.fn('STR_TO_DATE', Sequelize.col('insert_date'), '%d/%m/%Y'), 'rsv_date'],
                    'confirmation_no',
                    'mp',
                    'room_class',
                    'firstname',
                    'lastname',
                    [Sequelize.fn('SUM', Sequelize.col('room_rev_usd')), 'room_usd'],
                    [Sequelize.fn('SUM', Sequelize.col('fb_rev_usd')), 'fb_usd'],
                    [Sequelize.fn('FLOOR', Sequelize.fn('SUM', Sequelize.literal('(room_rev_usd * 5) + (fb_rev_usd * 0.5)'))), 'total_points'],
                    [Sequelize.fn('MIN', Sequelize.col('arrival_date')), 'arrival_date'],
                    [Sequelize.fn('MAX', Sequelize.col('departure_date')), 'departure_date'],
                    'email',
                    'resv_status'
                ],
                include: [
                    {
                        model: Member,
                        as: "members",
                        attributes: [],
                        required: true,
                        where: {
                            member_id: { [Op.ne]: '' },
                        },
                    },
                ],
                where: filter ? {
                    [Op.or]: [{
                        confirmation_no: {
                            [Op.like]: `%${filter}%`
                        }
                    },
                    {
                        mp: {
                            [Op.like]: `%${filter}%`
                        }
                    },
                    {
                        room_class: {
                            [Op.like]: `%${filter}%`
                        }
                    },
                    {
                        firstname: {
                            [Op.eq]: `${filter}`
                        }
                    },
                    {
                        lastname: {
                            [Op.like]: `%${filter}%`
                        }
                    },
                    {
                        email: {
                            [Op.like]: `%${filter}%`
                        }
                    },
                    {
                        resv_status: {
                            [Op.like]: `%${filter}%`
                        }
                    }
                    ],
                    [Op.and]: [
                        { email: { [Op.ne]: '' } },
                        { arrival_date: { [Op.not]: '' } },
                        { departure_date: { [Op.not]: '' } },
                        { stay_date: { [Op.not]: '' } },
                        { market_code: { [Op.in]: ['RAC', 'WEB', 'PACKDOM', 'PACKINT'] } },
                        { resv_status: { [Op.eq]: 'CHECKED OUT' } },
                        { flag: { [Op.eq]: 0 } },
                    ],
                } : {
                    [Op.and]: [
                        { email: { [Op.ne]: '' } },
                        { arrival_date: { [Op.not]: '' } },
                        { departure_date: { [Op.not]: '' } },
                        { stay_date: { [Op.not]: '' } },
                        { market_code: { [Op.in]: ['RAC', 'WEB', 'PACKDOM', 'PACKINT'] } },
                        { resv_status: { [Op.eq]: 'CHECKED OUT' } },
                        { flag: { [Op.eq]: 0 } },
                    ],
                },
                group: [
                    'confirmation_no',
                    'mp',
                    'market_code',
                    'room_class',
                    'firstname',
                    'lastname',
                    'email',
                    'resv_status',
                    Sequelize.fn('STR_TO_DATE', Sequelize.col('insert_date'), '%d/%m/%Y'),
                ],
                order: [
                    [Sequelize.fn('STR_TO_DATE', Sequelize.col('insert_date'), '%d/%m/%Y'), 'DESC'],
                    ['firstname', 'ASC'],
                    ['lastname', 'ASC'],
                ],
                having: {
                    // Menggunakan HAVING untuk menampilkan hanya data dengan count mp = 1 dan count confirmation_no = 1
                    [Op.and]: [
                        Sequelize.literal('COUNT(mp) > 1'),
                        Sequelize.literal('COUNT(confirmation_no) > 1'),
                        Sequelize.literal('total_points > 0'),
                    ],
                },
                limit: perPageInt,
                offset: offset,
            });

            // Query untuk mendapatkan totalData
            const totalDataResult = await Master.findAll({
                attributes: [
                    [Sequelize.fn('STR_TO_DATE', Sequelize.col('insert_date'), '%d/%m/%Y'), 'rsv_date'],
                    'confirmation_no',
                    'mp',
                    'market_code',
                    'firstname',
                    'lastname',
                    [Sequelize.fn('SUM', Sequelize.col('room_rev_usd')), 'room_usd'], // Sum of room_rev_usd
                    [Sequelize.fn('SUM', Sequelize.col('fb_rev_usd')), 'fb_usd'], // Sum of fb_rev_usd    
                    [Sequelize.fn('FLOOR', Sequelize.fn('SUM', Sequelize.literal('(room_rev_usd * 5) + (fb_rev_usd * 0.5)'))), 'total_points'],
                    [Sequelize.fn('MIN', Sequelize.col('arrival_date')), 'arrival_date'], // Minimum arrival_date
                    [Sequelize.fn('MAX', Sequelize.col('departure_date')), 'departure_date'], // Maximum departure_date
                    'email',
                    'resv_status'
                ],
                where: filter ? {
                    [Op.or]: [{
                        confirmation_no: {
                            [Op.like]: `%${filter}%`
                        }
                    },
                    {
                        mp: {
                            [Op.like]: `%${filter}%`
                        }
                    },
                    {
                        market_code: {
                            [Op.like]: `%${filter}%`
                        }
                    },
                    {
                        firstname: {
                            [Op.eq]: `${filter}`
                        }
                    },
                    {
                        lastname: {
                            [Op.like]: `%${filter}%`
                        }
                    },
                    {
                        email: {
                            [Op.like]: `%${filter}%`
                        }
                    },
                    {
                        resv_status: {
                            [Op.like]: `%${filter}%`
                        }
                    }
                    ],
                    [Op.and]: [
                        { email: { [Op.not]: '' } },
                        { arrival_date: { [Op.not]: '' } },
                        { departure_date: { [Op.not]: '' } },
                        { stay_date: { [Op.not]: '' } },
                        { market_code: { [Op.in]: ['RAC', 'WEB', 'PACKDOM', 'PACKINT'] } },
                        { resv_status: { [Op.eq]: 'CHECKED OUT' } },
                        { flag: { [Op.eq]: 0 } },
                    ],
                } : {
                    [Op.and]: [
                        { email: { [Op.ne]: '' } },
                        { arrival_date: { [Op.not]: '' } },
                        { departure_date: { [Op.not]: '' } },
                        { stay_date: { [Op.not]: '' } },
                        { market_code: { [Op.in]: ['RAC', 'WEB', 'PACKDOM', 'PACKINT'] } },
                        { resv_status: { [Op.eq]: 'CHECKED OUT' } },
                        { flag: { [Op.eq]: 0 } },
                    ],
                },
                group: [
                    'confirmation_no',
                    'mp',
                    'market_code',
                    'room_class',
                    'firstname',
                    'lastname',
                    'email',
                    'resv_status',
                    Sequelize.fn('STR_TO_DATE', Sequelize.col('insert_date'), '%d/%m/%Y'),
                ],
                having: {
                    // Menggunakan HAVING untuk menampilkan hanya data dengan count mp = 1 dan count confirmation_no = 1
                    [Op.and]: [
                        Sequelize.literal('COUNT(mp) > 1'),
                        Sequelize.literal('COUNT(confirmation_no) > 1'),
                        Sequelize.literal('total_points > 0'),
                    ],
                },
            });

            const totalData = totalDataResult ? totalDataResult.length : 0;


            res.status(200).json({
                data: result,
                totalData: totalData,
            });
        } catch (error) {
            console.log(error);
        }

    }

    static async getNeedPoint_OLD(req, res) {

        const startDate = moment().subtract(24, 'months').startOf('day').format('YYYY-MM-DD'); // 2 tahun ke belakang
        const endDate = moment().endOf('day').format('YYYY-MM-DD');

        try {

            const result = await Master.findAll({
                attributes: [
                    [Sequelize.fn('STR_TO_DATE', Sequelize.col('insert_date'), '%d/%m/%Y'), 'rsv_date'],
                    'confirmation_no',
                    'mp',
                    'room_class',
                    'firstname',
                    'lastname',
                    [Sequelize.fn('SUM', Sequelize.col('room_rev_usd')), 'room_usd'], // Sum of room_rev_usd
                    [Sequelize.fn('SUM', Sequelize.col('fb_rev_usd')), 'fb_usd'], // Sum of fb_rev_usd    
                    [Sequelize.fn('FLOOR', Sequelize.fn('SUM', Sequelize.literal('(room_rev_usd * 5) + (fb_rev_usd * 0.5)'))), 'total_points'],
                    [Sequelize.fn('MIN', Sequelize.col('arrival_date')), 'arrival_date'], // Minimum arrival_date
                    [Sequelize.fn('MAX', Sequelize.col('departure_date')), 'departure_date'], // Maximum departure_date
                    'email',
                    'resv_status'
                ],
                include: [
                    {
                        model: Member,
                        as: "members",
                        attributes: [],
                        required: true, // Use 'required: true' for INNER JOIN or 'required: false' for LEFT JOIN
                        where: {
                            member_id: { [Op.ne]: '' },
                        },
                    },
                ],
                where: {
                    [Op.and]: [
                        { email: { [Op.not]: '' } },
                        { arrival_date: { [Op.not]: '' } },
                        { departure_date: { [Op.not]: '' } },
                        { stay_date: { [Op.not]: '' } },
                        { market_code: { [Op.in]: ['RAC', 'WEB', 'PACKDOM', 'PACKINT'] } },
                        { resv_status: { [Op.eq]: 'CHECKED OUT' } },
                        { flag: { [Op.eq]: 0 } },
                    ],
                },
                group: [
                    'confirmation_no',
                    'mp',
                    'market_code',
                    'room_class',
                    'firstname',
                    'lastname',
                    'email',
                    'resv_status',
                    Sequelize.fn('STR_TO_DATE', Sequelize.col('insert_date'), '%d/%m/%Y'),
                ],
                order: [
                    [Sequelize.fn('STR_TO_DATE', Sequelize.col('insert_date'), '%d/%m/%Y'), 'DESC'],
                    ['firstname', 'ASC'],
                    ['lastname', 'ASC'],
                ],
                having: Sequelize.where(Sequelize.literal('total_points'), '>', 0),
            });

            const filteredResult = result.filter(item => {
                // Count the occurrences of each mp and confirmation_no
                const mpCount = result.filter(innerItem => innerItem.mp === item.mp).length;
                const confirmationCount = result.filter(innerItem => innerItem.confirmation_no === item.confirmation_no).length;

                // Return items with both mp and confirmation_no having more than one occurrence
                return mpCount > 1 && confirmationCount > 1;
            });

            res.status(200).json(filteredResult);
        } catch (error) {
            console.log(error);
        }
    };

    static async getDetailMemberTrx(req, res) {
        let { email, resdate } = req.query
        let whereClause = {};

        if (resdate != 'undefined') {
            whereClause = {
                [Op.and]: [
                    { email: email },
                    { arrival_date: { [Op.not]: '' } },
                    { departure_date: { [Op.not]: '' } },
                    { market_code: { [Op.in]: ['RAC', 'WEB', 'PACKDOM', 'PACKINT'] } },
                    Sequelize.where(Sequelize.fn('STR_TO_DATE', Sequelize.col('insert_date'), '%d/%m/%Y'), resdate),
                ]
            };
        } else {
            whereClause = {
                [Op.and]: [
                    { email: email },
                    { arrival_date: { [Op.not]: '' } },
                    { departure_date: { [Op.not]: '' } },
                    { market_code: { [Op.in]: ['RAC', 'WEB', 'PACKDOM', 'PACKINT'] } },
                ]
            };
        }

        try {

            // Step 1: Fetch multiplier values from Settings
            const settings = await Setting.findOne({ where: { id: 1 } });
            if (!settings) {
                return res.status(404).json({ error: 'Settings not found.' });
            }

            const mpsMultiplier = parseFloat(settings.room_point_silver) / parseFloat(settings.room_amount); // Default multiplier
            const mpgMultiplier = parseFloat(settings.room_point_gold) / parseFloat(settings.room_amount); // Default multiplier
            const mpdMultiplier = parseFloat(settings.room_point_diamond) / parseFloat(settings.room_amount); // Default multiplier
            const fbsMultiplier = parseFloat(settings.fb_point_silver) / parseFloat(settings.fb_amount); // Default multiplier
            const fbgMultiplier = parseFloat(settings.fb_point_gold) / parseFloat(settings.fb_amount); // Default multiplier
            const fbdMultiplier = parseFloat(settings.fb_point_diamond) / parseFloat(settings.fb_amount); // Default multiplier


            const member = await Master.findAll({
                attributes: [
                    'confirmation_no',
                    'mp',
                    'market_code',
                    'firstname',
                    'lastname',
                    [
                        Sequelize.fn(
                            'SUM',
                            Sequelize.cast(
                                Sequelize.fn('REPLACE', Sequelize.col('room_rev_usd'), ',', ''),
                                'DECIMAL'
                            )
                        ),
                        'room_usd',
                    ],
                    [
                        Sequelize.fn(
                            'SUM',
                            Sequelize.cast(
                                Sequelize.fn('REPLACE', Sequelize.col('fb_rev_usd'), ',', ''),
                                'DECIMAL'
                            )
                        ),
                        'fb_usd',
                    ],
                    [
                        Sequelize.fn(
                            'FLOOR',
                            Sequelize.fn(
                                'SUM',
                                Sequelize.literal(`
                                    CASE 
                                        WHEN mp_level = 'MPS' THEN (room_rev_usd * ${mpsMultiplier}) + (fb_rev_usd * ${fbsMultiplier})
                                        WHEN mp_level = 'MPG' THEN (room_rev_usd * ${mpgMultiplier}) + (fb_rev_usd * ${fbgMultiplier})
                                        WHEN mp_level = 'MPD' THEN (room_rev_usd * ${mpdMultiplier}) + (fb_rev_usd * ${fbdMultiplier})
                                        ELSE (room_rev_usd * ${mpsMultiplier}) + (fb_rev_usd * ${fbsMultiplier})
                                    END
                                `)
                            )
                        ),
                        'total_points'
                    ],
                    'room_class',
                    [Sequelize.fn('MIN', Sequelize.col('arrival_date')), 'arrival_date'], // Minimum arrival_date
                    [Sequelize.fn('MAX', Sequelize.col('departure_date')), 'departure_date'], // Maximum departure_date
                ],
                where: whereClause,
                group: [
                    'confirmation_no',
                    'mp',
                    'market_code',
                    'firstname',
                    'lastname',
                    'room_class',
                ],
                order: [
                    [Sequelize.fn('SUM', Sequelize.col('room_rev_usd')), 'DESC'],
                    [Sequelize.fn('SUM', Sequelize.col('fb_rev_usd')), 'DESC']
                ],
                raw: true,
                having: Sequelize.where(Sequelize.literal('total_points'), '>', 0),
            });
            res.status(200).json(member);
        } catch (error) {
            console.log(error);
        }
    };


    static async getAutoPoint(req, res) {
        const { page, per_page, filter, location } = req.query;
        const perPageInt = parseInt(per_page, 10);
        const offset = (page - 1) * perPageInt;

        const uppercaseLocation = location ? location.toUpperCase() : '';

        try {
            // Step 1: Fetch multiplier values from Settings
            const settings = await Setting.findOne({ where: { id: 1 } });
            if (!settings) {
                return res.status(404).json({ error: 'Settings not found.' });
            }

            const mpsMultiplier = parseFloat(settings.room_point_silver) / parseFloat(settings.room_amount); // Default multiplier
            const mpgMultiplier = parseFloat(settings.room_point_gold) / parseFloat(settings.room_amount); // Default multiplier
            const mpdMultiplier = parseFloat(settings.room_point_diamond) / parseFloat(settings.room_amount); // Default multiplier
            const fbsMultiplier = parseFloat(settings.fb_point_silver) / parseFloat(settings.fb_amount); // Default multiplier
            const fbgMultiplier = parseFloat(settings.fb_point_gold) / parseFloat(settings.fb_amount); // Default multiplier
            const fbdMultiplier = parseFloat(settings.fb_point_diamond) / parseFloat(settings.fb_amount); // Default multiplier

            // Query utama untuk mendapatkan data anggota
            const resultQuery = `
                SELECT
                    STR_TO_DATE(m.insert_date, '%d/%m/%Y') as rsv_date,
                    m.confirmation_no,
                    m.mp,
                    m.market_code,
                    m.market_group,
                    m.room_class,
                    m.firstname,
                    m.lastname,
                    SUM(m.room_rev_idr) as room_idr,
                    SUM(m.room_rev_usd) as room_usd,
                    SUM(m.fb_rev_idr) as fb_idr,
                    SUM(m.fb_rev_usd) as fb_usd,
                    FLOOR(SUM(
                    CASE
                        WHEN mem.member_level = 'MPS' THEN (m.room_rev_usd * ${mpsMultiplier})
                        WHEN mem.member_level = 'MPG' THEN (m.room_rev_usd * ${mpgMultiplier}) 
                        WHEN mem.member_level = 'MPD' THEN (m.room_rev_usd * ${mpdMultiplier})
                        ELSE (m.room_rev_usd * ${mpsMultiplier})
                    END
                )) as room_points,
                  FLOOR(SUM(
                    CASE
                        WHEN mem.member_level = 'MPS' THEN (m.fb_rev_usd * ${fbsMultiplier})
                        WHEN mem.member_level = 'MPG' THEN (m.fb_rev_usd * ${fbgMultiplier})
                        WHEN mem.member_level = 'MPD' THEN (m.fb_rev_usd * ${fbdMultiplier})
                        ELSE (m.fb_rev_usd * ${fbsMultiplier})
                    END
                )) as fb_points,
                    FLOOR(SUM(
                    CASE
                        WHEN mem.member_level = 'MPS' THEN (m.room_rev_usd * ${mpsMultiplier}) + (m.fb_rev_usd * ${fbsMultiplier})
                        WHEN mem.member_level = 'MPG' THEN (m.room_rev_usd * ${mpgMultiplier}) + (m.fb_rev_usd * ${fbgMultiplier})
                        WHEN mem.member_level = 'MPD' THEN (m.room_rev_usd * ${mpdMultiplier}) + (m.fb_rev_usd * ${fbdMultiplier})
                        ELSE (m.room_rev_usd * ${mpsMultiplier}) + (m.fb_rev_usd * ${fbsMultiplier})
                    END
                )) as total_points,
                    MIN(STR_TO_DATE(m.arrival_date, '%d/%m/%Y')) as arrival_date,
                    MAX(STR_TO_DATE(m.departure_date, '%d/%m/%Y')) as departure_date,
                    m.email,
                    m.resv_status,
                    m.region,
                    m.point_promotion,
                    m.agent_name
                FROM
                    Masters m
                INNER JOIN
                    Members mem ON m.mp = mem.member_id AND mem.member_id != ''
                WHERE
                    m.email != ''
                    AND m.arrival_date IS NOT NULL
                    AND m.departure_date IS NOT NULL
                    AND m.stay_date IS NOT NULL
                    ${uppercaseLocation == 'BALI' ? `AND m.market_code IN ('RAC', 'WEB', 'PACKDOM', 'PACKINT')` : `AND m.market_group IN ('COT', 'PKG', 'RCK', 'VCO', 'LST')`}
                    AND m.resv_status = 'CHECKED OUT'
                    AND m.flag = 0
                    AND m.region = '${uppercaseLocation}'
                    ${filter ? `AND (
                        m.confirmation_no LIKE '%${filter}%' OR
                        m.mp LIKE '%${filter}%' OR
                        m.market_code LIKE '%${filter}%' OR
                        m.firstname LIKE '%${filter}%' OR
                        m.lastname LIKE '%${filter}%' OR
                        m.email LIKE '%${filter}%' OR
                        m.resv_status LIKE '%${filter}%'
                    )` : ''}
                GROUP BY
                    m.confirmation_no,
                    m.mp,
                    m.market_code,
                    m.market_group,
                    m.room_class,
                    m.firstname,
                    m.lastname,
                    m.email,
                    m.resv_status,
                    m.region,
                    m.point_promotion,
                    m.agent_name,
                    STR_TO_DATE(m.insert_date, '%d/%m/%Y'),
                    STR_TO_DATE(m.departure_date, '%d/%m/%Y')
                HAVING
                    COUNT(m.mp) = 1 AND
                    COUNT(m.confirmation_no) = 1 AND
                    total_points > 0
                ORDER BY
                    m.mp ASC, 
                    STR_TO_DATE(m.departure_date, '%d/%m/%Y') ASC,
                    m.firstname ASC,
                    m.lastname ASC
                LIMIT :limit OFFSET :offset
            `;

            const result = await sequelize.query(resultQuery, {
                replacements: { limit: perPageInt, offset: offset },
                type: sequelize.QueryTypes.SELECT,
            });


            // Query untuk mendapatkan total data
            const totalDataQuery = `
                SELECT
                    COUNT(*) as total
                FROM (
                    SELECT
                        m.confirmation_no
                    FROM
                        Masters m
                    INNER JOIN
                        Members mem ON m.mp = mem.member_id AND mem.member_id != ''
                    WHERE
                        m.email != ''
                        AND m.arrival_date IS NOT NULL
                        AND m.departure_date IS NOT NULL
                        AND m.stay_date IS NOT NULL
                        ${uppercaseLocation == 'BALI' ? `AND m.market_code IN ('RAC', 'WEB', 'PACKDOM', 'PACKINT')` : `AND m.market_group IN ('COT', 'PKG', 'RCK', 'VCO', 'LST')`}
                        AND m.resv_status = 'CHECKED OUT'
                        AND m.flag = 0
                        AND m.region = '${uppercaseLocation}'
                        ${filter ? `AND (
                            m.confirmation_no LIKE '%${filter}%' OR
                            m.mp LIKE '%${filter}%' OR
                            m.market_code LIKE '%${filter}%' OR
                            m.room_class LIKE '%${filter}%' OR
                            m.firstname LIKE '%${filter}%' OR
                            m.lastname LIKE '%${filter}%' OR
                            m.email LIKE '%${filter}%' OR
                            m.resv_status LIKE '%${filter}%'
                        )` : ''}
                    GROUP BY
                        m.confirmation_no,
                        m.mp,
                        m.market_code,
                        m.market_group,
                        m.room_class,
                        m.firstname,
                        m.lastname,
                        m.email,
                        m.resv_status,
                        m.region,
                        m.point_promotion,
                        m.agent_name,
                        STR_TO_DATE(m.insert_date, '%d/%m/%Y')
                    HAVING
                        COUNT(m.mp) = 1 AND
                        COUNT(m.confirmation_no) = 1 AND
                        FLOOR(SUM(
                        CASE
                            WHEN mem.member_level = 'MPS' THEN (m.room_rev_usd * ${mpsMultiplier}) + (m.fb_rev_usd * ${fbsMultiplier})
                            WHEN mem.member_level = 'MPG' THEN (m.room_rev_usd * ${mpgMultiplier}) + (m.fb_rev_usd * ${fbgMultiplier})
                            WHEN mem.member_level = 'MPD' THEN (m.room_rev_usd * ${mpdMultiplier}) + (m.fb_rev_usd * ${fbdMultiplier})
                            ELSE (m.room_rev_usd * ${mpsMultiplier}) + (m.fb_rev_usd * ${fbsMultiplier})
                        END
                    )) > 0
                ) as subquery
            `;

            const totalDataResult = await sequelize.query(totalDataQuery, {
                type: sequelize.QueryTypes.SELECT,
            });

            const totalData = totalDataResult[0].total;

            res.status(200).json({
                data: result,
                totalData: totalData,
            });
        } catch (error) {
            console.log(error);
            res.status(500).json({ error: 'An error occurred while fetching auto points data.' });
        }
    }


    static async getAutoWccPoint(req, res) {
        const { page, per_page, filter, location } = req.query;
        const perPageInt = parseInt(per_page, 10);
        const offset = (page - 1) * perPageInt;

        const uppercaseLocation = location ? location.toUpperCase() : '';

        try {
            // Step 1: Fetch multiplier values from Settings
            const settings = await Setting.findOne({ where: { id: 1 } });
            if (!settings) {
                return res.status(404).json({ error: 'Settings not found.' });
            }

            const mpsMultiplier = parseFloat(settings.room_point_silver) / parseFloat(settings.room_amount); // Default multiplier
            const mpgMultiplier = parseFloat(settings.room_point_gold) / parseFloat(settings.room_amount); // Default multiplier
            const mpdMultiplier = parseFloat(settings.room_point_diamond) / parseFloat(settings.room_amount); // Default multiplier
            const fbsMultiplier = parseFloat(settings.fb_point_silver) / parseFloat(settings.fb_amount); // Default multiplier
            const fbgMultiplier = parseFloat(settings.fb_point_gold) / parseFloat(settings.fb_amount); // Default multiplier
            const fbdMultiplier = parseFloat(settings.fb_point_diamond) / parseFloat(settings.fb_amount); // Default multiplier

            // Query utama untuk mendapatkan data anggota
            const resultQuery = `
                SELECT
                    STR_TO_DATE(m.insert_date, '%d/%m/%Y') as rsv_date,
                    m.confirmation_no,
                    m.wcc_member_id as mp,
                    m.market_code,
                    m.market_group,
                    m.room_class,
                    mem.first as firstname,
                    mem.last as lastname,
                    SUM(m.room_rev_idr) as room_idr,
                    SUM(m.room_rev_usd) as room_usd,
                    SUM(m.fb_rev_idr) as fb_idr,
                    SUM(m.fb_rev_usd) as fb_usd,
                    FLOOR(SUM(
                    CASE
                        WHEN mem.member_level = 'MPS' THEN (m.room_rev_usd * ${mpsMultiplier})
                        WHEN mem.member_level = 'MPG' THEN (m.room_rev_usd * ${mpgMultiplier}) 
                        WHEN mem.member_level = 'MPD' THEN (m.room_rev_usd * ${mpdMultiplier})
                        ELSE (m.room_rev_usd * ${mpsMultiplier})
                    END
                )) as room_points,
                  FLOOR(SUM(
                    CASE
                        WHEN mem.member_level = 'MPS' THEN (m.fb_rev_usd * ${fbsMultiplier})
                        WHEN mem.member_level = 'MPG' THEN (m.fb_rev_usd * ${fbgMultiplier})
                        WHEN mem.member_level = 'MPD' THEN (m.fb_rev_usd * ${fbdMultiplier})
                        ELSE (m.fb_rev_usd * ${fbsMultiplier})
                    END
                )) as fb_points,
                    FLOOR(SUM(
                    CASE
                        WHEN mem.member_level = 'MPS' THEN (m.room_rev_usd * ${mpsMultiplier}) + (m.fb_rev_usd * ${fbsMultiplier})
                        WHEN mem.member_level = 'MPG' THEN (m.room_rev_usd * ${mpgMultiplier}) + (m.fb_rev_usd * ${fbgMultiplier})
                        WHEN mem.member_level = 'MPD' THEN (m.room_rev_usd * ${mpdMultiplier}) + (m.fb_rev_usd * ${fbdMultiplier})
                        ELSE (m.room_rev_usd * ${mpsMultiplier}) + (m.fb_rev_usd * ${fbsMultiplier})
                    END
                )) as total_points,
                    MIN(STR_TO_DATE(m.arrival_date, '%d/%m/%Y')) as arrival_date,
                    MAX(STR_TO_DATE(m.departure_date, '%d/%m/%Y')) as departure_date,
                    mem.primary_email as email,
                    m.resv_status,
                    m.region,
                    m.point_promotion,
                    m.agent_name
                FROM
                    Masters m
                INNER JOIN
                    Members mem ON m.wcc_member_id = mem.member_id AND mem.member_id != ''
                WHERE
                    m.arrival_date IS NOT NULL
                    AND m.departure_date IS NOT NULL
                    AND m.stay_date IS NOT NULL
                    AND m.resv_status = 'CHECKED OUT'
                    AND m.wcc_member_id != ''
                    AND m.is_point_wcc = 1
                    AND m.wcc_enroll = ''
                    AND m.wccenroll_date = ''
                    AND m.flag = 0
                    AND m.region = '${uppercaseLocation}'
                    ${filter ? `AND (
                        m.confirmation_no LIKE '%${filter}%' OR
                        m.wcc_member_id LIKE '%${filter}%' OR
                        m.market_code LIKE '%${filter}%' OR
                        mem.first LIKE '%${filter}%' OR
                        mem.last LIKE '%${filter}%' OR
                        mem.primary_email LIKE '%${filter}%' OR
                        m.resv_status LIKE '%${filter}%'
                    )` : ''}
                GROUP BY
                    m.confirmation_no,
                    m.wcc_member_id,
                    m.market_code,
                    m.market_group,
                    m.room_class,
                    mem.first,
                    mem.last,
                    mem.primary_email,
                    m.resv_status,
                    m.region,
                    m.point_promotion,
                    m.agent_name,
                    STR_TO_DATE(m.insert_date, '%d/%m/%Y'),
                    STR_TO_DATE(m.departure_date, '%d/%m/%Y')
                HAVING
                    total_points > 0
                ORDER BY
                    m.wcc_member_id ASC, 
                    STR_TO_DATE(m.departure_date, '%d/%m/%Y') ASC,
                    mem.first ASC,
                    mem.last ASC
                LIMIT :limit OFFSET :offset
            `;

            const result = await sequelize.query(resultQuery, {
                replacements: { limit: perPageInt, offset: offset },
                type: sequelize.QueryTypes.SELECT,
            });

            // COUNT(m.wcc_member_id) = 1 AND
            // COUNT(m.confirmation_no) = 1 AND

            // Query untuk mendapatkan total data
            const totalDataQuery = `
                SELECT
                    COUNT(*) as total
                FROM (
                    SELECT
                        m.confirmation_no
                    FROM
                        Masters m
                    INNER JOIN
                        Members mem ON m.wcc_member_id = mem.member_id AND mem.member_id != ''
                    WHERE
                        m.arrival_date IS NOT NULL
                        AND m.departure_date IS NOT NULL
                        AND m.stay_date IS NOT NULL
                        AND m.resv_status = 'CHECKED OUT'
                        AND m.wcc_member_id != ''
                        AND m.is_point_wcc = 1
                        AND m.wcc_enroll = ''
                        AND m.wccenroll_date = ''
                        AND m.flag = 0
                        AND m.region = '${uppercaseLocation}'
                        ${filter ? `AND (
                            m.confirmation_no LIKE '%${filter}%' OR
                            m.wcc_member_id LIKE '%${filter}%' OR
                            m.market_code LIKE '%${filter}%' OR
                            m.room_class LIKE '%${filter}%' OR
                            mem.first LIKE '%${filter}%' OR
                            mem.last LIKE '%${filter}%' OR
                            mem.primary_email LIKE '%${filter}%' OR
                            m.resv_status LIKE '%${filter}%'
                        )` : ''}
                    GROUP BY
                        m.confirmation_no,
                        m.wcc_member_id,
                        m.market_code,
                        m.market_group,
                        m.room_class,
                        mem.first,
                        mem.last,
                        mem.primary_email,
                        m.resv_status,
                        m.region,
                        m.point_promotion,
                        m.agent_name,
                        STR_TO_DATE(m.insert_date, '%d/%m/%Y')
                    HAVING
                        FLOOR(SUM(
                        CASE
                            WHEN mem.member_level = 'MPS' THEN (m.room_rev_usd * ${mpsMultiplier}) + (m.fb_rev_usd * ${fbsMultiplier})
                            WHEN mem.member_level = 'MPG' THEN (m.room_rev_usd * ${mpgMultiplier}) + (m.fb_rev_usd * ${fbgMultiplier})
                            WHEN mem.member_level = 'MPD' THEN (m.room_rev_usd * ${mpdMultiplier}) + (m.fb_rev_usd * ${fbdMultiplier})
                            ELSE (m.room_rev_usd * ${mpsMultiplier}) + (m.fb_rev_usd * ${fbsMultiplier})
                        END
                    )) > 0
                ) as subquery
            `;

            const totalDataResult = await sequelize.query(totalDataQuery, {
                type: sequelize.QueryTypes.SELECT,
            });

            const totalData = totalDataResult[0].total;

            res.status(200).json({
                data: result,
                totalData: totalData,
            });
        } catch (error) {
            console.log(error);
            res.status(500).json({ error: 'An error occurred while fetching auto points data.' });
        }
    }


    static async getNeedExtend(req, res) {
        const { page, per_page, filter, location } = req.query;
        const perPageInt = parseInt(per_page, 10);
        const offset = (page - 1) * perPageInt;

        const uppercaseLocation = location ? location.toUpperCase() : '';

        try {
            // Step 1: Fetch multiplier values from Settings
            const settings = await Setting.findOne({ where: { id: 1 } });
            if (!settings) {
                return res.status(404).json({ error: 'Settings not found.' });
            }

            const mpsMultiplier = parseFloat(settings.room_point_silver) / parseFloat(settings.room_amount); // Default multiplier
            const mpgMultiplier = parseFloat(settings.room_point_gold) / parseFloat(settings.room_amount); // Default multiplier
            const mpdMultiplier = parseFloat(settings.room_point_diamond) / parseFloat(settings.room_amount); // Default multiplier
            const fbsMultiplier = parseFloat(settings.fb_point_silver) / parseFloat(settings.fb_amount); // Default multiplier
            const fbgMultiplier = parseFloat(settings.fb_point_gold) / parseFloat(settings.fb_amount); // Default multiplier
            const fbdMultiplier = parseFloat(settings.fb_point_diamond) / parseFloat(settings.fb_amount); // Default multiplier

            // Query utama untuk mendapatkan data anggota
            const resultQuery = `
                SELECT
                    STR_TO_DATE(m.insert_date, '%d/%m/%Y') as rsv_date,
                    m.confirmation_no,
                    m.mp,
                    m.market_code,
                    m.market_group,
                    m.room_class,
                    m.firstname,
                    m.lastname,
                    SUM(m.room_rev_usd) as room_usd,
                    SUM(m.fb_rev_usd) as fb_usd,
                    FLOOR(SUM(
                    CASE
                        WHEN mem.member_level = 'MPS' THEN (m.room_rev_usd * ${mpsMultiplier}) + (m.fb_rev_usd * ${fbsMultiplier})
                        WHEN mem.member_level = 'MPG' THEN (m.room_rev_usd * ${mpgMultiplier}) + (m.fb_rev_usd * ${fbgMultiplier})
                        WHEN mem.member_level = 'MPD' THEN (m.room_rev_usd * ${mpdMultiplier}) + (m.fb_rev_usd * ${fbdMultiplier})
                        ELSE (m.room_rev_usd * ${mpsMultiplier}) + (m.fb_rev_usd * ${fbsMultiplier})
                    END
                )) as total_points,
                    MIN(STR_TO_DATE(m.arrival_date, '%d/%m/%Y')) as arrival_date,
                    MAX(STR_TO_DATE(m.departure_date, '%d/%m/%Y')) as departure_date,
                    m.email,
                    m.resv_status,
                    m.region,
                    m.point_promotion
                FROM
                    Masters m
                INNER JOIN
                    Members mem ON m.mp = mem.member_id AND mem.member_id != ''
                WHERE
                    m.email != ''
                    AND m.arrival_date IS NOT NULL
                    AND m.departure_date IS NOT NULL
                    AND m.stay_date IS NOT NULL
                    ${uppercaseLocation == 'BALI' ? `AND m.market_code NOT IN ('RAC', 'WEB', 'PACKDOM', 'PACKINT')` : `AND m.market_group NOT IN ('COT', 'PKG', 'RCK', 'VCO', 'LST')`}
                    AND m.resv_status = 'CHECKED OUT'
                    AND m.flag = 0
                    AND m.region = '${uppercaseLocation}'
                    ${filter ? `AND (
                        m.confirmation_no LIKE '%${filter}%' OR
                        m.mp LIKE '%${filter}%' OR
                        m.market_code LIKE '%${filter}%' OR
                        m.firstname LIKE '%${filter}%' OR
                        m.lastname LIKE '%${filter}%' OR
                        m.email LIKE '%${filter}%' OR
                        m.resv_status LIKE '%${filter}%'
                    )` : ''}
                GROUP BY
                    m.confirmation_no,
                    m.mp,
                    m.market_code,
                    m.market_group,
                    m.room_class,
                    m.firstname,
                    m.lastname,
                    m.email,
                    m.resv_status,
                    m.region,
                    m.point_promotion,
                    STR_TO_DATE(m.insert_date, '%d/%m/%Y'),
                    STR_TO_DATE(m.departure_date, '%d/%m/%Y')
                HAVING
                    COUNT(m.mp) = 1 AND
                    COUNT(m.confirmation_no) = 1 AND
                    total_points > 0
                ORDER BY
                    STR_TO_DATE(m.departure_date, '%d/%m/%Y') DESC,
                    m.mp ASC, 
                    m.firstname ASC,
                    m.lastname ASC
                LIMIT :limit OFFSET :offset
            `;

            const result = await sequelize.query(resultQuery, {
                replacements: { limit: perPageInt, offset: offset },
                type: sequelize.QueryTypes.SELECT,
            });


            // Query untuk mendapatkan total data
            const totalDataQuery = `
                SELECT
                    COUNT(*) as total
                FROM (
                    SELECT
                        m.confirmation_no
                    FROM
                        Masters m
                    INNER JOIN
                        Members mem ON m.mp = mem.member_id AND mem.member_id != ''
                    WHERE
                        m.email != ''
                        AND m.arrival_date IS NOT NULL
                        AND m.departure_date IS NOT NULL
                        AND m.stay_date IS NOT NULL
                        ${uppercaseLocation == 'BALI' ? `AND m.market_code NOT IN ('RAC', 'WEB', 'PACKDOM', 'PACKINT')` : `AND m.market_group NOT IN ('COT', 'PKG', 'RCK', 'VCO', 'LST')`}
                        AND m.resv_status = 'CHECKED OUT'
                        AND m.flag = 0
                        AND m.region = '${uppercaseLocation}'
                        ${filter ? `AND (
                            m.confirmation_no LIKE '%${filter}%' OR
                            m.mp LIKE '%${filter}%' OR
                            m.market_code LIKE '%${filter}%' OR
                            m.room_class LIKE '%${filter}%' OR
                            m.firstname LIKE '%${filter}%' OR
                            m.lastname LIKE '%${filter}%' OR
                            m.email LIKE '%${filter}%' OR
                            m.resv_status LIKE '%${filter}%'
                        )` : ''}
                    GROUP BY
                        m.confirmation_no,
                        m.mp,
                        m.market_code,
                        m.market_group,
                        m.room_class,
                        m.firstname,
                        m.lastname,
                        m.email,
                        m.resv_status,
                        m.region,
                        m.point_promotion,
                        STR_TO_DATE(m.insert_date, '%d/%m/%Y')
                    HAVING
                        COUNT(m.mp) = 1 AND
                        COUNT(m.confirmation_no) = 1 AND
                        FLOOR(SUM(
                        CASE
                            WHEN mem.member_level = 'MPS' THEN (m.room_rev_usd * ${mpsMultiplier}) + (m.fb_rev_usd * ${fbsMultiplier})
                            WHEN mem.member_level = 'MPG' THEN (m.room_rev_usd * ${mpgMultiplier}) + (m.fb_rev_usd * ${fbgMultiplier})
                            WHEN mem.member_level = 'MPD' THEN (m.room_rev_usd * ${mpdMultiplier}) + (m.fb_rev_usd * ${fbdMultiplier})
                            ELSE (m.room_rev_usd * ${mpsMultiplier}) + (m.fb_rev_usd * ${fbsMultiplier})
                        END
                    )) > 0
                ) as subquery
            `;

            const totalDataResult = await sequelize.query(totalDataQuery, {
                type: sequelize.QueryTypes.SELECT,
            });

            const totalData = totalDataResult[0].total;

            res.status(200).json({
                data: result,
                totalData: totalData,
            });
        } catch (error) {
            console.log(error);
            res.status(500).json({ error: 'An error occurred while fetching auto points data.' });
        }
    }

    static async getAutoPoint_OLD(req, res) {

        const { page, per_page, filter } = req.query;
        const perPageInt = parseInt(per_page, 10);
        const offset = (page - 1) * per_page;

        try {

            const result = await Master.findAll({
                attributes: [
                    [Sequelize.fn('STR_TO_DATE', Sequelize.col('insert_date'), '%d/%m/%Y'), 'rsv_date'],
                    'confirmation_no',
                    'mp',
                    'market_code',
                    'firstname',
                    'lastname',
                    [Sequelize.fn('SUM', Sequelize.col('room_rev_usd')), 'room_usd'], // Sum of room_rev_usd
                    [Sequelize.fn('SUM', Sequelize.col('fb_rev_usd')), 'fb_usd'], // Sum of fb_rev_usd    
                    [Sequelize.fn('FLOOR', Sequelize.fn('SUM', Sequelize.literal('(room_rev_usd * 5) + (fb_rev_usd * 0.5)'))), 'total_points'],
                    [Sequelize.fn('MIN', Sequelize.col('arrival_date')), 'arrival_date'], // Minimum arrival_date
                    [Sequelize.fn('MAX', Sequelize.col('departure_date')), 'departure_date'], // Maximum departure_date
                    'email',
                    'resv_status'
                ],
                include: [
                    {
                        model: Member,
                        as: "members",
                        attributes: [],
                        required: true, // Use 'required: true' for INNER JOIN or 'required: false' for LEFT JOIN
                        where: {
                            member_id: { [Op.ne]: '' },
                        },
                    },
                ],
                where: filter ? {
                    [Op.or]: [{
                        confirmation_no: {
                            [Op.like]: `%${filter}%`
                        }
                    },
                    {
                        mp: {
                            [Op.like]: `%${filter}%`
                        }
                    },
                    {
                        market_code: {
                            [Op.like]: `%${filter}%`
                        }
                    },
                    {
                        firstname: {
                            [Op.eq]: `${filter}`
                        }
                    },
                    {
                        lastname: {
                            [Op.like]: `%${filter}%`
                        }
                    },
                    {
                        email: {
                            [Op.like]: `%${filter}%`
                        }
                    },
                    {
                        resv_status: {
                            [Op.like]: `%${filter}%`
                        }
                    }
                    ],
                    [Op.and]: [
                        { email: { [Op.not]: '' } },
                        { arrival_date: { [Op.not]: '' } },
                        { departure_date: { [Op.not]: '' } },
                        { stay_date: { [Op.not]: '' } },
                        { market_code: { [Op.in]: ['RAC', 'WEB', 'PACKDOM', 'PACKINT'] } },
                        { resv_status: { [Op.eq]: 'CHECKED OUT' } },
                        { flag: { [Op.eq]: 0 } },
                    ],
                } : {
                    [Op.and]: [
                        { email: { [Op.ne]: '' } },
                        { arrival_date: { [Op.not]: '' } },
                        { departure_date: { [Op.not]: '' } },
                        { stay_date: { [Op.not]: '' } },
                        { market_code: { [Op.in]: ['RAC', 'WEB', 'PACKDOM', 'PACKINT'] } },
                        { resv_status: { [Op.eq]: 'CHECKED OUT' } },
                        { flag: { [Op.eq]: 0 } },
                    ],
                },
                group: [
                    'confirmation_no',
                    'mp',
                    'market_code',
                    'room_class',
                    'firstname',
                    'lastname',
                    'email',
                    'resv_status',
                    Sequelize.fn('STR_TO_DATE', Sequelize.col('insert_date'), '%d/%m/%Y'),
                ],
                order: [
                    [Sequelize.fn('STR_TO_DATE', Sequelize.col('insert_date'), '%d/%m/%Y'), 'DESC'],
                    ['firstname', 'ASC'],
                    ['lastname', 'ASC'],
                ],
                having: {
                    // Menggunakan HAVING untuk menampilkan hanya data dengan count mp = 1 dan count confirmation_no = 1
                    [Op.and]: [
                        Sequelize.literal('COUNT(mp) = 1'),
                        Sequelize.literal('COUNT(confirmation_no) = 1'),
                        Sequelize.literal('total_points > 0'),
                    ],
                },
                //having: Sequelize.where(Sequelize.literal('total_points'), '>', 0),
                limit: perPageInt,
                offset: offset,
            });


            // Query untuk mendapatkan totalData
            const totalDataResult = await Master.findAll({
                attributes: [
                    [Sequelize.fn('STR_TO_DATE', Sequelize.col('insert_date'), '%d/%m/%Y'), 'rsv_date'],
                    'confirmation_no',
                    'mp',
                    'market_code',
                    'firstname',
                    'lastname',
                    [Sequelize.fn('SUM', Sequelize.col('room_rev_usd')), 'room_usd'], // Sum of room_rev_usd
                    [Sequelize.fn('SUM', Sequelize.col('fb_rev_usd')), 'fb_usd'], // Sum of fb_rev_usd    
                    [Sequelize.fn('FLOOR', Sequelize.fn('SUM', Sequelize.literal('(room_rev_usd * 5) + (fb_rev_usd * 0.5)'))), 'total_points'],
                    [Sequelize.fn('MIN', Sequelize.col('arrival_date')), 'arrival_date'], // Minimum arrival_date
                    [Sequelize.fn('MAX', Sequelize.col('departure_date')), 'departure_date'], // Maximum departure_date
                    'email',
                    'resv_status'
                ],
                where: filter ? {
                    [Op.or]: [{
                        confirmation_no: {
                            [Op.like]: `%${filter}%`
                        }
                    },
                    {
                        mp: {
                            [Op.like]: `%${filter}%`
                        }
                    },
                    {
                        market_code: {
                            [Op.like]: `%${filter}%`
                        }
                    },
                    {
                        firstname: {
                            [Op.eq]: `${filter}`
                        }
                    },
                    {
                        lastname: {
                            [Op.like]: `%${filter}%`
                        }
                    },
                    {
                        email: {
                            [Op.like]: `%${filter}%`
                        }
                    },
                    {
                        resv_status: {
                            [Op.like]: `%${filter}%`
                        }
                    }
                    ],
                    [Op.and]: [
                        { email: { [Op.not]: '' } },
                        { arrival_date: { [Op.not]: '' } },
                        { departure_date: { [Op.not]: '' } },
                        { stay_date: { [Op.not]: '' } },
                        { market_code: { [Op.in]: ['RAC', 'WEB', 'PACKDOM', 'PACKINT'] } },
                        { resv_status: { [Op.eq]: 'CHECKED OUT' } },
                        { flag: { [Op.eq]: 0 } },
                    ],
                } : {
                    [Op.and]: [
                        { email: { [Op.ne]: '' } },
                        { arrival_date: { [Op.not]: '' } },
                        { departure_date: { [Op.not]: '' } },
                        { stay_date: { [Op.not]: '' } },
                        { market_code: { [Op.in]: ['RAC', 'WEB', 'PACKDOM', 'PACKINT'] } },
                        { resv_status: { [Op.eq]: 'CHECKED OUT' } },
                        { flag: { [Op.eq]: 0 } },
                    ],
                },
                group: [
                    'confirmation_no',
                    'mp',
                    'market_code',
                    'room_class',
                    'firstname',
                    'lastname',
                    'email',
                    'resv_status',
                    Sequelize.fn('STR_TO_DATE', Sequelize.col('insert_date'), '%d/%m/%Y'),
                ],
                having: {
                    // Menggunakan HAVING untuk menampilkan hanya data dengan count mp = 1 dan count confirmation_no = 1
                    [Op.and]: [
                        Sequelize.literal('COUNT(mp) = 1'),
                        Sequelize.literal('COUNT(confirmation_no) = 1'),
                        Sequelize.literal('total_points > 0'),
                    ],
                },
            });

            const totalData = totalDataResult ? totalDataResult.length : 0;

            res.status(200).json({
                data: result,
                totalData: totalData,
            });

        } catch (error) {
            console.log(error);
        }
    };

    static async getListLastStay(req, res) {

        let { lastDate } = req.query

        try {
            const result = await Master.findAll({
                attributes: [
                    [Sequelize.col('members.member_id'), 'member_id'],
                    [Sequelize.col('members.title'), 'title'],
                    [Sequelize.col('members.first'), 'first'],
                    [Sequelize.col('members.last'), 'last'],
                    [Sequelize.col('members.primary_email'), 'email'],
                    [Sequelize.col('members.country'), 'country'],
                    [Sequelize.col('members.city'), 'city'],
                    [Sequelize.col('Master.stay_date'), 'stay_date'],
                    [Sequelize.col('Master.confirmation_no'), 'confNo'],
                    [Sequelize.col('Master.arrival_date'), 'arrival_date'],
                    [Sequelize.col('Master.departure_date'), 'departure_date'],
                    [Sequelize.fn('SUM', Sequelize.col('Master.room_rev_usd')), 'room_usd'],
                    [Sequelize.fn('SUM', Sequelize.col('Master.fb_rev_usd')), 'fb_usd'],
                ],
                include: [
                    {
                        model: Member,
                        as: "members",
                        attributes: [],
                    },
                ],
                where: {
                    [Op.and]: [
                        { arrival_date: { [Op.ne]: '' } },
                        { departure_date: { [Op.ne]: '' } },
                        { mp: { [Op.ne]: '' } },
                        { '$members.member_id$': { [Op.ne]: '' } },
                        Sequelize.where(
                            Sequelize.fn('STR_TO_DATE', Sequelize.col('departure_date'), '%d/%m/%Y'),
                            lastDate
                        ),
                    ],
                },
                group: [
                    'members.member_id',
                    'members.title',
                    'members.first',
                    'members.last',
                    'members.primary_email',
                    'members.country',
                    'members.city',
                    'Master.stay_date',
                    'Master.confirmation_no',
                    'Master.arrival_date',
                    'Master.departure_date',
                ],
            });
            res.status(200).json(result);
        } catch (error) {
            console.log(error);
        }
    };

    static async getDetailMember(req, res) {
        let { email, confno, resdate, region } = req.query
        let whereClause = {};

        if (resdate != 'undefined') {
            whereClause = {
                [Op.or]: [
                    { email: email },
                    { confirmation_no: confno },
                ],
                [Op.and]: [
                    { mp: '' },
                    { mp_level: '' },
                    //{ resv_status: 'CHECKED IN' },
                    { resv_status: { [Op.in]: ['CHECKED IN', 'CHECKED OUT'] } },
                    { arrival_date: { [Op.not]: '' } },
                    { departure_date: { [Op.not]: '' } },
                    //{ room_rev_usd: { [Op.gt]: 0 } },
                    //{ fb_rev_usd: { [Op.gt]: 0 } },
                    Sequelize.where(Sequelize.fn('STR_TO_DATE', Sequelize.col('insert_date'), '%d/%m/%Y'), resdate),
                    { region: region },
                ]
            };
        } else {
            whereClause = {
                [Op.or]: [
                    { email: email },
                    { confirmation_no: confno },

                ],
                [Op.and]: [
                    { mp: '' },
                    { mp_level: '' },
                    //{ resv_status: 'CHECKED IN' },
                    { resv_status: { [Op.in]: ['CHECKED IN', 'CHECKED OUT'] } },
                    { arrival_date: { [Op.not]: '' } },
                    { departure_date: { [Op.not]: '' } },
                    { region: region },
                    //{ room_rev_usd: { [Op.gt]: 0 } },
                    //{ fb_rev_usd: { [Op.gt]: 0 } },
                ]
            };
        }

        try {
            const member = await Master.findAll({
                attributes: [
                    'confirmation_no',
                    [Sequelize.fn('Max', Sequelize.col('title')), 'title'],
                    //'title',
                    'firstname',
                    'lastname',
                    'birthday',
                    'passport',
                    //'city',
                    [Sequelize.fn('Min', Sequelize.col('city')), 'city'],
                    //'country',
                    [Sequelize.fn('Min', Sequelize.col('country')), 'country'],
                    // 'telephone',
                    [Sequelize.fn('Max', Sequelize.col('telephone')), 'telephone'],
                    'mp',
                    'mp_level',
                    'market_code',
                    'market_group',
                    [Sequelize.fn('MIN', Sequelize.col('room_class')), 'room_class'],
                    [Sequelize.fn('MAX', Sequelize.col('arrival_date')), 'arrival_date'],
                    [Sequelize.fn('MAX', Sequelize.col('departure_date')), 'departure_date'],
                    [
                        Sequelize.fn(
                            'SUM',
                            Sequelize.cast(
                                Sequelize.fn('REPLACE', Sequelize.col('room_rev_usd'), ',', ''),
                                'DECIMAL'
                            )
                        ),
                        'room_usd',
                    ],
                    [
                        Sequelize.fn(
                            'SUM',
                            Sequelize.cast(
                                Sequelize.fn('REPLACE', Sequelize.col('room_rev_idr'), ',', ''),
                                'DECIMAL'
                            )
                        ),
                        'room_idr',
                    ],
                    [
                        Sequelize.fn(
                            'SUM',
                            Sequelize.cast(
                                Sequelize.fn('REPLACE', Sequelize.col('fb_rev_usd'), ',', ''),
                                'DECIMAL'
                            )
                        ),
                        'fb_usd',
                    ],
                    [
                        Sequelize.fn(
                            'SUM',
                            Sequelize.cast(
                                Sequelize.fn('REPLACE', Sequelize.col('fb_rev_idr'), ',', ''),
                                'DECIMAL'
                            )
                        ),
                        'fb_idr',
                    ],
                    'region',
                    'point_promotion',
                    'agent_name',
                ],
                where: whereClause,
                group: [
                    'confirmation_no',
                    //'title',
                    'firstname',
                    'lastname',
                    'birthday',
                    'passport',
                    //'city',
                    //'country',
                    //'telephone',
                    'mp',
                    'mp_level',
                    'market_code',
                    'market_group',
                    'region',
                    'point_promotion',
                    'agent_name',
                ],
                order: [
                    [Sequelize.literal('MAX(departure_date)'), 'ASC'], // Urutkan berdasarkan departure_date (alias)
                    [Sequelize.fn('SUM', Sequelize.col('room_rev_usd')), 'DESC'],
                    [Sequelize.fn('SUM', Sequelize.col('fb_rev_usd')), 'DESC']
                ],
                //limit: 2,
                raw: true
            });
            res.status(200).json(member);
        } catch (error) {
            console.log(error);
        }
    };

    static async getDetailMemberOpera(req, res) {
        let { email, confno, resdate } = req.query
        let whereClause = {};

        if (resdate != 'undefined') {
            whereClause = {
                [Op.or]: [
                    { email: email },
                    { confirmation_no: confno },
                ],
                [Op.and]: [
                    { mp: { [Op.not]: '' } },
                    { mp_level: { [Op.not]: '' } },
                    { resv_status: 'CHECKED OUT' },
                    { arrival_date: { [Op.not]: '' } },
                    { departure_date: { [Op.not]: '' } },
                    //{ room_rev_usd: { [Op.gt]: 0 } },
                    //{ fb_rev_usd: { [Op.gt]: 0 } },
                    Sequelize.where(Sequelize.fn('STR_TO_DATE', Sequelize.col('insert_date'), '%d/%m/%Y'), resdate),
                ]
            };
        } else {
            whereClause = {
                [Op.or]: [
                    { email: email },
                    { confirmation_no: confno },
                ],
                [Op.and]: [
                    { mp: { [Op.not]: '' } },
                    { mp_level: { [Op.not]: '' } },
                    { resv_status: 'CHECKED OUT' },
                    { arrival_date: { [Op.not]: '' } },
                    { departure_date: { [Op.not]: '' } },
                    //{ room_rev_usd: { [Op.gt]: 0 } },
                    //{ fb_rev_usd: { [Op.gt]: 0 } },
                ]
            };
        }

        try {
            const member = await Master.findAll({
                attributes: [
                    'confirmation_no',
                    [Sequelize.fn('Max', Sequelize.col('title')), 'title'],
                    //'title',
                    'firstname',
                    'lastname',
                    'birthday',
                    'passport',
                    //'city',
                    [Sequelize.fn('Min', Sequelize.col('city')), 'city'],
                    //'country',
                    [Sequelize.fn('Min', Sequelize.col('country')), 'country'],
                    // 'telephone',
                    [Sequelize.fn('Max', Sequelize.col('telephone')), 'telephone'],
                    'mp',
                    'mp_level',
                    'market_code',
                    'market_group',
                    [Sequelize.fn('MIN', Sequelize.col('room_class')), 'room_class'],
                    [Sequelize.fn('MAX', Sequelize.col('arrival_date')), 'arrival_date'],
                    [Sequelize.fn('MAX', Sequelize.col('departure_date')), 'departure_date'],
                    [
                        Sequelize.fn(
                            'SUM',
                            Sequelize.cast(
                                Sequelize.fn('REPLACE', Sequelize.col('room_rev_usd'), ',', ''),
                                'DECIMAL'
                            )
                        ),
                        'room_usd',
                    ],
                    [
                        Sequelize.fn(
                            'SUM',
                            Sequelize.cast(
                                Sequelize.fn('REPLACE', Sequelize.col('fb_rev_usd'), ',', ''),
                                'DECIMAL'
                            )
                        ),
                        'fb_usd',
                    ]
                ],
                where: whereClause,
                group: [
                    'confirmation_no',
                    //'title',
                    'firstname',
                    'lastname',
                    'birthday',
                    'passport',
                    //'city',
                    //'country',
                    //'telephone',
                    'mp',
                    'mp_level',
                    'market_code',
                    'market_group',
                ],
                order: [
                    [Sequelize.fn('SUM', Sequelize.col('room_rev_usd')), 'DESC'],
                    [Sequelize.fn('SUM', Sequelize.col('fb_rev_usd')), 'DESC']
                ],
                //limit: 2,
                raw: true
            });
            res.status(200).json(member);
        } catch (error) {
            console.log(error);
        }
    };

    static async updateMemberByConfNo(req, res) {
        const confno = req.params.confno;
        //const email = req.params.email;
        const { mp, mp_level, mpenroll_date, mp_enroll, username } = req.body

        //cek apakah member sdh ada
        const master = await Master.findAll({
            attributes: ['confirmation_no', 'title', 'firstname', 'lastname', 'email', 'telephone', 'city', 'country'],
            where: {
                confirmation_no: confno
            }
        });

        if (!master[0]) return res.status(400).json({ msg: "Confirmation No is not exist" });

        try {

            await Master.update(
                {
                    mp: mp,
                    mp_level: mp_level,
                    mpenroll_date: mpenroll_date,
                    mp_enroll: mp_enroll,
                    flag: 1,
                    updatedAt: Sequelize.fn('NOW'),
                    updatedBy: username,
                },
                {
                    where: {
                        confirmation_no: confno
                    }
                });

            res.json({ msg: "Update Member ID is successfully" });
        } catch (error) {
            console.log(error);
        }
    };

    static async updateWCCMemberByConfNo(req, res) {
        const confno = req.params.confno;
        //const email = req.params.email;
        const { wccenroll_date, wcc_enroll, username } = req.body

        //cek apakah member sdh ada
        const master = await Master.findAll({
            attributes: ['confirmation_no', 'title', 'firstname', 'lastname', 'email', 'telephone', 'city', 'country'],
            where: {
                confirmation_no: confno
            }
        });

        if (!master[0]) return res.status(400).json({ msg: "Confirmation No is not exist" });

        try {

            await Master.update(
                {
                    wccenroll_date: wccenroll_date,
                    wcc_enroll: wcc_enroll,
                    flag: 1,
                    updatedAt: Sequelize.fn('NOW'),
                    updatedBy: username,
                },
                {
                    where: {
                        confirmation_no: confno
                    }
                });

            res.json({ msg: "Update Member ID is successfully" });
        } catch (error) {
            console.log(error);
        }
    };

    static async updateMemberByEmail(req, res) {
        const email = req.params.email;
        const { mp, mp_level, mpenroll_date, mp_enroll, username } = req.body

        //cek apakah member sdh ada
        const master = await Master.findAll({
            attributes: ['confirmation_no', 'title', 'firstname', 'lastname', 'email', 'telephone', 'city', 'country'],
            where: {
                email: email
            }
        });

        if (!master[0]) return res.status(400).json({ msg: "Confirmation No is not exist" });

        try {

            await Master.update(
                {
                    mp: mp,
                    mp_level: mp_level,
                    mpenroll_date: mpenroll_date,
                    mp_enroll: mp_enroll,
                    flag: 1,
                    updatedAt: Sequelize.fn('NOW'),
                    updatedBy: username,
                },
                {
                    where: {
                        email: email
                    }
                });

            res.json({ msg: "Update Member ID is successfully" });
        } catch (error) {
            console.log(error);
        }
    };

    static async releasePointByConfNo(req, res) {
        const confno = req.params.confno;
        const { flag, username } = req.body

        //cek apakah member sdh ada
        const master = await Master.findAll({
            attributes: ['confirmation_no', 'title', 'firstname', 'lastname', 'email', 'telephone', 'city', 'country'],
            where: {
                confirmation_no: confno
            }
        });

        if (!master[0]) return res.status(400).json({ msg: "Confirmation No is not exist" });

        try {

            await Master.update(
                {
                    flag: flag,
                    updatedAt: Sequelize.fn('NOW'),
                    updatedBy: username,
                },
                {
                    where: {
                        confirmation_no: confno
                    }
                });

            res.json({ msg: "Release point is successfully" });
        } catch (error) {
            console.log(error);
        }
    };

    static async getTotalPointsByConfNo(req, res) {
        const confno = req.params.confno;
        const region = req.params.region.toUpperCase();
        const level = req.params.level.toUpperCase();

        const startDate = moment().subtract(3, 'months').startOf('day').format('YYYY-MM-DD');
        const endDate = moment().endOf('day').format('YYYY-MM-DD');
        try {

            // Fetch settings data
            const setting = await Setting.findOne({ where: { id: 1 } }); // Adjust as needed
            if (!setting) {
                throw new Error("Settings not found");
            }

            let multiplier_room;
            let multiplier_fb;

            let room_amount = parseFloat(setting.room_amount);
            let fb_amount = parseFloat(setting.fb_amount);

            if (level === 'MPS') {
                multiplier_room = parseFloat(setting.room_point_silver) / room_amount;
                multiplier_fb = parseFloat(setting.fb_point_silver) / fb_amount;
            } else if (level === 'MPG') {
                multiplier_room = parseFloat(setting.room_point_gold) / room_amount;
                multiplier_fb = parseFloat(setting.fb_point_gold) / fb_amount;
            } else if (level === 'MPD') {
                multiplier_room = parseFloat(setting.room_point_diamond) / room_amount;
                multiplier_fb = parseFloat(setting.fb_point_diamond) / fb_amount;
            } else {
                // default multiplier jika tidak ada level yang sesuai
                multiplier_room = parseFloat(setting.room_point_silver) / room_amount;
                multiplier_fb = parseFloat(setting.fb_point_silver) / fb_amount;
            }

            const points = await Master.findAll({

                attributes: [
                    ['confirmation_no', 'confirmation_no'],
                    [
                        Sequelize.literal(
                            `FLOOR((SUM(CAST(REPLACE(REPLACE(room_rev_usd, ',', ''), ' ', '') AS DECIMAL(10, 2))) * ${multiplier_room}) + (SUM(CAST(REPLACE(REPLACE(fb_rev_usd, ',', ''), ' ', '') AS DECIMAL(10, 2))) * ${multiplier_fb}))`
                        ),
                        'total_points',
                    ],
                ],
                where: {
                    [Op.and]: [
                        { confirmation_no: confno },
                        { arrival_date: { [Op.not]: '' } },
                        { departure_date: { [Op.not]: '' } },
                        (region === 'BALI')
                            ? { market_code: { [Op.in]: ['RAC', 'WEB', 'PACKDOM', 'PACKINT'] } }
                            : { market_group: { [Op.in]: ['COT', 'PKG', 'RCK', 'VCO', 'LST'] } }
                    ],
                },
                group: [
                    'confirmation_no'
                ]
            });

            res.status(200).json(points);
        } catch (error) {
            console.log(error);
        }
    };

    static async calculateTotalPoints(req, res) {
        const confno = req.params.confno;
        const memberId = req.params.memberId;
        const startDate = moment().subtract(3, 'months').startOf('day').format('YYYY-MM-DD');
        const endDate = moment().endOf('day').format('YYYY-MM-DD');

        //cek apakah member sdh ada and get member level
        const members = await Member.findAll({
            attributes: ['name_on_card', 'first', 'last', 'member_level'],
            where: {
                member_id: memberId
            }
        });

        if (!members[0]) return res.status(400).json({ msg: "Members No is not exist" });

        const level = members[0].member_level.toUpperCase();

        try {

            // Fetch settings data
            const setting = await Setting.findOne({ where: { id: 1 } }); // Adjust as needed
            if (!setting) {
                throw new Error("Settings not found");
            }

            let multiplier_room;
            let multiplier_fb;

            let room_amount = parseFloat(setting.room_amount);
            let fb_amount = parseFloat(setting.fb_amount);

            if (level === 'MPS') {
                multiplier_room = parseFloat(setting.room_point_silver) / room_amount;
                multiplier_fb = parseFloat(setting.fb_point_silver) / fb_amount;
            } else if (level === 'MPG') {
                multiplier_room = parseFloat(setting.room_point_gold) / room_amount;
                multiplier_fb = parseFloat(setting.fb_point_gold) / fb_amount;
            } else if (level === 'MPD') {
                multiplier_room = parseFloat(setting.room_point_diamond) / room_amount;
                multiplier_fb = parseFloat(setting.fb_point_diamond) / fb_amount;
            } else {
                multiplier_room = parseFloat(setting.room_point_silver) / room_amount; // default multiplier jika tidak ada level yang sesuai
                multiplier_fb = parseFloat(setting.fb_point_silver) / fb_amount;
            }

            const points = await Master.findAll({

                attributes: [['confirmation_no', 'confirmation_no'],
                [
                    Sequelize.literal(
                        `FLOOR((SUM(CAST(REPLACE(REPLACE(room_rev_usd, ',', ''), ' ', '') AS DECIMAL(10, 2))) * ${multiplier_room}) + (SUM(CAST(REPLACE(REPLACE(fb_rev_usd, ',', ''), ' ', '') AS DECIMAL(10, 2))) * ${multiplier_fb}))`
                    ),
                    'total_points',
                ],
                ],
                where: {
                    [Op.and]: [
                        { mp: memberId },
                        { confirmation_no: confno },
                        //{ birthday: { [Op.not]: null } },
                        { arrival_date: { [Op.not]: '' } },
                        { departure_date: { [Op.not]: '' } },
                        { market_code: { [Op.in]: ['RAC', 'WEB', 'PACKDOM', 'PACKINT'] } },
                    ],
                },
                group: [
                    'confirmation_no'
                ]
            });
            res.status(200).json(points);
        } catch (error) {
            console.log(error);
        }
    };

    static async initPoints(req, res) {
        const { member_id, arrival_date, departure_date, property } = req.body
        try {
            await InitialPoint.create({
                member_id: member_id,
                checkin_date: arrival_date,
                checkout_date: departure_date,
                property: property,
                room_usd: 0,
                fb_usd: 0,
                init_point: process.env.NEW_MEMBER_POINT,
                createdAt: Sequelize.fn('NOW'),
                updatedAt: Sequelize.fn('NOW'),
            });
            res.json({ msg: "Add new init points successfully" });
        } catch (error) {
            console.log(error);
        }
    };

    static async getPointLogsByConfNo(req, res) {

        const confNo = req.params.confNo;
        try {
            const pointlog = await Master.findOne({
                attributes: ['confirmation_no', 'title', 'firstname', 'lastname', 'email', 'telephone', 'city', 'country'],
                where: {
                    [Op.and]: [
                        { confirmation_no: confNo },
                        { market_code: { [Op.in]: ['RAC', 'WEB', 'PACKDOM', 'PACKINT'] } },
                    ],
                },
            });
            res.status(200).json(pointlog);
        } catch (error) {
            console.log(error);
        }
    };

    static async xxxgetDepartureDate(req, res) {
        const deptDate = req.params.deptDate;
        let region = req.params.region;
        console.log('get---', deptDate);
        // Convert the Unix timestamp back to a formatted date string
        // Convert Unix timestamp to a Date object
        let dateFromTimestamp = new Date(Number(deptDate));
        let convertedDate = `${dateFromTimestamp.getDate().toString().padStart(2, '0')}/${(dateFromTimestamp.getMonth() + 1).toString().padStart(2, '0')}/${dateFromTimestamp.getFullYear()}`;
        console.log(" Date Back:", dateFromTimestamp);
        console.log("Converted Date:", convertedDate);
        console.log("Region:", region);

        try {
            const setting = await Master.findOne({
                attributes: ['bookkeeping', 'departure_date', 'region'],
                where: {
                    region: region,
                    departure_date: convertedDate

                }
            });
            res.status(200).json(setting);
        } catch (error) {
            console.log(error);
        }
    };

    static async getDepartureDate(req, res) {
        const deptDate = req.params.deptDate;
        let uppercaseRegion = req.params.region.toUpperCase();

        try {
            const whereCondition = {
                [Op.and]: [
                    { region: uppercaseRegion },
                    Sequelize.literal(`STR_TO_DATE(departure_date, '%d/%m/%Y') = '${deptDate}'`)
                ]
            };
            const setting = await Master.findOne({
                attributes: ['bookkeeping', 'departure_date', 'region'],
                where: whereCondition
            });

            res.status(200).json(setting);
        } catch (error) {
            console.log(error);
        }
    };

}

module.exports = MasterController