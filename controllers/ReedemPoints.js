const Sequelize = require('sequelize');
const sequelize = require('../config/database');
const { Op } = require('sequelize');
const { QueryTypes } = require('sequelize');
const moment = require('moment');

const { ReedemsPointLog, ReedemsPoint, RedemsPointItem, Member } = require('../db/models/index');

class ReedemPointController {

    static async getRedeemPoints(req, res, next) {
        const { region, page, per_page, filter } = req.query;
        const perPageInt = parseInt(per_page, 10);
        const offset = (page - 1) * per_page;

        try {

            // Lakukan query ke database menggunakan Sequelize
            const redeem = await ReedemsPoint.findAndCountAll({
                attributes: [
                    'id', 'member_id', 'reedem_id','previous_balance', 'required_points', 'qty_points', 'total_reedem_points', 
                    'remaining_points', 'beneficiary', 'comments', 'is_cancel', 'is_loyalty', 'is_revenue', 'is_financial', 
                    'is_resident', 'approve_status_id', 'is_manual', 'createdAt', 'createdBy', 'updatedAt', 'updatedBy'
                ],
                offset: offset,
                limit: perPageInt,
                where: {
                    approve_status_id: {
                        [Op.lt]: 4
                    },
                    is_cancel: 0,
                    ...(filter && {
                        [Op.or]: [
                            { member_id: { [Op.like]: `%${filter}%` } },
                            { beneficiary: { [Op.like]: `%${filter}%` } },
                            { comments: { [Op.like]: `%${filter}%` } }
                        ]
                    }),
                    location: region
                },
                include: [
                    {
                        model: Member,
                        attributes: ["name_on_card"],
                        // required: true,
                        as: 'members' // Sesuaikan dengan nama asosiasi antara Dining dan Outlet
                    },
                ],
                order: [['createdAt', 'DESC']],
            });

            // Ekstrak data dan jumlah total dari hasil query
            const data = redeem.rows;
            const totalCount = redeem.count;

            res.status(200).json({
                data: data,
                totalData: totalCount,
            });
        } catch (error) {
            console.log(error);
        }

    }

    static async getRedemPendingApprove(req, res, next) {
        const { region, page, per_page, filter } = req.query;
        const perPageInt = parseInt(per_page, 10);
        const offset = (page - 1) * per_page;

        try {

            // Lakukan query ke database menggunakan Sequelize
            const redeem = await ReedemsPoint.findAndCountAll({
                attributes: [
                    'id', 'member_id', 'reedem_id','previous_balance', 'required_points', 'qty_points', 'total_reedem_points', 
                    'remaining_points', 'beneficiary', 'comments', 'is_cancel', 'is_loyalty', 'is_revenue', 'is_financial', 
                    'is_resident', 'approve_status_id', 'is_manual', 'createdAt', 'createdBy', 'updatedAt', 'updatedBy'
                ],
                offset: offset,
                limit: perPageInt,
                where: {
                    approve_status_id: {
                        [Op.lt]: 4
                    },
                    ...(filter && {
                        [Op.or]: [
                            { member_id: { [Op.like]: `%${filter}%` } },
                            { beneficiary: { [Op.like]: `%${filter}%` } },
                            { comments: { [Op.like]: `%${filter}%` } }
                        ]
                    }),
                    location: region,
                    is_manual: 0
                },
                include: [
                    {
                        model: Member,
                        attributes: ["name_on_card"],
                        // required: true,
                        as: 'members' // Sesuaikan dengan nama asosiasi antara Dining dan Outlet
                    },
                ],
                order: [['createdAt', 'DESC']],
            });

            // Ekstrak data dan jumlah total dari hasil query
            const data = redeem.rows;
            const totalCount = redeem.count;

            res.status(200).json({
                data: data,
                totalData: totalCount,
            });
        } catch (error) {
            console.log(error);
        }

    }

    static async getReedemPointDetailById(req, res) {
        const reedemId = req.params.id;

        try {

            const reservs = await ReedemsPoint.findOne({

                attributes: ['id', 'member_id', 'reedem_id', 'previous_balance', 
                    'required_points', 'qty_points', 'total_reedem_points', 'remaining_points', 
                    'surcharge_type_id', 'surcharge_points',
                    'beneficiary', 'comments', 'is_cancel', 'is_loyalty', 'is_revenue', 
                    'is_financial', 'is_resident', 'location',  'is_manual', 'approve_status_id', 
                    'is_addsurcharge_loyalty', 'is_addsurcharge_revenue', 'is_addsurcharge_financial',
                    'is_addsurcharge_resident', 'addsurcharge_status_id',
                    'createdAt', 'createdBy', 'updatedAt', 'updatedBy',
                    [Sequelize.literal(`CASE 
                        WHEN surcharge_type_id = 1 THEN 'High session surcharge' 
                        WHEN surcharge_type_id = 2 THEN 'Peak session surcharge' 
                        ELSE '' END`), 'surcharge_type_name']
                ],
                where: {
                    id: reedemId
                }
            });

            res.status(200).json(reservs);
        } catch (error) {
            console.log(error);
        }
    };

    static async getReedemPointItemsById(req, res) {
        const redemPointId = req.params.id;
        console.log(redemPointId, "redemPointId");
        try {   

            const redemitems = await RedemsPointItem.findAll({
                where: {
                    redem_point_id: redemPointId
                }
            });
            console.log(redemitems, "redemitems");
            res.status(200).json(redemitems);
        } catch (error) {
            console.log(error);
        }
    };

    static async createReedemPointLogs(req, res) {
        const { member_id, reedem_id, total_reedem_points, comments, surcharge_type_id, surcharge_points, createdAt, createdBy, updatedAt, updatedBy } = req.body
        try {
            await ReedemsPointLog.create({
                member_id: member_id,
                reedem_id: reedem_id,
                total_reedem_points: total_reedem_points,
                comments: comments,
                surcharge_type_id: surcharge_type_id,
                surcharge_points: surcharge_points,
                createdAt: createdAt,
                createdBy: createdBy,
                updatedAt: updatedAt,
                updatedBy: updatedBy,
            });
            res.json({ msg: "Add reedem points logs is successfully" });
        } catch (error) {
            console.log(error);
        }
    };

    static async updateReedemPoint(req, res) {
        const reedemId = req.params.reedemId;
        const { total_reedem_points, surcharge_type_id, surcharge_points, username } = req.body

        //cek apakah member sdh ada
        const reedem = await ReedemsPoint.findAll({
            attributes: ['id', 'member_id', 'reedem_id', 'previous_balance', 'total_reedem_points', 'comments', 'createdAt', 'createdBy', 'updatedAt', 'updatedBy'],
            where: {
                id: reedemId
            }
        });


        if (!reedem[0]) return res.status(400).json({ msg: "Reedem points is not exist" });

        let prevBalance = Number(reedem[0].previous_balance);
        let remainingPoints = prevBalance - Number(total_reedem_points+surcharge_points)

        try {

            await ReedemsPoint.update(
                {
                    total_reedem_points: total_reedem_points,
                    surcharge_type_id: surcharge_type_id,
                    surcharge_points: surcharge_points,
                    remaining_points: remainingPoints,
                    updatedAt: Sequelize.fn('NOW'),
                    updatedBy: username,
                },
                {
                    where: {
                        id: reedemId
                    }
                });

            res.json({ msg: "Update reedem points logs is successfully" });
        } catch (error) {
            console.log(error);
        }

    };

    static async updateIsLoyalty(req, res) {
        const pointId = req.params.id;
        const { username } = req.body

        //cek apakah member sdh ada
        const reedemPoint = await ReedemsPoint.findAll({
            attributes: ['id', 'member_id', 'reedem_id', 'total_reedem_points', 'comments', 'is_loyalty', 'approve_status_id', 'createdAt', 'createdBy', 'updatedAt', 'updatedBy'],
            where: {
                id: pointId
            }
        });


        if (!reedemPoint[0]) return res.status(400).json({ msg: "Reedem points is not exist" });

        try {

            await ReedemsPoint.update(
                {
                    is_loyalty: 1,
                    approve_status_id: 1, // approve status loyalty
                    updatedAt: Sequelize.fn('NOW'),
                    updatedBy: username,
                },
                {
                    where: {
                        id: pointId
                    }
                });

            res.json({ msg: "Set is loyalty is successfully" });
        } catch (error) {
            console.log(error);
        }

    };

    static async updateIsSurchargeLoyalty(req, res) {
        const pointId = req.params.id;
        const { username } = req.body

        //cek apakah member sdh ada
        const reedemPoint = await ReedemsPoint.findAll({
            attributes: ['id', 'member_id', 'reedem_id', 'total_reedem_points', 'comments', 'is_addsurcharge_loyalty', 'addsurcharge_status_id', 'createdAt', 'createdBy', 'updatedAt', 'updatedBy'],
            where: {
                id: pointId
            }
        });


        if (!reedemPoint[0]) return res.status(400).json({ msg: "Reedem points is not exist" });

        try {

            await ReedemsPoint.update(
                {
                    is_addsurcharge_loyalty: 1,
                    addsurcharge_status_id: 1, // approve status surcharge loyalty
                    updatedAt: Sequelize.fn('NOW'),
                    updatedBy: username,
                },
                {
                    where: {
                        id: pointId
                    }
                });

            res.json({ msg: "Set is loyalty is successfully" });
        } catch (error) {
            console.log(error);
        }

    };

    static async updateIsRevenue(req, res) {
        const pointId = req.params.id;
        const { username } = req.body

        console.log("Masuk ke update is revenue")

        //cek apakah member sdh ada
        const reedemPoint = await ReedemsPoint.findAll({
            attributes: ['id', 'member_id', 'reedem_id', 'total_reedem_points', 'comments', 'is_revenue', 'approve_status_id', 'createdAt', 'createdBy', 'updatedAt', 'updatedBy'],
            where: {
                id: pointId
            }
        });

        if (!reedemPoint[0]) return res.status(400).json({ msg: "Reedem points is not exist" });

        try {

            await ReedemsPoint.update(
                {
                    is_revenue: 1,
                    approve_status_id: 2, // approve status revenue
                    updatedAt: Sequelize.fn('NOW'),
                    updatedBy: username,
                },
                {
                    where: {
                        id: pointId
                    }
                });

            res.json({ msg: "Set is revenue is successfully" });
        } catch (error) {
            console.log(error);
        }

    };

    static async updateIsSurchargeRevenue(req, res) {
        const pointId = req.params.id;
        const { username } = req.body

        console.log("Masuk ke update is revenue")

        //cek apakah member sdh ada
        const reedemPoint = await ReedemsPoint.findAll({
            attributes: ['id', 'member_id', 'reedem_id', 'total_reedem_points', 'comments', 'is_addsurcharge_revenue', 'addsurcharge_status_id', 'createdAt', 'createdBy', 'updatedAt', 'updatedBy'],
            where: {
                id: pointId
            }
        });

        if (!reedemPoint[0]) return res.status(400).json({ msg: "Reedem points is not exist" });

        try {

            await ReedemsPoint.update(
                {
                    is_addsurcharge_revenue: 1,
                    addsurcharge_status_id: 2, // approve status revenue
                    updatedAt: Sequelize.fn('NOW'),
                    updatedBy: username,
                },
                {
                    where: {
                        id: pointId
                    }
                });

            res.json({ msg: "Set is revenue is successfully" });
        } catch (error) {
            console.log(error);
        }

    };

    static async updateIsFinancial(req, res) {
        const pointId = req.params.id;
        const { username } = req.body

        //cek apakah member sdh ada
        const reedemPoint = await ReedemsPoint.findAll({
            attributes: ['id', 'member_id', 'reedem_id', 'total_reedem_points', 'comments', 'is_financial', 'approve_status_id', 'createdAt', 'createdBy', 'updatedAt', 'updatedBy'],
            where: {
                id: pointId
            }
        });


        if (!reedemPoint[0]) return res.status(400).json({ msg: "Reedem points is not exist" });

        try {

            await ReedemsPoint.update(
                {
                    is_financial: 1,
                    approve_status_id: 3, // approve status financial
                    updatedAt: Sequelize.fn('NOW'),
                    updatedBy: username,
                },
                {
                    where: {
                        id: pointId
                    }
                });

            res.json({ msg: "Set is financial is successfully" });
        } catch (error) {
            console.log(error);
        }

    };

    static async updateIsSurchargeFinancial(req, res) {
        const pointId = req.params.id;
        const { username } = req.body

        //cek apakah member sdh ada
        const reedemPoint = await ReedemsPoint.findAll({
            attributes: ['id', 'member_id', 'reedem_id', 'total_reedem_points', 'comments', 'is_addsurcharge_financial', 'addsurcharge_status_id', 'createdAt', 'createdBy', 'updatedAt', 'updatedBy'],
            where: {
                id: pointId
            }
        });


        if (!reedemPoint[0]) return res.status(400).json({ msg: "Reedem points is not exist" });

        try {

            await ReedemsPoint.update(
                {
                    is_addsurcharge_financial: 1,
                    addsurcharge_status_id: 3, // approve status financial
                    updatedAt: Sequelize.fn('NOW'),
                    updatedBy: username,
                },
                {
                    where: {
                        id: pointId
                    }
                });

            res.json({ msg: "Set is financial is successfully" });
        } catch (error) {
            console.log(error);
        }

    };

    static async updateIsResident(req, res) {
        const pointId = req.params.id;
        const { username } = req.body

        //cek apakah member sdh ada
        const reedemPoint = await ReedemsPoint.findAll({
            attributes: ['id', 'member_id', 'reedem_id', 'total_reedem_points', 'comments', 'is_resident', 'approve_status_id', 'createdAt', 'createdBy', 'updatedAt', 'updatedBy'],
            where: {
                id: pointId
            }
        });


        if (!reedemPoint[0]) return res.status(400).json({ msg: "Reedem points is not exist" });

        try {

            await ReedemsPoint.update(
                {
                    is_resident: 1,
                    approve_status_id: 4, // approve status resident
                    updatedAt: Sequelize.fn('NOW'),
                    updatedBy: username,
                },
                {
                    where: {
                        id: pointId
                    }
                });

            res.json({ msg: "Set is resident is successfully" });
        } catch (error) {
            console.log(error);
        }

    };

    static async updateIsSurchargeResident(req, res) {
        const pointId = req.params.id;
        const { username } = req.body

        //cek apakah member sdh ada
        const reedemPoint = await ReedemsPoint.findAll({
            attributes: ['id', 'member_id', 'reedem_id', 'total_reedem_points', 'comments', 'is_addsurcharge_resident', 'addsurcharge_status_id', 'createdAt', 'createdBy', 'updatedAt', 'updatedBy'],
            where: {
                id: pointId
            }
        });


        if (!reedemPoint[0]) return res.status(400).json({ msg: "Reedem points is not exist" });

        try {

            await ReedemsPoint.update(
                {
                    is_addsurcharge_resident: 1,
                    addsurcharge_status_id: 4, // approve status resident
                    updatedAt: Sequelize.fn('NOW'),
                    updatedBy: username,
                },
                {
                    where: {
                        id: pointId
                    }
                });

            res.json({ msg: "Set is resident is successfully" });
        } catch (error) {
            console.log(error);
        }

    };

    static async updateIsSurchargeResident(req, res) {
        const redeemPointId = req.params.id;
        const { beneficiary, username } = req.body

        //cek apakah member sdh ada
        const reedemPoint = await RedemsPointItem.findAll({
             where: {
                redem_point_id: redeemPointId
            }
        });


        if (!reedemPoint[0]) return res.status(400).json({ msg: "Reedem points items is not exist" });

        try {

            await RedemsPointItem.update(
                {
                    beneficiary: beneficiary,
                    updatedAt: Sequelize.fn('NOW'),
                    updatedBy: username,
                },
                {
                    where: {
                        redem_point_id: redeemPointId
                    }
                });

            res.json({ msg: "Update benefiaciary is successfully" });
        } catch (error) {
            console.log(error);
        }

    };

    static async deleteRedeemPoint(req, res) {
        const Id = req.params.id;

        const data = await ReedemsPoint.findAll({
            where: {
                id: Id
            }
        });

        if (!data[0]) return res.status(400).json({ status: 400, msg: "data is not exist" });

        try {

            await ReedemsPoint.destroy(
                {
                    where: {
                        id: Id
                    }
                });

            res.json({ status: 200, msg: "Delete Redeem Point is successfully" });
        } catch (error) {
            console.log(error);
        }
    };

    static async getTotalReedemPoints(req, res) {
        const memberId = req.params.memberId;
        try {
            const reservs = await ReedemsPoint.findAll({

                attributes: [
                    'member_id',
                    [Sequelize.fn('SUM', Sequelize.col('total_reedem_points')), 'total_points'],
                    [Sequelize.fn('SUM', Sequelize.col('surcharge_points')), 'surcharge_points']
                ],
                where: {
                    member_id: memberId,
                },
                group: [
                    'member_id'
                ],
            });
            res.status(200).json(reservs);
        } catch (error) {
            console.log(error);
        }
    };

    static async getLatestReedemPoints(req, res) {
        try {
            const latestPoint = await ReedemsPoint.findOne({
                attributes: [
                    'id',
                    'member_id',
                    'reedem_id',
                    'total_reedem_points',
                    'comments',
                    'is_cancel',
                    [Sequelize.fn('DATE_FORMAT', Sequelize.col('ReedemsPoint.createdAt'), '%m/%d/%Y'), 'createdAt'],
                    [Sequelize.fn('DATE_FORMAT', Sequelize.col('ReedemsPoint.updatedAt'), '%m/%d/%Y'), 'updatedAt']
                ],
                order: [
                    ['id', 'DESC']
                ],
            });

            res.status(200).json(latestPoint);
        } catch (error) {
            console.log(error);
            res.status(500).json({ error: 'Internal Server Error' });
        }
    };

};

module.exports = ReedemPointController