const { PointBuyPromo, Member } = require('../db/models/index');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const Sequelize = require('sequelize');
const { Op } = require('sequelize');

function convertUTCToLocal(utcDate, offset) {
    if (!(utcDate instanceof Date)) {
        throw new TypeError('The first argument must be a Date object.');
    }
    if (typeof offset !== 'number') {
        throw new TypeError('The second argument must be a number.');
    }

    // Convert the UTC time to the local time
    return new Date(utcDate.getTime() + offset * 60 * 60 * 1000);
}


class PointController {

    static async getPoints(req, res, next) {
        const { page, per_page, filter } = req.query;
        const perPageInt = parseInt(per_page, 10);
        const offset = (page - 1) * per_page;

        try {

            // Membuat objek where condition
            const whereCondition = {};

            if (filter) {
                whereCondition[Op.or] = [
                    {
                        member_id: {
                            [Op.like]: `%${filter}%`
                        }
                    },
                    {
                        member_name: {
                            [Op.like]: `%${filter}%`
                        }
                    },
                ];
            }

            const points = await PointBuyPromo.findAndCountAll({
                attributes: ['id', 'member_id', 'member_name', 'purchase_date', 'property', 'check_number', 'total_point','createdBy', 'createdAt'],
                offset: offset,
                limit: perPageInt,
                where: whereCondition,
                order: [['createdAt', 'DESC']],
            });

            
            const totalCount = points.count;

            res.status(200).json({
                data: points,
                totalData: totalCount,
            });
        } catch (error) {
            console.log(error);
        }

    }

    static async getTotalPointsByMemberId(req, res) {
        const memberId = req.params.memberId;

        try {

            const points = await Member.findAll({
                attributes: ['member_id', 'member_name', 'createdAt',
                    [Sequelize.fn('SUM', Sequelize.col('room_usd')), 'room_usd'],
                    [Sequelize.fn('SUM', Sequelize.col('fb_usd')), 'fb_usd'],
                    [Sequelize.fn('SUM', Sequelize.col('total_point')), 'total_point'],
                ],
                where: {
                    member_id: memberId,
                },
                group: ['member_id', 'member_name', 'room_usd', 'fb_usd', 'total_point', 'createdAt'],
                order: [
                    ['createdAt', 'ASC']
                ],
            });

            res.status(200).json(points);
        } catch (error) {
            console.log(error);
        }
    };

    static async getPurchasePointsByMemberId(req, res) {
        const memberId = req.params.memberId;

        try {

            const purchase = await PointBuyPromo.findAll({

                attributes: ['member_id', 'purchase_date', 'property', 'check_number',
                    [Sequelize.fn('SUM', Sequelize.col('total_point')), 'total_point'],
                ],
                where: {
                    member_id: memberId
                },
                group: ['member_id', 'purchase_date', 'property', 'check_number'],
                order: [
                    ['purchase_date', 'DESC'],
                ],

            });

            res.status(200).json(purchase);
        } catch (error) {
            console.log(error);
        }
    };


    static async createPoint(req, res) {
        const { member_id, member_name, purchase_date, property, check_number, total_point, username } = req.body;
        const total_points = Number(total_point);

        // using UTC time
        const nowUTC = new Date(); // Current UTC time
        const offset = 8; // Bali (WITA) is UTC+8
        const localDateTime = convertUTCToLocal(nowUTC, offset);

        try {
            await PointBuyPromo.create({
                member_id: member_id,
                member_name: member_name,
                purchase_date: purchase_date,
                property: property,
                check_number: check_number,
                total_point: total_points,
                createdAt: localDateTime,
                createdBy: username,
                updatedAt: localDateTime,
                updatedBy: username,
            });
            res.json({ status: 200, msg: "Add new Buy Point successfully" });
        } catch (error) {
            console.log(error);
        }
    };

    static async detailPoint(req, res) {

        const pointId = req.params.id;
        try {
            const point = await PointBuyPromo.findOne({
                where: {
                    id: pointId
                }
            });
            res.status(200).json(point);
        } catch (error) {
            console.log(error);
        }
    };

    static async updatePoint(req, res) {
        const pointId = req.params.id;
        const { member_id, member_name, purchase_date, property, check_number, total_point, username } = req.body;
        const total_points = Number(total_point);

        // using UTC time
        const nowUTC = new Date(); // Current UTC time
        const offset = 8; // Bali (WITA) is UTC+8
        const localDateTime = convertUTCToLocal(nowUTC, offset);

        //cek apakah Role sdh ada
        const point = await PointBuyPromo.findAll({
            where: {
                id: pointId
            }
        });

        if (!point[0]) return res.status(400).json({ status: 400, msg: "Point is not exist" });

        try {

            await PointBuyPromo.update(
                {
                    member_id: member_id,
                    member_name: member_name,
                    purchase_date: purchase_date,
                    property: property,
                    check_number: check_number,
                    total_point: total_points,
                    updatedAt: localDateTime,
                    updatedBy: username
                },
                {
                    where: {
                        id: pointId
                    }
                });

            res.json({ status: 200, msg: "Update Buy Point is successfully" });
        } catch (error) {
            console.log(error);
        }
    };

    static async updateMember(req, res) {
        const memberId = req.params.memberId;
        const { exp_date, total_point, username } = req.body;
        const totalPoints = Number(total_point);

        // using UTC time
        const nowUTC = new Date(); // Current UTC time
        const offset = 8; // Bali (WITA) is UTC+8
        const localDateTime = convertUTCToLocal(nowUTC, offset);

        let availablePoints = 0;
        let totPoints = 0;
        let reedemPoints = 0;

        //cek apakah member sdh ada
        const member = await Member.findAll({
            attributes: ['id', 'first', 'last', 'primary_email', 'primary_mobilephone', 'city', 'country', 'reedem_points', 'total_points', 'available_points'],
            where: {
                member_id: memberId
            }
        });


        if (!member[0]) return res.status(400).json({ msg: "Member is not exist" });

        totPoints = Number(member[0].total_points) + totalPoints
        reedemPoints = Number(member[0].reedem_points)
        availablePoints = totPoints - reedemPoints

        try {

            await Member.update(
                {
                    total_points: totPoints,
                    reedem_points: reedemPoints,
                    available_points: availablePoints,
                    exp_date: exp_date,
                    updatedAt: localDateTime,
                    updatedBy: username
                },
                {
                    where: {
                        member_id: memberId
                    }
                });

            res.json({ status: 200, msg: "Update Member Point is successfully" });
        } catch (error) {
            console.log(error);
        }
    };


    static async deletePoint(req, res) {
        const pointId = req.params.id;
        //cek apakah Role sdh ada
        const point = await PointBuyPromo.findAll({
            where: {
                id: pointId
            }
        });

        if (!point[0]) return res.status(400).json({ status: 400, msg: "Buy Point is not exist" });

        try {

            await PointBuyPromo.destroy(
                {
                    where: {
                        id: pointId
                    }
                });

            res.json({ status: 200, msg: "Delete Buy point is successfully" });
        } catch (error) {
            console.log(error);
        }
    };
}


module.exports = PointController







