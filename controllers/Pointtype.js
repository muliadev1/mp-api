const { PointType } = require('../db/models/index');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const Sequelize = require('sequelize');


class PointtypeController {
    static async getPointtypesUsedPages(req, res) {
        let { page, pageSize } = req.query

        //! Calculate starting index and number of records to retrieve
        const startIndex = (Number(page) - 1) * Number(pageSize);
        const endIndex = Number(page) * Number(pageSize);
        try {
            const types = await PointType.findAll({
                offset: startIndex,
                limit: Number(pageSize)
            });
              //! Retrieve total count of records for pagination info
              const totalCount = await PointType.count();
              //! Calculate total number of pages
              const totalPages = Math.ceil(totalCount / Number(pageSize));
            res.status(200).json({
                data: types,
                totalRecords: totalCount,
                currentPage: Number(page),
                totalPages: totalPages });
        } catch (error) {
            console.log(error);
        }
    };

    static async getPointtypes(req, res) {
        try {
            const types = await PointType.findAll({
            });
            res.status(200).json(types);
        } catch (error) {
            console.log(error);
        }
    };

    static async createPointtype(req, res) {
        const { point_type, username } = req.body;
        //cek apakah language sdh ada
        const types = await PointType.findAll({
            where: {
                point_type: point_type
            }
        });

        if (types[0]) return res.status(400).json({ msg: "Point type is alredy exist" });

        try {
            await PointType.create({
                point_type: point_type,
                createdAt: Sequelize.fn('NOW'),
                createdBy: username,
                updatedAt: Sequelize.fn('NOW'), 
                updatedBy: username,
            });
            res.json({ msg: "Add new Point type successfully" });
        } catch (error) {
            console.log(error);
        }
    };

    static async detailPointtype(req, res) {

        const typeId = req.params.id;
        try {
            const type = await PointType.findOne({
                where: {
                    id: typeId
                }
            });
            res.status(200).json(type);
        } catch (error) {
            console.log(error);
        }
    };

    static async updatePointtype (req, res) {
        const typeId = req.params.id;
        const { point_type, username } = req.body;
        //cek apakah language sdh ada
        const types = await PointType.findAll({
            attributes: ['id', 'point_type'],
            where: {
                id: typeId
            }
        });
    
        if (!types[0]) return res.status(400).json({ msg: "Point type is not exist" });
    
        try {
    
            await PointType.update(
                {
                    point_type: point_type,
                    updatedBy: username,
                    updatedAt: Sequelize.fn('NOW')
                },
                {
                    where: {
                        id: typeId
                    }
                });
    
            res.json({ msg: "Update Point type is successfully" });
        } catch (error) {
            console.log(error);
        }
    };

    static async deletePointtype(req, res) {
        const typeId = req.params.id;
        //cek apakah language sdh ada
        const types = await PointType.findAll({
            where: {
                id: typeId
            }
        });
    
        if (!types[0]) return res.status(400).json({ msg: "Point type is not exist" });
    
        try {
    
            await PointType.destroy(
                {
                    where: {
                        id: typeId
                    }
                });
    
            res.json({ msg: "Delete Point type is successfully" });
        } catch (error) {
            console.log(error);
        }
    };
}


module.exports = PointtypeController







