const { Role } = require('../db/models/index');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const Sequelize = require('sequelize');


class RoleController {
    static async getRoles(req, res) {
        try {
            const roles = await Role.findAll({
            });
            res.status(200).json(roles);
        } catch (error) {
            console.log(error);
        }
    };


    static async createRole(req, res) {
        const { role_name, role_description, is_active, username } = req.body;

        try {
            await Role.create({
                role_name: role_name,
                role_description: role_description,
                is_active: is_active,
                createdAt: Sequelize.fn('NOW'),
                createdBy: username,
                updatedAt: Sequelize.fn('NOW'),
                updatedBy: username,
            });
            res.json({ status: 200, msg: "Add new role successfully" });
        } catch (error) {
            console.log(error);
        }
    };

    static async detailRole(req, res) {

        const roleId = req.params.id;
        try {
            const role = await Role.findOne({
                where: {
                    id: roleId
                }
            });
            res.status(200).json(role);
        } catch (error) {
            console.log(error);
        }
    };

    static async updateRole(req, res) {
        const roleId = req.params.id;
        const { role_name, role_description, is_active, is_delete, username } = req.body;
        //cek apakah Role sdh ada
        const role = await Role.findAll({
            where: {
                id: roleId
            }
        });

        if (!role[0]) return res.status(400).json({ status: 400, msg: "Role is not exist" });

        try {

            await Role.update(
                {
                    role_name: role_name,
                    role_description: role_description,
                    is_active: is_active,
                    is_delete: is_delete,
                    updatedAt: Sequelize.fn('NOW'),
                    updatedBy: username
                },
                {
                    where: {
                        id: roleId
                    }
                });

            res.json({ status: 200, msg: "Update Role is successfully" });
        } catch (error) {
            console.log(error);
        }
    };

    static async deleteRole(req, res) {
        const roleId = req.params.id;
        //cek apakah Role sdh ada
        const roles = await Role.findAll({
            where: {
                id: roleId
            }
        });

        if (!roles[0]) return res.status(400).json({ status: 400, msg: "Role is not exist" });

        try {

            await Role.destroy(
                {
                    where: {
                        id: roleId
                    }
                });

            res.json({ status: 200, msg: "Delete Role is successfully" });
        } catch (error) {
            console.log(error);
        }
    };
}


module.exports = RoleController







