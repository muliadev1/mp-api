const { ReedemLang } = require("../db/models/index");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

class ReedemLangController {
  static async getReedemLangs(req, res) {
    const langId = req.params.langId;
    try {
      const reedemLangs = await ReedemLang.findAll({
        attributes: ['id', 'description','title', 'language_id'],
        include: ["Reedem"],
        where: {
          language_id: langId,
        },
      });

      res.status(200).json({status: 200,reedemlang:reedemLangs});
    } catch (error) {
      console.log(error);
    }
  }
  static async byIdReedemLang(req, res) {

    const Id = req.params.id;
    try {
        const reedems = await ReedemLang.findOne({
          include: ["Reedem"],
            where: {
                id: Id
            }
        });
        res.status(200).json({status: 200,data:reedems});
    } catch (error) {
        console.log(error);
    }
};
static async createReedemLang(req, res) {
  const reedemId = req.params.reedemId;
  const langId = req.params.langId;
  const { description,title } = req.body;

  try {
    await ReedemLang.create({
      title:title,
      description: description,
      reedem_id: reedemId,
      language_id: langId,
    });
    res.json({ status: 200,msg: "Add new page successfully" });
  } catch (error) {
    console.log(error);
  }
}

  static async createReedemLangOld(req, res) {
    const reedemId = req.params.reedemId;
    const langId = req.params.langId;
    const { description,title } = req.body;

    const reedemLangs = await ReedemLang.findAll({
      attributes: ["id", "description","title"],
      where: {
        reedem_id: reedemId,
        language_id: langId,
      },
    });

    if (reedemLangs[0])
      return res.status(400).json({ status:400,msg: "Page content is alredy exist" });

    try {
      await ReedemLang.create({
        title:title,
        description: description,
        reedem_id: reedemId,
        language_id: langId,
      });
      res.json({ status: 200,msg: "Add new page successfully" });
    } catch (error) {
      console.log(error);
    }
  }

  static async detailReedemLang(req, res) {
    const reedemId = req.params.reedemId;
    const langId = req.params.langId;
    const reedemLangs = await ReedemLang.findAll({
      where: {
        reedem_id: reedemId,
        language_id: langId,
      },
    });
    if (!reedemLangs[0])
      return res.status(400).json({ status:400,msg: "ReedemLang is not exist" });
    const reedemLangsId = reedemLangs[0].id;
    try {
      const reedemLangs = await ReedemLang.findOne({
        where: {
          id: reedemLangsId,
        },
      });
      res.status(200).json({status: 200,data:reedemLangs});
    } catch (error) {
      console.log(error);
    }
  }

  //update if when languange
  static async updateReedemLang(req, res) {
    const reedemId = req.params.reedemId;
    const langId = req.params.langId;
    const { description,title,id } = req.body;
    //cek apakah ReedemLang sdh ada
    const reedemLangs = await ReedemLang.findOne({
      where: {
        reedem_id: reedemId,
        language_id: langId,
      },
    });
// console.log(reedemLangs);
    if(reedemLangs == null){
      
      try {
        await reedemLangs.update(
          {
            title:title,
            description: description,
            reedem_id: reedemId,
            language_id: langId
          },
          {
            where: {
              id: id
            },
          }
        );

        res.json({ status: 200,msg: "Update ReedemLang is successfully" });
      } catch (error) {
        console.log(error);
      }
    }else{
      const reedemLangsId = reedemLangs.id;
      try {
        await reedemLangs.update(
          {
            title:title,
            description: description,
            reedem_id: reedemId,
            language_id: langId
          },
          {
            where: {
              id: reedemLangsId
            },
          }
        );

        res.json({ status: 200,msg: "Update ReedemLang is successfully" });
      } catch (error) {
        console.log(error);
      }
    }
  }

  static async deleteReedemLang(req, res) {
    const reedemId = req.params.reedemId;
    const langId = req.params.langId;
    //cek apakah ReedemLang sdh ada
    const reedemLangs = await ReedemLang.findAll({
      where: {
        reedem_id: reedemId,
        language_id: langId,
      },
    });

    if (!reedemLangs[0])
      return res.status(400).json({ status:400,msg: "ReedemLang is not exist" });
    const reedemLangsId = reedemLangs[0].id;
    try {
      await ReedemLang.destroy({
        where: {
          id: reedemLangsId,
        },
      });

      res.json({ status: 200,msg: "Delete ReedemLang is successfully" });
    } catch (error) {
      console.log(error);
    }
  }
  static async deleteReedemLangById(req, res) {
    const Id = req.params.id;
    //cek apakah ReedemLang sdh ada
    try {
      await ReedemLang.destroy({
        where: {
          id: Id,
        },
      });

      res.json({ status: 200,msg: "Delete ReedemLang is successfully" });
    } catch (error) {
      console.log(error);
    }
  }
}

module.exports = ReedemLangController;
