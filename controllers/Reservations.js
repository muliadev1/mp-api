const Sequelize = require('sequelize');
const sequelize = require('../config/database');
const { Op } = require('sequelize');
const { QueryTypes } = require('sequelize');
const moment = require('moment');

const { Master, EnrollsLog, InitialPoint, UpgradeLog, SecondPointLog } = require('../db/models/index');

class ReservationController {

    static async getResevByMemberId(req, res) {
        const memberId = req.params.memberId;
        const startDate = moment().subtract(3, 'months').startOf('day').format('YYYY-MM-DD'); // ambil data 3 bulan ke belakang
        const endDate = moment().endOf('day').format('YYYY-MM-DD');

        try {

            const data = await Master.findAll({
                attributes: [
                    [Sequelize.fn('DISTINCT', Sequelize.col('confirmation_no')), 'confirmation_no'],
                    [Sequelize.fn('STR_TO_DATE', Sequelize.col('arrival_date'), '%d/%m/%Y'), 'arrival_date'],
                    [Sequelize.fn('STR_TO_DATE', Sequelize.col('departure_date'), '%d/%m/%Y'), 'departure_date'],
                    'room_class', 'resv_status'
                ],
                where: {
                    [Op.and]: [
                        { room_rev_usd: { [Op.gt]: 0 } },
                        { fb_rev_usd: { [Op.gt]: 0 } },
                        Sequelize.literal("STR_TO_DATE(`arrival_date`, '%d/%m/%Y') >= '" + startDate + "' AND mp='" + memberId + "'"),
                    ],
                },
                order: [
                    ['confirmation_no', 'DESC']
                ],
            });

            res.status(200).json(data)

        } catch (error) {
            console.log(error);
        }
    };

    // static async getMember(req, res) {
    //     let { email, resdate } = req.query
    //     try {
    //         const member = await Master.findAll({
    //             attributes: [
    //                 'confirmation_no',
    //                 'title',
    //                 'firstname',
    //                 'lastname',
    //                 'birthday',
    //                 'passport',
    //                 'country',
    //                 'city',
    //                 'mp',
    //                 'mp_level',
    //                 'email',
    //                 'telephone',
    //                 'market_code',
    //                 [Sequelize.fn('MIN', Sequelize.col('room_class')), 'room_class'],
    //                 [Sequelize.fn('MIN', Sequelize.col('arrival_date')), 'arrival_date'],
    //                 [Sequelize.fn('MAX', Sequelize.col('departure_date')), 'departure_date'],
    //                 [Sequelize.fn('SUM', Sequelize.col('room_rev_usd')), 'room_usd'],
    //                 [Sequelize.fn('SUM', Sequelize.col('fb_rev_usd')), 'fb_usd']
    //             ],
    //             where: {
    //                 [Op.and]: [
    //                     { mp: '' },
    //                     { mp_level: '' },
    //                     { email: email },
    //                     { birthday: { [Op.not]: null } },
    //                     { arrival_date: { [Op.not]: null } },
    //                     { departure_date: { [Op.not]: null } },
    //                     //{ room_rev_usd: { [Op.gt]: 0 } },
    //                     //{ fb_rev_usd: { [Op.gt]: 0 } },
    //                     //{ market_code: { [Op.in]: ['RAC', 'WEB', 'PACKDOM', 'PACKINT']}},
    //                     Sequelize.where(Sequelize.fn('STR_TO_DATE', Sequelize.col('insert_date'), '%d/%m/%Y'), resdate),
    //                 ],
    //             },
    //             group: [
    //                 'confirmation_no',
    //                 'title',
    //                 'firstname',
    //                 'lastname',
    //                 'birthday',
    //                 'passport',
    //                 'country',
    //                 'city',
    //                 'mp',
    //                 'mp_level',
    //                 'email',
    //                 'telephone',
    //                 'market_code'
    //             ],
    //             order: [
    //                 [Sequelize.fn('SUM', Sequelize.col('room_rev_usd')), 'DESC'],
    //                 [Sequelize.fn('SUM', Sequelize.col('fb_rev_usd')), 'DESC']
    //             ],
    //             //limit: 2,
    //             raw: true
    //         });
    //         res.status(200).json(member);
    //     } catch (error) {
    //         console.log(error);
    //     }
    // };

    static async getPointsByMemberId(req, res) {
        const memberId = req.params.memberId;

        try {

            const reservs = await EnrollsLog.findAll({

                attributes: ['confirmation_no', 'arrival_date', 'departure_date', 'room_class', 'point_promotion',
                    [Sequelize.fn('SUM', Sequelize.col('room_usd')), 'room_usd'],
                    [Sequelize.fn('SUM', Sequelize.col('fb_usd')), 'fb_usd'],
                    [Sequelize.fn('SUM', Sequelize.col('room_point')), 'room_point'],
                    [Sequelize.fn('SUM', Sequelize.col('fb_point')), 'fb_point'],
                    [Sequelize.fn('SUM', Sequelize.col('total_points')), 'total_points']
                ],
                where: {
                    member_id: memberId
                },
                group: ['confirmation_no', 'arrival_date', 'departure_date', 'room_class', 'point_promotion'],
                order: [
                    ['arrival_date', 'DESC'],
                    ['departure_date', 'DESC']
                ],

            });

            res.status(200).json(reservs);
        } catch (error) {
            console.log(error);
        }
    };

    static async getUpgradePointsByMemberId(req, res) {
        const memberId = req.params.memberId;

        try {

            const reservs = await UpgradeLog.findAll({

                attributes: ['member_id', 'member_level', 'member_next_level', 'createdBy', 'createdAt',
                    [Sequelize.fn('SUM', Sequelize.col('bonus_points')), 'total_bonus_points'],
                ],
                where: {
                    member_id: memberId
                },
                group: ['member_id', 'member_level', 'member_next_level', 'createdBy', 'createdAt'],
                order: [
                    ['member_id', 'DESC']
                ],

            });

            res.status(200).json(reservs);
        } catch (error) {
            console.log(error);
        }
    };

    static async getPointDetailByConfNo(req, res) {
        const confNo = req.params.confNo;

        try {

            const reservs = await EnrollsLog.findOne({

                attributes: ['id', 'confirmation_no', 'member_id', 'first_name', 'last_name', 'arrival_date', 'departure_date', 'room_class',
                    'room_usd', 'fb_usd', 'room_point', 'fb_point', 'total_points', 'createdAt', 'createdBy', 'updatedAt', 'updatedBy'],
                where: {
                    confirmation_no: confNo
                }
            });

            res.status(200).json(reservs);
        } catch (error) {
            console.log(error);
        }
    };


    static async getInitPointsByMemberId(req, res) {
        const memberId = req.params.memberId;

        try {
            const initpoints = await InitialPoint.findAll({

                attributes: [['createdAt', 'trx_date'],
                    'member_id', 'checkin_date', 'checkout_date', 'property',
                    'room_usd', 'fb_usd',
                    [Sequelize.literal('0'), 'room_point'], // Adding room_point with default value 0
                    [Sequelize.literal('0'), 'fb_point'],    // Adding fb_point with default value 0
                    'init_point'
                ],
                where: {
                    member_id: memberId
                },
            });

            res.status(200).json(initpoints);
        } catch (error) {
            console.log(error);
        }
    };

    static async getSecondPointsByMemberId(req, res) {
        const memberId = req.params.memberId;

        try {
            const secondpoints = await SecondPointLog.findAll({

                attributes: [['createdAt', 'trx_date'],
                    'member_id', 'total_points', 'createdAt', 'createdBy'
                ],
                where: {
                    member_id: memberId
                },
            });

            res.status(200).json(secondpoints);
        } catch (error) {
            console.log(error);
        }
    };

    static async getTotalPointsByMemberId(req, res) {
        const memberId = req.params.memberId;
        try {
            const reservs = await Master.findAll({

                attributes: [['mp', 'member_id'],
                [Sequelize.fn('SUM', Sequelize.literal('room_rev_usd * 5 + ROUND((fb_rev_usd/10) * 5,0)')), 'total_points']
                ],
                where: {
                    mp: memberId,
                    room_rev_usd: {
                        [Op.gt]: 0, // room_rev_usd>0
                    },
                    fb_rev_usd: {
                        [Op.gt]: 0,  // fb_rev_usd>0
                    },
                    market_code: {
                        [Op.in]: ['RAC', 'WEB', 'PACKDOM', 'PACKINT']
                    },
                },
            });
            res.status(200).json(reservs);
        } catch (error) {
            console.log(error);
        }
    };

    static async getTotalPoints(req, res) {
        try {
            const reservs = await Master.findAll({
                attributes: [['mp', 'member_id'],
                [Sequelize.fn('SUM', Sequelize.literal('room_rev_usd * 5 + ROUND((fb_rev_usd/10) * 5,0)')), 'total_points']
                ],
                where: {
                    mp: {
                        [Op.notIn]: ['']
                    },
                    room_rev_usd: {
                        [Op.gt]: 0, // room_rev_usd>0
                    },
                    fb_rev_usd: {
                        [Op.gt]: 0,  // fb_rev_usd>0
                    },
                    market_code: {
                        [Op.in]: ['RAC', 'WEB', 'PACKDOM', 'PACKINT']
                    },
                },
                group: ['mp'],
            });
            res.status(200).json(reservs);
        } catch (error) {
            console.log(error);
        }
    };
}

module.exports = ReservationController