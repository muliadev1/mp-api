const { RoomType } = require('../db/models/index');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const Sequelize = require('sequelize');


class RoomtypeController {
    static async getRoomtypes(req, res) {
        try {
            const rtypes = await RoomType.findAll({
            });
            res.status(200).json(rtypes);
        } catch (error) {
            console.log(error);
        }
    };


    static async createRoomtype(req, res) {
        const { room_type_code, room_type, room_type_description } = req.body;

        try {
            await RoomType.create({
                room_type_code: room_type_code,
                room_type: room_type,
                room_type_description: room_type_description,
            });
            res.json({ status: 200, msg: "Add new room type successfully" });
        } catch (error) {
            console.log(error);
        }
    };

    static async detailRoomtype(req, res) {

        const rtypeId = req.params.id;
        try {
            const roomtype = await RoomType.findOne({
                where: {
                    id: rtypeId
                }
            });
            res.status(200).json(roomtype);
        } catch (error) {
            console.log(error);
        }
    };

    static async updateRomtype(req, res) {
        const rtypeId = req.params.id;
        const { room_type_code, room_type, room_type_description } = req.body;
        //cek apakah Role sdh ada
        const roomtype = await RoomType.findAll({
            where: {
                id: rtypeId
            }
        });

        if (!roomtype[0]) return res.status(400).json({ status: 400, msg: "Room type is not exist" });

        try {

            await RoomType.update(
                {
                    room_type_code: room_type_code,
                    room_type: room_type,
                    room_type_description: room_type_description,
                },
                {
                    where: {
                        id: rtypeId
                    }
                });

            res.json({ status: 200, msg: "Update Room Type is successfully" });
        } catch (error) {
            console.log(error);
        }
    };

    static async deleteRoomtype(req, res) {
        const rtypeId = req.params.id;
        //cek apakah Role sdh ada
        const roomtype = await RoomType.findAll({
            where: {
                id: rtypeId
            }
        });

        if (!roomtype[0]) return res.status(400).json({ status: 400, msg: "Room Type is not exist" });

        try {

            await RoomType.destroy(
                {
                    where: {
                        id: rtypeId
                    }
                });

            res.json({ status: 200, msg: "Delete Room Type is successfully" });
        } catch (error) {
            console.log(error);
        }
    };
}


module.exports = RoomtypeController







