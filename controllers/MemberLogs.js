const { Member, MemberLog, sequelize } = require('../db/models/index');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const Sequelize = require('sequelize');
const { Op, where, col, fn, literal } = require('sequelize');

class MemberLogController {
    static async getPointLogs(req, res) {
        const memberId = req.params.memberId;
        try {
            const memberLogs = await Member.findAll({
                attributes: [
                    'member_id',
                    [Sequelize.col('memberlogs.first_name'), 'first_name'],
                    [Sequelize.col('memberlogs.last_name'), 'last_name'],
                    [Sequelize.col('memberlogs.name_on_card'), 'name_on_card'],
                    [Sequelize.col('memberlogs.birth_date'), 'birth_date'],
                    [Sequelize.col('memberlogs.member_level'), 'member_level'],
                    [Sequelize.col('memberlogs.joined_date'), 'joined_date'],
                    [Sequelize.col('memberlogs.exp_date'), 'exp_date'],
                    [Sequelize.col('memberlogs.exp_level_date'), 'exp_level_date'],
                    [Sequelize.col('memberlogs.primary_email'), 'primary_email'],
                    [Sequelize.col('memberlogs.total_points'), 'total_points'],
                    [Sequelize.col('memberlogs.reedem_points'), 'reedem_points'],
                    [Sequelize.col('memberlogs.available_points'), 'available_points'],

                    [Sequelize.col('memberlogs.createdBy'), 'createdBy'],
                    [Sequelize.col('memberlogs.updatedBy'), 'updatedBy'],
                    [Sequelize.fn('DATE_FORMAT', Sequelize.col('memberlogs.createdAt'), '%Y-%m-%d'), 'createdAt'],
                    [Sequelize.fn('DATE_FORMAT', Sequelize.col('memberlogs.updatedAt'), '%Y-%m-%d'), 'updatedAt'],
                ],
                include: [{
                    model: MemberLog,
                    as: "memberlogs",
                    attributes: [],
                }],
                where: {
                    member_id: memberId,
                    is_reset: 1
                }
            });
            res.status(200).json(memberLogs);
        } catch (error) {
            console.log(error);
        }
    };
}

module.exports = MemberLogController