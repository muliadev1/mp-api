const { Module } = require('../db/models/index');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const Sequelize = require('sequelize');

class ModuleController {
    static async getModules(req, res) {
        try {
            const modules = await Module.findAll({
                order: [
                    ['order_no', 'ASC'] // Urutkan berdasarkan order_no secara menaik (ASC)
                ]
            });
            res.status(200).json(modules);
        } catch (error) {
            console.log(error);
        }
    };


    static async createModule(req, res) {
        const { module_code, module_name, order_no, url, is_parent, is_trx, username } = req.body;

        try {
            await Module.create({
                module_code: module_code,
                module_name: module_name,
                order_no: order_no,
                url: url,
                is_parent: is_parent,
                is_trx: is_trx,
                createdAt: Sequelize.fn('NOW'),
                createdBy: username,
                updatedAt: Sequelize.fn('NOW'),
                updatedBy: username
            });
            res.json({ status: 200, msg: "Add new module successfully" });
        } catch (error) {
            console.log(error);
        }
    };

    static async detailModule(req, res) {

        const moduleId = req.params.id;
        try {
            const module = await Module.findOne({
                where: {
                    id: moduleId
                }
            });
            res.status(200).json(module);
        } catch (error) {
            console.log(error);
        }
    };

    static async updateModule(req, res) {
        const moduleId = req.params.id;
        const { module_code, module_name, order_no, url, is_parent, is_trx, username } = req.body;
        //cek apakah Module sdh ada
        const module = await Module.findAll({
            where: {
                id: moduleId
            }
        });

        if (!module[0]) return res.status(400).json({ status: 400, msg: "Module is not exist" });

        try {

            await Module.update(
                {
                    module_code: module_code,
                    module_name: module_name,
                    order_no: order_no,
                    url: url,
                    is_parent: is_parent,
                    is_trx: is_trx,
                    updatedAt: Sequelize.fn('NOW'),
                    updatedBy: username
                },
                {
                    where: {
                        id: moduleId
                    }
                });

            res.json({ status: 200, msg: "Update Module is successfully" });
        } catch (error) {
            console.log(error);
        }
    };

    static async deleteModule(req, res) {
        const moduleId = req.params.id;
        //cek apakah Module sdh ada
        const modules = await Module.findAll({
            where: {
                id: moduleId
            }
        });

        if (!modules[0]) return res.status(400).json({ status: 400, msg: "Module is not exist" });

        try {

            await Module.destroy(
                {
                    where: {
                        id: moduleId
                    }
                });

            res.json({ status: 200, msg: "Delete Module is successfully" });
        } catch (error) {
            console.log(error);
        }
    };
}


module.exports = ModuleController







