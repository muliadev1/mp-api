const { OfferLang } = require("../db/models/index");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

class OfferLangController {
  static async getOfferLang(req, res) {
    const langId = req.params.langId;
    try {
      const results = await OfferLang.findAll({
        attributes: ["id","title", "description"],
        include: ["ExclusiveOffer"],
        where: {
          language_id: langId,
        },
      });

      res.status(200).json({status: 200,data:results});
    } catch (error) {
      console.log(error);
    }
  }
  static async createOfferLang(req, res) {
    const offerId = req.params.offerId;
    const langId = req.params.langId;
    const { title,description } = req.body;

    try {
      await OfferLang.create({
        title:title,
        description: description,
        offer_id: offerId,
        language_id: langId,
      });
      res.json({ status: 200,msg: "Add new offerlang successfully" });
    } catch (error) {
      console.log(error);
    }
  }

  static async createOfferLangOld(req, res) {
    const offerId = req.params.offerId;
    const langId = req.params.langId;
    const { title,description } = req.body;

    const results = await OfferLang.findAll({
      attributes: ["id","title", "description"],
      where: {
        offer_id: offerId,
        language_id: langId,
      },
    });

    if (results[0])
      return res.status(400).json({ status:400,msg: "offerlang content is alredy exist" });

    try {
      await OfferLang.create({
        title:title,
        description: description,
        offer_id: offerId,
        language_id: langId,
      });
      res.json({ status: 200,msg: "Add new offerlang successfully" });
    } catch (error) {
      console.log(error);
    }
  }

  static async getOfferLangById(req, res) {
    const Id = req.params.Id;
   
    try {
      const offerLangs = await OfferLang.findOne({
        where: {
          id: Id,
        },
      });
      res.status(200).json({status: 200,data:offerLangs});
    } catch (error) {
      console.log(error);
    }
  }

  static async detailOfferLang(req, res) {
    const offerId = req.params.offerId;
    const langId = req.params.langId;
    const offerLangs = await OfferLang.findAll({
      include: ["ExclusiveOffer"],
      where: {
        offer_id: offerId,
        language_id: langId,
      },
    });
    if (!offerLangs[0])
      return res.status(400).json({ status:400,msg: "offerLang is not exist" });
    const offerLangsId = offerLangs[0].id;
    try {
      const offerLangs = await OfferLang.findOne({
        where: {
          id: offerLangsId,
        },
      });
      res.status(200).json({status: 200,data:offerLangs});
    } catch (error) {
      console.log(error);
    }
  }
  

  static async updateOfferLang(req, res) {
    const offerId = req.params.offerId;
    const langId = req.params.langId;
    const { title,description,id } = req.body;
    //cek apakah offerLang sdh ada
    const OfferLangs = await OfferLang.findOne({
      where: {
        offer_id: offerId,
        language_id: langId,
      },
    });
// console.log(offerLangs);
    if(OfferLangs == null){
      try {
        await OfferLangs.update(
          {
            title:title,
            description: description,
            offer_id: offerId,
            language_id: langId
          },
          {
            where: {
              id: id
            },
          }
        );

        res.json({ status: 200,msg: "Update offerLang is successfully" });
      } catch (error) {
        console.log(error);
      }
    }else{
      const offerLangsId = OfferLangs.id;
        try {
          await OfferLangs.update(
            {
              title:title,
              description: description,
              offer_id: offerId,
              language_id: langId
            },
            {
              where: {
                id: offerLangsId
              },
            }
          );

          res.json({ status: 200,msg: "Update offerLang is successfully" });
        } catch (error) {
          console.log(error);
        }
    }
  }

  static async deleteOfferLang(req, res) {
    const Id = req.params.id;
   
    try {
      await OfferLang.destroy({
        where: {
          id: Id,
        },
      });

      res.json({ status: 200,msg: "Delete offerLang is successfully" });
    } catch (error) {
      console.log(error);
    }
  }

  static async deleteOfferLangOld(req, res) {
    const offerId = req.params.offerId;
    const langId = req.params.langId;
    //cek apakah offerLang sdh ada
    const offerLangs = await OfferLang.findAll({
      where: {
        offer_id: offerId,
        language_id: langId,
      },
    });

    if (!offerLangs[0])
      return res.status(400).json({ status:400,msg: "offerLang is not exist" });
    const offerLangsId = offerLangs[0].id;
    try {
      await OfferLang.destroy({
        where: {
          id: offerLangsId,
        },
      });

      res.json({ status: 200,msg: "Delete offerLang is successfully" });
    } catch (error) {
      console.log(error);
    }
  }
}

module.exports = OfferLangController;
