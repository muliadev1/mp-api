const { ExtraFBCredit } = require('../db/models/index');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const Sequelize = require('sequelize');


class ExtracreditController {
    static async getExtracreditsUsedPages(req, res) {
        let { page, pageSize } = req.query

        //! Calculate starting index and number of records to retrieve
        const startIndex = (Number(page) - 1) * Number(pageSize);
        const endIndex = Number(page) * Number(pageSize);
        try {
            const langs = await ExtraFBCredit.findAll({
                offset: startIndex,
                limit: Number(pageSize)
            });
            //! Retrieve total count of records for pagination info
            const totalCount = await ExtraFBCredit.count();
            //! Calculate total number of pages
            const totalPages = Math.ceil(totalCount / Number(pageSize));
            res.status(200).json({
                data: langs,
                totalRecords: totalCount,
                currentPage: Number(page),
                totalPages: totalPages
            });
        } catch (error) {
            console.log(error);
        }
    };

    static async getExtracredits(req, res) {
        try {
            const extcredits = await ExtraFBCredit.findAll({
            });
            res.status(200).json(extcredits);
        } catch (error) {
            console.log(error);
        }
    };

    static async createExtracredit(req, res) {
        const { room_type_code, room_type_name, amount_3_months, amount_6_months, amount_9_months, username } = req.body;
        //cek apakah language sdh ada
        const extcredit = await ExtraFBCredit.findAll({
            where: {
                room_type_code: room_type_code
            }
        });

        if (extcredit[0]) return res.status(400).json({ msg: "Extracredit is alredy exist" });

        try {
            await ExtraFBCredit.create({
                room_type_code: room_type_code,
                room_type_name: room_type_name,
                amount_3_months: amount_3_months,
                amount_6_months: amount_6_months,
                amount_9_months: amount_9_months,
                createdAt: Sequelize.fn('NOW'),
                createdBy: username,
                updatedAt: Sequelize.fn('NOW'),
                updatedBy: username,
            });
            res.json({ msg: "Add new Extrapoint successfully" });
        } catch (error) {
            console.log(error);
        }
    };

    static async detailExtracredit(req, res) {

        const extcreditId = req.params.id;
        try {
            const extcredit = await ExtraFBCredit.findOne({
                where: {
                    id: extcreditId
                }
            });
            res.status(200).json(extcredit);
        } catch (error) {
            console.log(error);
        }
    };

    static async updateExtracredit(req, res) {
        const extcreditId = req.params.id;
        const { amount_3_months, amount_6_months, amount_9_months, username } = req.body;
        //cek apakah language sdh ada
        const extcredit = await ExtraFBCredit.findAll({
            attributes: ['id', 'room_type_code', 'room_type_name'],
            where: {
                id: extcreditId
            }
        });

        if (!extcredit[0]) return res.status(400).json({ msg: "Extracredit is not exist" });

        try {

            await ExtraFBCredit.update(
                {
                    amount_3_months: amount_3_months,
                    amount_6_months: amount_6_months,
                    amount_9_months: amount_9_months,
                    updatedBy: username,
                    updatedAt: Sequelize.fn('NOW')
                },
                {
                    where: {
                        id: extcreditId
                    }
                });

            res.json({ msg: "Update Extracredit is successfully" });
        } catch (error) {
            console.log(error);
        }
    };

    static async deleteExtracredit(req, res) {
        const extcreditId = req.params.id;
        //cek apakah language sdh ada
        const extcredit = await ExtraFBCredit.findAll({
            where: {
                id: extcreditId
            }
        });

        if (!extcredit[0]) return res.status(400).json({ msg: "Extracredit is not exist" });

        try {

            await ExtraFBCredit.destroy(
                {
                    where: {
                        id: extcreditId
                    }
                });

            res.json({ msg: "Delete Extracredit is successfully" });
        } catch (error) {
            console.log(error);
        }
    };
}


module.exports = ExtracreditController







