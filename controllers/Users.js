const { User, Role } = require("../db/models/index");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const Sequelize = require('sequelize');

class UserController {
  static async getUsers_pages(req, res, next) {
    let { page, pageSize } = req.query

    //! Calculate starting index and number of records to retrieve
    const startIndex = (Number(page) - 1) * Number(pageSize);
    const endIndex = Number(page) * Number(pageSize);
    try {
      const users = await User.findAll({
        include: ["role"],
        offset: startIndex,
        limit: Number(pageSize)
      });
      //! Retrieve total count of records for pagination info
      const totalCount = await User.count();
      //! Calculate total number of pages
      const totalPages = Math.ceil(totalCount / Number(pageSize));
      res.status(200).json(
        {
          users: users,
          totalRecords: totalCount,
          currentPage: Number(page),
          totalPages: totalPages
        });
    } catch (error) {
      console.log(error);
    }
  }

  static async getUsers(req, res, next) {
    console.log("MASUK API")
    try {
      const user = await User.findAll({
        include: ["role"],
      });
      res.status(200).json(user);
    } catch (error) {
      console.log(error);
    }
  }

  static async getListAccount(req, res, next) {
    try {
      const users = await User.findAll();
      res.status(200).json(users);
    } catch (error) {
      console.log(error);
    }
  }

  static async createUser(req, res, next) {
    const {
      title_name,
      first_name,
      last_name,
      username,
      password,
      email,
      phone,
      image_name,
      image_url,
      role_id,
      region,
      outlet_id,
      outlet_name,
      updatedby
    } = req.body;
    const salt = await bcrypt.genSalt();
    const hashPassword = await bcrypt.hash(password, salt);
    try {
      await User.create({
        title_name: title_name,
        first_name: first_name,
        last_name: last_name,
        username: username,
        password: hashPassword,
        email: email,
        phone: phone,
        image_name: image_name,
        image_url: image_url,
        role_id: role_id,
        region: region,
        outlet_id: outlet_id,
        outlet_name: outlet_name,
        createdAt: Sequelize.fn('NOW'),
        createdBy: updatedby,
        updatedAt: Sequelize.fn('NOW'),
        updatedBy: updatedby,
      });
      res.status(200).json({ msg: "Add user is successfully" });
    } catch (error) {
      console.log(error);
    }
  }

  static async detailUser(req, res) {
    const userId = req.params.id;
    try {
      const user = await User.findOne({
        where: {
          id: userId,
        },
      });
      res.status(200).json(user);
    } catch (error) {
      console.log(error);
    }
  }

  static async updateUser(req, res) {
    const userId = req.params.id;
    const {
      title_name,
      first_name,
      last_name,
      username,
      password,
      email,
      phone,
      image_name,
      image_url,
      role_id,
      region,
      outlet_id,
      outlet_name,
      updatedby
    } = req.body;

    //cek apakah user sdh ada
    const user = await User.findAll({
      where: {
        id: userId,
      },
    });

    if (!user[0]) return res.status(400).json({ msg: "User is not exist" });

    const salt = await bcrypt.genSalt();
    const hashPassword = password == user[0].password ? user[0].password : await bcrypt.hash(password, salt);

    try {
      await User.update(
        {
          title_name: title_name,
          first_name: first_name,
          last_name: last_name,
          username: username,
          password: hashPassword,
          email: email,
          phone: phone,
          image_name: image_name,
          image_url: image_url,
          role_id: role_id,
          region: region,
          outlet_id: outlet_id,
          outlet_name: outlet_name,
          updatedAt: Sequelize.fn('NOW'),
          updatedBy: updatedby
        },
        {
          where: {
            id: userId,
          },
        }
      );

      res.json({ msg: "Update User is successfully" });
    } catch (error) {
      console.log(error);
    }
  }

  static async deleteUser(req, res) {
    const userId = req.params.id;
    //cek apakah user sdh ada
    const user = await User.findAll({
      where: {
        id: userId,
      },
    });

    if (!user[0]) return res.status(400).json({ msg: "User is not exist" });

    try {
      await User.destroy({
        where: {
          id: userId,
        },
      });

      res.json({ msg: "Delete User is successfully" });
    } catch (error) {
      console.log(error);
    }
  }

  static async Login(req, res) {

    console.log(req.body, ">>>>>>>> isi body di api >>>>>>>>>>>");

    try {
      const user = await User.findOne({
        attributes: ["id", "first_name", "last_name", "username", "password", "email", "image_name", "image_url", "refresh_token", "role_id","region","outlet_id","outlet_name"],
        where: {
          username: req.body.username,
        },
        include: [
          {
            model: Role,
            as: 'role',
            attributes: ["id", "role_name"], // Kolom yang ingin Anda tampilkan dari model Role
            required: false
          },
        ],
      });

      const match = await bcrypt.compare(req.body.password, user.password);

      console.log(match, "match 219>>>>>>>>");

      if (!match) return res.status(400).json({ msg: "Wrong Password" });
      const userId = user.id;
      const first_name = user.first_name;
      const last_name = user.last_name;
      const username = user.username;
      const email = user.email;
      const image_name = user.image_name;
      const image_url = user.image_url;
      const role_id = user.role_id;
      const role_name = user.role.role_name; // get rolename 
      const region = user.region;
      const outlet_id = user.outlet_id;
      const outlet = user.outlet_name;

      const accessToken = jwt.sign(
        { userId, first_name, last_name, username },
        process.env.ACCESS_TOKEN_SECRET,
        {
          expiresIn: "20s",
        }
      );
      const refreshToken = jwt.sign(
        { userId, first_name, last_name, username },
        process.env.REFRESH_TOKEN_SECRET,
        {
          expiresIn: "1d",
        }
      );
      await User.update(
        { refresh_token: accessToken },
        {
          where: {
            id: userId,
          },
        }
      );
      res.cookie("refreshToken", accessToken, {
        httpOnly: true,
        maxAge: 24 * 60 * 60 * 1000,
        // secure: true //ini digunakan ketika menggunakan https
      });
      res.json({ accessToken, userId, first_name, last_name, username, email, image_name, image_url, role_id, role_name, region, outlet_id, outlet });
    } catch (error) {
      res.status(404).json({ msg: "Username not found" });
    }
  }

  static async LoginEmail(req, res) {
    try {
      const user = await User.findOne({
        attributes: ["id", "first_name", "last_name", "username", "password", "email", "image_name", "image_url", "refresh_token", "role_id","region","outlet_id","outlet_name"],
        where: {
          email: req.body.email,
        },
        include: [
          {
            model: Role,
            as: 'role',
            attributes: ["id", "role_name"], // Kolom yang ingin Anda tampilkan dari model Role
            required: false
          },
        ],
      });

      const match = await bcrypt.compare(req.body.password, user.password);

      if (!match) return res.status(400).json({ msg: "Wrong Password" });
      const userId = user.id;
      const first_name = user.first_name;
      const last_name = user.last_name;
      const username = user.username;
      const email = user.email;
      const image_name = user.image_name;
      const image_url = user.image_url;
      const role_id = user.role_id;
      const role_name = user.role.role_name; // get rolename 
      const region = user.region;
      const outlet_id = user.outlet_id;
      const outlet = user.outlet_name;

      const accessToken = jwt.sign(
        { userId, first_name, last_name, email },
        process.env.ACCESS_TOKEN_SECRET,
        {
          expiresIn: "20s",
        }
      );
      const refreshToken = jwt.sign(
        { userId, first_name, last_name, email },
        process.env.REFRESH_TOKEN_SECRET,
        {
          expiresIn: "1d",
        }
      );
      await User.update(
        { refresh_token: accessToken },
        {
          where: {
            id: userId,
          },
        }
      );
      res.cookie("refreshToken", accessToken, {
        httpOnly: true,
        maxAge: 24 * 60 * 60 * 1000,
        // secure: true //ini digunakan ketika menggunakan https
      });
      res.json({ accessToken, userId, first_name, last_name, username, email, image_name, image_url, role_id, role_name, region, outlet_id, outlet });
    } catch (error) {
      res.status(404).json({ msg: "Username not found" });
    }
  }

  static async Logout(req, res) {
    const refreshToken = req.cookies.refreshToken;
    if (!refreshToken) return res.sendStatus(204);
    const user = await User.findAll({
      where: {
        refresh_token: refreshToken,
      },
    });
    if (!user[0]) return res.sendStatus(204);
    const userId = user[0].id;
    await User.update(
      { refresh_token: null },
      {
        where: {
          id: userId,
        },
      }
    );
    res.clearCookie("refreshToken");
    return res.sendStatus(200);
  }
}

module.exports = UserController;
