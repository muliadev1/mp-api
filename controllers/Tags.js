const { Tag, TagTranslation, Page } = require('../db/models/index');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const Sequelize = require('sequelize');
const tagtransalation = require('../db/models/tag');
const { Op } = require('sequelize')


class TagController {
    static async getTagsUsedPages(req, res) {
        let { page, pageSize } = req.query

        //! Calculate starting index and number of records to retrieve
        const startIndex = (Number(page) - 1) * Number(pageSize);
        const endIndex = Number(page) * Number(pageSize);
        try {
            const tags = await Tag.findAll({
                offset: startIndex,
                limit: Number(pageSize)
            });
            //! Retrieve total count of records for pagination info
            const totalCount = await Tag.count();
            //! Calculate total number of pages
            const totalPages = Math.ceil(totalCount / Number(pageSize));
            res.status(200).json({
                data: tags,
                totalRecords: totalCount,
                currentPage: Number(page),
                totalPages: totalPages
            });
        } catch (error) {
            console.log(error);
        }
    };

    static async getTags(req, res) {
        try {
            const tags = await Tag.findAll({
                include: [
                    {
                        model: Page,
                        attributes: ['page_name'],
                        as: 'page'
                    }
                ],
                attributes: ['id', 'tag_name', 'createdAt', 'createdBy', 'updatedAt', 'updatedBy', 'is_delete'],
            });

            // Mapping data agar page_name sejajar dengan tag_id dan tag_name
            const formattedTags = tags.map(tag => ({
                id: tag.id,
                tag_name: tag.tag_name,
                page_name: tag.page ? tag.page.page_name : null, // Cek jika ada relasi Page
                createdAt: tag.createdAt,
                createdBy: tag.createdBy,
                updatedAt: tag.updatedAt,
                updatedBy: tag.updatedBy,
                is_delete: tag.is_delete
            }));

            res.status(200).json(formattedTags);
        } catch (error) {
            console.log(error);
        }
    };

    static async getTranslations(req, res, next) {
        const { page, per_page, language_id, filter } = req.query
        const perPageInt = parseInt(per_page, 10)
        const pageInt = parseInt(page, 10);
        const offset = (page - 1) * per_page

        try {

            // Buat kondisi where untuk filter
            const whereCondition = filter
                ? {
                    tag_name: {
                        [Op.like]: `%${filter}%`,
                    },
                }
                : {};

            // Hitung total data yang sesuai dengan filter
            const totalCount = await Tag.count({ where: whereCondition });


            // Lakukan query ke database menggunakan Sequelize
            const tags = await Tag.findAll({
                include: [
                    {
                        model: TagTranslation,
                        attributes: ['tag_translation', 'language_id', 'is_delete'],
                        as: 'translations',
                        where: {
                            is_delete: 0,
                            language_id: language_id // Ambil berdasarkan parameter query
                        },
                        required: false // Supaya tag tetap muncul meskipun tidak ada terjemahan yang sesuai
                    },
                    {
                        model: Page,
                        attributes: ['page_name'],
                        as: 'page'
                    }
                ],
                attributes: ['id', 'tag_name', 'page_id'],
                where: whereCondition,
                limit: perPageInt, // Batasi jumlah data per halaman
                offset: offset, // Mulai dari data ke-offset
                order: [['id', 'ASC']] // Urutkan berdasarkan ID
            })


            // Ekstrak hanya data yang diperlukan
            const extractedTags = tags.map(tag => ({
                id: tag.dataValues.id,
                tag_name: tag.dataValues.tag_name,
                page_name: tag.dataValues.page ? tag.dataValues.page.page_name : null,
                page_id: tag.dataValues.page_id,
                tag_translation: tag.dataValues.translations.length > 0
                    ? tag.dataValues.translations[0].tag_translation
                    : "" // Pastikan data selalu ada
            }));

            res.status(200).json({
                data: extractedTags,
                totalData: totalCount,
            })

        } catch (error) {
            console.log(error)
        }
    }

    static async upInsertTranslations(req, res) {
        if (req.method !== "POST") {
            return res.status(405).json({ message: "Method Not Allowed" });
        }

        const { translations, language_id, username } = req.body;

        try {
            for (const translation of translations) {
                if (!translation.tag_id || translation.tag_translation === undefined) {
                    console.error("Invalid translation data:", translation);
                    continue; // Skip jika data tidak valid
                }

                const existing = await TagTranslation.findOne({
                    where: { tag_id: translation.tag_id, language_id: language_id },
                });

                if (existing) {
                    // Update jika sudah ada
                    await TagTranslation.update(
                        {
                            tag_translation: translation.tag_translation,
                            updatedAt: Sequelize.fn('NOW'),
                            updatedBy: username
                        },
                        { where: { tag_id: translation.tag_id, language_id: language_id } }
                    );
                } else {
                    // Insert jika belum ada
                    await TagTranslation.create({
                        tag_id: translation.tag_id,
                        language_id: language_id,
                        tag_translation: translation.tag_translation,
                        createdAt: Sequelize.fn('NOW'),
                        createdBy: username,
                        updatedAt: Sequelize.fn('NOW'),
                        updatedBy: username
                    });
                }
            }

            return res.status(200).json({ message: "Translations updated successfully" });
        } catch (error) {
            console.error(error);
            return res.status(500).json({ message: "Failed to update translations" });
        }
    }

    static async createTag(req, res) {
        const { tag_name, page_id, username } = req.body;
        //cek apakah language sdh ada
        const tag = await Tag.findAll({
            where: {
                tag_name: tag_name
            }
        });

        if (tag[0]) return res.status(400).json({ msg: "Tag is alredy exist" });

        try {
            await Tag.create({
                tag_name: tag_name,
                page_id: page_id,
                is_delete: 0,
                createdAt: Sequelize.fn('NOW'),
                createdBy: username,
                updatedAt: Sequelize.fn('NOW'),
                updatedBy: username,
            });
            res.json({ msg: "Add new Tag successfully" });
        } catch (error) {
            console.log(error);
        }
    };

    static async detailTag(req, res) {

        const tagId = req.params.id;
        try {
            const tag = await Tag.findOne({
                where: {
                    id: tagId
                }
            });
            res.status(200).json(tag);
        } catch (error) {
            console.log(error);
        }
    };

    static async updateTag(req, res) {
        const tagId = req.params.id;
        const { tag_name, page_id, username } = req.body;
        //cek apakah tag sdh ada
        const tag = await Tag.findAll({
            attributes: ['id', 'tag_name', 'page_id'],
            where: {
                id: tagId
            }
        });

        if (!tag[0]) return res.status(400).json({ msg: "Tag is not exist" });

        try {

            await Tag.update(
                {
                    tag_name: tag_name,
                    page_id: page_id,
                    updatedBy: username,
                    updatedAt: Sequelize.fn('NOW')
                },
                {
                    where: {
                        id: tagId
                    }
                });

            res.json({ msg: "Update Tag is successfully" });
        } catch (error) {
            console.log(error);
        }
    };

    static async deleteTag(req, res) {
        const tagId = req.params.id;
        //cek apakah tag sdh ada
        const tag = await Tag.findAll({
            where: {
                id: tagId
            }
        });

        if (!tag[0]) return res.status(400).json({ msg: "Tas is not exist" });

        try {

            await Tag.destroy(
                {
                    where: {
                        id: tagId
                    }
                });

            res.json({ msg: "Delete Tag is successfully" });
        } catch (error) {
            console.log(error);
        }
    };
}


module.exports = TagController







