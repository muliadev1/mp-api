const { MissingPointEmail } = require('../db/models/index');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const Sequelize = require('sequelize');


class MissingPointController {

    static async getPointEmail(req, res) {

        try {

            const result = await MissingPointEmail.findAll({
                attributes: [
                    'id',
                    'confirmation_no',
                    'guest_name',
                    'property',
                    'checkin',
                    'checkout',
                    'comment'
                ],
                order: [
                    ['createdAt', 'DESC'],

                ],
                offset: startIndex,
                limit: Number(pageSize)
            });
             //! Retrieve total count of records for pagination info
             const totalCount = await MissingPointEmail.count();
             //! Calculate total number of pages
             const totalPages = Math.ceil(totalCount / Number(pageSize));
            res.status(200).json({
                missing:missing,
                totalRecords: totalCount,
                currentPage: Number(page),
                totalPages: totalPages 
            });
            res.status(200).json(result);
        } catch (error) {
            console.log(error);
        }

    };

    static async getPointEmailById(req, res) {

        const Id = req.params.id;
        try {
            const missing = await MissingPointEmail.findOne({
                where: {
                    id: Id
                }
            });
            res.status(200).json(missing);
        } catch (error) {
            console.log(error);
        }
    };

    static async createPointEmail(req, res) {
        const { member_id, confirmation_no, guest_name, hotel, checkin_date, checkout_date, attach_file, attach_url, comments, username } = req.body;

        try {
            await MissingPointEmail.create({
                member_id: member_id,
                confirmation_no: confirmation_no,
                guest_name: guest_name,
                property: hotel,
                checkin: checkin_date,
                checkout: checkout_date,
                file: attach_file,
                url: attach_url,
                comment: comments,
                createdAt: Sequelize.fn('NOW'),
                createdBy: username,
                updatedAt: Sequelize.fn('NOW'),
                updatedBy: username,
            });
            res.json({ msg: "Add new data successfully" });
        } catch (error) {
            console.log(error);
        }
    };

    static async updatePointEmail(req, res) {
        const Id = req.params.id;
        const { member_id, confirmation_no, guest_name, hotel, checkin_date, checkout_date, attach_file, attach_url, comments, username } = req.body;

        const data = await MissingPointEmail.findAll({
            where: {
                id: Id
            }
        });

        if (!data[0]) return res.status(400).json({ status: 400, msg: "Data is not exist" });

        try {

            await MissingPointEmail.update(
                {
                    member_id: member_id,
                    confirmation_no: confirmation_no,
                    guest_name: guest_name,
                    property: hotel,
                    checkin: checkin_date,
                    checkout: checkout_date,
                    file: attach_file,
                    url: attach_url,
                    comment: comments,
                    updatedAt: Sequelize.fn('NOW'),
                    updatedBy: username,
                },
                {
                    where: {
                        id: Id
                    }
                });

            res.json({ status: 200, msg: "Update Data is successfully" });
        } catch (error) {
            console.log(error);
        }
    };

    static async deletePointEmail(req, res) {
        const Id = req.params.id;

        const data = await MissingPointEmail.findAll({
            where: {
                id: Id
            }
        });

        if (!data[0]) return res.status(400).json({ status: 400, msg: "data is not exist" });

        try {

            await MissingPointEmail.destroy(
                {
                    where: {
                        id: Id
                    }
                });

            res.json({ status: 200, msg: "Delete data is successfully" });
        } catch (error) {
            console.log(error);
        }
    };

}

module.exports = MissingPointController