
const XlsxStreamReader = require('xlsx-stream-reader');
const fs = require('fs');
const Sequelize = require('sequelize');
const { Op, literal, fn, col } = require('sequelize');
const moment = require('moment');
const { Master, MasterCSV, MasterXLS } = require('../db/models/index');
const { default: readXlsxFile } = require('read-excel-file/node');

class excelController {
    static async uploadExcel(req, res) {

        console.log("<< masuk controller >> ")

        const location = req.body.location;
        const departuredate = req.body.departuredate;
        const username = req.body.username;
        const filename = req.body.excelfile.filename.split('.').slice(0, -1).join('.');
        const uriFile = process.env.UPLOAD_XLSX + req.body.excelfile.filename;

        // create stay date, ambil tanggal kemarin
        const currDate = new Date();
        currDate.setDate(currDate.getDate() - 1);
        const current_date = currDate.getFullYear() + "-" + (currDate.getMonth() + 1) + "-" + currDate.getDate();
        const current_time = currDate.getHours() + ":" + currDate.getMinutes() + ":" + currDate.getSeconds();
        const date_time = current_date + " " + current_time;

        try {
            if (uriFile == undefined) {
                return res.status(400).send({
                    message: "Please upload an excel file!",
                });
            }

            // ================================================
            // Urutan kolom yang ada di Excel file
            // ================================================
            // Confirmation No (0)	Rate Description (1)	
            // Promotions (Codes) (2)	Market (Code) (3)	
            // Market Group (Code) (4)	Rate Code (5)	
            // Currency (Code) (6)	Business Block (7)	
            // Adults (8)	Children (9)	First Name (10)	Last Name (11)	
            // Source (Description) (12)	Source (Code) (13)	Agent Name (14)	
            // Company Name (15)	Room Number (16)	Group Status (17)	
            // Reservation Status (18)	Guest Title (Code) (19)	Insert Date (20)	
            // Arrival Date (21)	Departure Date (22)	Length of Stay (Nights) (23)	
            // Label (room type) (24)	Room Class (Code) (25)	Nationality (26)	
            // Country Name (27)	City (28)	Membership Card Numbers (29)	
            // Memberships (Codes) (30)	Birth Date (31)	E-mail (32)	Phone No (33)	
            // Bookeeping (34)	Room Revenue over the Whole Reservation (35)	
            // Room Revenue USD (36)	F&B Revenue over the Whole Reservation (37)	FB Revenue USD (38)

            var workBookReader = new XlsxStreamReader();

            workBookReader.on('error', function (error) {
                console.log("Error:", error);
                res.status(500).send({
                    message: "Could not read the excel file: " + req.body.excelfile.filename,
                });
            });

            let total=0;

            workBookReader.on('worksheet', function (workSheetReader) {

                if (workSheetReader.id > 1) {
                    // we only want first sheet
                    console.log('Skip Worksheet:', workSheetReader.id)
                    workSheetReader.skip()
                    return
                  }
                  console.log('Worksheet:', workSheetReader.id)

                workSheetReader.on('row', function (row) {
                    row.values.forEach(function (rowVal, colNum) {
                      console.log('RowNum', row.attributes.r, 'colNum', colNum, 'rowValLen', rowVal.length, 'rowVal', "'" + rowVal + "'")
                    })
                  })

                // call process after registering handlers
                workSheetReader.process();
            });

            workBookReader.on('error', function (error) {
                console.error('Error:', error);
            });

            workBookReader.on('end', function () {
                console.log('Selesai membaca file Excel.');
            });

            fs.createReadStream(uriFile).pipe(workBookReader);

            // console.log(uriFile, "URL file excel >>>>>>>>>>>>")


            // -------------------------------------
            // const workBookReader = new XlsxStreamReader();
            // workBookReader.on("error", function (error) {
            //     console.log("Error:", error);
            //     res.status(500).send({
            //         message: "Could not upload the file: " + req.body.excelfile.filename,
            //     });
            // });
            // workBookReader.on("worksheet", function (workSheetReader) {
            //     if (workSheetReader.id == 1) { // Sheet1
            //         workSheetReader.on("row", function (row) {
            //             // Proses setiap baris data di sini
            //         });
            //         workSheetReader.on("end", function () {
            //             console.log("Selesai membaca lembar kerja 'Sheet1'.");
            //         });
            //         workSheetReader.process();
            //     }
            // });
            // workBookReader.open(uriFile);

            // ------------------------------------


            // workBookReader.on('worksheet', function (workSheetReader) {
            //     if (workSheetReader.id == 1) { // Sheet1
            //         workSheetReader.on('row', function (row) {

            //             console.log("jumlah Row data:", row);


            //             // if (isHeader) {
            //             //     // Skip header
            //             //     isHeader = false;
            //             //     return;
            //             // }

            //             let insertDate = moment(row[20]).format('DD/MM/YYYY');
            //             let arrivalDate = moment(row[21]).format('DD/MM/YYYY');
            //             let departureDate = moment(row[22]).format('DD/MM/YYYY');
            //             let revStatus = row[18] ? row[18].toUpperCase() : '';

            //             masters.push({
            //                 stay_date: arrivalDate,
            //                 confirmation_no: row[0] || '',
            //                 rn: row[23] || '',
            //                 bookkeeping: row[34] || '',
            //                 room_rev: 0,
            //                 fb_rev: 0,
            //                 other_rev: 0,
            //                 market_code: row[3] || '',
            //                 market_group: row[4] || '',
            //                 rate_code: row[5] || '',
            //                 currency: row[6] || '',
            //                 rate_amount: 0,
            //                 share_amount: 0,
            //                 room_dep: '',
            //                 adults: row[8] || '',
            //                 children: row[9] || '',
            //                 firstname: row[10] || '',
            //                 lastname: row[11] || '',
            //                 source_name: row[12] || '',
            //                 agent_name: row[14] || '',
            //                 company_name: row[15] || '',
            //                 qq_room: '',
            //                 qs: '',
            //                 group_status: row[17] || '',
            //                 resv_status: revStatus,
            //                 packages: '',
            //                 title: row[19] || '',
            //                 passport: '',
            //                 local_id: '',
            //                 cancelation_date: '',
            //                 insert_date: insertDate,
            //                 lead_days: '',
            //                 arrival_date: arrivalDate,
            //                 departure_date: departureDate,
            //                 los: row[23] || '',
            //                 no_show: '',
            //                 rtc: '',
            //                 room_type: row[24] || '',
            //                 room_class: row[25] || '',
            //                 nationality: row[26] || '',
            //                 country: row[27] || '',
            //                 city: row[28] || '',
            //                 mp: row[29] || '',
            //                 mpenroll_date: '',
            //                 mp_level: row[30] || '',
            //                 mp_enroll: '',
            //                 wcc: '',
            //                 wcc_enroll: '',
            //                 sc: '',
            //                 sc_enroll: '',
            //                 birthday: row[31] || '',
            //                 mailist: '',
            //                 email: row[32] || '',
            //                 telephone: row[33] || '',
            //                 room_rev_idr: row[35],
            //                 room_rev_usd: row[36],
            //                 fb_rev_idr: row[37],
            //                 fb_rev_usd: row[38],
            //                 other_rev_idr: 0,
            //                 other_rev_usd: 0,
            //                 temp_points: 0,
            //                 region: location,
            //                 createdBy: username,
            //                 updatedBy: username
            //             });
            //         });

            //         workSheetReader.on('end', async function () {
            //             console.log(masters, ">>>>>>> isi data masters>>>>>>>>");
            //             // Lakukan apa yang perlu dilakukan setelah membaca semua baris
            //             // Misalnya, lakukan penghapusan data lama dan masukkan data baru ke database di sini
            //             // ===========================================================
            //             // hapus data di masters berdasarkan staydate dan location
            //             // ===========================================================
            //             // await Master.destroy({
            //             //     where: {
            //             //         [Op.and]: [
            //             //             Sequelize.literal("CASE WHEN departure_date <> '' THEN STR_TO_DATE(departure_date, '%d/%m/%Y') ELSE NULL END = '" + departuredate + "'"),
            //             //             { region: location }
            //             //         ]
            //             //     }
            //             // })

            //             //==================================
            //             // bulk insert to masters table
            //             //==================================
            //             // await Master.bulkCreate(masters)
            //             //     .then(() => {
            //             //         res.status(200).send({
            //             //             message: "Uploaded the file successfully: " + req.body.excelfile.filename,
            //             //         });
            //             //     })
            //             //     .catch((error) => {
            //             //         res.status(500).send({
            //             //             message: "Fail to import data into database!",
            //             //             error: error.message,
            //             //         });
            //             //     });
            //             //================================= 
            //             //    end bulk insert
            //             // ================================

            //             try {
            //                 // Lakukan bulkCreate ke tabel Anda
            //                 await Master.bulkCreate(masters);
            //                 console.log("Data berhasil disimpan ke dalam tabel.");
            //                 // Kirim respons jika perlu
            //                 res.status(200).send({
            //                     message: "Uploaded the file successfully: " + req.body.excelfile.filename,
            //                 });
            //             } catch (error) {
            //                 console.error("Gagal menyimpan data ke dalam tabel:", error);
            //                 // Kirim respons dengan status error jika terjadi kesalahan
            //                 res.status(500).send({
            //                     message: "Fail to import data into database!",
            //                     error: error.message,
            //                 });
            //             }
            //         });

            //         workSheetReader.process();
            //     }
            // });

            // fs.createReadStream(uriFile).pipe(workBookReader);

            // Buka aliran file XLSX
            // const stream = fs.createReadStream(uriFile);
            // stream.pipe(workBookReader);

        } catch (error) {
            console.log(error);
            res.status(500).send({
                message: "Could not upload the file: " + req.body.excelfile.filename,
            });
        }
    }

}

module.exports = excelController





