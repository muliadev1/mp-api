const { MemberVoucher, ReedemsPoint, RedemsPointItem, Member, User, Reedem } = require('../db/models/index');
const Sequelize = require('sequelize');
const { Op } = require('sequelize');
const { getVouchersUsed } = require('./Members');
const membervoucher = require('../db/models/membervoucher');


class QRCodeController {

    static async getVouchers(req, res, next) {
        const { page = 1, per_page = 10, filter } = req.query;
        const perPageInt = parseInt(per_page, 10);
        const offset = (page - 1) * perPageInt;

        try {
            // Basic where condition
            const whereCondition = {
                is_used: 0,
                [Op.or]: [
                    {
                        member_id: {
                            [Op.like]: `%${filter}%`
                        }
                    },
                    {
                        voucher_no: {
                            [Op.like]: `%${filter}%`
                        }
                    },
                    Sequelize.literal(`LOWER(members.name_on_card) LIKE '%${filter.toLowerCase()}%'`) // Add this line
                ],

            };

            // Combined query for data and count
            const { count, rows } = await MemberVoucher.findAndCountAll({
                attributes: [
                    'member_id',
                    'voucher_no',
                    'date_expired',
                ],
                include: [
                    {
                        model: Member,
                        as: 'members',
                        attributes: ['name_on_card'],
                        required: true,
                    },
                    {
                        model: Reedem,
                        as: 'reedems', // Pastikan alias ini sesuai dengan asosiasi di model
                        attributes: ['location'],
                        required: false, // Ubah menjadi `true` jika hanya ingin data dengan `reedems` yang terkait
                    },
                ],
                where: whereCondition,
                offset: offset,
                limit: perPageInt,
                group: [
                    'MemberVoucher.member_id',
                    'MemberVoucher.voucher_no',
                    'MemberVoucher.date_expired',
                    'members.id',// Add this to the group
                    'reedems.id', // Tambahkan ID reedem ke grup
                ],
                order: [
                    ['date_expired', 'DESC'],
                    ['member_id', 'ASC'],
                    ['voucher_no', 'ASC'],
                ],
            });

            res.status(200).json({
                data: rows,
                totalData: count.length,
            });
        } catch (error) {
            console.log(error);
            res.status(500).json({ error: 'Internal Server Error' });
        }
    }

    static async getVoucher(req, res) {

        const voucherNo = req.params.voucherNo;

        //cek apakah voucher sudah ada
        const voucher = await MemberVoucher.findOne({
            where: {
                voucher_no: voucherNo
            }
        });

        if (!voucher) return res.status(400).json({ status: 400, msg: "Voucher is not exist" });

        try {
            const vouchers = await MemberVoucher.findAll({
                include: [
                    {
                        model: Member,
                        attributes: ['name_on_card','primary_email'], // Atur atribut yang ingin Anda ambil dari model Outlet
                        as: 'members', // Sesuaikan dengan nama asosiasi antara Dining dan Outlet
                    },
                    {
                        model: ReedemsPoint,
                        attributes: ['previous_balance','total_reedem_points','remaining_points','location','createdAt','is_cancel','is_loyalty', 'is_revenue', 'is_financial', 'is_resident', 'is_manual', 'approve_status_id'],
                        as: 'reedemspoints',
                        where: {
                            id: voucher.reedem_point_id,
                        },
                        include: [ // Hubungkan langsung dengan RedemsPointItem
                            {
                                model: RedemsPointItem,
                                attributes: ['redem_type_id','redem_type', 'required_points', 'qty_points',  ['total_redem_points', 'redem_points'], 'comments'],
                                as: 'items',
                            }
                        ]
                    },
                ],
                where: {
                    voucher_no: voucherNo
                },

            });
            return res.json({ status: 200, msg: "Success", vouchers: vouchers });
        } catch (error) {
            console.log(error);
        }
    };

    static async updateVoucher(req, res) {
        const voucherId = req.params.id;
        const { beneficiary, username } = req.body;
        //cek apakah language sdh ada
        const voucher = await MemberVoucher.findAll({
            where: {
                id: voucherId
            }
        });

        if (!voucher[0]) return res.status(400).json({ msg: "Voucher is not exist" });

        try {

            await MemberVoucher.update(
                {
                    beneficiary: beneficiary,
                    updatedBy: username,
                    updatedAt: Sequelize.fn('NOW')
                },
                {
                    where: {
                        id: voucherId
                    }
                }); 

            res.json({ msg: "Update beneficiary is successfully" });
        } catch (error) {
            console.log(error);
        }
    };

    static async updateIsUsed(req, res) {
        const { qrCode, username } = req.body;

        //cek apakah voucher sudah ada
        const voucher = await MemberVoucher.findOne({
            where: {
                voucher_no: qrCode
            }
        });

        if (!voucher) return res.status(400).json({ status: 400, msg: "Voucher is not exist" });

        //cek apakah Reedem sdh ada
        const vouchers = await MemberVoucher.findAll({
            include: [
                {
                    model: Member,
                    attributes: ['name_on_card'], // Atur atribut yang ingin Anda ambil dari model Outlet
                    as: 'members', // Sesuaikan dengan nama asosiasi antara Dining dan Outlet
                    where: {
                        member_id: voucher.member_id,
                    }
                },
            ],
            where: {
                voucher_no: qrCode
            },
        });


        const chkCancel = await ReedemsPoint.findOne({
            where: {
                id: voucher.reedem_point_id,
                member_id: voucher.member_id,
                reedem_id: voucher.reedem_id,
                is_cancel: 1
            }
        })

        if (chkCancel) return res.status(400).json({ status: 'cancel', msg: "Voucher is already cancel." });

        // Retrieve outlet name
        const user = await User.findOne({
            where: {
                username: username
            },
            attributes: ['outlet_name']
        })

        try {

            // Cek apakah voucher ditemukan
            if (!vouchers || vouchers.length === 0) {
                return res.status(200).json({ status: 'not_found', msg: "QR Code does not exist" });
            } else {
                // Cek apakah voucher sudah digunakan
                if (vouchers[0]?.is_used === 1) {
                    return res.status(200).json({ status: 'used', msg: 'This voucher has been used.', vouchers });
                } else {
                    await MemberVoucher.update(
                        {
                            is_used: 1,
                            outlet_name: user?.outlet_name,
                            updatedAt: Sequelize.fn('NOW'),
                            updatedBy: username
                        },
                        {
                            where: {
                                voucher_no: qrCode
                            }
                        });

                    // Retrieve the updated voucher to include the latest values
                    const updatedVoucher = await MemberVoucher.findAll({
                        where: {
                            voucher_no: qrCode
                        },
                        include: [
                            {
                                model: Member,
                                attributes: ['name_on_card'],
                                as: 'members'
                            },
                        ],
                    });

                    return res.json({ status: 200, msg: "Update QR Code is successfully", vouchers: updatedVoucher });
                }
            }

        } catch (error) {
            console.error('Error updating voucher:', error);
            return res.status(500).json({ status: 'error', msg: 'Internal server error', error: error.message });
        }
    };

}


module.exports = QRCodeController







