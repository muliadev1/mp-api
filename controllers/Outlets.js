const { Outlet } = require('../db/models/index');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const Sequelize = require('sequelize');


class OutletController {
    static async getOutlets(req, res) {
        try {
            const outlets = await Outlet.findAll({
                order: [['outlet_name', 'ASC']]
            });
            res.status(200).json(outlets);
        } catch (error) {
            console.log(error);
        }
    };

    static async getOutletsByRegion(req, res) {
        const region = req.params.region;
        try {
            const outlets = await Outlet.findAll({
                where : {
                    region: region
                },
                order: [['outlet_name', 'ASC']]
            });
            res.status(200).json(outlets);
        } catch (error) {
            console.log(error);
        }
    };

    static async createOutlet(req, res) {
        const { outlet_name, cuisine, opening_hour, breakfast, lunch, dinner, snack, sunday_brunch,
            sunday_lunch, all_day_dining, afternoon_tea, late_night, entertainment, region, username } = req.body;

        try {
            await Outlet.create({
                outlet_name: outlet_name,
                cuisine: cuisine,
                opening_hour: opening_hour,
                breakfast: breakfast,
                lunch: lunch,
                dinner: dinner,
                snack: snack,
                sunday_brunch: sunday_brunch,
                sunday_lunch: sunday_lunch,
                all_day_dining: all_day_dining,
                afternoon_tea: afternoon_tea,
                late_night: late_night,
                entertainment: entertainment,
                region: region,
                createdAt: Sequelize.fn('NOW'),
                createdBy: username,
                updatedAt: Sequelize.fn('NOW'),
                updatedBy: username,
            });
            res.json({ status: 200, msg: "Add new Outlet successfully" });
        } catch (error) {
            console.log(error);
        }
    };

    static async detailOutlet(req, res) {

        const outletId = req.params.id;
        try {
            const outlet = await Outlet.findOne({
                where: {
                    id: outletId
                }
            });
            res.status(200).json(outlet);
        } catch (error) {
            console.log(error);
        }
    };

    static async updateOutlet(req, res) {
        const outletId = req.params.id;
        const { outlet_name, cuisine, opening_hour, breakfast, lunch, dinner, snack, sunday_brunch,
            sunday_lunch, all_day_dining, afternoon_tea, late_night, entertainment, region, username } = req.body;

        //cek apakah Role sdh ada
        const outlet = await Outlet.findAll({
            where: {
                id: outletId
            }
        });

        if (!outlet[0]) return res.status(400).json({ status: 400, msg: "Outlet is not exist" });

        try {

            await Outlet.update(
                {
                    outlet_name: outlet_name,
                    cuisine: cuisine,
                    opening_hour: opening_hour,
                    breakfast: breakfast,
                    lunch: lunch,
                    dinner: dinner,
                    snack: snack,
                    sunday_brunch: sunday_brunch,
                    sunday_lunch: sunday_lunch,
                    all_day_dining: all_day_dining,
                    afternoon_tea: afternoon_tea,
                    late_night: late_night,
                    entertainment: entertainment,
                    region: region,
                    updatedAt: Sequelize.fn('NOW'),
                    updatedBy: username
                },
                {
                    where: {
                        id: outletId
                    }
                });

            res.json({ status: 200, msg: "Update Outlet is successfully" });
        } catch (error) {
            console.log(error);
        }
    };


    static async deleteOutlet(req, res) {
        const outletId = req.params.id;
        //cek apakah Role sdh ada
        const outlet = await Outlet.findAll({
            where: {
                id: outletId
            }
        });

        if (!outlet[0]) return res.status(400).json({ status: 400, msg: "Outlet is not exist" });

        try {

            await Outlet.destroy(
                {
                    where: {
                        id: outletId
                    }
                });

            res.json({ status: 200, msg: "Delete Outlet is successfully" });
        } catch (error) {
            console.log(error);
        }
    };
}


module.exports = OutletController







