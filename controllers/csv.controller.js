
const { Sequelize } = require('sequelize');
const { Op, literal } = require('sequelize');

const fs = require('fs');
const path = require('path');
const multer = require('multer');
const csv = require('fast-csv');
const util = require('util');
const sequelize = require('../middleware/sequelize'); // Sesuaikan dengan path yang sesuai
const { Master } = require('../db/models/index');



// mapping antara kolom CSV dan kolom tabel
const columnMapping = {
    'Stay Date': 'stay_date',
    'Confirmation No': 'confirmation_no',
    'Room Night': 'rn',
    'Bookkeeping': 'bookkeeping',
    'Rate Desc': 'room_rev',
    'Special Code': 'fb_rev',
    'Promotions': 'other_rev',
    'Market Code': 'market_code',
    'Market Group': 'market_group',
    'Rate Code': 'rate_code',
    'Currency': 'currency',
    'Rate Amount': 'rate_amount',
    'Share Amount': 'share_amount',
    'Room Dep': 'room_dep',
    'Adults': 'adults',
    'Children': 'children',
    'First Name': 'firstname',
    'Last Name': 'lastname',
    'Source Name': 'source_name',
    'Agent Name': 'agent_name',
    'Company Name': 'company_name',
    'Out of Order': 'qq_room',
    'Out of Service': 'qs',
    'Group Status': 'group_status',
    'Resv Status': 'resv_status',
    'Packages': 'packages',
    'Title': 'title',
    'Passport': 'passport',
    'Local ID': 'local_id',
    'Cancellation Date': 'cancelation_date',
    'Insert Date': 'insert_date',
    'Lead Days': 'lead_days',
    'Arrival Date': 'arrival_date',
    'Departure Date': 'departure_date',
    'Length of Stay': 'los',
    'No Show': 'no_show',
    'Room Type Charge': 'rtc',
    'Room Type': 'room_type',
    'Room Class': 'room_class',
    'Natonality': 'nationality',
    'Country': 'country',
    'City': 'city',
    'Member #': 'mp',
    'Member Enroll Date': 'mpenroll_date',
    'Member Level': 'mp_level',
    'Member Enroll': 'mp_enroll',
    'Winner Circle Club #': 'wcc',
    'Winner Circle Club Enroll': 'wcc_enroll',
    'Signature Club #': 'sc',
    'Signature Club Enroll': 'sc_enroll',
    'BirthDay': 'birthday',
    'MailList': 'mailist',
    'Email': 'email',
    'Telphone': 'telephone',
    'Room Rev USD': 'room_rev_usd',
    'Room Rev IDR': 'room_rev_idr',
    'FB Rev USD': 'fb_rev_usd',
    'FB Rev IDR': 'fb_rev_idr',
    'Other Rev USD': 'other_rev_usd',
    'Other Rev IDR': 'other_rev_idr'
};

// (1)Stay Date,
// (2)Confirmation No,
// (3)Room Night,
// (4)Bookkeeping,
// (5)Rate Desc,
// (6)Special Code,
// (7)Promotions,
// (8)Market Code,
// (9)Market Group,
// (10)Rate Code,
// (11)Currency,
// (12)Rate Amount,
// (13)Share Amount,
// (14)Pay/Com, ==> @dummy
// (15)Room Arr, ==> @dummy
// (16)Room Dep,
// (17)Business Block, ==> @dummy
// (18)Adults,
// (19)Children,
// (20)Deduc, ==> @dummy
// (21)Group, ==> @dummy
// (22)First Name,
// (23)Last Name,
// (24)Source Name,
// (25)Source Code, ==> @dummy
// (26)Agent Name,
// (27)Agent Code, ==> @dummy
// (28)Company Name,
// (29)Company Code, ==> @dummy
// (30)Comp Rooms, ==> @dummy
// (31)Tax Incl YN, ==> @dummy
// (32)Due Out YN, ==> @dummy
// (33)Out of Order,
// (34)Out of Service,
// (35)Room No, ==> @dummy
// (36)Group Status,
// (37)Resv Status,
// (38)Event Type, ==> @dummy
// (39)Resv Type, ==> @dummy
// (40)Resv Name Id, ==> @dummy
// (41)Packages,
// (42)Title,
// (43)Passport,
// (44)Local ID,
// (45)Cancellation Date,
// (46)Cancellation Code, ==> @dummy
// (47)Cancellation Desc, ==> @dummy
// (48)Insert Date,
// (49)Lead Days,
// (50)Arrival Date,
// (51)Departure Date,
// (52)Length of Stay,
// (53)No Show,
// (54)TC, ==> @dummy
// (55)Room Type Charge,
// (56)Room Type,
// (57)Booked Room Class, ==> @dummy
// (58)Room Class,
// (59)PHY/PSE, ==> @dummy
// (60)RES/ALT, ==> @dummy
// (61)Natonality,
// (62)Country,
// (63)City,
// (64)Member #,
// (65)Member Enroll Date,
// (66)Member Level,
// (67)Member Enroll,
// (68)Winner Circle Club #,
// (69)Winner Circle Club Enroll,
// (70)Signature Club #,
// (71)Signature Club Enroll,
// (72)BirthDay,
// (73)MailList,
// (74)Email,
// (75)Telphone,
// (76)Room Rev USD,
// (77)Room Rev IDR,
// (78)FB Rev USD,
// (79)FB Rev IDR,
// (80)Other Rev USD,
// (81)Other Rev IDR

// (stay_date,confirmation_no,rn,bookkeeping,room_rev,fb_rev,other_rev,market_code,market_group,rate_code,currency,
//     rate_amount,share_amount,@dummy,@dummy,room_dep,@dummy,adults,children,@dummy,@dummy,firstname,lastname,source_name,
//     @dummy,agent_name,@dummy,company_name,@dummy,@dummy,@dummy,@dummy,qq_room,qs,@dummy,group_status,resv_status,@dummy,@dummy,
//     @dummy,packages,title,passport,local_id,cancelation_date,@dummy,@dummy,insert_date,lead_days,arrival_date,departure_date,los,
//     no_show,@dummy,rtc,room_type,@dummy,room_class,@dummy,@dummy,nationality,country,city,mp,mpenroll_date,mp_level,mp_enroll,wcc,wcc_enroll,
//     sc,sc_enroll,birthday,mailist,email,telephone,room_rev_usd,room_rev_idr,fb_rev_usd,fb_rev_idr,other_rev_usd,other_rev_idr);



const deleteRecords = async (yearToDelete) => {
    const currentDate = new Date();

    // Mengurangkan 30 hari dari tanggal sekarang
    // currentDate.setDate(currentDate.getDate() - 30);

    const currentYear = currentDate.getFullYear();

    // Mengambil informasi tanggal, bulan, dan tahun
    const year = currentDate.getFullYear();
    const month = currentDate.getMonth() + 1; // Ingat bahwa bulan dimulai dari 0 (Januari) hingga 11 (Desember)
    const day = currentDate.getDate();

    // Format tanggal menjadi string "YYYY-MM-DD"
    const formattedDate = `${year}-${month < 10 ? '0' : ''}${month}-${day < 10 ? '0' : ''}${day}`;

    try {
        if (yearToDelete == currentYear) {
            await Master.destroy({
                //where: literal(`STR_TO_DATE(stay_date, '%d/%m/%Y') >= ${formattedDate}`),
                where: literal(`YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) >= ${yearToDelete}`),
            });
        } else {
            await Master.destroy({
                where: literal(`YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${yearToDelete}`),
            });
        }

        console.log(`Data tahun ${yearToDelete} dihapus.`);
    } catch (error) {
        console.error(error);
    }
};

class csvController {
    static async uploadCsv(req, res) {

        console.log("<< masuk controller >> ")

        const location = req.body.location;
        const username = req.body.username;
        const filename = req.body.csvfile.filename.split('.').slice(0, -1).join('.');
        const uriFile = process.env.UPLOAD_CSV + req.body.csvfile.filename;

        const currDate = new Date();
        const current_date = currDate.getFullYear() + "-" + (currDate.getMonth() + 1) + "-" + currDate.getDate();
        const current_time = currDate.getHours() + ":" + currDate.getMinutes() + ":" + currDate.getSeconds();
        const date_time = current_date + " " + current_time;

        await deleteRecords(filename)


        let stream = fs.createReadStream(uriFile);
        let csvDataColl = [];

        let fileStream = csv
            .parse({
                headers: true,
            })
            .on("data", function (data) {

                // Membaca data dari kolom yang sesuai dalam CSV berdasarkan mapping
                const mappedData = {};
                for (const csvColumn in columnMapping) {
                    if (data.hasOwnProperty(csvColumn)) {
                        mappedData[columnMapping[csvColumn]] = data[csvColumn];
                    }
                }

                // Trim kolom "Email" di sini
                if (mappedData.hasOwnProperty('email')) {
                    mappedData.email = mappedData.email.trim();
                }

                csvDataColl.push(mappedData);
            })
            .on("end", async function () {

                try {

                    // Memasukkan data yang telah dipilih dari CSV ke tabel
                    const batchSize = 1000;
                    const dataToInsert = csvDataColl.map(row => ({
                        ...row,
                        region: location,
                        createdAt: date_time,
                        createdBy: username,
                        updatedAt: date_time,
                        updatedBy: username,
                    }));

                    //proses data berdasarkan batch size
                    for (let i = 0; i < dataToInsert.length; i += batchSize) {
                        const batch = dataToInsert.slice(i, i + batchSize);
                        await Master.bulkCreate(batch);
                    }

                    res.status(200).json({ msg: "Import csv success." })
                } catch (error) {
                    console.error(error);
                }

                fs.unlinkSync(uriFile);
            });

        stream.pipe(fileStream);
    }

}

module.exports = csvController





