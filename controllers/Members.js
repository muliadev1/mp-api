const {
  Member,
  Company,
  MemberLog,
  ReedemsPoint,
  RedemsPointItem,
  Reedem,
  Master,
  EnrollsLog,
  MemberVoucher,
  ReturningInterval,
  Outlet,
  SecondPointLog,
  PointsLog,
  UpgradeLog,
  PointAdjustment,
  PointDailyTransaction,
  Dining,
  PointBuyPromo,
  Setting,
  MemberType,
  sequelize,
} = require('../db/models/index')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const Sequelize = require('sequelize')
const { Op, where, col, fn, literal } = require('sequelize')
const moment = require('moment-timezone');
const {
  calculateEarnPoint,
  calculateRoomPoint,
} = require('../utils/pointCalculator')

function convertUTCToLocal(utcDate, offset) {
  if (!(utcDate instanceof Date)) {
    throw new TypeError('The first argument must be a Date object.')
  }
  if (typeof offset !== 'number') {
    throw new TypeError('The second argument must be a number.')
  }

  // Convert the UTC time to the local time
  return new Date(utcDate.getTime() + offset * 60 * 60 * 1000)
}

function formatDateToYMD(date) {
  const year = date.getFullYear()
  const month = (date.getMonth() + 1).toString().padStart(2, '0') // Months are 0-indexed
  const day = date.getDate().toString().padStart(2, '0') // Add leading zero if needed
  return `${year}-${month}-${day}`
}

class MemberController {
  static async getMembers_pages(req, res, next) {
    //! Extract pagination parameters from request query
    //const { page, pageSize, sort, filter } = req.query
    let { page, pageSize, filter } = req.query

    //! Calculate starting index and number of records to retrieve
    const startIndex = (Number(page) - 1) * Number(pageSize)
    const endIndex = Number(page) * Number(pageSize)

    try {
      const members = await Member.findAndCountAll({
        attributes: [
          'id',
          'title',
          'first',
          'last',
          'primary_email',
          'member_id',
          'country',
          'city',
          'member_level',
          'joined_date',
          'exp_date',
        ],
        order: [['joined_date', 'DESC']],
        where: {
          is_deleted: 0, // Hanya ambil data yang is_deleted = 0
          ...(filter && {
            [Op.or]: [
              { first: { [Op.like]: `%${filter}%` } },
              { last: { [Op.like]: `%${filter}%` } },
              { primary_email: { [Op.like]: `%${filter}%` } },
              { member_id: { [Op.eq]: `${filter}` } },
              { country: { [Op.like]: `%${filter}%` } },
              { city: { [Op.like]: `%${filter}%` } },
              { member_level: { [Op.like]: `%${filter}%` } },
            ],
          }),
        },
        //order: sort ? [sort.split(' ')] : [],
        offset: startIndex,
        limit: Number(pageSize),
      })

      //! Retrieve total count of records for pagination info
      const totalCount = members.count
      //! Calculate total number of pages
      const totalPages = Math.ceil(totalCount / Number(pageSize))

      res.status(200).json({
        members: members,
        totalRecords: totalCount,
        currentPage: Number(page),
        totalPages: totalPages,
      })
    } catch (error) {
      console.log(error)
    }
  }

  static async getSelectMembers(req, res) {
    try {
      const members = await Member.findAll({
        attributes: [
          'member_id',
          [Sequelize.literal('CONCAT(first, " ", last)'), 'member_name'], // Menggabungkan first dan last menjadi member_name
        ],
        where: {
          is_deleted: 0, // Hanya ambil data yang is_deleted = 0
        },
      })
      res.status(200).json(members)
    } catch (error) {
      console.log(error)
    }
  }

  static async getMembertypes(req, res) {
    try {
      const membertypes = await MemberType.findAll({})
      res.status(200).json(membertypes)
    } catch (error) {
      console.log(error)
    }
  }

  static async getMembers(req, res, next) {
    const { page, per_page, filter } = req.query
    const perPageInt = parseInt(per_page, 10)
    const offset = (page - 1) * per_page

    try {
      // Lakukan query ke database menggunakan Sequelize
      const member = await Member.findAndCountAll({
        attributes: [
          'id',
          'title',
          'name_on_card',
          'first',
          'last',
          'primary_email',
          'member_id',
          'country',
          'city',
          'member_level',
          'available_points',
          'joined_date',
          'exp_date',
          'temp_password',
          'password',
          'createdAt',
        ],
        offset: offset,
        limit: perPageInt,

        where: {
          is_deleted: 0, // Hanya ambil data yang is_deleted = 0
          ...(filter && {
            [Op.or]: [
              { first: { [Op.like]: `%${filter}%` } },
              { last: { [Op.like]: `%${filter}%` } },
              { primary_email: { [Op.like]: `%${filter}%` } },
              { member_id: { [Op.eq]: `${filter}` } },
              { country: { [Op.like]: `%${filter}%` } },
              { city: { [Op.like]: `%${filter}%` } },
              { member_level: { [Op.like]: `%${filter}%` } },
            ],
          }),
        },
        order: [['joined_date', 'DESC']],
      })

      // Ekstrak data dan jumlah total dari hasil query
      const data = member.rows
      const totalCount = member.count

      res.status(200).json({
        data: data,
        totalData: totalCount,
      })
    } catch (error) {
      console.log(error)
    }
  }

  static async getMembersWcc(req, res, next) {
    try {
      // Lakukan query ke database menggunakan Sequelize
      const members = await Member.findAll({
        attributes: [
          [Sequelize.fn('TRIM', Sequelize.col('name_on_card')), 'member_name'],
          'member_id',
          [Sequelize.col('companies.company_name'), 'company_name'],
          [Sequelize.col('primary_email'), 'email_address'],
          [Sequelize.col('primary_phone'), 'phone_number'],
          [Sequelize.col('primary_mobilephone'), 'mobile_phone_number']
        ],
        include: [
          {
            model: Company,
            as: 'companies', // Sesuai alias dalam `associations`
            attributes: []
          }
        ],

        where: {
          member_type_id: 2, // only WCC members
          is_deleted: 0, // Hanya ambil data yang is_deleted = 0
        },
        order: [['name_on_card', 'ASC']],
      })

      res.status(200).json(members)
    } catch (error) {
      console.log(error)
    }
  }

  static async getDetailMemberWcc(req, res, next) {
    const memberId = req.params.memberId
    try {
      // Lakukan query ke database menggunakan Sequelize
      const members = await Member.findAll({
        attributes: [
          [Sequelize.fn('TRIM', Sequelize.col('name_on_card')), 'member_name'],
          'member_id',
          [Sequelize.col('companies.company_name'), 'company_name'],
          [Sequelize.col('primary_email'), 'email_address'],
          [Sequelize.col('primary_phone'), 'phone_number'],
          [Sequelize.col('primary_mobilephone'), 'mobile_phone_number']
        ],
        include: [
          {
            model: Company,
            as: 'companies', // Sesuai alias dalam `associations`
            attributes: []
          }
        ],

        where: {
          member_id: memberId,
          member_type_id: 2, // only WCC members
          is_deleted: 0, // Hanya ambil data yang is_deleted = 0
        },
        order: [['name_on_card', 'ASC']],
      })

      res.status(200).json(members)
    } catch (error) {
      console.log(error)
    }
  }

  static async getMembershipPoints(req, res, next) {
    const { page, per_page } = req.query
    const perPageInt = parseInt(per_page, 10)
    const offset = (page - 1) * per_page

    try {
      // Lakukan query ke database menggunakan Sequelize
      const member = await Member.findAndCountAll({
        attributes: [
          'id',
          'title',
          'name_on_card',
          'first',
          'last',
          'primary_email',
          'member_id',
          'country',
          'city',
          'member_level',
          'available_points',
          'joined_date',
          'exp_date',
          'temp_password',
          'password',
          'createdAt',
        ],
        offset: offset,
        limit: perPageInt,
        where: {
          is_deleted: 0,
          available_points: {
            [Op.not]: '0',
          },
        },
        // Anda dapat menambahkan pengurutan jika diperlukan
        order: [['joined_date', 'DESC']],
      })

      // Ekstrak data dan jumlah total dari hasil query
      const data = member.rows
      const totalCount = member.count

      res.status(200).json({
        data: data,
        totalData: totalCount,
      })
    } catch (error) {
      console.log(error)
    }
  }

  static async getSummaryPoints(req, res, next) {
    const { page, per_page } = req.query
    const perPageInt = parseInt(per_page, 10)
    const offset = (page - 1) * per_page

    const setting = await Setting.findAll({
      attributes: [
        'id',
        'room_amount',
        'room_point_silver',
        'room_point_gold',
        'room_point_diamond',
        'fb_amount',
        'fb_point_silver',
        'fb_point_gold',
        'fb_point_diamond',
        'hotel_pay_to_loyalty',
        'loyalty_pay_to_hotel',
        'system_profit_percentage',
        'system_profit_amount',
      ],
    })

    if (!setting[0])
      return res.status(400).json({ msg: 'Setting loyalty is not exist' })

    let var_HPL = setting[0].hotel_pay_to_loyalty
    let var_LPH = setting[0].loyalty_pay_to_hotel

    try {
      // Lakukan query ke database menggunakan Sequelize
      const member = await Member.findAndCountAll({
        attributes: [
          'id',
          'title',
          'name_on_card',
          'first',
          'last',
          'primary_email',
          'member_id',
          'country',
          'city',
          'member_level',
          'available_points',
          'joined_date',
          'exp_date',
          'temp_password',
          'password',
          'createdAt',
        ],
        offset: offset,
        limit: perPageInt,
        where: {
          is_deleted: 0,
          available_points: {
            [Op.not]: '0',
          },
        },
        // Anda dapat menambahkan pengurutan jika diperlukan
        order: [['joined_date', 'DESC']],
      })

      // Ekstrak data dan jumlah total dari hasil query
      const data = member.rows.map((member) => {
        let hotel_pay_loyalty = 0
        let loyalty_pay_hotel = 0
        let system_profit = 0

        hotel_pay_loyalty = member.available_points * var_HPL
        loyalty_pay_hotel = member.available_points * var_LPH

        // Lakukan pembulatan ke angka bulat
        hotel_pay_loyalty = Math.round(hotel_pay_loyalty)
        loyalty_pay_hotel = Math.round(loyalty_pay_hotel)

        system_profit = hotel_pay_loyalty - loyalty_pay_hotel

        // Kembalikan data member beserta nilai value_usd yang sudah dihitung
        return {
          ...member.get(), // Mengambil data dari instance Sequelize
          hotel_pay_loyalty,
          loyalty_pay_hotel,
          system_profit, // Tambahkan field value_usd yang dihitung
        }
      })
      const totalCount = member.count

      res.status(200).json({
        data: data,
        totalData: totalCount,
      })
    } catch (error) {
      console.log(error)
    }
  }

  static async getMembersLevel(req, res, next) {
    const { page, per_page, filter } = req.query
    const perPageInt = parseInt(per_page, 10)
    const offset = (page - 1) * perPageInt

    try {
      // Query untuk menghitung total room_usd dan fb_usd
      const totalUSDQuery = `
                SELECT
                    member_id,
                    SUM(room_usd) as total_room_usd,
                    SUM(fb_usd) as total_fb_usd
                FROM EnrollsLogs
                GROUP BY member_id
            `
      const [totalUSDResults] = await sequelize.query(totalUSDQuery)

      // Transform hasil ke dalam bentuk dictionary untuk akses cepat
      const totalsByMemberId = totalUSDResults.reduce((acc, result) => {
        acc[result.member_id] = result
        return acc
      }, {})

      // Query utama untuk mendapatkan data anggota
      const result = await Member.findAll({
        attributes: [
          'id',
          'member_id',
          'first',
          'last',
          'primary_email',
          'country',
          'city',
          'member_level',
          'joined_date',
          'exp_date',
        ],
        where: {
          is_deleted: 0, // Hanya ambil data yang is_deleted = 0
          member_type_id: 1, // only MPS
          ...(filter && {
            [Op.or]: [
              { member_id: { [Op.like]: `%${filter}%` } },
              { first: { [Op.like]: `%${filter}%` } },
              { last: { [Op.like]: `%${filter}%` } },
              { primary_email: { [Op.eq]: `${filter}` } },
              { country: { [Op.like]: `%${filter}%` } },
              { city: { [Op.like]: `%${filter}%` } },
              { member_level: { [Op.like]: `%${filter}%` } },
            ],
          }),
        },
        order: [['joined_date', 'DESC']],
      })

      // Tambahkan total_room_usd, total_fb_usd, dan next_level ke hasil anggota
      const data = result.map((member) => {
        const totals = totalsByMemberId[member.member_id] || {
          total_room_usd: 0,
          total_fb_usd: 0,
        }
        const totalRoomUSD = Number(totals.total_room_usd)
        let nextLevel = ''

        if (totalRoomUSD >= 20000) {
          if (member.member_level === 'MPS') {
            nextLevel = 'MPG'
          } else if (member.member_level === 'MPG') {
            nextLevel = 'MPD'
          }
        }

        return {
          ...member.toJSON(),
          total_room_usd: totalRoomUSD,
          total_fb_usd: totals.total_fb_usd,
          next_level: nextLevel !== member.member_level ? nextLevel : '',
        }
      })

      console.log(data, 'isi data >>>>')

      // Filter data untuk hanya menampilkan yang memiliki next_level 'MPD' atau 'MPG'
      const filteredData = data.filter(
        (member) => member.next_level === 'MPD' || member.next_level === 'MPG'
      )

      const totalData = filteredData.length

      res.status(200).json({
        data: filteredData, // Menggunakan filteredData
        totalData: totalData,
        limit: perPageInt,
        offset: offset,
      })
    } catch (error) {
      console.log(error)
      res
        .status(500)
        .json({ error: 'An error occurred while fetching members level.' })
    }
  }

  static async getMembersLevel_OLD(req, res, next) {
    const { page, per_page, filter } = req.query
    const perPageInt = parseInt(per_page, 10)
    const offset = (page - 1) * per_page

    try {
      const result = await Member.findAll({
        attributes: [
          'id',
          'member_id',
          'first',
          'last',
          'primary_email',
          'country',
          'city',
          'member_level',
          'joined_date',
          'exp_date',
          [
            Sequelize.literal(
              '(SELECT SUM(enrollLogs.room_usd) FROM EnrollsLogs AS enrollLogs WHERE enrollLogs.member_id = Member.member_id)'
            ),
            'total_room_usd',
          ],
          [
            Sequelize.literal(
              '(SELECT SUM(enrollLogs.fb_usd) FROM EnrollsLogs AS enrollLogs WHERE enrollLogs.member_id = Member.member_id)'
            ),
            'total_fb_usd',
          ],
          // Tambahkan kolom Next Level di sini
          [
            Sequelize.literal(`
                        CASE 
                            WHEN SUM((SELECT SUM(enrollLogs.room_usd) FROM EnrollsLogs AS enrollLogs WHERE enrollLogs.member_id = Member.member_id)) >= 10000 AND SUM((SELECT SUM(enrollLogs.room_usd) FROM EnrollsLogs AS enrollLogs WHERE enrollLogs.member_id = Member.member_id)) < 19999 AND member_level = 'MPS' THEN 'MPG'
                            WHEN SUM((SELECT SUM(enrollLogs.room_usd) FROM EnrollsLogs AS enrollLogs WHERE enrollLogs.member_id = Member.member_id)) >= 10000 AND SUM((SELECT SUM(enrollLogs.room_usd) FROM EnrollsLogs AS enrollLogs WHERE enrollLogs.member_id = Member.member_id)) < 19999 AND member_level = 'MPG' THEN ''
                            WHEN SUM((SELECT SUM(enrollLogs.room_usd) FROM EnrollsLogs AS enrollLogs WHERE enrollLogs.member_id = Member.member_id)) >= 20000 AND member_level = 'MPS' THEN 'MPD'
                            WHEN SUM((SELECT SUM(enrollLogs.room_usd) FROM EnrollsLogs AS enrollLogs WHERE enrollLogs.member_id = Member.member_id)) >= 20000 AND (member_level = 'MPG' OR member_level = 'MPD') THEN 'MPD'
                            ELSE ''
                        END
                    `),
            'next_level',
          ],
        ],
        include: [
          {
            model: EnrollsLog,
            as: 'enrollLogs',
            attributes: [],
            required: true,
          },
        ],
        where: {
          is_deleted: 0, // Hanya ambil data yang is_deleted = 0
          ...(filter && {
            [Op.or]: [
              { member_id: { [Op.like]: `%${filter}%` } },
              { first: { [Op.like]: `%${filter}%` } },
              { last: { [Op.like]: `%${filter}%` } },
              { primary_email: { [Op.eq]: `${filter}` } },
              { country: { [Op.like]: `%${filter}%` } },
              { city: { [Op.like]: `%${filter}%` } },
              { member_level: { [Op.like]: `%${filter}%` } },
            ],
          }),
        },
        group: [
          'id',
          'member_id',
          'first',
          'last',
          'primary_email',
          'country',
          'city',
          'member_level',
          'joined_date',
          'exp_date',
        ],
        order: [['joined_date', 'DESC']],
        having: {
          // Menggunakan HAVING untuk menampilkan hanya data dengan count mp = 1 dan count confirmation_no = 1
          [Op.and]: [
            Sequelize.literal('total_room_usd > 10000'),
            Sequelize.literal(
              "next_level IS NOT NULL AND next_level <> '' AND next_level <> member_level"
            ),
          ],
        },
        limit: perPageInt,
        offset: offset,
      })

      // Query untuk mendapatkan totalData
      const totalDataResult = await Member.findAll({
        attributes: [
          'id',
          'member_id',
          'first',
          'last',
          'primary_email',
          'country',
          'city',
          'member_level',
          'joined_date',
          'exp_date',
          [
            Sequelize.literal(
              '(SELECT SUM(enrollLogs.room_usd) FROM EnrollsLogs AS enrollLogs WHERE enrollLogs.member_id = Member.member_id)'
            ),
            'total_room_usd',
          ],
          [
            Sequelize.literal(
              '(SELECT SUM(enrollLogs.fb_usd) FROM EnrollsLogs AS enrollLogs WHERE enrollLogs.member_id = Member.member_id)'
            ),
            'total_fb_usd',
          ],
          // Tambahkan kolom Next Level di sini
          [
            Sequelize.literal(`
                        CASE 
                        WHEN SUM((SELECT SUM(enrollLogs.room_usd) FROM EnrollsLogs AS enrollLogs WHERE enrollLogs.member_id = Member.member_id)) >= 10000 AND SUM((SELECT SUM(enrollLogs.room_usd) FROM EnrollsLogs AS enrollLogs WHERE enrollLogs.member_id = Member.member_id)) < 19999 AND member_level = 'MPS' THEN 'MPG'
                        WHEN SUM((SELECT SUM(enrollLogs.room_usd) FROM EnrollsLogs AS enrollLogs WHERE enrollLogs.member_id = Member.member_id)) >= 10000 AND SUM((SELECT SUM(enrollLogs.room_usd) FROM EnrollsLogs AS enrollLogs WHERE enrollLogs.member_id = Member.member_id)) < 19999 AND member_level = 'MPG' THEN ''
                        WHEN SUM((SELECT SUM(enrollLogs.room_usd) FROM EnrollsLogs AS enrollLogs WHERE enrollLogs.member_id = Member.member_id)) >= 20000 AND member_level = 'MPS' THEN 'MPD'
                        WHEN SUM((SELECT SUM(enrollLogs.room_usd) FROM EnrollsLogs AS enrollLogs WHERE enrollLogs.member_id = Member.member_id)) >= 20000 AND (member_level = 'MPG' OR member_level = 'MPD') THEN 'MPD'
                        ELSE ''
                        END
                    `),
            'next_level',
          ],
        ],
        include: [
          {
            model: EnrollsLog,
            attributes: [],
            required: true,
            as: 'enrollLogs',
          },
        ],
        where: filter
          ? {
            [Op.or]: [
              {
                member_id: {
                  [Op.like]: `%${filter}%`,
                },
              },
              {
                first: {
                  [Op.like]: `%${filter}%`,
                },
              },
              {
                last: {
                  [Op.like]: `%${filter}%`,
                },
              },
              {
                primary_email: {
                  [Op.eq]: `${filter}`,
                },
              },
              {
                country: {
                  [Op.like]: `%${filter}%`,
                },
              },
              {
                city: {
                  [Op.like]: `%${filter}%`,
                },
              },
              {
                member_level: {
                  [Op.like]: `%${filter}%`,
                },
              },
            ],
          }
          : {},
        group: [
          'id',
          'member_id',
          'first',
          'last',
          'primary_email',
          'country',
          'city',
          'member_level',
          'joined_date',
          'exp_date',
        ],
        order: [['joined_date', 'DESC']],
        having: {
          // Menggunakan HAVING untuk menampilkan hanya data dengan count mp = 1 dan count confirmation_no = 1
          [Op.and]: [
            Sequelize.literal('total_room_usd > 10000'),
            Sequelize.literal(
              "next_level IS NOT NULL AND next_level <> '' AND next_level <> member_level"
            ),
          ],
        },
      })

      const totalData = totalDataResult ? totalDataResult.length : 0

      res.status(200).json({
        data: result,
        totalData: result.length,
      })
    } catch (error) {
      console.log(error)
    }
  }

  static async getMembersLevelDetail(req, res, next) {
    var memberId = req.params.memberId
    try {
      const result = await Member.findAll({
        attributes: [
          'id',
          'member_id',
          'first',
          'last',
          'primary_email',
          'country',
          'city',
          'member_level',
          'joined_date',
          'exp_date',
          'available_points',
          'region',
          [
            Sequelize.fn('SUM', Sequelize.col('enrollLogs.room_usd')),
            'roomUSD',
          ],
          [Sequelize.fn('SUM', Sequelize.col('enrollLogs.fb_usd')), 'fbUSD'],
        ],
        include: [
          {
            model: EnrollsLog,
            attributes: [],
            required: true,
            as: 'enrollLogs',
          },
        ],
        where: {
          member_id: memberId,
          is_deleted: 0,
        },
        group: [
          'id',
          'member_id',
          'first',
          'last',
          'primary_email',
          'country',
          'city',
          'member_level',
          'joined_date',
          'exp_date',
          'available_points',
          'region',
        ],
        order: [['joined_date', 'DESC']],
      })

      res.status(200).json(result)
    } catch (error) {
      console.log(error)
    }
  }

  static async xxxgetMembers(req, res, next) {
    try {
      const member = await Member.findAll({
        attributes: [
          'id',
          'title',
          'name_on_card',
          'first',
          'last',
          'primary_email',
          'member_id',
          'country',
          'city',
          'member_level',
          'joined_date',
          'exp_date',
          'temp_password',
          'password',
        ],
        order: [['joined_date', 'DESC']],
      })
      res.status(200).json(member)
    } catch (error) {
      console.log(error)
    }
  }

  static async getListTierProperty(req, res) {
    let { region } = req.query

    try {
      const member = await Member.findAll({
        attributes: [
          'member_level',
          [Sequelize.fn('COUNT', Sequelize.col('member_id')), 'jumlah_member'],
        ],
        where: {
          is_deleted: 0,
          region: region,
          member_level: {
            [Op.ne]: '',
          },
        },
        group: ['member_level'],
      })

      // Map member_level values to their corresponding names
      const translatedMemberLevels = member.map((row) => {
        const memberLevel = row.getDataValue('member_level')
        let translatedLevelName = ''

        // Map member_level values to names
        switch (memberLevel) {
          case 'MPS':
            translatedLevelName = 'Mulia Privileges Silver'
            break
          case 'MPG':
            translatedLevelName = 'Mulia Privileges Gold'
            break
          case 'MPD':
            translatedLevelName = 'Mulia Privileges Diamond'
            break
          // Add more cases for other member_level values if needed

          default:
            translatedLevelName = 'Unknown'
            break
        }

        // Return an object with the translated name and jumlah_member
        return {
          member_level: row.getDataValue('member_level'),
          member_level_name: translatedLevelName,
          jumlah_member: row.getDataValue('jumlah_member'),
        }
      })

      res.status(200).json(translatedMemberLevels)
    } catch (error) {
      console.log(error)
    }
  }

  static async getListDetailTierProperty(req, res) {
    let { level, region, page, pageSize } = req.query

    // Convert page and pageSize to integers
    page = parseInt(page)
    pageSize = parseInt(pageSize)

    // Calculate offset based on the current page and pageSize
    const offset = (page - 1) * pageSize

    try {
      const member = await Member.findAndCountAll({
        attributes: [
          'id',
          'name_on_card',
          'first',
          'last',
          'birth_date',
          'primary_email',
          'member_id',
          'member_level',
          'exp_level_date',
          'joined_date',
          'exp_date',
          'available_points',
          'region',
        ],
        where: {
          is_deleted: 0,
          region: region,
          member_level: level,
        },
        order: [['joined_date', 'DESC']],
        limit: pageSize, // Limit the number of records per page
        offset: offset,
      })

      // Prepare the pagination response
      const response = {
        totalItems: member.count,
        totalPages: Math.ceil(member.count / pageSize),
        currentPage: page,
        pageSize: pageSize,
        members: member.rows,
      }

      res.status(200).json(response)

      // res.status(200).json(member);
    } catch (error) {
      console.log(error)
    }
  }

  static async getPasswords(req, res, next) {
    const { page, per_page, filter } = req.query
    const perPageInt = parseInt(per_page, 10)
    const offset = (page - 1) * per_page

    try {
      // Lakukan query ke database menggunakan Sequelize
      const member = await Member.findAndCountAll({
        attributes: [
          'id',
          'name_on_card',
          'primary_email',
          'temp_password',
          'password',
        ],
        offset: offset,
        limit: perPageInt,
        where: {
          is_deleted: 0, // Hanya ambil data yang is_deleted = 0
          ...(filter && {
            [Op.or]: [
              { name_on_card: { [Op.like]: `%${filter}%` } },
              { primary_email: { [Op.like]: `%${filter}%` } },
              { temp_password: { [Op.like]: `%${filter}%` } },
              { password: { [Op.eq]: `${filter}` } },
            ],
          }),
        },
        order: [['name_on_card', 'DESC']],
      })

      // Ekstrak data dan jumlah total dari hasil query
      const data = member.rows
      const totalCount = member.count

      res.status(200).json({
        data: data,
        totalData: totalCount,
      })
    } catch (error) {
      console.log(error)
    }
  }

  static async xxxgetPasswords(req, res, next) {
    try {
      const member = await Member.findAll({
        attributes: [
          'id',
          'name_on_card',
          'primary_email',
          'temp_password',
          'password',
        ],
        order: [['name_on_card', 'ASC']],
      })
      res.status(200).json(member)
    } catch (error) {
      console.log(error)
    }
  }

  static async Login(req, res) {
    try {
      const member = await Member.findAll({
        attributes: [
          'id',
          'first',
          'last',
          'member_id',
          'source',
          'primary_email',
          'password',
          'primary_mobilephone',
          'available_points',
          'is_active',
          'is_deleted',
          'activate_date',
          'member_type_id',
        ],
        where: {
          is_deleted: 0,
          primary_email: req.body.email,
        },
      })

      // Check if member exists
      if (member.length === 0) {
        return res.status(404).json({ msg: 'Email address not found' })
      }

      // Check if account is active
      if (member[0].is_active === 0) {
        return res.status(403).json({ msg: 'Your account is not activated!' })
      }

      const match = await bcrypt.compare(req.body.password, member[0].password)

      if (!match) return res.status(400).json({ msg: 'Wrong Password' })
      const idm = member[0].dataValues.id
      const first_name = member[0].first
      const last_name = member[0].last
      const email = member[0].primary_email
      const memberId = member[0].member_id
      const points = member[0].available_points
      const source = member[0].source
      const member_type_id = member[0].member_type_id

      const accessToken = jwt.sign(
        { memberId, first_name, last_name, email },
        process.env.ACCESS_TOKEN_SECRET,
        {
          expiresIn: '20s',
        }
      )
      const refreshToken = jwt.sign(
        { memberId, first_name, last_name, email },
        process.env.REFRESH_TOKEN_SECRET,
        {
          expiresIn: '1d',
        }
      )
      await Member.update(
        { refresh_token: accessToken },
        {
          where: {
            id: idm,
          },
        }
      )
      res.cookie('refreshTokenMember', accessToken, {
        httpOnly: true,
        maxAge: 24 * 60 * 60 * 1000,
        // secure: true //ini digunakan ketika menggunakan https
      })
      res.json({
        accessToken,
        idm,
        memberId,
        first_name,
        last_name,
        email,
        points,
        source,
        member_type_id,
      })
    } catch (error) {
      res.status(404).json({ msg: 'Email address not found!' })
    }
  }

  static async changePassword(req, res) {
    var memberId = req.params.id
    const { current_password, new_password, confirm_password } = req.body
    const member = await Member.findAll({
      attributes: [
        'id',
        'first',
        'last',
        'primary_email',
        'password',
        'primary_mobilephone',
      ],
      where: {
        id: memberId,
      },
    })

    //check apakah current password match
    const match = await bcrypt.compare(current_password, member[0].password)
    if (!match)
      return res.status(400).json({ msg: 'Current password not match' })
    //check password and confirm password is match
    if (new_password !== confirm_password)
      return res
        .status(400)
        .json({ msg: 'Password not match with Confirm password' })
    const salt = await bcrypt.genSalt()
    const hashPassword = await bcrypt.hash(new_password, salt)

    try {
      await Member.update(
        {
          temp_password: confirm_password,
          password: hashPassword,
        },
        {
          where: {
            id: memberId,
          },
        }
      )

      res.json({ msg: 'Update password is successfully' })
    } catch (error) {
      console.log(error)
    }
  }

  static async getProfile(req, res) {
    const memberId = req.params.id
    try {
      const member = await Member.findOne({
        attributes: [
          'id',
          'name_on_card',
          'member_id',
          'member_level',
          'title',
          'first',
          'last',
          'gender',
          'birth_date',
          'primary_email',
          'primary_mobilephone',
          'city',
          'country',
          'joined_date',
          'exp_date',
          'exp_level_date',
          'available_points',
        ],
        where: {
          id: memberId,
        },
      })
      res.status(200).json(member)
    } catch (error) {
      console.log(error)
    }
  }

  static async getProfileByName(req, res) {
    const firstName = req.params.firstName
    const lastName = req.params.lastName
    try {
      const member = await Member.findOne({
        attributes: [
          'id',
          'name_on_card',
          'member_id',
          'member_level',
          'title',
          'first',
          'last',
          'gender',
          'birth_date',
          'primary_email',
          'primary_mobilephone',
          'city',
          'country',
          'joined_date',
          'available_points',
        ],
        where: {
          first: firstName,
          last: lastName,
        },
      })
      res.status(200).json(member)
    } catch (error) {
      console.log(error)
    }
  }

  static async getListMembersByName(req, res) {
    const filter = req.params.filter
    try {
      const members = await Member.findAll({
        attributes: ['member_id', ['name_on_card', 'member_name']],
        where: {
          is_deleted: 0, // Hanya ambil data yang is_deleted = 0
          ...(filter && {
            [Op.or]: [
              { name_on_card: { [Op.like]: `%${filter}%` } },
              { member_id: { [Op.like]: `%${filter}%` } },
              { primary_email: { [Op.like]: `%${filter}%` } },
            ],
          }),
        },
      })
      res.status(200).json(members)
    } catch (error) {
      console.log(error)
    }
  }

  static async getProfileByEmail(req, res) {
    const email = req.params.email
    try {
      const member = await Member.findOne({
        attributes: [
          'id',
          'name_on_card',
          'member_id',
          'member_level',
          'title',
          'first',
          'last',
          'birth_date',
          'primary_email',
          'primary_mobilephone',
          'city',
          'country',
          'joined_date',
          'available_points',
          'is_first_enroll',
          'is_active',
          'activate_date',
        ],
        where: {
          primary_email: email,
        },
      })
      res.status(200).json(member)
    } catch (error) {
      console.log(error)
    }
  }

  static async getListEnroll(req, res) {
    let { dateFrom, dateTo, username } = req.query

    try {
      // Pecah username menjadi array jika ada lebih dari satu username
      const usernames = username ? username.split(',') : []

      // Define the first query

      const resultQuery = `
      SELECT m.member_id, m.name_on_card, m.member_level, m.title, m.first, m.last, 
      m.birth_date, m.primary_email, m.primary_mobilephone, m.primary_phone, m.city, m.country, 
m.joined_date, SUM(CASE WHEN e.is_new_enroll = 1 THEN e.room_idr ELSE 0 END) AS room_idr_first, 
SUM(CASE WHEN e.is_new_enroll = 1 THEN e.fb_idr ELSE 0 END) AS fb_idr_first, 
SUM(CASE WHEN e.is_new_enroll = 1 THEN e.room_usd ELSE 0 END) AS room_usd_first, 
SUM(CASE WHEN e.is_new_enroll = 1 THEN e.fb_usd ELSE 0 END) AS fb_usd_first, 
SUM(CASE WHEN e.is_new_enroll = 1 THEN e.total_points ELSE 0 END) AS total_points_first, 
SUM(e.room_usd) AS room_usd, SUM(e.fb_usd) AS fb_usd, m.available_points, 
m.notifications, e.createdBy, e.createdAt 
FROM Members m INNER join
(select * from EnrollsLogs e where e.createdBy IN ('${usernames.join(
        "', '"
      )}') and DATE_FORMAT(e.createdAt, '%Y-%m-%d') BETWEEN '${dateFrom}' AND '${dateTo}') 
AS e ON m.member_id = e.member_id 
GROUP BY m.member_id, m.name_on_card, m.member_level, m.title, m.first, m.last,
m.birth_date, m.primary_email, m.primary_mobilephone, m.city, m.country, m.joined_date, 
m.available_points, m.notifications, 
e.createdBy, e.createdAt`

      const query1 = await sequelize.query(resultQuery, {
        type: sequelize.QueryTypes.SELECT,
      })

      console.log(query1, '>>>>>>>>> isi query 1>>>>>>>>>>>')

      // Define the second query (initial points)
      const query2 = Member.findAll({
        attributes: [
          'member_id',
          'name_on_card',
          'member_level',
          'title',
          'first',
          'last',
          'birth_date',
          'primary_email',
          'primary_mobilephone',
          'primary_phone',
          'city',
          'country',
          'joined_date',
          [Sequelize.literal('0'), 'room_idr_first'],
          [Sequelize.literal('0'), 'fb_idr_first'],
          [Sequelize.literal('0'), 'room_usd_first'],
          [Sequelize.literal('0'), 'fb_usd_first'],
          [Sequelize.literal('0'), 'total_points_first'],
          [Sequelize.literal('0'), 'room_usd'], // Add a virtual column 'room_usd' with value 0
          [Sequelize.literal('0'), 'fb_usd'], // Add a virtual column 'fb_usd' with value 0
          'available_points',
          'notifications',
          'createdBy',
          'createdAt',
        ],
        where: {
          [Op.and]: [
            where(fn('DATE_FORMAT', col('createdAt'), '%Y-%m-%d'), {
              [Op.between]: [dateFrom, dateTo],
            }),
            ...(usernames.length > 0
              ? [{ createdBy: { [Op.in]: usernames } }]
              : []),
            { total_points: { [Op.gte]: 500 } },
          ],
        },
      })

      // Execute both queries concurrently
      Promise.all([query1, query2])
        .then(([result1, result2]) => {
          // Merge the results if necessary
          const combinedResult = result1.concat(result2)
          res.status(200).json(combinedResult)
        })
        .catch((error) => {
          console.log(error)
          res.status(500).json({ error: 'Internal server error' })
        })

      //res.status(200).json(member);
    } catch (error) {
      console.log(error)
    }
  }

  static async getListScanner(req, res) {
    let { scannerDate, username } = req.query

    try {
      const scanner = await MemberVoucher.findAll({
        attributes: [
          'id',
          'reedem_id',
          'reedem_name',
          'member_id',

          'voucher_no',
          'date_expired',
          'is_used',
          'createdBy',
          'updatedBy',
          [
            Sequelize.fn(
              'DATE_FORMAT',
              Sequelize.col('MemberVoucher.createdAt'),
              '%m/%d/%Y'
            ),
            'createdAt',
          ],
          [
            Sequelize.fn(
              'DATE_FORMAT',
              Sequelize.col('MemberVoucher.updatedAt'),
              '%m/%d/%Y'
            ),
            'updatedAt',
          ],
        ],
        include: [
          {
            model: Member,
            as: 'members',
            attributes: ['name_on_card', 'first', 'last'],
          },
        ],
        where: {
          [Op.and]: [
            where(
              fn('DATE_FORMAT', col('MemberVoucher.updatedAt'), '%Y-%m-%d'),
              '=',
              scannerDate
            ),
            { updatedBy: username },
          ],
        },
        order: [['createdAt', 'DESC']],
      })

      res.status(200).json(scanner)
    } catch (error) {
      console.log(error)
    }
  }

  static async resetPointByMemberId(req, res) {
    const memberId = req.params.memberId
    const {
      total_points,
      reedem_points,
      available_points,
      is_reset,
      username,
    } = req.body

    console.log(req.body, '>>>>>>>>> ISI passing data >>>>>>>>>>>>>>')

    // using UTC time
    const nowUTC = new Date() // Current UTC time
    const offset = 8 // Bali (WITA) is UTC+8
    const localDateTime = convertUTCToLocal(nowUTC, offset)

    //cek apakah member sdh ada
    const member = await Member.findAll({
      attributes: [
        'member_id',
        'name_on_card',
        'first',
        'last',
        'primary_email',
        'city',
        'country',
      ],
      where: {
        member_id: memberId,
      },
    })

    if (!member[0]) return res.status(400).json({ msg: 'Member is not exist' })

    try {
      await Member.update(
        {
          total_points: total_points,
          reedem_points: reedem_points,
          available_points: available_points,
          is_reset: is_reset,
          updatedAt: localDateTime,
          updatedBy: username,
        },
        {
          where: {
            member_id: memberId,
          },
        }
      )

      res.json({ msg: 'Reset point is successfully' })
    } catch (error) {
      console.log(error)
    }
  }

  static async getListPoints(req, res) {
    try {
      const member = await Member.findAll({
        attributes: [
          'name_on_card',
          'first',
          'last',
          'primary_email',
          'member_id',
          'member_level',
          'available_points',
          'exp_date',
        ],
        where: {
          available_points: {
            [Op.not]: '0',
          },
        },
        // Anda dapat menambahkan pengurutan jika diperlukan
        order: [['name_on_card', 'ASC']],
      })

      res.status(200).json(member)
    } catch (error) {
      console.log(error)
    }
  }

  static async getListExpiryDate(req, res) {
    let { expDate } = req.query

    try {
      const today = new Date() // Get the current date

      // If expDate is provided, ensure it is in the YYYY-MM-DD format
      if (expDate) {
        expDate = new Date(expDate) // Parse expDate as a Date object
      }

      // format the expired date as YYYY-MM-DD
      expDate = expDate ? expDate.toISOString().split('T')[0] : null

      // Format the current date as YYYY-MM-DD
      const formattedToday = today.toISOString().split('T')[0]

      // Use Sequelize.literal to apply the MySQL DATE() function to the exp_date field
      const whereClause = {
        is_reset: 0, // Add condition for is_reset=0
        [Op.or]: [
          Sequelize.where(
            Sequelize.fn('DATE', Sequelize.col('exp_date')),
            expDate
          ), // Exact match to expDate
          {
            exp_date: {
              [Op.lte]: formattedToday, // Records with exp_date less than or equal to today
            },
          },
        ],
      }

      const member = await Member.findAll({
        attributes: [
          'member_id',
          'title',
          'first',
          'last',
          'primary_email',
          'country',
          'city',
          'member_level',
          'joined_date',
          'exp_date',
          'available_points',
        ],
        where: whereClause,
        order: [[Sequelize.col('exp_date'), 'DESC']],
      })
      res.status(200).json(member)
    } catch (error) {
      console.log(error)
    }
  }

  static async getListBirthday(req, res) {
    let { dateFrom, dateTo } = req.query

    const dateObjFrom = new Date(dateFrom)
    const monthFrom = dateObjFrom.getMonth() + 1
    const dayFrom = dateObjFrom.getDate()

    const dateObjTo = new Date(dateTo)
    const monthTo = dateObjTo.getMonth() + 1
    const dayTo = dateObjTo.getDate()

    let whereClause = {}

    whereClause = {
      [Op.or]: [
        {
          [Op.and]: [
            Sequelize.where(
              Sequelize.fn('MONTH', Sequelize.col('birth_date')),
              monthFrom
            ),
            Sequelize.where(Sequelize.fn('DAY', Sequelize.col('birth_date')), {
              [Op.between]: [dayFrom, dayTo],
            }),
          ],
        },
        {
          [Op.and]: [
            Sequelize.where(
              Sequelize.fn('MONTH', Sequelize.col('birth_date')),
              monthTo
            ),
            Sequelize.where(Sequelize.fn('DAY', Sequelize.col('birth_date')), {
              [Op.between]: [dayFrom, dayTo],
            }),
          ],
        },
      ],
    }

    try {
      const members = await Member.findAll({
        attributes: [
          'member_id',
          'title',
          'first',
          'last',
          'primary_email',
          'country',
          'city',
          'member_level',
          'joined_date',
          'exp_date',
          'available_points',
          'birth_date',
        ],
        where: whereClause,
        order: [[Sequelize.col('birth_date'), 'DESC']],
      })
      res.status(200).json(members)
    } catch (error) {
      console.log(error)
    }
  }

  static async getSummaryDailyTransaction(req, res) {
    let { dateFrom, dateTo } = req.query

    // Konversi ke format yang tepat di zona waktu Jakarta
    const startDate = moment.tz(dateFrom, 'Asia/Jakarta').startOf('day').toDate();
    const endDate = moment.tz(dateTo, 'Asia/Jakarta').endOf('day').toDate();

    try {
      const enrolls = await PointDailyTransaction.findAll({
        attributes: [
          'inquiry_date',
          'opening_balance',
          'new_enroll_bali',
          'new_enroll_jakarta',
          'existing_enroll_bali',
          'existing_enroll_jakarta',
          'purchase_point_bali',
          'purchase_point_jakarta',
          'bonus_point_bali',
          'bonus_point_jakarta',
          'dining_point_bali',
          'dining_point_jakarta',
          'redeem_point_bali',
          'redeem_point_jakarta',
          'closing_balance',
        ],
        where: {
          inquiry_date: {
            [Op.between]: [startDate, endDate]
        }
        },
        order: [[Sequelize.col('inquiry_date'), 'ASC']],
      })
      res.status(200).json(enrolls)
    } catch (error) {
      console.log(error)
    }
  }

  static async getSummaryPointCalculation(req, res) {
    const setting = await Setting.findAll({
      attributes: [
        'id',
        'room_amount',
        'room_point_silver',
        'room_point_gold',
        'room_point_diamond',
        'fb_amount',
        'fb_point_silver',
        'fb_point_gold',
        'fb_point_diamond',
        'hotel_pay_to_loyalty',
        'loyalty_pay_to_hotel',
        'system_profit_percentage',
        'system_profit_amount',
      ],
    })

    if (!setting[0])
      return res.status(400).json({ msg: 'Setting loyalty is not exist' })

    let var_HPL = setting[0].hotel_pay_to_loyalty
    let var_LPH = setting[0].loyalty_pay_to_hotel

    try {
      const points = await Member.findAll({
        attributes: [
          'id',
          'title',
          'name_on_card',
          'first',
          'last',
          'primary_email',
          'member_id',
          'country',
          'city',
          'member_level',
          'available_points',
          'joined_date',
          'exp_date',
        ],
        where: {
          available_points: {
            [Op.not]: '0',
          },
        },
        // Anda dapat menambahkan pengurutan jika diperlukan
        order: [['joined_date', 'DESC']],
      })

      // Proses data untuk menambahkan 2 kolom baru
      const processedPoints = points.map((member) => {
        let hotel_pay_loyalty = 0
        let loyalty_pay_hotel = 0
        let system_profit = 0

        // Hitung nilai hotel_pay_to_loyalty dan loyalty_pay_to_hotel
        hotel_pay_loyalty = var_HPL * member.available_points
        loyalty_pay_hotel = var_LPH * member.available_points

        // Lakukan pembulatan ke angka bulat
        hotel_pay_loyalty = Math.round(hotel_pay_loyalty)
        loyalty_pay_hotel = Math.round(loyalty_pay_hotel)

        system_profit = hotel_pay_loyalty - loyalty_pay_hotel

        // Return data member dengan 2 kolom tambahan
        return {
          ...member.get(), // Mengambil data dari instance Sequelize
          hotel_pay_loyalty, // Tambahkan nilai yang sudah dihitung
          loyalty_pay_hotel, // Tambahkan nilai yang sudah dihitung
          system_profit,
        }
      })

      res.status(200).json(processedPoints)
    } catch (error) {
      console.log(error)
    }
  }

  static async getMemberReturnBack(req, res) {
    const { page, per_page, filter } = req.query
    const perPageInt = parseInt(per_page, 10)
    const offset = (page - 1) * per_page

    try {
      const master = await Master.findAndCountAll({
        attributes: [
          [col('mp'), 'member_id'],
          [col('members.first'), 'first_name'],
          [col('members.last'), 'last_name'],
          [col('members.name_on_card'), 'name_on_card'],
          [fn('STR_TO_DATE', col('arrival_date'), '%d/%m/%Y'), 'arrival_date'],
          [
            fn('STR_TO_DATE', col('departure_date'), '%d/%m/%Y'),
            'departure_date',
          ],
          [
            fn('DATE_SUB', fn('CURDATE'), literal('INTERVAL 3 MONTH')),
            'date_after_3_months',
          ],
          [
            fn('DATE_SUB', fn('CURDATE'), literal('INTERVAL 6 MONTH')),
            'date_after_6_months',
          ],
          [
            fn('DATE_SUB', fn('CURDATE'), literal('INTERVAL 9 MONTH')),
            'date_after_9_months',
          ],
          [
            literal(
              `SUM(CASE WHEN STR_TO_DATE(arrival_date, '%d/%m/%Y') >= DATE_SUB(CURDATE(), INTERVAL 3 MONTH) THEN 1 ELSE 0 END) OVER (PARTITION BY mp)`
            ),
            'visits_last_3_months',
          ],
          [
            literal(
              `SUM(CASE WHEN STR_TO_DATE(arrival_date, '%d/%m/%Y') > DATE_SUB(CURDATE(), INTERVAL 3 MONTH) AND STR_TO_DATE(arrival_date, '%d/%m/%Y') <= DATE_SUB(CURDATE(), INTERVAL 6 MONTH) THEN 1 ELSE 0 END) OVER (PARTITION BY mp)`
            ),
            'visits_last_6_months',
          ],
          [
            literal(
              `SUM(CASE WHEN STR_TO_DATE(arrival_date, '%d/%m/%Y') > DATE_SUB(CURDATE(), INTERVAL 6 MONTH) AND STR_TO_DATE(arrival_date, '%d/%m/%Y') <= DATE_SUB(CURDATE(), INTERVAL 9 MONTH) THEN 1 ELSE 0 END) OVER (PARTITION BY mp)`
            ),
            'visits_last_9_months',
          ],
        ],
        include: [
          {
            model: Member,
            as: 'members',
            attributes: [],
            required: true,
            where: filter
              ? {
                [Op.or]: [
                  {
                    first: {
                      [Op.like]: `%${filter}%`,
                    },
                  },
                  {
                    last: {
                      [Op.like]: `%${filter}%`,
                    },
                  },
                  {
                    name_on_card: {
                      [Op.like]: `%${filter}%`,
                    },
                  },
                  {
                    member_id: {
                      [Op.eq]: `${filter}`,
                    },
                  },
                ],
              }
              : {},
          },
        ],
        where: {
          mp: {
            [Op.ne]: null,
            [Op.ne]: '',
          },
          arrival_date: {
            [Op.ne]: null,
            [Op.ne]: '',
          },
          resv_status: {
            [Op.in]: ['CHECKED IN', 'CHECKED OUT'],
          },
        },
        group: [
          'Master.mp',
          'members.first',
          'members.last',
          'Master.arrival_date',
          'Master.departure_date',
        ],
        having: {
          [Op.or]: [
            literal(
              `SUM(CASE WHEN STR_TO_DATE(arrival_date, '%d/%m/%Y') >= DATE_SUB(CURDATE(), INTERVAL 3 MONTH) THEN 1 ELSE 0 END) > 0`
            ),
            literal(
              `SUM(CASE WHEN STR_TO_DATE(arrival_date, '%d/%m/%Y') > DATE_SUB(CURDATE(), INTERVAL 3 MONTH) AND STR_TO_DATE(arrival_date, '%d/%m/%Y') <= DATE_SUB(CURDATE(), INTERVAL 6 MONTH) THEN 1 ELSE 0 END) > 0`
            ),
            literal(
              `SUM(CASE WHEN STR_TO_DATE(arrival_date, '%d/%m/%Y') > DATE_SUB(CURDATE(), INTERVAL 6 MONTH) AND STR_TO_DATE(arrival_date, '%d/%m/%Y') <= DATE_SUB(CURDATE(), INTERVAL 9 MONTH) THEN 1 ELSE 0 END) > 0`
            ),
          ],
        },
        order: [
          [col('members.first'), 'ASC'],
          [col('members.last'), 'ASC'],
        ],
        raw: true,
        limit: perPageInt,
        offset: offset,
        logging: console.log,
      })

      const data = master.rows
      const totalCount = master.count

      res.status(200).json({
        data: data,
        totalData: totalCount.length,
      })
    } catch (error) {
      console.log(error)
      res.status(500).json({ error: 'Internal Server Error' })
    }
  }

  static async getSummaryEnrolls(req, res) {
    let { currentDate, membertypeId } = req.query

    const dateObj = new Date(currentDate)

    const year = dateObj.getFullYear()
    const month = dateObj.getMonth() + 1
    const day = dateObj.getDate()

    let whereClause = {}

    whereClause = {
      [Op.and]: [
        Sequelize.where(Sequelize.fn('YEAR', Sequelize.col('EnrollsLog.createdAt')), year),
        Sequelize.where(
          Sequelize.fn('MONTH', Sequelize.col('EnrollsLog.createdAt')),
          month
        ),
        Sequelize.where(Sequelize.fn('DAY', Sequelize.col('EnrollsLog.createdAt')), day),
      ],
    }

    // Tambahkan filter berdasarkan member_type_id jika ada
    if (membertypeId && membertypeId !== '' && membertypeId !== 'null') {
      whereClause[Op.and].push(
        Sequelize.where(Sequelize.col('members.member_type_id'), membertypeId)
      )
    }

    try {
      const enrolls = await EnrollsLog.findAll({
        attributes: [
          [
            Sequelize.fn(
              'IFNULL',
              Sequelize.cast(
                Sequelize.fn(
                  'SUM',
                  Sequelize.literal(
                    `CASE WHEN is_new_enroll = 1 AND EnrollsLog.location = 'BALI' THEN EnrollsLog.total_points ELSE 0 END`
                  )
                ),
                'DECIMAL'
              ),
              0
            ),
            'new_enroll_bali',
          ],
          [
            Sequelize.fn(
              'IFNULL',
              Sequelize.cast(
                Sequelize.fn(
                  'SUM',
                  Sequelize.literal(
                    `CASE WHEN is_new_enroll = 1 AND EnrollsLog.location = 'JAKARTA' THEN EnrollsLog.total_points ELSE 0 END`
                  )
                ),
                'DECIMAL'
              ),
              0
            ),
            'new_enroll_jakarta',
          ],
          [
            Sequelize.fn(
              'IFNULL',
              Sequelize.cast(
                Sequelize.fn(
                  'SUM',
                  Sequelize.literal(
                    `CASE WHEN is_new_enroll = 0 AND EnrollsLog.location = 'BALI' THEN EnrollsLog.total_points ELSE 0 END`
                  )
                ),
                'DECIMAL'
              ),
              0
            ),
            'existing_enroll_bali',
          ],
          [
            Sequelize.fn(
              'IFNULL',
              Sequelize.cast(
                Sequelize.fn(
                  'SUM',
                  Sequelize.literal(
                    `CASE WHEN is_new_enroll = 0 AND EnrollsLog.location = 'JAKARTA' THEN EnrollsLog.total_points ELSE 0 END`
                  )
                ),
                'DECIMAL'
              ),
              0
            ),
            'existing_enroll_jakarta',
          ],
          'members.member_type_id',
        ],
        where: whereClause,
        include: [
          {
            model: Member,  // Join dengan tabel Member
            required: true, // Pastikan hanya mengambil data yang memiliki member terkait
            attributes: [],
            as: 'members',  // Tidak mengambil data tambahan dari tabel Member
          }
        ],
        group: ['members.member_type_id'],
      })
      res.status(200).json(enrolls)
    } catch (error) {
      console.log(error)
    }
  }

  static async getDetailNewEnrolls(req, res) {
    let { currentDate, membertypeId } = req.query

    const dateObj = new Date(currentDate)

    const year = dateObj.getFullYear()
    const month = dateObj.getMonth() + 1
    const day = dateObj.getDate()

    let whereClause = {}

    whereClause = {
      [Op.and]: [
        Sequelize.where(Sequelize.fn('YEAR', Sequelize.col('EnrollsLog.createdAt')), year),
        Sequelize.where(
          Sequelize.fn('MONTH', Sequelize.col('EnrollsLog.createdAt')),
          month
        ),
        Sequelize.where(Sequelize.fn('DAY', Sequelize.col('EnrollsLog.createdAt')), day),
        {
          is_new_enroll: 1,
          total_points: {
            [Op.gt]: 0,
          },
        },
      ],
    }

    // Tambahkan filter berdasarkan member_type_id jika ada
    if (membertypeId && membertypeId !== '' && membertypeId !== 'null') {
      whereClause[Op.and].push(
        Sequelize.where(Sequelize.col('members.member_type_id'), membertypeId)
      )
    }

    try {
      const enrolls = await EnrollsLog.findAll({
        attributes: [
          'member_id',
          'first_name',
          'last_name',
          'is_new_enroll',
          'location',
          'total_points',
          'createdAt',
          // Add other necessary attributes here
        ],
        where: whereClause,
        include: [
          {
            model: Member,  // Join dengan tabel Member
            required: true, // Pastikan hanya mengambil data yang memiliki member terkait
            attributes: [],
            as: 'members',  // Tidak mengambil data tambahan dari tabel Member
          }
        ],
        order: [[Sequelize.col('EnrollsLog.createdAt'), 'DESC']],
      })

      res.status(200).json(enrolls)
    } catch (error) {
      console.log(error)
    }
  }

  static async getDetailExistingEnrolls(req, res) {
    let { currentDate, membertypeId } = req.query

    const dateObj = new Date(currentDate)

    const year = dateObj.getFullYear()
    const month = dateObj.getMonth() + 1
    const day = dateObj.getDate()

    let whereClause = {}

    whereClause = {
      [Op.and]: [
        Sequelize.where(Sequelize.fn('YEAR', Sequelize.col('EnrollsLog.createdAt')), year),
        Sequelize.where(
          Sequelize.fn('MONTH', Sequelize.col('EnrollsLog.createdAt')),
          month
        ),
        Sequelize.where(Sequelize.fn('DAY', Sequelize.col('EnrollsLog.createdAt')), day),
        {
          is_new_enroll: 0,
          total_points: {
            [Op.gt]: 0,
          },
        },
      ],
    }

    // Tambahkan filter berdasarkan member_type_id jika ada
    if (membertypeId && membertypeId !== '' && membertypeId !== 'null') {
      whereClause[Op.and].push(
        Sequelize.where(Sequelize.col('members.member_type_id'), membertypeId)
      )
    }

    try {
      const enrolls = await EnrollsLog.findAll({
        attributes: [
          'member_id',
          'first_name',
          'last_name',
          'location',
          'total_points',
          'createdAt',
          // Add other necessary attributes here
        ],
        where: whereClause,
        include: [
          {
            model: Member,  // Join dengan tabel Member
            required: true, // Pastikan hanya mengambil data yang memiliki member terkait
            attributes: [],
            as: 'members',  // Tidak mengambil data tambahan dari tabel Member
          }
        ],
        order: [[Sequelize.col('EnrollsLog.createdAt'), 'DESC']],
      })

      res.status(200).json(enrolls)
    } catch (error) {
      console.log(error)
    }
  }

  static async getPointTransaction(req, res) {
    let { currentDate } = req.query

    const dateObj = new Date(currentDate)

    const year = dateObj.getFullYear()
    const month = dateObj.getMonth() + 1
    const day = dateObj.getDate()

    let whereClause = {}

    whereClause = {
      [Op.and]: [
        Sequelize.where(
          Sequelize.fn('YEAR', Sequelize.col('inquiry_date')),
          year
        ),
        Sequelize.where(
          Sequelize.fn('MONTH', Sequelize.col('inquiry_date')),
          month
        ),
        Sequelize.where(
          Sequelize.fn('DAY', Sequelize.col('inquiry_date')),
          day
        ),
      ],
    }

    try {
      const points = await PointDailyTransaction.findAll({
        attributes: [
          'id',
          'inquiry_date',
          'opening_balance',
          'closing_balance',
        ],
        where: whereClause,
        order: [[Sequelize.col('inquiry_date'), 'ASC']],
      })
      res.status(200).json(points)
    } catch (error) {
      console.log(error)
    }
  }

  static async getSummaryPurchase(req, res) {
    let { currentDate, membertypeId } = req.query

    const dateObj = new Date(currentDate)

    const year = dateObj.getFullYear()
    const month = dateObj.getMonth() + 1
    const day = dateObj.getDate()

    let whereClause = {}

    whereClause = {
      [Op.and]: [
        Sequelize.where(
          Sequelize.fn('YEAR', Sequelize.col('PointBuyPromo.purchase_date')),
          year
        ),
        Sequelize.where(
          Sequelize.fn('MONTH', Sequelize.col('PointBuyPromo.purchase_date')),
          month
        ),
        Sequelize.where(
          Sequelize.fn('DAY', Sequelize.col('PointBuyPromo.purchase_date')),
          day
        ),
      ],
    }

    // Tambahkan filter berdasarkan member_type_id jika ada
    if (membertypeId && membertypeId !== '' && membertypeId !== 'null') {
      whereClause[Op.and].push(
        Sequelize.where(Sequelize.col('member.member_type_id'), membertypeId)
      )
    }

    try {
      const enrolls = await PointBuyPromo.findAll({
        attributes: [
          [
            Sequelize.fn(
              'IFNULL',
              Sequelize.cast(
                Sequelize.fn(
                  'SUM',
                  Sequelize.literal(
                    `CASE WHEN property = 'HMS' THEN PointBuyPromo.total_point ELSE 0 END`
                  )
                ),
                'DECIMAL'
              ),
              0
            ),
            'purchase_point_jakarta',
          ],
          [
            Sequelize.fn(
              'IFNULL',
              Sequelize.cast(
                Sequelize.fn(
                  'SUM',
                  Sequelize.literal(
                    `CASE WHEN property != 'HMS' THEN PointBuyPromo.total_point ELSE 0 END`
                  )
                ),
                'DECIMAL'
              ),
              0
            ),
            'purchase_point_bali',
          ],
          'member.member_type_id',
        ],
        where: whereClause,
        include: [
          {
            model: Member,  // Join dengan tabel Member
            required: true, // Pastikan hanya mengambil data yang memiliki member terkait
            attributes: [],
            as: 'member',  // Tidak mengambil data tambahan dari tabel Member
          }
        ],
        group: ['member.member_type_id'],
      })
      res.status(200).json(enrolls)
    } catch (error) {
      console.log(error)
    }
  }

  static async getDetailPurchase(req, res) {
    let { currentDate, membertypeId } = req.query

    const dateObj = new Date(currentDate)

    const year = dateObj.getFullYear()
    const month = dateObj.getMonth() + 1
    const day = dateObj.getDate()

    let whereClause = {}

    whereClause = {
      [Op.and]: [
        Sequelize.where(
          Sequelize.fn('YEAR', Sequelize.col('PointBuyPromo.purchase_date')),
          year
        ),
        Sequelize.where(
          Sequelize.fn('MONTH', Sequelize.col('PointBuyPromo.purchase_date')),
          month
        ),
        Sequelize.where(
          Sequelize.fn('DAY', Sequelize.col('PointBuyPromo.purchase_date')),
          day
        ),
        {
          total_point: {
            [Op.gt]: 0,
          },
        },
      ],
    }

    // Tambahkan filter berdasarkan member_type_id jika ada
    if (membertypeId && membertypeId !== '' && membertypeId !== 'null') {
      whereClause[Op.and].push(
        Sequelize.where(Sequelize.col('member.member_type_id'), membertypeId)
      )
    }

    try {
      const purchases = await PointBuyPromo.findAll({
        attributes: [
          'member_id',
          'member_name',
          'purchase_date',
          'check_number',
          'property',
          'total_point',
          'createdAt',
        ],
        where: whereClause,
        include: [
          {
            model: Member,  // Join dengan tabel Member
            required: true, // Pastikan hanya mengambil data yang memiliki member terkait
            attributes: [],
            as: 'member',  // Tidak mengambil data tambahan dari tabel Member
          }
        ],
        order: [[Sequelize.col('PointBuyPromo.createdAt'), 'DESC']],
      })

      // Add location field based on property value
      const purchasesWithLocation = purchases.map((purchase) => {
        return {
          ...purchase.dataValues,
          location: purchase.property === 'HMS' ? 'JAKARTA' : 'BALI',
        }
      })

      res.status(200).json(purchasesWithLocation)
    } catch (error) {
      console.log(error)
    }
  }

  static async getSummaryBonus(req, res) {
    let { currentDate, membertypeId } = req.query

    const dateObj = new Date(currentDate)

    const year = dateObj.getFullYear()
    const month = dateObj.getMonth() + 1
    const day = dateObj.getDate()

    let whereClause = {}

    whereClause = {
      [Op.and]: [
        Sequelize.where(Sequelize.fn('YEAR', Sequelize.col('UpgradeLog.createdAt')), year),
        Sequelize.where(
          Sequelize.fn('MONTH', Sequelize.col('UpgradeLog.createdAt')),
          month
        ),
        Sequelize.where(Sequelize.fn('DAY', Sequelize.col('UpgradeLog.createdAt')), day),
      ],
    }

    // Tambahkan filter berdasarkan member_type_id jika ada
    if (membertypeId && membertypeId !== '' && membertypeId !== 'null') {
      whereClause[Op.and].push(
        Sequelize.where(Sequelize.col('members.member_type_id'), membertypeId)
      )
    }


    try {
      const bonus = await UpgradeLog.findAll({
        attributes: [
          [
            Sequelize.fn(
              'IFNULL',
              Sequelize.cast(
                Sequelize.fn(
                  'SUM',
                  Sequelize.literal(
                    `CASE WHEN location = 'BALI' THEN UpgradeLog.bonus_points ELSE 0 END`
                  )
                ),
                'DECIMAL'
              ),
              0
            ),
            'bonus_point_bali',
          ],
          [
            Sequelize.fn(
              'IFNULL',
              Sequelize.cast(
                Sequelize.fn(
                  'SUM',
                  Sequelize.literal(
                    `CASE WHEN location = 'JAKARTA' THEN UpgradeLog.bonus_points ELSE 0 END`
                  )
                ),
                'DECIMAL'
              ),
              0
            ),
            'bonus_point_jakarta',
          ],
          'members.member_type_id',
        ],
        where: whereClause,
        include: [
          {
            model: Member,  // Join dengan tabel Member
            required: true, // Pastikan hanya mengambil data yang memiliki member terkait
            attributes: [],
            as: 'members',  // Tidak mengambil data tambahan dari tabel Member
          }
        ],
        group: ['members.member_type_id'],
      })
      res.status(200).json(bonus)
    } catch (error) {
      console.log(error)
    }
  }

  static async getDetailBonus(req, res) {
    let { currentDate, membertypeId } = req.query

    const dateObj = new Date(currentDate)

    const year = dateObj.getFullYear()
    const month = dateObj.getMonth() + 1
    const day = dateObj.getDate()

    let whereClause = {}

    whereClause = {
      [Op.and]: [
        Sequelize.where(
          Sequelize.fn('YEAR', Sequelize.col('UpgradeLog.createdAt')),
          year
        ),
        Sequelize.where(
          Sequelize.fn('MONTH', Sequelize.col('UpgradeLog.createdAt')),
          month
        ),
        Sequelize.where(
          Sequelize.fn('DAY', Sequelize.col('UpgradeLog.createdAt')),
          day
        ),
        {
          bonus_points: {
            [Op.gt]: 0,
          },
        },
      ],
    }

    // Tambahkan filter berdasarkan member_type_id jika ada
    if (membertypeId && membertypeId !== '' && membertypeId !== 'null') {
      whereClause[Op.and].push(
        Sequelize.where(Sequelize.col('members.member_type_id'), membertypeId)
      )
    }

    try {
      const bonus = await UpgradeLog.findAll({
        attributes: [
          'member_id',
          [Sequelize.col('members.first'), 'first'],
          [Sequelize.col('members.last'), 'last'],
          'member_level',
          'member_next_level',
          'location',
          'bonus_points',
          'createdAt',
        ],
        where: whereClause,
        include: [
          {
            model: Member,
            as: 'members',
            attributes: [],
            required: true, // This ensures an inner join
          },
        ],
        order: [[Sequelize.col('UpgradeLog.createdAt'), 'DESC']],
      })
      res.status(200).json(bonus)
    } catch (error) {
      console.log(error)
    }
  }

  static async getSummaryDining(req, res) {
    let { currentDate, membertypeId } = req.query

    const dateObj = new Date(currentDate)

    const year = dateObj.getFullYear()
    const month = dateObj.getMonth() + 1
    const day = dateObj.getDate()

    let whereClause = {}

    whereClause = {
      [Op.and]: [
        Sequelize.where(
          Sequelize.fn('YEAR', Sequelize.col('Dining.dining_date')),
          year
        ),
        Sequelize.where(
          Sequelize.fn('MONTH', Sequelize.col('Dining.dining_date')),
          month
        ),
        Sequelize.where(Sequelize.fn('DAY', Sequelize.col('Dining.dining_date')), day),
        {
          is_approved: 1,
        },
      ],
    }

    // Tambahkan filter berdasarkan member_type_id jika ada
    if (membertypeId && membertypeId !== '' && membertypeId !== 'null') {
      whereClause[Op.and].push(
        Sequelize.where(Sequelize.col('member.member_type_id'), membertypeId)
      )
    }

    try {
      const dinings = await Dining.findAll({
        attributes: [
          [
            Sequelize.fn(
              'IFNULL',
              Sequelize.cast(
                Sequelize.fn(
                  'SUM',
                  Sequelize.literal(
                    `CASE WHEN location = 'BALI' THEN Dining.earned_point ELSE 0 END`
                  )
                ),
                'DECIMAL'
              ),
              0
            ),
            'dining_point_bali',
          ],
          [
            Sequelize.fn(
              'IFNULL',
              Sequelize.cast(
                Sequelize.fn(
                  'SUM',
                  Sequelize.literal(
                    `CASE WHEN location = 'JAKARTA' THEN Dining.earned_point ELSE 0 END`
                  )
                ),
                'DECIMAL'
              ),
              0
            ),
            'dining_point_jakarta',
          ],
          'member.member_type_id',
        ],
        where: whereClause,
        include: [
          {
            model: Member,  // Join dengan tabel Member
            required: true, // Pastikan hanya mengambil data yang memiliki member terkait
            attributes: [],
            as: 'member',  // Tidak mengambil data tambahan dari tabel Member
          }
        ],
        group: ['member.member_type_id'],
      })
      res.status(200).json(dinings)
    } catch (error) {
      console.log(error)
    }
  }

  static async getDetailDining(req, res) {
    let { currentDate, membertypeId } = req.query

    const dateObj = new Date(currentDate)

    const year = dateObj.getFullYear()
    const month = dateObj.getMonth() + 1
    const day = dateObj.getDate()

    let whereClause = {}

    whereClause = {
      [Op.and]: [
        Sequelize.where(
          Sequelize.fn('YEAR', Sequelize.col('Dining.dining_date')),
          year
        ),
        Sequelize.where(
          Sequelize.fn('MONTH', Sequelize.col('Dining.dining_date')),
          month
        ),
        Sequelize.where(Sequelize.fn('DAY', Sequelize.col('Dining.dining_date')), day),
        {
          is_approved: 1,
          earned_point: {
            [Op.gt]: 0,
          },
        },
      ],
    }

    // Tambahkan filter berdasarkan member_type_id jika ada
    if (membertypeId && membertypeId !== '' && membertypeId !== 'null') {
      whereClause[Op.and].push(
        Sequelize.where(Sequelize.col('member.member_type_id'), membertypeId)
      )
    }

    try {
      const dinings = await Dining.findAll({
        attributes: [
          'member_id',
          'member_name',
          'check_number',
          'location',
          'earned_point',
          'createdAt',
        ],
        where: whereClause,
        include: [
          {
            model: Member,  // Join dengan tabel Member
            required: true, // Pastikan hanya mengambil data yang memiliki member terkait
            attributes: [],
            as: 'member',  // Tidak mengambil data tambahan dari tabel Member
          }
        ],
        order: [[Sequelize.col('Dining.dining_date'), 'DESC']],
      })
      res.status(200).json(dinings)
    } catch (error) {
      console.log(error)
    }
  }

  static async getSummaryRedeem(req, res) {
    let { currentDate, membertypeId } = req.query

    const dateObj = new Date(currentDate)

    const year = dateObj.getFullYear()
    const month = dateObj.getMonth() + 1
    const day = dateObj.getDate()

    let whereClause = {}

    whereClause = {
      [Op.and]: [
        Sequelize.where(
          Sequelize.fn('YEAR', Sequelize.col('ReedemsPoint.createdAt')),
          year
        ),
        Sequelize.where(
          Sequelize.fn('MONTH', Sequelize.col('ReedemsPoint.createdAt')),
          month
        ),
        Sequelize.where(
          Sequelize.fn('DAY', Sequelize.col('ReedemsPoint.createdAt')),
          day
        ),
        {
          approve_status_id: 4,
        },
      ],
    }

    // Tambahkan filter berdasarkan member_type_id jika ada
    if (membertypeId && membertypeId !== '' && membertypeId !== 'null') {
      whereClause[Op.and].push(
        Sequelize.where(Sequelize.col('members.member_type_id'), membertypeId)
      )
    }

    try {
      const redeems = await ReedemsPoint.findAll({
        attributes: [
          [
            Sequelize.fn(
              'IFNULL',
              Sequelize.cast(
                Sequelize.fn(
                  'SUM',
                  Sequelize.literal(
                    `CASE WHEN reedems.location = 'BALI' THEN ReedemsPoint.total_reedem_points ELSE 0 END`
                  )
                ),
                'DECIMAL'
              ),
              0
            ),
            'redeem_point_bali',
          ],
          [
            Sequelize.fn(
              'IFNULL',
              Sequelize.cast(
                Sequelize.fn(
                  'SUM',
                  Sequelize.literal(
                    `CASE WHEN reedems.location = 'JAKARTA' THEN ReedemsPoint.total_reedem_points ELSE 0 END`
                  )
                ),
                'DECIMAL'
              ),
              0
            ),
            'redeem_point_jakarta',
          ],
        ],
        include: [
          {
            model: Reedem,
            as: 'reedems',
            attributes: [],
            required: true, // This ensures an inner join
          },
          {
            model: Member,  // Join dengan tabel Member
            required: true, // Pastikan hanya mengambil data yang memiliki member terkait
            attributes: [],
            as: 'members',  // Tidak mengambil data tambahan dari tabel Member
          }
        ],
        where: whereClause,
        group: ['members.member_type_id'],
        raw: true,
      })

      res.status(200).json(redeems)
    } catch (error) {
      console.log(error)
    }
  }

  static async getDetailRedeem(req, res) {
    let { currentDate, membertypeId } = req.query

    const dateObj = new Date(currentDate)

    const year = dateObj.getFullYear()
    const month = dateObj.getMonth() + 1
    const day = dateObj.getDate()

    let whereClause = {}

    whereClause = {
      [Op.and]: [
        Sequelize.where(
          Sequelize.fn('YEAR', Sequelize.col('ReedemsPoint.createdAt')),
          year
        ),
        Sequelize.where(
          Sequelize.fn('MONTH', Sequelize.col('ReedemsPoint.createdAt')),
          month
        ),
        Sequelize.where(
          Sequelize.fn('DAY', Sequelize.col('ReedemsPoint.createdAt')),
          day
        ),
        {
          approve_status_id: 4,
          total_reedem_points: {
            [Op.gt]: 0,
          },
        },
      ],
    }

    // Tambahkan filter berdasarkan member_type_id jika ada
    if (membertypeId && membertypeId !== '' && membertypeId !== 'null') {
      whereClause[Op.and].push(
        Sequelize.where(Sequelize.col('members.member_type_id'), membertypeId)
      )
    }

    try {
      const redeems = await ReedemsPoint.findAll({
        attributes: [
          'member_id',
          'beneficiary',
          [Sequelize.col('reedems.reedem_name'), 'reedem_name'],
          [Sequelize.col('reedems.location'), 'location'],
          'total_reedem_points',
          'createdAt',
        ],
        include: [
          {
            model: Reedem,
            as: 'reedems',
            attributes: [],
            required: true, // This ensures an inner join
          },
          {
            model: Member,  // Join dengan tabel Member
            required: true, // Pastikan hanya mengambil data yang memiliki member terkait
            attributes: [],
            as: 'members',  // Tidak mengambil data tambahan dari tabel Member
          }
        ],
        where: whereClause,
        order: [[Sequelize.col('ReedemsPoint.createdAt'), 'DESC']],
      })
      res.status(200).json(redeems)
    } catch (error) {
      console.log(error)
    }
  }

  static async getSummaryReturningGuest(req, res) {
    const { page, per_page, filter } = req.query
    const perPageInt = parseInt(per_page, 10)
    const offset = (page - 1) * per_page

    // Initialize filter conditions
    let filterConditions = {}
    let roomClassFilter = null

    // Determine room class values based on location filter
    const baliRoomClasses = ['MVB', 'TMB', 'MRB']
    const jakartaRoomClasses = ['PSE', 'MUL', 'STE', 'MCL']

    // Apply filters
    if (filter) {
      // const filterTrimmed = filter.trim();
      const filterTrimmed = filter.trim().toUpperCase()

      // Determine if filter matches a room class or a general filter
      if (filterTrimmed === 'BALI') {
        roomClassFilter = { [Op.in]: baliRoomClasses }
      } else if (filterTrimmed === 'JAKARTA') {
        roomClassFilter = { [Op.in]: jakartaRoomClasses }
      } else {
        filterConditions[Op.or] = [
          { member_id: { [Op.eq]: filterTrimmed } },
          { name_on_card: { [Op.like]: `%${filterTrimmed}%` } },
          { first: { [Op.like]: `%${filterTrimmed}%` } },
          { last: { [Op.like]: `%${filterTrimmed}%` } },
        ]
      }
    }

    try {
      const returnings = await Member.findAndCountAll({
        attributes: [
          'member_id',
          'name_on_card',
          [Sequelize.col('first'), 'first_name'],
          [Sequelize.col('last'), 'last_name'],
          'joined_date',
          [Sequelize.col('masters.room_class'), 'property'],
          [
            literal(`CASE 
                            WHEN masters.room_class IN ('MVB', 'TMB', 'MRB') THEN 'BALI'
                            ELSE 'JAKARTA'
                        END`),
            'location',
          ],
          [
            fn('COUNT', literal('DISTINCT masters.arrival_date')),
            'visit_count',
          ],
        ],
        include: [
          {
            model: Master,
            as: 'masters',
            attributes: [],
            required: true, // This ensures an inner join
            where: {
              room_class: roomClassFilter || {
                [Op.in]: ['MVB', 'TMB', 'MRB', 'PSE', 'MUL', 'STE', 'MCL'],
              },
              [Op.or]: [
                { arrival_date: { [Op.ne]: '' } },
                { departure_date: { [Op.ne]: '' } },
              ],
              resv_status: 'CHECKED OUT',
            },
          },
        ],
        where: filterConditions,
        group: [
          'Member.member_id',
          'Member.name_on_card',
          'Member.first',
          'Member.last',
          'Member.joined_date',
          'masters.room_class',
        ],
        having: fn('COUNT', col('masters.arrival_date')),
        raw: true,
        limit: perPageInt,
        offset: offset,
      })

      const data = returnings.rows
      const totalCount = returnings.count

      res.status(200).json({
        data: data,
        totalData: totalCount.length,
      })

      // res.status(200).json(returnings);
    } catch (error) {
      console.log(error)
    }
  }

  static async getDetailReturningGuest(req, res) {
    const { memberId, region } = req.query

    try {
      const results = await Master.findAll({
        attributes: [
          'arrival_date',
          'departure_date',
          'room_class',
          [
            Sequelize.fn('SUM', Sequelize.col('room_rev_usd')),
            'total_room_rev_usd',
          ],
          [
            Sequelize.fn('SUM', Sequelize.col('fb_rev_usd')),
            'total_fb_rev_usd',
          ],
          // Add other necessary attributes here
        ],
        where: {
          mp: memberId,
          resv_status: 'CHECKED OUT',
          [Op.or]: [
            { arrival_date: { [Op.ne]: '' } },
            { departure_date: { [Op.ne]: '' } },
          ],
          region: region,
        },
        group: ['arrival_date', 'departure_date', 'room_class'],
        order: [
          [
            Sequelize.fn(
              'STR_TO_DATE',
              Sequelize.col('arrival_date'),
              '%d/%m/%Y'
            ),
            'ASC',
          ],
          [
            Sequelize.fn(
              'STR_TO_DATE',
              Sequelize.col('departure_date'),
              '%d/%m/%Y'
            ),
            'ASC',
          ],
        ],
      })

      res.status(200).json(results)
    } catch (error) {
      console.log(error)
    }
  }

  static async getSummaryReturningGuestReport(req, res) {
    const { filter } = req.query

    // Initialize filter conditions
    let filterConditions = {}
    let roomClassFilter = null

    // Determine room class values based on location filter
    const baliRoomClasses = ['MVB', 'TMB', 'MRB']
    const jakartaRoomClasses = ['PSE', 'MUL', 'STE', 'MCL']

    // Apply filters
    if (filter) {
      // const filterTrimmed = filter.trim();
      const filterTrimmed = filter.trim().toUpperCase()

      // Determine if filter matches a room class or a general filter
      if (filterTrimmed === 'BALI') {
        roomClassFilter = { [Op.in]: baliRoomClasses }
      } else if (filterTrimmed === 'JAKARTA') {
        roomClassFilter = { [Op.in]: jakartaRoomClasses }
      } else {
        filterConditions[Op.or] = [
          { member_id: { [Op.eq]: filterTrimmed } },
          { name_on_card: { [Op.like]: `%${filterTrimmed}%` } },
          { first: { [Op.like]: `%${filterTrimmed}%` } },
          { last: { [Op.like]: `%${filterTrimmed}%` } },
        ]
      }
    }

    try {
      const returnings = await Member.findAll({
        attributes: [
          'member_id',
          'name_on_card',
          [Sequelize.col('first'), 'first_name'],
          [Sequelize.col('last'), 'last_name'],
          'joined_date',
          [Sequelize.col('masters.room_class'), 'property'],
          [
            literal(`CASE 
                            WHEN masters.room_class IN ('MVB', 'TMB', 'MRB') THEN 'BALI'
                            ELSE 'JAKARTA'
                        END`),
            'location',
          ],
          [
            fn('COUNT', literal('DISTINCT masters.arrival_date')),
            'visit_count',
          ],
        ],
        include: [
          {
            model: Master,
            as: 'masters',
            attributes: [],
            required: true, // This ensures an inner join
            where: {
              room_class: roomClassFilter || {
                [Op.in]: ['MVB', 'TMB', 'MRB', 'PSE', 'MUL', 'STE', 'MCL'],
              },
              [Op.or]: [
                { arrival_date: { [Op.ne]: '' } },
                { departure_date: { [Op.ne]: '' } },
              ],
            },
          },
        ],
        where: filterConditions,
        group: [
          'Member.member_id',
          'Member.name_on_card',
          'Member.first',
          'Member.last',
          'Member.joined_date',
          'masters.room_class',
        ],
        having: fn('COUNT', col('masters.arrival_date')),
        raw: true,
      })

      res.status(200).json(returnings)
    } catch (error) {
      console.log(error)
    }
  }

  static async getVisitInterval(req, res) {
    try {
      // Truncate the ReturningInterval table
      await ReturningInterval.truncate()

      // Step 1: Get the first stay records
      const firstStayRecords = await Master.findAll({
        attributes: [
          [Sequelize.col('mp'), 'memberId'],
          [Sequelize.col('members.name_on_card'), 'name_on_card'],
          [fn('MIN', col('departure_date')), 'first_stay'],
        ],
        include: [
          {
            model: Member,
            as: 'members',
            attributes: ['name_on_card'],
            required: true,
          },
        ],
        group: ['Master.mp', 'members.name_on_card'],
        where: {
          departure_date: {
            [Op.ne]: null,
          },
        },
        having: {
          first_stay: {
            [Op.and]: [
              { [Op.ne]: null }, // Exclude null first_stay
              { [Op.ne]: '' }, // Exclude empty string first_stay
            ],
          },
        },
      })

      // Step 2: Process each record to get next departures and intervals
      const results = []
      for (const record of firstStayRecords) {
        const memberId = record.dataValues.memberId
        let firstStayDate = record.dataValues.first_stay

        // Convert firstStayDate to a Date object
        if (
          typeof firstStayDate === 'string' ||
          firstStayDate instanceof String
        ) {
          firstStayDate = new Date(firstStayDate)
        }

        // Check if firstStayDate is valid
        if (!firstStayDate || isNaN(firstStayDate.getTime())) {
          console.warn(`No valid first stay date for memberId: ${memberId}`)
          continue // Skip this record if no valid first stay date
        }

        // Format firstStayDate as YYYY-MM-DD
        const formattedFirstStayDate = firstStayDate.toISOString().slice(0, 10)

        const nextDepartures = await Master.findAll({
          attributes: [
            [
              fn('STR_TO_DATE', col('departure_date'), '%d/%m/%Y'),
              'departure_date',
            ],
            [
              literal(`
                            MIN(
                        CASE
                        WHEN STR_TO_DATE(departure_date, '%d/%m/%Y') <= DATE_ADD('${firstStayDate
                  .toISOString()
                  .slice(0, 10)}', INTERVAL 30 DAY) THEN '30_days'
                        WHEN STR_TO_DATE(departure_date, '%d/%m/%Y') <= DATE_ADD('${firstStayDate
                  .toISOString()
                  .slice(0, 10)}', INTERVAL 60 DAY) THEN '60_days'
                        WHEN STR_TO_DATE(departure_date, '%d/%m/%Y') <= DATE_ADD('${firstStayDate
                  .toISOString()
                  .slice(0, 10)}', INTERVAL 90 DAY) THEN '90_days'
                        WHEN STR_TO_DATE(departure_date, '%d/%m/%Y') <= DATE_ADD('${firstStayDate
                  .toISOString()
                  .slice(0, 10)}', INTERVAL 180 DAY) THEN '180_days'
                        WHEN STR_TO_DATE(departure_date, '%d/%m/%Y') <= DATE_ADD('${firstStayDate
                  .toISOString()
                  .slice(0, 10)}', INTERVAL 210 DAY) THEN '210_days'
                        WHEN STR_TO_DATE(departure_date, '%d/%m/%Y') <= DATE_ADD('${firstStayDate
                  .toISOString()
                  .slice(0, 10)}', INTERVAL 240 DAY) THEN '240_days'
                        WHEN STR_TO_DATE(departure_date, '%d/%m/%Y') <= DATE_ADD('${firstStayDate
                  .toISOString()
                  .slice(0, 10)}', INTERVAL 270 DAY) THEN '270_days'
                        WHEN STR_TO_DATE(departure_date, '%d/%m/%Y') <= DATE_ADD('${firstStayDate
                  .toISOString()
                  .slice(0, 10)}', INTERVAL 300 DAY) THEN '300_days'
                        WHEN STR_TO_DATE(departure_date, '%d/%m/%Y') <= DATE_ADD('${firstStayDate
                  .toISOString()
                  .slice(0, 10)}', INTERVAL 330 DAY) THEN '330_days'
                        WHEN STR_TO_DATE(departure_date, '%d/%m/%Y') <= DATE_ADD('${firstStayDate
                  .toISOString()
                  .slice(0, 10)}', INTERVAL 360 DAY) THEN '360_days'
                        ELSE 'More_360_days'
                        END
                    )
                        `),
              'interval_label',
            ],
          ],
          where: {
            mp: memberId,
            [Op.and]: [
              Sequelize.where(
                fn('STR_TO_DATE', col('departure_date'), '%d/%m/%Y'),
                '>',
                `${firstStayDate.toISOString().slice(0, 10)}`
              ),
              Sequelize.where(
                fn('STR_TO_DATE', col('departure_date'), '%d/%m/%Y'),
                '<=',
                literal(
                  `DATE_ADD('${firstStayDate
                    .toISOString()
                    .slice(0, 10)}', INTERVAL 360 DAY)`
                )
              ),
            ],
          },
          group: [fn('STR_TO_DATE', col('departure_date'), '%d/%m/%Y')],
          order: [[literal("STR_TO_DATE(departure_date, '%d/%m/%Y')"), 'ASC']],
        })

        // Only push to results if nextDepartures is not null or empty
        if (nextDepartures && nextDepartures.length > 0) {
          results.push({
            memberId,
            name_on_card: record.dataValues.name_on_card,
            first_stay: formattedFirstStayDate,
            departures: nextDepartures,
          })
        }
      }

      // Insert data to returning_interval table
      for (const result of results) {
        await ReturningInterval.create({
          member_id: result.memberId,
          name_on_card: result.name_on_card,
          first_stay: result.first_stay,
        })

        if (result.departures.length > 0) {
          const groupByDeparture = result.departures.reduce((acc, cur) => {
            const key = cur.dataValues.interval_label
            if (!acc[key]) acc[key] = []
            acc[key].push(cur.departure_date)
            return acc
          }, {})

          const updatedDepartureDate = Object.keys(groupByDeparture).map(
            (key) => {
              if (groupByDeparture[key].length > 1) {
                return {
                  interval_label: key,
                  departure_date: groupByDeparture[key].join(';'),
                }
              }

              return {
                interval_label: key,
                departure_date: groupByDeparture[key][0],
              }
            }
          )

          // Assuming you have a map of interval labels to database columns
          const intervalFieldMap = {
            '30_days': 'thirty_days',
            '60_days': 'sixty_days',
            '90_days': 'ninety_days',
            '120_days': 'one_hundred_twenty_days',
            '150_days': 'one_hundred_fifty_days',
            '180_days': 'one_hundred_eighty_days',
            '210_days': 'two_hundred_ten_days',
            '240_days': 'two_hundred_forty_days',
            '270_days': 'two_hundred_seventy_days',
            '300_days': 'three_hundred_days',
            '330_days': 'three_hundred_thirty_days',
            '360_days': 'three_hundred_sixty_days',
            More_360_days: 'more_three_hundred_sixty_days',
          }

          console.log(updatedDepartureDate, '>>>>> IWAN updatedDepartureDate')

          for (const cur of updatedDepartureDate) {
            const fieldToUpdate = intervalFieldMap[cur.interval_label]

            if (!fieldToUpdate) {
              throw new Error(`Unknown interval_label: ${cur.interval_label}`)
            }

            // Update the field dynamically
            await ReturningInterval.update(
              {
                [fieldToUpdate]: JSON.stringify(cur.departure_date), // Assuming cur.departure_date contains the date to be updated
              },
              {
                where: {
                  member_id: result.memberId,
                },
              }
            )
          }
        }
      }

      res.status(200).json(results)
    } catch (error) {
      console.error(error)
      res.status(500).json({ error: 'An error occurred' })
    }
  }

  static async getReturningGuestInterval(req, res) {
    const { page, per_page, filter } = req.query
    const perPageInt = parseInt(per_page, 10)
    const offset = (page - 1) * per_page

    try {
      // Lakukan query ke database menggunakan Sequelize
      const member = await ReturningInterval.findAndCountAll({
        attributes: [
          'id',
          'member_id',
          'name_on_card',
          'first_stay',
          'thirty_days',
          'sixty_days',
          'ninety_days',
          'one_hundred_twenty_days',
          'one_hundred_fifty_days',
          'one_hundred_eighty_days',
          'two_hundred_ten_days',
          'two_hundred_forty_days',
          'two_hundred_seventy_days',
          'three_hundred_days',
          'three_hundred_thirty_days',
          'three_hundred_sixty_days',
          'more_three_hundred_sixty_days',
          'createdAt',
        ],
        offset: offset,
        limit: perPageInt,
        where: filter
          ? {
            [Op.or]: [
              {
                member_id: {
                  [Op.like]: `%${filter}%`,
                },
              },
              {
                name_on_card: {
                  [Op.like]: `%${filter}%`,
                },
              },
            ],
          }
          : {},
        // Anda dapat menambahkan pengurutan jika diperlukan
        order: [['member_id', 'ASC']],
      })

      // Ekstrak data dan jumlah total dari hasil query
      const data = member.rows
      const totalCount = member.count

      res.status(200).json({
        data: data,
        totalData: totalCount,
      })
    } catch (error) {
      console.log(error)
    }
  }

  static async getCloseBalance(req, res) {
    let { previousDate } = req.query

    const dateObj = new Date(previousDate)

    const year = dateObj.getFullYear()
    const month = dateObj.getMonth() + 1
    const day = dateObj.getDate()

    let whereClause = {}

    whereClause = {
      [Op.and]: [
        Sequelize.where(
          Sequelize.fn('YEAR', Sequelize.col('inquiry_date')),
          year
        ),
        Sequelize.where(
          Sequelize.fn('MONTH', Sequelize.col('inquiry_date')),
          month
        ),
        Sequelize.where(
          Sequelize.fn('DAY', Sequelize.col('inquiry_date')),
          day
        ),
      ],
    }

    try {
      const closebalance = await PointDailyTransaction.findAll({
        attributes: ['closing_balance'],
        where: whereClause,
      })
      res.status(200).json(closebalance)
    } catch (error) {
      console.log(error)
    }
  }

  static async getListRedemption(req, res) {
    let { dateFrom, dateTo, region } = req.query
    try {
      const query = `
      SELECT ReedemsPoint.member_id, members.first AS first_name, members.last AS last_name,
              CASE
                WHEN ReedemsPoint.reedem_id IS NOT NULL THEN ReedemsPoint.reedem_id
                ELSE items.redem_id
              END
             AS reedem_id,
              CASE
                WHEN ReedemsPoint.reedem_id IS NOT NULL THEN reedems.reedem_name
                ELSE items.redem_name
              END
             AS reedem_name,
              CASE
                WHEN ReedemsPoint.reedem_id IS NOT NULL THEN reedems.location
                ELSE items.redem_location
              END
             AS location,
              CASE
                WHEN ReedemsPoint.reedem_id IS NOT NULL THEN ReedemsPoint.qty_points
                ELSE items.qty_points
              END
             AS total_qty_points,
              CASE
                WHEN ReedemsPoint.reedem_id IS NOT NULL THEN ReedemsPoint.total_reedem_points
                ELSE items.total_redem_points
              END
             AS total_redeem_points FROM ReedemsPoints AS ReedemsPoint 
             LEFT OUTER JOIN RedemsPointItems AS items ON ReedemsPoint.id = items.redem_point_id 
             LEFT OUTER JOIN Reedems AS reedems ON ReedemsPoint.reedem_id = reedems.id 
             INNER JOIN Members AS members ON ReedemsPoint.member_id = members.member_id 
             WHERE ReedemsPoint.approve_status_id = 4 AND ReedemsPoint.is_manual = 0 
             AND ReedemsPoint.createdAt BETWEEN '${dateFrom}' AND '${dateTo}' 
             AND items.redem_location = '${region}' ORDER BY ReedemsPoint.member_id ASC
      `

      const redemption = await sequelize.query(query, {
        type: Sequelize.QueryTypes.SELECT,
      })

      console.log(redemption, '>>>>>>>>>> ISI REDEMPTION >>>>>>>>>>>')

      res.status(200).json(redemption)
    } catch (error) {
      console.log(error)
    }
  }

  static async getListRedemptionManual(req, res) {
    let { dateFrom, dateTo, region } = req.query
    try {
      const query = `SELECT r.member_id, m.first as first_name, m.last as last_name ,r.reedem_id ,rdm.reedem_name,rdm.reedem_type, 
      r.qty_points as total_qty_points ,r.total_reedem_points as total_redeem_points,
      r.beneficiary ,r.comments ,r.createdAt ,r.createdBy  from ReedemsPoints r 
left outer join Reedems rdm on r.reedem_id =rdm.id
left outer join Members m on r.member_id = m.member_id 
where  r.is_manual = 1 and r.approve_status_id =4 and r.location ='${region}' and r.createdAt between '${dateFrom}' and '${dateTo}'
order by r.member_id asc`

      const redemption = await sequelize.query(query, {
        type: Sequelize.QueryTypes.SELECT,
      })

      console.log(redemption, '>>>>>>>>>> ISI REDEMPTION MANUAL>>>>>>>>>>>')

      res.status(200).json(redemption)
    } catch (error) {
      console.log(error)
    }
  }

  static async getListDining(req, res) {
    let { dateFrom, dateTo, region, outlet } = req.query

    // using UTC time
    const dateFromUTC = new Date(dateFrom)
    const dateToUTC = new Date(dateTo)
    const offset = 8 // Bali (WITA) is UTC+8
    const _dateFrom = convertUTCToLocal(dateFromUTC, offset)
    const _dateTo = convertUTCToLocal(dateToUTC, offset)

    try {
      const dining = await Dining.findAll({
        attributes: [
          [
            Sequelize.fn(
              'DATE_FORMAT',
              Sequelize.col('dining_date'),
              '%m/%d/%Y'
            ),
            'dining_date',
          ],
          'member_id',
          'member_name',
          [Sequelize.literal('`member`.`joined_date`'), 'joined_date'],
          'outlet_id',
          [Sequelize.literal('`outlet`.`outlet_name`'), 'outlet_name'],
          'check_number',
          'location',
          'is_first_dining',
          'approvedBy',
          [
            Sequelize.fn(
              'DATE_FORMAT',
              Sequelize.col('approvedAt'),
              '%m/%d/%Y'
            ),
            'approvedAt',
          ],
          [
            Sequelize.fn('SUM', Sequelize.col('total_bill_rp')),
            'total_bill_rp',
          ],
          [Sequelize.fn('SUM', Sequelize.col('total_bill')), 'total_bill'],
          [
            Sequelize.fn('SUM', Sequelize.col('earned_point')),
            'total_earned_point',
          ],
        ],
        include: [
          {
            model: Outlet,
            as: 'outlet',
            attributes: [],
          },
          {
            model: Member,
            as: 'member',
            attributes: [],
          },
        ],
        where: {
          dining_date: {
            [Op.between]: [_dateFrom, _dateTo],
          },
          location: region,
          outlet_id: outlet,
          is_approved: 1,
        },
        group: [
          'dining_date',
          'member_id',
          'member_name',
          'outlet_id',
          'check_number',
          'location',
          'is_first_dining',
          'approvedBy',
          'approvedAt',
          'outlet.outlet_name', // Tambahkan ke group agar sesuai dengan aggregation
          'member.joined_date',
        ], // Group by fields to aggregate
        order: [['member_id', 'ASC']],
      })

      res.status(200).json(dining)
    } catch (error) {
      console.log(error)
    }
  }

  static async getListVoucher(req, res) {
    let { memberId, reedemId, region } = req.query

    // Initialize the whereClause object
    let whereClause = {}

    // Add member_id filter if provided
    if (memberId) {
      whereClause['member_id'] = memberId
    }

    // Add reedem_id filter if provided
    if (reedemId) {
      whereClause['reedem_id'] = reedemId
    }

    // Add region filter if provided
    if (region) {
      whereClause[Op.and] = [
        whereClause[Op.and] || {}, // Include existing conditions if any
        { '$reedems.location$': region },
      ]
    }

    try {
      const redemption = await MemberVoucher.findAll({
        attributes: [
          'id',
          'reedem_id',
          'reedem_name',
          'member_id',
          'voucher_no',
          'date_expired',
          'is_used',
          'createdBy',
          'updatedBy',
          [
            Sequelize.fn(
              'DATE_FORMAT',
              Sequelize.col('MemberVoucher.createdAt'),
              '%m/%d/%Y'
            ),
            'createdAt',
          ],
          [
            Sequelize.fn(
              'DATE_FORMAT',
              Sequelize.col('MemberVoucher.updatedAt'),
              '%m/%d/%Y'
            ),
            'updatedAt',
          ],
        ],
        include: [
          {
            model: Reedem,
            as: 'reedems',
            attributes: ['reedem_name', 'location'],
          },
        ],
        where: whereClause,
        order: [['createdAt', 'DESC']],
      })

      res.status(200).json(redemption)
    } catch (error) {
      console.log(error)
    }
  }

  static async getVouchersUsed(req, res) {
    let { dateFrom, dateTo, region } = req.query

    const dateObjFrom = new Date(dateFrom)
    const yearFrom = dateObjFrom.getFullYear()
    const monthFrom = dateObjFrom.getMonth() + 1
    const dayFrom = dateObjFrom.getDate()

    const dateObjTo = new Date(dateTo)
    const yearTo = dateObjTo.getFullYear()
    const monthTo = dateObjTo.getMonth() + 1
    const dayTo = dateObjTo.getDate()

    let whereClause = {}

    whereClause = {
      is_used: 1, // Filter for approved status
      [Op.and]: [
        {
          [Op.or]: [
            {
              [Op.and]: [
                Sequelize.where(
                  Sequelize.fn(
                    'YEAR',
                    Sequelize.col('MemberVoucher.updatedAt')
                  ),
                  yearFrom
                ),
                Sequelize.where(
                  Sequelize.fn(
                    'MONTH',
                    Sequelize.col('MemberVoucher.updatedAt')
                  ),
                  monthFrom
                ),
                Sequelize.where(
                  Sequelize.fn('DAY', Sequelize.col('MemberVoucher.updatedAt')),
                  {
                    [Op.between]: [dayFrom, dayTo],
                  }
                ),
              ],
            },
            {
              [Op.and]: [
                Sequelize.where(
                  Sequelize.fn(
                    'YEAR',
                    Sequelize.col('MemberVoucher.updatedAt')
                  ),
                  yearTo
                ),
                Sequelize.where(
                  Sequelize.fn(
                    'MONTH',
                    Sequelize.col('MemberVoucher.updatedAt')
                  ),
                  monthTo
                ),
                Sequelize.where(
                  Sequelize.fn('DAY', Sequelize.col('MemberVoucher.updatedAt')),
                  {
                    [Op.between]: [dayFrom, dayTo],
                  }
                ),
              ],
            },
          ],
        },
        region ? { '$reedems.location$': region } : {},
      ],
    }

    try {
      const dining = await MemberVoucher.findAll({
        attributes: [
          'id',
          'member_id',
          [Sequelize.col('members.name_on_card'), 'name_on_card'],
          'voucher_no',
          [
            Sequelize.fn(
              'DATE_FORMAT',
              Sequelize.col('MemberVoucher.date_expired'),
              '%m/%d/%Y'
            ),
            'date_expired',
          ],
          'reedem_name',
          'outlet_name',
          'createdBy',
          [
            Sequelize.fn(
              'DATE_FORMAT',
              Sequelize.col('MemberVoucher.createdAt'),
              '%m/%d/%Y'
            ),
            'createdAt',
          ],
          'updatedBy',
          [
            Sequelize.fn(
              'DATE_FORMAT',
              Sequelize.col('MemberVoucher.updatedAt'),
              '%m/%d/%Y'
            ),
            'updatedAt',
          ],
        ],
        include: [
          {
            model: Member,
            as: 'members',
            attributes: ['name_on_card'],
          },
          {
            model: Reedem,
            as: 'reedems',
            attributes: ['reedem_name', 'location'],
          },
        ],
        where: whereClause,
        // group: ['member_id','reedem_id','voucher_no'],  // Group by fields to aggregate
        order: [['member_id', 'ASC']],
      })

      res.status(200).json(dining)
    } catch (error) {
      console.log(error)
    }
  }

  static async getProfileByMemberId(req, res) {
    const memberId = req.params.memberId
    try {
      const member = await Member.findAll({
        attributes: [
          'id',
          'name_on_card',
          'member_id',
          'member_level',
          'source',
          'member_type_id',
          'title',
          'first',
          'last',
          'birth_date',
          'primary_email',
          'primary_mobilephone',
          'city',
          'country',
          'joined_date',
          'exp_date',
          'total_points',
          'reedem_points',
          'available_points',
          'is_first_enroll',
          'createdAt',
        ],
        where: {
          member_id: memberId,
        },
      })
      res.status(200).json(member)
    } catch (error) {
      console.log(error)
    }
  }

  static async getProfileByMemberId(req, res) {
    const memberId = req.params.memberId
    try {
      const member = await Member.findAll({
        attributes: [
          'id',
          'name_on_card',
          'member_id',
          'member_level',
          'title',
          'first',
          'last',
          'birth_date',
          'primary_email',
          'primary_mobilephone',
          'city',
          'country',
          'joined_date',
          'exp_date',
          'total_points',
          'reedem_points',
          'available_points',
          'is_first_enroll',
        ],
        where: {
          member_id: memberId,
        },
      })
      res.status(200).json(member)
    } catch (error) {
      console.log(error)
    }
  }

  static async getStatusByMemberId(req, res) {
    const memberId = req.params.memberId
    try {
      const member = await Member.findOne({
        attributes: [
          'id',
          'name_on_card',
          'member_id',
          'first',
          'last',
          'birth_date',
          'primary_email',
          'is_active',
          'activate_date',
        ],
        where: {
          member_id: memberId,
        },
      })
      res.status(200).json(member)
    } catch (error) {
      console.log(error)
    }
  }

  static async getVoucherUsedStatus(req, res) {
    const memberId = req.params.memberId
    const redemPointId = req.params.redemPointId

    try {
      const voucher = await MemberVoucher.findOne({
        attributes: [
          'id',
          'member_id',
          'reedem_point_id',
          'reedem_id',
          'reedem_name',
          'beneficiary',
          'comments',
          'is_used',
        ],
        where: {
          member_id: memberId,
          reedem_point_id: redemPointId,
          is_used: 1,
        },
      })

      res.status(200).json(voucher)
    } catch (error) {
      console.log(error)
    }
  }

  static async updateProfile(req, res) {
    const memberId = req.params.id
    const {
      title,
      first,
      last,
      birth_date,
      primary_email,
      primary_mobilephone,
      city,
      country,
    } = req.body

    //cek apakah member sdh ada
    const member = await Member.findAll({
      attributes: [
        'id',
        'first',
        'last',
        'primary_email',
        'primary_mobilephone',
        'city',
        'country',
      ],
      where: {
        id: memberId,
      },
    })

    if (!member[0]) return res.status(400).json({ msg: 'Member is not exist' })

    try {
      await Member.update(
        {
          title: title,
          first: first,
          last: last,
          birth_date: birth_date,
          primary_email: primary_email,
          primary_mobilephone: primary_mobilephone,
          city: city,
          country: country,
        },
        {
          where: {
            id: memberId,
          },
        }
      )

      res.json({ msg: 'Update profile is successfully' })
    } catch (error) {
      console.log(error)
    }
  }

  // update member by Admin

  static async createMember(req, res) {
    const {
      name_on_card,
      first,
      middle,
      last,
      title,
      gender,
      birth_date,
      city,
      countryid,
      country,
      member_id,
      member_type_id,
      company_id,
      member_level,
      joined_date,
      exp_date,
      exp_level_date,
      total_points,
      reedem_points,
      available_points,
      primary_email,
      other_email,
      is_mailist,
      primary_mobilephone,
      sales_person,
      sales_email,
      is_active,
      activate_date,
      is_first_enroll,
      region,
      username,
    } = req.body

    // using UTC time
    const nowUTC = new Date() // Current UTC time
    const offset = 8 // Bali (WITA) is UTC+8
    const localDateTime = convertUTCToLocal(nowUTC, offset)

    //cek apakah member sdh ada
    const member = await Member.findAll({
      where: {
        primary_email: primary_email,
        is_deleted: 0,
      },
    })

    if (member[0])
      return res.status(400).json({
        msg:
          "Sorry can't enroll, the email address is already registered or already used by " +
          member[0].first +
          ' ' +
          member[0].last,
      })

    try {
      await Member.create({
        name_on_card: name_on_card,
        first: first,
        middle: middle,
        last: last,
        title: title,
        gender: gender,
        birth_date: birth_date,
        city: city,
        countryid: countryid,
        country: country,
        member_id: member_id,
        member_type_id: member_type_id,
        company_id: company_id,
        member_level: member_level,
        joined_date: joined_date,
        exp_date: exp_date,
        exp_level_date: exp_level_date,
        total_points: total_points,
        reedem_points: reedem_points,
        available_points: available_points,
        primary_email: primary_email,
        other_email: other_email,
        is_mailist: is_mailist,
        primary_mobilephone: primary_mobilephone,
        sales_person: sales_person,
        sales_email: sales_email,
        is_active: is_active,
        activate_date: activate_date,
        is_first_enroll: is_first_enroll,
        region: region,
        createdAt: localDateTime,
        createdBy: username,
        updatedAt: localDateTime,
        updatedBy: username,
      })
      res.json({ msg: 'Add new member successfully' })
    } catch (error) {
      console.log(error)
    }
  }

  static async createFbMember(req, res) {
    const {
      name_on_card,
      first,
      middle,
      last,
      title,
      gender,
      birth_date,
      city,
      countryid,
      country,
      source,
      member_id,
      member_type_id,
      member_level,
      joined_date,
      exp_date,
      exp_level_date,
      total_points,
      reedem_points,
      available_points,
      primary_email,
      other_email,
      is_mailist,
      primary_mobilephone,
      is_active,
      activate_date,
      is_first_enroll,
      region,
      username,
    } = req.body

    // using UTC time
    const nowUTC = new Date() // Current UTC time
    const offset = 8 // Bali (WITA) is UTC+8
    const localDateTime = convertUTCToLocal(nowUTC, offset)

    //cek apakah member sdh ada
    const member = await Member.findAll({
      where: {
        is_deleted: 0,
        [Op.or]: [
          { primary_email: primary_email },
          { primary_mobilephone: primary_mobilephone },
        ],
      },
    })

    if (member[0])
      return res.status(400).json({
        msg:
          "Sorry can't enroll, the email address or mobile phone is already used by " +
          member[0].first +
          ' ' +
          member[0].last,
      })

    try {
      await Member.create({
        name_on_card: name_on_card,
        first: first,
        middle: middle,
        last: last,
        title: title,
        gender: gender,
        birth_date: birth_date,
        city: city,
        countryid: countryid,
        country: country,
        source: source,
        member_id: member_id,
        member_type_id: member_type_id,
        member_level: member_level,
        joined_date: joined_date,
        exp_date: exp_date,
        exp_level_date: exp_level_date,
        total_points: total_points,
        reedem_points: reedem_points,
        available_points: available_points,
        primary_email: primary_email,
        other_email: other_email,
        is_mailist: is_mailist,
        primary_mobilephone: primary_mobilephone,
        is_active: is_active,
        activate_date: activate_date,
        is_first_enroll: is_first_enroll,
        region: region,
        createdAt: localDateTime,
        createdBy: username,
        updatedAt: localDateTime,
        updatedBy: username,
      })
      res.json({ msg: 'Add new member successfully' })
    } catch (error) {
      console.log(error)
    }
  }

  static async createFbMember_OLD(req, res) {
    const {
      name_on_card,
      first,
      middle,
      last,
      title,
      gender,
      birth_date,
      city,
      countryid,
      country,
      source,
      member_id,
      member_type_id,
      member_level,
      joined_date,
      exp_date,
      exp_level_date,
      total_points,
      reedem_points,
      available_points,
      primary_email,
      other_email,
      is_mailist,
      primary_mobilephone,
      is_active,
      activate_date,
      is_first_enroll,
      region,
      username,
    } = req.body

    // using UTC time
    const nowUTC = new Date() // Current UTC time
    const offset = 8 // Bali (WITA) is UTC+8
    const localDateTime = convertUTCToLocal(nowUTC, offset)

    //cek apakah member sdh ada
    const member = await Member.findAll({
      where: {
        [Op.or]: [
          { primary_email: primary_email },
          { primary_mobilephone: primary_mobilephone },
        ],
      },
    })

    // Check if a member was found with the provided email or mobile phone
    if (member.length > 0) {
      // Check if the first member has the primary_email
      const existingMember = member.find(
        (m) => m.primary_email === primary_email
      )

      if (existingMember) {
        return res.status(400).json({
          // msg: `The email address is already registered by ${existingMember.first} ${existingMember.last}.`,
          msg: `
                        <p> &nbsp;Email address is already registered</p>
                        <table border="1" cellpadding="5" cellspacing="0" style="border-collapse: collapse; width: 100%;">
                            <tr>
                                <td style="padding: 4px;">Member ID :</td>
                                <td style="padding: 4px;">${existingMember.member_id}</td>
                            </tr>
                            <tr>
                                <td style="padding: 4px;">First Name :</td>
                                <td style="padding: 4px;">${existingMember.first}</td>
                            </tr>
                            <tr>
                                <td style="padding: 4px;">Birth Date :</td>
                                <td style="padding: 4px;">${existingMember.birth_date}</td>
                            </tr>
                            <tr>
                                <td style="padding: 4px;">Last Name :</td>
                                <td style="padding: 4px;">${existingMember.last}</td>
                            </tr>
                            <tr>
                                <td style="padding: 4px;">Email :</td>
                                <td style="padding: 4px;">${existingMember.primary_email}</td>
                            </tr>
                            <tr>
                                <td style="padding: 4px;">Mobile :</td>
                                <td style="padding: 4px;">${existingMember.primary_mobilephone}</td>
                            </tr>
                        </table>
                    `,
        })
      } else {
        // Optionally, handle the case where the email is not matched, but there are other matching records (for mobile phone)
        const mobileMember = member.find(
          (m) => m.primary_mobilephone === primary_mobilephone
        )
        if (mobileMember) {
          return res.status(400).json({
            // msg: `The mobile phone number is already registered by ${mobileMember.first} ${mobileMember.last}.`,
            msg: `
                        <p> &nbsp;Mobile phone number is already registered</p>
                        <table border="1" cellpadding="5" cellspacing="0" style="border-collapse: collapse; width: 100%;">
                            <tr>
                                <td style="padding: 4px;">Member ID :</td>
                                <td style="padding: 4px;">${mobileMember.member_id}</td>
                            </tr>
                            <tr>
                                <td style="padding: 4px;">First Name :</td>
                                <td style="padding: 4px;">${mobileMember.first}</td>
                            </tr>
                             <tr>
                                <td style="padding: 4px;">Birth Date :</td>
                                <td style="padding: 4px;">${mobileMember.birth_date}</td>
                            </tr>
                            <tr>
                                <td style="padding: 4px;">Last Name :</td>
                                <td style="padding: 4px;">${mobileMember.last}</td>
                            </tr>
                            <tr>
                                <td style="padding: 4px;">Email :</td>
                                <td style="padding: 4px;">${mobileMember.primary_email}</td>
                            </tr>
                            <tr>
                                <td style="padding: 4px;">Mobile :</td>
                                <td style="padding: 4px;">${mobileMember.primary_mobilephone}</td>
                            </tr>
                        </table>
                    `,
          })
        }
      }
    }

    //if (member[0]) return res.status(400).json({ msg: "Sorry can't enroll, the email address is already registered or already used by " + member[0].first + " " + member[0].last });

    try {
      await Member.create({
        name_on_card: name_on_card,
        first: first,
        middle: middle,
        last: last,
        title: title,
        gender: gender,
        birth_date: birth_date,
        city: city,
        countryid: countryid,
        country: country,
        source: source,
        member_id: member_id,
        member_type_id: member_type_id,
        member_level: member_level,
        joined_date: joined_date,
        exp_date: exp_date,
        exp_level_date: exp_level_date,
        total_points: total_points,
        reedem_points: reedem_points,
        available_points: available_points,
        primary_email: primary_email,
        other_email: other_email,
        is_mailist: is_mailist,
        primary_mobilephone: primary_mobilephone,
        is_active: is_active,
        activate_date: activate_date,
        is_first_enroll: is_first_enroll,
        region: region,
        createdAt: localDateTime,
        createdBy: username,
        updatedAt: localDateTime,
        updatedBy: username,
      })
      res.json({ msg: 'Add new member successfully' })
    } catch (error) {
      console.log(error)
    }
  }

  static async createMemberLogs(req, res) {
    var memberId = req.params.memberId
    const { logs_location, username } = req.body

    // using UTC time
    const nowUTC = new Date() // Current UTC time
    const offset = 8 // Bali (WITA) is UTC+8
    const localDateTime = convertUTCToLocal(nowUTC, offset)

    //cek apakah member sdh ada
    const member = await Member.findOne({
      where: {
        member_id: memberId,
      },
    })

    if (!member)
      return res.status(400).json({ msg: "Sorry , members doesn't exist" })

    try {
      await MemberLog.create({
        member_id: member.member_id,
        first_name: member.first,
        last_name: member.last,
        name_on_card: member.name_on_card,
        birth_date: member.birth_date,
        member_level: member.member_level,
        joined_date: member.joined_date,
        exp_date: member.exp_date,
        exp_level_date: member.exp_level_date,
        primary_email: member.primary_email,
        total_points: Number(member.total_points),
        reedem_points: Number(member.reedem_points),
        available_points: Number(member.available_points),
        logs_location: logs_location,
        createdAt: localDateTime,
        createdBy: username,
        updatedAt: localDateTime,
        updatedBy: username,
      })
      res.json({ msg: 'Add new member logs successfully' })
    } catch (error) {
      console.log(error)
    }
  }

  static async createEnrollLogs(req, res) {
    const {
      confirmation_no,
      member_id,
      first_name,
      last_name,
      arrival_date,
      departure_date,
      room_class,
      room_idr,
      room_usd,
      fb_idr,
      fb_usd,
      point_promotion,
      is_new_enroll,
      location,
      username,
    } = req.body

    // Get member level
    const member = await Member.findOne({ where: { member_id: member_id } })
    if (!member) {
      return res.status(404).json({ status: 404, msg: 'Settings not found' })
    }
    const member_level = member?.member_level

    let roomPoint = 0
    let fbPoint = 0

    try {
      // Calculate roomPoint and fbPoint
      const room_point = await calculateRoomPoint(member_level, room_usd)
      const fb_point = await calculateEarnPoint(member_level, fb_usd)

      roomPoint = room_point.total_point
      fbPoint = fb_point.total_point
    } catch (error) {
      console.error('Error calculating points:', error.message)
      return res.status(500).json({ msg: 'Failed to calculate points' })
    }

    // using UTC time
    const nowUTC = new Date() // Current UTC time
    const offset = 8 // Bali (WITA) is UTC+8
    const localDateTime = convertUTCToLocal(nowUTC, offset)

    // Terapkan pengalian berdasarkan point_promotion
    switch (point_promotion) {
      case 'DOUBLE':
        roomPoint *= 2
        fbPoint *= 2
        break
      case 'TRIPLE':
        roomPoint *= 3
        fbPoint *= 3
        break
      default:
        // Tidak ada pengalian tambahan jika default
        roomPoint *= 1
        fbPoint *= 1
        break
    }

    // Hitung totalPoints berdasarkan roomPoint dan fbPoint yang sudah disesuaikan
    const totalPoints = roomPoint + fbPoint

    //cek apakah memberid dan confirmation_no sdh ada
    const chkEnrolls = await EnrollsLog.findAll({
      where: {
        confirmation_no: confirmation_no,
        member_id: member_id,
      },
    })

    if (chkEnrolls[0])
      return res.status(400).json({
        msg: "Sorry can't enroll, the memberid and confirmationno is already exist.",
      })

    try {
      await EnrollsLog.create({
        confirmation_no: confirmation_no,
        member_id: member_id,
        first_name: first_name,
        last_name: last_name,
        arrival_date: arrival_date,
        departure_date: departure_date,
        room_class: room_class,
        room_idr: room_idr,
        room_usd: room_usd,
        fb_idr: fb_idr,
        fb_usd: fb_usd,
        room_point: roomPoint,
        fb_point: fbPoint,
        total_points: totalPoints,
        point_promotion: point_promotion,
        is_new_enroll: is_new_enroll,
        location: location,
        createdAt: localDateTime,
        createdBy: username,
        updatedAt: localDateTime,
        updatedBy: username,
      })
      res.json({ msg: 'Add enroll logs is successfully' })
    } catch (error) {
      console.log(error)
    }
  }

  static async create2ndEnrollLogs(req, res) {
    const { member_id, first_name, last_name, total_points, username } =
      req.body

    // using UTC time
    const nowUTC = new Date() // Current UTC time
    const offset = 8 // Bali (WITA) is UTC+8
    const localDateTime = convertUTCToLocal(nowUTC, offset)

    try {
      await SecondPointLog.create({
        member_id: member_id,
        first_name: first_name,
        last_name: last_name,
        total_points: total_points,
        createdAt: localDateTime,
        createdBy: username,
        updatedAt: localDateTime,
        updatedBy: username,
      })
      res.json({ msg: 'Add 2nd enroll logs is successfully' })
    } catch (error) {
      console.log(error)
    }
  }

  static async createUpgradeLogs(req, res) {
    const {
      member_id,
      member_level,
      member_next_level,
      bonus_points,
      location,
      username,
    } = req.body

    // using UTC time
    const nowUTC = new Date() // Current UTC time
    const offset = 8 // Bali (WITA) is UTC+8
    const localDateTime = convertUTCToLocal(nowUTC, offset)

    try {
      await UpgradeLog.create({
        member_id: member_id,
        member_level: member_level,
        member_next_level: member_next_level,
        bonus_points: bonus_points,
        location: location,
        createdAt: localDateTime,
        createdBy: username,
        updatedAt: localDateTime,
        updatedBy: username,
      })
      res.json({ msg: 'Add upgrade logs is successfully' })
    } catch (error) {
      console.log(error)
    }
  }

  static async createDailyTransaction(req, res) {
    const {
      inquiry_date,
      opening_balance,
      new_enroll_bali,
      new_enroll_jakarta,
      existing_enroll_bali,
      existing_enroll_jakarta,
      purchase_point_bali,
      purchase_point_jakarta,
      bonus_point_bali,
      bonus_point_jakarta,
      dining_point_bali,
      dining_point_jakarta,
      redeem_point_bali,
      redeem_point_jakarta,
      closing_balance,
      username,
    } = req.body

    // using UTC time
    const nowUTC = new Date() // Current UTC time
    const offset = 8 // Bali (WITA) is UTC+8
    const localDateTime = convertUTCToLocal(nowUTC, offset)

    try {
      await PointDailyTransaction.create({
        inquiry_date: inquiry_date,
        opening_balance: opening_balance,
        new_enroll_bali: new_enroll_bali,
        new_enroll_jakarta: new_enroll_jakarta,
        existing_enroll_bali: existing_enroll_bali,
        existing_enroll_jakarta: existing_enroll_jakarta,
        purchase_point_bali: purchase_point_bali,
        purchase_point_jakarta: purchase_point_jakarta,
        bonus_point_bali: bonus_point_bali,
        bonus_point_jakarta: bonus_point_jakarta,
        dining_point_bali: dining_point_bali,
        dining_point_jakarta: dining_point_jakarta,
        redeem_point_bali: redeem_point_bali,
        redeem_point_jakarta: redeem_point_jakarta,
        closing_balance: closing_balance,
        createdAt: localDateTime,
        createdBy: username,
        updatedAt: localDateTime,
        updatedBy: username,
      })
      res.json({ msg: 'Add Point Daily Transaction is successfully' })
    } catch (error) {
      console.log(error)
    }
  }

  static async updateDailyTransaction(req, res) {
    const currentDate = req.params.currentDate
    const {
      opening_balance,
      new_enroll_bali,
      new_enroll_jakarta,
      existing_enroll_bali,
      existing_enroll_jakarta,
      purchase_point_bali,
      purchase_point_jakarta,
      bonus_point_bali,
      bonus_point_jakarta,
      dining_point_bali,
      dining_point_jakarta,
      redeem_point_bali,
      redeem_point_jakarta,
      closing_balance,
      username,
    } = req.body

    const dateObj = new Date(currentDate)

    const year = dateObj.getFullYear()
    const month = dateObj.getMonth() + 1
    const day = dateObj.getDate()

    let whereClause = {}

    whereClause = {
      [Op.and]: [
        Sequelize.where(
          Sequelize.fn('YEAR', Sequelize.col('inquiry_date')),
          year
        ),
        Sequelize.where(
          Sequelize.fn('MONTH', Sequelize.col('inquiry_date')),
          month
        ),
        Sequelize.where(
          Sequelize.fn('DAY', Sequelize.col('inquiry_date')),
          day
        ),
      ],
    }

    //cek apakah member sdh ada
    const point = await PointDailyTransaction.findAll({
      attributes: ['id', 'opening_balance', 'closing_balance'],
      where: whereClause,
    })

    if (!point[0])
      return res.status(400).json({ msg: 'Point Transaction is not exist' })

    try {
      await PointDailyTransaction.update(
        {
          opening_balance: opening_balance,
          new_enroll_bali: new_enroll_bali,
          new_enroll_jakarta: new_enroll_jakarta,
          existing_enroll_bali: existing_enroll_bali,
          existing_enroll_jakarta: existing_enroll_jakarta,
          purchase_point_bali: purchase_point_bali,
          purchase_point_jakarta: purchase_point_jakarta,
          bonus_point_bali: bonus_point_bali,
          bonus_point_jakarta: bonus_point_jakarta,
          dining_point_bali: dining_point_bali,
          dining_point_jakarta: dining_point_jakarta,
          redeem_point_bali: redeem_point_bali,
          redeem_point_jakarta: redeem_point_jakarta,
          closing_balance: closing_balance,
          username: username,
        },
        {
          where: whereClause,
        }
      )

      res.json({ msg: 'Update Point Transaction is successfully' })
    } catch (error) {
      console.log(error)
    }
  }

  static async updateInitBalance(req, res) {
    const id = req.params.id
    const { opening_balance, closing_balance, username } = req.body

    try {
      await PointDailyTransaction.update(
        {
          opening_balance: opening_balance,
          closing_balance: closing_balance,
          username: username,
        },
        {
          where: { id: id },
        }
      )

      res.json({
        msg: 'Update Init Balance Point Daily Transaction is successfully',
      })
    } catch (error) {
      console.log(error)
    }
  }

  static async getTotalPointsByMemberId(req, res) {
    const memberId = req.params.memberId
    try {
      const reservs = await EnrollsLog.findAll({
        attributes: [
          'member_id',
          [Sequelize.fn('SUM', Sequelize.col('total_points')), 'total_points'],
        ],
        where: {
          member_id: memberId,
        },
        group: ['member_id'],
      })
      res.status(200).json(reservs)
    } catch (error) {
      console.log(error)
    }
  }

  static async getMember(req, res) {
    const memberId = req.params.id
    try {
      const member = await Member.findOne({
        attributes: [
          'id',
          'name_on_card',
          'member_id',
          'member_type_id',
          'company_id',
          'member_level',
          'title',
          'first',
          'middle',
          'last',
          'gender',
          'birth_date',
          'primary_email',
          'other_email',
          'password',
          'primary_mobilephone',
          'sales_person',
          'sales_email',
          'city',
          'countryid',
          'country',
          'joined_date',
          'exp_date',
          'exp_level_date',
          'total_points',
          'reedem_points',
          'available_points',
          'is_mailist',
          'is_active',
          'activate_date',
        ],
        where: {
          id: memberId,
        },
      })
      res.status(200).json(member)
    } catch (error) {
      console.log(error)
    }
  }

  static async updateMember(req, res) {
    const memberId = req.params.id
    const {
      name_on_card,
      first,
      middle,
      last,
      title,
      gender,
      birth_date,
      city,
      countryid,
      country,
      member_id,
      member_type_id,
      company_id,
      member_level,
      joined_date,
      exp_date,
      exp_level_date,
      primary_email,
      other_email,
      is_mailist,
      primary_mobilephone,
      sales_person,
      sales_email,
      is_active,
      activate_date,
      username,
    } = req.body

    // using UTC time
    const nowUTC = new Date() // Current UTC time
    const offset = 8 // Bali (WITA) is UTC+8
    const localDateTime = convertUTCToLocal(nowUTC, offset)

    //cek apakah member sdh ada
    const member = await Member.findAll({
      attributes: [
        'id',
        'first',
        'last',
        'primary_email',
        'primary_mobilephone',
        'city',
        'country',
      ],
      where: {
        id: memberId,
      },
    })

    if (!member[0]) return res.status(400).json({ msg: 'Member is not exist' })

    if (primary_email === '')
      return res.status(400).json({ msg: 'Email address is required!' })

    try {
      await Member.update(
        {
          name_on_card: name_on_card,
          first: first,
          middle: middle,
          last: last,
          title: title,
          gender: gender,
          birth_date: birth_date,
          city: city,
          countryid: countryid,
          country: country,
          member_id: member_id,
          member_type_id: member_type_id,
          company_id: company_id,
          member_level: member_level,
          joined_date: joined_date,
          exp_date: exp_date,
          exp_level_date: exp_level_date,
          primary_email: primary_email,
          other_email: other_email,
          is_mailist: is_mailist,
          primary_mobilephone: primary_mobilephone,
          sales_person: sales_person,
          sales_email: sales_email,
          is_active: is_active,
          activate_date: activate_date,
          updatedAt: localDateTime,
          updatedBy: username,
        },
        {
          where: {
            id: memberId,
          },
        }
      )
      res.status(200).json({ msg: 'Update profile is successfully' })
    } catch (error) {
      console.log(error)
    }
  }

  static async updatePassword(req, res) {
    const memberId = req.params.id
    const { tempPass, username } = req.body

    // using UTC time
    const nowUTC = new Date() // Current UTC time
    const offset = 8 // Bali (WITA) is UTC+8
    const localDateTime = convertUTCToLocal(nowUTC, offset)

    //cek apakah member sdh ada
    const member = await Member.findAll({
      attributes: [
        'id',
        'first',
        'last',
        'primary_email',
        'primary_mobilephone',
        'city',
        'country',
      ],
      where: {
        id: memberId,
      },
    })

    if (!member[0]) return res.status(400).json({ msg: 'Member is not exist' })

    // generate password disini
    const saltRounds = 10 // The number of salt rounds for bcrypt
    const salt = await bcrypt.genSalt(saltRounds)
    const hashPassword = await bcrypt.hash(tempPass, salt)

    try {
      await Member.update(
        {
          password: hashPassword,
          updatedAt: localDateTime,
          updatedBy: username,
        },
        {
          where: {
            id: memberId,
          },
        }
      )
      res.status(200).json({ msg: 'Update password is successfully' })
    } catch (error) {
      console.log(error)
    }
  }

  static async generatePassword(req, res) {
    const email = req.params.email
    const { tempPass, username } = req.body

    // using UTC time
    const nowUTC = new Date() // Current UTC time
    const offset = 8 // Bali (WITA) is UTC+8
    const localDateTime = convertUTCToLocal(nowUTC, offset)

    //cek apakah member sdh ada
    const member = await Member.findAll({
      attributes: [
        'id',
        'first',
        'last',
        'primary_email',
        'primary_mobilephone',
        'city',
        'country',
      ],
      where: {
        primary_email: email,
      },
    })

    if (!member[0]) return res.status(400).json({ msg: 'Member is not exist' })

    // generate password disini
    const saltRounds = 10 // The number of salt rounds for bcrypt
    const salt = await bcrypt.genSalt(saltRounds)
    const hashPassword = await bcrypt.hash(tempPass, salt)

    try {
      await Member.update(
        {
          temp_password: tempPass,
          password: hashPassword,
          updatedAt: localDateTime,
          updatedBy: username,
        },
        {
          where: {
            primary_email: email,
          },
        }
      )
      res.status(200).json({ msg: 'Update password is successfully' })
    } catch (error) {
      console.log(error)
    }
  }

  static async generatePasswordFb(req, res) {
    const email = req.params.email
    const { tempPass, username } = req.body

    // using UTC time
    const nowUTC = new Date() // Current UTC time
    const offset = 8 // Bali (WITA) is UTC+8
    const localDateTime = convertUTCToLocal(nowUTC, offset)
    const formattedDate = formatDateToYMD(localDateTime)

    //cek apakah member sdh ada
    const member = await Member.findAll({
      attributes: [
        'id',
        'first',
        'last',
        'primary_email',
        'primary_mobilephone',
        'city',
        'country',
      ],
      where: {
        primary_email: email,
      },
    })

    if (!member[0]) return res.status(400).json({ msg: 'Member is not exist' })

    // generate password disini
    const saltRounds = 10 // The number of salt rounds for bcrypt
    const salt = await bcrypt.genSalt(saltRounds)
    const hashPassword = await bcrypt.hash(tempPass, salt)

    try {
      await Member.update(
        {
          is_active: 1,
          is_first_enroll: 1,
          activate_date: formattedDate,
          temp_password: tempPass,
          password: hashPassword,
          updatedAt: localDateTime,
          updatedBy: member[0].first,
        },
        {
          where: {
            primary_email: email,
          },
        }
      )
      res.status(200).json({ msg: 'Update password is successfully' })
    } catch (error) {
      console.log(error)
    }
  }

  static async initPoints(req, res) {
    const memberId = req.params.memberId
    const { total_points, reedem_points, available_points } = req.body

    //cek apakah member sdh ada
    const member = await Member.findOne({
      attributes: [
        'id',
        'first',
        'last',
        'primary_email',
        'primary_mobilephone',
        'city',
        'country',
      ],
      where: {
        member_id: memberId,
      },
    })

    if (!member[0]) return res.status(400).json({ msg: 'Member is not exist' })

    try {
      await Member.update(
        {
          total_points: total_points,
          reedem_points: reedem_points,
          available_points: available_points,
        },
        {
          where: {
            member_id: memberId,
          },
        }
      )

      res.json({ msg: 'Initial member points is successfully' })
    } catch (error) {
      console.log(error)
    }
  }

  static async getOpenBalance(req, res) {
    let { previousDate } = req.query

    const dateObj = new Date(previousDate)

    const year = dateObj.getFullYear()
    const month = dateObj.getMonth() + 1
    const day = dateObj.getDate()

    let whereClause = {}

    whereClause = {
      [Op.and]: [
        Sequelize.where(
          Sequelize.fn('YEAR', Sequelize.col('inquiry_date')),
          year
        ),
        Sequelize.where(
          Sequelize.fn('MONTH', Sequelize.col('inquiry_date')),
          month
        ),
        Sequelize.where(
          Sequelize.fn('DAY', Sequelize.col('inquiry_date')),
          day
        ),
      ],
    }

    try {
      const openbalance = await PointDailyTransaction.findAll({
        attributes: ['opening_balance'],
        where: whereClause,
      })
      res.status(200).json(openbalance)
    } catch (error) {
      console.log(error)
    }
  }

  static async getInitBalance(req, res) {
    let whereClause = {}

    whereClause = {
      is_deleted: 0,
      exp_date: {
        [Op.gte]: new Date(), // CURRENT_DATE() equivalent
      },
    }

    try {
      const initbalance = await Member.findAll({
        attributes: [
          [
            Sequelize.fn(
              'FLOOR',
              Sequelize.fn('SUM', Sequelize.col('available_points'))
            ),
            'init_points',
          ],
        ],
        where: whereClause,
      })
      res.status(200).json(initbalance)
    } catch (error) {
      console.log(error)
    }
  }

  static async updatePoints(req, res) {
    const memberId = req.params.memberId
    const { totalBills } = req.body // berdasarkan data yang diselect di enroll modal

    if (!totalBills || typeof totalBills !== 'number') {
      return res.status(400).json({ msg: 'Total bills is invalid or missing' })
    }

    const settings = await Setting.findAll({
      attributes: [
        'id',
        'fb_amount',
        'fb_point_silver',
        'fb_point_gold',
        'fb_point_diamond',
      ],
    })

    if (!settings.length) {
      return res.status(400).json({ msg: 'Setting loyalty does not exist' })
    }

    const {
      fb_point_silver: var_FB_Point_Silver,
      fb_point_gold: var_FB_Point_Gold,
      fb_point_diamond: var_FB_Point_Diamond,
      fb_amount: var_FB_Amount,
    } = settings[0]

    //cek apakah member sdh ada
    const members = await Member.findAll({
      attributes: [
        'id',
        'first',
        'last',
        'primary_email',
        'primary_mobilephone',
        'city',
        'country',
        'reedem_points',
        'total_points',
        'available_points',
        'member_level',
      ],
      where: {
        member_id: memberId,
      },
    })

    // Get member level
    const member = await Member.findOne({ where: { member_id: memberId } })
    if (!member) {
      return res.status(404).json({ status: 404, msg: 'Settings not found' })
    }

    if (!members.length) {
      return res.status(400).json({ msg: 'Member does not exist' })
    }

    const memberData = members[0]
    const member_level = memberData.member_level

    let totalPoints = 0
    switch (member_level) {
      case 'MPS':
        totalPoints = Math.floor(
          totalBills * (var_FB_Point_Silver / var_FB_Amount)
        )
        break
      case 'MPG':
        totalPoints = Math.floor(
          totalBills * (var_FB_Point_Gold / var_FB_Amount)
        )
        break
      case 'MPD':
        totalPoints = Math.floor(
          totalBills * (var_FB_Point_Diamond / var_FB_Amount)
        )
        break
      default:
        totalPoints = Math.floor(
          totalBills * (var_FB_Point_Silver / var_FB_Amount)
        )
        break
    }

    const totPoints = Number(memberData.total_points || 0) + totalPoints
    const reedemPoints = Number(memberData.reedem_points || 0)
    const availablePoints = totPoints - reedemPoints

    try {
      await Member.update(
        {
          total_points: totPoints,
          reedem_points: reedemPoints,
          available_points: availablePoints,
        },
        {
          where: {
            member_id: memberId,
          },
        }
      )

      res.json({ msg: 'Update point member is successfully' })
    } catch (error) {
      console.error(error)
      res
        .status(500)
        .json({ msg: 'An error occurred while updating member points' })
    }
  }

  static async updateTotalPoints(req, res) {
    const memberId = req.params.memberId
    const { totalPoints } = req.body // berdasarkan data yang diselect di enroll modal
    let availablePoints = 0
    let totPoints = 0
    let reedemPoints = 0

    //cek apakah member sdh ada
    const member = await Member.findAll({
      attributes: [
        'id',
        'first',
        'last',
        'primary_email',
        'primary_mobilephone',
        'city',
        'country',
        'reedem_points',
        'total_points',
        'available_points',
      ],
      where: {
        member_id: memberId,
      },
    })

    if (!member[0]) return res.status(400).json({ msg: 'Member is not exist' })

    totPoints = Number(member[0].total_points) + Number(totalPoints)
    reedemPoints = Number(member[0].reedem_points)
    availablePoints = totPoints - reedemPoints

    try {
      await Member.update(
        {
          total_points: totPoints,
          reedem_points: reedemPoints,
          available_points: availablePoints,
        },
        {
          where: {
            member_id: memberId,
          },
        }
      )

      res.json({ msg: 'Update point member is successfully' })
    } catch (error) {
      console.log(error)
    }
  }

  static async updateExpDate(req, res) {
    const memberId = req.params.memberId
    const { exp_date, username } = req.body // berdasarkan data yang diselect di enroll modal

    // using UTC time
    const nowUTC = new Date() // Current UTC time
    const offset = 8 // Bali (WITA) is UTC+8
    const localDateTime = convertUTCToLocal(nowUTC, offset)

    //cek apakah member sdh ada
    const member = await Member.findAll({
      attributes: [
        'id',
        'first',
        'last',
        'primary_email',
        'primary_mobilephone',
        'city',
        'country',
        'reedem_points',
        'total_points',
        'available_points',
      ],
      where: {
        member_id: memberId,
      },
    })

    if (!member[0]) return res.status(400).json({ msg: 'Member is not exist' })

    try {
      await Member.update(
        {
          exp_date: exp_date,
          updatedAt: localDateTime,
          updatedBy: username,
        },
        {
          where: {
            member_id: memberId,
          },
        }
      )

      res.json({ msg: 'Update Expired Date member is successfully' })
    } catch (error) {
      console.log(error)
    }
  }

  static async updateBirthDate(req, res) {
    const memberId = req.params.memberId
    const { birth_date, username } = req.body // berdasarkan data yang diselect di enroll modal

    // using UTC time
    const nowUTC = new Date() // Current UTC time
    const offset = 8 // Bali (WITA) is UTC+8
    const localDateTime = convertUTCToLocal(nowUTC, offset)

    //cek apakah member sdh ada
    const member = await Member.findAll({
      attributes: [
        'id',
        'first',
        'last',
        'primary_email',
        'primary_mobilephone',
        'city',
        'country',
        'reedem_points',
        'total_points',
        'available_points',
      ],
      where: {
        member_id: memberId,
      },
    })

    if (!member[0]) return res.status(400).json({ msg: 'Member is not exist' })

    try {
      await Member.update(
        {
          birth_date: birth_date,
          updatedAt: localDateTime,
          updatedBy: username,
        },
        {
          where: {
            member_id: memberId,
          },
        }
      )

      res.json({ msg: 'Update Birth Date member is successfully' })
    } catch (error) {
      console.log(error)
    }
  }

  static async adjustPoints(req, res) {
    const memberId = req.params.memberId
    const { totalPoints } = req.body // berdasarkan data yang diselect di enroll modal
    let availablePoints = 0
    let totPoints = 0
    let reedemPoints = 0

    //cek apakah member sdh ada
    const member = await Member.findAll({
      attributes: [
        'id',
        'first',
        'last',
        'primary_email',
        'primary_mobilephone',
        'city',
        'country',
        'reedem_points',
        'total_points',
        'available_points',
      ],
      where: {
        member_id: memberId,
      },
    })

    if (!member[0]) return res.status(400).json({ msg: 'Member is not exist' })

    totPoints = Number(member[0].total_points) - Number(totalPoints)
    reedemPoints = Number(member[0].reedem_points)
    availablePoints = totPoints - reedemPoints

    try {
      await Member.update(
        {
          total_points: totPoints,
          reedem_points: reedemPoints,
          available_points: availablePoints,
        },
        {
          where: {
            member_id: memberId,
          },
        }
      )

      res.json({ msg: 'Update point member is successfully' })
    } catch (error) {
      console.log(error)
    }
  }

  static async updateLevelPoints(req, res) {
    const memberId = req.params.memberId
    const { totalPoints, nextLevel, expLevelDate } = req.body // berdasarkan data yang diselect di enroll modal
    let availablePoints = 0
    let totPoints = 0
    let reedemPoints = 0

    //cek apakah member sdh ada
    const member = await Member.findAll({
      attributes: [
        'id',
        'first',
        'last',
        'primary_email',
        'primary_mobilephone',
        'city',
        'country',
        'reedem_points',
        'total_points',
        'available_points',
      ],
      where: {
        member_id: memberId,
      },
    })

    if (!member[0]) return res.status(400).json({ msg: 'Member is not exist' })

    totPoints = Number(member[0].total_points) + Number(totalPoints)
    reedemPoints = Number(member[0].reedem_points)
    availablePoints = totPoints - reedemPoints

    try {
      await Member.update(
        {
          member_level: nextLevel,
          total_points: totPoints,
          reedem_points: reedemPoints,
          available_points: availablePoints,
          exp_level_date: expLevelDate,
        },
        {
          where: {
            member_id: memberId,
          },
        }
      )

      res.json({ msg: 'Update point member is successfully' })
    } catch (error) {
      console.log(error)
    }
  }

  static async replacePoints(req, res) {
    const memberId = req.params.memberId
    const { totalPoints } = req.body // berdasarkan data yang diselect di enroll modal
    let availablePoints = totalPoints

    //cek apakah member sdh ada
    const member = await Member.findAll({
      attributes: [
        'id',
        'first',
        'last',
        'primary_email',
        'primary_mobilephone',
        'city',
        'country',
        'reedem_points',
        'total_points',
        'available_points',
      ],
      where: {
        member_id: memberId,
      },
    })

    if (!member[0]) return res.status(400).json({ msg: 'Member is not exist' })

    try {
      await Member.update(
        {
          total_points: totalPoints,
          available_points: availablePoints,
        },
        {
          where: {
            member_id: memberId,
          },
        }
      )

      res.json({ msg: 'Update point member is successfully' })
    } catch (error) {
      console.log(error)
    }
  }

  static async updateReedemPoints(req, res) {
    const memberId = req.params.memberId
    const { reedem_points } = req.body
    let availablePoint = 0

    //cek apakah member sdh ada
    const member = await Member.findAll({
      attributes: [
        'id',
        'first',
        'last',
        'primary_email',
        'primary_mobilephone',
        'city',
        'country',
        'total_points',
      ],
      where: {
        member_id: memberId,
      },
    })

    if (!member[0]) return res.status(400).json({ msg: 'Member is not exist' })

    availablePoint = Number(member[0].total_points) - reedem_points

    try {
      await Member.update(
        {
          reedem_points: reedem_points,
          available_points: availablePoint,
        },
        {
          where: {
            member_id: memberId,
          },
        }
      )

      res.json({ msg: 'Update reedem points is successfully' })
    } catch (error) {
      console.log(error)
    }
  }

  static async updateNotif(req, res) {
    const memberId = req.params.memberId
    const { notifications } = req.body

    //cek apakah member sdh ada
    const member = await Member.findAll({
      attributes: [
        'id',
        'first',
        'last',
        'primary_email',
        'primary_mobilephone',
        'city',
        'country',
      ],
      where: {
        member_id: memberId,
      },
    })

    if (!member[0]) return res.status(400).json({ msg: 'Member is not exist' })

    try {
      await Member.update(
        {
          notifications: notifications,
        },
        {
          where: {
            member_id: memberId,
          },
        }
      )

      res.json({ msg: 'Send email notifications is successfully' })
    } catch (error) {
      console.log(error)
    }
  }

  static async updateIsFirstEnroll(req, res) {
    const memberId = req.params.memberId
    const { is_first_enroll, username } = req.body

    // using UTC time
    const nowUTC = new Date() // Current UTC time
    const offset = 8 // Bali (WITA) is UTC+8
    const localDateTime = convertUTCToLocal(nowUTC, offset)

    //cek apakah member sdh ada
    const member = await Member.findAll({
      attributes: [
        'id',
        'first',
        'last',
        'primary_email',
        'primary_mobilephone',
        'city',
        'country',
      ],
      where: {
        member_id: memberId,
      },
    })

    if (!member[0]) return res.status(400).json({ msg: 'Member is not exist' })

    try {
      await Member.update(
        {
          is_first_enroll: is_first_enroll,
          updatedAt: localDateTime,
          updatedBy: username,
        },
        {
          where: {
            member_id: memberId,
          },
        }
      )

      res.json({ msg: 'Update is first enroll is successfully' })
    } catch (error) {
      console.log(error)
    }
  }

  static async cancelReedemPoints(req, res) {
    const id = req.params.id
    const { member_id, total_reedem_points, surcharge_points, username } =
      req.body

    // using UTC time
    const nowUTC = new Date() // Current UTC time
    const offset = 8 // Bali (WITA) is UTC+8
    const localDateTime = convertUTCToLocal(nowUTC, offset)

    //cek apakah reedem points sdh ada
    const reedem = await ReedemsPoint.findAll({
      attributes: [
        'id',
        'member_id',
        'reedem_id',
        'total_reedem_points',
        'surcharge_points',
        'comments',
        'is_cancel',
      ],
      where: {
        id: id,
      },
    })

    if (!reedem[0])
      return res.status(400).json({ msg: 'Reedempoints is not exist' })

    try {
      // cancel reedem points berdasarkan id
      await ReedemsPoint.update(
        {
          is_cancel: 1, // set reedem points as cancel
          updatedAt: localDateTime,
          updatedBy: username,
        },
        {
          where: {
            id: id,
          },
        }
      )

      //cek member table
      const member = await Member.findOne({
        where: {
          member_id: member_id,
        },
      })

      if (member) {
        //update reedem points and available points
        await Member.update(
          {
            reedem_points:
              parseInt(member.reedem_points) -
              (parseInt(total_reedem_points) + parseInt(surcharge_points)),
            available_points:
              parseInt(member.available_points) +
              (parseInt(total_reedem_points) + parseInt(surcharge_points)),
          },
          {
            where: {
              member_id: member_id,
            },
          }
        )
      }

      res.json({ msg: 'Cancel reedem points is successfully' })
    } catch (error) {
      console.log(error)
    }
  }

  static async cancelVoucher(req, res) {
    const id = req.params.id
    const { is_used, username } = req.body

    // using UTC time
    const nowUTC = new Date() // Current UTC time
    const offset = 8 // Bali (WITA) is UTC+8
    const localDateTime = convertUTCToLocal(nowUTC, offset)

    //cek apakah reedem points sdh ada
    const voucher = await MemberVoucher.findAll({
      where: {
        id: id,
      },
    })

    if (!voucher[0])
      return res.status(400).json({ msg: 'Voucher is not exist' })

    try {
      // cancel reedem points berdasarkan id
      await MemberVoucher.update(
        {
          is_used: is_used, // set reedem points as cancel
          updatedAt: localDateTime,
          updatedBy: username,
        },
        {
          where: {
            id: id,
          },
        }
      )
      res.json({ msg: 'Cancel voucher is successfully' })
    } catch (error) {
      console.log(error)
    }
  }

  static async createPointLogs(req, res) {
    const {
      confirmation_no,
      member_id,
      first_name,
      last_name,
      arrival_date,
      departure_date,
      room_class,
      room_usd,
      fb_usd,
      room_point,
      fb_point,
      total_points,
      createdAt,
      createdBy,
      updatedAt,
      updatedBy,
    } = req.body

    try {
      await PointsLog.create({
        confirmation_no: confirmation_no,
        member_id: member_id,
        first_name: first_name,
        last_name: last_name,
        arrival_date: arrival_date,
        departure_date: departure_date,
        room_class: room_class,
        room_usd: room_usd,
        fb_usd: fb_usd,
        room_point: room_point,
        fb_point: fb_point,
        total_points: total_points,
        createdAt: createdAt,
        createdBy: createdBy,
        updatedAt: updatedAt,
        updatedBy: updatedBy,
      })
      res.json({ msg: 'Add new points logs successfully' })
    } catch (error) {
      console.log(error)
    }
  }

  static async updateEnrollLogs(req, res) {
    const confNo = req.params.confNo
    const { room_usd, fb_usd, room_point, fb_point, total_points, username } =
      req.body

    // using UTC time
    const nowUTC = new Date() // Current UTC time
    const offset = 8 // Bali (WITA) is UTC+8
    const localDateTime = convertUTCToLocal(nowUTC, offset)

    //cek apakah member sdh ada
    const enroll = await EnrollsLog.findAll({
      where: {
        confirmation_no: confNo,
      },
    })

    if (!enroll[0])
      return res.status(400).json({ msg: 'Enroll logs is not exist' })

    try {
      await EnrollsLog.update(
        {
          room_usd: room_usd,
          fb_usd: fb_usd,
          room_point: room_point,
          fb_point: fb_point,
          total_points: total_points,
          updatedAt: localDateTime,
          updatedBy: username,
        },
        {
          where: {
            confirmation_no: confNo,
          },
        }
      )

      res.json({ msg: 'Update enrolls logs is successfully' })
    } catch (error) {
      console.log(error)
    }
  }

  static async deleteMember(req, res) {
    const memberId = req.params.id
    const { is_deleted, username } = req.body

    // using UTC time
    const nowUTC = new Date() // Current UTC time
    const offset = 8 // Bali (WITA) is UTC+8
    const localDateTime = convertUTCToLocal(nowUTC, offset)
    //cek apakah language sdh ada
    const member = await Member.findAll({
      where: {
        id: memberId,
      },
    })

    if (!member[0]) return res.status(400).json({ msg: 'Member is not exist' })

    try {
      await Member.update(
        {
          is_deleted: is_deleted,
          updatedAt: localDateTime,
          updatedBy: username,
        },
        {
          where: {
            id: memberId,
          },
        }
      )

      res.json({ msg: 'Delete Member is successfully' })
    } catch (error) {
      console.log(error)
    }
  }

  static async deleteMemberOLD(req, res) {
    const memberId = req.params.id
    //cek apakah language sdh ada
    const member = await Member.findAll({
      where: {
        id: memberId,
      },
    })

    if (!member[0]) return res.status(400).json({ msg: 'Member is not exist' })

    try {
      await Member.destroy({
        where: {
          id: memberId,
        },
      })

      res.json({ msg: 'Delete Member is successfully' })
    } catch (error) {
      console.log(error)
    }
  }

  static async rewardClaim(req, res) {
    const {
      member_id,
      reedem_id,
      required_points,
      qty_points,
      total_points,
      surcharge_type_id,
      surcharge_points,
      beneficiary,
      comments,
      location,
      is_manual,
      username,
    } = req.body

    console.log(req.body, 'ISI BODY DI API >>>>>>>>>>>>>>')
    // using UTC time
    const nowUTC = new Date() // Current UTC time
    const offset = 8 // Bali (WITA) is UTC+8
    const localDateTime = convertUTCToLocal(nowUTC, offset)

    //cek member table
    const member = await Member.findOne({
      where: {
        member_id: member_id,
      },
    })

    if (!member) {
      return res.status(404).json({ error: 'Member not found' })
    }

    // Check if the member has enough points
    if (
      parseInt(member.total_points) <
      parseInt(total_points) + parseInt(surcharge_points) ||
      member.available_points < total_points + surcharge_points
    ) {
      return res
        .status(400)
        .json({ error: 'Insufficient points for this transaction' })
    }

    let prev_balance = 0
    let remain_point = 0

    if (member) {
      //update reedem points and available points
      prev_balance =
        parseInt(member.total_points) - parseInt(member.reedem_points)
      remain_point =
        parseInt(member.total_points) -
        (parseInt(member.reedem_points) +
          parseInt(total_points) +
          parseInt(surcharge_points))
      await Member.update(
        {
          reedem_points:
            parseInt(member.reedem_points) +
            parseInt(total_points) +
            parseInt(surcharge_points),
          available_points:
            parseInt(member.total_points) -
            (parseInt(member.reedem_points) +
              parseInt(total_points) +
              parseInt(surcharge_points)),
        },
        {
          where: {
            member_id: member_id,
          },
        }
      )
    }

    try {
      const data = await ReedemsPoint.create({
        member_id: member_id,
        reedem_id: reedem_id,
        previous_balance: prev_balance,
        required_points: required_points,
        qty_points: qty_points,
        total_reedem_points: total_points,
        remaining_points: remain_point,
        surcharge_type_id: surcharge_type_id,
        surcharge_points: surcharge_points,
        beneficiary: beneficiary,
        comments: comments,
        location: location,
        is_manual: is_manual,
        createdBy: username,
        createdAt: localDateTime,
        updatedBy: username,
        updatedAt: localDateTime,
      })

      // res.json(data, { msg: "Reward claim form is successfully" });
      res.status(200).json({
        data,
        msg: 'Reward claim form is successfully',
      })
    } catch (error) {
      console.log(error)
    }
  }

  static async rewardClaimMulti(req, res) {
    const {
      member_id,
      PreviousBalance,
      PointClaimedToday,
      RemainingBalance,
      voucher_location,
      is_manual,
      username,
    } = req.body

    console.log(req.body, 'ISI BODY DI API >>>>>>>>>>>>>>')
    // using UTC time
    const nowUTC = new Date() // Current UTC time
    const offset = 8 // Bali (WITA) is UTC+8
    const localDateTime = convertUTCToLocal(nowUTC, offset)

    //cek member table
    const member = await Member.findOne({
      where: {
        member_id: member_id,
      },
    })

    if (!member) {
      return res.status(404).json({ error: 'Member not found' })
    }

    if (member) {
      //update reedem points and available points
      await Member.update(
        {
          reedem_points:
            parseInt(member.reedem_points) + parseInt(PointClaimedToday),
          available_points:
            parseInt(member.total_points) -
            (parseInt(member.reedem_points) + parseInt(PointClaimedToday)),
        },
        {
          where: {
            member_id: member_id,
          },
        }
      )
    }

    try {
      const data = await ReedemsPoint.create({
        member_id: member_id,
        previous_balance: PreviousBalance,
        total_reedem_points: PointClaimedToday,
        remaining_points: RemainingBalance,
        surcharge_type_id: 0,
        surcharge_points: 0,
        location: voucher_location,
        is_manual: is_manual,
        createdBy: username,
        createdAt: localDateTime,
        updatedBy: username,
        updatedAt: localDateTime,
      })

      // res.json(data, { msg: "Reward claim form is successfully" });
      res.status(200).json({
        data,
        msg: 'Reward claim form is successfully',
      })
    } catch (error) {
      console.log(error)
    }
  }

  static async rewardClaimItems(req, res) {
    const {
      reedem_point_id,
      reedem_id,
      cart_voucherName,
      redem_type_id,
      cart_voucherType,
      cart_location,
      required_points,
      qty_points,
      total_points,
      beneficiary,
      comments,
      username,
    } = req.body
    console.log(req.body, 'ISI BODY DI API >>>>>>>>>>>>>>')
    // using UTC time
    const nowUTC = new Date() // Current UTC time
    const offset = 8 // Bali (WITA) is UTC+8
    const localDateTime = convertUTCToLocal(nowUTC, offset)

    try {
      const data = await RedemsPointItem.create({
        redem_point_id: reedem_point_id,
        redem_id: reedem_id,
        redem_name: cart_voucherName,
        redem_type_id: redem_type_id,
        redem_type: cart_voucherType,
        redem_location: cart_location,
        required_points: required_points,
        qty_points: qty_points,
        total_redem_points: total_points,
        beneficiary: beneficiary,
        comments: comments,
        createdBy: username,
        createdAt: localDateTime,
        updatedBy: username,
        updatedAt: localDateTime,
      })

      // res.json(data, { msg: "Reward claim form is successfully" });
      res.status(200).json({
        data,
        msg: 'Reward claim form is successfully',
      })
    } catch (error) {
      console.log(error)
    }
  }

  static async getReedemPoints(req, res) {
    const memberId = req.params.memberId
    try {
      const reedem = await ReedemsPoint.findAll({
        attributes: [
          'id',
          'member_id',
          'reedem_id',
          'total_reedem_points',
          'surcharge_type_id',
          [
            Sequelize.literal(`
                  CASE 
                    WHEN surcharge_type_id = 1 THEN 'High session surcharge' 
                    WHEN surcharge_type_id = 2 THEN 'Peak session surcharge' 
                    ELSE '' 
                  END
                `),
            'surcharge_type_name',
          ],
          'surcharge_points',
          'comments',
          'is_cancel',
          'is_manual',
          'approve_status_id',
          [
            Sequelize.fn(
              'DATE_FORMAT',
              Sequelize.col('ReedemsPoint.createdAt'),
              '%m/%d/%Y'
            ),
            'createdAt',
          ],
          [
            Sequelize.fn(
              'DATE_FORMAT',
              Sequelize.col('ReedemsPoint.updatedAt'),
              '%m/%d/%Y'
            ),
            'updatedAt',
          ],
        ],
        include: [
          //   {
          //     model: Reedem,
          //     as: "reedems",
          //     attributes: ["reedem_name", "location"]
          //   },
          {
            model: RedemsPointItem,
            as: 'items',
            attributes: [
              'redem_name',
              'redem_location',
              'required_points',
              'qty_points',
              'total_redem_points',
            ],
            required: false, // LEFT JOIN untuk memuat jika ada data
          },
        ],
        where: {
          member_id: memberId, // Filter untuk member_id di ReedemsPoint
        },
        order: [[Sequelize.col('ReedemsPoint.createdAt'), 'DESC']],
      })

      res.status(200).json(reedem)
    } catch (error) {
      console.error(error)
      res.status(500).json({ message: 'Error fetching data', error })
    }
  }

  // static async getReedemPoints(req, res) {
  //     const memberId = req.params.memberId;
  //     try {
  //         const reedem = await ReedemsPoint.findAll({
  //             include: [
  //                 {
  //                     model: MemberVoucher,
  //                     as: "memberVouchers",
  //                     attributes: ["voucher_no", "is_used"],
  //                     on: {
  //                         'reedem_point_id': Sequelize.col('ReedemsPoint.id') // Join condition
  //                     },
  //                     required: false,
  //                 }
  //             ],
  //             where: {
  //                 member_id: memberId
  //             },
  //             order: [
  //                 [Sequelize.col('ReedemsPoint.createdAt'), 'DESC']
  //             ]
  //         });
  //         res.status(200).json(reedem);
  //     } catch (error) {
  //         console.log(error);
  //     }
  // };

  static async getEnrollMember(req, res) {
    console.log(req.query, 'APAKAH MASUK DISINI ??????????????????')

    //const { resdate } = req.params;

    const { resdate } = req.query

    try {
      const result = await Master.findAndCountAll({
        attributes: [
          'confirmation_no',
          'mp',
          'firstname',
          'lastname',
          'nationality',
          'country',
          'city',
          'email',
          'telephone',
          'resv_status',
        ],
        where: {
          [Op.and]: [
            { mp: '' },
            { mp_level: '' },
            { email: { [Op.ne]: '' } },
            { birthday: { [Op.not]: null } },
            { arrival_date: { [Op.not]: null } },
            { departure_date: { [Op.not]: null } },
            //{ room_rev_usd: { [Op.gt]: 0 } },
            //{ fb_rev_usd: { [Op.gt]: 0 } },
            //{ market_code: { [Op.in]: ['RAC', 'WEB', 'PACKDOM', 'PACKINT']}},
            { resv_status: { [Op.eq]: 'CHECKED OUT' } },
            Sequelize.where(
              Sequelize.fn(
                'STR_TO_DATE',
                Sequelize.col('insert_date'),
                '%d/%m/%Y'
              ),
              resdate
            ), // format data (YYYY-MM-DD) 2023-06-05
          ],
        },
        group: [
          'confirmation_no',
          'mp',
          'firstname',
          'lastname',
          'nationality',
          'country',
          'city',
          'email',
          'telephone',
          'resv_status',
        ],
        order: [
          ['firstname', 'ASC'],
          ['lastname', 'ASC'],
        ],
      })

      res.status(200).json(result)
    } catch (error) {
      console.log(error)
    }
  }
}

module.exports = MemberController
