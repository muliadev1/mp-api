
const XLSX = require('xlsx');
const fs = require('fs');
const Sequelize = require('sequelize');
const { Op, literal, fn, col } = require('sequelize');
const moment = require('moment');
const { Master } = require('../db/models/index');


function excelDateToJSDate(serial) {
    const MS_PER_DAY = 24 * 60 * 60 * 1000;
    const EPOCH_OFFSET = 25569; // Tanggal 1 Januari 1970 dalam format Excel serial number
    const MS_OFFSET = EPOCH_OFFSET * MS_PER_DAY;
    const date = new Date((serial - 1 + 1) * MS_PER_DAY - MS_OFFSET);
    return date;
}

class excelController {
    static async uploadExcel(req, res) {

        console.log("<< masuk controller >> ")

        const location = req.body.location;
        const departuredate = req.body.departuredate;
        const isdelete = req.body.isdelete;
        const username = req.body.username;
        const filename = req.body.excelfile.filename.split('.').slice(0, -1).join('.');
        const uriFile = process.env.UPLOAD_XLSX + req.body.excelfile.filename;

        // create stay date, ambil tanggal kemarin
        const currDate = new Date();
        currDate.setDate(currDate.getDate() - 1);
        const current_date = currDate.getFullYear() + "-" + (currDate.getMonth() + 1) + "-" + currDate.getDate();
        const current_time = currDate.getHours() + ":" + currDate.getMinutes() + ":" + currDate.getSeconds();
        const date_time = current_date + " " + current_time;

        try {
            if (uriFile == undefined) {
                return res.status(400).send({
                    message: "Please upload an excel file!",
                });
            }

            // ================================================
            // Urutan kolom yang ada di Excel file
            // ================================================
            // Confirmation No (0)	Rate Description (1)	
            // Promotions (Codes) (2)	Market (Code) (3)	
            // Market Group (Code) (4)	Rate Code (5)	
            // Currency (Code) (6)	Business Block (7)	
            // Adults (8)	Children (9)	First Name (10)	Last Name (11)	
            // Source (Description) (12)	Source (Code) (13)	Agent Name (14)	
            // Company Name (15)	Room Number (16)	Group Status (17)	
            // Reservation Status (18)	Guest Title (Code) (19)	Insert Date (20)	
            // Arrival Date (21)	Departure Date (22)	Length of Stay (Nights) (23)	
            // Label (room type) (24)	Room Class (Code) (25)	Nationality (26)	
            // Country Name (27)	City (28)	Membership Card Numbers (29)	
            // Memberships (Codes) (30)	Birth Date (31)	E-mail (32)	Phone No (33)	
            // Bookeeping (34)	Room Revenue over the Whole Reservation (35)	
            // Room Revenue USD (36) F&B Revenue over the Whole Reservation (37) FB Revenue USD (38) Point Promotion (39)
            // WCC Membership Card Numbers (40) Point to WCC (41) 


            var workbook = XLSX.readFile(uriFile);
            var sheetName = workbook.SheetNames[1] === undefined ? workbook.SheetNames[0] : workbook.SheetNames[1]; // Ambil nama sheet pertama
            var worksheet = workbook.Sheets[sheetName];


            console.log(uriFile, ">>>>>>>> url file >>>>>>>>>>>>>")

            console.log(sheetName, ">>>>>>>> nama sheet >>>>>>>>>>>>>")

            console.log(worksheet, ">>>>>>>> nama worksheet >>>>>>>>>>>>>")

            // var xldata = XLSX.utils.sheet_to_json(workbook, worksheet["Sheet1"]);

            // var xldata = XLSX.utils.sheet_to_json(worksheet, { header: 1, columnDefs: [
            //     { type: 'date', column: 20 } // Ubah nomor kolom sesuai dengan kolom tanggal Anda
            // ] });


            var xldata = XLSX.utils.sheet_to_json(worksheet, { header: 1 });

            // ===========================================================
            // hapus data di masters berdasarkan staydate dan location
            // ===========================================================
            if (isdelete === 1) {
                await Master.destroy({
                    where: {
                        [Op.and]: [
                            Sequelize.literal("CASE WHEN departure_date <> '' THEN STR_TO_DATE(departure_date, '%d/%m/%Y') ELSE NULL END = '" + departuredate + "'"),
                            { region: location }
                        ]
                    }
                })
            }


            let masters = [];

            // Looping data dari xldata dan simpan ke tabel masters
            for (let i = 1; i < xldata.length; i++) { // Mulai dari indeks 1 untuk menghindari header
                const data = xldata[i];
                let revStatus = data[18] ? data[18].toUpperCase() : '';

                let insertDate = moment(excelDateToJSDate(data[20])).format('DD/MM/YYYY');
                let arrivalDate = moment(excelDateToJSDate(data[21])).format('DD/MM/YYYY');
                let departureDate = moment(excelDateToJSDate(data[22])).format('DD/MM/YYYY');

                // Menghapus spasi di awal, akhir, dan di dalam email
                // let email = (data[32] || '').replace(/\s+/g, '').trim();

                // Menghapus spasi di awal dan akhir email
                let email = (data[32] || '').trim();

                let master = {
                    // Sesuaikan dengan struktur tabel masters dan kolom-kolom yang sesuai dengan data dari Excel
                    stay_date: arrivalDate,
                    confirmation_no: data[0],
                    rn: data[23],
                    bookkeeping: data[34],
                    room_rev: 0,
                    fb_rev: 0,
                    other_rev: 0,
                    market_code: data[3],
                    market_group: data[4],
                    rate_code: data[5],
                    currency: data[6],
                    rate_amount: 0,
                    share_amount: 0,
                    room_dep: '',
                    adults: data[8] || '',
                    children: data[9] || '',
                    firstname: data[10] || '',
                    lastname: data[11] || '',
                    source_name: data[12] || '',
                    agent_name: data[14] || '',
                    company_name: data[15] || '',
                    qq_room: '',
                    qs: '',
                    group_status: data[17] || '',
                    resv_status: revStatus,
                    packages: '',
                    title: data[19] || '',
                    passport: '',
                    local_id: '',
                    cancelation_date: '',
                    insert_date: insertDate,
                    lead_days: '',
                    arrival_date: arrivalDate,
                    departure_date: departureDate,
                    los: data[23] || '',
                    no_show: '',
                    rtc: '',
                    room_type: data[24] || '',
                    room_class: data[25] || '',
                    nationality: data[26] || '',
                    country: data[27] || '',
                    city: data[28] || '',
                    mp: data[29] || '',
                    mpenroll_date: '',
                    mp_level: data[30] || '',
                    mp_enroll: '',
                    wccenroll_date: '',
                    wcc_enroll: '',
                    sc: '',
                    sc_enroll: '',
                    birthday: data[31] || '',
                    mailist: '',
                    email: email, // Email sudah dihapus spasinya
                    telephone: data[33] || '',
                    room_rev_idr: data[35],
                    room_rev_usd: data[36],
                    fb_rev_idr: data[37],
                    fb_rev_usd: data[38],
                    other_rev_idr: 0,
                    other_rev_usd: 0,
                    temp_points: 0,
                    point_promotion: data[39],
                    wcc_member_id: data[40] || '',
                    is_point_wcc: data[41] === 'Y' ? 1 : 0 || 0,
                    region: location,
                    createdBy: username,
                    updatedBy: username,
                };

                masters.push(master);
            }

            console.log(masters, ">>>>>>>> isi masters data >>>>>>>>>>>>>>>>>>>")

            const chunkSize = 100; // Jumlah data yang dimasukkan dalam satu batch
            for (let i = 0; i < masters.length; i += chunkSize) {
                const chunk = masters.slice(i, i + chunkSize);
                await Master.bulkCreate(chunk);
            }

            //await Master.bulkCreate(masters);

            // Kirim respons jika berhasil
            res.status(200).send({
                message: "Uploaded the file successfully: " + req.body.excelfile.filename,
            });

        } catch (error) {
            console.log(error);
            res.status(500).send({
                message: "Could not upload the file: " + req.body.excelfile.filename,
            });
        }
    }

}

module.exports = excelController





