const { Module, Menu, RolesMenu } = require('../db/models/index');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const Sequelize = require('sequelize');

class MenuController {

    static async getMenus(req, res) {
        try {
            const menus = await Menu.findAll({
                include: ["rolesmenu"],
                order: [
                    ['order_no', 'ASC'] // Urutkan berdasarkan order_no secara menaik (ASC)
                ]
            });
            res.status(200).json(menus);
        } catch (error) {
            console.log(error);
        }
    };

    static async getMenusByModule(req, res) {
        const moduleId = req.params.id;
        try {
            const menus = await Menu.findAll({
                include: ["rolesmenu"],
                where: {
                    module_id: moduleId
                },
                order: [
                    ['order_no', 'ASC'] // Urutkan berdasarkan order_no secara menaik (ASC)
                ]
            });
            res.status(200).json(menus);
        } catch (error) {
            console.log(error);
        }
    };


    static async createMenu(req, res) {
        const { menu_code, menu_name, module_id, order_no, url, is_trx, username } = req.body;

        try {
            await Menu.create({
                menu_code: menu_code,
                menu_name: menu_name,
                module_id: module_id,
                order_no: order_no,
                url: url,
                is_trx: is_trx,
                createdAt: Sequelize.fn('NOW'),
                createdBy: username,
                updatedAt: Sequelize.fn('NOW'),
                updatedBy: username,
            });
            res.json({ status: 200, msg: "Add new menu successfully" });
        } catch (error) {
            console.log(error);
        }
    };

    static async detailMenu(req, res) {

        const menuId = req.params.id;
        try {
            const menu = await Menu.findOne({
                where: {
                    id: menuId
                }
            });
            res.status(200).json(menu);
        } catch (error) {
            console.log(error);
        }
    };

    static async updateMenu(req, res) {
        const menuId = req.params.id;
        const { menu_code, menu_name, module_id, order_no, url, is_trx, username } = req.body;
        //cek apakah Menu sdh ada
        const menu = await Menu.findAll({
            where: {
                id: menuId
            }
        });

        if (!menu[0]) return res.status(400).json({ status: 400, msg: "Menu is not exist" });

        try {

            await Menu.update(
                {
                    menu_code: menu_code,
                    menu_name: menu_name,
                    module_id: module_id,
                    order_no: order_no,
                    url: url,
                    is_trx: is_trx,
                    updatedAt: Sequelize.fn('NOW'),
                    updatedBy: username
                },
                {
                    where: {
                        id: menuId
                    }
                });

            res.json({ status: 200, msg: "Update Menu is successfully" });
        } catch (error) {
            console.log(error);
        }
    };

    static async deleteMenu(req, res) {
        const menuId = req.params.id;
        //cek apakah Menu sdh ada
        const menus = await Menu.findAll({
            where: {
                id: menuId
            }
        });

        if (!menus[0]) return res.status(400).json({ status: 400, msg: "Menu is not exist" });

        try {

            await Menu.destroy(
                {
                    where: {
                        id: menuId
                    }
                });

            res.json({ status: 200, msg: "Delete Menu is successfully" });
        } catch (error) {
            console.log(error);
        }
    };

    static async checkRoleMenu(req, res) {
        const roleId = req.params.roleId;
        const moduleId = req.params.moduleId;
        const menuId = req.params.menuId;
        try {
            const rolemenus = await RolesMenu.findAll({
                where: {
                    role_id: roleId,
                    module_id: moduleId,
                    menu_id: menuId
                }
            });
            res.status(200).json(rolemenus);
        } catch (error) {
            console.log(error);
        }
    };
}


module.exports = MenuController







