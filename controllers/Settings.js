
const { Setting } = require('../db/models/index');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const Sequelize = require('sequelize');

class SettingController {
    static async getSettings(req, res) {
        try {
            const settings = await Setting.findAll({
            });
            res.status(200).json(settings);
        } catch (error) {
            console.log(error);
        }
    };

    static async detailSetting(req, res) {

        const settingId = req.params.id;
        try {
            const setting = await Setting.findOne({
                where: {
                    id: settingId
                }
            });
            res.status(200).json(setting);
        } catch (error) {
            console.log(error);
        }
    };

    static async updateSetting(req, res) {
        
        const settingId = req.params.id;
        //console.log('bookeeping_jkt value received:', req.body);
        
        const { room_amount, room_point_silver, room_point_gold, room_point_diamond, 
            fb_amount, fb_point_silver, fb_point_gold, fb_point_diamond, 
            other_amount, other_point_silver, other_point_gold, other_point_diamond,
            hotel_pay_to_loyalty, loyalty_pay_to_hotel, system_profit_percentage, system_profit_amount,
            complimentary_stay_nights, dividing_factors, bookeeping, bookeeping_jkt, hotel_level_1,hotel_level_2,
            hotel_level_3, hotel_level_4, hotel_level_5, hotel_level_6, hotel_level_7,
            hotel_level_8, hotel_level_9, hotel_level_10, username } = req.body;
        //cek apakah Role sdh ada
        const settings = await Setting.findAll({
            where: {
                id: settingId
            }
        });


        if (!settings[0]) return res.status(400).json({ status: 400, msg: "Setting is not exist" });

        try {

            await Setting.update(
                {
                    room_amount: room_amount,
                    room_point_silver: room_point_silver, 
                    room_point_gold: room_point_gold, 
                    room_point_diamond: room_point_diamond,
                    fb_amount: fb_amount,
                    fb_point_silver: fb_point_silver,
                    fb_point_gold: fb_point_gold,
                    fb_point_diamond: fb_point_diamond,
                    other_amount: other_amount,
                    other_point_silver: other_point_silver,
                    other_point_gold: other_point_gold,
                    other_point_diamond: other_point_diamond,
                    hotel_pay_to_loyalty: hotel_pay_to_loyalty,
                    loyalty_pay_to_hotel: loyalty_pay_to_hotel,
                    system_profit_percentage: system_profit_percentage,
                    system_profit_amount: system_profit_amount,
                    complimentary_stay_nights: complimentary_stay_nights,
                    dividing_factors: dividing_factors,
                    bookeeping: bookeeping,
                    bookeeping_jkt: bookeeping_jkt,
                    hotel_level_1: hotel_level_1,
                    hotel_level_2: hotel_level_2,
                    hotel_level_3: hotel_level_3,
                    hotel_level_4: hotel_level_4,
                    hotel_level_5: hotel_level_5,
                    hotel_level_6: hotel_level_6,
                    hotel_level_7: hotel_level_7,
                    hotel_level_8: hotel_level_8,
                    hotel_level_9: hotel_level_9,
                    hotel_level_10: hotel_level_10,
                    updatedAt: Sequelize.fn('NOW'),
                    updatedBy: username
                },
                {
                    where: {
                        id: settingId
                    }
                });

            res.json({ status: 200, msg: "Update Settings is successfully" });
        } catch (error) {
            console.log(error);
        }
    };

}

module.exports = SettingController