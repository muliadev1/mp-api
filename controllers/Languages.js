const { Language } = require('../db/models/index');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const Sequelize = require('sequelize');


class LanguageController {
    static async getLanguagesUsedPages(req, res) {
        let { page, pageSize } = req.query

        //! Calculate starting index and number of records to retrieve
        const startIndex = (Number(page) - 1) * Number(pageSize);
        const endIndex = Number(page) * Number(pageSize);
        try {
            const langs = await Language.findAll({
                offset: startIndex,
                limit: Number(pageSize)
            });
              //! Retrieve total count of records for pagination info
              const totalCount = await Language.count();
              //! Calculate total number of pages
              const totalPages = Math.ceil(totalCount / Number(pageSize));
            res.status(200).json({
                data: langs,
                totalRecords: totalCount,
                currentPage: Number(page),
                totalPages: totalPages });
        } catch (error) {
            console.log(error);
        }
    };

    static async getLanguages(req, res) {
        try {
            const lang = await Language.findAll({
            });
            res.status(200).json(lang);
        } catch (error) {
            console.log(error);
        }
    };

    static async createLanguage(req, res) {
        const { language_name, language_code, iso_code, username } = req.body;
        //cek apakah language sdh ada
        const lang = await Language.findAll({
            where: {
                language_name: language_name
            }
        });

        if (lang[0]) return res.status(400).json({ msg: "Language is alredy exist" });

        try {
            await Language.create({
                language_name: language_name,
                language_code: language_code,
                iso_code: iso_code,
                createdAt: Sequelize.fn('NOW'),
                createdBy: username,
                updatedAt: Sequelize.fn('NOW'), 
                updatedBy: username,
            });
            res.json({ msg: "Add new Language successfully" });
        } catch (error) {
            console.log(error);
        }
    };

    static async detailLanguage(req, res) {

        const langId = req.params.id;
        try {
            const lang = await Language.findOne({
                where: {
                    id: langId
                }
            });
            res.status(200).json(lang);
        } catch (error) {
            console.log(error);
        }
    };

    static async updateLanguage (req, res) {
        const langId = req.params.id;
        const { language_name, language_code, iso_code, username } = req.body;
        //cek apakah language sdh ada
        const lang = await Language.findAll({
            attributes: ['id', 'language_name', 'language_code','iso_code'],
            where: {
                id: langId
            }
        });
    
        if (!lang[0]) return res.status(400).json({ msg: "Language is not exist" });
    
        try {
    
            await Language.update(
                {
                    language_name: language_name,
                    language_code: language_code,
                    iso_code: iso_code,
                    updatedBy: username,
                    updatedAt: Sequelize.fn('NOW')
                },
                {
                    where: {
                        id: langId
                    }
                });
    
            res.json({ msg: "Update Language is successfully" });
        } catch (error) {
            console.log(error);
        }
    };

    static async deleteLanguage(req, res) {
        const langId = req.params.id;
        //cek apakah language sdh ada
        const lang = await Language.findAll({
            where: {
                id: langId
            }
        });
    
        if (!lang[0]) return res.status(400).json({ msg: "Language is not exist" });
    
        try {
    
            await Language.destroy(
                {
                    where: {
                        id: langId
                    }
                });
    
            res.json({ msg: "Delete Language is successfully" });
        } catch (error) {
            console.log(error);
        }
    };
}


module.exports = LanguageController







