
const { Benefit } = require('../db/models/index');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const Sequelize = require('sequelize');

class BenefitController {
    static async detailBenefit(req, res) {

        const benefitId = req.params.id;
        try {
            const benefit = await Benefit.findOne({
                where: {
                    id: benefitId,
                    language_id: 1 // default english
                }
            });
            res.status(200).json(benefit);
        } catch (error) {
            console.log(error);
        }
    };

    static async detailBenefitLang(req, res) {

        const benefitId = req.params.id;
        const langId = req.params.language;
        try {
            const benefit = await Benefit.findOne({
                where: {
                    benefit_id: benefitId,
                    language_id: langId
                }
            });
            res.status(200).json(benefit);
        } catch (error) {
            console.log(error);
        }
    };

    static async updateBenefit(req, res) {
        const benefitId = req.params.id;
        const { qualification_silver, qualification_gold, qualification_diamond, earning_points_silver, earning_points_gold,
            earning_points_diamond, extra_privileges_silver, extra_privileges_gold,
            extra_privileges_diamond, general_toc, username } = req.body;
        //cek apakah Benefit sdh ada
        const benefits = await Benefit.findAll({
            where: {
                id: benefitId
            }
        });

        if (!benefits[0]) return res.status(400).json({ status: 400, msg: "Benefit is not exist" });

        try {

            await Benefit.update(
                {
                    qualification_silver: qualification_silver,
                    qualification_gold: qualification_gold,
                    qualification_diamond: qualification_diamond,
                    earning_points_silver: earning_points_silver,
                    earning_points_gold: earning_points_gold,
                    earning_points_diamond: earning_points_diamond,
                    extra_privileges_silver: extra_privileges_silver,
                    extra_privileges_gold: extra_privileges_gold,
                    extra_privileges_diamond: extra_privileges_diamond,
                    general_toc: general_toc,
                    updatedAt: Sequelize.fn('NOW'),
                    updatedBy: username
                },
                {
                    where: {
                        id: benefitId
                    }
                });

            res.json({ status: 200, msg: "Update Benefit is successfully" });
        } catch (error) {
            console.log(error);
        }
    };

    static async updateBenefitLang(req, res) {
        const benefitId = req.params.id;
        const langId = req.params.language;
        const { qualification_silver, qualification_gold, qualification_diamond, earning_points_silver, earning_points_gold,
            earning_points_diamond, extra_privileges_silver, extra_privileges_gold,
            extra_privileges_diamond, general_toc, username } = req.body;
        

        try {

            // 1. Cek apakah benefits langs sudah ada berdasarkan `id` dan `language_id`
            const benefitLang = await Benefit.findOne({
                where: { benefit_id: benefitId, language_id: langId }
            });

            if (benefitLang) {
                // Jika sudah ada, lakukan update
                await Benefit.update(
                    {
                        qualification_silver: qualification_silver,
                        qualification_gold: qualification_gold,
                        qualification_diamond: qualification_diamond,
                        earning_points_silver: earning_points_silver,
                        earning_points_gold: earning_points_gold,
                        earning_points_diamond: earning_points_diamond,
                        extra_privileges_silver: extra_privileges_silver,
                        extra_privileges_gold: extra_privileges_gold,
                        extra_privileges_diamond: extra_privileges_diamond,
                        general_toc: general_toc,
                        updatedAt: Sequelize.fn('NOW'),
                        updatedBy: username
                    },
                    { where: { benefit_id: benefitId, language_id: langId } }
                );
            } else {
                // Jika belum ada, lakukan insert (create)
                await Benefit.create({
                    benefit_id: benefitId,
                    qualification_silver: qualification_silver,
                    qualification_gold: qualification_gold,
                    qualification_diamond: qualification_diamond,
                    earning_points_silver: earning_points_silver,
                    earning_points_gold: earning_points_gold,
                    earning_points_diamond: earning_points_diamond,
                    extra_privileges_silver: extra_privileges_silver,
                    extra_privileges_gold: extra_privileges_gold,
                    extra_privileges_diamond: extra_privileges_diamond,
                    general_toc: general_toc,
                    language_id: langId,
                    createdAt: Sequelize.fn('NOW'),
                    updatedAt: Sequelize.fn('NOW'),
                    createdBy: username,
                    updatedBy: username
                });
            }

            res.json({ status: 200, msg: "Update Benefit is successfully" });
        } catch (error) {
            console.log(error);
        }
    };

}

module.exports = BenefitController