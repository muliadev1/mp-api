const {
  Company,
  sequelize,
} = require('../db/models/index')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const Sequelize = require('sequelize')
const { Op, where, col, fn, literal } = require('sequelize')

function convertUTCToLocal(utcDate, offset) {
  if (!(utcDate instanceof Date)) {
    throw new TypeError('The first argument must be a Date object.')
  }
  if (typeof offset !== 'number') {
    throw new TypeError('The second argument must be a number.')
  }

  // Convert the UTC time to the local time
  return new Date(utcDate.getTime() + offset * 60 * 60 * 1000)
}

class CompanyController {

  static async getCompanies(req, res) {
    try {
      const companies = await Company.findAll({
        order: [['company_name', 'ASC']] // Mengurutkan berdasarkan company_name secara ascending
      });
      res.status(200).json(companies)
    } catch (error) {
      console.log(error)
    }
  }

  static async getListCompanies(req, res, next) {
    const { page, per_page, filter } = req.query
    const perPageInt = parseInt(per_page, 10)
    const offset = (page - 1) * per_page

    try {
      // Lakukan query ke database menggunakan Sequelize
      const companies = await Company.findAndCountAll({
        attributes: [
          'id',
          'company_name',
          'createdAt',
          'updatedAt',
          'createdBy',
          'updatedBy',
        ],
        offset: offset,
        limit: perPageInt,
        where: filter
          ? {
            [Op.or]: [
              {
                company_name: {
                  [Op.like]: `%${filter}%`,
                },
              },
              {
                createdBy: {
                  [Op.like]: `%${filter}%`,
                },
              },
              {
                updatedBy: {
                  [Op.like]: `%${filter}%`,
                },
              },
            ],
          }
          : {},
        // Anda dapat menambahkan pengurutan jika diperlukan
        order: [['company_name', 'ASC']],
      })

      // Ekstrak data dan jumlah total dari hasil query
      const data = companies.rows
      const totalCount = companies.count

      res.status(200).json({
        data: data,
        totalData: totalCount,
      })
    } catch (error) {
      console.log(error)
    }
  }

  static async getCompany(req, res) {
    const compId = req.params.id
    try {
      const company = await Company.findOne({
        attributes: [
          'id',
          'company_name',
          'createdAt',
          'createdBy',
          'updatedAt',
          'updatedBy',
        ],
        where: {
          id: compId,
        },
      })
      res.status(200).json(company)
    } catch (error) {
      console.log(error)
    }
  }

  static async createCompany(req, res) {
    const {
      company_name,
      username,
    } = req.body

    // using UTC time
    const nowUTC = new Date() // Current UTC time
    const offset = 8 // Bali (WITA) is UTC+8
    const localDateTime = convertUTCToLocal(nowUTC, offset)

    //cek apakah member sdh ada
    const company = await Company.findAll({
      where: {
        company_name: company_name,
      },
    })

    if (company[0])
      return res.status(400).json({
        msg:
          "Company name is already registered."
      })

    try {
      await Company.create({
        company_name: company_name,
        createdAt: localDateTime,
        createdBy: username,
        updatedAt: localDateTime,
        updatedBy: username,
      })
      res.json({ msg: 'Add new company successfully' })
    } catch (error) {
      console.log(error)
    }
  }

  static async updateCompany(req, res) {
    const compId = req.params.id
    const {
      company_name,
      username,
    } = req.body

    // using UTC time
    const nowUTC = new Date() // Current UTC time
    const offset = 8 // Bali (WITA) is UTC+8
    const localDateTime = convertUTCToLocal(nowUTC, offset)

    //cek apakah member sdh ada
    const company = await Company.findAll({
      attributes: [
        'id',
        'company_name',
      ],
      where: {
        id: compId,
      },
    })

    if (!company[0]) return res.status(400).json({ msg: 'Company is not exist' })

    if (company_name === '')
      return res.status(400).json({ msg: 'Company name address is required!' })

    try {
      await Company.update(
        {
          company_name: company_name,
          updatedAt: localDateTime,
          updatedBy: username,
        },
        {
          where: {
            id: compId,
          },
        }
      )
      res.status(200).json({ msg: 'Update company is successfully' })
    } catch (error) {
      console.log(error)
    }
  }

  static async deleteCompany(req, res) {
    const compId = req.params.id
    //cek apakah language sdh ada
    const company = await Company.findAll({
      where: {
        id: compId,
      },
    })

    if (!company[0]) return res.status(400).json({ msg: 'Company is not exist' })

    try {
      await Company.destroy({
        where: {
          id: compId,
        },
      })

      res.json({ msg: 'Delete Company is successfully' })
    } catch (error) {
      console.log(error)
    }
  }
}

module.exports = CompanyController