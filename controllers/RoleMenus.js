const { RolesMenu, Module, Menu } = require('../db/models/index');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const Sequelize = require('sequelize');


class RoleMenuController {
    static async getRoleMenus(req, res) {
        const roleId = req.params.roleId;
        try {
            const roles = await RolesMenu.findAll({
                include: [
                    {
                        model: Module,
                        attributes: ['module_name'],
                        as: 'module'
                    },
                    {
                        model: Menu,
                        attributes: ['menu_name','url'],
                        as: 'menu'
                    },
                ],
                where: {
                    role_id: roleId
                },
                order: [
                    ['module_id', 'ASC'], // Urutan berdasarkan menu_id secara menaik
                    ['order_no', 'ASC'] // Kemudian urut berdasarkan order_no secara menaik
                ],
                raw: true
            });

            // Mengonversi hasil menjadi format yang diinginkan
            const formattedRoles = roles.map(role => ({
                id: role.id,
                role_id: role.role_id,
                module_id: role.module_id,
                module_name: role['module.module_name'], // Mengakses kolom module_name dari model Module
                menu_id: role.menu_id,
                menu_name: role['menu.menu_name'], // Mengakses kolom menu_name dari model Menu
                menu_url: role['menu.url'],
                order_no: role.order_no,
                is_insert: role.is_insert,
                is_update: role.is_update,
                is_delete: role.is_delete,
                is_read: role.is_read,
                is_cancel: role.is_cancel,
                is_search: role.is_search,
                is_loyalty: role.is_loyalty,
                is_revenue: role.is_revenue,
                is_finance: role.is_finance,
                is_resident: role.is_resident   
            }));

            res.status(200).json(formattedRoles);
        } catch (error) {
            console.log(error);
        }
    };


    static async createRoleMenu(req, res) {
        const roleId = req.params.roleId;
        const { module_id, menu_id, order_no, is_insert, is_update, is_delete, is_read, is_cancel, is_search, is_loyalty, is_revenue, is_finance, is_resident, username } = req.body;

        try {
            await RolesMenu.create({
                role_id: roleId,
                module_id: module_id,
                menu_id: menu_id,
                order_no: order_no,
                is_insert: is_insert,
                is_update: is_update,
                is_delete: is_delete,
                is_read: is_read,
                is_cancel: is_cancel,
                is_search: is_search,
                is_loyalty: is_loyalty,
                is_revenue: is_revenue,
                is_finance: is_finance,
                is_resident: is_resident,
                createdAt: Sequelize.fn('NOW'),
                createdBy: username,
                updatedAt: Sequelize.fn('NOW'),
                updatedBy: username,
            });
            res.json({ status: 200, msg: "Add new Role menus successfully" });
        } catch (error) {
            console.log(error);
        }
    };

    static async detailRoleMenu(req, res) {

        const detailId = req.params.id;
        try {
            const role = await RolesMenu.findOne({
                where: {
                    id: detailId
                }
            });
            res.status(200).json(role);
        } catch (error) {
            console.log(error);
        }
    };

    static async updateRoleMenu(req, res) {
        const detailId = req.params.id;
        const { module_id, menu_id, order_no, is_insert, is_update, is_delete, is_read, is_cancel, is_search, is_loyalty, is_revenue, is_finance, is_resident, username } = req.body;
        //cek apakah Role sdh ada
        const role = await RolesMenu.findAll({
            where: {
                id: detailId
            }
        });

        if (!role[0]) return res.status(400).json({ status: 400, msg: "Role menus is not exist" });

        try {

            await RolesMenu.update(
                {
                    module_id: module_id,
                    menu_id: menu_id,
                    order_no: order_no,
                    is_insert: is_insert,
                    is_update: is_update,
                    is_delete: is_delete,
                    is_read: is_read,
                    is_cancel: is_cancel,
                    is_search: is_search,
                    is_loyalty: is_loyalty,
                    is_revenue: is_revenue,
                    is_finance: is_finance,
                    is_resident: is_resident,
                    updatedAt: Sequelize.fn('NOW'),
                    updatedBy: username
                },
                {
                    where: {
                        id: detailId
                    }
                });

            res.json({ status: 200, msg: "Update Role menus is successfully" });
        } catch (error) {
            console.log(error);
        }
    };

    static async deleteRoleMenu(req, res) {
        const detailId = req.params.id;
        //cek apakah Role sdh ada
        const roles = await RolesMenu.findAll({
            where: {
                id: detailId
            }
        });

        if (!roles[0]) return res.status(400).json({ status: 400, msg: "Role menus is not exist" });

        try {

            await RolesMenu.destroy(
                {
                    where: {
                        id: detailId
                    }
                });

            res.json({ status: 200, msg: "Delete Role menus is successfully" });
        } catch (error) {
            console.log(error);
        }
    };
}


module.exports = RoleMenuController







