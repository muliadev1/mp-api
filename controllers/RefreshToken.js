const {User} = require('../db/models/index');
const jwt = require('jsonwebtoken');

const refreshToken = async(req,res) =>{
    try {
        const refreshToken = req.cookies.refreshToken;
        if(!refreshToken) return res.sendStatus(401);
        const user = await User.findOne({
            where:{
                refresh_token: refreshToken
            }
        });

        if(!user) return res.sendStatus(403);

        jwt.verify(refreshToken, process.env.REFRESH_TOKEN_SECRET, async(err,decoded) =>{
            //if(err) return res.sendStatus(403);
            const userId = user.dataValues.id;
            const first_name = user.dataValues.first_name;
            const last_name = user.dataValues.last_name;
            const username = user.dataValues.username;
            const accessToken = jwt.sign({ userId, first_name, last_name, username }, process.env.ACCESS_TOKEN_SECRET, {
                expiresIn: '20s'
            });

            await User.update({refresh_token: accessToken},{
                where: {
                    id:userId
                }
            });
            res.cookie('refreshToken', accessToken,{
                httpOnly: true,
                maxAge: 24 * 60 * 60 * 1000
                // secure: true //ini digunakan ketika menggunakan https
            });

            res.json({ accessToken });
        });
    } catch (error) {
        console.log(error);
    }
}

module.exports = refreshToken;