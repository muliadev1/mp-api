const { Member, Contact, sequelize } = require('../db/models/index');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const Sequelize = require('sequelize');
const { Op, where, col, fn, literal } = require('sequelize');

function convertUTCToLocal(utcDate, offset) {
    if (!(utcDate instanceof Date)) {
        throw new TypeError('The first argument must be a Date object.');
    }
    if (typeof offset !== 'number') {
        throw new TypeError('The second argument must be a number.');
    }

    // Convert the UTC time to the local time
    return new Date(utcDate.getTime() + offset * 60 * 60 * 1000);
}

class ContactController {

    static async getContactsByCompany(req, res) {

        const compId = req.params.companyId;
        try {
            const contacts = await Contact.findAll({
                where: {
                    company_id: compId
                }
            });
            res.status(200).json(contacts);
        } catch (error) {
            console.log(error);
        }
    };

    static async createContact(req, res) {
        const {
            company_id,
            first_name,
            last_name,
            phone_number,
            email,
            is_primary,
            is_milist,
            username
        } = req.body


        // using UTC time
        const nowUTC = new Date() // Current UTC time
        const offset = 8 // Bali (WITA) is UTC+8
        const localDateTime = convertUTCToLocal(nowUTC, offset)

        //cek apakah member sdh ada
        const contact = await Contact.findAll({
            where: {
                email: email,
                company_id: company_id
            },
        })

        if (contact[0])
            return res.status(400).json({
                msg:
                    "Contact is already registered."
            })

        try {
            await Contact.create({
                company_id: company_id,
                first_name: first_name,
                last_name: last_name,
                phone_number: phone_number,
                email: email,
                is_primary: is_primary,
                is_milist: is_milist,
                createdAt: localDateTime,
                createdBy: username,
                updatedAt: localDateTime,
                updatedBy: username,
            })
            res.json({ msg: 'Add new contact successfully' })
        } catch (error) {
            console.log(error)
        }
    }


    static async updateContact(req, res) {
        const contactId = req.params.id
        const {
            company_id,
            first_name,
            last_name,
            phone_number,
            email,
            is_primary,
            is_milist,
            username
        } = req.body
    
        // using UTC time
        const nowUTC = new Date() // Current UTC time
        const offset = 8 // Bali (WITA) is UTC+8
        const localDateTime = convertUTCToLocal(nowUTC, offset)
    
        //cek apakah member sdh ada
        const contact = await Contact.findAll({
          where: {
            id: contactId,
          },
        })
    
        if (!contact[0]) return res.status(400).json({ msg: 'Contact is not exist' })
    
        if (email === '')
          return res.status(400).json({ msg: 'Email address is required!' })
    
        try {
          await Contact.update(
            {
              company_id: company_id,
              first_name: first_name,
              last_name: last_name,
              phone_number: phone_number,
              email: email,
              is_primary: is_primary,
              is_milist: is_milist,
              updatedAt: localDateTime,
              updatedBy: username,
            },
            {
              where: {
                id: contactId,
              },
            }
          )
          res.status(200).json({ msg: 'Update contact is successfully' })
        } catch (error) {
          console.log(error)
        }
      }

    

    static async getAdjustments(req, res) {

        let { dateFrom, dateTo, username } = req.query

        const dateObjFrom = new Date(dateFrom);
        const yearFrom = dateObjFrom.getFullYear();
        const monthFrom = dateObjFrom.getMonth() + 1;
        const dayFrom = dateObjFrom.getDate();

        const dateObjTo = new Date(dateTo);
        const yearTo = dateObjTo.getFullYear();
        const monthTo = dateObjTo.getMonth() + 1;
        const dayTo = dateObjTo.getDate();

        let whereClause = {
            [Op.and]: [
                {
                    [Op.or]: [
                        {
                            [Op.and]: [
                                Sequelize.where(Sequelize.fn('YEAR', Sequelize.col('PointAdjustment.createdAt')), yearFrom),
                                Sequelize.where(Sequelize.fn('MONTH', Sequelize.col('PointAdjustment.createdAt')), monthFrom),
                                Sequelize.where(Sequelize.fn('DAY', Sequelize.col('PointAdjustment.createdAt')), {
                                    [Op.between]: [dayFrom, dayTo],
                                }),
                            ],
                        },
                        {
                            [Op.and]: [
                                Sequelize.where(Sequelize.fn('YEAR', Sequelize.col('PointAdjustment.createdAt')), yearTo),
                                Sequelize.where(Sequelize.fn('MONTH', Sequelize.col('PointAdjustment.createdAt')), monthTo),
                                Sequelize.where(Sequelize.fn('DAY', Sequelize.col('PointAdjustment.createdAt')), {
                                    [Op.between]: [dayFrom, dayTo],
                                }),
                            ],
                        },
                    ],
                },
            ],
        };

        // Menambahkan filter untuk 'createdBy' hanya jika username diberikan
        if (username) {
            whereClause[Op.and].push({
                '$PointAdjustment.createdBy$': username,
            });
        }


        try {
            const adjustment = await PointAdjustment.findAll({

                attributes: [
                    'id',
                    'member_id',
                    [Sequelize.col('members.name_on_card'), 'name_on_card'],
                    'prev_total_points',
                    'total_points',
                    'prev_reedem_points',
                    'reedem_points',
                    'prev_available_points',
                    'available_points',
                    'remarks',
                    'createdBy',
                    [Sequelize.fn('DATE_FORMAT', Sequelize.col('PointAdjustment.createdAt'), '%m/%d/%Y'), 'createdAt'],
                    'updatedBy',
                    [Sequelize.fn('DATE_FORMAT', Sequelize.col('PointAdjustment.updatedAt'), '%m/%d/%Y'), 'updatedAt']
                ],
                include: [
                    {
                        model: Member,
                        as: "members",
                        attributes: ["name_on_card"]
                    },
                ],
                where: whereClause,
                order: [
                    ['member_id', 'ASC'],
                ],
            });


            res.status(200).json(adjustment);
        } catch (error) {
            console.log(error);
        }
    };
}

module.exports = ContactController