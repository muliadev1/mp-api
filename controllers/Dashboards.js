const { Master, MarketSegment, RateCode, RoomType } = require('../db/models/index');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const Sequelize = require('sequelize');
const { Op, literal, fn, col } = require('sequelize');

class DashboardController {
    // ---------------------------------------------------
    // MTD DAILY ON THE BOOK
    // ---------------------------------------------------
    static async getMtdDailyBooking(req, res) {

        const { hotel, year, comparison, month, group, segment, description, country } = req.query;

        var currentTime = new Date();
        var currYear = year > 0 ? year : currentTime.getFullYear();
        var prevYear = comparison > 0 ? Number(comparison) : currentTime.getFullYear() - 1;
        var nextYear = currentTime.getFullYear() + 1

        // list 1 years before current year
        const startCurrYear = currYear - 1;
        const curryearList = [];

        for (let tahun = startCurrYear; tahun <= currYear; tahun++) {
            if (tahun != 2020 && tahun != 2021) {
                curryearList.push(tahun);
            }
        }

        curryearList.push(nextYear)

        const yearsBeforeCurrYear = '[' + curryearList.join(', ') + ']'


        console.log(">>>>>>>>>>>>>>>> bikin kondisi baru disini >>>>>>>>>>>>>>>>>>>>>>")
        let whereClause = {};
        let whereClauseSegment = {}

        if (year != '' && comparison == '') {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', 'PF', '']
                    },
                    group_status: {
                        [Op.notIn]: ['LOS', 'PEN', 'PRO']
                    },
                    room_type: {
                        [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear)
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', 'PF', '']
                    },
                    group_status: {
                        [Op.notIn]: ['LOS', 'PEN', 'PRO']
                    },
                    room_type: {
                        [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month),
                    ]
                };
            }
        } else if (year != '' && comparison != '') {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', 'PF', '']
                    },
                    group_status: {
                        [Op.notIn]: ['LOS', 'PEN', 'PRO']
                    },
                    room_type: {
                        [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                    },
                    [Op.and]: [
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', 'PF', '']
                    },
                    group_status: {
                        [Op.notIn]: ['LOS', 'PEN', 'PRO']
                    },
                    room_type: {
                        [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                    },
                    [Op.and]: [
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month),
                    ]
                };
            }
        } else {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', 'PF', '']
                    },
                    group_status: {
                        [Op.notIn]: ['LOS', 'PEN', 'PRO']
                    },
                    room_type: {
                        [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                    },
                    [Op.and]: [
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', 'PF', '']
                    },
                    group_status: {
                        [Op.notIn]: ['LOS', 'PEN', 'PRO']
                    },
                    room_type: {
                        [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                    },
                    [Op.and]: [
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month),
                    ]
                };
            }
        }
        // }

        if (hotel != '') {
            whereClause['room_class'] = hotel;
        }

        if (group != '') { // filter marketsegment.group sesudah where
            whereClause['group'] = {
                [Op.and]: [
                    Sequelize.where(Sequelize.col('marketsegment.group'), { [Op.eq]: group }),
                ]
            };
        }

        if (segment != '') {
            whereClauseSegment['segment'] = segment;
        }

        if (description != '') {
            whereClauseSegment['description'] = description;
        }

        if (country != '') {
            whereClause['country'] = country;
        }

        const include_option = [
            {
                model: MarketSegment,
                as: 'marketsegment',
                attributes: [],  // just make relation not show column
                required: false,  //left outer join; true : inner join
                where: whereClauseSegment
            }
        ]

        if (description != '' || segment != '') {
            include_option[0].required = true
        }

        const attributes = [];
        const years = JSON.parse(yearsBeforeCurrYear);

        for (let i = 0; i < years.length; i++) {
            const year = years[i];

            const attribute = [
                Sequelize.fn(
                    'SUM',
                    Sequelize.literal(
                        "CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " +
                        year +
                        ' THEN `rn` ELSE 0 END'
                    )
                ),
                'room_night_' + year,
            ];

            attributes.push(attribute);
        }

        if (year != '' && comparison == '') { // year terisi, comparison null
            try {
                console.log(">>>>>>>>>> MASUK DISINI >>>>>>>>>>>>>>>>>>>")
                const bookings = await Master.findAll({
                    attributes: [
                        [Sequelize.literal("RIGHT(STR_TO_DATE(stay_date, '%d/%m/%Y'), 2)"), "day"],
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'room_night']
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        Sequelize.literal("RIGHT(STR_TO_DATE(stay_date, '%d/%m/%Y'), 2)"),
                    ],
                    order: [
                        [Sequelize.literal("RIGHT(STR_TO_DATE(stay_date, '%d/%m/%Y'), 2)"), 'ASC'],
                    ],
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        [Sequelize.literal("RIGHT(STR_TO_DATE(stay_date, '%d/%m/%Y'), 2)"), "day"],
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + prevYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + prevYear,
                        ],
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + currYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + currYear,
                        ],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        Sequelize.literal("RIGHT(STR_TO_DATE(stay_date, '%d/%m/%Y'), 2)"),
                    ],
                    order: [
                        [Sequelize.literal("RIGHT(STR_TO_DATE(stay_date, '%d/%m/%Y'), 2)"), 'ASC'],
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        [Sequelize.literal("RIGHT(STR_TO_DATE(stay_date, '%d/%m/%Y'), 2)"), "day"],
                        ...attributes,
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        Sequelize.literal("RIGHT(STR_TO_DATE(stay_date, '%d/%m/%Y'), 2)"),
                    ],
                    order: [
                        [Sequelize.literal("RIGHT(STR_TO_DATE(stay_date, '%d/%m/%Y'), 2)"), 'ASC'],
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        }
    };
    // ---------------------------------------------------
    // MTD TOP COUNTRIES
    // ---------------------------------------------------
    static async getMtdTopCountries(req, res) {
        const { hotel, year, comparison, month, group, segment, description, country } = req.query;

        var currentTime = new Date();
        var currYear = year > 0 ? year : currentTime.getFullYear();
        var prevYear = comparison > 0 ? Number(comparison) : currentTime.getFullYear() - 1;
        var nextYear = currentTime.getFullYear() + 1

        // list 1 years before current year
        const startCurrYear = currYear - 1;
        const curryearList = [];

        for (let tahun = startCurrYear; tahun <= currYear; tahun++) {
            if (tahun != 2020 && tahun != 2021) {
                curryearList.push(tahun);
            }
        }

        curryearList.push(nextYear)

        const yearsBeforeCurrYear = '[' + curryearList.join(', ') + ']'

        console.log(">>>>>>>>>>>>>>>> bikin kondisi baru disini >>>>>>>>>>>>>>>>>>>>>>")
        let whereClause = {};
        let whereClauseSegment = {};

        if (group != '') {
            if (group == 'FIT') {
                if (year != '' && comparison == '') {
                    if (month == '') {
                        whereClause = {
                            market_code: {
                                [Op.notIn]: ['HSU', 'PF', '']
                            },
                            group_status: {
                                [Op.notIn]: ['LOS', 'PEN', 'PRO']
                            },
                            room_type: {
                                [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                            },
                            market_group: {
                                [Op.notIn]: ['GRL', 'GRO', 'GRW', 'MICE']
                            },
                            [Op.and]: [
                                Sequelize.where(Sequelize.col('country'), { [Op.ne]: '' }),
                                Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear)
                            ]
                        };
                    } else {
                        whereClause = {
                            market_code: {
                                [Op.notIn]: ['HSU', 'PF', '']
                            },
                            group_status: {
                                [Op.notIn]: ['LOS', 'PEN', 'PRO']
                            },
                            room_type: {
                                [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                            },
                            market_group: {
                                [Op.notIn]: ['GRL', 'GRO', 'GRW', 'MICE']
                            },
                            [Op.and]: [
                                Sequelize.where(Sequelize.col('country'), { [Op.ne]: '' }),
                                Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear),
                                Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month),
                            ]
                        };
                    }
                } else if (year != '' && comparison != '') {
                    if (month == '') {
                        whereClause = {
                            market_code: {
                                [Op.notIn]: ['HSU', 'PF', '']
                            },
                            group_status: {
                                [Op.notIn]: ['LOS', 'PEN', 'PRO']
                            },
                            room_type: {
                                [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                            },
                            market_group: {
                                [Op.notIn]: ['GRL', 'GRO', 'GRW', 'MICE']
                            },
                            [Op.and]: [
                                Sequelize.where(Sequelize.col('country'), { [Op.ne]: '' }),
                                Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                            ]
                        };
                    } else {
                        whereClause = {
                            market_code: {
                                [Op.notIn]: ['HSU', 'PF', '']
                            },
                            group_status: {
                                [Op.notIn]: ['LOS', 'PEN', 'PRO']
                            },
                            room_type: {
                                [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                            },
                            market_group: {
                                [Op.notIn]: ['GRL', 'GRO', 'GRW', 'MICE']
                            },
                            [Op.and]: [
                                Sequelize.where(Sequelize.col('country'), { [Op.ne]: '' }),
                                Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                                Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month),
                            ]
                        };
                    }
                } else {
                    if (month == '') {
                        whereClause = {
                            market_code: {
                                [Op.notIn]: ['HSU', 'PF', '']
                            },
                            group_status: {
                                [Op.notIn]: ['LOS', 'PEN', 'PRO']
                            },
                            room_type: {
                                [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                            },
                            market_group: {
                                [Op.notIn]: ['GRL', 'GRO', 'GRW', 'MICE']
                            },
                            [Op.and]: [
                                Sequelize.where(Sequelize.col('country'), { [Op.ne]: '' }),
                                Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                            ]
                        };
                    } else {
                        whereClause = {
                            market_code: {
                                [Op.notIn]: ['HSU', 'PF', '']
                            },
                            group_status: {
                                [Op.notIn]: ['LOS', 'PEN', 'PRO']
                            },
                            room_type: {
                                [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                            },
                            market_group: {
                                [Op.notIn]: ['GRL', 'GRO', 'GRW', 'MICE']
                            },
                            [Op.and]: [
                                Sequelize.where(Sequelize.col('country'), { [Op.ne]: '' }),
                                Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                                Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month),
                            ]
                        };
                    }
                }
            } else { // GIT
                if (year != '' && comparison == '') {
                    if (month == '') {
                        whereClause = {
                            market_code: {
                                [Op.notIn]: ['HSU', 'PF', '']
                            },
                            group_status: {
                                [Op.notIn]: ['LOS', 'PEN', 'PRO']
                            },
                            room_type: {
                                [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                            },
                            market_group: {
                                [Op.in]: ['GRL', 'GRO', 'GRW', 'MICE']
                            },
                            [Op.and]: [
                                Sequelize.where(Sequelize.col('country'), { [Op.ne]: '' }),
                                Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear)
                            ]
                        };
                    } else {
                        whereClause = {
                            market_code: {
                                [Op.notIn]: ['HSU', 'PF', '']
                            },
                            group_status: {
                                [Op.notIn]: ['LOS', 'PEN', 'PRO']
                            },
                            room_type: {
                                [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                            },
                            market_group: {
                                [Op.in]: ['GRL', 'GRO', 'GRW', 'MICE']
                            },
                            [Op.and]: [
                                Sequelize.where(Sequelize.col('country'), { [Op.ne]: '' }),
                                Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear),
                                Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month),
                            ]
                        };
                    }
                } else if (year != '' && comparison != '') {
                    if (month == '') {
                        whereClause = {
                            market_code: {
                                [Op.notIn]: ['HSU', 'PF', '']
                            },
                            group_status: {
                                [Op.notIn]: ['LOS', 'PEN', 'PRO']
                            },
                            room_type: {
                                [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                            },
                            market_group: {
                                [Op.in]: ['GRL', 'GRO', 'GRW', 'MICE']
                            },
                            [Op.and]: [
                                Sequelize.where(Sequelize.col('country'), { [Op.ne]: '' }),
                                Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                            ]
                        };
                    } else {
                        whereClause = {
                            market_code: {
                                [Op.notIn]: ['HSU', 'PF', '']
                            },
                            group_status: {
                                [Op.notIn]: ['LOS', 'PEN', 'PRO']
                            },
                            room_type: {
                                [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                            },
                            market_group: {
                                [Op.in]: ['GRL', 'GRO', 'GRW', 'MICE']
                            },
                            [Op.and]: [
                                Sequelize.where(Sequelize.col('country'), { [Op.ne]: '' }),
                                Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                                Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month),
                            ]
                        };
                    }
                } else {
                    if (month == '') {
                        whereClause = {
                            market_code: {
                                [Op.notIn]: ['HSU', 'PF', '']
                            },
                            group_status: {
                                [Op.notIn]: ['LOS', 'PEN', 'PRO']
                            },
                            room_type: {
                                [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                            },
                            market_group: {
                                [Op.in]: ['GRL', 'GRO', 'GRW', 'MICE']
                            },
                            [Op.and]: [
                                Sequelize.where(Sequelize.col('country'), { [Op.ne]: '' }),
                                Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                            ]
                        };
                    } else {
                        whereClause = {
                            market_code: {
                                [Op.notIn]: ['HSU', 'PF', '']
                            },
                            group_status: {
                                [Op.notIn]: ['LOS', 'PEN', 'PRO']
                            },
                            room_type: {
                                [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                            },
                            market_group: {
                                [Op.in]: ['GRL', 'GRO', 'GRW', 'MICE']
                            },
                            [Op.and]: [
                                Sequelize.where(Sequelize.col('country'), { [Op.ne]: '' }),
                                Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                                Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month),
                            ]
                        };
                    }
                }
            }
        } else { // JIKA GROUP KOSONG
            if (year != '' && comparison == '') {
                if (month == '') {
                    whereClause = {
                        market_code: {
                            [Op.notIn]: ['HSU', 'PF', '']
                        },
                        group_status: {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        },
                        room_type: {
                            [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                        },
                        [Op.and]: [
                            Sequelize.where(Sequelize.col('country'), { [Op.ne]: '' }),
                            Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear)
                        ]
                    };
                } else {
                    whereClause = {
                        market_code: {
                            [Op.notIn]: ['HSU', 'PF', '']
                        },
                        group_status: {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        },
                        room_type: {
                            [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                        },
                        [Op.and]: [
                            Sequelize.where(Sequelize.col('country'), { [Op.ne]: '' }),
                            Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear),
                            Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month),
                        ]
                    };
                }
            } else if (year != '' && comparison != '') {
                if (month == '') {
                    whereClause = {
                        market_code: {
                            [Op.notIn]: ['HSU', 'PF', '']
                        },
                        group_status: {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        },
                        room_type: {
                            [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                        },
                        [Op.and]: [
                            Sequelize.where(Sequelize.col('country'), { [Op.ne]: '' }),
                            Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                        ]
                    };
                } else {
                    whereClause = {
                        market_code: {
                            [Op.notIn]: ['HSU', 'PF', '']
                        },
                        group_status: {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        },
                        room_type: {
                            [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                        },
                        [Op.and]: [
                            Sequelize.where(Sequelize.col('country'), { [Op.ne]: '' }),
                            Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                            Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month),
                        ]
                    };
                }
            } else {
                if (month == '') {
                    whereClause = {
                        market_code: {
                            [Op.notIn]: ['HSU', 'PF', '']
                        },
                        group_status: {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        },
                        room_type: {
                            [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                        },
                        [Op.and]: [
                            Sequelize.where(Sequelize.col('country'), { [Op.ne]: '' }),
                            Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                        ]
                    };
                } else {
                    whereClause = {
                        market_code: {
                            [Op.notIn]: ['HSU', 'PF', '']
                        },
                        group_status: {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        },
                        room_type: {
                            [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                        },
                        [Op.and]: [
                            Sequelize.where(Sequelize.col('country'), { [Op.ne]: '' }),
                            Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                            Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month),
                        ]
                    };
                }
            }
        }

        if (hotel != '') {
            whereClause['room_class'] = hotel;
        }

        if (segment != '') {
            whereClauseSegment['segment'] = segment;
        }

        if (description != '') {
            whereClauseSegment['description'] = description;
        }

        if (country != '') {
            whereClause['country'] = country;
        }

        const include_option = [
            {
                model: MarketSegment,
                as: 'marketsegment',
                attributes: [],  // just make relation not show column
                required: false,  //left outer join; true : inner join
                where: whereClauseSegment
            }
        ]

        if (description != '' || segment != '') {
            include_option[0].required = true
        }

        const attributes = [];
        const years = JSON.parse(yearsBeforeCurrYear);

        for (let i = 0; i < years.length; i++) {
            const year = years[i];

            const attribute = [
                Sequelize.fn(
                    'SUM',
                    Sequelize.literal(
                        "CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " +
                        year +
                        ' THEN `rn` ELSE 0 END'
                    )
                ),
                'room_night_' + year,
            ];

            attributes.push(attribute);
        }

        console.log(whereClause, ">>>>>>>>>>>>>>>>>>>> ISI KONDISI >>>>>>>>>>>>>>>>>>>")
        if (year != '' && comparison == '') { // year terisi, comparison null
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'country',
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'room_night'],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: ['country'],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC'],
                        ['country', 'ASC'],
                    ],
                    limit: 10,
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'country',
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + prevYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + prevYear,
                        ],
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + currYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + currYear,
                        ],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: ["country"],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'country',
                        ...attributes,
                    ],
                    include: include_option,
                    where: whereClause,
                    group: ["country"],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        }
    };

    // ---------------------------------------------------
    // MTD TOP LENGTH OF STAY
    // ---------------------------------------------------
    static async getMtdTopLengthStay(req, res) {
        const { hotel, year, comparison, month, group, segment, description, country } = req.query;

        var currentTime = new Date();
        var currYear = year > 0 ? year : currentTime.getFullYear();
        var prevYear = comparison > 0 ? Number(comparison) : currentTime.getFullYear() - 1;
        var nextYear = currentTime.getFullYear() + 1

        // list 1 years before current year
        const startCurrYear = currYear - 1;
        const curryearList = [];

        for (let tahun = startCurrYear; tahun <= currYear; tahun++) {
            if (tahun != 2020 && tahun != 2021) {
                curryearList.push(tahun);
            }
        }

        curryearList.push(nextYear)

        const yearsBeforeCurrYear = '[' + curryearList.join(', ') + ']'

        console.log(">>>>>>>>>>>>>>>> bikin kondisi baru disini >>>>>>>>>>>>>>>>>>>>>>")

        let whereClause = {};
        let whereClauseSegment = {};

        if (group != '') {
            if (group == 'FIT') {
                if (year != '' && comparison == '') {
                    if (month == '') {
                        whereClause = {
                            market_code: {
                                [Op.notIn]: ['HSU', 'PF', '']
                            },
                            group_status: {
                                [Op.notIn]: ['LOS', 'PEN', 'PRO']
                            },
                            room_type: {
                                [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                            },
                            market_group: {
                                [Op.notIn]: ['GRL', 'GRO', 'GRW', 'MICE']
                            },
                            [Op.and]: [
                                Sequelize.where(Sequelize.col('los'), { [Op.ne]: '' }),
                                Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear)
                            ]
                        };
                    } else {
                        whereClause = {
                            market_code: {
                                [Op.notIn]: ['HSU', 'PF', '']
                            },
                            group_status: {
                                [Op.notIn]: ['LOS', 'PEN', 'PRO']
                            },
                            room_type: {
                                [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                            },
                            market_group: {
                                [Op.notIn]: ['GRL', 'GRO', 'GRW', 'MICE']
                            },
                            [Op.and]: [
                                Sequelize.where(Sequelize.col('los'), { [Op.ne]: '' }),
                                Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear),
                                Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month),
                            ]
                        };
                    }
                } else if (year != '' && comparison != '') {
                    if (month == '') {
                        whereClause = {
                            market_code: {
                                [Op.notIn]: ['HSU', 'PF', '']
                            },
                            group_status: {
                                [Op.notIn]: ['LOS', 'PEN', 'PRO']
                            },
                            room_type: {
                                [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                            },
                            market_group: {
                                [Op.notIn]: ['GRL', 'GRO', 'GRW', 'MICE']
                            },
                            [Op.and]: [
                                Sequelize.where(Sequelize.col('los'), { [Op.ne]: '' }),
                                Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                            ]
                        };
                    } else {
                        whereClause = {
                            market_code: {
                                [Op.notIn]: ['HSU', 'PF', '']
                            },
                            group_status: {
                                [Op.notIn]: ['LOS', 'PEN', 'PRO']
                            },
                            room_type: {
                                [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                            },
                            market_group: {
                                [Op.notIn]: ['GRL', 'GRO', 'GRW', 'MICE']
                            },
                            [Op.and]: [
                                Sequelize.where(Sequelize.col('los'), { [Op.ne]: '' }),
                                Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                                Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month),
                            ]
                        };
                    }
                } else {
                    if (month == '') {
                        whereClause = {
                            market_code: {
                                [Op.notIn]: ['HSU', 'PF', '']
                            },
                            group_status: {
                                [Op.notIn]: ['LOS', 'PEN', 'PRO']
                            },
                            room_type: {
                                [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                            },
                            market_group: {
                                [Op.notIn]: ['GRL', 'GRO', 'GRW', 'MICE']
                            },
                            [Op.and]: [
                                Sequelize.where(Sequelize.col('los'), { [Op.ne]: '' }),
                                Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                            ]
                        };
                    } else {
                        whereClause = {
                            market_code: {
                                [Op.notIn]: ['HSU', 'PF', '']
                            },
                            group_status: {
                                [Op.notIn]: ['LOS', 'PEN', 'PRO']
                            },
                            room_type: {
                                [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                            },
                            market_group: {
                                [Op.notIn]: ['GRL', 'GRO', 'GRW', 'MICE']
                            },
                            [Op.and]: [
                                Sequelize.where(Sequelize.col('los'), { [Op.ne]: '' }),
                                Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                                Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month),
                            ]
                        };
                    }
                }
            } else { // GIT
                if (year != '' && comparison == '') {
                    if (month == '') {
                        whereClause = {
                            market_code: {
                                [Op.notIn]: ['HSU', 'PF', '']
                            },
                            group_status: {
                                [Op.notIn]: ['LOS', 'PEN', 'PRO']
                            },
                            room_type: {
                                [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                            },
                            market_group: {
                                [Op.in]: ['GRL', 'GRO', 'GRW', 'MICE']
                            },
                            [Op.and]: [
                                Sequelize.where(Sequelize.col('los'), { [Op.ne]: '' }),
                                Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear)
                            ]
                        };
                    } else {
                        whereClause = {
                            market_code: {
                                [Op.notIn]: ['HSU', 'PF', '']
                            },
                            group_status: {
                                [Op.notIn]: ['LOS', 'PEN', 'PRO']
                            },
                            room_type: {
                                [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                            },
                            market_group: {
                                [Op.in]: ['GRL', 'GRO', 'GRW', 'MICE']
                            },
                            [Op.and]: [
                                Sequelize.where(Sequelize.col('los'), { [Op.ne]: '' }),
                                Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear),
                                Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month),
                            ]
                        };
                    }
                } else if (year != '' && comparison != '') {
                    if (month == '') {
                        whereClause = {
                            market_code: {
                                [Op.notIn]: ['HSU', 'PF', '']
                            },
                            group_status: {
                                [Op.notIn]: ['LOS', 'PEN', 'PRO']
                            },
                            room_type: {
                                [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                            },
                            market_group: {
                                [Op.in]: ['GRL', 'GRO', 'GRW', 'MICE']
                            },
                            [Op.and]: [
                                Sequelize.where(Sequelize.col('los'), { [Op.ne]: '' }),
                                Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                            ]
                        };
                    } else {
                        whereClause = {
                            market_code: {
                                [Op.notIn]: ['HSU', 'PF', '']
                            },
                            group_status: {
                                [Op.notIn]: ['LOS', 'PEN', 'PRO']
                            },
                            room_type: {
                                [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                            },
                            market_group: {
                                [Op.in]: ['GRL', 'GRO', 'GRW', 'MICE']
                            },
                            [Op.and]: [
                                Sequelize.where(Sequelize.col('los'), { [Op.ne]: '' }),
                                Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                                Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month),
                            ]
                        };
                    }
                } else {
                    if (month == '') {
                        whereClause = {
                            market_code: {
                                [Op.notIn]: ['HSU', 'PF', '']
                            },
                            group_status: {
                                [Op.notIn]: ['LOS', 'PEN', 'PRO']
                            },
                            room_type: {
                                [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                            },
                            market_group: {
                                [Op.in]: ['GRL', 'GRO', 'GRW', 'MICE']
                            },
                            [Op.and]: [
                                Sequelize.where(Sequelize.col('los'), { [Op.ne]: '' }),
                                Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                            ]
                        };
                    } else {
                        whereClause = {
                            market_code: {
                                [Op.notIn]: ['HSU', 'PF', '']
                            },
                            group_status: {
                                [Op.notIn]: ['LOS', 'PEN', 'PRO']
                            },
                            room_type: {
                                [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                            },
                            market_group: {
                                [Op.in]: ['GRL', 'GRO', 'GRW', 'MICE']
                            },
                            [Op.and]: [
                                Sequelize.where(Sequelize.col('los'), { [Op.ne]: '' }),
                                Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                                Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month),
                            ]
                        };
                    }
                }
            }
        } else { // JIKA GROUP KOSONG
            if (year != '' && comparison == '') {
                if (month == '') {
                    whereClause = {
                        market_code: {
                            [Op.notIn]: ['HSU', 'PF', '']
                        },
                        group_status: {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        },
                        room_type: {
                            [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                        },
                        [Op.and]: [
                            Sequelize.where(Sequelize.col('los'), { [Op.ne]: '' }),
                            Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), year)
                        ]
                    };
                } else {
                    whereClause = {
                        market_code: {
                            [Op.notIn]: ['HSU', 'PF', '']
                        },
                        group_status: {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        },
                        room_type: {
                            [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                        },
                        [Op.and]: [
                            Sequelize.where(Sequelize.col('los'), { [Op.ne]: '' }),
                            Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear),
                            Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month),
                        ]
                    };
                }
            } else if (year != '' && comparison != '') {
                if (month == '') {
                    whereClause = {
                        market_code: {
                            [Op.notIn]: ['HSU', 'PF', '']
                        },
                        group_status: {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        },
                        room_type: {
                            [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                        },
                        [Op.and]: [
                            Sequelize.where(Sequelize.col('los'), { [Op.ne]: '' }),
                            Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                        ]
                    };
                } else {
                    whereClause = {
                        market_code: {
                            [Op.notIn]: ['HSU', 'PF', '']
                        },
                        group_status: {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        },
                        room_type: {
                            [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                        },
                        [Op.and]: [
                            Sequelize.where(Sequelize.col('los'), { [Op.ne]: '' }),
                            Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                            Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month),
                        ]
                    };
                }
            } else {
                if (month == '') {
                    whereClause = {
                        market_code: {
                            [Op.notIn]: ['HSU', 'PF', '']
                        },
                        group_status: {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        },
                        room_type: {
                            [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                        },
                        [Op.and]: [
                            Sequelize.where(Sequelize.col('los'), { [Op.ne]: '' }),
                            Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                        ]
                    };
                } else {
                    whereClause = {
                        market_code: {
                            [Op.notIn]: ['HSU', 'PF', '']
                        },
                        group_status: {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        },
                        room_type: {
                            [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                        },
                        [Op.and]: [
                            Sequelize.where(Sequelize.col('los'), { [Op.ne]: '' }),
                            Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                            Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month),
                        ]
                    };
                }
            }
        }

        if (hotel != '') {
            whereClause['room_class'] = hotel;
        }

        if (segment != '') {
            whereClauseSegment['segment'] = segment;
        }

        if (description != '') {
            whereClauseSegment['description'] = description;
        }

        if (country != '') {
            whereClause['country'] = country;
        }

        const include_option = [
            {
                model: MarketSegment,
                as: 'marketsegment',
                attributes: [],  // just make relation not show column
                required: false,  //left outer join; true : inner join
                where: whereClauseSegment
            }
        ]

        if (description != '' || segment != '') {
            include_option[0].required = true
        }

        const attributes = [];
        const years = JSON.parse(yearsBeforeCurrYear);

        for (let i = 0; i < years.length; i++) {
            const year = years[i];

            const attribute = [
                Sequelize.fn(
                    'SUM',
                    Sequelize.literal(
                        "CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " +
                        year +
                        ' THEN `rn` ELSE 0 END'
                    )
                ),
                'room_night_' + year,
            ];

            attributes.push(attribute);
        }

        console.log(whereClause, ">>>>>>>>>>>>>>>>>>>> ISI KONDISI >>>>>>>>>>>>>>>>>>>")

        if (year != '' && comparison == '') {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'los',
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'room_night'],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: ['los'],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC'],
                        ['los', 'ASC'],
                    ],
                    limit: 10,
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'los',
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + prevYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + prevYear,
                        ],
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + currYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + currYear,
                        ],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: ["los"],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'los',
                        ...attributes,
                    ],
                    include: include_option,
                    where: whereClause,
                    group: ["los"],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        }
    };

    // ---------------------------------------------------
    // MTD TOP BOOKING LEAD TIME
    // ---------------------------------------------------
    static async getMtdTopBookingLeadTime(req, res) {
        const { hotel, year, comparison, month, group, segment, description, country } = req.query;

        var currentTime = new Date();
        var currYear = year > 0 ? year : currentTime.getFullYear();
        var prevYear = comparison > 0 ? Number(comparison) : currentTime.getFullYear() - 1;
        var nextYear = currentTime.getFullYear() + 1

        // list 1 years before current year
        const startCurrYear = currYear - 1;
        const curryearList = [];

        for (let tahun = startCurrYear; tahun <= currYear; tahun++) {
            if (tahun != 2020 && tahun != 2021) {
                curryearList.push(tahun);
            }
        }

        curryearList.push(nextYear)

        const yearsBeforeCurrYear = '[' + curryearList.join(', ') + ']'

        console.log(">>>>>>>>>>>>>>>> bikin kondisi baru disini >>>>>>>>>>>>>>>>>>>>>>")

        let whereClause = {};
        let whereClauseSegment = {};

        if (group != '') {
            if (group == 'FIT') {
                if (year != '' && comparison == '') {
                    if (month == '') {
                        whereClause = {
                            market_code: {
                                [Op.notIn]: ['HSU', 'PF', '']
                            },
                            group_status: {
                                [Op.notIn]: ['LOS', 'PEN', 'PRO']
                            },
                            room_type: {
                                [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                            },
                            market_group: {
                                [Op.notIn]: ['GRL', 'GRO', 'GRW', 'MICE']
                            },
                            [Op.and]: [
                                Sequelize.where(Sequelize.col('lead_days'), { [Op.ne]: '' }),
                                Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear)
                            ]
                        };
                    } else {
                        whereClause = {
                            market_code: {
                                [Op.notIn]: ['HSU', 'PF', '']
                            },
                            group_status: {
                                [Op.notIn]: ['LOS', 'PEN', 'PRO']
                            },
                            room_type: {
                                [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                            },
                            market_group: {
                                [Op.notIn]: ['GRL', 'GRO', 'GRW', 'MICE']
                            },
                            [Op.and]: [
                                Sequelize.where(Sequelize.col('lead_days'), { [Op.ne]: '' }),
                                Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear),
                                Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month),
                            ]
                        };
                    }
                } else if (year != '' && comparison != '') {
                    if (month == '') {
                        whereClause = {
                            market_code: {
                                [Op.notIn]: ['HSU', 'PF', '']
                            },
                            group_status: {
                                [Op.notIn]: ['LOS', 'PEN', 'PRO']
                            },
                            room_type: {
                                [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                            },
                            market_group: {
                                [Op.notIn]: ['GRL', 'GRO', 'GRW', 'MICE']
                            },
                            [Op.and]: [
                                Sequelize.where(Sequelize.col('lead_days'), { [Op.ne]: '' }),
                                Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                            ]
                        };
                    } else {
                        whereClause = {
                            market_code: {
                                [Op.notIn]: ['HSU', 'PF', '']
                            },
                            group_status: {
                                [Op.notIn]: ['LOS', 'PEN', 'PRO']
                            },
                            room_type: {
                                [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                            },
                            market_group: {
                                [Op.notIn]: ['GRL', 'GRO', 'GRW', 'MICE']
                            },
                            [Op.and]: [
                                Sequelize.where(Sequelize.col('lead_days'), { [Op.ne]: '' }),
                                Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                                Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month),
                            ]
                        };
                    }
                } else {
                    if (month == '') {
                        whereClause = {
                            market_code: {
                                [Op.notIn]: ['HSU', 'PF', '']
                            },
                            group_status: {
                                [Op.notIn]: ['LOS', 'PEN', 'PRO']
                            },
                            room_type: {
                                [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                            },
                            market_group: {
                                [Op.notIn]: ['GRL', 'GRO', 'GRW', 'MICE']
                            },
                            [Op.and]: [
                                Sequelize.where(Sequelize.col('lead_days'), { [Op.ne]: '' }),
                                Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                            ]
                        };
                    } else {
                        whereClause = {
                            market_code: {
                                [Op.notIn]: ['HSU', 'PF', '']
                            },
                            group_status: {
                                [Op.notIn]: ['LOS', 'PEN', 'PRO']
                            },
                            room_type: {
                                [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                            },
                            market_group: {
                                [Op.notIn]: ['GRL', 'GRO', 'GRW', 'MICE']
                            },
                            [Op.and]: [
                                Sequelize.where(Sequelize.col('lead_days'), { [Op.ne]: '' }),
                                Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                                Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month),
                            ]
                        };
                    }
                }
            } else { // GIT
                if (year != '' && comparison == '') {
                    if (month == '') {
                        whereClause = {
                            market_code: {
                                [Op.notIn]: ['HSU', 'PF', '']
                            },
                            group_status: {
                                [Op.notIn]: ['LOS', 'PEN', 'PRO']
                            },
                            room_type: {
                                [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                            },
                            market_group: {
                                [Op.in]: ['GRL', 'GRO', 'GRW', 'MICE']
                            },
                            [Op.and]: [
                                Sequelize.where(Sequelize.col('lead_days'), { [Op.ne]: '' }),
                                Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear)
                            ]
                        };
                    } else {
                        whereClause = {
                            market_code: {
                                [Op.notIn]: ['HSU', 'PF', '']
                            },
                            group_status: {
                                [Op.notIn]: ['LOS', 'PEN', 'PRO']
                            },
                            room_type: {
                                [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                            },
                            market_group: {
                                [Op.in]: ['GRL', 'GRO', 'GRW', 'MICE']
                            },
                            [Op.and]: [
                                Sequelize.where(Sequelize.col('lead_days'), { [Op.ne]: '' }),
                                Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear),
                                Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month),
                            ]
                        };
                    }
                } else if (year != '' && comparison != '') {
                    if (month == '') {
                        whereClause = {
                            market_code: {
                                [Op.notIn]: ['HSU', 'PF', '']
                            },
                            group_status: {
                                [Op.notIn]: ['LOS', 'PEN', 'PRO']
                            },
                            room_type: {
                                [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                            },
                            market_group: {
                                [Op.in]: ['GRL', 'GRO', 'GRW', 'MICE']
                            },
                            [Op.and]: [
                                Sequelize.where(Sequelize.col('lead_days'), { [Op.ne]: '' }),
                                Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                            ]
                        };
                    } else {
                        whereClause = {
                            market_code: {
                                [Op.notIn]: ['HSU', 'PF', '']
                            },
                            group_status: {
                                [Op.notIn]: ['LOS', 'PEN', 'PRO']
                            },
                            room_type: {
                                [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                            },
                            market_group: {
                                [Op.in]: ['GRL', 'GRO', 'GRW', 'MICE']
                            },
                            [Op.and]: [
                                Sequelize.where(Sequelize.col('lead_days'), { [Op.ne]: '' }),
                                Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                                Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month),
                            ]
                        };
                    }
                } else {
                    if (month == '') {
                        whereClause = {
                            market_code: {
                                [Op.notIn]: ['HSU', 'PF', '']
                            },
                            group_status: {
                                [Op.notIn]: ['LOS', 'PEN', 'PRO']
                            },
                            room_type: {
                                [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                            },
                            market_group: {
                                [Op.in]: ['GRL', 'GRO', 'GRW', 'MICE']
                            },
                            [Op.and]: [
                                Sequelize.where(Sequelize.col('lead_days'), { [Op.ne]: '' }),
                                Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                            ]
                        };
                    } else {
                        whereClause = {
                            market_code: {
                                [Op.notIn]: ['HSU', 'PF', '']
                            },
                            group_status: {
                                [Op.notIn]: ['LOS', 'PEN', 'PRO']
                            },
                            room_type: {
                                [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                            },
                            market_group: {
                                [Op.in]: ['GRL', 'GRO', 'GRW', 'MICE']
                            },
                            [Op.and]: [
                                Sequelize.where(Sequelize.col('lead_days'), { [Op.ne]: '' }),
                                Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                                Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month),
                            ]
                        };
                    }
                }
            }
        } else { // JIKA GROUP KOSONG
            if (year != '' && comparison == '') {
                if (month == '') {
                    whereClause = {
                        market_code: {
                            [Op.notIn]: ['HSU', 'PF', '']
                        },
                        group_status: {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        },
                        room_type: {
                            [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                        },
                        [Op.and]: [
                            Sequelize.where(Sequelize.col('lead_days'), { [Op.ne]: '' }),
                            Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), year)
                        ]
                    };
                } else {
                    whereClause = {
                        market_code: {
                            [Op.notIn]: ['HSU', 'PF', '']
                        },
                        group_status: {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        },
                        room_type: {
                            [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                        },
                        [Op.and]: [
                            Sequelize.where(Sequelize.col('lead_days'), { [Op.ne]: '' }),
                            Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), year),
                            Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month),
                        ]
                    };
                }
            } else if (year != '' && comparison != '') {
                if (month == '') {
                    whereClause = {
                        market_code: {
                            [Op.notIn]: ['HSU', 'PF', '']
                        },
                        group_status: {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        },
                        room_type: {
                            [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                        },
                        [Op.and]: [
                            Sequelize.where(Sequelize.col('lead_days'), { [Op.ne]: '' }),
                            Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                        ]
                    };
                } else {
                    whereClause = {
                        market_code: {
                            [Op.notIn]: ['HSU', 'PF', '']
                        },
                        group_status: {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        },
                        room_type: {
                            [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                        },
                        [Op.and]: [
                            Sequelize.where(Sequelize.col('lead_days'), { [Op.ne]: '' }),
                            Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                            Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month),
                        ]
                    };
                }
            } else {
                if (month == '') {
                    whereClause = {
                        market_code: {
                            [Op.notIn]: ['HSU', 'PF', '']
                        },
                        group_status: {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        },
                        room_type: {
                            [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                        },
                        [Op.and]: [
                            Sequelize.where(Sequelize.col('lead_days'), { [Op.ne]: '' }),
                            Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                        ]
                    };
                } else {
                    whereClause = {
                        market_code: {
                            [Op.notIn]: ['HSU', 'PF', '']
                        },
                        group_status: {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        },
                        room_type: {
                            [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                        },
                        [Op.and]: [
                            Sequelize.where(Sequelize.col('lead_days'), { [Op.ne]: '' }),
                            Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                            Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month),
                        ]
                    };
                }
            }
        }

        if (hotel != '') {
            whereClause['room_class'] = hotel;
        }

        if (segment != '') {
            whereClauseSegment['segment'] = segment;
        }

        if (description != '') {
            whereClauseSegment['description'] = description;
        }

        if (country != '') {
            whereClause['country'] = country;
        }

        const include_option = [
            {
                model: MarketSegment,
                as: 'marketsegment',
                attributes: [],  // just make relation not show column
                required: false,  //left outer join; true : inner join
                where: whereClauseSegment
            }
        ]

        if (description != '' || segment != '') {
            include_option[0].required = true
        }

        const attributes = [];
        const years = JSON.parse(yearsBeforeCurrYear);

        for (let i = 0; i < years.length; i++) {
            const year = years[i];

            const attribute = [
                Sequelize.fn(
                    'SUM',
                    Sequelize.literal(
                        "CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " +
                        year +
                        ' THEN `rn` ELSE 0 END'
                    )
                ),
                'room_night_' + year,
            ];

            attributes.push(attribute);
        }

        console.log(whereClause, ">>>>>>>>>>>>>>>>>>>> ISI KONDISI >>>>>>>>>>>>>>>>>>>")

        if (year != '' && comparison == '') {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'lead_days',
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'room_night'],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: ['lead_days'],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC'],
                        ['lead_days', 'ASC'],
                    ],
                    limit: 10,
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'lead_days',
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + prevYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + prevYear,
                        ],
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + currYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + currYear,
                        ],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: ["lead_days"],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'lead_days',
                        ...attributes,
                    ],
                    include: include_option,
                    where: whereClause,
                    group: ["lead_days"],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        }
    };

    // ---------------------------------------------------
    // MTD TOP MARKET SEGMENTS
    // ---------------------------------------------------
    static async getMtdTopMarketSegment(req, res) {
        const { hotel, year, comparison, month, group, segment, description, country } = req.query;

        var currentTime = new Date();
        var currYear = year > 0 ? year : currentTime.getFullYear();
        var prevYear = comparison > 0 ? Number(comparison) : currentTime.getFullYear() - 1;
        var nextYear = currentTime.getFullYear() + 1

        // list 1 years before current year
        const startCurrYear = currYear - 1;
        const curryearList = [];

        for (let tahun = startCurrYear; tahun <= currYear; tahun++) {
            if (tahun != 2020 && tahun != 2021) {
                curryearList.push(tahun);
            }
        }

        curryearList.push(nextYear)

        const yearsBeforeCurrYear = '[' + curryearList.join(', ') + ']'

        console.log(">>>>>>>>>>>>>>>> bikin kondisi baru disini >>>>>>>>>>>>>>>>>>>>>>")

        let whereClause = {};
        let whereClauseSegment = {};

        if (group != '') {
            if (group == 'FIT') {
                if (year != '' && comparison == '') {
                    if (month == '') {
                        whereClause = {
                            market_code: {
                                [Op.notIn]: ['HSU', 'PF', '']
                            },
                            group_status: {
                                [Op.notIn]: ['LOS', 'PEN', 'PRO']
                            },
                            room_type: {
                                [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                            },
                            market_group: {
                                [Op.notIn]: ['GRL', 'GRO', 'GRW', 'MICE']
                            },
                            [Op.and]: [
                                Sequelize.where(Sequelize.col('market_group'), { [Op.ne]: '' }),
                                Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear)
                            ]
                        };
                    } else {
                        whereClause = {
                            market_code: {
                                [Op.notIn]: ['HSU', 'PF', '']
                            },
                            group_status: {
                                [Op.notIn]: ['LOS', 'PEN', 'PRO']
                            },
                            room_type: {
                                [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                            },
                            market_group: {
                                [Op.notIn]: ['GRL', 'GRO', 'GRW', 'MICE']
                            },
                            [Op.and]: [
                                Sequelize.where(Sequelize.col('market_group'), { [Op.ne]: '' }),
                                Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear),
                                Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month),
                            ]
                        };
                    }
                } else if (year != '' && comparison != '') {
                    if (month == '') {
                        whereClause = {
                            market_code: {
                                [Op.notIn]: ['HSU', 'PF', '']
                            },
                            group_status: {
                                [Op.notIn]: ['LOS', 'PEN', 'PRO']
                            },
                            room_type: {
                                [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                            },
                            market_group: {
                                [Op.notIn]: ['GRL', 'GRO', 'GRW', 'MICE']
                            },
                            [Op.and]: [
                                Sequelize.where(Sequelize.col('market_group'), { [Op.ne]: '' }),
                                Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                            ]
                        };
                    } else {
                        whereClause = {
                            market_code: {
                                [Op.notIn]: ['HSU', 'PF', '']
                            },
                            group_status: {
                                [Op.notIn]: ['LOS', 'PEN', 'PRO']
                            },
                            room_type: {
                                [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                            },
                            market_group: {
                                [Op.notIn]: ['GRL', 'GRO', 'GRW', 'MICE']
                            },
                            [Op.and]: [
                                Sequelize.where(Sequelize.col('market_group'), { [Op.ne]: '' }),
                                Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                                Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month),
                            ]
                        };
                    }
                } else {
                    if (month == '') {
                        whereClause = {
                            market_code: {
                                [Op.notIn]: ['HSU', 'PF', '']
                            },
                            group_status: {
                                [Op.notIn]: ['LOS', 'PEN', 'PRO']
                            },
                            room_type: {
                                [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                            },
                            market_group: {
                                [Op.notIn]: ['GRL', 'GRO', 'GRW', 'MICE']
                            },
                            [Op.and]: [
                                Sequelize.where(Sequelize.col('market_group'), { [Op.ne]: '' }),
                                Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                            ]
                        };
                    } else {
                        whereClause = {
                            market_code: {
                                [Op.notIn]: ['HSU', 'PF', '']
                            },
                            group_status: {
                                [Op.notIn]: ['LOS', 'PEN', 'PRO']
                            },
                            room_type: {
                                [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                            },
                            market_group: {
                                [Op.notIn]: ['GRL', 'GRO', 'GRW', 'MICE']
                            },
                            [Op.and]: [
                                Sequelize.where(Sequelize.col('market_group'), { [Op.ne]: '' }),
                                Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                                Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month),
                            ]
                        };
                    }
                }
            } else { // GIT
                if (year != '' && comparison == '') {
                    if (month == '') {
                        whereClause = {
                            market_code: {
                                [Op.notIn]: ['HSU', 'PF', '']
                            },
                            group_status: {
                                [Op.notIn]: ['LOS', 'PEN', 'PRO']
                            },
                            room_type: {
                                [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                            },
                            market_group: {
                                [Op.in]: ['GRL', 'GRO', 'GRW', 'MICE']
                            },
                            [Op.and]: [
                                Sequelize.where(Sequelize.col('market_group'), { [Op.ne]: '' }),
                                Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear)
                            ]
                        };
                    } else {
                        whereClause = {
                            market_code: {
                                [Op.notIn]: ['HSU', 'PF', '']
                            },
                            group_status: {
                                [Op.notIn]: ['LOS', 'PEN', 'PRO']
                            },
                            room_type: {
                                [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                            },
                            market_group: {
                                [Op.in]: ['GRL', 'GRO', 'GRW', 'MICE']
                            },
                            [Op.and]: [
                                Sequelize.where(Sequelize.col('market_group'), { [Op.ne]: '' }),
                                Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear),
                                Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month),
                            ]
                        };
                    }
                } else if (year != '' && comparison != '') {
                    if (month == '') {
                        whereClause = {
                            market_code: {
                                [Op.notIn]: ['HSU', 'PF', '']
                            },
                            group_status: {
                                [Op.notIn]: ['LOS', 'PEN', 'PRO']
                            },
                            room_type: {
                                [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                            },
                            market_group: {
                                [Op.in]: ['GRL', 'GRO', 'GRW', 'MICE']
                            },
                            [Op.and]: [
                                Sequelize.where(Sequelize.col('market_group'), { [Op.ne]: '' }),
                                Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                            ]
                        };
                    } else {
                        whereClause = {
                            market_code: {
                                [Op.notIn]: ['HSU', 'PF', '']
                            },
                            group_status: {
                                [Op.notIn]: ['LOS', 'PEN', 'PRO']
                            },
                            room_type: {
                                [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                            },
                            market_group: {
                                [Op.in]: ['GRL', 'GRO', 'GRW', 'MICE']
                            },
                            [Op.and]: [
                                Sequelize.where(Sequelize.col('market_group'), { [Op.ne]: '' }),
                                Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                                Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month),
                            ]
                        };
                    }
                } else {
                    if (month == '') {
                        whereClause = {
                            market_code: {
                                [Op.notIn]: ['HSU', 'PF', '']
                            },
                            group_status: {
                                [Op.notIn]: ['LOS', 'PEN', 'PRO']
                            },
                            room_type: {
                                [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                            },
                            market_group: {
                                [Op.in]: ['GRL', 'GRO', 'GRW', 'MICE']
                            },
                            [Op.and]: [
                                Sequelize.where(Sequelize.col('market_group'), { [Op.ne]: '' }),
                                Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                            ]
                        };
                    } else {
                        whereClause = {
                            market_code: {
                                [Op.notIn]: ['HSU', 'PF', '']
                            },
                            group_status: {
                                [Op.notIn]: ['LOS', 'PEN', 'PRO']
                            },
                            room_type: {
                                [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                            },
                            market_group: {
                                [Op.in]: ['GRL', 'GRO', 'GRW', 'MICE']
                            },
                            [Op.and]: [
                                Sequelize.where(Sequelize.col('market_group'), { [Op.ne]: '' }),
                                Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                                Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month),
                            ]
                        };
                    }
                }
            }
        } else { // JIKA GROUP KOSONG
            if (year != '' && comparison == '') {
                if (month == '') {
                    whereClause = {
                        market_code: {
                            [Op.notIn]: ['HSU', 'PF', '']
                        },
                        group_status: {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        },
                        room_type: {
                            [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                        },
                        [Op.and]: [
                            Sequelize.where(Sequelize.col('market_group'), { [Op.ne]: '' }),
                            Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear)
                        ]
                    };
                } else {
                    whereClause = {
                        market_code: {
                            [Op.notIn]: ['HSU', 'PF', '']
                        },
                        group_status: {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        },
                        room_type: {
                            [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                        },
                        [Op.and]: [
                            Sequelize.where(Sequelize.col('market_group'), { [Op.ne]: '' }),
                            Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear),
                            Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month),
                        ]
                    };
                }
            } else if (year != '' && comparison != '') {
                if (month == '') {
                    whereClause = {
                        market_code: {
                            [Op.notIn]: ['HSU', 'PF', '']
                        },
                        group_status: {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        },
                        room_type: {
                            [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                        },
                        [Op.and]: [
                            Sequelize.where(Sequelize.col('market_group'), { [Op.ne]: '' }),
                            Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                        ]
                    };
                } else {
                    whereClause = {
                        market_code: {
                            [Op.notIn]: ['HSU', 'PF', '']
                        },
                        group_status: {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        },
                        room_type: {
                            [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                        },
                        [Op.and]: [
                            Sequelize.where(Sequelize.col('market_group'), { [Op.ne]: '' }),
                            Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                            Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month),
                        ]
                    };
                }
            } else {
                if (month == '') {
                    whereClause = {
                        market_code: {
                            [Op.notIn]: ['HSU', 'PF', '']
                        },
                        group_status: {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        },
                        room_type: {
                            [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                        },
                        [Op.and]: [
                            Sequelize.where(Sequelize.col('market_group'), { [Op.ne]: '' }),
                            Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                        ]
                    };
                } else {
                    whereClause = {
                        market_code: {
                            [Op.notIn]: ['HSU', 'PF', '']
                        },
                        group_status: {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        },
                        room_type: {
                            [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                        },
                        [Op.and]: [
                            Sequelize.where(Sequelize.col('market_group'), { [Op.ne]: '' }),
                            Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                            Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month),
                        ]
                    };
                }
            }
        }

        if (hotel != '') {
            whereClause['room_class'] = hotel;
        }

        if (segment != '') {
            whereClauseSegment['segment'] = segment;
        }

        if (description != '') {
            whereClauseSegment['description'] = description;
        }

        if (country != '') {
            whereClause['country'] = country;
        }

        const include_option = [
            {
                model: MarketSegment,
                as: 'marketsegment',
                attributes: [],  // just make relation not show column
                required: false,  //left outer join; true : inner join
                where: whereClauseSegment
            }
        ]

        if (description != '' || segment != '') {
            include_option[0].required = true
        }

        const attributes = [];
        const years = JSON.parse(yearsBeforeCurrYear);

        for (let i = 0; i < years.length; i++) {
            const year = years[i];

            const attribute = [
                Sequelize.fn(
                    'SUM',
                    Sequelize.literal(
                        "CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " +
                        year +
                        ' THEN `rn` ELSE 0 END'
                    )
                ),
                'room_night_' + year,
            ];

            attributes.push(attribute);
        }

        console.log(whereClause, ">>>>>>>>>>>>>>>>>>>> ISI KONDISI >>>>>>>>>>>>>>>>>>>")

        if (year != '' && comparison == '') {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        [Sequelize.col('marketsegment.segment'), 'segment'],
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'room_night'],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: ['marketsegment.segment'],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC'],
                    ],
                    limit: 10,
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        [Sequelize.col('marketsegment.segment'), 'segment'],
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + prevYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + prevYear,
                        ],
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + currYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + currYear,
                        ],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: ['marketsegment.segment'],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        [Sequelize.col('marketsegment.segment'), 'segment'],
                        ...attributes,
                    ],
                    include: include_option,
                    where: whereClause,
                    group: ['marketsegment.segment'],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        }
    };

    // ---------------------------------------------------
    // MTD TOP SUPPORTERS
    // ---------------------------------------------------
    static async getMtdTopSupporters(req, res) {
        const { hotel, year, comparison, month, group, segment, description, country } = req.query;

        var currentTime = new Date();
        var currYear = year > 0 ? year : currentTime.getFullYear();
        var prevYear = comparison > 0 ? Number(comparison) : currentTime.getFullYear() - 1;
        var nextYear = currentTime.getFullYear() + 1

        // list 1 years before current year
        const startCurrYear = currYear - 1;
        const curryearList = [];

        for (let tahun = startCurrYear; tahun <= currYear; tahun++) {
            if (tahun != 2020 && tahun != 2021) {
                curryearList.push(tahun);
            }
        }

        curryearList.push(nextYear)

        const yearsBeforeCurrYear = '[' + curryearList.join(', ') + ']'

        console.log(">>>>>>>>>>>>>>>> bikin kondisi baru disini >>>>>>>>>>>>>>>>>>>>>>")

        let whereClause = {};
        let whereClauseSegment = {};

        if (group != '') {
            if (group == 'FIT') {
                if (year != '' && comparison == '') {
                    if (month == '') {
                        whereClause = {
                            market_code: {
                                [Op.notIn]: ['HSU', 'PF', '']
                            },
                            group_status: {
                                [Op.notIn]: ['LOS', 'PEN', 'PRO']
                            },
                            room_type: {
                                [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                            },
                            market_group: {
                                [Op.notIn]: ['GRL', 'GRO', 'GRW', 'MICE']
                            },
                            [Op.and]: [
                                Sequelize.where(Sequelize.col('agent_name'), { [Op.ne]: '' }),
                                Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear)
                            ]
                        };
                    } else {
                        whereClause = {
                            market_code: {
                                [Op.notIn]: ['HSU', 'PF', '']
                            },
                            group_status: {
                                [Op.notIn]: ['LOS', 'PEN', 'PRO']
                            },
                            room_type: {
                                [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                            },
                            market_group: {
                                [Op.notIn]: ['GRL', 'GRO', 'GRW', 'MICE']
                            },
                            [Op.and]: [
                                Sequelize.where(Sequelize.col('agent_name'), { [Op.ne]: '' }),
                                Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear),
                                Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month),
                            ]
                        };
                    }
                } else if (year != '' && comparison != '') {
                    if (month == '') {
                        whereClause = {
                            market_code: {
                                [Op.notIn]: ['HSU', 'PF', '']
                            },
                            group_status: {
                                [Op.notIn]: ['LOS', 'PEN', 'PRO']
                            },
                            room_type: {
                                [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                            },
                            market_group: {
                                [Op.notIn]: ['GRL', 'GRO', 'GRW', 'MICE']
                            },
                            [Op.and]: [
                                Sequelize.where(Sequelize.col('agent_name'), { [Op.ne]: '' }),
                                Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                            ]
                        };
                    } else {
                        whereClause = {
                            market_code: {
                                [Op.notIn]: ['HSU', 'PF', '']
                            },
                            group_status: {
                                [Op.notIn]: ['LOS', 'PEN', 'PRO']
                            },
                            room_type: {
                                [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                            },
                            market_group: {
                                [Op.notIn]: ['GRL', 'GRO', 'GRW', 'MICE']
                            },
                            [Op.and]: [
                                Sequelize.where(Sequelize.col('agent_name'), { [Op.ne]: '' }),
                                Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                                Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month),
                            ]
                        };
                    }
                } else {
                    if (month == '') {
                        whereClause = {
                            market_code: {
                                [Op.notIn]: ['HSU', 'PF', '']
                            },
                            group_status: {
                                [Op.notIn]: ['LOS', 'PEN', 'PRO']
                            },
                            room_type: {
                                [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                            },
                            market_group: {
                                [Op.notIn]: ['GRL', 'GRO', 'GRW', 'MICE']
                            },
                            [Op.and]: [
                                Sequelize.where(Sequelize.col('agent_name'), { [Op.ne]: '' }),
                                Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                            ]
                        };
                    } else {
                        whereClause = {
                            market_code: {
                                [Op.notIn]: ['HSU', 'PF', '']
                            },
                            group_status: {
                                [Op.notIn]: ['LOS', 'PEN', 'PRO']
                            },
                            room_type: {
                                [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                            },
                            market_group: {
                                [Op.notIn]: ['GRL', 'GRO', 'GRW', 'MICE']
                            },
                            [Op.and]: [
                                Sequelize.where(Sequelize.col('agent_name'), { [Op.ne]: '' }),
                                Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                                Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month),
                            ]
                        };
                    }
                }
            } else { // GIT
                if (year != '' && comparison == '') {
                    if (month == '') {
                        whereClause = {
                            market_code: {
                                [Op.notIn]: ['HSU', 'PF', '']
                            },
                            group_status: {
                                [Op.notIn]: ['LOS', 'PEN', 'PRO']
                            },
                            room_type: {
                                [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                            },
                            market_group: {
                                [Op.in]: ['GRL', 'GRO', 'GRW', 'MICE']
                            },
                            [Op.and]: [
                                Sequelize.where(Sequelize.col('agent_name'), { [Op.ne]: '' }),
                                Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear)
                            ]
                        };
                    } else {
                        whereClause = {
                            market_code: {
                                [Op.notIn]: ['HSU', 'PF', '']
                            },
                            group_status: {
                                [Op.notIn]: ['LOS', 'PEN', 'PRO']
                            },
                            room_type: {
                                [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                            },
                            market_group: {
                                [Op.in]: ['GRL', 'GRO', 'GRW', 'MICE']
                            },
                            [Op.and]: [
                                Sequelize.where(Sequelize.col('agent_name'), { [Op.ne]: '' }),
                                Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear),
                                Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month),
                            ]
                        };
                    }
                } else if (year != '' && comparison != '') {
                    if (month == '') {
                        whereClause = {
                            market_code: {
                                [Op.notIn]: ['HSU', 'PF', '']
                            },
                            group_status: {
                                [Op.notIn]: ['LOS', 'PEN', 'PRO']
                            },
                            room_type: {
                                [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                            },
                            market_group: {
                                [Op.in]: ['GRL', 'GRO', 'GRW', 'MICE']
                            },
                            [Op.and]: [
                                Sequelize.where(Sequelize.col('agent_name'), { [Op.ne]: '' }),
                                Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                            ]
                        };
                    } else {
                        whereClause = {
                            market_code: {
                                [Op.notIn]: ['HSU', 'PF', '']
                            },
                            group_status: {
                                [Op.notIn]: ['LOS', 'PEN', 'PRO']
                            },
                            room_type: {
                                [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                            },
                            market_group: {
                                [Op.in]: ['GRL', 'GRO', 'GRW', 'MICE']
                            },
                            [Op.and]: [
                                Sequelize.where(Sequelize.col('agent_name'), { [Op.ne]: '' }),
                                Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                                Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month),
                            ]
                        };
                    }
                } else {
                    if (month == '') {
                        whereClause = {
                            market_code: {
                                [Op.notIn]: ['HSU', 'PF', '']
                            },
                            group_status: {
                                [Op.notIn]: ['LOS', 'PEN', 'PRO']
                            },
                            room_type: {
                                [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                            },
                            market_group: {
                                [Op.in]: ['GRL', 'GRO', 'GRW', 'MICE']
                            },
                            [Op.and]: [
                                Sequelize.where(Sequelize.col('agent_name'), { [Op.ne]: '' }),
                                Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                            ]
                        };
                    } else {
                        whereClause = {
                            market_code: {
                                [Op.notIn]: ['HSU', 'PF', '']
                            },
                            group_status: {
                                [Op.notIn]: ['LOS', 'PEN', 'PRO']
                            },
                            room_type: {
                                [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                            },
                            market_group: {
                                [Op.in]: ['GRL', 'GRO', 'GRW', 'MICE']
                            },
                            [Op.and]: [
                                Sequelize.where(Sequelize.col('agent_name'), { [Op.ne]: '' }),
                                Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                                Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month),
                            ]
                        };
                    }
                }
            }
        } else { // JIKA GROUP KOSONG
            if (year != '' && comparison == '') {
                if (month == '') {
                    whereClause = {
                        market_code: {
                            [Op.notIn]: ['HSU', 'PF', '']
                        },
                        group_status: {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        },
                        room_type: {
                            [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                        },
                        [Op.and]: [
                            Sequelize.where(Sequelize.col('agent_name'), { [Op.ne]: '' }),
                            Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear)
                        ]
                    };
                } else {
                    whereClause = {
                        market_code: {
                            [Op.notIn]: ['HSU', 'PF', '']
                        },
                        group_status: {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        },
                        room_type: {
                            [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                        },
                        [Op.and]: [
                            Sequelize.where(Sequelize.col('agent_name'), { [Op.ne]: '' }),
                            Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear),
                            Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month),
                        ]
                    };
                }
            } else if (year != '' && comparison != '') {
                if (month == '') {
                    whereClause = {
                        market_code: {
                            [Op.notIn]: ['HSU', 'PF', '']
                        },
                        group_status: {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        },
                        room_type: {
                            [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                        },
                        [Op.and]: [
                            Sequelize.where(Sequelize.col('agent_name'), { [Op.ne]: '' }),
                            Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                        ]
                    };
                } else {
                    whereClause = {
                        market_code: {
                            [Op.notIn]: ['HSU', 'PF', '']
                        },
                        group_status: {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        },
                        room_type: {
                            [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                        },
                        [Op.and]: [
                            Sequelize.where(Sequelize.col('agent_name'), { [Op.ne]: '' }),
                            Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                            Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month),
                        ]
                    };
                }
            } else {
                if (month == '') {
                    whereClause = {
                        market_code: {
                            [Op.notIn]: ['HSU', 'PF', '']
                        },
                        group_status: {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        },
                        room_type: {
                            [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                        },
                        [Op.and]: [
                            Sequelize.where(Sequelize.col('agent_name'), { [Op.ne]: '' }),
                            Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                        ]
                    };
                } else {
                    whereClause = {
                        market_code: {
                            [Op.notIn]: ['HSU', 'PF', '']
                        },
                        group_status: {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        },
                        room_type: {
                            [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                        },
                        [Op.and]: [
                            Sequelize.where(Sequelize.col('agent_name'), { [Op.ne]: '' }),
                            Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                            Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month),
                        ]
                    };
                }
            }
        }

        if (hotel != '') {
            whereClause['room_class'] = hotel;
        }

        if (segment != '') {
            whereClauseSegment['segment'] = segment;
        }

        if (description != '') {
            whereClauseSegment['description'] = description;
        }

        if (country != '') {
            whereClause['country'] = country;
        }

        const include_option = [
            {
                model: MarketSegment,
                as: 'marketsegment',
                attributes: [],  // just make relation not show column
                required: false,  //left outer join; true : inner join
                where: whereClauseSegment
            }
        ]

        if (description != '' || segment != '') {
            include_option[0].required = true
        }

        const attributes = [];
        const years = JSON.parse(yearsBeforeCurrYear);

        for (let i = 0; i < years.length; i++) {
            const year = years[i];

            const attribute = [
                Sequelize.fn(
                    'SUM',
                    Sequelize.literal(
                        "CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " +
                        year +
                        ' THEN `rn` ELSE 0 END'
                    )
                ),
                'room_night_' + year,
            ];

            attributes.push(attribute);
        }

        console.log(whereClause, ">>>>>>>>>>>>>>>>>>>> ISI KONDISI >>>>>>>>>>>>>>>>>>>")

        if (year != '' && comparison == '') {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'agent_name',
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'room_night'],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: ['agent_name'],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC'],
                        ['agent_name', 'ASC'],
                    ],
                    limit: 10,
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'agent_name',
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + prevYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + prevYear,
                        ],
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + currYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + currYear,
                        ],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: ["agent_name"],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'agent_name',
                        ...attributes,
                    ],
                    include: include_option,
                    where: whereClause,
                    group: ["agent_name"],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        }
    };

    // ---------------------------------------------------
    // MTD SUMMARY
    // --------------------------------------------------- 
    static async getMtdSummary(req, res) {
        const { hotel, year, comparison, month, group, segment, description, country } = req.query;

        var currentTime = new Date();
        var currYear = year > 0 ? year : currentTime.getFullYear();
        var prevYear = comparison > 0 ? Number(comparison) : currentTime.getFullYear() - 1;
        var nextYear = currentTime.getFullYear() + 1

        console.log(">>>>>>>>>>>>>>>> bikin kondisi baru disini >>>>>>>>>>>>>>>>>>>>>>")
        let whereClause = {};
        let whereClauseSegment = {}

        if (year != '' && comparison == '') {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', 'PF', '']
                    },
                    group_status: {
                        [Op.notIn]: ['LOS', 'PEN', 'PRO', 'CAN']
                    },
                    room_type: {
                        [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('room_rev_usd'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear)
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', 'PF', '']
                    },
                    group_status: {
                        [Op.notIn]: ['LOS', 'PEN', 'PRO', 'CAN']
                    },
                    room_type: {
                        [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('room_rev_usd'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else if (year != '' && comparison != '') {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', 'PF', '']
                    },
                    group_status: {
                        [Op.notIn]: ['LOS', 'PEN', 'PRO', 'CAN']
                    },
                    room_type: {
                        [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('room_rev_usd'), { [Op.ne]: '' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', 'PF', '']
                    },
                    group_status: {
                        [Op.notIn]: ['LOS', 'PEN', 'PRO', 'CAN']
                    },
                    room_type: {
                        [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('room_rev_usd'), { [Op.ne]: '' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', 'PF', '']
                    },
                    group_status: {
                        [Op.notIn]: ['LOS', 'PEN', 'PRO', 'CAN']
                    },
                    room_type: {
                        [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('room_rev_usd'), { [Op.ne]: '' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', 'PF', '']
                    },
                    group_status: {
                        [Op.notIn]: ['LOS', 'PEN', 'PRO', 'CAN']
                    },
                    room_type: {
                        [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('room_rev_usd'), { [Op.ne]: '' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        }

        if (hotel != '') {
            whereClause['room_class'] = hotel;
        }

        if (group != '') { // filter marketsegment.group sesudah where
            whereClause['group'] = {
                [Op.and]: [
                    Sequelize.where(Sequelize.col('marketsegment.group'), { [Op.eq]: group }),
                ]
            };
        }

        if (segment != '') {
            whereClauseSegment['segment'] = segment;
        }

        if (description != '') {
            whereClauseSegment['description'] = description;
        }

        if (country != '') {
            whereClause['country'] = country;
        }

        const include_option = [
            {
                model: MarketSegment,
                as: 'marketsegment',
                attributes: [],  // just make relation not show column
                required: false,  //left outer join; true : inner join
                where: whereClauseSegment
            },
        ]

        if (description != '' || segment != '') {
            include_option[0].required = true
        }

        try {

            if (year != '' && comparison == '') {

                const result = await Master.findAll({
                    attributes: [
                        [Sequelize.fn('month', Sequelize.fn('str_to_date', Sequelize.col('stay_date'), '%d/%m/%Y')), 'stay_month'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${currYear} THEN REPLACE(rn, ',', '') ELSE 0 END`)), 'XRN_MTD_CURRYEAR'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${currYear} THEN REPLACE(room_rev_usd, ',', '') ELSE 0 END`)), 'XRREV_MTD_CURRYEAR'],
                    ],
                    include: include_option,

                    where: whereClause,
                    group: [
                        Sequelize.fn('month', Sequelize.fn('str_to_date', Sequelize.col('stay_date'), '%d/%m/%Y'))
                    ],
                    order: [
                        [Sequelize.fn('month', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), 'ASC']
                    ],
                });

                // Manipulasi hasil query untuk menghasilkan output yang diinginkan
                const output = result.map(item => {
                    // console.log(XPU23BEFORE23, " <<<<<< SETIAP ITEM")
                    const stay_month = item.dataValues.stay_month;
                    const XRN_MTD_CURRYEAR = Number(item.dataValues.XRN_MTD_CURRYEAR);
                    const XRREV_MTD_CURRYEAR = Number(item.dataValues.XRREV_MTD_CURRYEAR);

                    const XARR_MTD_CURRYEAR = (XRREV_MTD_CURRYEAR / XRN_MTD_CURRYEAR).toFixed(2);

                    return {
                        stay_month,
                        XRN_MTD_CURRYEAR,
                        XRREV_MTD_CURRYEAR,
                        XARR_MTD_CURRYEAR
                    }
                })
                res.status(200).json(output)
            } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
                const result = await Master.findAll({
                    attributes: [
                        [Sequelize.fn('month', Sequelize.fn('str_to_date', Sequelize.col('stay_date'), '%d/%m/%Y')), 'stay_month'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${prevYear} THEN REPLACE(rn, ',', '') ELSE 0 END`)), 'XRN_MTD_PREVYEAR'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${currYear} THEN REPLACE(rn, ',', '') ELSE 0 END`)), 'XRN_MTD_CURRYEAR'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${prevYear} THEN REPLACE(room_rev_usd, ',', '') ELSE 0 END`)), 'XRREV_MTD_PREVYEAR'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${currYear} THEN REPLACE(room_rev_usd, ',', '') ELSE 0 END`)), 'XRREV_MTD_CURRYEAR'],
                    ],
                    include: include_option,

                    where: whereClause,
                    group: [
                        Sequelize.fn('month', Sequelize.fn('str_to_date', Sequelize.col('stay_date'), '%d/%m/%Y'))
                    ],
                    order: [
                        [Sequelize.fn('month', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), 'ASC']
                    ],
                });

                // Manipulasi hasil query untuk menghasilkan output yang diinginkan
                const output = result.map(item => {
                    // console.log(XPU23BEFORE23, " <<<<<< SETIAP ITEM")
                    const stay_month = item.dataValues.stay_month;
                    const XRN_MTD_PREVYEAR = Number(item.dataValues.XRN_MTD_PREVYEAR);
                    const XRN_MTD_CURRYEAR = Number(item.dataValues.XRN_MTD_CURRYEAR);
                    const XRREV_MTD_PREVYEAR = Number(item.dataValues.XRREV_MTD_PREVYEAR);
                    const XRREV_MTD_CURRYEAR = Number(item.dataValues.XRREV_MTD_CURRYEAR);

                    const XARR_MTD_PREVYEAR = (XRREV_MTD_PREVYEAR / XRN_MTD_PREVYEAR).toFixed(2);
                    const XARR_MTD_CURRYEAR = (XRREV_MTD_CURRYEAR / XRN_MTD_CURRYEAR).toFixed(2);

                    return {
                        stay_month,
                        XRN_MTD_PREVYEAR,
                        XRREV_MTD_PREVYEAR,
                        XARR_MTD_PREVYEAR,
                        XRN_MTD_CURRYEAR,
                        XRREV_MTD_CURRYEAR,
                        XARR_MTD_CURRYEAR
                    }

                })

                const filteredOutput = output.map(item => {
                    const filteredItem = { ...item };

                    if (filteredItem.XRN_MTD_PREVYEAR === 0 || isNaN(parseFloat(filteredItem.XRN_MTD_PREVYEAR))) {
                        delete filteredItem.XRN_MTD_PREVYEAR;
                    }

                    if (filteredItem.XRREV_MTD_PREVYEAR === 0 || isNaN(parseFloat(filteredItem.XRREV_MTD_PREVYEAR))) {
                        delete filteredItem.XRREV_MTD_PREVYEAR;
                    }

                    if (filteredItem.XARR_MTD_PREVYEAR === 'NaN') {
                        delete filteredItem.XARR_MTD_PREVYEAR;
                    }

                    if (filteredItem.XRN_MTD_CURRYEAR === 0 || isNaN(parseFloat(filteredItem.XRN_MTD_CURRYEAR))) {
                        delete filteredItem.XRN_MTD_CURRYEAR;
                    }

                    if (filteredItem.XRREV_MTD_CURRYEAR === 0 || isNaN(parseFloat(filteredItem.XRREV_MTD_CURRYEAR))) {
                        delete filteredItem.XRREV_MTD_CURRYEAR;
                    }

                    if (filteredItem.XARR_MTD_CURRYEAR === 'NaN') {
                        delete filteredItem.XARR_MTD_CURRYEAR;
                    }

                    return filteredItem;
                });

                // const filteredYears = [prevYear, currYear]; // Daftar tahun yang ingin Anda filter

                // const filteredData = filteredOutput.filter(item => filteredYears.includes(item.stay_year));

                res.status(200).json(filteredOutput)
            } else {
                const result = await Master.findAll({
                    attributes: [
                        [Sequelize.fn('month', Sequelize.fn('str_to_date', Sequelize.col('stay_date'), '%d/%m/%Y')), 'stay_month'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${prevYear} THEN REPLACE(rn, ',', '') ELSE 0 END`)), 'XRN_MTD_PREVYEAR'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${currYear} THEN REPLACE(rn, ',', '') ELSE 0 END`)), 'XRN_MTD_CURRYEAR'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${nextYear} THEN REPLACE(rn, ',', '') ELSE 0 END`)), 'XRN_MTD_NEXTYEAR'],

                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${prevYear} THEN REPLACE(room_rev_usd, ',', '') ELSE 0 END`)), 'XRREV_MTD_PREVYEAR'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${currYear} THEN REPLACE(room_rev_usd, ',', '') ELSE 0 END`)), 'XRREV_MTD_CURRYEAR'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${nextYear} THEN REPLACE(room_rev_usd, ',', '') ELSE 0 END`)), 'XRREV_MTD_NEXTYEAR'],
                    ],
                    include: include_option,

                    where: whereClause,
                    group: [
                        Sequelize.fn('month', Sequelize.fn('str_to_date', Sequelize.col('stay_date'), '%d/%m/%Y'))
                    ],
                    order: [
                        [Sequelize.fn('month', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), 'ASC']
                    ],
                });

                // Manipulasi hasil query untuk menghasilkan output yang diinginkan
                const output = result.map(item => {
                    // console.log(XPU23BEFORE23, " <<<<<< SETIAP ITEM")
                    const stay_month = item.dataValues.stay_month;
                    const XRN_MTD_PREVYEAR = Number(item.dataValues.XRN_MTD_PREVYEAR);
                    const XRN_MTD_CURRYEAR = Number(item.dataValues.XRN_MTD_CURRYEAR);
                    const XRN_MTD_NEXTYEAR = Number(item.dataValues.XRN_MTD_NEXTYEAR);

                    const XRREV_MTD_PREVYEAR = Number(item.dataValues.XRREV_MTD_PREVYEAR);
                    const XRREV_MTD_CURRYEAR = Number(item.dataValues.XRREV_MTD_CURRYEAR);
                    const XRREV_MTD_NEXTYEAR = Number(item.dataValues.XRREV_MTD_NEXTYEAR);

                    const XARR_MTD_PREVYEAR = (XRREV_MTD_PREVYEAR / XRN_MTD_PREVYEAR).toFixed(2);
                    const XARR_MTD_CURRYEAR = (XRREV_MTD_CURRYEAR / XRN_MTD_CURRYEAR).toFixed(2);
                    const XARR_MTD_NEXTYEAR = (XRREV_MTD_NEXTYEAR / XRN_MTD_NEXTYEAR).toFixed(2);

                    return {
                        stay_month,
                        XRN_MTD_PREVYEAR,
                        XRREV_MTD_PREVYEAR,
                        XARR_MTD_PREVYEAR,
                        XRN_MTD_CURRYEAR,
                        XRREV_MTD_CURRYEAR,
                        XARR_MTD_CURRYEAR,
                        XRN_MTD_NEXTYEAR,
                        XRREV_MTD_NEXTYEAR,
                        XARR_MTD_NEXTYEAR
                    }

                })

                const filteredOutput = output.map(item => {
                    const filteredItem = { ...item };

                    if (filteredItem.XRN_MTD_PREVYEAR === 0 || isNaN(parseFloat(filteredItem.XRN_MTD_PREVYEAR))) {
                        delete filteredItem.XRN_MTD_PREVYEAR;
                    }

                    if (filteredItem.XRREV_MTD_PREVYEAR === 0 || isNaN(parseFloat(filteredItem.XRREV_MTD_PREVYEAR))) {
                        delete filteredItem.XRREV_MTD_PREVYEAR;
                    }

                    if (filteredItem.XARR_MTD_PREVYEAR === 'NaN') {
                        delete filteredItem.XARR_MTD_PREVYEAR;
                    }


                    if (filteredItem.XRN_MTD_CURRYEAR === 0 || isNaN(parseFloat(filteredItem.XRN_MTD_CURRYEAR))) {
                        delete filteredItem.XRN_MTD_CURRYEAR;
                    }

                    if (filteredItem.XRREV_MTD_CURRYEAR === 0 || isNaN(parseFloat(filteredItem.XRREV_MTD_CURRYEAR))) {
                        delete filteredItem.XRREV_MTD_CURRYEAR;
                    }

                    if (filteredItem.XARR_MTD_CURRYEAR === 'NaN') {
                        delete filteredItem.XARR_MTD_CURRYEAR;
                    }


                    if (filteredItem.XRN_MTD_NEXTYEAR === 0 || isNaN(parseFloat(filteredItem.XRN_MTD_NEXTYEAR))) {
                        delete filteredItem.XRN_MTD_NEXTYEAR;
                    }

                    if (filteredItem.XRREV_MTD_NEXTYEAR === 0 || isNaN(parseFloat(filteredItem.XRREV_MTD_NEXTYEAR))) {
                        delete filteredItem.XRREV_MTD_NEXTYEAR;
                    }


                    if (filteredItem.XARR_MTD_NEXTYEAR === 'NaN') {
                        delete filteredItem.XARR_MTD_NEXTYEAR;
                    }

                    return filteredItem;
                });

                // const filteredYears = [2019, 2020, 2021]; // Daftar tahun yang ingin Anda filter

                // const filteredData = filteredOutput.filter(item => !filteredYears.includes(item.stay_year));

                res.status(200).json(filteredOutput)
            }

        } catch (error) {
            console.log(error);
        }
    };

    // ---------------------------------------------------
    // YTD MONTHLY ON THE BOOK
    // ---------------------------------------------------
    static async getYtdMonthlyBooking(req, res) {

        const { hotel, year, comparison, month, group, segment, description, country } = req.query;

        var currentTime = new Date();
        var currYear = year > 0 ? year : currentTime.getFullYear();
        var prevYear = comparison > 0 ? Number(comparison) : currentTime.getFullYear() - 1;
        var nextYear = currentTime.getFullYear() + 1

        // list 1 years before current year
        const startCurrYear = currYear - 1;
        const curryearList = [];

        for (let tahun = startCurrYear; tahun <= currYear; tahun++) {
            if (tahun != 2020 && tahun != 2021) {
                curryearList.push(tahun);
            }
        }

        curryearList.push(nextYear)

        const yearsBeforeCurrYear = '[' + curryearList.join(', ') + ']'

        console.log(">>>>>>>>>>>>>>>> bikin kondisi baru disini >>>>>>>>>>>>>>>>>>>>>>")
        let whereClause = {};
        let whereClauseSegment = {}

        if (group != '') {
            if (group == 'FIT') {
                if (year != '' && comparison == '') {
                    whereClause = {
                        market_code: {
                            [Op.notIn]: ['HSU', 'PF', '']
                        },
                        group_status: {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        },
                        room_type: {
                            [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                        },
                        market_group: {
                            [Op.notIn]: ['GRL', 'GRO', 'GRW', 'MICE']
                        },
                        [Op.and]: [
                            Sequelize.where(Sequelize.col('stay_date'), { [Op.ne]: '' }),
                            Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear)
                        ]
                    };
                } else if (year != '' && comparison != '') {
                    whereClause = {
                        market_code: {
                            [Op.notIn]: ['HSU', 'PF', '']
                        },
                        group_status: {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        },
                        room_type: {
                            [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                        },
                        market_group: {
                            [Op.notIn]: ['GRL', 'GRO', 'GRW', 'MICE']
                        },
                        [Op.and]: [
                            Sequelize.where(Sequelize.col('stay_date'), { [Op.ne]: '' }),
                            Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                        ]
                    };
                } else {
                    whereClause = {
                        market_code: {
                            [Op.notIn]: ['HSU', 'PF', '']
                        },
                        group_status: {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        },
                        room_type: {
                            [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                        },
                        market_group: {
                            [Op.notIn]: ['GRL', 'GRO', 'GRW', 'MICE']
                        },
                        [Op.and]: [
                            Sequelize.where(Sequelize.col('stay_date'), { [Op.ne]: '' }),
                            Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                        ]
                    };
                }
            } else { // GIT
                if (year != '' && comparison == '') {
                    whereClause = {
                        market_code: {
                            [Op.notIn]: ['HSU', 'PF', '']
                        },
                        group_status: {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        },
                        room_type: {
                            [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                        },
                        market_group: {
                            [Op.in]: ['GRL', 'GRO', 'GRW', 'MICE']
                        },
                        [Op.and]: [
                            Sequelize.where(Sequelize.col('stay_date'), { [Op.ne]: '' }),
                            Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear)
                        ]
                    };
                } else if (year != '' && comparison != '') {
                    whereClause = {
                        market_code: {
                            [Op.notIn]: ['HSU', 'PF', '']
                        },
                        group_status: {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        },
                        room_type: {
                            [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                        },
                        market_group: {
                            [Op.in]: ['GRL', 'GRO', 'GRW', 'MICE']
                        },
                        [Op.and]: [
                            Sequelize.where(Sequelize.col('stay_date'), { [Op.ne]: '' }),
                            Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                        ]
                    };
                } else {
                    whereClause = {
                        market_code: {
                            [Op.notIn]: ['HSU', 'PF', '']
                        },
                        group_status: {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        },
                        room_type: {
                            [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                        },
                        market_group: {
                            [Op.in]: ['GRL', 'GRO', 'GRW', 'MICE']
                        },
                        [Op.and]: [
                            Sequelize.where(Sequelize.col('stay_date'), { [Op.ne]: '' }),
                            Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                        ]
                    };
                }
            }
        } else { // JIKA GROUP KOSONG
            if (year != '' && comparison == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', 'PF', '']
                    },
                    group_status: {
                        [Op.notIn]: ['LOS', 'PEN', 'PRO']
                    },
                    room_type: {
                        [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('stay_date'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear)
                    ]
                };
            } else if (year != '' && comparison != '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', 'PF', '']
                    },
                    group_status: {
                        [Op.notIn]: ['LOS', 'PEN', 'PRO']
                    },
                    room_type: {
                        [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('stay_date'), { [Op.ne]: '' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', 'PF', '']
                    },
                    group_status: {
                        [Op.notIn]: ['LOS', 'PEN', 'PRO']
                    },
                    room_type: {
                        [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('stay_date'), { [Op.ne]: '' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                    ]
                };
            }
        }

        if (hotel != '') {
            whereClause['room_class'] = hotel;
        }

        if (segment != '') {
            whereClauseSegment['segment'] = segment;
        }

        if (description != '') {
            whereClauseSegment['description'] = description;
        }

        if (country != '') {
            whereClause['country'] = country;
        }

        const include_option = [
            {
                model: MarketSegment,
                as: 'marketsegment',
                attributes: [],  // just make relation not show column
                required: false,  //left outer join; true : inner join
                where: whereClauseSegment
            }
        ]

        if (description != '' || segment != '') {
            include_option[0].required = true
        }

        const attributes = [];
        const years = JSON.parse(yearsBeforeCurrYear);

        for (let i = 0; i < years.length; i++) {
            const year = years[i];

            const attribute = [
                Sequelize.fn(
                    'SUM',
                    Sequelize.literal(
                        "CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " +
                        year +
                        ' THEN `rn` ELSE 0 END'
                    )
                ),
                'room_night_' + year,
            ];

            attributes.push(attribute);
        }

        // end of condition

        if (year != '' && comparison == '') {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        [Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), 'stay_month'],
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'room_night'],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')),
                    ],
                    order: [
                        [Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), 'ASC'],
                    ],
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        [Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), 'stay_month'],
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + prevYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + prevYear,
                        ],
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + currYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + currYear,
                        ],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')),
                    ],
                    order: [
                        [Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), 'ASC'],
                    ],
                    raw: true,
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        [Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), 'stay_month'],
                        ...attributes,
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')),
                    ],
                    order: [
                        [Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), 'ASC'],
                    ],
                    raw: true,
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        }
    };

    // ---------------------------------------------------
    // YTD TOP COUNTRIES
    // ---------------------------------------------------
    static async getYtdTopCountries(req, res) {
        const { hotel, year, comparison, month, group, segment, description, country } = req.query;

        var currentTime = new Date();
        var currYear = year > 0 ? year : currentTime.getFullYear();
        var prevYear = comparison > 0 ? Number(comparison) : currentTime.getFullYear() - 1;
        var nextYear = currentTime.getFullYear() + 1

        // list 1 years before current year
        const startCurrYear = currYear - 1;
        const curryearList = [];

        for (let tahun = startCurrYear; tahun <= currYear; tahun++) {
            if (tahun != 2020 && tahun != 2021) {
                curryearList.push(tahun);
            }
        }

        curryearList.push(nextYear)

        const yearsBeforeCurrYear = '[' + curryearList.join(', ') + ']'

        console.log(">>>>>>>>>>>>>>>> bikin kondisi baru disini >>>>>>>>>>>>>>>>>>>>>>")
        let whereClause = {};
        let whereClauseSegment = {};

        if (group != '') {
            if (group == 'FIT') {
                if (year != '' && comparison == '') {
                    whereClause = {
                        market_code: {
                            [Op.notIn]: ['HSU', 'PF', '']
                        },
                        group_status: {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        },
                        room_type: {
                            [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                        },
                        market_group: {
                            [Op.notIn]: ['GRL', 'GRO', 'GRW', 'MICE']
                        },
                        [Op.and]: [
                            Sequelize.where(Sequelize.col('country'), { [Op.ne]: '' }),
                            Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear)
                        ]
                    };
                } else if (year != '' && comparison != '') {
                    whereClause = {
                        market_code: {
                            [Op.notIn]: ['HSU', 'PF', '']
                        },
                        group_status: {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        },
                        room_type: {
                            [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                        },
                        market_group: {
                            [Op.notIn]: ['GRL', 'GRO', 'GRW', 'MICE']
                        },
                        [Op.and]: [
                            Sequelize.where(Sequelize.col('country'), { [Op.ne]: '' }),
                            Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                        ]
                    };
                } else {
                    whereClause = {
                        market_code: {
                            [Op.notIn]: ['HSU', 'PF', '']
                        },
                        group_status: {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        },
                        room_type: {
                            [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                        },
                        market_group: {
                            [Op.notIn]: ['GRL', 'GRO', 'GRW', 'MICE']
                        },
                        [Op.and]: [
                            Sequelize.where(Sequelize.col('country'), { [Op.ne]: '' }),
                            Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                        ]
                    };
                }
            } else { // GIT
                if (year != '' && comparison == '') {
                    whereClause = {
                        market_code: {
                            [Op.notIn]: ['HSU', 'PF', '']
                        },
                        group_status: {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        },
                        room_type: {
                            [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                        },
                        market_group: {
                            [Op.in]: ['GRL', 'GRO', 'GRW', 'MICE']
                        },
                        [Op.and]: [
                            Sequelize.where(Sequelize.col('country'), { [Op.ne]: '' }),
                            Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear)
                        ]
                    };
                } else if (year != '' && comparison != '') {
                    whereClause = {
                        market_code: {
                            [Op.notIn]: ['HSU', 'PF', '']
                        },
                        group_status: {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        },
                        room_type: {
                            [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                        },
                        market_group: {
                            [Op.in]: ['GRL', 'GRO', 'GRW', 'MICE']
                        },
                        [Op.and]: [
                            Sequelize.where(Sequelize.col('country'), { [Op.ne]: '' }),
                            Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                        ]
                    };
                } else {
                    whereClause = {
                        market_code: {
                            [Op.notIn]: ['HSU', 'PF', '']
                        },
                        group_status: {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        },
                        room_type: {
                            [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                        },
                        market_group: {
                            [Op.in]: ['GRL', 'GRO', 'GRW', 'MICE']
                        },
                        [Op.and]: [
                            Sequelize.where(Sequelize.col('country'), { [Op.ne]: '' }),
                            Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                        ]
                    };
                }
            }
        } else { // JIKA GROUP KOSONG
            if (year != '' && comparison == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', 'PF', '']
                    },
                    group_status: {
                        [Op.notIn]: ['LOS', 'PEN', 'PRO']
                    },
                    room_type: {
                        [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('country'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear)
                    ]
                };
            } else if (year != '' && comparison != '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', 'PF', '']
                    },
                    group_status: {
                        [Op.notIn]: ['LOS', 'PEN', 'PRO']
                    },
                    room_type: {
                        [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('country'), { [Op.ne]: '' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', 'PF', '']
                    },
                    group_status: {
                        [Op.notIn]: ['LOS', 'PEN', 'PRO']
                    },
                    room_type: {
                        [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('country'), { [Op.ne]: '' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                    ]
                };
            }
        }

        if (hotel != '') {
            whereClause['room_class'] = hotel;
        }

        if (segment != '') {
            whereClauseSegment['segment'] = segment;
        }

        if (description != '') {
            whereClauseSegment['description'] = description;
        }

        if (country != '') {
            whereClause['country'] = country;
        }

        const include_option = [
            {
                model: MarketSegment,
                as: 'marketsegment',
                attributes: [],  // just make relation not show column
                required: false,  //left outer join; true : inner join
                where: whereClauseSegment
            }
        ]

        if (description != '' || segment != '') {
            include_option[0].required = true
        }

        const attributes = [];
        const years = JSON.parse(yearsBeforeCurrYear);

        for (let i = 0; i < years.length; i++) {
            const year = years[i];

            const attribute = [
                Sequelize.fn(
                    'SUM',
                    Sequelize.literal(
                        "CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " +
                        year +
                        ' THEN `rn` ELSE 0 END'
                    )
                ),
                'room_night_' + year,
            ];

            attributes.push(attribute);
        }

        console.log(whereClause, ">>>>>>>>>>>>>>>>>>>> ISI KONDISI >>>>>>>>>>>>>>>>>>>")

        if (year != '' && comparison == '') {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'country',
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'room_night'],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: ['country'],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC'],
                        ['country', 'ASC'],
                    ],
                    limit: 10,
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'country',
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + prevYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + prevYear,
                        ],
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + currYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + currYear,
                        ],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: ["country"],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'country',
                        ...attributes,
                    ],
                    include: include_option,
                    where: whereClause,
                    group: ["country"],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        }
    };

    // YTD TOP LENGTH OF STAY
    // ---------------------------------------------------
    static async getYtdTopLengthStay(req, res) {
        const { hotel, year, comparison, month, group, segment, description, country } = req.query;

        var currentTime = new Date();
        var currYear = year > 0 ? year : currentTime.getFullYear();
        var prevYear = comparison > 0 ? Number(comparison) : currentTime.getFullYear() - 1;
        var nextYear = currentTime.getFullYear() + 1

        // list 1 years before current year
        const startCurrYear = currYear - 1;
        const curryearList = [];

        for (let tahun = startCurrYear; tahun <= currYear; tahun++) {
            if (tahun != 2020 && tahun != 2021) {
                curryearList.push(tahun);
            }
        }

        curryearList.push(nextYear)

        const yearsBeforeCurrYear = '[' + curryearList.join(', ') + ']'

        console.log(">>>>>>>>>>>>>>>> bikin kondisi baru disini >>>>>>>>>>>>>>>>>>>>>>")

        let whereClause = {};
        let whereClauseSegment = {};

        if (group != '') {
            if (group == 'FIT') {
                if (year != '' && comparison == '') {
                    whereClause = {
                        market_code: {
                            [Op.notIn]: ['HSU', 'PF', '']
                        },
                        group_status: {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        },
                        room_type: {
                            [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                        },
                        market_group: {
                            [Op.notIn]: ['GRL', 'GRO', 'GRW', 'MICE']
                        },
                        [Op.and]: [
                            Sequelize.where(Sequelize.col('los'), { [Op.ne]: '' }),
                            Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear)
                        ]
                    };
                } else if (year != '' && comparison != '') {
                    whereClause = {
                        market_code: {
                            [Op.notIn]: ['HSU', 'PF', '']
                        },
                        group_status: {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        },
                        room_type: {
                            [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                        },
                        market_group: {
                            [Op.notIn]: ['GRL', 'GRO', 'GRW', 'MICE']
                        },
                        [Op.and]: [
                            Sequelize.where(Sequelize.col('los'), { [Op.ne]: '' }),
                            Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                        ]
                    };
                } else {
                    whereClause = {
                        market_code: {
                            [Op.notIn]: ['HSU', 'PF', '']
                        },
                        group_status: {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        },
                        room_type: {
                            [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                        },
                        market_group: {
                            [Op.notIn]: ['GRL', 'GRO', 'GRW', 'MICE']
                        },
                        [Op.and]: [
                            Sequelize.where(Sequelize.col('los'), { [Op.ne]: '' }),
                            Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                        ]
                    };
                }
            } else { // GIT
                if (year != '' && comparison == '') {
                    whereClause = {
                        market_code: {
                            [Op.notIn]: ['HSU', 'PF', '']
                        },
                        group_status: {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        },
                        room_type: {
                            [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                        },
                        market_group: {
                            [Op.in]: ['GRL', 'GRO', 'GRW', 'MICE']
                        },
                        [Op.and]: [
                            Sequelize.where(Sequelize.col('los'), { [Op.ne]: '' }),
                            Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear)
                        ]
                    };
                } else if (year != '' && comparison != '') {
                    whereClause = {
                        market_code: {
                            [Op.notIn]: ['HSU', 'PF', '']
                        },
                        group_status: {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        },
                        room_type: {
                            [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                        },
                        market_group: {
                            [Op.in]: ['GRL', 'GRO', 'GRW', 'MICE']
                        },
                        [Op.and]: [
                            Sequelize.where(Sequelize.col('los'), { [Op.ne]: '' }),
                            Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                        ]
                    };
                } else {
                    whereClause = {
                        market_code: {
                            [Op.notIn]: ['HSU', 'PF', '']
                        },
                        group_status: {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        },
                        room_type: {
                            [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                        },
                        market_group: {
                            [Op.in]: ['GRL', 'GRO', 'GRW', 'MICE']
                        },
                        [Op.and]: [
                            Sequelize.where(Sequelize.col('los'), { [Op.ne]: '' }),
                            Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                        ]
                    };
                }
            }
        } else { // JIKA GROUP KOSONG
            if (year != '' && comparison == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', 'PF', '']
                    },
                    group_status: {
                        [Op.notIn]: ['LOS', 'PEN', 'PRO']
                    },
                    room_type: {
                        [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('los'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear)
                    ]
                };
            } else if (year != '' && comparison != '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', 'PF', '']
                    },
                    group_status: {
                        [Op.notIn]: ['LOS', 'PEN', 'PRO']
                    },
                    room_type: {
                        [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('los'), { [Op.ne]: '' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', 'PF', '']
                    },
                    group_status: {
                        [Op.notIn]: ['LOS', 'PEN', 'PRO']
                    },
                    room_type: {
                        [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('los'), { [Op.ne]: '' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                    ]
                };
            }
        }

        if (hotel != '') {
            whereClause['room_class'] = hotel;
        }

        if (segment != '') {
            whereClauseSegment['segment'] = segment;
        }

        if (description != '') {
            whereClauseSegment['description'] = description;
        }

        if (country != '') {
            whereClause['country'] = country;
        }

        const include_option = [
            {
                model: MarketSegment,
                as: 'marketsegment',
                attributes: [],  // just make relation not show column
                required: false,  //left outer join; true : inner join
                where: whereClauseSegment
            }
        ]

        if (description != '' || segment != '') {
            include_option[0].required = true
        }

        const attributes = [];
        const years = JSON.parse(yearsBeforeCurrYear);

        for (let i = 0; i < years.length; i++) {
            const year = years[i];

            const attribute = [
                Sequelize.fn(
                    'SUM',
                    Sequelize.literal(
                        "CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " +
                        year +
                        ' THEN `rn` ELSE 0 END'
                    )
                ),
                'room_night_' + year,
            ];

            attributes.push(attribute);
        }

        console.log(whereClause, ">>>>>>>>>>>>>>>>>>>> ISI KONDISI >>>>>>>>>>>>>>>>>>>")

        if (year != '' && comparison == '') {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'los',
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'room_night'],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: ['los'],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC'],
                        ['los', 'ASC'],
                    ],
                    limit: 10,
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'los',
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + prevYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + prevYear,
                        ],
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + currYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + currYear,
                        ],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: ["los"],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'los',
                        ...attributes,
                    ],
                    include: include_option,
                    where: whereClause,
                    group: ["los"],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        }
    };

    // ---------------------------------------------------
    // YTD TOP BOOKING LEAD TIME
    // ---------------------------------------------------
    static async getYtdTopBookingLeadTime(req, res) {
        const { hotel, year, comparison, month, group, segment, description, country } = req.query;

        var currentTime = new Date();
        var currYear = year > 0 ? year : currentTime.getFullYear();
        var prevYear = comparison > 0 ? Number(comparison) : currentTime.getFullYear() - 1;
        var nextYear = currentTime.getFullYear() + 1

        // list 1 years before current year
        const startCurrYear = currYear - 1;
        const curryearList = [];

        for (let tahun = startCurrYear; tahun <= currYear; tahun++) {
            if (tahun != 2020 && tahun != 2021) {
                curryearList.push(tahun);
            }
        }

        curryearList.push(nextYear)

        const yearsBeforeCurrYear = '[' + curryearList.join(', ') + ']'

        console.log(">>>>>>>>>>>>>>>> bikin kondisi baru disini >>>>>>>>>>>>>>>>>>>>>>")

        let whereClause = {};
        let whereClauseSegment = {};

        if (group != '') {
            if (group == 'FIT') {
                if (year != '' && comparison == '') {
                    whereClause = {
                        market_code: {
                            [Op.notIn]: ['HSU', 'PF', '']
                        },
                        group_status: {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        },
                        room_type: {
                            [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                        },
                        market_group: {
                            [Op.notIn]: ['GRL', 'GRO', 'GRW', 'MICE']
                        },
                        [Op.and]: [
                            Sequelize.where(Sequelize.col('lead_days'), { [Op.ne]: '' }),
                            Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear)
                        ]
                    };
                } else if (year != '' && comparison != '') {
                    whereClause = {
                        market_code: {
                            [Op.notIn]: ['HSU', 'PF', '']
                        },
                        group_status: {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        },
                        room_type: {
                            [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                        },
                        market_group: {
                            [Op.notIn]: ['GRL', 'GRO', 'GRW', 'MICE']
                        },
                        [Op.and]: [
                            Sequelize.where(Sequelize.col('lead_days'), { [Op.ne]: '' }),
                            Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                        ]
                    };
                } else {
                    whereClause = {
                        market_code: {
                            [Op.notIn]: ['HSU', 'PF', '']
                        },
                        group_status: {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        },
                        room_type: {
                            [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                        },
                        market_group: {
                            [Op.notIn]: ['GRL', 'GRO', 'GRW', 'MICE']
                        },
                        [Op.and]: [
                            Sequelize.where(Sequelize.col('lead_days'), { [Op.ne]: '' }),
                            Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                        ]
                    };
                }
            } else { // GIT
                if (year != '' && comparison == '') {
                    whereClause = {
                        market_code: {
                            [Op.notIn]: ['HSU', 'PF', '']
                        },
                        group_status: {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        },
                        room_type: {
                            [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                        },
                        market_group: {
                            [Op.in]: ['GRL', 'GRO', 'GRW', 'MICE']
                        },
                        [Op.and]: [
                            Sequelize.where(Sequelize.col('lead_days'), { [Op.ne]: '' }),
                            Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear)
                        ]
                    };
                } else if (year != '' && comparison != '') {
                    whereClause = {
                        market_code: {
                            [Op.notIn]: ['HSU', 'PF', '']
                        },
                        group_status: {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        },
                        room_type: {
                            [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                        },
                        market_group: {
                            [Op.in]: ['GRL', 'GRO', 'GRW', 'MICE']
                        },
                        [Op.and]: [
                            Sequelize.where(Sequelize.col('lead_days'), { [Op.ne]: '' }),
                            Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                        ]
                    };
                } else {
                    whereClause = {
                        market_code: {
                            [Op.notIn]: ['HSU', 'PF', '']
                        },
                        group_status: {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        },
                        room_type: {
                            [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                        },
                        market_group: {
                            [Op.in]: ['GRL', 'GRO', 'GRW', 'MICE']
                        },
                        [Op.and]: [
                            Sequelize.where(Sequelize.col('lead_days'), { [Op.ne]: '' }),
                            Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                        ]
                    };
                }
            }
        } else { // JIKA GROUP KOSONG
            if (year != '' && comparison == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', 'PF', '']
                    },
                    group_status: {
                        [Op.notIn]: ['LOS', 'PEN', 'PRO']
                    },
                    room_type: {
                        [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('lead_days'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear)
                    ]
                };
            } else if (year != '' && comparison != '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', 'PF', '']
                    },
                    group_status: {
                        [Op.notIn]: ['LOS', 'PEN', 'PRO']
                    },
                    room_type: {
                        [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('lead_days'), { [Op.ne]: '' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', 'PF', '']
                    },
                    group_status: {
                        [Op.notIn]: ['LOS', 'PEN', 'PRO']
                    },
                    room_type: {
                        [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('lead_days'), { [Op.ne]: '' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                    ]
                };
            }
        }


        if (hotel != '') {
            whereClause['room_class'] = hotel;
        }

        if (segment != '') {
            whereClauseSegment['segment'] = segment;
        }

        if (description != '') {
            whereClauseSegment['description'] = description;
        }

        if (country != '') {
            whereClause['country'] = country;
        }

        const include_option = [
            {
                model: MarketSegment,
                as: 'marketsegment',
                attributes: [],  // just make relation not show column
                required: false,  //left outer join; true : inner join
                where: whereClauseSegment
            }
        ]

        if (description != '' || segment != '') {
            include_option[0].required = true
        }

        const attributes = [];
        const years = JSON.parse(yearsBeforeCurrYear);

        for (let i = 0; i < years.length; i++) {
            const year = years[i];

            const attribute = [
                Sequelize.fn(
                    'SUM',
                    Sequelize.literal(
                        "CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " +
                        year +
                        ' THEN `rn` ELSE 0 END'
                    )
                ),
                'room_night_' + year,
            ];

            attributes.push(attribute);
        }

        console.log(whereClause, ">>>>>>>>>>>>>>>>>>>> ISI KONDISI >>>>>>>>>>>>>>>>>>>")

        if (year != '' && comparison == '') {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'lead_days',
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'room_night'],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: ['lead_days'],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC'],
                        ['lead_days', 'ASC'],
                    ],
                    limit: 10,
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'lead_days',
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + prevYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + prevYear,
                        ],
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + currYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + currYear,
                        ],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: ["lead_days"],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'lead_days',
                        ...attributes,
                    ],
                    include: include_option,
                    where: whereClause,
                    group: ["lead_days"],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        }
    };

    // ---------------------------------------------------
    // YTD TOP MARKET SEGMENTS
    // ---------------------------------------------------
    static async getYtdTopMarketSegment(req, res) {
        const { hotel, year, comparison, month, group, segment, description, country } = req.query;

        var currentTime = new Date();
        var currYear = year > 0 ? year : currentTime.getFullYear();
        var prevYear = comparison > 0 ? Number(comparison) : currentTime.getFullYear() - 1;
        var nextYear = currentTime.getFullYear() + 1

        // list 1 years before current year
        const startCurrYear = currYear - 1;
        const curryearList = [];

        for (let tahun = startCurrYear; tahun <= currYear; tahun++) {
            if (tahun != 2020 && tahun != 2021) {
                curryearList.push(tahun);
            }
        }

        curryearList.push(nextYear)

        const yearsBeforeCurrYear = '[' + curryearList.join(', ') + ']'

        console.log(">>>>>>>>>>>>>>>> bikin kondisi baru disini >>>>>>>>>>>>>>>>>>>>>>")

        let whereClause = {};
        let whereClauseSegment = {};

        if (group != '') {
            if (group == 'FIT') {
                if (year != '' && comparison == '') {
                    whereClause = {
                        market_code: {
                            [Op.notIn]: ['HSU', 'PF', '']
                        },
                        group_status: {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        },
                        room_type: {
                            [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                        },
                        market_group: {
                            [Op.notIn]: ['GRL', 'GRO', 'GRW', 'MICE']
                        },
                        [Op.and]: [
                            Sequelize.where(Sequelize.col('market_group'), { [Op.ne]: '' }),
                            Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear)
                        ]
                    };
                } else if (year != '' && comparison != '') {
                    whereClause = {
                        market_code: {
                            [Op.notIn]: ['HSU', 'PF', '']
                        },
                        group_status: {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        },
                        room_type: {
                            [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                        },
                        market_group: {
                            [Op.notIn]: ['GRL', 'GRO', 'GRW', 'MICE']
                        },
                        [Op.and]: [
                            Sequelize.where(Sequelize.col('market_group'), { [Op.ne]: '' }),
                            Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                        ]
                    };
                } else {
                    whereClause = {
                        market_code: {
                            [Op.notIn]: ['HSU', 'PF', '']
                        },
                        group_status: {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        },
                        room_type: {
                            [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                        },
                        market_group: {
                            [Op.notIn]: ['GRL', 'GRO', 'GRW', 'MICE']
                        },
                        [Op.and]: [
                            Sequelize.where(Sequelize.col('market_group'), { [Op.ne]: '' }),
                            Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                        ]
                    };
                }
            } else { // GIT
                if (year != '' && comparison == '') {
                    whereClause = {
                        market_code: {
                            [Op.notIn]: ['HSU', 'PF', '']
                        },
                        group_status: {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        },
                        room_type: {
                            [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                        },
                        market_group: {
                            [Op.in]: ['GRL', 'GRO', 'GRW', 'MICE']
                        },
                        [Op.and]: [
                            Sequelize.where(Sequelize.col('market_group'), { [Op.ne]: '' }),
                            Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear)
                        ]
                    };
                } else if (year != '' && comparison != '') {
                    whereClause = {
                        market_code: {
                            [Op.notIn]: ['HSU', 'PF', '']
                        },
                        group_status: {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        },
                        room_type: {
                            [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                        },
                        market_group: {
                            [Op.in]: ['GRL', 'GRO', 'GRW', 'MICE']
                        },
                        [Op.and]: [
                            Sequelize.where(Sequelize.col('market_group'), { [Op.ne]: '' }),
                            Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                        ]
                    };
                } else {
                    whereClause = {
                        market_code: {
                            [Op.notIn]: ['HSU', 'PF', '']
                        },
                        group_status: {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        },
                        room_type: {
                            [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                        },
                        market_group: {
                            [Op.in]: ['GRL', 'GRO', 'GRW', 'MICE']
                        },
                        [Op.and]: [
                            Sequelize.where(Sequelize.col('market_group'), { [Op.ne]: '' }),
                            Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                        ]
                    };
                }
            }
        } else { // JIKA GROUP KOSONG
            if (year != '' && comparison == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', 'PF', '']
                    },
                    group_status: {
                        [Op.notIn]: ['LOS', 'PEN', 'PRO']
                    },
                    room_type: {
                        [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('market_group'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear)
                    ]
                };
            } else if (year != '' && comparison != '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', 'PF', '']
                    },
                    group_status: {
                        [Op.notIn]: ['LOS', 'PEN', 'PRO']
                    },
                    room_type: {
                        [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('market_group'), { [Op.ne]: '' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', 'PF', '']
                    },
                    group_status: {
                        [Op.notIn]: ['LOS', 'PEN', 'PRO']
                    },
                    room_type: {
                        [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('market_group'), { [Op.ne]: '' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                    ]
                };
            }
        }

        if (hotel != '') {
            whereClause['room_class'] = hotel;
        }

        if (segment != '') {
            whereClauseSegment['segment'] = segment;
        }

        if (description != '') {
            whereClauseSegment['description'] = description;
        }

        if (country != '') {
            whereClause['country'] = country;
        }

        const include_option = [
            {
                model: MarketSegment,
                as: 'marketsegment',
                attributes: [],  // just make relation not show column
                required: false,  //left outer join; true : inner join
                where: whereClauseSegment
            }
        ]

        if (description != '' || segment != '') {
            include_option[0].required = true
        }

        const attributes = [];
        const years = JSON.parse(yearsBeforeCurrYear);

        for (let i = 0; i < years.length; i++) {
            const year = years[i];

            const attribute = [
                Sequelize.fn(
                    'SUM',
                    Sequelize.literal(
                        "CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " +
                        year +
                        ' THEN `rn` ELSE 0 END'
                    )
                ),
                'room_night_' + year,
            ];

            attributes.push(attribute);
        }

        console.log(whereClause, ">>>>>>>>>>>>>>>>>>>> ISI KONDISI >>>>>>>>>>>>>>>>>>>")

        if (year != '' && comparison == '') {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        [Sequelize.col('marketsegment.segment'), 'segment'],
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'room_night'],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: ['marketsegment.segment'],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC'],
                    ],
                    limit: 10,
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        [Sequelize.col('marketsegment.segment'), 'segment'],
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + prevYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + prevYear,
                        ],
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + currYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + currYear,
                        ],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: ['marketsegment.segment'],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        [Sequelize.col('marketsegment.segment'), 'segment'],
                        ...attributes,
                    ],
                    include: include_option,
                    where: whereClause,
                    group: ['marketsegment.segment'],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        }
    };

    // ---------------------------------------------------
    // YTD TOP SUPPORTERS
    // ---------------------------------------------------
    static async getYtdTopSupporters(req, res) {
        const { hotel, year, comparison, month, group, segment, description, country } = req.query;

        var currentTime = new Date();
        var currYear = year > 0 ? year : currentTime.getFullYear();
        var prevYear = comparison > 0 ? Number(comparison) : currentTime.getFullYear() - 1;
        var nextYear = currentTime.getFullYear() + 1

        // list 1 years before current year
        const startCurrYear = currYear - 1;
        const curryearList = [];

        for (let tahun = startCurrYear; tahun <= currYear; tahun++) {
            if (tahun != 2020 && tahun != 2021) {
                curryearList.push(tahun);
            }
        }

        curryearList.push(nextYear)

        const yearsBeforeCurrYear = '[' + curryearList.join(', ') + ']'

        console.log(">>>>>>>>>>>>>>>> bikin kondisi baru disini >>>>>>>>>>>>>>>>>>>>>>")

        let whereClause = {};
        let whereClauseSegment = {};

        if (group != '') {
            if (group == 'FIT') {
                if (year != '' && comparison == '') {
                    whereClause = {
                        market_code: {
                            [Op.notIn]: ['HSU', 'PF', '']
                        },
                        group_status: {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        },
                        room_type: {
                            [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                        },
                        market_group: {
                            [Op.notIn]: ['GRL', 'GRO', 'GRW', 'MICE']
                        },
                        [Op.and]: [
                            Sequelize.where(Sequelize.col('agent_name'), { [Op.ne]: '' }),
                            Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear)
                        ]
                    };
                } else if (year != '' && comparison != '') {
                    whereClause = {
                        market_code: {
                            [Op.notIn]: ['HSU', 'PF', '']
                        },
                        group_status: {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        },
                        room_type: {
                            [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                        },
                        market_group: {
                            [Op.notIn]: ['GRL', 'GRO', 'GRW', 'MICE']
                        },
                        [Op.and]: [
                            Sequelize.where(Sequelize.col('agent_name'), { [Op.ne]: '' }),
                            Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                        ]
                    };
                } else {
                    whereClause = {
                        market_code: {
                            [Op.notIn]: ['HSU', 'PF', '']
                        },
                        group_status: {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        },
                        room_type: {
                            [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                        },
                        market_group: {
                            [Op.notIn]: ['GRL', 'GRO', 'GRW', 'MICE']
                        },
                        [Op.and]: [
                            Sequelize.where(Sequelize.col('agent_name'), { [Op.ne]: '' }),
                            Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                        ]
                    };
                }
            } else { // GIT
                if (year != '' && comparison == '') {
                    whereClause = {
                        market_code: {
                            [Op.notIn]: ['HSU', 'PF', '']
                        },
                        group_status: {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        },
                        room_type: {
                            [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                        },
                        market_group: {
                            [Op.in]: ['GRL', 'GRO', 'GRW', 'MICE']
                        },
                        [Op.and]: [
                            Sequelize.where(Sequelize.col('agent_name'), { [Op.ne]: '' }),
                            Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear)
                        ]
                    };
                } else if (year != '' && comparison != '') {
                    whereClause = {
                        market_code: {
                            [Op.notIn]: ['HSU', 'PF', '']
                        },
                        group_status: {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        },
                        room_type: {
                            [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                        },
                        market_group: {
                            [Op.in]: ['GRL', 'GRO', 'GRW', 'MICE']
                        },
                        [Op.and]: [
                            Sequelize.where(Sequelize.col('agent_name'), { [Op.ne]: '' }),
                            Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                        ]
                    };
                } else {
                    whereClause = {
                        market_code: {
                            [Op.notIn]: ['HSU', 'PF', '']
                        },
                        group_status: {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        },
                        room_type: {
                            [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                        },
                        market_group: {
                            [Op.in]: ['GRL', 'GRO', 'GRW', 'MICE']
                        },
                        [Op.and]: [
                            Sequelize.where(Sequelize.col('agent_name'), { [Op.ne]: '' }),
                            Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                        ]
                    };
                }
            }
        } else { // JIKA GROUP KOSONG
            if (year != '' && comparison == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', 'PF', '']
                    },
                    group_status: {
                        [Op.notIn]: ['LOS', 'PEN', 'PRO']
                    },
                    room_type: {
                        [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear)
                    ]
                };
            } else if (year != '' && comparison != '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', 'PF', '']
                    },
                    group_status: {
                        [Op.notIn]: ['LOS', 'PEN', 'PRO']
                    },
                    room_type: {
                        [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.ne]: '' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', 'PF', '']
                    },
                    group_status: {
                        [Op.notIn]: ['LOS', 'PEN', 'PRO']
                    },
                    room_type: {
                        [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.ne]: '' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                    ]
                };
            }
        }

        if (hotel != '') {
            whereClause['room_class'] = hotel;
        }

        if (segment != '') {
            whereClauseSegment['segment'] = segment;
        }

        if (description != '') {
            whereClauseSegment['description'] = description;
        }

        if (country != '') {
            whereClause['country'] = country;
        }

        const include_option = [
            {
                model: MarketSegment,
                as: 'marketsegment',
                attributes: [],  // just make relation not show column
                required: false,  //left outer join; true : inner join
                where: whereClauseSegment
            }
        ]

        if (description != '' || segment != '') {
            include_option[0].required = true
        }

        const attributes = [];
        const years = JSON.parse(yearsBeforeCurrYear);

        for (let i = 0; i < years.length; i++) {
            const year = years[i];

            const attribute = [
                Sequelize.fn(
                    'SUM',
                    Sequelize.literal(
                        "CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " +
                        year +
                        ' THEN `rn` ELSE 0 END'
                    )
                ),
                'room_night_' + year,
            ];

            attributes.push(attribute);
        }

        console.log(whereClause, ">>>>>>>>>>>>>>>>>>>> ISI KONDISI >>>>>>>>>>>>>>>>>>>")

        if (year != '' && comparison == '') {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'agent_name',
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'room_night'],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: ['agent_name'],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC'],
                        ['agent_name', 'ASC'],
                    ],
                    limit: 10,
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'agent_name',
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + prevYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + prevYear,
                        ],
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + currYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + currYear,
                        ],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: ["agent_name"],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'agent_name',
                        ...attributes,
                    ],
                    include: include_option,
                    where: whereClause,
                    group: ["agent_name"],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        }
    };

    // ---------------------------------------------------
    // YTD SUMMARY
    // --------------------------------------------------- 
    static async getYtdSummary(req, res) {
        const { hotel, year, comparison, group, segment, description, country } = req.query;

        var currentTime = new Date();
        var currYear = year > 0 ? year : currentTime.getFullYear();
        var prevYear = comparison > 0 ? Number(comparison) : currentTime.getFullYear() - 1;
        var nextYear = currentTime.getFullYear() + 1

        console.log(">>>>>>>>>>>>>>>> bikin kondisi baru disini >>>>>>>>>>>>>>>>>>>>>>")
        let whereClause = {};
        let whereClauseSegment = {}

        if (year != '' && comparison == '') {
            whereClause = {
                market_code: {
                    [Op.notIn]: ['HSU', 'PF', '']
                },
                group_status: {
                    [Op.notIn]: ['LOS', 'PEN', 'PRO', 'CAN']
                },
                room_type: {
                    [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                },
                [Op.and]: [
                    Sequelize.where(Sequelize.col('room_rev_usd'), { [Op.ne]: '' }),
                    Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear),
                ]
            };
        } else if (year != '' && comparison != '') {
            whereClause = {
                market_code: {
                    [Op.notIn]: ['HSU', 'PF', '']
                },
                group_status: {
                    [Op.notIn]: ['LOS', 'PEN', 'PRO', 'CAN']
                },
                room_type: {
                    [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                },
                [Op.and]: [
                    Sequelize.where(Sequelize.col('room_rev_usd'), { [Op.ne]: '' }),
                    Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                ]
            };
        } else {
            whereClause = {
                market_code: {
                    [Op.notIn]: ['HSU', 'PF', '']
                },
                group_status: {
                    [Op.notIn]: ['LOS', 'PEN', 'PRO', 'CAN']
                },
                room_type: {
                    [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                },
                [Op.and]: [
                    Sequelize.where(Sequelize.col('room_rev_usd'), { [Op.ne]: '' }),
                    Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                ]
            };
        }

        if (hotel != '') {
            whereClause['room_class'] = hotel;
        }

        if (group != '') { // filter marketsegment.group sesudah where
            whereClause['group'] = {
                [Op.and]: [
                    Sequelize.where(Sequelize.col('marketsegment.group'), { [Op.eq]: group }),
                ]
            };
        }

        if (segment != '') {
            whereClauseSegment['segment'] = segment;
        }

        if (description != '') {
            whereClauseSegment['description'] = description;
        }

        if (country != '') {
            whereClause['country'] = country;
        }

        const include_option = [
            {
                model: MarketSegment,
                as: 'marketsegment',
                attributes: [],  // just make relation not show column
                required: false,  //left outer join; true : inner join
                where: whereClauseSegment
            },
        ]

        if (description != '' || segment != '') {
            include_option[0].required = true
        }

        try {

            if (year != '' && comparison == '') {

                const result = await Master.findAll({
                    attributes: [
                        [Sequelize.fn('year', Sequelize.fn('str_to_date', Sequelize.col('stay_date'), '%d/%m/%Y')), 'stay_year'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${currYear} THEN REPLACE(rn, ',', '') ELSE 0 END`)), 'XRN_MTD_CURRYEAR'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${currYear} THEN REPLACE(room_rev_usd, ',', '') ELSE 0 END`)), 'XRREV_MTD_CURRYEAR'],
                    ],
                    include: include_option,

                    where: whereClause,
                    group: [
                        Sequelize.fn('year', Sequelize.fn('str_to_date', Sequelize.col('stay_date'), '%d/%m/%Y'))
                    ],
                    order: [
                        [Sequelize.fn('year', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), 'ASC']
                    ],
                });

                // Manipulasi hasil query untuk menghasilkan output yang diinginkan
                const output = result.map(item => {
                    // console.log(XPU23BEFORE23, " <<<<<< SETIAP ITEM")
                    const stay_year = item.dataValues.stay_year;
                    const XRN_MTD_CURRYEAR = Number(item.dataValues.XRN_MTD_CURRYEAR);
                    const XRREV_MTD_CURRYEAR = Number(item.dataValues.XRREV_MTD_CURRYEAR);

                    const XARR_MTD_CURRYEAR = (XRREV_MTD_CURRYEAR / XRN_MTD_CURRYEAR).toFixed(2);

                    return {
                        stay_year,
                        XRN_MTD_CURRYEAR,
                        XRREV_MTD_CURRYEAR,
                        XARR_MTD_CURRYEAR
                    }
                })
                res.status(200).json(output)
            } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
                const result = await Master.findAll({
                    attributes: [
                        [Sequelize.fn('year', Sequelize.fn('str_to_date', Sequelize.col('stay_date'), '%d/%m/%Y')), 'stay_year'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${prevYear} THEN REPLACE(rn, ',', '') ELSE 0 END`)), 'XRN_MTD_PREVYEAR'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${currYear} THEN REPLACE(rn, ',', '') ELSE 0 END`)), 'XRN_MTD_CURRYEAR'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${prevYear} THEN REPLACE(room_rev_usd, ',', '') ELSE 0 END`)), 'XRREV_MTD_PREVYEAR'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${currYear} THEN REPLACE(room_rev_usd, ',', '') ELSE 0 END`)), 'XRREV_MTD_CURRYEAR'],
                    ],
                    include: include_option,

                    where: whereClause,
                    group: Sequelize.literal('YEAR(STR_TO_DATE(stay_date, "%d/%m/%Y"))'),
                    order: Sequelize.literal('YEAR(STR_TO_DATE(stay_date, "%d/%m/%Y")) ASC'),
                });

                // Manipulasi hasil query untuk menghasilkan output yang diinginkan
                const output = result.map(item => {
                    // console.log(XPU23BEFORE23, " <<<<<< SETIAP ITEM")
                    const stay_year = item.dataValues.stay_year;
                    const XRN_MTD_PREVYEAR = Number(item.dataValues.XRN_MTD_PREVYEAR);
                    const XRN_MTD_CURRYEAR = Number(item.dataValues.XRN_MTD_CURRYEAR);
                    const XRREV_MTD_PREVYEAR = Number(item.dataValues.XRREV_MTD_PREVYEAR);
                    const XRREV_MTD_CURRYEAR = Number(item.dataValues.XRREV_MTD_CURRYEAR);

                    const XARR_MTD_PREVYEAR = (XRREV_MTD_PREVYEAR / XRN_MTD_PREVYEAR).toFixed(2);
                    const XARR_MTD_CURRYEAR = (XRREV_MTD_CURRYEAR / XRN_MTD_CURRYEAR).toFixed(2);

                    return {
                        stay_year,
                        XRN_MTD_PREVYEAR,
                        XRREV_MTD_PREVYEAR,
                        XARR_MTD_PREVYEAR,
                        XRN_MTD_CURRYEAR,
                        XRREV_MTD_CURRYEAR,
                        XARR_MTD_CURRYEAR
                    }

                })

                const filteredOutput = output.map(item => {
                    const filteredItem = { ...item };

                    if (filteredItem.XRN_MTD_PREVYEAR === 0 || isNaN(parseFloat(filteredItem.XRN_MTD_PREVYEAR))) {
                        delete filteredItem.XRN_MTD_PREVYEAR;
                    }

                    if (filteredItem.XRREV_MTD_PREVYEAR === 0 || isNaN(parseFloat(filteredItem.XRREV_MTD_PREVYEAR))) {
                        delete filteredItem.XRREV_MTD_PREVYEAR;
                    }

                    if (filteredItem.XARR_MTD_PREVYEAR === 'NaN') {
                        delete filteredItem.XARR_MTD_PREVYEAR;
                    }

                    if (filteredItem.XRN_MTD_CURRYEAR === 0 || isNaN(parseFloat(filteredItem.XRN_MTD_CURRYEAR))) {
                        delete filteredItem.XRN_MTD_CURRYEAR;
                    }

                    if (filteredItem.XRREV_MTD_CURRYEAR === 0 || isNaN(parseFloat(filteredItem.XRREV_MTD_CURRYEAR))) {
                        delete filteredItem.XRREV_MTD_CURRYEAR;
                    }

                    if (filteredItem.XARR_MTD_CURRYEAR === 'NaN') {
                        delete filteredItem.XARR_MTD_CURRYEAR;
                    }

                    return filteredItem;
                });

                 const filteredYears = [prevYear, currYear]; // Daftar tahun yang ingin Anda filter

                if(comparison != '' && year == '') {
                    const filteredData = filteredOutput.filter(item => filteredYears.includes(item.stay_year));
                    res.status(200).json(filteredData)
                } else {
                    const filteredData2 = filteredOutput
                    res.status(200).json(filteredData2)
                }
            } else {
                const result = await Master.findAll({
                    attributes: [
                        [Sequelize.fn('year', Sequelize.fn('str_to_date', Sequelize.col('stay_date'), '%d/%m/%Y')), 'stay_year'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${prevYear} THEN REPLACE(rn, ',', '') ELSE 0 END`)), 'XRN_MTD_PREVYEAR'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${currYear} THEN REPLACE(rn, ',', '') ELSE 0 END`)), 'XRN_MTD_CURRYEAR'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${nextYear} THEN REPLACE(rn, ',', '') ELSE 0 END`)), 'XRN_MTD_NEXTYEAR'],

                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${prevYear} THEN REPLACE(room_rev_usd, ',', '') ELSE 0 END`)), 'XRREV_MTD_PREVYEAR'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${currYear} THEN REPLACE(room_rev_usd, ',', '') ELSE 0 END`)), 'XRREV_MTD_CURRYEAR'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${nextYear} THEN REPLACE(room_rev_usd, ',', '') ELSE 0 END`)), 'XRREV_MTD_NEXTYEAR'],
                    ],
                    include: include_option,

                    where: whereClause,
                    group: Sequelize.literal('YEAR(STR_TO_DATE(stay_date, "%d/%m/%Y"))'),
                    order: Sequelize.literal('YEAR(STR_TO_DATE(stay_date, "%d/%m/%Y")) ASC'),
                });

                // Manipulasi hasil query untuk menghasilkan output yang diinginkan
                const output = result.map(item => {
                    // console.log(XPU23BEFORE23, " <<<<<< SETIAP ITEM")
                    const stay_year = item.dataValues.stay_year;
                    const XRN_MTD_PREVYEAR = Number(item.dataValues.XRN_MTD_PREVYEAR);
                    const XRN_MTD_CURRYEAR = Number(item.dataValues.XRN_MTD_CURRYEAR);
                    const XRN_MTD_NEXTYEAR = Number(item.dataValues.XRN_MTD_NEXTYEAR);


                    const XRREV_MTD_PREVYEAR = Number(item.dataValues.XRREV_MTD_PREVYEAR);
                    const XRREV_MTD_CURRYEAR = Number(item.dataValues.XRREV_MTD_CURRYEAR);
                    const XRREV_MTD_NEXTYEAR = Number(item.dataValues.XRREV_MTD_NEXTYEAR);

                    const XARR_MTD_PREVYEAR = (XRREV_MTD_PREVYEAR / XRN_MTD_PREVYEAR).toFixed(2);
                    const XARR_MTD_CURRYEAR = (XRREV_MTD_CURRYEAR / XRN_MTD_CURRYEAR).toFixed(2);
                    const XARR_MTD_NEXTYEAR = (XRREV_MTD_NEXTYEAR / XRN_MTD_NEXTYEAR).toFixed(2);

                    return {
                        stay_year,
                        XRN_MTD_PREVYEAR,
                        XRREV_MTD_PREVYEAR,
                        XARR_MTD_PREVYEAR,
                        XRN_MTD_CURRYEAR,
                        XRREV_MTD_CURRYEAR,
                        XARR_MTD_CURRYEAR,
                        XRN_MTD_NEXTYEAR,
                        XRREV_MTD_NEXTYEAR,
                        XARR_MTD_NEXTYEAR
                    }

                })

                const filteredOutput = output.map(item => {
                    const filteredItem = { ...item };

                    if (filteredItem.XRN_MTD_PREVYEAR === 0 || isNaN(parseFloat(filteredItem.XRN_MTD_PREVYEAR))) {
                        delete filteredItem.XRN_MTD_PREVYEAR;
                    }

                    if (filteredItem.XRREV_MTD_PREVYEAR === 0 || isNaN(parseFloat(filteredItem.XRREV_MTD_PREVYEAR))) {
                        delete filteredItem.XRREV_MTD_PREVYEAR;
                    }

                    if (filteredItem.XARR_MTD_PREVYEAR === 'NaN') {
                        delete filteredItem.XARR_MTD_PREVYEAR;
                    }


                    if (filteredItem.XRN_MTD_CURRYEAR === 0 || isNaN(parseFloat(filteredItem.XRN_MTD_CURRYEAR))) {
                        delete filteredItem.XRN_MTD_CURRYEAR;
                    }

                    if (filteredItem.XRREV_MTD_CURRYEAR === 0 || isNaN(parseFloat(filteredItem.XRREV_MTD_CURRYEAR))) {
                        delete filteredItem.XRREV_MTD_CURRYEAR;
                    }

                    if (filteredItem.XARR_MTD_CURRYEAR === 'NaN') {
                        delete filteredItem.XARR_MTD_CURRYEAR;
                    }


                    if (filteredItem.XRN_MTD_NEXTYEAR === 0 || isNaN(parseFloat(filteredItem.XRN_MTD_NEXTYEAR))) {
                        delete filteredItem.XRN_MTD_NEXTYEAR;
                    }

                    if (filteredItem.XRREV_MTD_NEXTYEAR === 0 || isNaN(parseFloat(filteredItem.XRREV_MTD_NEXTYEAR))) {
                        delete filteredItem.XRREV_MTD_NEXTYEAR;
                    }

                    if (filteredItem.XARR_MTD_NEXTYEAR === 'NaN') {
                        delete filteredItem.XARR_MTD_NEXTYEAR;
                    }

                    return filteredItem;
                });

                const filteredYears = [2019, 2020, 2021]; // Daftar tahun yang ingin Anda filter

                const filteredData = filteredOutput.filter(item => !filteredYears.includes(item.stay_year));

                res.status(200).json(filteredData)
            }

        } catch (error) {
            console.log(error);
        }
    };

    //! DI ATAS INI SUDAH SEMUA 


    // ---------------------------------------------------
    // YTD FIRECAST
    // ---------------------------------------------------
    static async getForeCast(req, res) {

        const { hotel, year, comparison, group, segment, description, country, card } = req.query;

        var currentTime = new Date();
        var currYear = year > 0 ? year : currentTime.getFullYear();
        var currMonth = currentTime.getMonth();
        var prevYear = comparison > 0 ? Number(comparison) : currentTime.getFullYear() - 1;
        var nextYear = currentTime.getFullYear() + 1

        // list 1 years before current year
        const startCurrYear = currYear - 1;
        const curryearList = [];

        for (let tahun = startCurrYear; tahun <= currYear; tahun++) {
            if (tahun != 2020 && tahun != 2021) {
                curryearList.push(tahun);
            }
        }

        curryearList.push(nextYear)

        const yearsBeforeCurrYear = '[' + curryearList.join(', ') + ']'

        var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        var listMonth = months.slice(0, currMonth + 1).map(month => `'${month}'`).join(', ');

        console.log(">>>>>>>>>>>>>>>> bikin kondisi baru disini >>>>>>>>>>>>>>>>>>>>>>")
        let whereClause = {};
        let whereClauseSegment = {};

        if (year != '' && comparison == '') {
            whereClause = {
                market_code: {
                    [Op.notIn]: ['HSU', 'PF', '']
                },
                group_status: {
                    [Op.notIn]: ['LOS', 'PEN', 'PRO', 'CAN']
                },
                // room_type: {
                //     [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                // },
                [Op.and]: [
                    Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear)
                ]
            };
        } else if (year != '' && comparison != '') {
            whereClause = {
                market_code: {
                    [Op.notIn]: ['HSU', 'PF', '']
                },
                group_status: {
                    [Op.notIn]: ['LOS', 'PEN', 'PRO', 'CAN']
                },
                // room_type: {
                //     [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                // },
                [Op.and]: [
                    Sequelize.where(Sequelize.col('stay_date'), { [Op.ne]: '' }),
                    Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                ]
            };
        } else {
            whereClause = {
                market_code: {
                    [Op.notIn]: ['HSU', 'PF', '']
                },
                group_status: {
                    [Op.notIn]: ['LOS', 'PEN', 'PRO', 'CAN']
                },
                // room_type: {
                //     [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                // },
                [Op.and]: [
                    Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                ]
            };
        }



        if (hotel != '') {
            whereClause['room_class'] = hotel;
        }

        if (group != '') { // filter marketsegment.group sesudah where
            whereClause['group'] = {
                [Op.and]: [
                    Sequelize.where(Sequelize.col('marketsegment.group'), { [Op.eq]: group }),
                ]
            };
        }

        if (segment != '') {
            whereClauseSegment['segment'] = segment;
        }

        if (description != '') {
            whereClauseSegment['description'] = description;
        }

        if (country != '') {
            whereClause['country'] = country;
        }

        if (card != '') {  // filter ratecode.card sesudah where
            whereClause['card'] = {
                [Op.and]: [
                    Sequelize.where(Sequelize.col('ratecode.card'), { [Op.eq]: card }),
                ]
            };
        }


        const include_option = [
            {
                model: MarketSegment,
                as: 'marketsegment',
                attributes: [],  // just make relation not show column
                required: false,  //left outer join; true : inner join
                where: whereClauseSegment
            },
            {
                model: RateCode,
                as: "ratecode",
                attributes: [],
                required: false
            }
        ]

        if (description != '' || segment != '') {
            include_option[0].required = true
        }

        try {

            // Query menggunakan Sequelize
            const result = await Master.findAll({
                attributes: [
                    [Sequelize.fn('month', Sequelize.fn('str_to_date', Sequelize.col('stay_date'), '%d/%m/%Y')), 'bulan'],
                    [Sequelize.fn('sum', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${prevYear} THEN rn ELSE 0 END`)), '_OTBPREV'],
                    [Sequelize.fn('sum', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${currYear} THEN rn ELSE 0 END`)), '_OTBCURR'],
                    [Sequelize.fn('sum', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${currYear} AND YEAR(STR_TO_DATE(insert_date, '%d/%m/%Y')) IN (2019, 2020, 2021, 2022) THEN rn ELSE 0 END`)), 'XPU_CURR_BEFORE_CURR'],
                    [Sequelize.fn('sum', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${prevYear} AND YEAR(STR_TO_DATE(insert_date, '%d/%m/%Y')) IN (2016, 2017, 2018) THEN rn ELSE 0 END`)), 'XPU_PREV_BEFORE_PREV'],
                    [Sequelize.fn('sum', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${currYear} AND YEAR(STR_TO_DATE(insert_date, '%d/%m/%Y')) IN (${currYear}) THEN rn ELSE 0 END`)), 'XPU_CURR_IN_CURR'],
                    [Sequelize.fn('sum', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${prevYear} AND YEAR(STR_TO_DATE(insert_date, '%d/%m/%Y')) IN (${prevYear}) AND MONTHNAME(STR_TO_DATE(insert_date, '%d/%m/%Y')) IN (${listMonth}) THEN rn ELSE 0 END`)), 'XPU_PREV_IN_PREV_M'],
                ],
                include: include_option,

                where: whereClause,
                group: [
                    Sequelize.fn('month', Sequelize.fn('str_to_date', Sequelize.col('stay_date'), '%d/%m/%Y'))
                ],
                order: [
                    [Sequelize.fn('month', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), 'ASC']
                ],
            });

            // Manipulasi hasil query untuk menghasilkan output yang diinginkan
            const output = result.map(item => {
                // console.log(XPU23BEFORE23, " <<<<<< SETIAP ITEM")
                const month = item.dataValues.bulan;
                const _OTBCOMP = Number(item.dataValues._OTBPREV);
                const XFY_COMP = _OTBCOMP;
                const XPU_CURR_BEFORE_CURR = Number(item.dataValues.XPU_CURR_BEFORE_CURR);
                const XPU_COMP_BEFORE_COMP = Number(item.dataValues.XPU_PREV_BEFORE_PREV);
                const XPU_COMP_IN_COMP_M = Number(item.dataValues.XPU_PREV_IN_PREV_M);
                const XPU_CURR_IN_CURR = Number(item.dataValues.XPU_CURR_IN_CURR);
                const XFORECAST = Math.ceil((((XPU_CURR_BEFORE_CURR - XPU_COMP_BEFORE_COMP) + (XPU_CURR_IN_CURR - XPU_COMP_IN_COMP_M) + _OTBCOMP) * 0.95))

                const dynamicKey = `XFY_COMP_${prevYear}`;
                const dynamicKey2 = `XFORECAST_CURR_${currYear}`;

                return {
                    month,
                    [dynamicKey]: XFY_COMP,
                    [dynamicKey2]: XFORECAST,
                }

            })

            const filteredOutput = output.map(item => {
                const filteredItem = { ...item };

                if (filteredItem.XFY_COMP === 0 || isNaN(parseFloat(filteredItem.XFY_COMP))) {
                    delete filteredItem.XFY_COMP;
                }

                if (filteredItem.XFORECAST === 0 || isNaN(parseFloat(filteredItem.XFORECAST))) {
                    delete filteredItem.XFORECAST;
                }

                return filteredItem;
            });

            const modifiedData = filteredOutput.map(item => {
                const modifiedItem = {};

                for (let key in item) {
                    const value = item[key];

                    if (value !== "NaN" && value !== 0 && value !== false) {
                        modifiedItem[key] = value;
                    }
                }

                return modifiedItem;
            });

            const modifiedData1 = modifiedData.map(item => {
                const modifiedItem = {};

                for (let key in item) {
                    const modifiedKey = key.replace(/(COMP_|CURR_)/g, "");
                    modifiedItem[modifiedKey] = item[key];
                }

                return modifiedItem;
            });

            res.status(200).json(modifiedData1)
        } catch (error) {
            console.log(error);
        }
    };

    // ---------------------------------------------------
    // FORECAST MONTHLY ON THE BOOK
    // ---------------------------------------------------
    static async getForecastMonthlyBooking(req, res) {
        const { hotel, year, comparison, group, segment, description, country, card } = req.query;

        var currentTime = new Date();
        var currYear = year > 0 ? year : currentTime.getFullYear();
        var prevYear = comparison > 0 ? Number(comparison) : currentTime.getFullYear() - 1;
        var nextYear = currentTime.getFullYear() + 1

        // list 1 years before current year
        const startCurrYear = currYear - 1;
        const curryearList = [];

        for (let tahun = startCurrYear; tahun <= currYear; tahun++) {
            if (tahun != 2020 && tahun != 2021) {
                curryearList.push(tahun);
            }
        }

        curryearList.push(nextYear)

        const yearsBeforeCurrYear = '[' + curryearList.join(', ') + ']'

        console.log(">>>>>>>>>>>>>>>> bikin kondisi baru disini >>>>>>>>>>>>>>>>>>>>>>")
        let whereClause = {};
        let whereClauseSegment = {}

        if (year != '' && comparison == '') {
            whereClause = {
                market_code: {
                    [Op.notIn]: ['HSU', 'PF', '']
                },
                group_status: {
                    [Op.notIn]: ['LOS', 'PEN', 'PRO', 'CAN']
                },
                // room_type: {
                //     [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                // },
                [Op.and]: [
                    Sequelize.where(Sequelize.col('stay_date'), { [Op.ne]: '' }),
                    Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear)
                ]
            };
        } else if (year != '' && comparison != '') {
            whereClause = {
                market_code: {
                    [Op.notIn]: ['HSU', 'PF', '']
                },
                group_status: {
                    [Op.notIn]: ['LOS', 'PEN', 'PRO', 'CAN']
                },
                // room_type: {
                //     [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                // },
                [Op.and]: [
                    Sequelize.where(Sequelize.col('stay_date'), { [Op.ne]: '' }),
                    Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                ]
            };
        } else {
            whereClause = {
                market_code: {
                    [Op.notIn]: ['HSU', 'PF', '']
                },
                group_status: {
                    [Op.notIn]: ['LOS', 'PEN', 'PRO', 'CAN']
                },
                // room_type: {
                //     [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                // },
                [Op.and]: [
                    Sequelize.where(Sequelize.col('stay_date'), { [Op.ne]: '' }),
                    Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                ]
            };
        }

        if (hotel != '') {
            whereClause['room_class'] = hotel;
        }

        if (group != '') { // filter marketsegment.group sesudah where
            whereClause['group'] = {
                [Op.and]: [
                    Sequelize.where(Sequelize.col('marketsegment.group'), { [Op.eq]: group }),
                ]
            };
        }

        if (segment != '') {
            whereClauseSegment['segment'] = segment;
        }

        if (description != '') {
            whereClauseSegment['description'] = description;
        }

        if (country != '') {
            whereClause['country'] = country;
        }

        if (card != '') {  // filter ratecode.card sesudah where
            whereClause['card'] = {
                [Op.and]: [
                    Sequelize.where(Sequelize.col('ratecode.card'), { [Op.eq]: card }),
                ]
            };
        }

        const include_option = [
            {
                model: MarketSegment,
                as: 'marketsegment',
                attributes: [],  // just make relation not show column
                required: false,  //left outer join; true : inner join
                where: whereClauseSegment
            },
            {
                model: RateCode,
                as: "ratecode",
                attributes: [],
                required: false
            }
        ]

        if (description != '' || segment != '') {
            include_option[0].required = true
        }

        const attributes = [];
        const years = JSON.parse(yearsBeforeCurrYear);

        for (let i = 0; i < years.length; i++) {
            const year = years[i];

            const attribute = [
                Sequelize.fn(
                    'SUM',
                    Sequelize.literal(
                        "CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " +
                        year +
                        ' THEN `rn` ELSE 0 END'
                    )
                ),
                'room_night_' + year,
            ];

            attributes.push(attribute);
        }

        if (year != '' && comparison == '') {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        [Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), 'stay_month'],
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'room_night'],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')),
                    ],
                    order: [
                        [Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), 'ASC'],
                    ],
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        [Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), 'stay_month'],
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + prevYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + prevYear,
                        ],
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + currYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + currYear,
                        ],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')),
                    ],
                    order: [
                        [Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), 'ASC'],
                    ],
                    raw: true,
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        [Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), 'stay_month'],
                        ...attributes,
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')),
                    ],
                    order: [
                        [Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), 'ASC'],
                    ],
                    raw: true,
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        }
    };

    // ---------------------------------------------------
    // FORECAST PICKUP WITHIN SELECTED YAER
    // ---------------------------------------------------
    static async getForecastPickupSelectedYear(req, res) {
        const { hotel, year, comparison, group, segment, description, country, card } = req.query;

        var currentTime = new Date();
        var currYear = year > 0 ? year : currentTime.getFullYear();
        var compYear = comparison > 0 ? Number(comparison) : currentTime.getFullYear() - 1;
        var nextYear = currentTime.getFullYear() + 1;
        var currMonth = currentTime.getMonth()

        // list 1 years before current year
        const startCurrYear = currYear - 1;
        const curryearList = [];

        for (let tahun = startCurrYear; tahun <= currYear; tahun++) {
            if (tahun != 2020 && tahun != 2021) {
                curryearList.push(tahun);
            }
        }

        curryearList.push(nextYear)

        const yearsBeforeCurrYear = '[' + curryearList.join(', ') + ']'

        var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        var listMonth = months.slice(0, currMonth + 1).map(month => `'${month}'`).join(', ');


        console.log(">>>>>>>>>>>>>>>> bikin kondisi baru disini >>>>>>>>>>>>>>>>>>>>>>")
        let whereClause = {};
        let whereClauseSegment = {}

        if (year != '' && comparison == '') {
            whereClause = {
                market_code: {
                    [Op.notIn]: ['HSU', 'PF', '']
                },
                group_status: {
                    [Op.notIn]: ['LOS', 'PEN', 'PRO', 'CAN']
                },
                // room_type: {
                //     [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                // },
                [Op.and]: [
                    Sequelize.where(Sequelize.col('stay_date'), { [Op.ne]: '' }),
                    Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear)
                ]
            };
        } else if (year != '' && comparison != '') {
            whereClause = {
                market_code: {
                    [Op.notIn]: ['HSU', 'PF', '']
                },
                group_status: {
                    [Op.notIn]: ['LOS', 'PEN', 'PRO', 'CAN']
                },
                // room_type: {
                //     [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                // },
                [Op.and]: [
                    Sequelize.where(Sequelize.col('stay_date'), { [Op.ne]: '' }),
                    Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + compYear + "', '" + currYear + "')"),
                ]
            };
        } else {
            console.log('>>>>>>>>>>> IWAN MASUK DISINI >>>>>>>>>')
            console.log(currYear, "CURREBT YEAR>>>>>>>>>>>>>>")
            console.log(compYear, "COMPARISON YEAR>>>>>>>>>>>>>>")
            whereClause = {
                market_code: {
                    [Op.notIn]: ['HSU', 'PF', '']
                },
                group_status: {
                    [Op.notIn]: ['LOS', 'PEN', 'PRO', 'CAN']
                },
                // room_type: {
                //     [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                // },
                [Op.and]: [
                    Sequelize.where(Sequelize.col('stay_date'), { [Op.ne]: '' }),
                    Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                ]
            };
        }

        if (hotel != '') {
            whereClause['room_class'] = hotel;
        }

        if (group != '') { // filter marketsegment.group sesudah where
            whereClause['group'] = {
                [Op.and]: [
                    Sequelize.where(Sequelize.col('marketsegment.group'), { [Op.eq]: group }),
                ]
            };
        }

        if (segment != '') {
            whereClauseSegment['segment'] = segment;
        }

        if (description != '') {
            whereClauseSegment['description'] = description;
        }

        if (country != '') {
            whereClause['country'] = country;
        }

        if (card != '') {  // filter ratecode.card sesudah where
            whereClause['card'] = {
                [Op.and]: [
                    Sequelize.where(Sequelize.col('ratecode.card'), { [Op.eq]: card }),
                ]
            };
        }

        const include_option = [
            {
                model: MarketSegment,
                as: 'marketsegment',
                attributes: [],  // just make relation not show column
                required: false,  //left outer join; true : inner join
                where: whereClauseSegment
            },
            {
                model: RateCode,
                as: "ratecode",
                attributes: [],
                required: false
            }
        ]

        if (description != '' || segment != '') {
            include_option[0].required = true
        }


        try {

            // if (year != '' && comparison == '') {

            // } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {

            // }else {

            // }
            // Query menggunakan Sequelize
            const result = await Master.findAll({
                attributes: [
                    [Sequelize.fn('month', Sequelize.fn('str_to_date', Sequelize.col('stay_date'), '%d/%m/%Y')), 'stay_month'],
                    [Sequelize.fn('sum', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN (` + currYear + `) AND YEAR(STR_TO_DATE(insert_date, '%d/%m/%Y')) IN (` + currYear + `) THEN rn ELSE 0 END`)), 'XPUCURRYEAR'],
                    [Sequelize.fn('sum', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN (` + compYear + `) AND YEAR(STR_TO_DATE(insert_date, '%d/%m/%Y')) IN (` + compYear + `) AND MONTHNAME(STR_TO_DATE(insert_date, '%d/%m/%Y')) IN (` + listMonth + `) THEN rn ELSE 0 END`)), 'XPUPREVYEAR'],
                ],
                include: include_option,

                where: whereClause,
                group: [
                    Sequelize.fn('month', Sequelize.fn('str_to_date', Sequelize.col('stay_date'), '%d/%m/%Y'))
                ],
                order: [
                    [Sequelize.fn('month', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), 'ASC']
                ],
            });

            // Manipulasi hasil query untuk menghasilkan output yang diinginkan
            const output = result.map(item => {
                // console.log(XPU23BEFORE23, " <<<<<< SETIAP ITEM")
                const stay_month = item.dataValues.stay_month;
                const XPU_COMP_YEAR = Number(item.dataValues.XPUPREVYEAR);
                const XPU_CURR_YEAR = Number(item.dataValues.XPUCURRYEAR);

                const dynamicKey = `XPU_COMP_${compYear}`;
                const dynamicKey2 = `XPU_CURR_${currYear}`;

                return {
                    stay_month,
                    [dynamicKey]: XPU_COMP_YEAR,
                    [dynamicKey2]: XPU_CURR_YEAR,
                }
            })

            

            const filteredOutput = output.map(item => {
                const filteredItem = { ...item };

                if (filteredItem.XPU_COMP_YEAR === 0 || isNaN(parseFloat(filteredItem.XPU_COMP_YEAR))) {
                    delete filteredItem.XPU_COMP_YEAR;
                }

                if (filteredItem.XPU_CURR_YEAR === 0 || isNaN(parseFloat(filteredItem.XPU_CURR_YEAR))) {
                    delete filteredItem.XPU_CURR_YEAR;
                }

                return filteredItem;
            });

            console.log(filteredOutput, ">>>>>>>>> display data >>>>>>>>>")
            
            const modifiedData = filteredOutput.map(item => {
                const modifiedItem = {};

                for (let key in item) {
                    const value = item[key];

                    if (value !== "NaN" && value !== 0 && value !== false) {
                        modifiedItem[key] = value;
                    }
                }

                return modifiedItem;
            });

            const modifiedData1 = modifiedData.map(item => {
                const modifiedItem = {};

                for (let key in item) {
                    const modifiedKey = key.replace(/(COMP_|CURR_)/g, "");
                    modifiedItem[modifiedKey] = item[key];
                }

                return modifiedItem;
            });


            res.status(200).json(modifiedData1)
        } catch (error) {
            console.log(error);
        }
    };

    // ---------------------------------------------------
    // FORECAST PICKUP PREVIOUS YEARS SELECTED YAER
    // ---------------------------------------------------
    static async getForecastPickupPreviousSelectedYear(req, res) {
        const { hotel, year, comparison, group, segment, description, country, card } = req.query;

        var currentTime = new Date();
        var currYear = year > 0 ? year : currentTime.getFullYear();
        var compYear = comparison > 0 ? Number(comparison) : currentTime.getFullYear() - 1;

        // list 4 years before current year
        const startCurrYear = currYear - 4;
        const curryearList = [];

        for (let year = startCurrYear; year < currYear; year++) {
            curryearList.push(year);
        }

        const yearsBeforeCurrYear = '(' + curryearList.join(', ') + ')';

        // list 3 years before previous yaer
        const startPrevYear = compYear - 3;
        const prevyearList = [];

        for (let year = startPrevYear; year < compYear; year++) {
            prevyearList.push(year);
        }

        const yearsBeforePrevYear = '(' + prevyearList.join(', ') + ')';

        console.log(">>>>>>>>>>>>>>>> bikin kondisi baru disini >>>>>>>>>>>>>>>>>>>>>>")
        let whereClause = {};
        let whereClauseSegment = {}

        if (year != '' && comparison == '') {
            whereClause = {
                market_code: {
                    [Op.notIn]: ['HSU', 'PF', '']
                },
                group_status: {
                    [Op.notIn]: ['LOS', 'PEN', 'PRO', 'CAN']
                },
                // room_type: {
                //     [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                // },
                [Op.and]: [
                    Sequelize.where(Sequelize.col('stay_date'), { [Op.ne]: '' }),
                    Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear)
                ]
            };

        } else if (year != '' && comparison != '') {
            whereClause = {
                market_code: {
                    [Op.notIn]: ['HSU', 'PF', '']
                },
                group_status: {
                    [Op.notIn]: ['LOS', 'PEN', 'PRO', 'CAN']
                },
                // room_type: {
                //     [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                // },
                [Op.and]: [
                    Sequelize.where(Sequelize.col('stay_date'), { [Op.ne]: '' }),
                    Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + compYear + "', '" + currYear + "')"),
                ]
            };
        } else {
            whereClause = {
                market_code: {
                    [Op.notIn]: ['HSU', 'PF', '']
                },
                group_status: {
                    [Op.notIn]: ['LOS', 'PEN', 'PRO', 'CAN']
                },
                // room_type: {
                //     [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                // },
                [Op.and]: [
                    Sequelize.where(Sequelize.col('stay_date'), { [Op.ne]: '' }),
                    Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                ]
            };
        }

        if (hotel != '') {
            whereClause['room_class'] = hotel;
        }

        if (group != '') { // filter marketsegment.group sesudah where
            whereClause['group'] = {
                [Op.and]: [
                    Sequelize.where(Sequelize.col('marketsegment.group'), { [Op.eq]: group }),
                ]
            };
        }

        if (segment != '') {
            whereClauseSegment['segment'] = segment;
        }

        if (description != '') {
            whereClauseSegment['description'] = description;
        }

        if (country != '') {
            whereClause['country'] = country;
        }

        if (card != '') {  // filter ratecode.card sesudah where
            whereClause['card'] = {
                [Op.and]: [
                    Sequelize.where(Sequelize.col('ratecode.card'), { [Op.eq]: card }),
                ]
            };
        }

        const include_option = [
            {
                model: MarketSegment,
                as: 'marketsegment',
                attributes: [],  // just make relation not show column
                required: false,  //left outer join; true : inner join
                where: whereClauseSegment
            },
            {
                model: RateCode,
                as: "ratecode",
                attributes: [],
                required: false
            }
        ]

        if (description != '' || segment != '') {
            include_option[0].required = true
        }


        try {

            // Query menggunakan Sequelize
            const result = await Master.findAll({
                attributes: [
                    [Sequelize.fn('month', Sequelize.fn('str_to_date', Sequelize.col('stay_date'), '%d/%m/%Y')), 'stay_month'],
                    [Sequelize.fn('sum', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN (` + currYear + `) AND YEAR(STR_TO_DATE(insert_date, '%d/%m/%Y')) IN ` + yearsBeforeCurrYear + ` THEN rn ELSE 0 END`)), 'XPUBEFORECURRYEAR'],
                    [Sequelize.fn('sum', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN (` + compYear + `) AND YEAR(STR_TO_DATE(insert_date, '%d/%m/%Y')) IN ` + yearsBeforePrevYear + ` THEN rn ELSE 0 END`)), 'XPUBEFOREPREVYEAR'],
                ],
                include: include_option,

                where: whereClause,
                group: [
                    Sequelize.fn('month', Sequelize.fn('str_to_date', Sequelize.col('stay_date'), '%d/%m/%Y'))
                ],
                order: [
                    [Sequelize.fn('month', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), 'ASC']
                ],
            });

            // Manipulasi hasil query untuk menghasilkan output yang diinginkan
            const output = result.map(item => {
                // console.log(XPU23BEFORE23, " <<<<<< SETIAP ITEM")
                const stay_month = item.dataValues.stay_month;
                const XPU_BEFORE_COMP_YEAR = Number(item.dataValues.XPUBEFOREPREVYEAR);
                const XPU_BEFORE_CURR_YEAR = Number(item.dataValues.XPUBEFORECURRYEAR);

                const dynamicKey = `XPU_BEFORE_COMP_${compYear}`;
                const dynamicKey2 = `XPU_BEFORE_CURR_${currYear}`;

                return {
                    stay_month,
                    [dynamicKey]: XPU_BEFORE_COMP_YEAR,
                    [dynamicKey2]: XPU_BEFORE_CURR_YEAR,
                }

            })

            const filteredOutput = output.map(item => {
                const filteredItem = { ...item };

                if (filteredItem.XPU_BEFORE_COMP_YEAR === 0 || isNaN(parseFloat(filteredItem.XPU_BEFORE_COMP_YEAR))) {
                    delete filteredItem.XPU_BEFORE_COMP_YEAR;
                }

                if (filteredItem.XPU_BEFORE_CURR_YEAR === 0 || isNaN(parseFloat(filteredItem.XPU_BEFORE_CURR_YEAR))) {
                    delete filteredItem.XPU_BEFORE_CURR_YEAR;
                }

                return filteredItem;
            });

            const modifiedData = filteredOutput.map(item => {
                const modifiedItem = {};

                for (let key in item) {
                    const value = item[key];

                    if (value !== "NaN" && value !== 0 && value !== false) {
                        modifiedItem[key] = value;
                    }
                }

                return modifiedItem;
            });

            const modifiedData1 = modifiedData.map(item => {
                const modifiedItem = {};

                for (let key in item) {
                    const modifiedKey = key.replace(/(COMP_|CURR_)/g, "");
                    modifiedItem[modifiedKey] = item[key];
                }

                return modifiedItem;
            });

            res.status(200).json(modifiedData1)
        } catch (error) {
            console.log(error);
        }
    };

    // ---------------------------------------------------
    // YTD SUMMARY
    // --------------------------------------------------- 
    static async getForecastSummary(req, res) {
        const { hotel, year, comparison, group, segment, description, country, card } = req.query;


        var currentTime = new Date();
        var currYear = year > 0 ? Number(year) : currentTime.getFullYear();
        var prevYear = comparison > 0 ? Number(comparison) : currentTime.getFullYear() - 1;
        var nextYear = currentTime.getFullYear() + 1

        console.log(">>>>>>>>>>>>>>>> bikin kondisi baru disini >>>>>>>>>>>>>>>>>>>>>>")
        let whereClause = {};
        let whereClauseSegment = {}

        if (year != '' && comparison == '') {
            whereClause = {
                market_code: {
                    [Op.notIn]: ['HSU', 'PF', '']
                },
                group_status: {
                    [Op.notIn]: ['LOS', 'PEN', 'PRO', 'CAN']
                },
                // room_type: {
                //     [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                // },
                [Op.and]: [
                    Sequelize.where(Sequelize.col('room_rev_usd'), { [Op.ne]: '' }),
                    Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear),
                ]
            };
        } else if (year == '' && comparison != '' || year != '' && comparison != '') {
            whereClause = {
                market_code: {
                    [Op.notIn]: ['HSU', 'PF', '']
                },
                group_status: {
                    [Op.notIn]: ['LOS', 'PEN', 'PRO', 'CAN']
                },
                // room_type: {
                //     [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                // },
                [Op.and]: [
                    Sequelize.where(Sequelize.col('room_rev_usd'), { [Op.ne]: '' }),
                    Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                ]
            };
        } else {
            whereClause = {
                market_code: {
                    [Op.notIn]: ['HSU', 'PF', '']
                },
                group_status: {
                    [Op.notIn]: ['LOS', 'PEN', 'PRO', 'CAN']
                },
                // room_type: {
                //     [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                // },
                [Op.and]: [
                    Sequelize.where(Sequelize.col('room_rev_usd'), { [Op.ne]: '' }),
                    Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                ]
            };
        }

        if (hotel != '') {
            whereClause['room_class'] = hotel;
        }

        if (group != '') { // filter marketsegment.group sesudah where
            whereClause['group'] = {
                [Op.and]: [
                    Sequelize.where(Sequelize.col('marketsegment.group'), { [Op.eq]: group }),
                ]
            };
        }

        if (segment != '') {
            whereClauseSegment['segment'] = segment;
        }

        if (description != '') {
            whereClauseSegment['description'] = description;
        }

        if (country != '') {
            whereClause['country'] = country;
        }

        if (card != '') {  // filter ratecode.card sesudah where
            whereClause['card'] = {
                [Op.and]: [
                    Sequelize.where(Sequelize.col('ratecode.card'), { [Op.eq]: card }),
                ]
            };
        }

        const include_option = [
            {
                model: MarketSegment,
                as: 'marketsegment',
                attributes: [],  // just make relation not show column
                required: false,  //left outer join; true : inner join
                where: whereClauseSegment
            },
            {
                model: RateCode,
                as: "ratecode",
                attributes: [],
                required: false
            }
        ]

        if (description != '' || segment != '') {
            include_option[0].required = true
        }

        try {

            if (year != '' && comparison == '') {
                const result = await Master.findAll({
                    attributes: [
                        [Sequelize.fn('year', Sequelize.fn('str_to_date', Sequelize.col('stay_date'), '%d/%m/%Y')), 'stay_year'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${currYear} THEN REPLACE(rn, ',', '') ELSE 0 END`)), 'XRN_MTD_CURRYEAR'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${currYear} THEN REPLACE(room_rev_usd, ',', '') ELSE 0 END`)), 'XRREV_MTD_CURRYEAR'],
                    ],
                    include: include_option,

                    where: whereClause,
                    group: [
                        Sequelize.fn('year', Sequelize.fn('str_to_date', Sequelize.col('stay_date'), '%d/%m/%Y'))
                    ],
                    order: [
                        [Sequelize.fn('year', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), 'ASC']
                    ],
                });

                // Manipulasi hasil query untuk menghasilkan output yang diinginkan
                const output = result.map(item => {
                    // console.log(XPU23BEFORE23, " <<<<<< SETIAP ITEM")
                    const stay_year = item.dataValues.stay_year;
                    const XRN_MTD_CURR_YEAR = Number(item.dataValues.XRN_MTD_CURRYEAR);
                    const XRREV_MTD_CURR_YEAR = Number(item.dataValues.XRREV_MTD_CURRYEAR);

                    const XARR_MTD_CURR_YEAR = (XRREV_MTD_CURR_YEAR / XRN_MTD_CURR_YEAR).toFixed(2);

                    const dynamicKey = `XRN_MTD_CURR_${stay_year}`;
                    const dynamicKey2 = `XRREV_MTD_CURR_${stay_year}`;
                    const dynamicKey3 = `XARR_MTD_CURR_${stay_year}`;


                    return {
                        stay_year,
                        [dynamicKey]: XRN_MTD_CURR_YEAR,
                        [dynamicKey2]: XRREV_MTD_CURR_YEAR,
                        [dynamicKey3]: XARR_MTD_CURR_YEAR
                    }
                })

                const modifiedData = output.map(item => {
                    const modifiedItem = {};

                    for (let key in item) {
                        const modifiedKey = key.replace(/(CURR_)/g, "");
                        modifiedItem[modifiedKey] = item[key];
                    }

                    return modifiedItem;
                });

                res.status(200).json(modifiedData)
            } else if (year == '' && comparison != '' || year != '' && comparison != '') {
                const result = await Master.findAll({
                    attributes: [
                        [Sequelize.fn('year', Sequelize.fn('str_to_date', Sequelize.col('stay_date'), '%d/%m/%Y')), 'stay_year'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${prevYear} THEN REPLACE(rn, ',', '') ELSE 0 END`)), 'XRN_MTD_PREVYEAR'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${currYear} THEN REPLACE(rn, ',', '') ELSE 0 END`)), 'XRN_MTD_CURRYEAR'],


                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${prevYear} THEN REPLACE(room_rev_usd, ',', '') ELSE 0 END`)), 'XRREV_MTD_PREVYEAR'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${currYear} THEN REPLACE(room_rev_usd, ',', '') ELSE 0 END`)), 'XRREV_MTD_CURRYEAR'],
                    ],
                    include: include_option,

                    where: whereClause,
                    group: Sequelize.literal('YEAR(STR_TO_DATE(stay_date, "%d/%m/%Y"))'),
                    order: Sequelize.literal('YEAR(STR_TO_DATE(stay_date, "%d/%m/%Y")) ASC'),
                });

                // Manipulasi hasil query untuk menghasilkan output yang diinginkan
                const output = result.map(item => {
                    // console.log(XPU23BEFORE23, " <<<<<< SETIAP ITEM")
                    const stay_year = item.dataValues.stay_year;
                    const XRN_MTD_COMP_YEAR = Number(item.dataValues.XRN_MTD_PREVYEAR);
                    const XRN_MTD_CURR_YEAR = Number(item.dataValues.XRN_MTD_CURRYEAR);


                    const XRREV_MTD_COMP_YEAR = Number(item.dataValues.XRREV_MTD_PREVYEAR);
                    const XRREV_MTD_CURR_YEAR = Number(item.dataValues.XRREV_MTD_CURRYEAR);

                    const XARR_MTD_COMP_YEAR = XRREV_MTD_COMP_YEAR > 0 ? (XRREV_MTD_COMP_YEAR / XRN_MTD_COMP_YEAR).toFixed(2) : 0;
                    const XARR_MTD_CURR_YEAR = XRREV_MTD_CURR_YEAR > 0 ? (XRREV_MTD_CURR_YEAR / XRN_MTD_CURR_YEAR).toFixed(2) : 0;

                    const dynamicKey = `XRN_MTD_COMP_${stay_year}`;
                    const dynamicKey2 = `XRREV_MTD_COMP_${stay_year}`;
                    const dynamicKey3 = `XARR_MTD_COMP_${stay_year}`;
                    const dynamicKey4 = `XRN_MTD_CURR_${stay_year}`;
                    const dynamicKey5 = `XRREV_MTD_CURR_${stay_year}`;
                    const dynamicKey6 = `XARR_MTD_CURR_${stay_year}`;

                    return {
                        stay_year,
                        [dynamicKey]: XRN_MTD_COMP_YEAR,
                        [dynamicKey2]: XRREV_MTD_COMP_YEAR,
                        [dynamicKey3]: XARR_MTD_COMP_YEAR,
                        [dynamicKey4]: XRN_MTD_CURR_YEAR,
                        [dynamicKey5]: XRREV_MTD_CURR_YEAR,
                        [dynamicKey6]: XARR_MTD_CURR_YEAR
                    }

                })



                const filteredOutput = output.map(item => {
                    const filteredItem = { ...item };

                    if (filteredItem.XRN_MTD_COMP_YEAR === 0 || isNaN(parseFloat(filteredItem.XRN_MTD_COMP_YEAR))) {
                        delete filteredItem.XRN_MTD_COMP_YEAR;
                    }

                    if (filteredItem.XRREV_MTD_COMP_YEAR === 0 || isNaN(parseFloat(filteredItem.XRREV_MTD_COMP_YEAR))) {
                        delete filteredItem.XRREV_MTD_COMP_YEAR;
                    }

                    if (filteredItem.XARR_MTD_COMP_YEAR === 'NaN') {
                        delete filteredItem.XARR_MTD_COMP_YEAR;
                    }

                    if (filteredItem.XRN_MTD_CURR_YEAR === 0 || isNaN(parseFloat(filteredItem.XRN_MTD_CURR_YEAR))) {
                        delete filteredItem.XRN_MTD_CURR_YEAR;
                    }

                    if (filteredItem.XRREV_MTD_CURR_YEAR === 0 || isNaN(parseFloat(filteredItem.XRREV_MTD_CURR_YEAR))) {
                        delete filteredItem.XRREV_MTD_CURR_YEAR;
                    }

                    if (filteredItem.XARR_MTD_CURR_YEAR === 'NaN') {
                        delete filteredItem.XARR_MTD_CURR_YEAR;
                    }

                    return filteredItem;
                });

                const filteredYears = [currYear, prevYear]; // Daftar tahun yang ingin Anda filter
                const filteredData = filteredOutput.filter(item => filteredYears.includes(item.stay_year));

                const modifiedData = filteredData.map(item => {
                    const modifiedItem = {};

                    for (let key in item) {
                        const value = item[key];

                        if (value !== "NaN" && value !== 0 && value !== false) {
                            modifiedItem[key] = value;
                        }
                    }

                    return modifiedItem;
                });

                const modifiedData1 = modifiedData.map(item => {
                    const modifiedItem = {};

                    for (let key in item) {
                        const modifiedKey = key.replace(/(COMP_|CURR_)/g, "");
                        modifiedItem[modifiedKey] = item[key];
                    }

                    return modifiedItem;
                });

                res.status(200).json(modifiedData1)

            } else {
                const result = await Master.findAll({
                    attributes: [
                        [Sequelize.fn('year', Sequelize.fn('str_to_date', Sequelize.col('stay_date'), '%d/%m/%Y')), 'stay_year'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${prevYear} THEN REPLACE(rn, ',', '') ELSE 0 END`)), 'XRN_MTD_PREVYEAR'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${currYear} THEN REPLACE(rn, ',', '') ELSE 0 END`)), 'XRN_MTD_CURRYEAR'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${nextYear} THEN REPLACE(rn, ',', '') ELSE 0 END`)), 'XRN_MTD_NEXTYEAR'],

                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${prevYear} THEN REPLACE(room_rev_usd, ',', '') ELSE 0 END`)), 'XRREV_MTD_PREVYEAR'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${currYear} THEN REPLACE(room_rev_usd, ',', '') ELSE 0 END`)), 'XRREV_MTD_CURRYEAR'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${nextYear} THEN REPLACE(room_rev_usd, ',', '') ELSE 0 END`)), 'XRREV_MTD_NEXTYEAR'],
                    ],
                    include: include_option,

                    where: whereClause,
                    group: Sequelize.literal('YEAR(STR_TO_DATE(stay_date, "%d/%m/%Y"))'),
                    order: Sequelize.literal('YEAR(STR_TO_DATE(stay_date, "%d/%m/%Y")) ASC'),
                });


                // Manipulasi hasil query untuk menghasilkan output yang diinginkan
                const output = result.map(item => {
                    // console.log(XPU23BEFORE23, " <<<<<< SETIAP ITEM")
                    const stay_year = item.dataValues.stay_year;
                    const XRN_MTD_PREV_YEAR = item.dataValues.XRN_MTD_PREVYEAR;
                    const XRN_MTD_CURR_YEAR = item.dataValues.XRN_MTD_CURRYEAR;
                    const XRN_MTD_NEXT_YEAR = item.dataValues.XRN_MTD_NEXTYEAR;

                    const XRREV_MTD_PREV_YEAR = item.dataValues.XRREV_MTD_PREVYEAR;
                    const XRREV_MTD_CURR_YEAR = item.dataValues.XRREV_MTD_CURRYEAR;
                    const XRREV_MTD_NEXT_YEAR = item.dataValues.XRREV_MTD_NEXTYEAR;

                    const XARR_MTD_PREV_YEAR = XRREV_MTD_PREV_YEAR > 0 ? (XRREV_MTD_PREV_YEAR / XRN_MTD_PREV_YEAR).toFixed(2) : 0;
                    const XARR_MTD_CURR_YEAR = XRREV_MTD_CURR_YEAR > 0 ? (XRREV_MTD_CURR_YEAR / XRN_MTD_CURR_YEAR).toFixed(2) : 0;
                    const XARR_MTD_NEXT_YEAR = XRREV_MTD_NEXT_YEAR > 0 ? (XRREV_MTD_NEXT_YEAR / XRN_MTD_NEXT_YEAR).toFixed(2) : 0;

                    const dynamicKey = `XRN_MTD_PREV_${stay_year}`;
                    const dynamicKey2 = `XRREV_MTD_PREV_${stay_year}`;
                    const dynamicKey3 = `XARR_MTD_PREV_${stay_year}`;
                    const dynamicKey4 = `XRN_MTD_CURR_${stay_year}`;
                    const dynamicKey5 = `XRREV_MTD_CURR_${stay_year}`;
                    const dynamicKey6 = `XARR_MTD_CURR_${stay_year}`;
                    const dynamicKey7 = `XRN_MTD_NEXT_${stay_year}`;
                    const dynamicKey8 = `XRREV_MTD_NEXT_${stay_year}`;
                    const dynamicKey9 = `XARR_MTD_NEXT_${stay_year}`;

                    return {
                        stay_year,
                        [dynamicKey]: XRN_MTD_PREV_YEAR,
                        [dynamicKey2]: XRREV_MTD_PREV_YEAR,
                        [dynamicKey3]: XARR_MTD_PREV_YEAR,
                        [dynamicKey4]: XRN_MTD_CURR_YEAR,
                        [dynamicKey5]: XRREV_MTD_CURR_YEAR,
                        [dynamicKey6]: XARR_MTD_CURR_YEAR,
                        [dynamicKey7]: XRN_MTD_NEXT_YEAR,
                        [dynamicKey8]: XRREV_MTD_NEXT_YEAR,
                        [dynamicKey9]: XARR_MTD_NEXT_YEAR
                    }

                })



                const filteredOutput = output.map(item => {
                    const filteredItem = { ...item };

                    if (filteredItem.XRN_MTD_PREV_YEAR === 0 || isNaN(parseFloat(filteredItem.XRN_MTD_PREV_YEAR))) {
                        delete filteredItem.XRN_MTD_PREV_YEAR;
                    }

                    if (filteredItem.XRREV_MTD_PREV_YEAR === 0 || isNaN(parseFloat(filteredItem.XRREV_MTD_PREV_YEAR))) {
                        delete filteredItem.XRREV_MTD_PREV_YEAR;
                    }

                    if (filteredItem.XARR_MTD_PREV_YEAR === 'NaN') {
                        delete filteredItem.XARR_MTD_PREV_YEAR;
                    }

                    if (filteredItem.XRN_MTD_CURR_YEAR === 0 || isNaN(parseFloat(filteredItem.XRN_MTD_CURR_YEAR))) {
                        delete filteredItem.XRN_MTD_CURR_YEAR;
                    }

                    if (filteredItem.XRREV_MTD_CURR_YEAR === 0 || isNaN(parseFloat(filteredItem.XRREV_MTD_CURR_YEAR))) {
                        delete filteredItem.XRREV_MTD_CURR_YEAR;
                    }

                    if (filteredItem.XARR_MTD_CURR_YEAR === 'NaN') {
                        delete filteredItem.XARR_MTD_CURR_YEAR;
                    }

                    if (filteredItem.XRN_MTD_NEXT_YEAR === 0 || isNaN(parseFloat(filteredItem.XRN_MTD_NEXT_YEAR))) {
                        delete filteredItem.XRN_MTD_NEXT_YEAR;
                    }

                    if (filteredItem.XRREV_MTD_NEXT_YEAR === 0 || isNaN(parseFloat(filteredItem.XRREV_MTD_NEXT_YEAR))) {
                        delete filteredItem.XRREV_MTD_NEXT_YEAR;
                    }


                    if (filteredItem.XARR_MTD_NEXT_YEAR === 'NaN') {
                        delete filteredItem.XARR_MTD_NEXT_YEAR;
                    }

                    return filteredItem;
                });

                const filteredYears = [2019, 2020, 2021]; // Daftar tahun yang ingin Anda filter

                const filteredData = filteredOutput.filter(item => !filteredYears.includes(item.stay_year));



                const modifiedData = filteredData.map(item => {
                    const modifiedItem = {};

                    for (let key in item) {
                        const value = item[key];

                        if (value !== "NaN" && value !== 0 && value !== false) {
                            modifiedItem[key] = value;
                        }
                    }

                    return modifiedItem;
                });


                const modifiedData1 = modifiedData.map(item => {
                    const modifiedItem = {};

                    for (let key in item) {
                        const modifiedKey = key.replace(/(PREV_|CURR_|NEXT_)/g, "");
                        modifiedItem[modifiedKey] = item[key];
                    }
                    console.log(modifiedItem, "< di dalam function")
                    return modifiedItem;
                });


                res.status(200).json(modifiedData1)
            }

        } catch (error) {
            console.log(error);
        }
    };

    // ---------------------------------------------------
    // GROUPS PROSPECT
    // --------------------------------------------------- 
    static async getProspect(req, res) {
        const { hotel, month } = req.query;

        var currentDate = new Date();
        var currYear = currentDate.getFullYear();


        console.log(">>>>>>>>>>>>>>>> bikin kondisi baru disini >>>>>>>>>>>>>>>>>>>>>>")
        let whereClause = {};

        if (month == '') {
            whereClause = {
                group_status: {
                    [Op.eq]: 'PRO'
                },
                [Op.and]: [
                    Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear),
                ]
            };
        } else {
            whereClause = {
                group_status: {
                    [Op.eq]: 'PRO'
                },
                [Op.and]: [
                    Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear),
                    Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month),
                ]
            };
        }



        if (hotel != '') {
            whereClause['room_class'] = hotel;
        }

        whereClause['code'] = {
            [Op.or]: [
                Sequelize.where(Sequelize.col('marketsegment.code'), { [Op.notIn]: ['HSU', ''] }),
            ]
        };

        const include_option = [
            {
                model: MarketSegment,
                as: 'marketsegment',
                attributes: [],  // just make relation not show column
                required: true,  // inner join
            },
        ]

        try {
            const bookings = await Master.findAll({
                attributes: [
                    [Sequelize.literal("RIGHT(STR_TO_DATE(stay_date, '%d/%m/%Y'), 2)"), "day"],
                    [Sequelize.fn('SUM', Sequelize.col('rn')), 'room_night'],
                ],
                include: include_option,
                where: whereClause,
                group: [
                    Sequelize.literal("RIGHT(STR_TO_DATE(stay_date, '%d/%m/%Y'), 2)"),
                ],
                order: [
                    [Sequelize.literal("RIGHT(STR_TO_DATE(stay_date, '%d/%m/%Y'), 2)"), 'ASC'],
                ],
                raw: true
            });
            res.status(200).json(bookings);
        } catch (error) {
            console.log(error);
        }

        //console.log(whereClause, ">>>>>> ISI KONDISI >>>>>>>>>>>>>>>>>>>")

    };

    // ---------------------------------------------------
    // GROUPS DAILY ACT/DEF/TEN
    // --------------------------------------------------- 
    static async getDailyActDefTen(req, res) {
        const { hotel, month } = req.query;

        var currentDate = new Date();
        var currYear = currentDate.getFullYear();

        console.log(">>>>>>>>>>>>>>>> bikin kondisi baru disini >>>>>>>>>>>>>>>>>>>>>>")
        let whereClause = {};

        if (month == '') {
            whereClause = {
                group_status: {
                    [Op.notIn]: ['LOS', 'PRO', 'PEN']
                },
                [Op.and]: [
                    Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear),
                ]
            };
        } else {
            whereClause = {
                group_status: {
                    [Op.notIn]: ['LOS', 'PRO', 'PEN']
                },
                [Op.and]: [
                    Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear),
                    Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month),
                ]
            };
        }



        if (hotel != '') {
            whereClause['room_class'] = hotel;
        }

        whereClause['code'] = {
            [Op.or]: [
                Sequelize.where(Sequelize.col('marketsegment.code'), { [Op.notIn]: ['HSU', ''] }),
            ]
        };

        const include_option = [
            {
                model: MarketSegment,
                as: 'marketsegment',
                attributes: [],  // just make relation not show column
                required: true,  // inner join
            },
        ]

        try {
            const bookings = await Master.findAll({
                attributes: [
                    [Sequelize.literal("RIGHT(STR_TO_DATE(stay_date, '%d/%m/%Y'), 2)"), "day"],
                    [Sequelize.fn('SUM', Sequelize.col('rn')), 'room_night'],
                ],
                include: include_option,
                where: whereClause,
                group: [
                    Sequelize.literal("RIGHT(STR_TO_DATE(stay_date, '%d/%m/%Y'), 2)"),
                ],
                order: [
                    [Sequelize.literal("RIGHT(STR_TO_DATE(stay_date, '%d/%m/%Y'), 2)"), 'ASC'],
                ],
                raw: true
            });
            res.status(200).json(bookings);
        } catch (error) {
            console.log(error);
        }
    };


    // ---------------------------------------------------
    // GROUPS DAILY ACT/DEF/TEN
    // --------------------------------------------------- 
    static async getDailyActDefTenGroup(req, res) {
        const { hotel, month } = req.query;

        var currentDate = new Date();
        var currYear = currentDate.getFullYear();

        console.log(">>>>>>>>>>>>>>>> bikin kondisi baru disini >>>>>>>>>>>>>>>>>>>>>>")
        let whereClause = {};

        if (month == '') {
            whereClause = {
                group_status: {
                    [Op.notIn]: ['LOS', 'PRO', 'PEN']
                },
                [Op.and]: [
                    Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear),
                ]
            };
        } else {
            whereClause = {
                group_status: {
                    [Op.notIn]: ['LOS', 'PRO', 'PEN']
                },
                [Op.and]: [
                    Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear),
                    Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month),
                ]
            };
        }

        if (hotel != '') {
            whereClause['room_class'] = hotel;
        }

        whereClause['code'] = {
            [Op.or]: [
                Sequelize.where(Sequelize.col('marketsegment.code'), { [Op.notIn]: ['HSU', ''] }),
            ]
        };


        whereClause['rate_description'] = {
            [Op.and]: [
                Sequelize.where(Sequelize.col('ratecode.rate_description'), { [Op.not]: '' }),
                Sequelize.where(Sequelize.col('ratecode.rate_description2'), { [Op.eq]: 'GroupRate' }),
            ]
        };

        const include_option = [
            {
                model: MarketSegment,
                as: 'marketsegment',
                attributes: [],  // just make relation not show column
                required: true,  // inner join
            },
            {
                model: RateCode,
                as: "ratecode",
                attributes: [],
                required: true
            }
        ]

        try {
            const bookings = await Master.findAll({
                attributes: [
                    // [Sequelize.literal("ratecode.rate_description"), "description"],
                    'ratecode.rate_description',
                    [Sequelize.fn('SUM', Sequelize.col('rn')), 'room_night'],
                ],
                include: include_option,
                where: whereClause,
                group: ["ratecode.rate_description"],
                order: [
                    [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                ],
                limit: 10,
                raw: true
            });
            res.status(200).json(bookings);
        } catch (error) {
            console.log(error);
        }
    };

    // ---------------------------------------------------
    // GROUPS MONTHLY PROSPECT
    // --------------------------------------------------- 
    static async getMonthlyProspect(req, res) {
        const { hotel, month } = req.query;

        var currentDate = new Date();
        var currYear = currentDate.getFullYear();

        console.log(">>>>>>>>>>>>>>>> bikin kondisi baru disini >>>>>>>>>>>>>>>>>>>>>>")
        let whereClause = {};

        if (month == '') {
            whereClause = {
                market_code: {
                    [Op.notIn]: ['HSU', '']
                },
                group_status: {
                    [Op.in]: ['PRO', 'PEN']
                },
                [Op.and]: [
                    Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear),
                ]
            };
        } else {
            whereClause = {
                market_code: {
                    [Op.notIn]: ['HSU', '']
                },
                group_status: {
                    [Op.in]: ['PRO', 'PEN']
                },
                [Op.and]: [
                    Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear),
                    Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month),
                ]
            };
        }



        if (hotel != '') {
            whereClause['room_class'] = hotel;
        }

        whereClause['group'] = {
            [Op.or]: [
                Sequelize.where(Sequelize.col('marketsegment.group'), { [Op.eq]: 'Group' }),
            ]
        };

        const include_option = [
            {
                model: MarketSegment,
                as: 'marketsegment',
                attributes: [],  // just make relation not show column
                required: true,  // inner join
            },
        ]

        try {
            const bookings = await Master.findAll({
                attributes: [
                    [Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), 'stay_month'],
                    [Sequelize.fn('SUM', Sequelize.col('rn')), 'room_night'],
                ],
                include: include_option,
                where: whereClause,
                group: [
                    Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')),
                ],
                order: [
                    [Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), 'ASC'],
                ],
                raw: true
            });
            res.status(200).json(bookings);
        } catch (error) {
            console.log(error);
        }

    };

    // ---------------------------------------------------
    // GROUPS MONTHLY PROSPECT
    // --------------------------------------------------- 
    static async getMonthlyActDefTen(req, res) {
        const { hotel, month, comparison } = req.query;

        var currentTime = new Date();
        var currYear = currentTime.getFullYear();
        var prevYear = comparison > 0 ? Number(comparison) : currentTime.getFullYear() - 1;

        console.log(">>>>>>>>>>>>>>>> bikin kondisi baru disini >>>>>>>>>>>>>>>>>>>>>>")
        let whereClause = {};

        if (month == '') {
            whereClause = {
                market_code: {
                    [Op.notIn]: ['HSU', '']
                },
                group_status: {
                    [Op.notIn]: ['LOS', 'PRO', 'PEN']
                },
                [Op.and]: [
                    Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                ]
            };
        } else {
            whereClause = {
                market_code: {
                    [Op.notIn]: ['HSU', '']
                },
                group_status: {
                    [Op.notIn]: ['LOS', 'PRO', 'PEN']
                },
                [Op.and]: [
                    Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                    Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month),
                ]
            };
        }

        if (hotel != '') {
            whereClause['room_class'] = hotel;
        }

        whereClause['group'] = {
            [Op.or]: [
                Sequelize.where(Sequelize.col('marketsegment.group'), { [Op.eq]: 'Group' }),
            ]
        };

        const include_option = [
            {
                model: MarketSegment,
                as: 'marketsegment',
                attributes: [],  // just make relation not show column
                required: true,  // inner join
            },
        ]

        try {
            const bookings = await Master.findAll({
                attributes: [
                    [Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), 'stay_month'],
                    [
                        Sequelize.fn(
                            "SUM",
                            Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + prevYear + " THEN `rn` ELSE 0 END")
                        ),
                        "room_night_" + prevYear,
                    ],
                    [
                        Sequelize.fn(
                            "SUM",
                            Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + currYear + " THEN `rn` ELSE 0 END")
                        ),
                        "room_night_" + currYear,
                    ],
                ],
                include: include_option,
                where: whereClause,
                group: [
                    Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')),
                ],
                order: [
                    [Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), 'ASC'],
                ],
                raw: true
            });
            res.status(200).json(bookings);
        } catch (error) {
            console.log(error);
        }
    };

    // ---------------------------------------------------
    // WHL MTD DAILY ON THE BOOK
    // ---------------------------------------------------
    static async getWhlMtdDailyBooking(req, res) {
        const { hotel, year, comparison, month, description, agent, roomtype } = req.query;

        var currentTime = new Date();
        var currYear = year > 0 ? year : currentTime.getFullYear();
        var prevYear = comparison > 0 ? Number(comparison) : currentTime.getFullYear() - 1;
        var nextYear = currentTime.getFullYear() + 1

        // list 1 years before current year
        const startCurrYear = currYear - 1;
        const curryearList = [];

        for (let tahun = startCurrYear; tahun <= currYear; tahun++) {
            if (tahun != 2020 && tahun != 2021) {
                curryearList.push(tahun);
            }
        }

        curryearList.push(nextYear)

        const yearsBeforeCurrYear = '[' + curryearList.join(', ') + ']'

        console.log(yearsBeforeCurrYear, ">>>>>>>>>>>>>>>> ISI DAFTAR TAHUN >>>>>>>>>>>>>>>>>>>>>>")

        let whereClause = {};
        let whereClauseSegment = {}

        if (year != '' && comparison == '') {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    room_class: {
                        [Op.notIn]: ['PSE','']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear)
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    room_class: {
                        [Op.notIn]: ['PSE','']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    room_class: {
                        [Op.notIn]: ['PSE','']
                    },
                    [Op.and]: [
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    room_class: {
                        [Op.notIn]: ['PSE','']
                    },
                    [Op.and]: [
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    room_class: {
                        [Op.notIn]: ['PSE','']
                    },
                    [Op.and]: [
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    room_class: {
                        [Op.notIn]: ['PSE','']
                    },
                    [Op.and]: [
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        }


        if (hotel != '') {
            whereClause['room_class'] = hotel;
        }

        whereClause['segment'] = {
            [Op.and]: [
                Sequelize.where(Sequelize.col('marketsegment.segment'), { [Op.eq]: 'WHL' }),
            ]
        };

        if (description != '') {
            whereClause['description'] = {
                [Op.and]: [
                    Sequelize.where(Sequelize.col('marketsegment.description'), { [Op.eq]: description }),
                ]
            };
        }

        if (agent != '') {
            whereClause['agent_name'] = agent;
        }

        if (roomtype != '') { // filter roomtype.room_type_code sesudah where
            whereClause['roomtype'] = {
                [Op.and]: [
                    Sequelize.where(Sequelize.col('roomtype.room_type_code'), { [Op.eq]: roomtype }),
                ]
            };
        }

        const include_option = [
            {
                model: MarketSegment,
                as: 'marketsegment',
                attributes: [],  // just make relation not show column
                required: false,  //left outer join; true : inner join
                where: whereClauseSegment
            },
            {
                model: RoomType,
                as: 'roomtype',
                attributes: [],
                required: false
            }
        ]

        if (description != '' || roomtype != '') {
            include_option[0].required = true
        }

        const attributes = [];
        const years = JSON.parse(yearsBeforeCurrYear);

        for (let i = 0; i < years.length; i++) {
            const year = years[i];

            const attribute = [
                Sequelize.fn(
                    'SUM',
                    Sequelize.literal(
                        "CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " +
                        year +
                        ' THEN `rn` ELSE 0 END'
                    )
                ),
                'room_night_' + year,
            ];

            attributes.push(attribute);
        }

        console.log(whereClause, ">>>>>>>>>>>>>>> ISI KONDISI >>>>>>>>>>>>>>>> ")


        if (year != '' && comparison == '') {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        [Sequelize.literal("RIGHT(STR_TO_DATE(stay_date, '%d/%m/%Y'), 2)"), "day"],
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'room_night'],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        Sequelize.literal("RIGHT(STR_TO_DATE(stay_date, '%d/%m/%Y'), 2)"),
                    ],
                    order: [
                        [Sequelize.literal("RIGHT(STR_TO_DATE(stay_date, '%d/%m/%Y'), 2)"), 'ASC'],
                    ],
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        [Sequelize.literal("RIGHT(STR_TO_DATE(stay_date, '%d/%m/%Y'), 2)"), "day"],
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + prevYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + prevYear,
                        ],
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + currYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + currYear,
                        ],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        Sequelize.literal("RIGHT(STR_TO_DATE(stay_date, '%d/%m/%Y'), 2)"),
                    ],
                    order: [
                        [Sequelize.literal("RIGHT(STR_TO_DATE(stay_date, '%d/%m/%Y'), 2)"), 'ASC'],
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }

        } else {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        [Sequelize.literal("RIGHT(STR_TO_DATE(stay_date, '%d/%m/%Y'), 2)"), "day"],
                        ...attributes,

                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        Sequelize.literal("RIGHT(STR_TO_DATE(stay_date, '%d/%m/%Y'), 2)"),
                    ],
                    order: [
                        [Sequelize.literal("RIGHT(STR_TO_DATE(stay_date, '%d/%m/%Y'), 2)"), 'ASC'],
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        }
    };

    // ---------------------------------------------------
    // WHL YTD DAILY ON THE BOOK
    // ---------------------------------------------------
    static async getWhlDailyBooking(req, res) {
        const { hotel, year, comparison, month, description, agent, roomtype } = req.query;

        var currentTime = new Date();
        var currYear = year > 0 ? year : currentTime.getFullYear();
        var prevYear = comparison > 0 ? Number(comparison) : currentTime.getFullYear() - 1;
        var nextYear = currentTime.getFullYear() + 1

        // list 1 years before current year
        const startCurrYear = currYear - 1;
        const curryearList = [];

        for (let tahun = startCurrYear; tahun <= currYear; tahun++) {
            if (tahun != 2020 && tahun != 2021) {
                curryearList.push(tahun);
            }
        }

        curryearList.push(nextYear)

        const yearsBeforeCurrYear = '[' + curryearList.join(', ') + ']'

        console.log(yearsBeforeCurrYear, ">>>>>>>>>>>>>>>> ISI DAFTAR TAHUN >>>>>>>>>>>>>>>>>>>>>>")

        let whereClause = {};
        let whereClauseSegment = {}

        if (year != '' && comparison == '') {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    room_class: {
                        [Op.notIn]: ['PSE','']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear)
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    room_class: {
                        [Op.notIn]: ['PSE','']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    room_class: {
                        [Op.notIn]: ['PSE','']
                    },
                    [Op.and]: [
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    room_class: {
                        [Op.notIn]: ['PSE','']
                    },
                    [Op.and]: [
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    room_class: {
                        [Op.notIn]: ['PSE','']
                    },
                    [Op.and]: [
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    room_class: {
                        [Op.notIn]: ['PSE','']
                    },
                    [Op.and]: [
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        }


        if (hotel != '') {
            whereClause['room_class'] = hotel;
        }

        whereClause['segment'] = {
            [Op.and]: [
                Sequelize.where(Sequelize.col('marketsegment.segment'), { [Op.eq]: 'WHL' }),
            ]
        };

        if (description != '') {
            whereClause['description'] = {
                [Op.and]: [
                    Sequelize.where(Sequelize.col('marketsegment.description'), { [Op.eq]: description }),
                ]
            };
        }

        if (agent != '') {
            whereClause['agent_name'] = agent;
        }

        if (roomtype != '') { // filter roomtype.room_type_code sesudah where
            whereClause['roomtype'] = {
                [Op.and]: [
                    Sequelize.where(Sequelize.col('roomtype.room_type_code'), { [Op.eq]: roomtype }),
                ]
            };
        }

        const include_option = [
            {
                model: MarketSegment,
                as: 'marketsegment',
                attributes: [],  // just make relation not show column
                required: false,  //left outer join; true : inner join
                where: whereClauseSegment
            },
            {
                model: RoomType,
                as: 'roomtype',
                attributes: [],
                required: false
            }
        ]

        if (description != '' || roomtype != '') {
            include_option[0].required = true
        }

        const attributes = [];
        const years = JSON.parse(yearsBeforeCurrYear);

        for (let i = 0; i < years.length; i++) {
            const year = years[i];

            const attribute = [
                Sequelize.fn(
                    'SUM',
                    Sequelize.literal(
                        "CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " +
                        year +
                        ' THEN `rn` ELSE 0 END'
                    )
                ),
                'room_night_' + year,
            ];

            attributes.push(attribute);
        }

        console.log(whereClause, ">>>>>>>>>>>>>>> ISI KONDISI >>>>>>>>>>>>>>>> ")


        if (year != '' && comparison == '') {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        [Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), 'stay_month'],
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'room_night'],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')),
                    ],
                    order: [
                        [Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), 'ASC'],
                    ],
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        [Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), 'stay_month'],
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + prevYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + prevYear,
                        ],
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + currYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + currYear,
                        ],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')),
                    ],
                    order: [
                        [Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), 'ASC'],
                    ],
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }

        } else {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        [Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), 'stay_month'],
                        ...attributes,

                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')),
                    ],
                    order: [
                        [Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), 'ASC'],
                    ],
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        }
    };

    // ---------------------------------------------------
    // WHL YTD TOP SEGMENTATIONS
    // ---------------------------------------------------
    static async getWhlTopSegmentation(req, res) {
        const { hotel, year, comparison, month, description, agent, roomtype } = req.query;

        var currentTime = new Date();
        var currYear = year > 0 ? year : currentTime.getFullYear();
        var prevYear = comparison > 0 ? Number(comparison) : currentTime.getFullYear() - 1;
        var nextYear = currentTime.getFullYear() + 1

        // list 1 years before current year
        const startCurrYear = currYear - 1;
        const curryearList = [];

        for (let tahun = startCurrYear; tahun <= currYear; tahun++) {
            if (tahun != 2020 && tahun != 2021) {
                curryearList.push(tahun);
            }
        }

        curryearList.push(nextYear)

        const yearsBeforeCurrYear = '[' + curryearList.join(', ') + ']'

        console.log(">>>>>>>>>>>>>>>> bikin kondisi baru disini >>>>>>>>>>>>>>>>>>>>>>")
        let whereClause = {};
        let whereClauseSegment = {}

        if (year != '' && comparison == '') {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('country'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear)
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('country'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('country'), { [Op.ne]: '' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('country'), { [Op.ne]: '' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        }

        if (hotel != '') {
            whereClause['room_class'] = hotel;
        }

        whereClause['segment'] = {
            [Op.and]: [
                Sequelize.where(Sequelize.col('marketsegment.segment'), { [Op.eq]: 'WHL' }),
            ]
        };

        if (description != '') {
            whereClause['description'] = {
                [Op.and]: [
                    Sequelize.where(Sequelize.col('marketsegment.description'), { [Op.eq]: description }),
                ]
            };
        }

        if (agent != '') {
            whereClause['agent_name'] = agent;
        }

        if (roomtype != '') { // filter roomtype.room_type_code sesudah where
            whereClause['roomtype'] = {
                [Op.and]: [
                    Sequelize.where(Sequelize.col('roomtype.room_type_code'), { [Op.eq]: roomtype }),
                ]
            };
        }

        const include_option = [
            {
                model: MarketSegment,
                as: 'marketsegment',
                attributes: [],  // just make relation not show column
                required: true,  //left outer join; true : inner join
                where: whereClauseSegment
            },
            {
                model: RoomType,
                as: 'roomtype',
                attributes: [],
                required: true
            }
        ]

        const attributes = [];
        const years = JSON.parse(yearsBeforeCurrYear);

        for (let i = 0; i < years.length; i++) {
            const year = years[i];

            const attribute = [
                Sequelize.fn(
                    'SUM',
                    Sequelize.literal(
                        "CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " +
                        year +
                        ' THEN `rn` ELSE 0 END'
                    )
                ),
                'room_night_' + year,
            ];

            attributes.push(attribute);
        }

        console.log(whereClause, ">>>>>>>>>>>>>>> ISI KONDISI >>>>>>>>>>>>>>>> ")


        if (year != '' && comparison == '') {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'marketsegment.description',
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'room_night'],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: ["marketsegment.description"],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'marketsegment.description',
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + prevYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + prevYear,
                        ],
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + currYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + currYear,
                        ],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        'marketsegment.description',
                    ],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'marketsegment.description',
                        ...attributes,
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        'marketsegment.description',
                    ],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        }
    };

    // ---------------------------------------------------
    // WHL TOP YRD LENGTH OF STAY
    // ---------------------------------------------------
    static async getWhlTopLengthStay(req, res) {
        const { hotel, year, comparison, month, description, agent, roomtype } = req.query;

        var currentTime = new Date();
        var currYear = year > 0 ? year : currentTime.getFullYear();
        var prevYear = comparison > 0 ? Number(comparison) : currentTime.getFullYear() - 1;
        var nextYear = currentTime.getFullYear() + 1

        // list 1 years before current year
        const startCurrYear = currYear - 1;
        const curryearList = [];

        for (let tahun = startCurrYear; tahun <= currYear; tahun++) {
            if (tahun != 2020 && tahun != 2021) {
                curryearList.push(tahun);
            }
        }

        curryearList.push(nextYear)

        const yearsBeforeCurrYear = '[' + curryearList.join(', ') + ']'

        console.log(">>>>>>>>>>>>>>>> bikin kondisi baru disini >>>>>>>>>>>>>>>>>>>>>>")
        let whereClause = {};
        let whereClauseSegment = {}

        if (year != '' && comparison == '') {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('los'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear)
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('los'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('los'), { [Op.ne]: '' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('los'), { [Op.ne]: '' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        }

        if (hotel != '') {
            whereClause['room_class'] = hotel;
        }

        whereClause['segment'] = {
            [Op.and]: [
                Sequelize.where(Sequelize.col('marketsegment.segment'), { [Op.eq]: 'WHL' }),
            ]
        };

        if (description != '') {
            whereClause['description'] = {
                [Op.and]: [
                    Sequelize.where(Sequelize.col('marketsegment.description'), { [Op.eq]: description }),
                ]
            };
        }

        if (agent != '') {
            whereClause['agent_name'] = agent;
        }

        if (roomtype != '') { // filter roomtype.room_type_code sesudah where
            whereClause['roomtype'] = {
                [Op.and]: [
                    Sequelize.where(Sequelize.col('roomtype.room_type_code'), { [Op.eq]: roomtype }),
                ]
            };
        }

        const include_option = [
            {
                model: MarketSegment,
                as: 'marketsegment',
                attributes: [],  // just make relation not show column
                required: true,  //left outer join; true : inner join
                where: whereClauseSegment
            },
            {
                model: RoomType,
                as: 'roomtype',
                attributes: [],
                required: true
            }
        ]

        const attributes = [];
        const years = JSON.parse(yearsBeforeCurrYear);

        for (let i = 0; i < years.length; i++) {
            const year = years[i];

            const attribute = [
                Sequelize.fn(
                    'SUM',
                    Sequelize.literal(
                        "CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " +
                        year +
                        ' THEN `rn` ELSE 0 END'
                    )
                ),
                'room_night_' + year,
            ];

            attributes.push(attribute);
        }


        console.log(whereClause, ">>>>>>>>>>>>>>> ISI KONDISI >>>>>>>>>>>>>>>> ")

        if (year != '' && comparison == '') {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'los',
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'room_night'],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: ['los'],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'los',
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + prevYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + prevYear,
                        ],
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + currYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + currYear,
                        ],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        'los',
                    ],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'los',
                        ...attributes,
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        'los',
                    ],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        }
    };

    // ---------------------------------------------------
    // WHL YTD TOP BOOKING LEAD TIME
    // ---------------------------------------------------
    static async getWhlTopBookingLeadTime(req, res) {
        const { hotel, year, comparison, month, description, agent, roomtype } = req.query;

        var currentTime = new Date();
        var currYear = year > 0 ? year : currentTime.getFullYear();
        var prevYear = comparison > 0 ? Number(comparison) : currentTime.getFullYear() - 1;
        var nextYear = currentTime.getFullYear() + 1

        // list 1 years before current year
        const startCurrYear = currYear - 1;
        const curryearList = [];

        for (let tahun = startCurrYear; tahun <= currYear; tahun++) {
            if (tahun != 2020 && tahun != 2021) {
                curryearList.push(tahun);
            }
        }

        curryearList.push(nextYear)

        const yearsBeforeCurrYear = '[' + curryearList.join(', ') + ']'

        console.log(">>>>>>>>>>>>>>>> bikin kondisi baru disini >>>>>>>>>>>>>>>>>>>>>>")
        let whereClause = {};
        let whereClauseSegment = {}

        if (year != '' && comparison == '') {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('lead_days'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear)
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('lead_days'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('lead_days'), { [Op.ne]: '' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "','" + currYear + "')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('lead_days'), { [Op.ne]: '' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "','" + currYear + "')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('lead_days'), { [Op.ne]: '' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('lead_days'), { [Op.ne]: '' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        }

        if (hotel != '') {
            whereClause['room_class'] = hotel;
        }

        whereClause['segment'] = {
            [Op.and]: [
                Sequelize.where(Sequelize.col('marketsegment.segment'), { [Op.eq]: 'WHL' }),
            ]
        };

        if (description != '') {
            whereClause['description'] = {
                [Op.and]: [
                    Sequelize.where(Sequelize.col('marketsegment.description'), { [Op.eq]: description }),
                ]
            };
        }

        if (agent != '') {
            whereClause['agent_name'] = agent;
        }

        if (roomtype != '') { // filter roomtype.room_type_code sesudah where
            whereClause['roomtype'] = {
                [Op.and]: [
                    Sequelize.where(Sequelize.col('roomtype.room_type_code'), { [Op.eq]: roomtype }),
                ]
            };
        }

        const include_option = [
            {
                model: MarketSegment,
                as: 'marketsegment',
                attributes: [],  // just make relation not show column
                required: true,  //left outer join; true : inner join
                where: whereClauseSegment
            },
            {
                model: RoomType,
                as: 'roomtype',
                attributes: [],
                required: true
            }
        ]

        const attributes = [];
        const years = JSON.parse(yearsBeforeCurrYear);

        for (let i = 0; i < years.length; i++) {
            const year = years[i];

            const attribute = [
                Sequelize.fn(
                    'SUM',
                    Sequelize.literal(
                        "CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " +
                        year +
                        ' THEN `rn` ELSE 0 END'
                    )
                ),
                'room_night_' + year,
            ];

            attributes.push(attribute);
        }

        console.log(whereClause, ">>>>>>>>>>>>>>> ISI KONDISI >>>>>>>>>>>>>>>> ")


        if (year != '' && comparison == '') {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'lead_days',
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'room_night'],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: ['lead_days'],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'lead_days',
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + prevYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + prevYear,
                        ],
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + currYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + currYear,
                        ],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        'lead_days',
                    ],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'lead_days',
                        ...attributes,
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        'lead_days',
                    ],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        }
    };

    // ---------------------------------------------------
    // WHL YTD TOP ROOM TYPES
    // ---------------------------------------------------
    static async getWhlTopRoomType(req, res) {
        const { hotel, year, comparison, month, description, agent, roomtype } = req.query;

        var currentTime = new Date();
        var currYear = year > 0 ? year : currentTime.getFullYear();
        var prevYear = comparison > 0 ? Number(comparison) : currentTime.getFullYear() - 1;
        var nextYear = currentTime.getFullYear() + 1

        // list 1 years before current year
        const startCurrYear = currYear - 1;
        const curryearList = [];

        for (let tahun = startCurrYear; tahun <= currYear; tahun++) {
            if (tahun != 2020 && tahun != 2021) {
                curryearList.push(tahun);
            }
        }

        curryearList.push(nextYear)

        const yearsBeforeCurrYear = '[' + curryearList.join(', ') + ']'

        console.log(">>>>>>>>>>>>>>>> bikin kondisi baru disini >>>>>>>>>>>>>>>>>>>>>>")
        let whereClause = {};
        let whereClauseSegment = {}

        if (year != '' && comparison == '') {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear)
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "','" + currYear + "')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "','" + currYear + "')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        }

        if (hotel != '') {
            whereClause['room_class'] = hotel;
        }

        whereClause['segment'] = {
            [Op.and]: [
                Sequelize.where(Sequelize.col('marketsegment.segment'), { [Op.eq]: 'WHL' }),
            ]
        };

        if (description != '') {
            whereClause['description'] = {
                [Op.and]: [
                    Sequelize.where(Sequelize.col('marketsegment.description'), { [Op.eq]: description }),
                ]
            };
        }

        if (agent != '') {
            whereClause['agent_name'] = agent;
        }

        if (roomtype != '') { // filter roomtype.room_type_code sesudah where
            whereClause['roomtype'] = {
                [Op.and]: [
                    Sequelize.where(Sequelize.col('roomtype.room_type'), { [Op.eq]: roomtype }),
                ]
            };
        }

        const include_option = [
            {
                model: MarketSegment,
                as: 'marketsegment',
                attributes: [],  // just make relation not show column
                required: true,  //left outer join; true : inner join
                where: whereClauseSegment
            },
            {
                model: RoomType,
                as: 'roomtype',
                attributes: [],
                required: true
            }
        ]

        const attributes = [];
        const years = JSON.parse(yearsBeforeCurrYear);

        for (let i = 0; i < years.length; i++) {
            const year = years[i];

            const attribute = [
                Sequelize.fn(
                    'SUM',
                    Sequelize.literal(
                        "CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " +
                        year +
                        ' THEN `rn` ELSE 0 END'
                    )
                ),
                'room_night_' + year,
            ];

            attributes.push(attribute);
        }

        console.log(whereClause, ">>>>>>>>>>>>>>> ISI KONDISI >>>>>>>>>>>>>>>> ")

        if (year != '' && comparison == '') {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'roomtype.room_type',
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'room_night'],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: ['roomtype.room_type'],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'roomtype.room_type',
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + prevYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + prevYear,
                        ],
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + currYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + currYear,
                        ],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        'roomtype.room_type',
                    ],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'roomtype.room_type',
                        ...attributes,
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        'roomtype.room_type',
                    ],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        }
    };

    // ---------------------------------------------------
    // WHL YTD TOP SUPPORTERS
    // ---------------------------------------------------
    static async getWhlTopSupporter(req, res) {
        const { hotel, year, comparison, month, description, agent, roomtype } = req.query;

        var currentTime = new Date();
        var currYear = year > 0 ? year : currentTime.getFullYear();
        var prevYear = comparison > 0 ? Number(comparison) : currentTime.getFullYear() - 1;
        var nextYear = currentTime.getFullYear() + 1

        // list 1 years before current year
        const startCurrYear = currYear - 1;
        const curryearList = [];

        for (let tahun = startCurrYear; tahun <= currYear; tahun++) {
            if (tahun != 2020 && tahun != 2021) {
                curryearList.push(tahun);
            }
        }

        curryearList.push(nextYear)

        const yearsBeforeCurrYear = '[' + curryearList.join(', ') + ']'

        console.log(">>>>>>>>>>>>>>>> bikin kondisi baru disini >>>>>>>>>>>>>>>>>>>>>>")
        let whereClause = {};
        let whereClauseSegment = {}

        if (year != '' && comparison == '') {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear)
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "','" + currYear + "')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "','" + currYear + "')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.ne]: '' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.ne]: '' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        }

        if (hotel != '') {
            whereClause['room_class'] = hotel;
        }

        whereClause['segment'] = {
            [Op.and]: [
                Sequelize.where(Sequelize.col('marketsegment.segment'), { [Op.eq]: 'WHL' }),
            ]
        };

        if (description != '') {
            whereClause['description'] = {
                [Op.and]: [
                    Sequelize.where(Sequelize.col('marketsegment.description'), { [Op.eq]: description }),
                ]
            };
        }

        if (agent != '') {
            whereClause['agent_name'] = agent;
        }

        if (roomtype != '') { // filter roomtype.room_type_code sesudah where
            whereClause['roomtype'] = {
                [Op.and]: [
                    Sequelize.where(Sequelize.col('roomtype.room_type_code'), { [Op.eq]: roomtype }),
                ]
            };
        }

        const include_option = [
            {
                model: MarketSegment,
                as: 'marketsegment',
                attributes: [],  // just make relation not show column
                required: true,  //left outer join; true : inner join
                where: whereClauseSegment
            },
            {
                model: RoomType,
                as: 'roomtype',
                attributes: [],
                required: true
            }
        ]

        const attributes = [];
        const years = JSON.parse(yearsBeforeCurrYear);

        for (let i = 0; i < years.length; i++) {
            const year = years[i];

            const attribute = [
                Sequelize.fn(
                    'SUM',
                    Sequelize.literal(
                        "CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " +
                        year +
                        ' THEN `rn` ELSE 0 END'
                    )
                ),
                'room_night_' + year,
            ];

            attributes.push(attribute);
        }

        console.log(whereClause, ">>>>>>>>>>>>>>> ISI KONDISI >>>>>>>>>>>>>>>> ")


        if (year != '' && comparison == '') {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'agent_name',
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'room_night'],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: ['agent_name'],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'agent_name',
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + prevYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + prevYear,
                        ],
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + currYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + currYear,
                        ],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        'agent_name',
                    ],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'agent_name',
                        ...attributes,
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        'agent_name',
                    ],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        }
    };

    // ---------------------------------------------------
    // WHL YTD SUMMARY
    // ---------------------------------------------------
    static async getWhlSummary(req, res) {
        const { hotel, year, comparison, month, description, agent, roomtype } = req.query;

        var currentTime = new Date();
        var currYear = year > 0 ? year : currentTime.getFullYear();
        var prevYear = comparison > 0 ? Number(comparison) : currentTime.getFullYear() - 1;
        var nextYear = currentTime.getFullYear() + 1


        let whereClause = {};
        let whereClauseSegment = {}

        if (year != '' && comparison == '') {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear)
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else if (year == '' && comparison != '' || year != '' && comparison != '') {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "','" + currYear + "')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "','" + currYear + "')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2019,2020,2021')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2019,2020,2021')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        }

        if (hotel != '') {
            whereClause['room_class'] = hotel;
        }

        whereClause['segment'] = {
            [Op.and]: [
                Sequelize.where(Sequelize.col('marketsegment.segment'), { [Op.eq]: 'WHL' }),
            ]
        };

        if (description != '') {
            whereClause['description'] = {
                [Op.and]: [
                    Sequelize.where(Sequelize.col('marketsegment.description'), { [Op.eq]: description }),
                ]
            };
        }

        if (agent != '') {
            whereClause['agent_name'] = agent;
        }

        if (roomtype != '') { // filter roomtype.room_type_code sesudah where
            whereClause['roomtype'] = {
                [Op.and]: [
                    Sequelize.where(Sequelize.col('roomtype.room_type_code'), { [Op.eq]: roomtype }),
                ]
            };
        }

        const include_option = [
            {
                model: MarketSegment,
                as: 'marketsegment',
                attributes: [],  // just make relation not show column
                required: true,  //left outer join; true : inner join
                where: whereClauseSegment
            },
            {
                model: RoomType,
                as: 'roomtype',
                attributes: [],
                required: true
            }
        ]

        try {

            if (year != '' && comparison == '') {
                const result = await Master.findAll({
                    attributes: [
                        [Sequelize.fn('year', Sequelize.fn('str_to_date', Sequelize.col('stay_date'), '%d/%m/%Y')), 'stay_year'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${currYear} THEN REPLACE(rn, ',', '') ELSE 0 END`)), 'XRN_MTD_CURRYEAR'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${currYear} THEN REPLACE(room_rev_usd, ',', '') ELSE 0 END`)), 'XRREV_MTD_CURRYEAR'],
                    ],
                    include: include_option,

                    where: whereClause,
                    group: [
                        Sequelize.fn('year', Sequelize.fn('str_to_date', Sequelize.col('stay_date'), '%d/%m/%Y'))
                    ],
                    order: [
                        [Sequelize.fn('year', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), 'ASC']
                    ],
                });

                // Manipulasi hasil query untuk menghasilkan output yang diinginkan
                const output = result.map(item => {
                    // console.log(XPU23BEFORE23, " <<<<<< SETIAP ITEM")
                    const stay_year = item.dataValues.stay_year;
                    const XRN_MTD_CURR_YEAR = Number(item.dataValues.XRN_MTD_CURRYEAR);
                    const XRREV_MTD_CURR_YEAR = Number(item.dataValues.XRREV_MTD_CURRYEAR);

                    const XARR_MTD_CURR_YEAR = (XRREV_MTD_CURR_YEAR / XRN_MTD_CURR_YEAR).toFixed(2);

                    const dynamicKey = `XRN_MTD_CURR_${stay_year}`;
                    const dynamicKey2 = `XRREV_MTD_CURR_${stay_year}`;
                    const dynamicKey3 = `XARR_MTD_CURR_${stay_year}`;

                    return {
                        stay_year,
                        [dynamicKey]: XRN_MTD_CURR_YEAR,
                        [dynamicKey2]: XRREV_MTD_CURR_YEAR,
                        [dynamicKey3]: XARR_MTD_CURR_YEAR
                    }
                })

                const modifiedData = output.map(item => {
                    const modifiedItem = {};

                    for (let key in item) {
                        const modifiedKey = key.replace(/(CURR_)/g, "");
                        modifiedItem[modifiedKey] = item[key];
                    }

                    return modifiedItem;
                });

                res.status(200).json(modifiedData)
            } else if (year == '' && comparison != '' || year != '' && comparison != '') {
                const result = await Master.findAll({
                    attributes: [
                        [Sequelize.fn('year', Sequelize.fn('str_to_date', Sequelize.col('stay_date'), '%d/%m/%Y')), 'stay_year'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${prevYear} THEN REPLACE(rn, ',', '') ELSE 0 END`)), 'XRN_MTD_PREVYEAR'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${currYear} THEN REPLACE(rn, ',', '') ELSE 0 END`)), 'XRN_MTD_CURRYEAR'],


                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${prevYear} THEN REPLACE(room_rev_usd, ',', '') ELSE 0 END`)), 'XRREV_MTD_PREVYEAR'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${currYear} THEN REPLACE(room_rev_usd, ',', '') ELSE 0 END`)), 'XRREV_MTD_CURRYEAR'],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: Sequelize.literal('YEAR(STR_TO_DATE(stay_date, "%d/%m/%Y"))'),
                    order: Sequelize.literal('YEAR(STR_TO_DATE(stay_date, "%d/%m/%Y")) ASC'),
                });

                // Manipulasi hasil query untuk menghasilkan output yang diinginkan
                const output = result.map(item => {
                    // console.log(XPU23BEFORE23, " <<<<<< SETIAP ITEM")
                    const stay_year = item.dataValues.stay_year;
                    const XRN_MTD_COMP_YEAR = Number(item.dataValues.XRN_MTD_PREVYEAR);
                    const XRN_MTD_CURR_YEAR = Number(item.dataValues.XRN_MTD_CURRYEAR);


                    const XRREV_MTD_COMP_YEAR = Number(item.dataValues.XRREV_MTD_PREVYEAR);
                    const XRREV_MTD_CURR_YEAR = Number(item.dataValues.XRREV_MTD_CURRYEAR);

                    const XARR_MTD_COMP_YEAR = (XRREV_MTD_COMP_YEAR / XRN_MTD_COMP_YEAR).toFixed(2);
                    const XARR_MTD_CURR_YEAR = (XRREV_MTD_CURR_YEAR / XRN_MTD_CURR_YEAR).toFixed(2);

                    const dynamicKey = `XRN_MTD_COMP_${stay_year}`;
                    const dynamicKey2 = `XRREV_MTD_COMP_${stay_year}`;
                    const dynamicKey3 = `XARR_MTD_COMP_${stay_year}`;
                    const dynamicKey4 = `XRN_MTD_CURR_${stay_year}`;
                    const dynamicKey5 = `XRREV_MTD_CURR_${stay_year}`;
                    const dynamicKey6 = `XARR_MTD_CURR_${stay_year}`;

                    return {
                        stay_year,
                        [dynamicKey]: XRN_MTD_COMP_YEAR,
                        [dynamicKey2]: XRREV_MTD_COMP_YEAR,
                        [dynamicKey3]: XARR_MTD_COMP_YEAR,
                        [dynamicKey4]: XRN_MTD_CURR_YEAR,
                        [dynamicKey5]: XRREV_MTD_CURR_YEAR,
                        [dynamicKey6]: XARR_MTD_CURR_YEAR,
                    }

                })

                const filteredOutput = output.map(item => {
                    const filteredItem = { ...item };

                    if (filteredItem.XRN_MTD_COMP_YEAR === 0 || isNaN(parseFloat(filteredItem.XRN_MTD_COMP_YEAR))) {
                        delete filteredItem.XRN_MTD_COMP_YEAR;
                    }

                    if (filteredItem.XRREV_MTD_COMP_YEAR === 0 || isNaN(parseFloat(filteredItem.XRREV_MTD_COMP_YEAR))) {
                        delete filteredItem.XRREV_MTD_COMP_YEAR;
                    }

                    if (filteredItem.XARR_MTD_COMP_YEAR === 'NaN') {
                        delete filteredItem.XARR_MTD_COMP_YEAR;
                    }

                    if (filteredItem.XRN_MTD_CURR_YEAR === 0 || isNaN(parseFloat(filteredItem.XRN_MTD_CURR_YEAR))) {
                        delete filteredItem.XRN_MTD_CURR_YEAR;
                    }

                    if (filteredItem.XRREV_MTD_CURR_YEAR === 0 || isNaN(parseFloat(filteredItem.XRREV_MTD_CURR_YEAR))) {
                        delete filteredItem.XRREV_MTD_CURR_YEAR;
                    }

                    if (filteredItem.XARR_MTD_CURR_YEAR === 'NaN') {
                        delete filteredItem.XARR_MTD_CURR_YEAR;
                    }

                    return filteredItem;
                });

                const modifiedData = filteredOutput.map(item => {
                    const modifiedItem = {};

                    for (let key in item) {
                        const value = item[key];

                        if (value !== "NaN" && value !== 0 && value !== false) {
                            modifiedItem[key] = value;
                        }
                    }

                    return modifiedItem;
                });

                const modifiedData1 = modifiedData.map(item => {
                    const modifiedItem = {};

                    for (let key in item) {
                        const modifiedKey = key.replace(/(COMP_|CURR_)/g, "");
                        modifiedItem[modifiedKey] = item[key];
                    }

                    return modifiedItem;
                });

                res.status(200).json(modifiedData1)
            } else {
                const result = await Master.findAll({
                    attributes: [
                        [Sequelize.fn('year', Sequelize.fn('str_to_date', Sequelize.col('stay_date'), '%d/%m/%Y')), 'stay_year'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${prevYear} THEN REPLACE(rn, ',', '') ELSE 0 END`)), 'XRN_MTD_PREV_YEAR'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${currYear} THEN REPLACE(rn, ',', '') ELSE 0 END`)), 'XRN_MTD_CURR_YEAR'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${nextYear} THEN REPLACE(rn, ',', '') ELSE 0 END`)), 'XRN_MTD_NEXT_YEAR'],


                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${prevYear} THEN REPLACE(room_rev_usd, ',', '') ELSE 0 END`)), 'XRREV_MTD_PREV_YEAR'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${currYear} THEN REPLACE(room_rev_usd, ',', '') ELSE 0 END`)), 'XRREV_MTD_CURR_YEAR'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${nextYear} THEN REPLACE(room_rev_usd, ',', '') ELSE 0 END`)), 'XRREV_MTD_NEXT_YEAR'],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: Sequelize.literal('YEAR(STR_TO_DATE(stay_date, "%d/%m/%Y"))'),
                    order: Sequelize.literal('YEAR(STR_TO_DATE(stay_date, "%d/%m/%Y")) ASC'),
                });



                // Manipulasi hasil query untuk menghasilkan output yang diinginkan
                const output = result.map(item => {
                    // console.log(XPU23BEFORE23, " <<<<<< SETIAP ITEM")
                    const stay_year = item.dataValues.stay_year;
                    const XRN_MTD_PREV_YEAR = Number(item.dataValues.XRN_MTD_PREV_YEAR);
                    const XRN_MTD_CURR_YEAR = Number(item.dataValues.XRN_MTD_CURR_YEAR);
                    const XRN_MTD_NEXT_YEAR = Number(item.dataValues.XRN_MTD_NEXT_YEAR);

                    const XRREV_MTD_PREV_YEAR = Number(item.dataValues.XRREV_MTD_PREV_YEAR);
                    const XRREV_MTD_CURR_YEAR = Number(item.dataValues.XRREV_MTD_CURR_YEAR);
                    const XRREV_MTD_NEXT_YEAR = Number(item.dataValues.XRREV_MTD_NEXT_YEAR);

                    const XARR_MTD_PREV_YEAR = (XRREV_MTD_PREV_YEAR / XRN_MTD_PREV_YEAR).toFixed(2);
                    const XARR_MTD_CURR_YEAR = (XRREV_MTD_CURR_YEAR / XRN_MTD_CURR_YEAR).toFixed(2);
                    const XARR_MTD_NEXT_YEAR = (XRREV_MTD_NEXT_YEAR / XRN_MTD_NEXT_YEAR).toFixed(2);

                    const dynamicKey = `XRN_MTD_PREV_${stay_year}`;
                    const dynamicKey2 = `XRREV_MTD_PREV_${stay_year}`;
                    const dynamicKey3 = `XARR_MTD_PREV_${stay_year}`;
                    const dynamicKey4 = `XRN_MTD_CURR_${stay_year}`;
                    const dynamicKey5 = `XRREV_MTD_CURR_${stay_year}`;
                    const dynamicKey6 = `XARR_MTD_CURR_${stay_year}`;
                    const dynamicKey7 = `XRN_MTD_NEXT_${stay_year}`;
                    const dynamicKey8 = `XRREV_MTD_NEXT_${stay_year}`;
                    const dynamicKey9 = `XARR_MTD_NEXT_${stay_year}`;

                    return {
                        stay_year,
                        [dynamicKey]: XRN_MTD_PREV_YEAR,
                        [dynamicKey2]: XRREV_MTD_PREV_YEAR,
                        [dynamicKey3]: XARR_MTD_PREV_YEAR,
                        [dynamicKey4]: XRN_MTD_CURR_YEAR,
                        [dynamicKey5]: XRREV_MTD_CURR_YEAR,
                        [dynamicKey6]: XARR_MTD_CURR_YEAR,
                        [dynamicKey7]: XRN_MTD_NEXT_YEAR,
                        [dynamicKey8]: XRREV_MTD_NEXT_YEAR,
                        [dynamicKey9]: XARR_MTD_NEXT_YEAR,
                    }

                })

                const filteredOutput = output.map(item => {
                    const filteredItem = { ...item };

                    if (filteredItem.XRN_MTD_PREV_YEAR === 0 || isNaN(parseFloat(filteredItem.XRN_MTD_PREV_YEAR))) {
                        delete filteredItem.XRN_MTD_PREV_YEAR;
                    }

                    if (filteredItem.XRREV_MTD_PREV_YEAR === 0 || isNaN(parseFloat(filteredItem.XRREV_MTD_PREV_YEAR))) {
                        delete filteredItem.XRREV_MTD_PREV_YEAR;
                    }

                    if (filteredItem.XARR_MTD_PREV_YEAR === 'NaN') {
                        delete filteredItem.XARR_MTD_PREV_YEAR;
                    }

                    if (filteredItem.XRN_MTD_CURR_YEAR === 0 || isNaN(parseFloat(filteredItem.XRN_MTD_CURR_YEAR))) {
                        delete filteredItem.XRN_MTD_CURR_YEAR;
                    }

                    if (filteredItem.XRREV_MTD_CURR_YEAR === 0 || isNaN(parseFloat(filteredItem.XRREV_MTD_CURR_YEAR))) {
                        delete filteredItem.XRREV_MTD_CURR_YEAR;
                    }

                    if (filteredItem.XARR_MTD_CURR_YEAR === 'NaN') {
                        delete filteredItem.XARR_MTD_CURR_YEAR;
                    }

                    if (filteredItem.XRN_MTD_NEXT_YEAR === 0 || isNaN(parseFloat(filteredItem.XRN_MTD_NEXT_YEAR))) {
                        delete filteredItem.XRN_MTD_NEXT_YEAR;
                    }

                    if (filteredItem.XRREV_MTD_NEXT_YEAR === 0 || isNaN(parseFloat(filteredItem.XRREV_MTD_NEXT_YEAR))) {
                        delete filteredItem.XRREV_MTD_NEXT_YEAR;
                    }


                    if (filteredItem.XARR_MTD_NEXT_YEAR === 'NaN') {
                        delete filteredItem.XARR_MTD_NEXT_YEAR;
                    }

                    return filteredItem;
                });

                const filteredYears = [2019, 2020, 2021]; // Daftar tahun yang ingin Anda filter

                const filteredData = filteredOutput.filter(item => !filteredYears.includes(item.stay_year));

                const modifiedData = filteredData.map(item => {
                    const modifiedItem = {};

                    for (let key in item) {
                        const value = item[key];

                        if (value !== "NaN" && value !== 0 && value !== false) {
                            modifiedItem[key] = value;
                        }
                    }

                    return modifiedItem;
                });

                const modifiedData1 = modifiedData.map(item => {
                    const modifiedItem = {};

                    for (let key in item) {
                        const modifiedKey = key.replace(/(PREV_|CURR_|NEXT_)/g, "");
                        modifiedItem[modifiedKey] = item[key];
                    }

                    return modifiedItem;
                });
                res.status(200).json(modifiedData1)
            }

        } catch (error) {
            console.log(error);
        }
    };

    // ---------------------------------------------------
    // LUXURY ESCAPE MTD DAILY ON THE BOOK
    // ---------------------------------------------------
    static async getLeMtdDailyBooking(req, res) {
        const { hotel, year, comparison, month, agent } = req.query;

        var currentTime = new Date();
        var currYear = year > 0 ? year : currentTime.getFullYear();
        var prevYear = comparison > 0 ? Number(comparison) : currentTime.getFullYear() - 1;
        var nextYear = currentTime.getFullYear() + 1

        // list 1 years before current year
        const startCurrYear = currYear - 1;
        const curryearList = [];

        for (let tahun = startCurrYear; tahun <= currYear; tahun++) {
            if (tahun != 2020 && tahun != 2021) {
                curryearList.push(tahun);
            }
        }

        curryearList.push(nextYear)

        const yearsBeforeCurrYear = '[' + curryearList.join(', ') + ']'

        console.log(yearsBeforeCurrYear, ">>>>>>>>>>>>>>>> ISI DAFTAR TAHUN >>>>>>>>>>>>>>>>>>>>>>")

        let whereClause = {};
        let whereClauseSegment = {}

        if (year != '' && comparison == '') {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.like]: '%Luxury Escape%' }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear)
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.like]: '%Luxury Escape%' }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.like]: '%Luxury Escape%' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.like]: '%Luxury Escape%' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.like]: '%Luxury Escape%' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.like]: '%Luxury Escape%' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        }


        if (hotel != '') {
            whereClause['room_class'] = hotel;
        }

        whereClause['segment'] = {
            [Op.and]: [
                Sequelize.where(Sequelize.col('marketsegment.segment'), { [Op.eq]: 'OTA' }),
            ]
        };

        if (agent != '') {
            whereClause['agent_name'] = agent;
        }


        const include_option = [
            {
                model: MarketSegment,
                as: 'marketsegment',
                attributes: [],  // just make relation not show column
                required: false,  //left outer join; true : inner join
                where: whereClauseSegment
            },
        ]

        const attributes = [];
        const years = JSON.parse(yearsBeforeCurrYear);

        for (let i = 0; i < years.length; i++) {
            const year = years[i];

            const attribute = [
                Sequelize.fn(
                    'SUM',
                    Sequelize.literal(
                        "CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " +
                        year +
                        ' THEN `rn` ELSE 0 END'
                    )
                ),
                'room_night_' + year,
            ];

            attributes.push(attribute);
        }

        console.log(whereClause, ">>>>>>>>>>>>>>> ISI KONDISI >>>>>>>>>>>>>>>> ")


        if (year != '' && comparison == '') {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        [Sequelize.literal("RIGHT(STR_TO_DATE(stay_date, '%d/%m/%Y'), 2)"), "day"],
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'room_night'],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        Sequelize.literal("RIGHT(STR_TO_DATE(stay_date, '%d/%m/%Y'), 2)"),
                    ],
                    order: [
                        [Sequelize.literal("RIGHT(STR_TO_DATE(stay_date, '%d/%m/%Y'), 2)"), 'ASC'],
                    ],
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        [Sequelize.literal("RIGHT(STR_TO_DATE(stay_date, '%d/%m/%Y'), 2)"), "day"],
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + prevYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + prevYear,
                        ],
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + currYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + currYear,
                        ],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        Sequelize.literal("RIGHT(STR_TO_DATE(stay_date, '%d/%m/%Y'), 2)"),
                    ],
                    order: [
                        [Sequelize.literal("RIGHT(STR_TO_DATE(stay_date, '%d/%m/%Y'), 2)"), 'ASC'],
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }

        } else {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        [Sequelize.literal("RIGHT(STR_TO_DATE(stay_date, '%d/%m/%Y'), 2)"), "day"],
                        ...attributes,

                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        Sequelize.literal("RIGHT(STR_TO_DATE(stay_date, '%d/%m/%Y'), 2)"),
                    ],
                    order: [
                        [Sequelize.literal("RIGHT(STR_TO_DATE(stay_date, '%d/%m/%Y'), 2)"), 'ASC'],
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        }
    };

    // ---------------------------------------------------
    // LUXURY ESCAPE YTD DAILY ON THE BOOK
    // ---------------------------------------------------
    static async getLeDailyBooking(req, res) {
        const { hotel, year, comparison, month, agent } = req.query;

        var currentTime = new Date();
        var currYear = year > 0 ? year : currentTime.getFullYear();
        var prevYear = comparison > 0 ? Number(comparison) : currentTime.getFullYear() - 1;
        var nextYear = currentTime.getFullYear() + 1

        // list 1 years before current year
        const startCurrYear = currYear - 1;
        const curryearList = [];

        for (let tahun = startCurrYear; tahun <= currYear; tahun++) {
            if (tahun != 2020 && tahun != 2021) {
                curryearList.push(tahun);
            }
        }

        curryearList.push(nextYear)

        const yearsBeforeCurrYear = '[' + curryearList.join(', ') + ']'

        console.log(">>>>>>>>>>>>>>>> bikin kondisi baru disini >>>>>>>>>>>>>>>>>>>>>>")
        let whereClause = {};
        let whereClauseSegment = {}

        if (year != '' && comparison == '') {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.like]: '%Luxury Escape%' }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear)
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.like]: '%Luxury Escape%' }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.like]: '%Luxury Escape%' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.like]: '%Luxury Escape%' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.like]: '%Luxury Escape%' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.like]: '%Luxury Escape%' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        }


        if (hotel != '') {
            whereClause['room_class'] = hotel;
        }

        whereClause['segment'] = {
            [Op.and]: [
                Sequelize.where(Sequelize.col('marketsegment.segment'), { [Op.eq]: 'OTA' }),
            ]
        };


        if (agent != '') {
            whereClause['agent_name'] = agent;
        }


        const include_option = [
            {
                model: MarketSegment,
                as: 'marketsegment',
                attributes: [],  // just make relation not show column
                required: false,  //left outer join; true : inner join
                where: whereClauseSegment
            },
        ]

        const attributes = [];
        const years = JSON.parse(yearsBeforeCurrYear);

        for (let i = 0; i < years.length; i++) {
            const year = years[i];

            const attribute = [
                Sequelize.fn(
                    'SUM',
                    Sequelize.literal(
                        "CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " +
                        year +
                        ' THEN `rn` ELSE 0 END'
                    )
                ),
                'room_night_' + year,
            ];

            attributes.push(attribute);
        }

        console.log(whereClause, ">>>>>>>>>>>>>>> ISI KONDISI >>>>>>>>>>>>>>>> ")


        if (year != '' && comparison == '') {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        [Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), 'stay_month'],
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'room_night'],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')),
                    ],
                    order: [
                        [Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), 'ASC'],
                    ],
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        [Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), 'stay_month'],
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + prevYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + prevYear,
                        ],
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + currYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + currYear,
                        ],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')),
                    ],
                    order: [
                        [Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), 'ASC'],
                    ],
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        [Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), 'stay_month'],
                        ...attributes,
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')),
                    ],
                    order: [
                        [Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), 'ASC'],
                    ],
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        }
    };

    // ---------------------------------------------------
    // LUXURY ESCAPE TOP COUNTRIES
    // ---------------------------------------------------
    static async getLeTopCountry(req, res) {
        const { hotel, year, comparison, month, agent } = req.query;

        var currentTime = new Date();
        var currYear = year > 0 ? year : currentTime.getFullYear();
        var prevYear = comparison > 0 ? Number(comparison) : currentTime.getFullYear() - 1;
        var nextYear = currentTime.getFullYear() + 1

        // list 1 years before current year
        const startCurrYear = currYear - 1;
        const curryearList = [];

        for (let tahun = startCurrYear; tahun <= currYear; tahun++) {
            if (tahun != 2020 && tahun != 2021) {
                curryearList.push(tahun);
            }
        }

        curryearList.push(nextYear)

        const yearsBeforeCurrYear = '[' + curryearList.join(', ') + ']'

        console.log(">>>>>>>>>>>>>>>> bikin kondisi baru disini >>>>>>>>>>>>>>>>>>>>>>")
        let whereClause = {};
        let whereClauseSegment = {}

        if (year != '' && comparison == '') {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('country'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.like]: '%Luxury Escape%' }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear)
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('country'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.like]: '%Luxury Escape%' }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('country'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.like]: '%Luxury Escape%' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('country'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.like]: '%Luxury Escape%' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('country'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.like]: '%Luxury Escape%' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('country'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.like]: '%Luxury Escape%' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        }


        if (hotel != '') {
            whereClause['room_class'] = hotel;
        }

        whereClause['segment'] = {
            [Op.and]: [
                Sequelize.where(Sequelize.col('marketsegment.segment'), { [Op.eq]: 'OTA' }),
            ]
        };


        if (agent != '') {
            whereClause['agent_name'] = agent;
        }

        const include_option = [
            {
                model: MarketSegment,
                as: 'marketsegment',
                attributes: [],  // just make relation not show column
                required: false,  //left outer join; true : inner join
                where: whereClauseSegment
            },
        ]

        const attributes = [];
        const years = JSON.parse(yearsBeforeCurrYear);

        for (let i = 0; i < years.length; i++) {
            const year = years[i];

            const attribute = [
                Sequelize.fn(
                    'SUM',
                    Sequelize.literal(
                        "CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " +
                        year +
                        ' THEN `rn` ELSE 0 END'
                    )
                ),
                'room_night_' + year,
            ];

            attributes.push(attribute);
        }

        console.log(whereClause, ">>>>>>>>>>>>>>> ISI KONDISI >>>>>>>>>>>>>>>> ")


        if (year != '' && comparison == '') {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'country',
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'room_night'],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: ["country"],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'country',
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + prevYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + prevYear,
                        ],
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + currYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + currYear,
                        ],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: ["country"],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'country',
                        ...attributes,
                    ],
                    include: include_option,
                    where: whereClause,
                    group: ["country"],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        }
    };

    // ---------------------------------------------------
    // LUXURY ESCAPE TOP LENGTH OF STAY
    // ---------------------------------------------------
    static async getLeTopLengthStay(req, res) {
        const { hotel, year, comparison, month, agent } = req.query;

        var currentTime = new Date();
        var currYear = year > 0 ? year : currentTime.getFullYear();
        var prevYear = comparison > 0 ? Number(comparison) : currentTime.getFullYear() - 1;
        var nextYear = currentTime.getFullYear() + 1

        // list 1 years before current year
        const startCurrYear = currYear - 1;
        const curryearList = [];

        for (let tahun = startCurrYear; tahun <= currYear; tahun++) {
            if (tahun != 2020 && tahun != 2021) {
                curryearList.push(tahun);
            }
        }

        curryearList.push(nextYear)

        const yearsBeforeCurrYear = '[' + curryearList.join(', ') + ']'

        console.log(">>>>>>>>>>>>>>>> bikin kondisi baru disini >>>>>>>>>>>>>>>>>>>>>>")
        let whereClause = {};
        let whereClauseSegment = {}

        if (year != '' && comparison == '') {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('los'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.like]: '%Luxury Escape%' }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear)
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('los'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.like]: '%Luxury Escape%' }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('los'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.like]: '%Luxury Escape%' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('los'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.like]: '%Luxury Escape%' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('los'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.like]: '%Luxury Escape%' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('los'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.like]: '%Luxury Escape%' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        }


        if (hotel != '') {
            whereClause['room_class'] = hotel;
        }

        whereClause['segment'] = {
            [Op.and]: [
                Sequelize.where(Sequelize.col('marketsegment.segment'), { [Op.eq]: 'OTA' }),
            ]
        };


        if (agent != '') {
            whereClause['agent_name'] = agent;
        }

        const include_option = [
            {
                model: MarketSegment,
                as: 'marketsegment',
                attributes: [],  // just make relation not show column
                required: false,  //left outer join; true : inner join
                where: whereClauseSegment
            },
        ]

        const attributes = [];
        const years = JSON.parse(yearsBeforeCurrYear);

        for (let i = 0; i < years.length; i++) {
            const year = years[i];

            const attribute = [
                Sequelize.fn(
                    'SUM',
                    Sequelize.literal(
                        "CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " +
                        year +
                        ' THEN `rn` ELSE 0 END'
                    )
                ),
                'room_night_' + year,
            ];

            attributes.push(attribute);
        }

        console.log(whereClause, ">>>>>>>>>>>>>>> ISI KONDISI >>>>>>>>>>>>>>>> ")

        if (year != '' && comparison == '') {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'los',
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'room_night'],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: ["los"],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'los',
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + prevYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + prevYear,
                        ],
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + currYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + currYear,
                        ],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: ["los"],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'los',
                        ...attributes,
                    ],
                    include: include_option,
                    where: whereClause,
                    group: ["los"],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        }
    };

    // ---------------------------------------------------
    // LUXURY ESCAPE TOP BOOKING LEAD TIME
    // ---------------------------------------------------
    static async getLeTopBookingLeadTime(req, res) {
        const { hotel, year, comparison, month, agent } = req.query;

        var currentTime = new Date();
        var currYear = year > 0 ? year : currentTime.getFullYear();
        var prevYear = comparison > 0 ? Number(comparison) : currentTime.getFullYear() - 1;
        var nextYear = currentTime.getFullYear() + 1

        // list 1 years before current year
        const startCurrYear = currYear - 1;
        const curryearList = [];

        for (let tahun = startCurrYear; tahun <= currYear; tahun++) {
            if (tahun != 2020 && tahun != 2021) {
                curryearList.push(tahun);
            }
        }

        curryearList.push(nextYear)

        const yearsBeforeCurrYear = '[' + curryearList.join(', ') + ']'

        console.log(">>>>>>>>>>>>>>>> bikin kondisi baru disini >>>>>>>>>>>>>>>>>>>>>>")
        let whereClause = {};
        let whereClauseSegment = {}

        if (year != '' && comparison == '') {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('lead_days'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.like]: '%Luxury Escape%' }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear)
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('lead_days'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.like]: '%Luxury Escape%' }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('lead_days'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.like]: '%Luxury Escape%' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('lead_days'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.like]: '%Luxury Escape%' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('lead_days'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.like]: '%Luxury Escape%' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('lead_days'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.like]: '%Luxury Escape%' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        }


        if (hotel != '') {
            whereClause['room_class'] = hotel;
        }

        whereClause['segment'] = {
            [Op.and]: [
                Sequelize.where(Sequelize.col('marketsegment.segment'), { [Op.eq]: 'OTA' }),
            ]
        };


        if (agent != '') {
            whereClause['agent_name'] = agent;
        }

        const include_option = [
            {
                model: MarketSegment,
                as: 'marketsegment',
                attributes: [],  // just make relation not show column
                required: false,  //left outer join; true : inner join
                where: whereClauseSegment
            },
        ]

        const attributes = [];
        const years = JSON.parse(yearsBeforeCurrYear);

        for (let i = 0; i < years.length; i++) {
            const year = years[i];

            const attribute = [
                Sequelize.fn(
                    'SUM',
                    Sequelize.literal(
                        "CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " +
                        year +
                        ' THEN `rn` ELSE 0 END'
                    )
                ),
                'room_night_' + year,
            ];

            attributes.push(attribute);
        }

        console.log(whereClause, ">>>>>>>>>>>>>>> ISI KONDISI >>>>>>>>>>>>>>>> ")


        if (year != '' && comparison == '') {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'lead_days',
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'room_night'],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: ["lead_days"],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'lead_days',
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + prevYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + prevYear,
                        ],
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + currYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + currYear,
                        ],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: ["lead_days"],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'lead_days',
                        ...attributes,
                    ],
                    include: include_option,
                    where: whereClause,
                    group: ["lead_days"],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        }
    };

    // ---------------------------------------------------
    // LUXURY ESCAPE TOP ROOM TYPES
    // ---------------------------------------------------
    static async getLeTopRoomType(req, res) {

        const { hotel, year, comparison, month, agent } = req.query;

        var currentTime = new Date();
        var currYear = year > 0 ? year : currentTime.getFullYear();
        var prevYear = comparison > 0 ? Number(comparison) : currentTime.getFullYear() - 1;
        var nextYear = currentTime.getFullYear() + 1

        // list 1 years before current year
        const startCurrYear = currYear - 1;
        const curryearList = [];

        for (let tahun = startCurrYear; tahun <= currYear; tahun++) {
            if (tahun != 2020 && tahun != 2021) {
                curryearList.push(tahun);
            }
        }

        curryearList.push(nextYear)

        const yearsBeforeCurrYear = '[' + curryearList.join(', ') + ']'

        console.log(">>>>>>>>>>>>>>>> bikin kondisi baru disini >>>>>>>>>>>>>>>>>>>>>>")
        let whereClause = {};
        let whereClauseSegment = {}

        if (year != '' && comparison == '') {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.like]: '%Luxury Escape%' }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear)
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.like]: '%Luxury Escape%' }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.like]: '%Luxury Escape%' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.like]: '%Luxury Escape%' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.like]: '%Luxury Escape%' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.like]: '%Luxury Escape%' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        }

        if (hotel != '') {
            whereClause['room_class'] = hotel;
        }

        whereClause['segment'] = {
            [Op.and]: [
                Sequelize.where(Sequelize.col('marketsegment.segment'), { [Op.eq]: 'OTA' }),
            ]
        };


        if (agent != '') {
            whereClause['agent_name'] = agent;
        }

        const include_option = [
            {
                model: MarketSegment,
                as: 'marketsegment',
                attributes: [],  // just make relation not show column
                required: true,  //left outer join; true : inner join
                where: whereClauseSegment
            },
            {
                model: RoomType,
                as: 'roomtype',
                attributes: [],
                required: true
            }
        ]

        const attributes = [];
        const years = JSON.parse(yearsBeforeCurrYear);

        for (let i = 0; i < years.length; i++) {
            const year = years[i];

            const attribute = [
                Sequelize.fn(
                    'SUM',
                    Sequelize.literal(
                        "CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " +
                        year +
                        ' THEN `rn` ELSE 0 END'
                    )
                ),
                'room_night_' + year,
            ];

            attributes.push(attribute);
        }

        console.log(whereClause, ">>>>>>>>>>>>>>> ISI KONDISI >>>>>>>>>>>>>>>> ")


        if (year != '' && comparison == '') {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'roomtype.room_type',
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'room_night'],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: ['roomtype.room_type'],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'roomtype.room_type',
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + prevYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + prevYear,
                        ],
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + currYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + currYear,
                        ],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        'roomtype.room_type',
                    ],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'roomtype.room_type',
                        ...attributes,
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        'roomtype.room_type',
                    ],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        }
    };

    // ---------------------------------------------------
    // LUXURY ESCAPE TOP NATIONALITY
    // ---------------------------------------------------
    static async getLeTopNationality(req, res) {
        const { hotel, year, comparison, month, agent } = req.query;

        var currentTime = new Date();
        var currYear = year > 0 ? year : currentTime.getFullYear();
        var prevYear = comparison > 0 ? Number(comparison) : currentTime.getFullYear() - 1;
        var nextYear = currentTime.getFullYear() + 1

        // list 1 years before current year
        const startCurrYear = currYear - 1;
        const curryearList = [];

        for (let tahun = startCurrYear; tahun <= currYear; tahun++) {
            if (tahun != 2020 && tahun != 2021) {
                curryearList.push(tahun);
            }
        }

        curryearList.push(nextYear)

        const yearsBeforeCurrYear = '[' + curryearList.join(', ') + ']'

        console.log(">>>>>>>>>>>>>>>> bikin kondisi baru disini >>>>>>>>>>>>>>>>>>>>>>")
        let whereClause = {};
        let whereClauseSegment = {}

        if (year != '' && comparison == '') {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('nationality'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.like]: '%Luxury Escape%' }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear)
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('nationality'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.like]: '%Luxury Escape%' }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('nationality'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.like]: '%Luxury Escape%' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('nationality'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.like]: '%Luxury Escape%' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('nationality'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.like]: '%Luxury Escape%' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('nationality'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.like]: '%Luxury Escape%' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        }

        if (hotel != '') {
            whereClause['room_class'] = hotel;
        }

        whereClause['segment'] = {
            [Op.and]: [
                Sequelize.where(Sequelize.col('marketsegment.segment'), { [Op.eq]: 'OTA' }),
            ]
        };


        if (agent != '') {
            whereClause['agent_name'] = agent;
        }

        const include_option = [
            {
                model: MarketSegment,
                as: 'marketsegment',
                attributes: [],  // just make relation not show column
                required: true,  //left outer join; true : inner join
                where: whereClauseSegment
            },
            {
                model: RoomType,
                as: 'roomtype',
                attributes: [],
                required: true
            }
        ]

        const attributes = [];
        const years = JSON.parse(yearsBeforeCurrYear);

        for (let i = 0; i < years.length; i++) {
            const year = years[i];

            const attribute = [
                Sequelize.fn(
                    'SUM',
                    Sequelize.literal(
                        "CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " +
                        year +
                        ' THEN `rn` ELSE 0 END'
                    )
                ),
                'room_night_' + year,
            ];

            attributes.push(attribute);
        }

        console.log(whereClause, ">>>>>>>>>>>>>>> ISI KONDISI >>>>>>>>>>>>>>>> ")


        if (year != '' && comparison == '') {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'nationality',
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'room_night'],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: ['nationality'],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'nationality',
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + prevYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + prevYear,
                        ],
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + currYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + currYear,
                        ],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        'nationality',
                    ],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'nationality',
                        ...attributes,
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        'nationality',
                    ],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        }
    };

    // ---------------------------------------------------
    // LUXURY ESCAPE SUMMARY
    // ---------------------------------------------------
    static async getLeSummary(req, res) {
        const { hotel, year, comparison, month, agent } = req.query;

        var currentTime = new Date();
        var currYear = year > 0 ? year : currentTime.getFullYear();
        var prevYear = comparison > 0 ? Number(comparison) : currentTime.getFullYear() - 1;
        var nextYear = currentTime.getFullYear() + 1

        // list 1 years before current year
        const startCurrYear = currYear - 1;
        const curryearList = [];

        for (let tahun = startCurrYear; tahun <= currYear; tahun++) {
            if (tahun != 2020 && tahun != 2021) {
                curryearList.push(tahun);
            }
        }

        curryearList.push(nextYear)

        const yearsBeforeCurrYear = '[' + curryearList.join(', ') + ']'

        console.log(">>>>>>>>>>>>>>>> bikin kondisi baru disini >>>>>>>>>>>>>>>>>>>>>>")
        let whereClause = {};
        let whereClauseSegment = {}

        if (year != '' && comparison == '') {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('nationality'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.like]: '%Luxury Escape%' }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear)
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('nationality'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.like]: '%Luxury Escape%' }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else if (year == '' && comparison != '' || year != '' && comparison != '') {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('nationality'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.like]: '%Luxury Escape%' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('nationality'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.like]: '%Luxury Escape%' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('nationality'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.like]: '%Luxury Escape%' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2019,2020,2021')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('nationality'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.like]: '%Luxury Escape%' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2019,2020,2021')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        }

        if (hotel != '') {
            whereClause['room_class'] = hotel;
        }

        whereClause['segment'] = {
            [Op.and]: [
                Sequelize.where(Sequelize.col('marketsegment.segment'), { [Op.eq]: 'OTA' }),
            ]
        };


        if (agent != '') {
            whereClause['agent_name'] = agent;
        }

        const include_option = [
            {
                model: MarketSegment,
                as: 'marketsegment',
                attributes: [],  // just make relation not show column
                required: true,  //left outer join; true : inner join
                where: whereClauseSegment
            },
            {
                model: RoomType,
                as: 'roomtype',
                attributes: [],
                required: true
            }
        ]


        console.log(whereClause, ">>>>>>>>>>>>>>> ISI KONDISI >>>>>>>>>>>>>>>> ")


        try {

            if (year != '' && comparison == '') {
                const result = await Master.findAll({
                    attributes: [
                        [Sequelize.fn('year', Sequelize.fn('str_to_date', Sequelize.col('stay_date'), '%d/%m/%Y')), 'stay_year'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${currYear} THEN REPLACE(rn, ',', '') ELSE 0 END`)), 'XRN_MTD_CURRYEAR'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${currYear} THEN REPLACE(room_rev_usd, ',', '') ELSE 0 END`)), 'XRREV_MTD_CURRYEAR'],
                    ],
                    include: include_option,

                    where: whereClause,
                    group: [
                        Sequelize.fn('year', Sequelize.fn('str_to_date', Sequelize.col('stay_date'), '%d/%m/%Y'))
                    ],
                    order: [
                        [Sequelize.fn('year', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), 'ASC']
                    ],
                });

                // Manipulasi hasil query untuk menghasilkan output yang diinginkan
                const output = result.map(item => {
                    // console.log(XPU23BEFORE23, " <<<<<< SETIAP ITEM")
                    const stay_year = item.dataValues.stay_year;
                    const XRN_MTD_CURR_YEAR = Number(item.dataValues.XRN_MTD_CURRYEAR);
                    const XRREV_MTD_CURR_YEAR = Number(item.dataValues.XRREV_MTD_CURRYEAR);

                    const XARR_MTD_CURR_YEAR = (XRREV_MTD_CURR_YEAR / XRN_MTD_CURR_YEAR).toFixed(2);

                    const dynamicKey = `XRN_MTD_CURR_${stay_year}`;
                    const dynamicKey2 = `XRREV_MTD_CURR_${stay_year}`;
                    const dynamicKey3 = `XARR_MTD_CURR_${stay_year}`;

                    return {
                        stay_year,
                        [dynamicKey]: XRN_MTD_CURR_YEAR,
                        [dynamicKey2]: XRREV_MTD_CURR_YEAR,
                        [dynamicKey3]: XARR_MTD_CURR_YEAR
                    }
                })

                const modifiedData = output.map(item => {
                    const modifiedItem = {};

                    for (let key in item) {
                        const modifiedKey = key.replace(/(CURR_)/g, "");
                        modifiedItem[modifiedKey] = item[key];
                    }

                    return modifiedItem;
                });

                res.status(200).json(modifiedData)
            } else if (year == '' && comparison != '' || year != '' && comparison != '') {
                const result = await Master.findAll({
                    attributes: [
                        [Sequelize.fn('year', Sequelize.fn('str_to_date', Sequelize.col('stay_date'), '%d/%m/%Y')), 'stay_year'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${prevYear} THEN REPLACE(rn, ',', '') ELSE 0 END`)), 'XRN_MTD_PREVYEAR'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${currYear} THEN REPLACE(rn, ',', '') ELSE 0 END`)), 'XRN_MTD_CURRYEAR'],


                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${prevYear} THEN REPLACE(room_rev_usd, ',', '') ELSE 0 END`)), 'XRREV_MTD_PREVYEAR'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${currYear} THEN REPLACE(room_rev_usd, ',', '') ELSE 0 END`)), 'XRREV_MTD_CURRYEAR'],
                    ],
                    include: include_option,

                    where: whereClause,
                    group: Sequelize.literal('YEAR(STR_TO_DATE(stay_date, "%d/%m/%Y"))'),
                    order: Sequelize.literal('YEAR(STR_TO_DATE(stay_date, "%d/%m/%Y")) ASC'),
                });

                // Manipulasi hasil query untuk menghasilkan output yang diinginkan
                const output = result.map(item => {
                    // console.log(XPU23BEFORE23, " <<<<<< SETIAP ITEM")
                    const stay_year = item.dataValues.stay_year;

                    const XRN_MTD_COMP_YEAR = Number(item.dataValues.XRN_MTD_PREVYEAR);
                    const XRN_MTD_CURR_YEAR = Number(item.dataValues.XRN_MTD_CURRYEAR);


                    const XRREV_MTD_COMP_YEAR = Number(item.dataValues.XRREV_MTD_PREVYEAR);
                    const XRREV_MTD_CURR_YEAR = Number(item.dataValues.XRREV_MTD_CURRYEAR);

                    const XARR_MTD_COMP_YEAR = (XRREV_MTD_COMP_YEAR / XRN_MTD_COMP_YEAR).toFixed(2);
                    const XARR_MTD_CURR_YEAR = (XRREV_MTD_CURR_YEAR / XRN_MTD_CURR_YEAR).toFixed(2);

                    const dynamicKey = `XRN_MTD_COMP_${stay_year}`;
                    const dynamicKey2 = `XRREV_MTD_COMP_${stay_year}`;
                    const dynamicKey3 = `XARR_MTD_COMP_${stay_year}`;
                    const dynamicKey4 = `XRN_MTD_CURR_${stay_year}`;
                    const dynamicKey5 = `XRREV_MTD_CURR_${stay_year}`;
                    const dynamicKey6 = `XARR_MTD_CURR_${stay_year}`;

                    return {
                        stay_year,
                        [dynamicKey]: XRN_MTD_COMP_YEAR,
                        [dynamicKey2]: XRREV_MTD_COMP_YEAR,
                        [dynamicKey3]: XARR_MTD_COMP_YEAR,
                        [dynamicKey4]: XRN_MTD_CURR_YEAR,
                        [dynamicKey5]: XRREV_MTD_CURR_YEAR,
                        [dynamicKey6]: XARR_MTD_CURR_YEAR,
                    }

                })

                const filteredOutput = output.map(item => {
                    const filteredItem = { ...item };

                    if (filteredItem.XRN_MTD_COMP_YEAR === 0 || isNaN(parseFloat(filteredItem.XRN_MTD_COMP_YEAR))) {
                        delete filteredItem.XRN_MTD_COMP_YEAR;
                    }

                    if (filteredItem.XRREV_MTD_COMP_YEAR === 0 || isNaN(parseFloat(filteredItem.XRREV_MTD_COMP_YEAR))) {
                        delete filteredItem.XRREV_MTD_COMP_YEAR;
                    }

                    if (filteredItem.XARR_MTD_COMP_YEAR === 'NaN') {
                        delete filteredItem.XARR_MTD_COMP_YEAR;
                    }

                    if (filteredItem.XRN_MTD_CURR_YEAR === 0 || isNaN(parseFloat(filteredItem.XRN_MTD_CURR_YEAR))) {
                        delete filteredItem.XRN_MTD_CURR_YEAR;
                    }

                    if (filteredItem.XRREV_MTD_CURR_YEAR === 0 || isNaN(parseFloat(filteredItem.XRREV_MTD_CURR_YEAR))) {
                        delete filteredItem.XRREV_MTD_CURR_YEAR;
                    }

                    if (filteredItem.XARR_MTD_CURR_YEAR === 'NaN') {
                        delete filteredItem.XARR_MTD_CURR_YEAR;
                    }

                    return filteredItem;
                });

                const modifiedData = filteredOutput.map(item => {
                    const modifiedItem = {};

                    for (let key in item) {
                        const value = item[key];

                        if (value !== "NaN" && value !== 0 && value !== false) {
                            modifiedItem[key] = value;
                        }
                    }

                    return modifiedItem;
                });

                const modifiedData1 = modifiedData.map(item => {
                    const modifiedItem = {};

                    for (let key in item) {
                        const modifiedKey = key.replace(/(COMP_|CURR_)/g, "");
                        modifiedItem[modifiedKey] = item[key];
                    }

                    return modifiedItem;
                });

                res.status(200).json(modifiedData1)
            } else {
                const result = await Master.findAll({
                    attributes: [
                        [Sequelize.fn('year', Sequelize.fn('str_to_date', Sequelize.col('stay_date'), '%d/%m/%Y')), 'stay_year'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${prevYear} THEN REPLACE(rn, ',', '') ELSE 0 END`)), 'XRN_MTD_PREVYEAR'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${currYear} THEN REPLACE(rn, ',', '') ELSE 0 END`)), 'XRN_MTD_CURRYEAR'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${nextYear} THEN REPLACE(rn, ',', '') ELSE 0 END`)), 'XRN_MTD_NEXTYEAR'],


                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${prevYear} THEN REPLACE(room_rev_usd, ',', '') ELSE 0 END`)), 'XRREV_MTD_PREVYEAR'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${currYear} THEN REPLACE(room_rev_usd, ',', '') ELSE 0 END`)), 'XRREV_MTD_CURRYEAR'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${nextYear} THEN REPLACE(room_rev_usd, ',', '') ELSE 0 END`)), 'XRREV_MTD_NEXTYEAR'],
                    ],
                    include: include_option,

                    where: whereClause,
                    group: Sequelize.literal('YEAR(STR_TO_DATE(stay_date, "%d/%m/%Y"))'),
                    order: Sequelize.literal('YEAR(STR_TO_DATE(stay_date, "%d/%m/%Y")) ASC'),
                });

                // Manipulasi hasil query untuk menghasilkan output yang diinginkan
                const output = result.map(item => {
                    // console.log(XPU23BEFORE23, " <<<<<< SETIAP ITEM")
                    const stay_year = item.dataValues.stay_year;
                    const XRN_MTD_PREV_YEAR = Number(item.dataValues.XRN_MTD_PREVYEAR);
                    const XRN_MTD_CURR_YEAR = Number(item.dataValues.XRN_MTD_CURRYEAR);
                    const XRN_MTD_NEXT_YEAR = Number(item.dataValues.XRN_MTD_NEXTYEAR);


                    const XRREV_MTD_PREV_YEAR = Number(item.dataValues.XRREV_MTD_PREVYEAR);
                    const XRREV_MTD_CURR_YEAR = Number(item.dataValues.XRREV_MTD_CURRYEAR);
                    const XRREV_MTD_NEXT_YEAR = Number(item.dataValues.XRREV_MTD_NEXTYEAR);

                    const XARR_MTD_PREV_YEAR = (XRREV_MTD_PREV_YEAR / XRN_MTD_PREV_YEAR).toFixed(2);
                    const XARR_MTD_CURR_YEAR = (XRREV_MTD_CURR_YEAR / XRN_MTD_CURR_YEAR).toFixed(2);
                    const XARR_MTD_NEXT_YEAR = (XRREV_MTD_NEXT_YEAR / XRN_MTD_NEXT_YEAR).toFixed(2);

                    const dynamicKey = `XRN_MTD_PREV_${stay_year}`;
                    const dynamicKey2 = `XRREV_MTD_PREV_${stay_year}`;
                    const dynamicKey3 = `XARR_MTD_PREV_${stay_year}`;
                    const dynamicKey4 = `XRN_MTD_CURR_${stay_year}`;
                    const dynamicKey5 = `XRREV_MTD_CURR_${stay_year}`;
                    const dynamicKey6 = `XARR_MTD_CURR_${stay_year}`;
                    const dynamicKey7 = `XRN_MTD_NEXT_${stay_year}`;
                    const dynamicKey8 = `XRREV_MTD_NEXT_${stay_year}`;
                    const dynamicKey9 = `XARR_MTD_NEXT_${stay_year}`;

                    return {
                        stay_year,
                        [dynamicKey]: XRN_MTD_PREV_YEAR,
                        [dynamicKey2]: XRREV_MTD_PREV_YEAR,
                        [dynamicKey3]: XARR_MTD_PREV_YEAR,
                        [dynamicKey4]: XRN_MTD_CURR_YEAR,
                        [dynamicKey5]: XRREV_MTD_CURR_YEAR,
                        [dynamicKey6]: XARR_MTD_CURR_YEAR,
                        [dynamicKey7]: XRN_MTD_NEXT_YEAR,
                        [dynamicKey8]: XRREV_MTD_NEXT_YEAR,
                        [dynamicKey9]: XARR_MTD_NEXT_YEAR,
                    }

                })

                const filteredOutput = output.map(item => {
                    const filteredItem = { ...item };

                    if (filteredItem.XRN_MTD_PREV_YEAR === 0 || isNaN(parseFloat(filteredItem.XRN_MTD_PREV_YEAR))) {
                        delete filteredItem.XRN_MTD_PREV_YEAR;
                    }

                    if (filteredItem.XRREV_MTD_PREV_YEAR === 0 || isNaN(parseFloat(filteredItem.XRREV_MTD_PREV_YEAR))) {
                        delete filteredItem.XRREV_MTD_PREV_YEAR;
                    }

                    if (filteredItem.XARR_MTD_PREV_YEAR === 'NaN') {
                        delete filteredItem.XARR_MTD_PREV_YEAR;
                    }

                    if (filteredItem.XRN_MTD_CURR_YEAR === 0 || isNaN(parseFloat(filteredItem.XRN_MTD_CURR_YEAR))) {
                        delete filteredItem.XRN_MTD_CURR_YEAR;
                    }

                    if (filteredItem.XRREV_MTD_CURR_YEAR === 0 || isNaN(parseFloat(filteredItem.XRREV_MTD_CURR_YEAR))) {
                        delete filteredItem.XRREV_MTD_CURR_YEAR;
                    }

                    if (filteredItem.XARR_MTD_CURR_YEAR === 'NaN') {
                        delete filteredItem.XARR_MTD_CURR_YEAR;
                    }

                    if (filteredItem.XRN_MTD_NEXT_YEAR === 0 || isNaN(parseFloat(filteredItem.XRN_MTD_NEXT_YEAR))) {
                        delete filteredItem.XRN_MTD_NEXT_YEAR;
                    }

                    if (filteredItem.XRREV_MTD_NEXT_YEAR === 0 || isNaN(parseFloat(filteredItem.XRREV_MTD_NEXT_YEAR))) {
                        delete filteredItem.XRREV_MTD_NEXT_YEAR;
                    }


                    if (filteredItem.XARR_MTD_NEXT_YEAR === 'NaN') {
                        delete filteredItem.XARR_MTD_NEXT_YEAR;
                    }

                    return filteredItem;
                });

                const filteredYears = [2019, 2020, 2021]; // Daftar tahun yang ingin Anda filter


                const filteredData = filteredOutput.filter(item => !filteredYears.includes(item.stay_year));

                const modifiedData = filteredData.map(item => {
                    const modifiedItem = {};

                    for (let key in item) {
                        const value = item[key];

                        if (value !== "NaN" && value !== 0 && value !== false) {
                            modifiedItem[key] = value;
                        }
                    }

                    return modifiedItem;
                });

                const modifiedData1 = modifiedData.map(item => {
                    const modifiedItem = {};

                    for (let key in item) {
                        const modifiedKey = key.replace(/(PREV_|CURR_|NEXT_)/g, "");
                        modifiedItem[modifiedKey] = item[key];
                    }

                    return modifiedItem;
                });

                res.status(200).json(modifiedData1)
            }

        } catch (error) {
            console.log(error);
        }
    };

    // ---------------------------------------------------
    // OTA EXCL LE MTD DAILY ON THE BOOK
    // ---------------------------------------------------
    static async getOtaExLeMtdDailyBooking(req, res) {
        const { hotel, year, comparison, month, agent } = req.query;

        var currentTime = new Date();
        var currYear = year > 0 ? year : currentTime.getFullYear();
        var prevYear = comparison > 0 ? Number(comparison) : currentTime.getFullYear() - 1;
        var nextYear = currentTime.getFullYear() + 1

        // list 1 years before current year
        const startCurrYear = currYear - 1;
        const curryearList = [];

        for (let tahun = startCurrYear; tahun <= currYear; tahun++) {
            if (tahun != 2020 && tahun != 2021) {
                curryearList.push(tahun);
            }
        }

        curryearList.push(nextYear)

        const yearsBeforeCurrYear = '[' + curryearList.join(', ') + ']'

        console.log(">>>>>>>>>>>>>>>> bikin kondisi baru disini >>>>>>>>>>>>>>>>>>>>>>")
        let whereClause = {};
        let whereClauseSegment = {}

        if (year != '' && comparison == '') {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.notLike]: '%Luxury Escape%' }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear)

                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.notLike]: '%Luxury Escape%' }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.notLike]: '%Luxury Escape%' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.notLike]: '%Luxury Escape%' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.notLike]: '%Luxury Escape%' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.notLike]: '%Luxury Escape%' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        }


        if (hotel != '') {
            whereClause['room_class'] = hotel;
        }

        whereClause['segment'] = {
            [Op.and]: [
                Sequelize.where(Sequelize.col('marketsegment.segment'), { [Op.eq]: 'OTA' }),
            ]
        };


        if (agent != '') {
            whereClause['agent_name'] = agent;
        }


        const include_option = [
            {
                model: MarketSegment,
                as: 'marketsegment',
                attributes: [],  // just make relation not show column
                required: false,  //left outer join; true : inner join
                where: whereClauseSegment
            },
        ]

        const attributes = [];
        const years = JSON.parse(yearsBeforeCurrYear);

        for (let i = 0; i < years.length; i++) {
            const year = years[i];

            const attribute = [
                Sequelize.fn(
                    'SUM',
                    Sequelize.literal(
                        "CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " +
                        year +
                        ' THEN `rn` ELSE 0 END'
                    )
                ),
                'room_night_' + year,
            ];

            attributes.push(attribute);
        }

        console.log(whereClause, ">>>>>>>>>>>>>>> ISI KONDISI >>>>>>>>>>>>>>>> ")


        if (year != '' && comparison == '') {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        [Sequelize.literal("RIGHT(STR_TO_DATE(stay_date, '%d/%m/%Y'), 2)"), "day"],
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'room_night'],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        Sequelize.literal("RIGHT(STR_TO_DATE(stay_date, '%d/%m/%Y'), 2)"),
                    ],
                    order: [
                        [Sequelize.literal("RIGHT(STR_TO_DATE(stay_date, '%d/%m/%Y'), 2)"), 'ASC'],
                    ],
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        [Sequelize.literal("RIGHT(STR_TO_DATE(stay_date, '%d/%m/%Y'), 2)"), "day"],
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + prevYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + prevYear,
                        ],
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + currYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + currYear,
                        ],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        Sequelize.literal("RIGHT(STR_TO_DATE(stay_date, '%d/%m/%Y'), 2)"),
                    ],
                    order: [
                        [Sequelize.literal("RIGHT(STR_TO_DATE(stay_date, '%d/%m/%Y'), 2)"), 'ASC'],
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }

        } else {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        [Sequelize.literal("RIGHT(STR_TO_DATE(stay_date, '%d/%m/%Y'), 2)"), "day"],
                        ...attributes,

                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        Sequelize.literal("RIGHT(STR_TO_DATE(stay_date, '%d/%m/%Y'), 2)"),
                    ],
                    order: [
                        [Sequelize.literal("RIGHT(STR_TO_DATE(stay_date, '%d/%m/%Y'), 2)"), 'ASC'],
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        }
    };


    // ---------------------------------------------------
    // OTA EXCL LE YTD DAILY ON THE BOOK
    // ---------------------------------------------------
    static async getOtaExLeDailyBooking(req, res) {
        const { hotel, year, comparison, month, agent } = req.query;

        var currentTime = new Date();
        var currYear = year > 0 ? year : currentTime.getFullYear();
        var prevYear = comparison > 0 ? Number(comparison) : currentTime.getFullYear() - 1;
        var nextYear = currentTime.getFullYear() + 1

        // list 1 years before current year
        const startCurrYear = currYear - 1;
        const curryearList = [];

        for (let tahun = startCurrYear; tahun <= currYear; tahun++) {
            if (tahun != 2020 && tahun != 2021) {
                curryearList.push(tahun);
            }
        }

        curryearList.push(nextYear)

        const yearsBeforeCurrYear = '[' + curryearList.join(', ') + ']'

        console.log(">>>>>>>>>>>>>>>> bikin kondisi baru disini >>>>>>>>>>>>>>>>>>>>>>")
        let whereClause = {};
        let whereClauseSegment = {}

        if (year != '' && comparison == '') {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.notLike]: '%Luxury Escape%' }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear)
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.notLike]: '%Luxury Escape%' }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.notLike]: '%Luxury Escape%' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.notLike]: '%Luxury Escape%' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.notLike]: '%Luxury Escape%' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.notLike]: '%Luxury Escape%' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        }


        if (hotel != '') {
            whereClause['room_class'] = hotel;
        }

        whereClause['segment'] = {
            [Op.and]: [
                Sequelize.where(Sequelize.col('marketsegment.segment'), { [Op.eq]: 'OTA' }),
            ]
        };


        if (agent != '') {
            whereClause['agent_name'] = agent;
        }


        const include_option = [
            {
                model: MarketSegment,
                as: 'marketsegment',
                attributes: [],  // just make relation not show column
                required: false,  //left outer join; true : inner join
                where: whereClauseSegment
            },
        ]

        const attributes = [];
        const years = JSON.parse(yearsBeforeCurrYear);

        for (let i = 0; i < years.length; i++) {
            const year = years[i];

            const attribute = [
                Sequelize.fn(
                    'SUM',
                    Sequelize.literal(
                        "CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " +
                        year +
                        ' THEN `rn` ELSE 0 END'
                    )
                ),
                'room_night_' + year,
            ];

            attributes.push(attribute);
        }

        console.log(whereClause, ">>>>>>>>>>>>>>> ISI KONDISI >>>>>>>>>>>>>>>> ")


        if (year != '' && comparison == '') {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        [Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), 'stay_month'],
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'room_night'],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')),
                    ],
                    order: [
                        [Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), 'ASC'],
                    ],
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        [Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), 'stay_month'],
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + prevYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + prevYear,
                        ],
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + currYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + currYear,
                        ],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')),
                    ],
                    order: [
                        [Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), 'ASC'],
                    ],
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        [Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), 'stay_month'],
                        ...attributes,
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')),
                    ],
                    order: [
                        [Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), 'ASC'],
                    ],
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        }
    };

    // ---------------------------------------------------
    // OTA EXCL LE TOP COUNTRIES
    // ---------------------------------------------------
    static async getOtaExLeTopCountry(req, res) {
        const { hotel, year, comparison, month, agent } = req.query;

        var currentTime = new Date();
        var currYear = year > 0 ? year : currentTime.getFullYear();
        var prevYear = comparison > 0 ? Number(comparison) : currentTime.getFullYear() - 1;
        var nextYear = currentTime.getFullYear() + 1

        // list 1 years before current year
        const startCurrYear = currYear - 1;
        const curryearList = [];

        for (let tahun = startCurrYear; tahun <= currYear; tahun++) {
            if (tahun != 2020 && tahun != 2021) {
                curryearList.push(tahun);
            }
        }

        curryearList.push(nextYear)

        const yearsBeforeCurrYear = '[' + curryearList.join(', ') + ']'

        console.log(">>>>>>>>>>>>>>>> bikin kondisi baru disini >>>>>>>>>>>>>>>>>>>>>>")
        let whereClause = {};
        let whereClauseSegment = {}

        if (year != '' && comparison == '') {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('country'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.notLike]: '%Luxury Escape%' }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), year)
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('country'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.notLike]: '%Luxury Escape%' }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), year),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('country'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.notLike]: '%Luxury Escape%' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('country'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.notLike]: '%Luxury Escape%' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('country'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.notLike]: '%Luxury Escape%' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('country'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.notLike]: '%Luxury Escape%' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        }


        if (hotel != '') {
            whereClause['room_class'] = hotel;
        }

        whereClause['segment'] = {
            [Op.and]: [
                Sequelize.where(Sequelize.col('marketsegment.segment'), { [Op.eq]: 'OTA' }),
            ]
        };


        if (agent != '') {
            whereClause['agent_name'] = agent;
        }

        const include_option = [
            {
                model: MarketSegment,
                as: 'marketsegment',
                attributes: [],  // just make relation not show column
                required: false,  //left outer join; true : inner join
                where: whereClauseSegment
            },
        ]

        const attributes = [];
        const years = JSON.parse(yearsBeforeCurrYear);

        for (let i = 0; i < years.length; i++) {
            const year = years[i];

            const attribute = [
                Sequelize.fn(
                    'SUM',
                    Sequelize.literal(
                        "CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " +
                        year +
                        ' THEN `rn` ELSE 0 END'
                    )
                ),
                'room_night_' + year,
            ];

            attributes.push(attribute);
        }

        console.log(whereClause, ">>>>>>>>>>>>>>> ISI KONDISI >>>>>>>>>>>>>>>> ")


        if (year != '' && comparison == '') {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'country',
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'room_night'],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: ["country"],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'country',
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + prevYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + prevYear,
                        ],
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + currYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + currYear,
                        ],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: ["country"],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'country',
                        ...attributes,
                    ],
                    include: include_option,
                    where: whereClause,
                    group: ["country"],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        }
    };

    // ---------------------------------------------------
    // OTA EXCL LE TOP LENGTH OF STAY
    // ---------------------------------------------------
    static async getOtaExLeTopLengthStay(req, res) {
        const { hotel, year, comparison, month, agent } = req.query;

        var currentTime = new Date();
        var currYear = year > 0 ? year : currentTime.getFullYear();
        var prevYear = comparison > 0 ? Number(comparison) : currentTime.getFullYear() - 1;
        var nextYear = currentTime.getFullYear() + 1

        // list 1 years before current year
        const startCurrYear = currYear - 1;
        const curryearList = [];

        for (let tahun = startCurrYear; tahun <= currYear; tahun++) {
            if (tahun != 2020 && tahun != 2021) {
                curryearList.push(tahun);
            }
        }

        curryearList.push(nextYear)

        const yearsBeforeCurrYear = '[' + curryearList.join(', ') + ']'

        console.log(">>>>>>>>>>>>>>>> bikin kondisi baru disini >>>>>>>>>>>>>>>>>>>>>>")
        let whereClause = {};
        let whereClauseSegment = {}

        if (year != '' && comparison == '') {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('los'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.notLike]: '%Luxury Escape%' }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear)
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('los'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.notLike]: '%Luxury Escape%' }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('los'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.notLike]: '%Luxury Escape%' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('los'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.notLike]: '%Luxury Escape%' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('los'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.notLike]: '%Luxury Escape%' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('los'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.notLike]: '%Luxury Escape%' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        }


        if (hotel != '') {
            whereClause['room_class'] = hotel;
        }

        whereClause['segment'] = {
            [Op.and]: [
                Sequelize.where(Sequelize.col('marketsegment.segment'), { [Op.eq]: 'OTA' }),
            ]
        };


        if (agent != '') {
            whereClause['agent_name'] = agent;
        }

        const include_option = [
            {
                model: MarketSegment,
                as: 'marketsegment',
                attributes: [],  // just make relation not show column
                required: false,  //left outer join; true : inner join
                where: whereClauseSegment
            },
        ]

        const attributes = [];
        const years = JSON.parse(yearsBeforeCurrYear);

        for (let i = 0; i < years.length; i++) {
            const year = years[i];

            const attribute = [
                Sequelize.fn(
                    'SUM',
                    Sequelize.literal(
                        "CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " +
                        year +
                        ' THEN `rn` ELSE 0 END'
                    )
                ),
                'room_night_' + year,
            ];

            attributes.push(attribute);
        }

        console.log(whereClause, ">>>>>>>>>>>>>>> ISI KONDISI >>>>>>>>>>>>>>>> ")


        if (year != '' && comparison == '') {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'los',
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'room_night'],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: ["los"],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'los',
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + prevYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + prevYear,
                        ],
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + currYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + currYear,
                        ],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: ["los"],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'los',
                        ...attributes,
                    ],
                    include: include_option,
                    where: whereClause,
                    group: ["los"],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        }
    };

    // ---------------------------------------------------
    // OTA EXCL LE TOP BOOKING LEAD TIME
    // ---------------------------------------------------
    static async getOtaExLeTopBookingLeadTime(req, res) {
        const { hotel, year, comparison, month, agent } = req.query;

        var currentTime = new Date();
        var currYear = year > 0 ? year : currentTime.getFullYear();
        var prevYear = comparison > 0 ? Number(comparison) : currentTime.getFullYear() - 1;
        var nextYear = currentTime.getFullYear() + 1

        // list 1 years before current year
        const startCurrYear = currYear - 1;
        const curryearList = [];

        for (let tahun = startCurrYear; tahun <= currYear; tahun++) {
            if (tahun != 2020 && tahun != 2021) {
                curryearList.push(tahun);
            }
        }

        curryearList.push(nextYear)

        const yearsBeforeCurrYear = '[' + curryearList.join(', ') + ']'

        console.log(">>>>>>>>>>>>>>>> bikin kondisi baru disini >>>>>>>>>>>>>>>>>>>>>>")
        let whereClause = {};
        let whereClauseSegment = {}

        if (year != '' && comparison == '') {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('lead_days'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.notLike]: '%Luxury Escape%' }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear)
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('lead_days'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.notLike]: '%Luxury Escape%' }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('lead_days'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.notLike]: '%Luxury Escape%' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('lead_days'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.notLike]: '%Luxury Escape%' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('lead_days'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.notLike]: '%Luxury Escape%' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('lead_days'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.notLike]: '%Luxury Escape%' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        }


        if (hotel != '') {
            whereClause['room_class'] = hotel;
        }

        whereClause['segment'] = {
            [Op.and]: [
                Sequelize.where(Sequelize.col('marketsegment.segment'), { [Op.eq]: 'OTA' }),
            ]
        };


        if (agent != '') {
            whereClause['agent_name'] = agent;
        }

        const include_option = [
            {
                model: MarketSegment,
                as: 'marketsegment',
                attributes: [],  // just make relation not show column
                required: false,  //left outer join; true : inner join
                where: whereClauseSegment
            },
        ]

        const attributes = [];
        const years = JSON.parse(yearsBeforeCurrYear);

        for (let i = 0; i < years.length; i++) {
            const year = years[i];

            const attribute = [
                Sequelize.fn(
                    'SUM',
                    Sequelize.literal(
                        "CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " +
                        year +
                        ' THEN `rn` ELSE 0 END'
                    )
                ),
                'room_night_' + year,
            ];

            attributes.push(attribute);
        }

        console.log(whereClause, ">>>>>>>>>>>>>>> ISI KONDISI >>>>>>>>>>>>>>>> ")


        if (year != '' && comparison == '') {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'lead_days',
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'room_night'],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: ["lead_days"],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'lead_days',
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + prevYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + prevYear,
                        ],
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + currYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + currYear,
                        ],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: ["lead_days"],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'lead_days',
                        ...attributes,
                    ],
                    include: include_option,
                    where: whereClause,
                    group: ["lead_days"],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        }
    };

    // ---------------------------------------------------
    // OTA EXCL LE TOP ROOM TYPES
    // ---------------------------------------------------
    static async getOtaExLeTopRoomType(req, res) {
        const { hotel, year, comparison, month, agent } = req.query;

        var currentTime = new Date();
        var currYear = year > 0 ? year : currentTime.getFullYear();
        var prevYear = comparison > 0 ? Number(comparison) : currentTime.getFullYear() - 1;
        var nextYear = currentTime.getFullYear() + 1

        // list 1 years before current year
        const startCurrYear = currYear - 1;
        const curryearList = [];

        for (let tahun = startCurrYear; tahun <= currYear; tahun++) {
            if (tahun != 2020 && tahun != 2021) {
                curryearList.push(tahun);
            }
        }

        curryearList.push(nextYear)

        const yearsBeforeCurrYear = '[' + curryearList.join(', ') + ']'

        console.log(">>>>>>>>>>>>>>>> bikin kondisi baru disini >>>>>>>>>>>>>>>>>>>>>>")
        let whereClause = {};
        let whereClauseSegment = {}

        if (year != '' && comparison == '') {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.notLike]: '%Luxury Escape%' }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear)
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.notLike]: '%Luxury Escape%' }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.notLike]: '%Luxury Escape%' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.notLike]: '%Luxury Escape%' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.notLike]: '%Luxury Escape%' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.notLike]: '%Luxury Escape%' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        }

        if (hotel != '') {
            whereClause['room_class'] = hotel;
        }

        whereClause['segment'] = {
            [Op.and]: [
                Sequelize.where(Sequelize.col('marketsegment.segment'), { [Op.eq]: 'OTA' }),
            ]
        };


        if (agent != '') {
            whereClause['agent_name'] = agent;
        }

        const include_option = [
            {
                model: MarketSegment,
                as: 'marketsegment',
                attributes: [],  // just make relation not show column
                required: true,  //left outer join; true : inner join
                where: whereClauseSegment
            },
            {
                model: RoomType,
                as: 'roomtype',
                attributes: [],
                required: true
            }
        ]

        const attributes = [];
        const years = JSON.parse(yearsBeforeCurrYear);

        for (let i = 0; i < years.length; i++) {
            const year = years[i];

            const attribute = [
                Sequelize.fn(
                    'SUM',
                    Sequelize.literal(
                        "CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " +
                        year +
                        ' THEN `rn` ELSE 0 END'
                    )
                ),
                'room_night_' + year,
            ];

            attributes.push(attribute);
        }

        console.log(whereClause, ">>>>>>>>>>>>>>> ISI KONDISI >>>>>>>>>>>>>>>> ")


        if (year != '' && comparison == '') {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'roomtype.room_type',
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'room_night'],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: ['roomtype.room_type'],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'roomtype.room_type',
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + prevYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + prevYear,
                        ],
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + currYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + currYear,
                        ],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        'roomtype.room_type',
                    ],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'roomtype.room_type',
                        ...attributes,
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        'roomtype.room_type',
                    ],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        }
    };

    // ---------------------------------------------------
    // OTA EXCL LE TOP SUPPORTERS
    // ---------------------------------------------------
    static async getOtaExLeTopSupporter(req, res) {
        const { hotel, year, comparison, month, agent } = req.query;

        var currentTime = new Date();
        var currYear = year > 0 ? year : currentTime.getFullYear();
        var prevYear = comparison > 0 ? Number(comparison) : currentTime.getFullYear() - 1;
        var nextYear = currentTime.getFullYear() + 1

        // list 1 years before current year
        const startCurrYear = currYear - 1;
        const curryearList = [];

        for (let tahun = startCurrYear; tahun <= currYear; tahun++) {
            if (tahun != 2020 && tahun != 2021) {
                curryearList.push(tahun);
            }
        }

        curryearList.push(nextYear)

        const yearsBeforeCurrYear = '[' + curryearList.join(', ') + ']'

        console.log(">>>>>>>>>>>>>>>> bikin kondisi baru disini >>>>>>>>>>>>>>>>>>>>>>")
        let whereClause = {};
        let whereClauseSegment = {}

        if (year != '' && comparison == '') {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.notLike]: '%Luxury Escape%' }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), year)
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.notLike]: '%Luxury Escape%' }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), year),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.notLike]: '%Luxury Escape%' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.notLike]: '%Luxury Escape%' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.notLike]: '%Luxury Escape%' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.notLike]: '%Luxury Escape%' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        }

        if (hotel != '') {
            whereClause['room_class'] = hotel;
        }

        whereClause['segment'] = {
            [Op.and]: [
                Sequelize.where(Sequelize.col('marketsegment.segment'), { [Op.eq]: 'OTA' }),
            ]
        };


        if (agent != '') {
            whereClause['agent_name'] = agent;
        }

        const include_option = [
            {
                model: MarketSegment,
                as: 'marketsegment',
                attributes: [],  // just make relation not show column
                required: true,  //left outer join; true : inner join
                where: whereClauseSegment
            },
            {
                model: RoomType,
                as: 'roomtype',
                attributes: [],
                required: true
            }
        ]

        const attributes = [];
        const years = JSON.parse(yearsBeforeCurrYear);

        for (let i = 0; i < years.length; i++) {
            const year = years[i];

            const attribute = [
                Sequelize.fn(
                    'SUM',
                    Sequelize.literal(
                        "CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " +
                        year +
                        ' THEN `rn` ELSE 0 END'
                    )
                ),
                'room_night_' + year,
            ];

            attributes.push(attribute);
        }

        console.log(whereClause, ">>>>>>>>>>>>>>> ISI KONDISI >>>>>>>>>>>>>>>> ")


        if (year != '' && comparison == '') {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'agent_name',
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'room_night'],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: ['agent_name'],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'agent_name',
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + prevYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + prevYear,
                        ],
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + currYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + currYear,
                        ],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        'agent_name',
                    ],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'agent_name',
                        ...attributes,
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        'agent_name',
                    ],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        }
    };

    // ---------------------------------------------------
    // OTA EXCL LE SUMMARY
    // ---------------------------------------------------
    static async getOtaExLeSummary(req, res) {

        const { hotel, year, comparison, month, agent } = req.query;

        var currentTime = new Date();
        var currYear = year > 0 ? year : currentTime.getFullYear();
        var prevYear = comparison > 0 ? Number(comparison) : currentTime.getFullYear() - 1;
        var nextYear = currentTime.getFullYear() + 1

        // list 1 years before current year
        const startCurrYear = currYear - 1;
        const curryearList = [];

        for (let tahun = startCurrYear; tahun <= currYear; tahun++) {
            if (tahun != 2020 && tahun != 2021) {
                curryearList.push(tahun);
            }
        }

        curryearList.push(nextYear)

        const yearsBeforeCurrYear = '[' + curryearList.join(', ') + ']'

        console.log(">>>>>>>>>>>>>>>> bikin kondisi baru disini >>>>>>>>>>>>>>>>>>>>>>")
        let whereClause = {};
        let whereClauseSegment = {}

        if (year != '' && comparison == '') {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.notLike]: '%Luxury Escape%' }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear)
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.notLike]: '%Luxury Escape%' }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else if (year == '' && comparison != '' || year != '' && comparison != '') {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.notLike]: '%Luxury Escape%' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.notLike]: '%Luxury Escape%' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.notLike]: '%Luxury Escape%' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2019,2020,2021')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.notLike]: '%Luxury Escape%' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2019,2020,2021')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        }

        if (hotel != '') {
            whereClause['room_class'] = hotel;
        }

        whereClause['segment'] = {
            [Op.and]: [
                Sequelize.where(Sequelize.col('marketsegment.segment'), { [Op.eq]: 'OTA' }),
            ]
        };


        if (agent != '') {
            whereClause['agent_name'] = agent;
        }

        const include_option = [
            {
                model: MarketSegment,
                as: 'marketsegment',
                attributes: [],  // just make relation not show column
                required: true,  //left outer join; true : inner join
                where: whereClauseSegment
            },
            {
                model: RoomType,
                as: 'roomtype',
                attributes: [],
                required: true
            }
        ]


        console.log(whereClause, ">>>>>>>>>>>>>>> ISI KONDISI >>>>>>>>>>>>>>>> ")


        try {

            if (year != '' && comparison == '') {

                const result = await Master.findAll({
                    attributes: [
                        [Sequelize.fn('year', Sequelize.fn('str_to_date', Sequelize.col('stay_date'), '%d/%m/%Y')), 'stay_year'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${currYear} THEN REPLACE(rn, ',', '') ELSE 0 END`)), 'XRN_MTD_CURRYEAR'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${currYear} THEN REPLACE(room_rev_usd, ',', '') ELSE 0 END`)), 'XRREV_MTD_CURRYEAR'],
                    ],
                    include: include_option,

                    where: whereClause,
                    group: [
                        Sequelize.fn('year', Sequelize.fn('str_to_date', Sequelize.col('stay_date'), '%d/%m/%Y'))
                    ],
                    order: [
                        [Sequelize.fn('year', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), 'ASC']
                    ],
                });

                // Manipulasi hasil query untuk menghasilkan output yang diinginkan
                const output = result.map(item => {
                    // console.log(XPU23BEFORE23, " <<<<<< SETIAP ITEM")
                    const stay_year = item.dataValues.stay_year;
                    const XRN_MTD_CURR_YEAR = Number(item.dataValues.XRN_MTD_CURRYEAR);
                    const XRREV_MTD_CURR_YEAR = Number(item.dataValues.XRREV_MTD_CURRYEAR);

                    const XARR_MTD_CURR_YEAR = (XRREV_MTD_CURR_YEAR / XRN_MTD_CURR_YEAR).toFixed(2);

                    const dynamicKey = `XRN_MTD_CURR_${stay_year}`;
                    const dynamicKey2 = `XRREV_MTD_CURR_${stay_year}`;
                    const dynamicKey3 = `XARR_MTD_CURR_${stay_year}`;

                    return {
                        stay_year,
                        [dynamicKey]: XRN_MTD_CURR_YEAR,
                        [dynamicKey2]: XRREV_MTD_CURR_YEAR,
                        [dynamicKey3]: XARR_MTD_CURR_YEAR
                    }
                })

                const modifiedData = output.map(item => {
                    const modifiedItem = {};

                    for (let key in item) {
                        const modifiedKey = key.replace(/(CURR_)/g, "");
                        modifiedItem[modifiedKey] = item[key];
                    }

                    return modifiedItem;
                });

                res.status(200).json(modifiedData)
            } else if (year == '' && comparison != '' || year != '' && comparison != '') {
                const result = await Master.findAll({
                    attributes: [
                        [Sequelize.fn('year', Sequelize.fn('str_to_date', Sequelize.col('stay_date'), '%d/%m/%Y')), 'stay_year'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${prevYear} THEN REPLACE(rn, ',', '') ELSE 0 END`)), 'XRN_MTD_PREVYEAR'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${currYear} THEN REPLACE(rn, ',', '') ELSE 0 END`)), 'XRN_MTD_CURRYEAR'],


                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${prevYear} THEN REPLACE(room_rev_usd, ',', '') ELSE 0 END`)), 'XRREV_MTD_PREVYEAR'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${currYear} THEN REPLACE(room_rev_usd, ',', '') ELSE 0 END`)), 'XRREV_MTD_CURRYEAR'],
                    ],
                    include: include_option,

                    where: whereClause,
                    group: Sequelize.literal('YEAR(STR_TO_DATE(stay_date, "%d/%m/%Y"))'),
                    order: Sequelize.literal('YEAR(STR_TO_DATE(stay_date, "%d/%m/%Y")) ASC'),
                });

                // Manipulasi hasil query untuk menghasilkan output yang diinginkan
                const output = result.map(item => {
                    // console.log(XPU23BEFORE23, " <<<<<< SETIAP ITEM")
                    const stay_year = item.dataValues.stay_year;
                    const XRN_MTD_COMP_YEAR = Number(item.dataValues.XRN_MTD_PREVYEAR);
                    const XRN_MTD_CURR_YEAR = Number(item.dataValues.XRN_MTD_CURRYEAR);


                    const XRREV_MTD_COMP_YEAR = Number(item.dataValues.XRREV_MTD_PREVYEAR);
                    const XRREV_MTD_CURR_YEAR = Number(item.dataValues.XRREV_MTD_CURRYEAR);

                    const XARR_MTD_COMP_YEAR = (XRREV_MTD_COMP_YEAR / XRN_MTD_COMP_YEAR).toFixed(2);
                    const XARR_MTD_CURR_YEAR = (XRREV_MTD_CURR_YEAR / XRN_MTD_CURR_YEAR).toFixed(2);

                    const dynamicKey = `XRN_MTD_COMP_${stay_year}`;
                    const dynamicKey2 = `XRREV_MTD_COMP_${stay_year}`;
                    const dynamicKey3 = `XARR_MTD_COMP_${stay_year}`;
                    const dynamicKey4 = `XRN_MTD_CURR_${stay_year}`;
                    const dynamicKey5 = `XRREV_MTD_CURR_${stay_year}`;
                    const dynamicKey6 = `XARR_MTD_CURR_${stay_year}`;

                    return {
                        stay_year,
                        [dynamicKey]: XRN_MTD_COMP_YEAR,
                        [dynamicKey2]: XRREV_MTD_COMP_YEAR,
                        [dynamicKey3]: XARR_MTD_COMP_YEAR,
                        [dynamicKey4]: XRN_MTD_CURR_YEAR,
                        [dynamicKey5]: XRREV_MTD_CURR_YEAR,
                        [dynamicKey6]: XARR_MTD_CURR_YEAR,
                    }

                })

                const filteredOutput = output.map(item => {
                    const filteredItem = { ...item };

                    if (filteredItem.XRN_MTD_COMP_YEAR === 0 || isNaN(parseFloat(filteredItem.XRN_MTD_COMP_YEAR))) {
                        delete filteredItem.XRN_MTD_COMP_YEAR;
                    }

                    if (filteredItem.XRREV_MTD_COMP_YEAR === 0 || isNaN(parseFloat(filteredItem.XRREV_MTD_COMP_YEAR))) {
                        delete filteredItem.XRREV_MTD_COMP_YEAR;
                    }

                    if (filteredItem.XARR_MTD_COMP_YEAR === 'NaN') {
                        delete filteredItem.XARR_MTD_COMP_YEAR;
                    }

                    if (filteredItem.XRN_MTD_CURR_YEAR === 0 || isNaN(parseFloat(filteredItem.XRN_MTD_CURR_YEAR))) {
                        delete filteredItem.XRN_MTD_CURR_YEAR;
                    }

                    if (filteredItem.XRREV_MTD_CURR_YEAR === 0 || isNaN(parseFloat(filteredItem.XRREV_MTD_CURR_YEAR))) {
                        delete filteredItem.XRREV_MTD_CURR_YEAR;
                    }

                    if (filteredItem.XARR_MTD_CURR_YEAR === 'NaN') {
                        delete filteredItem.XARR_MTD_CURR_YEAR;
                    }

                    return filteredItem;
                });

                const modifiedData = filteredOutput.map(item => {
                    const modifiedItem = {};

                    for (let key in item) {
                        const value = item[key];

                        if (value !== "NaN" && value !== 0 && value !== false) {
                            modifiedItem[key] = value;
                        }
                    }

                    return modifiedItem;
                });

                const modifiedData1 = modifiedData.map(item => {
                    const modifiedItem = {};

                    for (let key in item) {
                        const modifiedKey = key.replace(/(COMP_|CURR_)/g, "");
                        modifiedItem[modifiedKey] = item[key];
                    }

                    return modifiedItem;
                });

                res.status(200).json(modifiedData1)

            } else {
                const result = await Master.findAll({
                    attributes: [
                        [Sequelize.fn('year', Sequelize.fn('str_to_date', Sequelize.col('stay_date'), '%d/%m/%Y')), 'stay_year'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${prevYear} THEN REPLACE(rn, ',', '') ELSE 0 END`)), 'XRN_MTD_PREVYEAR'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${currYear} THEN REPLACE(rn, ',', '') ELSE 0 END`)), 'XRN_MTD_CURRYEAR'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${nextYear} THEN REPLACE(rn, ',', '') ELSE 0 END`)), 'XRN_MTD_NEXTYEAR'],


                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${prevYear} THEN REPLACE(room_rev_usd, ',', '') ELSE 0 END`)), 'XRREV_MTD_PREVYEAR'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${currYear} THEN REPLACE(room_rev_usd, ',', '') ELSE 0 END`)), 'XRREV_MTD_CURRYEAR'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${nextYear} THEN REPLACE(room_rev_usd, ',', '') ELSE 0 END`)), 'XRREV_MTD_NEXTYEAR'],
                    ],
                    include: include_option,

                    where: whereClause,
                    group: Sequelize.literal('YEAR(STR_TO_DATE(stay_date, "%d/%m/%Y"))'),
                    order: Sequelize.literal('YEAR(STR_TO_DATE(stay_date, "%d/%m/%Y")) ASC'),
                });

                // Manipulasi hasil query untuk menghasilkan output yang diinginkan
                const output = result.map(item => {
                    // console.log(XPU23BEFORE23, " <<<<<< SETIAP ITEM")
                    const stay_year = item.dataValues.stay_year;
                    const XRN_MTD_PREV_YEAR = Number(item.dataValues.XRN_MTD_PREVYEAR);
                    const XRN_MTD_CURR_YEAR = Number(item.dataValues.XRN_MTD_CURRYEAR);
                    const XRN_MTD_NEXT_YEAR = Number(item.dataValues.XRN_MTD_NEXTYEAR);


                    const XRREV_MTD_PREV_YEAR = Number(item.dataValues.XRREV_MTD_PREVYEAR);
                    const XRREV_MTD_CURR_YEAR = Number(item.dataValues.XRREV_MTD_CURRYEAR);
                    const XRREV_MTD_NEXT_YEAR = Number(item.dataValues.XRREV_MTD_NEXTYEAR);

                    const XARR_MTD_PREV_YEAR = (XRREV_MTD_PREV_YEAR / XRN_MTD_PREV_YEAR).toFixed(2);
                    const XARR_MTD_CURR_YEAR = (XRREV_MTD_CURR_YEAR / XRN_MTD_CURR_YEAR).toFixed(2);
                    const XARR_MTD_NEXT_YEAR = (XRREV_MTD_NEXT_YEAR / XRN_MTD_NEXT_YEAR).toFixed(2);

                    const dynamicKey = `XRN_MTD_PREV_${stay_year}`;
                    const dynamicKey2 = `XRREV_MTD_PREV_${stay_year}`;
                    const dynamicKey3 = `XARR_MTD_PREV_${stay_year}`;
                    const dynamicKey4 = `XRN_MTD_CURR_${stay_year}`;
                    const dynamicKey5 = `XRREV_MTD_CURR_${stay_year}`;
                    const dynamicKey6 = `XARR_MTD_CURR_${stay_year}`;
                    const dynamicKey7 = `XRN_MTD_NEXT_${stay_year}`;
                    const dynamicKey8 = `XRREV_MTD_NEXT_${stay_year}`;
                    const dynamicKey9 = `XARR_MTD_NEXT_${stay_year}`;


                    return {
                        stay_year,
                        [dynamicKey]: XRN_MTD_PREV_YEAR,
                        [dynamicKey2]: XRREV_MTD_PREV_YEAR,
                        [dynamicKey3]: XARR_MTD_PREV_YEAR,
                        [dynamicKey4]: XRN_MTD_CURR_YEAR,
                        [dynamicKey5]: XRREV_MTD_CURR_YEAR,
                        [dynamicKey6]: XARR_MTD_CURR_YEAR,
                        [dynamicKey7]: XRN_MTD_NEXT_YEAR,
                        [dynamicKey8]: XRREV_MTD_NEXT_YEAR,
                        [dynamicKey9]: XARR_MTD_NEXT_YEAR,
                    }

                })

                const filteredOutput = output.map(item => {
                    const filteredItem = { ...item };

                    if (filteredItem.XRN_MTD_PREV_YEAR === 0 || isNaN(parseFloat(filteredItem.XRN_MTD_PREV_YEAR))) {
                        delete filteredItem.XRN_MTD_PREV_YEAR;
                    }

                    if (filteredItem.XRREV_MTD_PREV_YEAR === 0 || isNaN(parseFloat(filteredItem.XRREV_MTD_PREV_YEAR))) {
                        delete filteredItem.XRREV_MTD_PREV_YEAR;
                    }

                    if (filteredItem.XARR_MTD_PREV_YEAR === 'NaN') {
                        delete filteredItem.XARR_MTD_PREV_YEAR;
                    }

                    if (filteredItem.XRN_MTD_CURR_YEAR === 0 || isNaN(parseFloat(filteredItem.XRN_MTD_CURR_YEAR))) {
                        delete filteredItem.XRN_MTD_CURR_YEAR;
                    }

                    if (filteredItem.XRREV_MTD_CURR_YEAR === 0 || isNaN(parseFloat(filteredItem.XRREV_MTD_CURR_YEAR))) {
                        delete filteredItem.XRREV_MTD_CURR_YEAR;
                    }

                    if (filteredItem.XARR_MTD_CURR_YEAR === 'NaN') {
                        delete filteredItem.XARR_MTD_CURR_YEAR;
                    }

                    if (filteredItem.XRN_MTD_NEXT_YEAR === 0 || isNaN(parseFloat(filteredItem.XRN_MTD_NEXT_YEAR))) {
                        delete filteredItem.XRN_MTD_NEXT_YEAR;
                    }

                    if (filteredItem.XRREV_MTD_NEXT_YEAR === 0 || isNaN(parseFloat(filteredItem.XRREV_MTD_NEXT_YEAR))) {
                        delete filteredItem.XRREV_MTD_NEXT_YEAR;
                    }


                    if (filteredItem.XARR_MTD_NEXT_YEAR === 'NaN') {
                        delete filteredItem.XARR_MTD_NEXT_YEAR;
                    }

                    return filteredItem;
                });

                const filteredYears = [2019, 2020, 2021]; // Daftar tahun yang ingin Anda filter

                const filteredData = filteredOutput.filter(item => !filteredYears.includes(item.stay_year));

                const modifiedData = filteredData.map(item => {
                    const modifiedItem = {};

                    for (let key in item) {
                        const value = item[key];

                        if (value !== "NaN" && value !== 0 && value !== false) {
                            modifiedItem[key] = value;
                        }
                    }

                    return modifiedItem;
                });

                const modifiedData1 = modifiedData.map(item => {
                    const modifiedItem = {};

                    for (let key in item) {
                        const modifiedKey = key.replace(/(PREV_|CURR_|NEXT_)/g, "");
                        modifiedItem[modifiedKey] = item[key];
                    }

                    return modifiedItem;
                });

                res.status(200).json(modifiedData1)
            }

        } catch (error) {
            console.log(error);
        }
    };

    // ---------------------------------------------------
    // DIRECT MTD DAILY ON THE BOOK
    // ---------------------------------------------------
    static async getDirectMtdDailyBooking(req, res) {
        const { hotel, year, comparison, month, description, card } = req.query;

        var currentTime = new Date();
        var currYear = year > 0 ? year : currentTime.getFullYear();
        var prevYear = comparison > 0 ? Number(comparison) : currentTime.getFullYear() - 1;
        var nextYear = currentTime.getFullYear() + 1

        // list 1 years before current year
        const startCurrYear = currYear - 1;
        const curryearList = [];

        for (let tahun = startCurrYear; tahun <= currYear; tahun++) {
            if (tahun != 2020 && tahun != 2021) {
                curryearList.push(tahun);
            }
        }

        curryearList.push(nextYear)

        const yearsBeforeCurrYear = '[' + curryearList.join(', ') + ']'

        console.log(">>>>>>>>>>>>>>>> bikin kondisi baru disini >>>>>>>>>>>>>>>>>>>>>>")
        let whereClause = {};
        let whereClauseSegment = {}

        if (year != '' && comparison == '') {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear)
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        }


        if (hotel != '') {
            whereClause['room_class'] = hotel;
        }

        whereClause['segment'] = {
            [Op.and]: [
                Sequelize.where(Sequelize.col('marketsegment.segment'), { [Op.eq]: 'DIRECT' }),
            ]
        };

        if (description != '') {  // filter marketsegment.description sesudah where
            whereClause['description'] = {
                [Op.and]: [
                    Sequelize.where(Sequelize.col('marketsegment.description'), { [Op.eq]: description }),
                ]
            };
        }

        if (card != '') {  // filter ratecode.card sesudah where
            whereClause['card'] = {
                [Op.and]: [
                    Sequelize.where(Sequelize.col('ratecode.card'), { [Op.eq]: card }),
                ]
            };
        }


        const include_option = [
            {
                model: MarketSegment,
                as: 'marketsegment',
                attributes: [],  // just make relation not show column
                required: false,  //left outer join; true : inner join
                where: whereClauseSegment
            },
            {
                model: RateCode,
                as: "ratecode",
                attributes: [],
                required: false
            }
        ]

        const attributes = [];
        const years = JSON.parse(yearsBeforeCurrYear);

        for (let i = 0; i < years.length; i++) {
            const year = years[i];

            const attribute = [
                Sequelize.fn(
                    'SUM',
                    Sequelize.literal(
                        "CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " +
                        year +
                        ' THEN `rn` ELSE 0 END'
                    )
                ),
                'room_night_' + year,
            ];

            attributes.push(attribute);
        }

        console.log(whereClause, ">>>>>>>>>>>>>>> ISI KONDISI >>>>>>>>>>>>>>>> ")


        if (year != '' && comparison == '') {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        [Sequelize.literal("RIGHT(STR_TO_DATE(stay_date, '%d/%m/%Y'), 2)"), "day"],
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'room_night'],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        Sequelize.literal("RIGHT(STR_TO_DATE(stay_date, '%d/%m/%Y'), 2)"),
                    ],
                    order: [
                        [Sequelize.literal("RIGHT(STR_TO_DATE(stay_date, '%d/%m/%Y'), 2)"), 'ASC'],
                    ],
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        [Sequelize.literal("RIGHT(STR_TO_DATE(stay_date, '%d/%m/%Y'), 2)"), "day"],
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + prevYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + prevYear,
                        ],
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + currYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + currYear,
                        ],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        Sequelize.literal("RIGHT(STR_TO_DATE(stay_date, '%d/%m/%Y'), 2)"),
                    ],
                    order: [
                        [Sequelize.literal("RIGHT(STR_TO_DATE(stay_date, '%d/%m/%Y'), 2)"), 'ASC'],
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }

        } else {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        [Sequelize.literal("RIGHT(STR_TO_DATE(stay_date, '%d/%m/%Y'), 2)"), "day"],
                        ...attributes,

                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        Sequelize.literal("RIGHT(STR_TO_DATE(stay_date, '%d/%m/%Y'), 2)"),
                    ],
                    order: [
                        [Sequelize.literal("RIGHT(STR_TO_DATE(stay_date, '%d/%m/%Y'), 2)"), 'ASC'],
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        }
    };

    // ---------------------------------------------------
    // DIRECT YTD DAILY ON THE BOOK
    // ---------------------------------------------------
    static async getDirectDailyBooking(req, res) {
        const { hotel, year, comparison, month, description, card } = req.query;

        var currentTime = new Date();
        var currYear = year > 0 ? year : currentTime.getFullYear();
        var prevYear = comparison > 0 ? Number(comparison) : currentTime.getFullYear() - 1;
        var nextYear = currentTime.getFullYear() + 1

        // list 1 years before current year
        const startCurrYear = currYear - 1;
        const curryearList = [];

        for (let tahun = startCurrYear; tahun <= currYear; tahun++) {
            if (tahun != 2020 && tahun != 2021) {
                curryearList.push(tahun);
            }
        }

        curryearList.push(nextYear)

        const yearsBeforeCurrYear = '[' + curryearList.join(', ') + ']'

        console.log(">>>>>>>>>>>>>>>> bikin kondisi baru disini >>>>>>>>>>>>>>>>>>>>>>")
        let whereClause = {};
        let whereClauseSegment = {}

        if (year != '' && comparison == '') {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear)
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        }


        if (hotel != '') {
            whereClause['room_class'] = hotel;
        }

        whereClause['segment'] = {
            [Op.and]: [
                Sequelize.where(Sequelize.col('marketsegment.segment'), { [Op.eq]: 'DIRECT' }),
            ]
        };

        if (description != '') {  // filter marketsegment.description sesudah where
            whereClause['description'] = {
                [Op.and]: [
                    Sequelize.where(Sequelize.col('marketsegment.description'), { [Op.eq]: description }),
                ]
            };
        }

        if (card != '') {  // filter ratecode.card sesudah where
            whereClause['card'] = {
                [Op.and]: [
                    Sequelize.where(Sequelize.col('ratecode.card'), { [Op.eq]: card }),
                ]
            };
        }


        const include_option = [
            {
                model: MarketSegment,
                as: 'marketsegment',
                attributes: [],  // just make relation not show column
                required: false,  //left outer join; true : inner join
                where: whereClauseSegment
            },
            {
                model: RateCode,
                as: "ratecode",
                attributes: [],
                required: false
            }
        ]

        const attributes = [];
        const years = JSON.parse(yearsBeforeCurrYear);

        for (let i = 0; i < years.length; i++) {
            const year = years[i];

            const attribute = [
                Sequelize.fn(
                    'SUM',
                    Sequelize.literal(
                        "CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " +
                        year +
                        ' THEN `rn` ELSE 0 END'
                    )
                ),
                'room_night_' + year,
            ];

            attributes.push(attribute);
        }

        console.log(whereClause, ">>>>>>>>>>>>>>> ISI KONDISI >>>>>>>>>>>>>>>> ")


        if (year != '' && comparison == '') {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        [Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), 'stay_month'],
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'room_night'],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')),
                    ],
                    order: [
                        [Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), 'ASC'],
                    ],
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        [Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), 'stay_month'],
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + prevYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + prevYear,
                        ],
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + currYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + currYear,
                        ],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')),
                    ],
                    order: [
                        [Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), 'ASC'],
                    ],
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        [Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), 'stay_month'],
                        ...attributes,
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')),
                    ],
                    order: [
                        [Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), 'ASC'],
                    ],
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        }
    };

    // ---------------------------------------------------
    // DIRECT DAILY ON THE BOOK
    // ---------------------------------------------------
    static async getDirectTopRateCode(req, res) {
        const { hotel, year, comparison, month, description, card } = req.query;

        var currentTime = new Date();
        var currYear = year > 0 ? year : currentTime.getFullYear();
        var prevYear = comparison > 0 ? Number(comparison) : currentTime.getFullYear() - 1;
        var nextYear = currentTime.getFullYear() + 1

        // list 1 years before current year
        const startCurrYear = currYear - 1;
        const curryearList = [];

        for (let tahun = startCurrYear; tahun <= currYear; tahun++) {
            if (tahun != 2020 && tahun != 2021) {
                curryearList.push(tahun);
            }
        }

        curryearList.push(nextYear)

        const yearsBeforeCurrYear = '[' + curryearList.join(', ') + ']'

        console.log(">>>>>>>>>>>>>>>> bikin kondisi baru disini >>>>>>>>>>>>>>>>>>>>>>")
        let whereClause = {};
        let whereClauseSegment = {}

        if (year != '' && comparison == '') {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear)
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        }


        if (hotel != '') {
            whereClause['room_class'] = hotel;
        }

        whereClause['segment'] = {
            [Op.and]: [
                Sequelize.where(Sequelize.col('marketsegment.segment'), { [Op.eq]: 'DIRECT' }),
            ]
        };

        if (description != '') {  // filter marketsegment.description sesudah where
            whereClause['description'] = {
                [Op.and]: [
                    Sequelize.where(Sequelize.col('marketsegment.description'), { [Op.eq]: description }),
                ]
            };
        }

        if (card != '') {  // filter ratecode.card sesudah where
            whereClause['card'] = {
                [Op.and]: [
                    Sequelize.where(Sequelize.col('ratecode.card'), { [Op.eq]: card }),
                ]
            };
        }


        const include_option = [
            {
                model: MarketSegment,
                as: 'marketsegment',
                attributes: [],  // just make relation not show column
                required: false,  //left outer join; true : inner join
                where: whereClauseSegment
            },
            {
                model: RateCode,
                as: "ratecode",
                attributes: [],
                required: false
            }
        ]

        const attributes = [];
        const years = JSON.parse(yearsBeforeCurrYear);

        for (let i = 0; i < years.length; i++) {
            const year = years[i];

            const attribute = [
                Sequelize.fn(
                    'SUM',
                    Sequelize.literal(
                        "CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " +
                        year +
                        ' THEN `rn` ELSE 0 END'
                    )
                ),
                'room_night_' + year,
            ];

            attributes.push(attribute);
        }

        console.log(whereClause, ">>>>>>>>>>>>>>> ISI KONDISI >>>>>>>>>>>>>>>> ")


        if (year != '' && comparison == '') {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'ratecode.rate_description2',
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'room_night'],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        'ratecode.rate_description2',
                    ],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'ratecode.rate_description2',
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + prevYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + prevYear,
                        ],
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + currYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + currYear,
                        ],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        'ratecode.rate_description2',
                    ],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'ratecode.rate_description2',
                        ...attributes,
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        'ratecode.rate_description2',
                    ],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        }
    };

    // ---------------------------------------------------
    // DIRECT TOP LENGTH OF STAY
    // ---------------------------------------------------
    static async getDirectTopLengthStay(req, res) {
        const { hotel, year, comparison, month, description, card } = req.query;

        var currentTime = new Date();
        var currYear = year > 0 ? year : currentTime.getFullYear();
        var prevYear = comparison > 0 ? Number(comparison) : currentTime.getFullYear() - 1;
        var nextYear = currentTime.getFullYear() + 1

        // list 1 years before current year
        const startCurrYear = currYear - 1;
        const curryearList = [];

        for (let tahun = startCurrYear; tahun <= currYear; tahun++) {
            if (tahun != 2020 && tahun != 2021) {
                curryearList.push(tahun);
            }
        }

        curryearList.push(nextYear)

        const yearsBeforeCurrYear = '[' + curryearList.join(', ') + ']'

        console.log(">>>>>>>>>>>>>>>> bikin kondisi baru disini >>>>>>>>>>>>>>>>>>>>>>")
        let whereClause = {};
        let whereClauseSegment = {}

        if (year != '' && comparison == '') {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('los'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear)
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('los'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('los'), { [Op.ne]: '' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('los'), { [Op.ne]: '' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('los'), { [Op.ne]: '' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('los'), { [Op.ne]: '' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        }


        if (hotel != '') {
            whereClause['room_class'] = hotel;
        }

        whereClause['segment'] = {
            [Op.and]: [
                Sequelize.where(Sequelize.col('marketsegment.segment'), { [Op.eq]: 'DIRECT' }),
            ]
        };

        if (description != '') {  // filter marketsegment.description sesudah where
            whereClause['description'] = {
                [Op.and]: [
                    Sequelize.where(Sequelize.col('marketsegment.description'), { [Op.eq]: description }),
                ]
            };
        }

        if (card != '') {  // filter ratecode.card sesudah where
            whereClause['card'] = {
                [Op.and]: [
                    Sequelize.where(Sequelize.col('ratecode.card'), { [Op.eq]: card }),
                ]
            };
        }


        const include_option = [
            {
                model: MarketSegment,
                as: 'marketsegment',
                attributes: [],  // just make relation not show column
                required: false,  //left outer join; true : inner join
                where: whereClauseSegment
            },
            {
                model: RateCode,
                as: "ratecode",
                attributes: [],
                required: false
            }
        ]

        const attributes = [];
        const years = JSON.parse(yearsBeforeCurrYear);

        for (let i = 0; i < years.length; i++) {
            const year = years[i];

            const attribute = [
                Sequelize.fn(
                    'SUM',
                    Sequelize.literal(
                        "CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " +
                        year +
                        ' THEN `rn` ELSE 0 END'
                    )
                ),
                'room_night_' + year,
            ];

            attributes.push(attribute);
        }

        console.log(whereClause, ">>>>>>>>>>>>>>> ISI KONDISI >>>>>>>>>>>>>>>> ")


        if (year != '' && comparison == '') {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'los',
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'room_night'],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        'los',
                    ],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'los',
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + prevYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + prevYear,
                        ],
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + currYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + currYear,
                        ],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        'los',
                    ],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'los',
                        ...attributes,
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        'los',
                    ],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        }
    };

    // ---------------------------------------------------
    // DIRECT TOP BOOKING LEAD TIME
    // ---------------------------------------------------
    static async getDirectTopBookingLeadTime(req, res) {
        const { hotel, year, comparison, month, description, card } = req.query;

        var currentTime = new Date();
        var currYear = year > 0 ? year : currentTime.getFullYear();
        var prevYear = comparison > 0 ? Number(comparison) : currentTime.getFullYear() - 1;
        var nextYear = currentTime.getFullYear() + 1

        // list 1 years before current year
        const startCurrYear = currYear - 1;
        const curryearList = [];

        for (let tahun = startCurrYear; tahun <= currYear; tahun++) {
            if (tahun != 2020 && tahun != 2021) {
                curryearList.push(tahun);
            }
        }

        curryearList.push(nextYear)

        const yearsBeforeCurrYear = '[' + curryearList.join(', ') + ']'

        console.log(">>>>>>>>>>>>>>>> bikin kondisi baru disini >>>>>>>>>>>>>>>>>>>>>>")
        let whereClause = {};
        let whereClauseSegment = {}

        if (year != '' && comparison == '') {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('lead_days'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear)
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('lead_days'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('lead_days'), { [Op.ne]: '' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('lead_days'), { [Op.ne]: '' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('lead_days'), { [Op.ne]: '' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('lead_days'), { [Op.ne]: '' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        }


        if (hotel != '') {
            whereClause['room_class'] = hotel;
        }

        whereClause['segment'] = {
            [Op.and]: [
                Sequelize.where(Sequelize.col('marketsegment.segment'), { [Op.eq]: 'DIRECT' }),
            ]
        };

        if (description != '') {  // filter marketsegment.description sesudah where
            whereClause['description'] = {
                [Op.and]: [
                    Sequelize.where(Sequelize.col('marketsegment.description'), { [Op.eq]: description }),
                ]
            };
        }

        if (card != '') {  // filter ratecode.card sesudah where
            whereClause['card'] = {
                [Op.and]: [
                    Sequelize.where(Sequelize.col('ratecode.card'), { [Op.eq]: card }),
                ]
            };
        }


        const include_option = [
            {
                model: MarketSegment,
                as: 'marketsegment',
                attributes: [],  // just make relation not show column
                required: false,  //left outer join; true : inner join
                where: whereClauseSegment
            },
            {
                model: RateCode,
                as: "ratecode",
                attributes: [],
                required: false
            }
        ]

        const attributes = [];
        const years = JSON.parse(yearsBeforeCurrYear);

        for (let i = 0; i < years.length; i++) {
            const year = years[i];

            const attribute = [
                Sequelize.fn(
                    'SUM',
                    Sequelize.literal(
                        "CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " +
                        year +
                        ' THEN `rn` ELSE 0 END'
                    )
                ),
                'room_night_' + year,
            ];

            attributes.push(attribute);
        }

        console.log(whereClause, ">>>>>>>>>>>>>>> ISI KONDISI >>>>>>>>>>>>>>>> ")


        if (year != '' && comparison == '') {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'lead_days',
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'room_night'],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        'lead_days',
                    ],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'lead_days',
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + prevYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + prevYear,
                        ],
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + currYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + currYear,
                        ],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        'lead_days',
                    ],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'lead_days',
                        ...attributes,
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        'lead_days',
                    ],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        }
    };

    // ---------------------------------------------------
    // DIRECT TOP ROOM TYPES
    // ---------------------------------------------------
    static async getDirectTopRoomType(req, res) {
        const { hotel, year, comparison, month, description, card } = req.query;

        var currentTime = new Date();
        var currYear = year > 0 ? year : currentTime.getFullYear();
        var prevYear = comparison > 0 ? Number(comparison) : currentTime.getFullYear() - 1;
        var nextYear = currentTime.getFullYear() + 1

        // list 1 years before current year
        const startCurrYear = currYear - 1;
        const curryearList = [];

        for (let tahun = startCurrYear; tahun <= currYear; tahun++) {
            if (tahun != 2020 && tahun != 2021) {
                curryearList.push(tahun);
            }
        }

        curryearList.push(nextYear)

        const yearsBeforeCurrYear = '[' + curryearList.join(', ') + ']'

        console.log(">>>>>>>>>>>>>>>> bikin kondisi baru disini >>>>>>>>>>>>>>>>>>>>>>")
        let whereClause = {};
        let whereClauseSegment = {}

        if (year != '' && comparison == '') {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('lead_days'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear)
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('lead_days'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('lead_days'), { [Op.ne]: '' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('lead_days'), { [Op.ne]: '' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('lead_days'), { [Op.ne]: '' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('lead_days'), { [Op.ne]: '' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        }


        if (hotel != '') {
            whereClause['room_class'] = hotel;
        }

        whereClause['segment'] = {
            [Op.and]: [
                Sequelize.where(Sequelize.col('marketsegment.segment'), { [Op.eq]: 'DIRECT' }),
            ]
        };

        if (description != '') {  // filter marketsegment.description sesudah where
            whereClause['description'] = {
                [Op.and]: [
                    Sequelize.where(Sequelize.col('marketsegment.description'), { [Op.eq]: description }),
                ]
            };
        }

        if (card != '') {  // filter ratecode.card sesudah where
            whereClause['card'] = {
                [Op.and]: [
                    Sequelize.where(Sequelize.col('ratecode.card'), { [Op.eq]: card }),
                ]
            };
        }


        const include_option = [
            {
                model: MarketSegment,
                as: 'marketsegment',
                attributes: [],  // just make relation not show column
                required: false,  //left outer join; true : inner join
                where: whereClauseSegment
            },
            {
                model: RateCode,
                as: "ratecode",
                attributes: [],
                required: false
            },
            {
                model: RoomType,
                as: 'roomtype',
                attributes: [],
                required: true
            }
        ]

        const attributes = [];
        const years = JSON.parse(yearsBeforeCurrYear);

        for (let i = 0; i < years.length; i++) {
            const year = years[i];

            const attribute = [
                Sequelize.fn(
                    'SUM',
                    Sequelize.literal(
                        "CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " +
                        year +
                        ' THEN `rn` ELSE 0 END'
                    )
                ),
                'room_night_' + year,
            ];

            attributes.push(attribute);
        }

        console.log(whereClause, ">>>>>>>>>>>>>>> ISI KONDISI >>>>>>>>>>>>>>>> ")


        if (year != '' && comparison == '') {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'roomtype.room_type',
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'room_night'],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        'roomtype.room_type',
                    ],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'roomtype.room_type',
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + prevYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + prevYear,
                        ],
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + currYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + currYear,
                        ],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        'roomtype.room_type',
                    ],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'roomtype.room_type',
                        ...attributes,
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        'roomtype.room_type',
                    ],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        }
    };

    // ---------------------------------------------------
    // DIRECT TOP SUPPORTERS
    // ---------------------------------------------------
    static async getDirectTopSupporter(req, res) {
        const { hotel, year, comparison, month, description, card } = req.query;

        var currentTime = new Date();
        var currYear = year > 0 ? year : currentTime.getFullYear();
        var prevYear = comparison > 0 ? Number(comparison) : currentTime.getFullYear() - 1;
        var nextYear = currentTime.getFullYear() + 1

        // list 1 years before current year
        const startCurrYear = currYear - 1;
        const curryearList = [];

        for (let tahun = startCurrYear; tahun <= currYear; tahun++) {
            if (tahun != 2020 && tahun != 2021) {
                curryearList.push(tahun);
            }
        }

        curryearList.push(nextYear)

        const yearsBeforeCurrYear = '[' + curryearList.join(', ') + ']'

        console.log(">>>>>>>>>>>>>>>> bikin kondisi baru disini >>>>>>>>>>>>>>>>>>>>>>")
        let whereClause = {};
        let whereClauseSegment = {}

        if (year != '' && comparison == '') {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear)
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.ne]: '' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.ne]: '' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.ne]: '' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.ne]: '' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        }


        if (hotel != '') {
            whereClause['room_class'] = hotel;
        }

        whereClause['segment'] = {
            [Op.and]: [
                Sequelize.where(Sequelize.col('marketsegment.segment'), { [Op.eq]: 'DIRECT' }),
            ]
        };

        if (description != '') {  // filter marketsegment.description sesudah where
            whereClause['description'] = {
                [Op.and]: [
                    Sequelize.where(Sequelize.col('marketsegment.description'), { [Op.eq]: description }),
                ]
            };
        }

        if (card != '') {  // filter ratecode.card sesudah where
            whereClause['card'] = {
                [Op.and]: [
                    Sequelize.where(Sequelize.col('ratecode.card'), { [Op.eq]: card }),
                ]
            };
        }


        const include_option = [
            {
                model: MarketSegment,
                as: 'marketsegment',
                attributes: [],  // just make relation not show column
                required: false,  //left outer join; true : inner join
                where: whereClauseSegment
            },
            {
                model: RateCode,
                as: "ratecode",
                attributes: [],
                required: false
            },
            {
                model: RoomType,
                as: 'roomtype',
                attributes: [],
                required: true
            }
        ]

        const attributes = [];
        const years = JSON.parse(yearsBeforeCurrYear);

        for (let i = 0; i < years.length; i++) {
            const year = years[i];

            const attribute = [
                Sequelize.fn(
                    'SUM',
                    Sequelize.literal(
                        "CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " +
                        year +
                        ' THEN `rn` ELSE 0 END'
                    )
                ),
                'room_night_' + year,
            ];

            attributes.push(attribute);
        }

        console.log(whereClause, ">>>>>>>>>>>>>>> ISI KONDISI >>>>>>>>>>>>>>>> ")


        if (year != '' && comparison == '') {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'agent_name',
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'room_night'],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        'agent_name',
                    ],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'agent_name',
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + prevYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + prevYear,
                        ],
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + currYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + currYear,
                        ],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        'agent_name',
                    ],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'agent_name',
                        ...attributes,
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        'agent_name',
                    ],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        }
    };

    // ---------------------------------------------------
    // DIRECT SUMMARY
    // ---------------------------------------------------
    static async getDirectSummary(req, res) {
        const { hotel, year, comparison, month, description, card } = req.query;

        var currentTime = new Date();
        var currYear = year > 0 ? year : currentTime.getFullYear();
        var prevYear = comparison > 0 ? Number(comparison) : currentTime.getFullYear() - 1;
        var nextYear = currentTime.getFullYear() + 1


        console.log(">>>>>>>>>>>>>>>> bikin kondisi baru disini >>>>>>>>>>>>>>>>>>>>>>")
        let whereClause = {};
        let whereClauseSegment = {}

        if (year != '' && comparison == '') {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear)
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else if (year == '' && comparison != '' || year != '' && comparison != '') {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2019,2020,2021')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2019,2020,2021')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        }


        if (hotel != '') {
            whereClause['room_class'] = hotel;
        }

        whereClause['segment'] = {
            [Op.and]: [
                Sequelize.where(Sequelize.col('marketsegment.segment'), { [Op.eq]: 'DIRECT' }),
            ]
        };

        if (description != '') {  // filter marketsegment.description sesudah where
            whereClause['description'] = {
                [Op.and]: [
                    Sequelize.where(Sequelize.col('marketsegment.description'), { [Op.eq]: description }),
                ]
            };
        }

        if (card != '') {  // filter ratecode.card sesudah where
            whereClause['card'] = {
                [Op.and]: [
                    Sequelize.where(Sequelize.col('ratecode.card'), { [Op.eq]: card }),
                ]
            };
        }


        const include_option = [
            {
                model: MarketSegment,
                as: 'marketsegment',
                attributes: [],  // just make relation not show column
                required: false,  //left outer join; true : inner join
                where: whereClauseSegment
            },
            {
                model: RateCode,
                as: "ratecode",
                attributes: [],
                required: false
            },
            {
                model: RoomType,
                as: 'roomtype',
                attributes: [],
                required: true
            }
        ]

        console.log(whereClause, ">>>>>>>>>>>>>>> ISI KONDISI >>>>>>>>>>>>>>>> ")


        try {

            if (year != '' && comparison == '') {
                const result = await Master.findAll({
                    attributes: [
                        [Sequelize.fn('year', Sequelize.fn('str_to_date', Sequelize.col('stay_date'), '%d/%m/%Y')), 'stay_year'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${year} THEN REPLACE(rn, ',', '') ELSE 0 END`)), 'XRN_MTD_CURRYEAR'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${year} THEN REPLACE(room_rev_usd, ',', '') ELSE 0 END`)), 'XRREV_MTD_CURRYEAR'],
                    ],
                    include: include_option,

                    where: whereClause,
                    group: [
                        Sequelize.fn('year', Sequelize.fn('str_to_date', Sequelize.col('stay_date'), '%d/%m/%Y'))
                    ],
                    order: [
                        [Sequelize.fn('year', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), 'ASC']
                    ],
                });

                // Manipulasi hasil query untuk menghasilkan output yang diinginkan
                const output = result.map(item => {
                    // console.log(XPU23BEFORE23, " <<<<<< SETIAP ITEM")
                    const stay_year = item.dataValues.stay_year;
                    const XRN_MTD_CURR_YEAR = Number(item.dataValues.XRN_MTD_CURRYEAR);
                    const XRREV_MTD_CURR_YEAR = Number(item.dataValues.XRREV_MTD_CURRYEAR);

                    const XARR_MTD_CURR_YEAR = (XRREV_MTD_CURR_YEAR / XRN_MTD_CURR_YEAR).toFixed(2);

                    const dynamicKey = `XRN_MTD_CURR_${stay_year}`;
                    const dynamicKey2 = `XRREV_MTD_CURR_${stay_year}`;
                    const dynamicKey3 = `XARR_MTD_CURR_${stay_year}`;

                    return {
                        stay_year,
                        [dynamicKey]: XRN_MTD_CURR_YEAR,
                        [dynamicKey2]: XRREV_MTD_CURR_YEAR,
                        [dynamicKey3]: XARR_MTD_CURR_YEAR
                    }
                })

                const modifiedData = output.map(item => {
                    const modifiedItem = {};

                    for (let key in item) {
                        const modifiedKey = key.replace(/(CURR_)/g, "");
                        modifiedItem[modifiedKey] = item[key];
                    }

                    return modifiedItem;
                });

                res.status(200).json(modifiedData)
            } else if (year == '' && comparison != '' || year != '' && comparison != '') {
                const result = await Master.findAll({
                    attributes: [
                        [Sequelize.fn('year', Sequelize.fn('str_to_date', Sequelize.col('stay_date'), '%d/%m/%Y')), 'stay_year'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${prevYear} THEN REPLACE(rn, ',', '') ELSE 0 END`)), 'XRN_MTD_PREVYEAR'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${currYear} THEN REPLACE(rn, ',', '') ELSE 0 END`)), 'XRN_MTD_CURRYEAR'],


                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${prevYear} THEN REPLACE(room_rev_usd, ',', '') ELSE 0 END`)), 'XRREV_MTD_PREVYEAR'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${currYear} THEN REPLACE(room_rev_usd, ',', '') ELSE 0 END`)), 'XRREV_MTD_CURRYEAR'],
                    ],
                    include: include_option,

                    where: whereClause,
                    group: Sequelize.literal('YEAR(STR_TO_DATE(stay_date, "%d/%m/%Y"))'),
                    order: Sequelize.literal('YEAR(STR_TO_DATE(stay_date, "%d/%m/%Y")) ASC'),
                });

                // Manipulasi hasil query untuk menghasilkan output yang diinginkan
                const output = result.map(item => {
                    // console.log(XPU23BEFORE23, " <<<<<< SETIAP ITEM")
                    const stay_year = item.dataValues.stay_year;
                    const XRN_MTD_COMP_YEAR = Number(item.dataValues.XRN_MTD_PREVYEAR);
                    const XRN_MTD_CURR_YEAR = Number(item.dataValues.XRN_MTD_CURRYEAR);


                    const XRREV_MTD_COMP_YEAR = Number(item.dataValues.XRREV_MTD_PREVYEAR);
                    const XRREV_MTD_CURR_YEAR = Number(item.dataValues.XRREV_MTD_CURRYEAR);

                    const XARR_MTD_COMP_YEAR = (XRREV_MTD_COMP_YEAR / XRN_MTD_COMP_YEAR).toFixed(2);
                    const XARR_MTD_CURR_YEAR = (XRREV_MTD_CURR_YEAR / XRN_MTD_CURR_YEAR).toFixed(2);

                    const dynamicKey = `XRN_MTD_COMP_${stay_year}`;
                    const dynamicKey2 = `XRREV_MTD_COMP_${stay_year}`;
                    const dynamicKey3 = `XARR_MTD_COMP_${stay_year}`;
                    const dynamicKey4 = `XRN_MTD_CURR_${stay_year}`;
                    const dynamicKey5 = `XRREV_MTD_CURR_${stay_year}`;
                    const dynamicKey6 = `XARR_MTD_CURR_${stay_year}`;

                    return {
                        stay_year,
                        [dynamicKey]: XRN_MTD_COMP_YEAR,
                        [dynamicKey2]: XRREV_MTD_COMP_YEAR,
                        [dynamicKey3]: XARR_MTD_COMP_YEAR,
                        [dynamicKey4]: XRN_MTD_CURR_YEAR,
                        [dynamicKey5]: XRREV_MTD_CURR_YEAR,
                        [dynamicKey6]: XARR_MTD_CURR_YEAR,
                    }

                })

                const filteredOutput = output.map(item => {
                    const filteredItem = { ...item };

                    if (filteredItem.XRN_MTD_COMP_YEAR === 0 || isNaN(parseFloat(filteredItem.XRN_MTD_COMP_YEAR))) {
                        delete filteredItem.XRN_MTD_COMP_YEAR;
                    }

                    if (filteredItem.XRREV_MTD_COMP_YEAR === 0 || isNaN(parseFloat(filteredItem.XRREV_MTD_COMP_YEAR))) {
                        delete filteredItem.XRREV_MTD_COMP_YEAR;
                    }

                    if (filteredItem.XARR_MTD_COMP_YEAR === 'NaN') {
                        delete filteredItem.XARR_MTD_COMP_YEAR;
                    }

                    if (filteredItem.XRN_MTD_CURR_YEAR === 0 || isNaN(parseFloat(filteredItem.XRN_MTD_CURR_YEAR))) {
                        delete filteredItem.XRN_MTD_CURR_YEAR;
                    }

                    if (filteredItem.XRREV_MTD_CURR_YEAR === 0 || isNaN(parseFloat(filteredItem.XRREV_MTD_CURR_YEAR))) {
                        delete filteredItem.XRREV_MTD_CURR_YEAR;
                    }

                    if (filteredItem.XARR_MTD_CURR_YEAR === 'NaN') {
                        delete filteredItem.XARR_MTD_CURR_YEAR;
                    }

                    return filteredItem;
                });

                const modifiedData = filteredOutput.map(item => {
                    const modifiedItem = {};

                    for (let key in item) {
                        const value = item[key];

                        if (value !== "NaN" && value !== 0 && value !== false) {
                            modifiedItem[key] = value;
                        }
                    }

                    return modifiedItem;
                });

                const modifiedData1 = modifiedData.map(item => {
                    const modifiedItem = {};

                    for (let key in item) {
                        const modifiedKey = key.replace(/(COMP_|CURR_)/g, "");
                        modifiedItem[modifiedKey] = item[key];
                    }

                    return modifiedItem;
                });

                res.status(200).json(modifiedData1)

            } else {
                const result = await Master.findAll({
                    attributes: [
                        [Sequelize.fn('year', Sequelize.fn('str_to_date', Sequelize.col('stay_date'), '%d/%m/%Y')), 'stay_year'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${prevYear} THEN REPLACE(rn, ',', '') ELSE 0 END`)), 'XRN_MTD_PREVYEAR'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${currYear} THEN REPLACE(rn, ',', '') ELSE 0 END`)), 'XRN_MTD_CURRYEAR'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${nextYear} THEN REPLACE(rn, ',', '') ELSE 0 END`)), 'XRN_MTD_NEXTYEAR'],


                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${prevYear} THEN REPLACE(room_rev_usd, ',', '') ELSE 0 END`)), 'XRREV_MTD_PREVYEAR'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${currYear} THEN REPLACE(room_rev_usd, ',', '') ELSE 0 END`)), 'XRREV_MTD_CURRYEAR'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${nextYear} THEN REPLACE(room_rev_usd, ',', '') ELSE 0 END`)), 'XRREV_MTD_NEXTYEAR'],
                    ],
                    include: include_option,

                    where: whereClause,
                    group: Sequelize.literal('YEAR(STR_TO_DATE(stay_date, "%d/%m/%Y"))'),
                    order: Sequelize.literal('YEAR(STR_TO_DATE(stay_date, "%d/%m/%Y")) ASC'),
                });

                // Manipulasi hasil query untuk menghasilkan output yang diinginkan
                const output = result.map(item => {
                    // console.log(XPU23BEFORE23, " <<<<<< SETIAP ITEM")
                    const stay_year = item.dataValues.stay_year;
                    const XRN_MTD_PREV_YEAR = Number(item.dataValues.XRN_MTD_PREVYEAR);
                    const XRN_MTD_CURR_YEAR = Number(item.dataValues.XRN_MTD_CURRYEAR);
                    const XRN_MTD_NEXT_YEAR = Number(item.dataValues.XRN_MTD_NEXTYEAR);

                    const XRREV_MTD_PREV_YEAR = Number(item.dataValues.XRREV_MTD_PREVYEAR);
                    const XRREV_MTD_CURR_YEAR = Number(item.dataValues.XRREV_MTD_CURRYEAR);
                    const XRREV_MTD_NEXT_YEAR = Number(item.dataValues.XRREV_MTD_NEXTYEAR);

                    const XARR_MTD_PREV_YEAR = (XRREV_MTD_PREV_YEAR / XRN_MTD_PREV_YEAR).toFixed(2);
                    const XARR_MTD_CURR_YEAR = (XRREV_MTD_CURR_YEAR / XRN_MTD_CURR_YEAR).toFixed(2);
                    const XARR_MTD_NEXT_YEAR = (XRREV_MTD_NEXT_YEAR / XRN_MTD_NEXT_YEAR).toFixed(2);

                    const dynamicKey = `XRN_MTD_PREV_${stay_year}`;
                    const dynamicKey2 = `XRREV_MTD_PREV_${stay_year}`;
                    const dynamicKey3 = `XARR_MTD_PREV_${stay_year}`;
                    const dynamicKey4 = `XRN_MTD_CURR_${stay_year}`;
                    const dynamicKey5 = `XRREV_MTD_CURR_${stay_year}`;
                    const dynamicKey6 = `XARR_MTD_CURR_${stay_year}`;
                    const dynamicKey7 = `XRN_MTD_NEXT_${stay_year}`;
                    const dynamicKey8 = `XRREV_MTD_NEXT_${stay_year}`;
                    const dynamicKey9 = `XARR_MTD_NEXT_${stay_year}`;

                    return {
                        stay_year,
                        [dynamicKey]: XRN_MTD_PREV_YEAR,
                        [dynamicKey2]: XRREV_MTD_PREV_YEAR,
                        [dynamicKey3]: XARR_MTD_PREV_YEAR,
                        [dynamicKey4]: XRN_MTD_CURR_YEAR,
                        [dynamicKey5]: XRREV_MTD_CURR_YEAR,
                        [dynamicKey6]: XARR_MTD_CURR_YEAR,
                        [dynamicKey7]: XRN_MTD_NEXT_YEAR,
                        [dynamicKey8]: XRREV_MTD_NEXT_YEAR,
                        [dynamicKey9]: XARR_MTD_NEXT_YEAR
                    }

                })

                const filteredOutput = output.map(item => {
                    const filteredItem = { ...item };

                    if (filteredItem.XRN_MTD_PREV_YEAR === 0 || isNaN(parseFloat(filteredItem.XRN_MTD_PREV_YEAR))) {
                        delete filteredItem.XRN_MTD_PREV_YEAR;
                    }

                    if (filteredItem.XRREV_MTD_PREV_YEAR === 0 || isNaN(parseFloat(filteredItem.XRREV_MTD_PREV_YEAR))) {
                        delete filteredItem.XRREV_MTD_PREV_YEAR;
                    }

                    if (filteredItem.XARR_MTD_PREV_YEAR === 'NaN') {
                        delete filteredItem.XARR_MTD_PREV_YEAR;
                    }

                    if (filteredItem.XRN_MTD_CURR_YEAR === 0 || isNaN(parseFloat(filteredItem.XRN_MTD_CURR_YEAR))) {
                        delete filteredItem.XRN_MTD_CURR_YEAR;
                    }

                    if (filteredItem.XRREV_MTD_CURR_YEAR === 0 || isNaN(parseFloat(filteredItem.XRREV_MTD_CURR_YEAR))) {
                        delete filteredItem.XRREV_MTD_CURR_YEAR;
                    }

                    if (filteredItem.XARR_MTD_CURR_YEAR === 'NaN') {
                        delete filteredItem.XARR_MTD_CURR_YEAR;
                    }

                    if (filteredItem.XRN_MTD_NEXT_YEAR === 0 || isNaN(parseFloat(filteredItem.XRN_MTD_NEXT_YEAR))) {
                        delete filteredItem.XRN_MTD_NEXT_YEAR;
                    }

                    if (filteredItem.XRREV_MTD_NEXT_YEAR === 0 || isNaN(parseFloat(filteredItem.XRREV_MTD_NEXT_YEAR))) {
                        delete filteredItem.XRREV_MTD_NEXT_YEAR;
                    }


                    if (filteredItem.XARR_MTD_NEXT_YEAR === 'NaN') {
                        delete filteredItem.XARR_MTD_NEXT_YEAR;
                    }

                    return filteredItem;
                });

                const filteredYears = [2019, 2020, 2021]; // Daftar tahun yang ingin Anda filter

                const filteredData = filteredOutput.filter(item => !filteredYears.includes(item.stay_year));

                const modifiedData = filteredData.map(item => {
                    const modifiedItem = {};

                    for (let key in item) {
                        const value = item[key];

                        if (value !== "NaN" && value !== 0 && value !== false) {
                            modifiedItem[key] = value;
                        }
                    }

                    return modifiedItem;
                });

                const modifiedData1 = modifiedData.map(item => {
                    const modifiedItem = {};

                    for (let key in item) {
                        const modifiedKey = key.replace(/(PREV_|CURR_|NEXT_)/g, "");
                        modifiedItem[modifiedKey] = item[key];
                    }

                    return modifiedItem;
                });

                res.status(200).json(modifiedData1)
            }

        } catch (error) {
            console.log(error);
        }
    };

    // ---------------------------------------------------
    // MP MTD DAILY ON THE BOOK
    // ---------------------------------------------------
    static async getMpMtdDailyBooking(req, res) {
        const { hotel, year, comparison, month, card, level } = req.query;

        var currentTime = new Date();
        var currYear = year > 0 ? year : currentTime.getFullYear();
        var prevYear = comparison > 0 ? Number(comparison) : currentTime.getFullYear() - 1;
        var nextYear = currentTime.getFullYear() + 1

        // list 1 years before current year
        const startCurrYear = currYear - 1;
        const curryearList = [];

        for (let tahun = startCurrYear; tahun <= currYear; tahun++) {
            if (tahun != 2020 && tahun != 2021) {
                curryearList.push(tahun);
            }
        }

        curryearList.push(nextYear)

        const yearsBeforeCurrYear = '[' + curryearList.join(', ') + ']'

        console.log(">>>>>>>>>>>>>>>> bikin kondisi baru disini >>>>>>>>>>>>>>>>>>>>>>")
        let whereClause = {};
        let whereClauseSegment = {}

        if (year != '' && comparison == '') {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear)
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        }

        if (hotel != '') {
            whereClause['room_class'] = hotel;
        }

        whereClause['segment'] = {
            [Op.and]: [
                Sequelize.where(Sequelize.col('marketsegment.segment'), { [Op.eq]: 'DIRECT' }),
            ]
        };


        if (card != '') {  // filter ratecode.card sesudah where
            whereClause['card'] = {
                [Op.and]: [
                    Sequelize.where(Sequelize.col('ratecode.card'), { [Op.eq]: card }),
                ]
            };
        }

        if (level != '') {
            whereClause['mp_level'] = level;
        }

        const include_option = [
            {
                model: MarketSegment,
                as: 'marketsegment',
                attributes: [],  // just make relation not show column
                required: false,  //left outer join; true : inner join
                where: whereClauseSegment
            },
            {
                model: RateCode,
                as: "ratecode",
                attributes: [],
                required: false
            }
        ]

        const attributes = [];
        const years = JSON.parse(yearsBeforeCurrYear);

        for (let i = 0; i < years.length; i++) {
            const year = years[i];

            const attribute = [
                Sequelize.fn(
                    'SUM',
                    Sequelize.literal(
                        "CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " +
                        year +
                        ' THEN `rn` ELSE 0 END'
                    )
                ),
                'room_night_' + year,
            ];

            attributes.push(attribute);
        }

        console.log(whereClause, ">>>>>>>>>>>>>>> ISI KONDISI >>>>>>>>>>>>>>>> ")


        if (year != '' && comparison == '') {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        [Sequelize.literal("RIGHT(STR_TO_DATE(stay_date, '%d/%m/%Y'), 2)"), "day"],
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'room_night'],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        Sequelize.literal("RIGHT(STR_TO_DATE(stay_date, '%d/%m/%Y'), 2)"),
                    ],
                    order: [
                        [Sequelize.literal("RIGHT(STR_TO_DATE(stay_date, '%d/%m/%Y'), 2)"), 'ASC'],
                    ],
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        [Sequelize.literal("RIGHT(STR_TO_DATE(stay_date, '%d/%m/%Y'), 2)"), "day"],
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + prevYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + prevYear,
                        ],
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + currYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + currYear,
                        ],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        Sequelize.literal("RIGHT(STR_TO_DATE(stay_date, '%d/%m/%Y'), 2)"),
                    ],
                    order: [
                        [Sequelize.literal("RIGHT(STR_TO_DATE(stay_date, '%d/%m/%Y'), 2)"), 'ASC'],
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }

        } else {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        [Sequelize.literal("RIGHT(STR_TO_DATE(stay_date, '%d/%m/%Y'), 2)"), "day"],
                        ...attributes,

                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        Sequelize.literal("RIGHT(STR_TO_DATE(stay_date, '%d/%m/%Y'), 2)"),
                    ],
                    order: [
                        [Sequelize.literal("RIGHT(STR_TO_DATE(stay_date, '%d/%m/%Y'), 2)"), 'ASC'],
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        }
    };


    // ---------------------------------------------------
    // MP YTD DAILY ON THE BOOK
    // ---------------------------------------------------
    static async getMpDailyBooking(req, res) {
        const { hotel, year, comparison, month, card, level } = req.query;

        var currentTime = new Date();
        var currYear = year > 0 ? year : currentTime.getFullYear();
        var prevYear = comparison > 0 ? Number(comparison) : currentTime.getFullYear() - 1;
        var nextYear = currentTime.getFullYear() + 1

        // list 1 years before current year
        const startCurrYear = currYear - 1;
        const curryearList = [];

        for (let tahun = startCurrYear; tahun <= currYear; tahun++) {
            if (tahun != 2020 && tahun != 2021) {
                curryearList.push(tahun);
            }
        }

        curryearList.push(nextYear)

        const yearsBeforeCurrYear = '[' + curryearList.join(', ') + ']'

        console.log(">>>>>>>>>>>>>>>> bikin kondisi baru disini >>>>>>>>>>>>>>>>>>>>>>")
        let whereClause = {};
        let whereClauseSegment = {}

        if (year != '' && comparison == '') {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear)
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')")
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        }

        if (hotel != '') {
            whereClause['room_class'] = hotel;
        }

        whereClause['segment'] = {
            [Op.and]: [
                Sequelize.where(Sequelize.col('marketsegment.segment'), { [Op.eq]: 'DIRECT' }),
            ]
        };


        if (card != '') {  // filter ratecode.card sesudah where
            whereClause['card'] = {
                [Op.and]: [
                    Sequelize.where(Sequelize.col('ratecode.card'), { [Op.eq]: card }),
                ]
            };
        }

        if (level != '') {
            whereClause['mp_level'] = level;
        }

        const include_option = [
            {
                model: MarketSegment,
                as: 'marketsegment',
                attributes: [],  // just make relation not show column
                required: false,  //left outer join; true : inner join
                where: whereClauseSegment
            },
            {
                model: RateCode,
                as: "ratecode",
                attributes: [],
                required: false
            }
        ]

        const attributes = [];
        const years = JSON.parse(yearsBeforeCurrYear);

        for (let i = 0; i < years.length; i++) {
            const year = years[i];

            const attribute = [
                Sequelize.fn(
                    'SUM',
                    Sequelize.literal(
                        "CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " +
                        year +
                        ' THEN `rn` ELSE 0 END'
                    )
                ),
                'room_night_' + year,
            ];

            attributes.push(attribute);
        }

        console.log(whereClause, ">>>>>>>>>>>>>>> ISI KONDISI >>>>>>>>>>>>>>>> ")


        if (year != '' && comparison == '') {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        [Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), 'stay_month'],
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'room_night'],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')),
                    ],
                    order: [
                        [Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), 'ASC'],
                    ],
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        [Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), 'stay_month'],
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + prevYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + prevYear,
                        ],
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + currYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + currYear,
                        ],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')),
                    ],
                    order: [
                        [Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), 'ASC'],
                    ],
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        [Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), 'stay_month'],
                        ...attributes,
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')),
                    ],
                    order: [
                        [Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), 'ASC'],
                    ],
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        }
    };

    // ---------------------------------------------------
    // MP TOP RATE CODES
    // ---------------------------------------------------
    static async getMpTopRateCode(req, res) {
        const { hotel, year, comparison, month, card, level } = req.query;

        var currentTime = new Date();
        var currYear = year > 0 ? year : currentTime.getFullYear();
        var prevYear = comparison > 0 ? Number(comparison) : currentTime.getFullYear() - 1;
        var nextYear = currentTime.getFullYear() + 1

        // list 1 years before current year
        const startCurrYear = currYear - 1;
        const curryearList = [];

        for (let tahun = startCurrYear; tahun <= currYear; tahun++) {
            if (tahun != 2020 && tahun != 2021) {
                curryearList.push(tahun);
            }
        }

        curryearList.push(nextYear)

        const yearsBeforeCurrYear = '[' + curryearList.join(', ') + ']'

        console.log(">>>>>>>>>>>>>>>> bikin kondisi baru disini >>>>>>>>>>>>>>>>>>>>>>")
        let whereClause = {};
        let whereClauseSegment = {}

        if (year != '' && comparison == '') {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear)
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        }


        if (hotel != '') {
            whereClause['room_class'] = hotel;
        }

        whereClause['segment'] = {
            [Op.and]: [
                Sequelize.where(Sequelize.col('marketsegment.segment'), { [Op.eq]: 'DIRECT' }),
            ]
        };


        if (card != '') {  // filter ratecode.card sesudah where
            whereClause['card'] = {
                [Op.and]: [
                    Sequelize.where(Sequelize.col('ratecode.card'), { [Op.eq]: card }),
                ]
            };
        }

        if (level != '') {
            whereClause['mp_level'] = level;
        }

        const include_option = [
            {
                model: MarketSegment,
                as: 'marketsegment',
                attributes: [],  // just make relation not show column
                required: false,  //left outer join; true : inner join
                where: whereClauseSegment
            },
            {
                model: RateCode,
                as: "ratecode",
                attributes: [],
                required: false
            }
        ]

        const attributes = [];
        const years = JSON.parse(yearsBeforeCurrYear);

        for (let i = 0; i < years.length; i++) {
            const year = years[i];

            const attribute = [
                Sequelize.fn(
                    'SUM',
                    Sequelize.literal(
                        "CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " +
                        year +
                        ' THEN `rn` ELSE 0 END'
                    )
                ),
                'room_night_' + year,
            ];

            attributes.push(attribute);
        }

        console.log(whereClause, ">>>>>>>>>>>>>>> ISI KONDISI >>>>>>>>>>>>>>>> ")


        if (year != '' && comparison == '') {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'ratecode.rate_description2',
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'room_night'],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        'ratecode.rate_description2',
                    ],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'ratecode.rate_description2',
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + prevYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + prevYear,
                        ],
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + currYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + currYear,
                        ],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        'ratecode.rate_description2',
                    ],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'ratecode.rate_description2',
                        ...attributes,
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        'ratecode.rate_description2',
                    ],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        }
    };

    // ---------------------------------------------------
    // MP TOP LENGTH OF STAY
    // ---------------------------------------------------
    static async getMpTopLengthStay(req, res) {
        const { hotel, year, comparison, month, card, level } = req.query;

        var currentTime = new Date();
        var currYear = year > 0 ? year : currentTime.getFullYear();
        var prevYear = comparison > 0 ? Number(comparison) : currentTime.getFullYear() - 1;
        var nextYear = currentTime.getFullYear() + 1

        // list 1 years before current year
        const startCurrYear = currYear - 1;
        const curryearList = [];

        for (let tahun = startCurrYear; tahun <= currYear; tahun++) {
            if (tahun != 2020 && tahun != 2021) {
                curryearList.push(tahun);
            }
        }

        curryearList.push(nextYear)

        const yearsBeforeCurrYear = '[' + curryearList.join(', ') + ']'

        console.log(">>>>>>>>>>>>>>>> bikin kondisi baru disini >>>>>>>>>>>>>>>>>>>>>>")
        let whereClause = {};
        let whereClauseSegment = {}

        if (year != '' && comparison == '') {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('los'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear)
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('los'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('los'), { [Op.ne]: '' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('los'), { [Op.ne]: '' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('los'), { [Op.ne]: '' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('los'), { [Op.ne]: '' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        }


        if (hotel != '') {
            whereClause['room_class'] = hotel;
        }

        whereClause['segment'] = {
            [Op.and]: [
                Sequelize.where(Sequelize.col('marketsegment.segment'), { [Op.eq]: 'DIRECT' }),
            ]
        };


        if (card != '') {  // filter ratecode.card sesudah where
            whereClause['card'] = {
                [Op.and]: [
                    Sequelize.where(Sequelize.col('ratecode.card'), { [Op.eq]: card }),
                ]
            };
        }

        if (level != '') {
            whereClause['mp_level'] = level;
        }

        const include_option = [
            {
                model: MarketSegment,
                as: 'marketsegment',
                attributes: [],  // just make relation not show column
                required: false,  //left outer join; true : inner join
                where: whereClauseSegment
            },
            {
                model: RateCode,
                as: "ratecode",
                attributes: [],
                required: false
            }
        ]

        const attributes = [];
        const years = JSON.parse(yearsBeforeCurrYear);

        for (let i = 0; i < years.length; i++) {
            const year = years[i];

            const attribute = [
                Sequelize.fn(
                    'SUM',
                    Sequelize.literal(
                        "CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " +
                        year +
                        ' THEN `rn` ELSE 0 END'
                    )
                ),
                'room_night_' + year,
            ];

            attributes.push(attribute);
        }

        console.log(whereClause, ">>>>>>>>>>>>>>> ISI KONDISI >>>>>>>>>>>>>>>> ")


        if (year != '' && comparison == '') {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'los',
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'room_night'],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        'los',
                    ],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'los',
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + prevYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + prevYear,
                        ],
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + currYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + currYear,
                        ],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        'los',
                    ],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'los',
                        ...attributes,
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        'los',
                    ],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        }
    };

    // ---------------------------------------------------
    // MP TOP BOOKING LEAD TOME
    // ---------------------------------------------------
    static async getMpTopBookingLeadTime(req, res) {
        const { hotel, year, comparison, month, card, level } = req.query;

        var currentTime = new Date();
        var currYear = year > 0 ? year : currentTime.getFullYear();
        var prevYear = comparison > 0 ? Number(comparison) : currentTime.getFullYear() - 1;
        var nextYear = currentTime.getFullYear() + 1

        // list 1 years before current year
        const startCurrYear = currYear - 1;
        const curryearList = [];

        for (let tahun = startCurrYear; tahun <= currYear; tahun++) {
            if (tahun != 2020 && tahun != 2021) {
                curryearList.push(tahun);
            }
        }

        curryearList.push(nextYear)

        const yearsBeforeCurrYear = '[' + curryearList.join(', ') + ']'

        console.log(">>>>>>>>>>>>>>>> bikin kondisi baru disini >>>>>>>>>>>>>>>>>>>>>>")
        let whereClause = {};
        let whereClauseSegment = {}

        if (year != '' && comparison == '') {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('lead_days'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear)
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('lead_days'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('lead_days'), { [Op.ne]: '' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('lead_days'), { [Op.ne]: '' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('lead_days'), { [Op.ne]: '' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('lead_days'), { [Op.ne]: '' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        }


        if (hotel != '') {
            whereClause['room_class'] = hotel;
        }

        whereClause['segment'] = {
            [Op.and]: [
                Sequelize.where(Sequelize.col('marketsegment.segment'), { [Op.eq]: 'DIRECT' }),
            ]
        };


        if (card != '') {  // filter ratecode.card sesudah where
            whereClause['card'] = {
                [Op.and]: [
                    Sequelize.where(Sequelize.col('ratecode.card'), { [Op.eq]: card }),
                ]
            };
        }

        if (level != '') {
            whereClause['mp_level'] = level;
        }

        const include_option = [
            {
                model: MarketSegment,
                as: 'marketsegment',
                attributes: [],  // just make relation not show column
                required: false,  //left outer join; true : inner join
                where: whereClauseSegment
            },
            {
                model: RateCode,
                as: "ratecode",
                attributes: [],
                required: false
            }
        ]

        const attributes = [];
        const years = JSON.parse(yearsBeforeCurrYear);

        for (let i = 0; i < years.length; i++) {
            const year = years[i];

            const attribute = [
                Sequelize.fn(
                    'SUM',
                    Sequelize.literal(
                        "CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " +
                        year +
                        ' THEN `rn` ELSE 0 END'
                    )
                ),
                'room_night_' + year,
            ];

            attributes.push(attribute);
        }

        console.log(whereClause, ">>>>>>>>>>>>>>> ISI KONDISI >>>>>>>>>>>>>>>> ")


        if (year != '' && comparison == '') {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'lead_days',
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'room_night'],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        'lead_days',
                    ],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'lead_days',
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + prevYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + prevYear,
                        ],
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + currYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + currYear,
                        ],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        'lead_days',
                    ],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'lead_days',
                        ...attributes,
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        'lead_days',
                    ],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        }
    };

    // ---------------------------------------------------
    // MP TOP ROOM TYPES
    // ---------------------------------------------------
    static async getMpTopRoomType(req, res) {
        const { hotel, year, comparison, month, card, level } = req.query;

        var currentTime = new Date();
        var currYear = year > 0 ? year : currentTime.getFullYear();
        var prevYear = comparison > 0 ? Number(comparison) : currentTime.getFullYear() - 1;
        var nextYear = currentTime.getFullYear() + 1

        // list 1 years before current year
        const startCurrYear = currYear - 1;
        const curryearList = [];

        for (let tahun = startCurrYear; tahun <= currYear; tahun++) {
            if (tahun != 2020 && tahun != 2021) {
                curryearList.push(tahun);
            }
        }

        curryearList.push(nextYear)

        const yearsBeforeCurrYear = '[' + curryearList.join(', ') + ']'

        console.log(">>>>>>>>>>>>>>>> bikin kondisi baru disini >>>>>>>>>>>>>>>>>>>>>>")
        let whereClause = {};
        let whereClauseSegment = {}

        if (year != '' && comparison == '') {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear)
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        }

        if (hotel != '') {
            whereClause['room_class'] = hotel;
        }

        whereClause['segment'] = {
            [Op.and]: [
                Sequelize.where(Sequelize.col('marketsegment.segment'), { [Op.eq]: 'DIRECT' }),
            ]
        };


        if (card != '') {  // filter ratecode.card sesudah where
            whereClause['card'] = {
                [Op.and]: [
                    Sequelize.where(Sequelize.col('ratecode.card'), { [Op.eq]: card }),
                ]
            };
        }

        if (level != '') {
            whereClause['mp_level'] = level;
        }

        const include_option = [
            {
                model: MarketSegment,
                as: 'marketsegment',
                attributes: [],  // just make relation not show column
                required: false,  //left outer join; true : inner join
                where: whereClauseSegment
            },
            {
                model: RateCode,
                as: "ratecode",
                attributes: [],
                required: false
            },
            {
                model: RoomType,
                as: 'roomtype',
                attributes: [],
                required: true
            }
        ]

        const attributes = [];
        const years = JSON.parse(yearsBeforeCurrYear);

        for (let i = 0; i < years.length; i++) {
            const year = years[i];

            const attribute = [
                Sequelize.fn(
                    'SUM',
                    Sequelize.literal(
                        "CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " +
                        year +
                        ' THEN `rn` ELSE 0 END'
                    )
                ),
                'room_night_' + year,
            ];

            attributes.push(attribute);
        }

        console.log(whereClause, ">>>>>>>>>>>>>>> ISI KONDISI >>>>>>>>>>>>>>>> ")


        if (year != '' && comparison == '') {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'roomtype.room_type',
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'room_night'],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        'roomtype.room_type',
                    ],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'roomtype.room_type',
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + prevYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + prevYear,
                        ],
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + currYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + currYear,
                        ],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        'roomtype.room_type',
                    ],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'roomtype.room_type',
                        ...attributes,
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        'roomtype.room_type',
                    ],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        }
    };

    // ---------------------------------------------------
    // MP TOP COUNTRIES
    // ---------------------------------------------------
    static async getMpTopCountry(req, res) {
        const { hotel, year, comparison, month, card, level } = req.query;

        var currentTime = new Date();
        var currYear = year > 0 ? year : currentTime.getFullYear();
        var prevYear = comparison > 0 ? Number(comparison) : currentTime.getFullYear() - 1;
        var nextYear = currentTime.getFullYear() + 1

        // list 1 years before current year
        const startCurrYear = currYear - 1;
        const curryearList = [];

        for (let tahun = startCurrYear; tahun <= currYear; tahun++) {
            if (tahun != 2020 && tahun != 2021) {
                curryearList.push(tahun);
            }
        }

        curryearList.push(nextYear)

        const yearsBeforeCurrYear = '[' + curryearList.join(', ') + ']'

        console.log(">>>>>>>>>>>>>>>> bikin kondisi baru disini >>>>>>>>>>>>>>>>>>>>>>")
        let whereClause = {};
        let whereClauseSegment = {}

        if (year != '' && comparison == '') {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('country'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear)
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('country'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('country'), { [Op.ne]: '' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('country'), { [Op.ne]: '' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('country'), { [Op.ne]: '' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('country'), { [Op.ne]: '' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        }


        if (hotel != '') {
            whereClause['room_class'] = hotel;
        }

        whereClause['segment'] = {
            [Op.and]: [
                Sequelize.where(Sequelize.col('marketsegment.segment'), { [Op.eq]: 'DIRECT' }),
            ]
        };


        if (card != '') {  // filter ratecode.card sesudah where
            whereClause['card'] = {
                [Op.and]: [
                    Sequelize.where(Sequelize.col('ratecode.card'), { [Op.eq]: card }),
                ]
            };
        }

        if (level != '') {
            whereClause['mp_level'] = level;
        }

        const include_option = [
            {
                model: MarketSegment,
                as: 'marketsegment',
                attributes: [],  // just make relation not show column
                required: false,  //left outer join; true : inner join
                where: whereClauseSegment
            },
            {
                model: RateCode,
                as: "ratecode",
                attributes: [],
                required: false
            }
        ]

        const attributes = [];
        const years = JSON.parse(yearsBeforeCurrYear);

        for (let i = 0; i < years.length; i++) {
            const year = years[i];

            const attribute = [
                Sequelize.fn(
                    'SUM',
                    Sequelize.literal(
                        "CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " +
                        year +
                        ' THEN `rn` ELSE 0 END'
                    )
                ),
                'room_night_' + year,
            ];

            attributes.push(attribute);
        }

        console.log(whereClause, ">>>>>>>>>>>>>>> ISI KONDISI >>>>>>>>>>>>>>>> ")


        if (year != '' && comparison == '') {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'country',
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'room_night'],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        'country',
                    ],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'country',
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + prevYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + prevYear,
                        ],
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + currYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + currYear,
                        ],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        'country',
                    ],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'country',
                        ...attributes,
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        'country',
                    ],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        }
    };

    // ---------------------------------------------------
    // MP SUMMARY
    // ---------------------------------------------------
    static async getMpSummary(req, res) {
        const { hotel, year, comparison, month, card, level } = req.query;

        var currentTime = new Date();
        var currYear = year > 0 ? year : currentTime.getFullYear();
        var prevYear = comparison > 0 ? Number(comparison) : currentTime.getFullYear() - 1;
        var nextYear = currentTime.getFullYear() + 1

        console.log(">>>>>>>>>>>>>>>> bikin kondisi baru disini >>>>>>>>>>>>>>>>>>>>>>")
        let whereClause = {};
        let whereClauseSegment = {}

        if (year != '' && comparison == '') {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear)
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else if (year == '' && comparison != '' || year != '' && comparison != '') {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        }


        if (hotel != '') {
            whereClause['room_class'] = hotel;
        }

        whereClause['segment'] = {
            [Op.and]: [
                Sequelize.where(Sequelize.col('marketsegment.segment'), { [Op.eq]: 'DIRECT' }),
            ]
        };


        if (card != '') {  // filter ratecode.card sesudah where
            whereClause['card'] = {
                [Op.and]: [
                    Sequelize.where(Sequelize.col('ratecode.card'), { [Op.eq]: card }),
                ]
            };
        }

        if (level != '') {
            whereClause['mp_level'] = level;
        }

        const include_option = [
            {
                model: MarketSegment,
                as: 'marketsegment',
                attributes: [],  // just make relation not show column
                required: false,  //left outer join; true : inner join
                where: whereClauseSegment
            },
            {
                model: RateCode,
                as: "ratecode",
                attributes: [],
                required: false
            }
        ]

        // if (description != '' || card != '') {
        //     include_option[0].required = true
        // }

        console.log(whereClause, ">>>>>>>>>>>>>>> ISI KONDISI >>>>>>>>>>>>>>>> ")


        try {

            if (year != '' && comparison == '') {
                const result = await Master.findAll({
                    attributes: [
                        [Sequelize.fn('year', Sequelize.fn('str_to_date', Sequelize.col('stay_date'), '%d/%m/%Y')), 'stay_year'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${currYear} THEN REPLACE(rn, ',', '') ELSE 0 END`)), 'XRN_MTD_CURRYEAR'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${currYear} THEN REPLACE(room_rev_usd, ',', '') ELSE 0 END`)), 'XRREV_MTD_CURRYEAR'],
                    ],
                    include: include_option,

                    where: whereClause,
                    group: [
                        Sequelize.fn('year', Sequelize.fn('str_to_date', Sequelize.col('stay_date'), '%d/%m/%Y'))
                    ],
                    order: [
                        [Sequelize.fn('year', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), 'ASC']
                    ],
                });

                // Manipulasi hasil query untuk menghasilkan output yang diinginkan
                const output = result.map(item => {
                    // console.log(XPU23BEFORE23, " <<<<<< SETIAP ITEM")
                    const stay_year = item.dataValues.stay_year;
                    const XRN_MTD_CURR_YEAR = Number(item.dataValues.XRN_MTD_CURRYEAR);
                    const XRREV_MTD_CURR_YEAR = Number(item.dataValues.XRREV_MTD_CURRYEAR);

                    const XARR_MTD_CURR_YEAR = (XRREV_MTD_CURR_YEAR / XRN_MTD_CURR_YEAR).toFixed(2);

                    const dynamicKey = `XRN_MTD_CURR_${stay_year}`;
                    const dynamicKey2 = `XRREV_MTD_CURR_${stay_year}`;
                    const dynamicKey3 = `XARR_MTD_CURR_${stay_year}`;

                    return {
                        stay_year,
                        [dynamicKey]: XRN_MTD_CURR_YEAR,
                        [dynamicKey2]: XRREV_MTD_CURR_YEAR,
                        [dynamicKey3]: XARR_MTD_CURR_YEAR
                    }
                })

                const modifiedData = output.map(item => {
                    const modifiedItem = {};

                    for (let key in item) {
                        const modifiedKey = key.replace(/(CURR_)/g, "");
                        modifiedItem[modifiedKey] = item[key];
                    }

                    return modifiedItem;
                });

                res.status(200).json(modifiedData)

            } else if (year == '' && comparison != '' || year != '' && comparison != '') {
                const result = await Master.findAll({
                    attributes: [
                        [Sequelize.fn('year', Sequelize.fn('str_to_date', Sequelize.col('stay_date'), '%d/%m/%Y')), 'stay_year'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${prevYear} THEN REPLACE(rn, ',', '') ELSE 0 END`)), 'XRN_MTD_PREVYEAR'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${currYear} THEN REPLACE(rn, ',', '') ELSE 0 END`)), 'XRN_MTD_CURRYEAR'],


                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${prevYear} THEN REPLACE(room_rev_usd, ',', '') ELSE 0 END`)), 'XRREV_MTD_PREVYEAR'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${currYear} THEN REPLACE(room_rev_usd, ',', '') ELSE 0 END`)), 'XRREV_MTD_CURRYEAR'],
                    ],
                    include: include_option,

                    where: whereClause,
                    group: Sequelize.literal('YEAR(STR_TO_DATE(stay_date, "%d/%m/%Y"))'),
                    order: Sequelize.literal('YEAR(STR_TO_DATE(stay_date, "%d/%m/%Y")) ASC'),
                });

                // Manipulasi hasil query untuk menghasilkan output yang diinginkan
                const output = result.map(item => {
                    // console.log(XPU23BEFORE23, " <<<<<< SETIAP ITEM")
                    const stay_year = item.dataValues.stay_year;
                    const XRN_MTD_COMP_YEAR = Number(item.dataValues.XRN_MTD_PREVYEAR);
                    const XRN_MTD_CURR_YEAR = Number(item.dataValues.XRN_MTD_CURRYEAR);


                    const XRREV_MTD_COMP_YEAR = Number(item.dataValues.XRREV_MTD_PREVYEAR);
                    const XRREV_MTD_CURR_YEAR = Number(item.dataValues.XRREV_MTD_CURRYEAR);

                    const XARR_MTD_COMP_YEAR = (XRREV_MTD_COMP_YEAR / XRN_MTD_COMP_YEAR).toFixed(2);
                    const XARR_MTD_CURR_YEAR = (XRREV_MTD_CURR_YEAR / XRN_MTD_CURR_YEAR).toFixed(2);

                    const dynamicKey = `XRN_MTD_COMP_${stay_year}`;
                    const dynamicKey2 = `XRREV_MTD_COMP_${stay_year}`;
                    const dynamicKey3 = `XARR_MTD_COMP_${stay_year}`;
                    const dynamicKey4 = `XRN_MTD_CURR_${stay_year}`;
                    const dynamicKey5 = `XRREV_MTD_CURR_${stay_year}`;
                    const dynamicKey6 = `XARR_MTD_CURR_${stay_year}`;

                    return {
                        stay_year,
                        [dynamicKey]: XRN_MTD_COMP_YEAR,
                        [dynamicKey2]: XRREV_MTD_COMP_YEAR,
                        [dynamicKey3]: XARR_MTD_COMP_YEAR,
                        [dynamicKey4]: XRN_MTD_CURR_YEAR,
                        [dynamicKey5]: XRREV_MTD_CURR_YEAR,
                        [dynamicKey6]: XARR_MTD_CURR_YEAR
                    }

                })

                const filteredOutput = output.map(item => {
                    const filteredItem = { ...item };

                    if (filteredItem.XRN_MTD_COMP_YEAR === 0 || isNaN(parseFloat(filteredItem.XRN_MTD_COMP_YEAR))) {
                        delete filteredItem.XRN_MTD_COMP_YEAR;
                    }

                    if (filteredItem.XRREV_MTD_COMP_YEAR === 0 || isNaN(parseFloat(filteredItem.XRREV_MTD_COMP_YEAR))) {
                        delete filteredItem.XRREV_MTD_COMP_YEAR;
                    }

                    if (filteredItem.XARR_MTD_COMP_YEAR === 'NaN') {
                        delete filteredItem.XARR_MTD_COMP_YEAR;
                    }

                    if (filteredItem.XRN_MTD_CURR_YEAR === 0 || isNaN(parseFloat(filteredItem.XRN_MTD_CURR_YEAR))) {
                        delete filteredItem.XRN_MTD_CURR_YEAR;
                    }

                    if (filteredItem.XRREV_MTD_CURR_YEAR === 0 || isNaN(parseFloat(filteredItem.XRREV_MTD_CURR_YEAR))) {
                        delete filteredItem.XRREV_MTD_CURR_YEAR;
                    }

                    if (filteredItem.XARR_MTD_CURR_YEAR === 'NaN') {
                        delete filteredItem.XARR_MTD_CURR_YEAR;
                    }

                    return filteredItem;
                });

                const modifiedData = filteredOutput.map(item => {
                    const modifiedItem = {};

                    for (let key in item) {
                        const value = item[key];

                        if (value !== "NaN" && value !== 0 && value !== false) {
                            modifiedItem[key] = value;
                        }
                    }

                    return modifiedItem;
                });

                const modifiedData1 = modifiedData.map(item => {
                    const modifiedItem = {};

                    for (let key in item) {
                        const modifiedKey = key.replace(/(COMP_|CURR_)/g, "");
                        modifiedItem[modifiedKey] = item[key];
                    }

                    return modifiedItem;
                });

                res.status(200).json(modifiedData1)

            } else {
                const result = await Master.findAll({
                    attributes: [
                        [Sequelize.fn('year', Sequelize.fn('str_to_date', Sequelize.col('stay_date'), '%d/%m/%Y')), 'stay_year'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${prevYear} THEN REPLACE(rn, ',', '') ELSE 0 END`)), 'XRN_MTD_PREVYEAR'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${currYear} THEN REPLACE(rn, ',', '') ELSE 0 END`)), 'XRN_MTD_CURRYEAR'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${nextYear} THEN REPLACE(rn, ',', '') ELSE 0 END`)), 'XRN_MTD_NEXTYEAR'],

                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${prevYear} THEN REPLACE(room_rev_usd, ',', '') ELSE 0 END`)), 'XRREV_MTD_PREVYEAR'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${currYear} THEN REPLACE(room_rev_usd, ',', '') ELSE 0 END`)), 'XRREV_MTD_CURRYEAR'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${nextYear} THEN REPLACE(room_rev_usd, ',', '') ELSE 0 END`)), 'XRREV_MTD_NEXTYEAR'],
                    ],
                    include: include_option,

                    where: whereClause,
                    group: Sequelize.literal('YEAR(STR_TO_DATE(stay_date, "%d/%m/%Y"))'),
                    order: Sequelize.literal('YEAR(STR_TO_DATE(stay_date, "%d/%m/%Y")) ASC'),
                });

                // Manipulasi hasil query untuk menghasilkan output yang diinginkan
                const output = result.map(item => {
                    // console.log(XPU23BEFORE23, " <<<<<< SETIAP ITEM")
                    const stay_year = item.dataValues.stay_year;
                    const XRN_MTD_PREV_YEAR = Number(item.dataValues.XRN_MTD_PREVYEAR);
                    const XRN_MTD_CURR_YEAR = Number(item.dataValues.XRN_MTD_CURRYEAR);
                    const XRN_MTD_NEXT_YEAR = Number(item.dataValues.XRN_MTD_NEXTYEAR);

                    const XRREV_MTD_PREV_YEAR = Number(item.dataValues.XRREV_MTD_PREVYEAR);
                    const XRREV_MTD_CURR_YEAR = Number(item.dataValues.XRREV_MTD_CURRYEAR);
                    const XRREV_MTD_NEXT_YEAR = Number(item.dataValues.XRREV_MTD_NEXTYEAR);

                    const XARR_MTD_PREV_YEAR = (XRREV_MTD_PREV_YEAR / XRN_MTD_PREV_YEAR).toFixed(2);
                    const XARR_MTD_CURR_YEAR = (XRREV_MTD_CURR_YEAR / XRN_MTD_CURR_YEAR).toFixed(2);
                    const XARR_MTD_NEXT_YEAR = (XRREV_MTD_NEXT_YEAR / XRN_MTD_NEXT_YEAR).toFixed(2);

                    const dynamicKey = `XRN_MTD_PREV_${stay_year}`;
                    const dynamicKey2 = `XRREV_MTD_PREV_${stay_year}`;
                    const dynamicKey3 = `XARR_MTD_PREV_${stay_year}`;
                    const dynamicKey4 = `XRN_MTD_CURR_${stay_year}`;
                    const dynamicKey5 = `XRREV_MTD_CURR_${stay_year}`;
                    const dynamicKey6 = `XARR_MTD_CURR_${stay_year}`;
                    const dynamicKey7 = `XRN_MTD_NEXT_${stay_year}`;
                    const dynamicKey8 = `XRREV_MTD_NEXT_${stay_year}`;
                    const dynamicKey9 = `XARR_MTD_NEXT_${stay_year}`;

                    return {
                        stay_year,
                        [dynamicKey]: XRN_MTD_PREV_YEAR,
                        [dynamicKey2]: XRREV_MTD_PREV_YEAR,
                        [dynamicKey3]: XARR_MTD_PREV_YEAR,
                        [dynamicKey4]: XRN_MTD_CURR_YEAR,
                        [dynamicKey5]: XRREV_MTD_CURR_YEAR,
                        [dynamicKey6]: XARR_MTD_CURR_YEAR,
                        [dynamicKey7]: XRN_MTD_NEXT_YEAR,
                        [dynamicKey8]: XRREV_MTD_NEXT_YEAR,
                        [dynamicKey9]: XARR_MTD_NEXT_YEAR
                    }

                })

                const filteredOutput = output.map(item => {
                    const filteredItem = { ...item };

                    if (filteredItem.XRN_MTD_PREV_YEAR === 0 || isNaN(parseFloat(filteredItem.XRN_MTD_PREV_YEAR))) {
                        delete filteredItem.XRN_MTD_PREV_YEAR;
                    }

                    if (filteredItem.XRREV_MTD_PREV_YEAR === 0 || isNaN(parseFloat(filteredItem.XRREV_MTD_PREV_YEAR))) {
                        delete filteredItem.XRREV_MTD_PREV_YEAR;
                    }

                    if (filteredItem.XARR_MTD_PREV_YEAR === 'NaN') {
                        delete filteredItem.XARR_MTD_PREV_YEAR;
                    }

                    if (filteredItem.XRN_MTD_CURR_YEAR === 0 || isNaN(parseFloat(filteredItem.XRN_MTD_CURR_YEAR))) {
                        delete filteredItem.XRN_MTD_CURR_YEAR;
                    }

                    if (filteredItem.XRREV_MTD_CURR_YEAR === 0 || isNaN(parseFloat(filteredItem.XRREV_MTD_CURR_YEAR))) {
                        delete filteredItem.XRREV_MTD_CURR_YEAR;
                    }

                    if (filteredItem.XARR_MTD_CURR_YEAR === 'NaN') {
                        delete filteredItem.XARR_MTD_CURR_YEAR;
                    }

                    if (filteredItem.XRN_MTD_NEXT_YEAR === 0 || isNaN(parseFloat(filteredItem.XRN_MTD_NEXT_YEAR))) {
                        delete filteredItem.XRN_MTD_NEXT_YEAR;
                    }

                    if (filteredItem.XRREV_MTD_NEXT_YEAR === 0 || isNaN(parseFloat(filteredItem.XRREV_MTD_NEXT_YEAR))) {
                        delete filteredItem.XRREV_MTD_NEXT_YEAR;
                    }


                    if (filteredItem.XARR_MTD_NEXT_YEAR === 'NaN') {
                        delete filteredItem.XARR_MTD_NEXT_YEAR;
                    }

                    return filteredItem;
                });

                const filteredYears = [2019, 2020, 2021]; // Daftar tahun yang ingin Anda filter

                const filteredData = filteredOutput.filter(item => !filteredYears.includes(item.stay_year));

                const modifiedData = filteredData.map(item => {
                    const modifiedItem = {};

                    for (let key in item) {
                        const value = item[key];

                        if (value !== "NaN" && value !== 0 && value !== false) {
                            modifiedItem[key] = value;
                        }
                    }

                    return modifiedItem;
                });

                const modifiedData1 = modifiedData.map(item => {
                    const modifiedItem = {};

                    for (let key in item) {
                        const modifiedKey = key.replace(/(PREV_|CURR_|NEXT_)/g, "");
                        modifiedItem[modifiedKey] = item[key];
                    }

                    return modifiedItem;
                });

                res.status(200).json(modifiedData1)
            }

        } catch (error) {
            console.log(error);
        }
    };

    // ---------------------------------------------------
    // COUNTRY MTD DAILY ON THE BOOK
    // ---------------------------------------------------
    static async getCountryMtdDailyBooking(req, res) {
        const { hotel, year, comparison, month, country } = req.query;

        var currentTime = new Date();
        var currYear = year > 0 ? year : currentTime.getFullYear();
        var prevYear = comparison > 0 ? Number(comparison) : currentTime.getFullYear() - 1;
        var nextYear = currentTime.getFullYear() + 1

        // list 1 years before current year
        const startCurrYear = currYear - 1;
        const curryearList = [];

        for (let tahun = startCurrYear; tahun <= currYear; tahun++) {
            if (tahun != 2020 && tahun != 2021) {
                curryearList.push(tahun);
            }
        }

        curryearList.push(nextYear)

        const yearsBeforeCurrYear = '[' + curryearList.join(', ') + ']'

        console.log(">>>>>>>>>>>>>>>> bikin kondisi baru disini >>>>>>>>>>>>>>>>>>>>>>")
        let whereClause = {};

        if (year != '' && comparison == '') {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear)
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        }


        if (hotel != '') {
            whereClause['room_class'] = hotel;
        }

        if (country != '') {
            whereClause['country'] = country;
        }

        const attributes = [];
        const years = JSON.parse(yearsBeforeCurrYear);

        for (let i = 0; i < years.length; i++) {
            const year = years[i];

            const attribute = [
                Sequelize.fn(
                    'SUM',
                    Sequelize.literal(
                        "CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " +
                        year +
                        ' THEN `rn` ELSE 0 END'
                    )
                ),
                'room_night_' + year,
            ];

            attributes.push(attribute);
        }

        console.log(whereClause, ">>>>>>>>>>>>>>> ISI KONDISI >>>>>>>>>>>>>>>> ")


        if (year != '' && comparison == '') {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        [Sequelize.literal("RIGHT(STR_TO_DATE(stay_date, '%d/%m/%Y'), 2)"), "day"],
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'room_night'],
                    ],
                    where: whereClause,
                    group: [
                        Sequelize.literal("RIGHT(STR_TO_DATE(stay_date, '%d/%m/%Y'), 2)"),
                    ],
                    order: [
                        [Sequelize.literal("RIGHT(STR_TO_DATE(stay_date, '%d/%m/%Y'), 2)"), 'ASC'],
                    ],
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        [Sequelize.literal("RIGHT(STR_TO_DATE(stay_date, '%d/%m/%Y'), 2)"), "day"],
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + prevYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + prevYear,
                        ],
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + currYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + currYear,
                        ],
                    ],
                    where: whereClause,
                    group: [
                        Sequelize.literal("RIGHT(STR_TO_DATE(stay_date, '%d/%m/%Y'), 2)"),
                    ],
                    order: [
                        [Sequelize.literal("RIGHT(STR_TO_DATE(stay_date, '%d/%m/%Y'), 2)"), 'ASC'],
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }

        } else {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        [Sequelize.literal("RIGHT(STR_TO_DATE(stay_date, '%d/%m/%Y'), 2)"), "day"],
                        ...attributes,

                    ],
                    where: whereClause,
                    group: [
                        Sequelize.literal("RIGHT(STR_TO_DATE(stay_date, '%d/%m/%Y'), 2)"),
                    ],
                    order: [
                        [Sequelize.literal("RIGHT(STR_TO_DATE(stay_date, '%d/%m/%Y'), 2)"), 'ASC'],
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        }
    };

    // ---------------------------------------------------
    // COUNTRY YTD DAILY ON THE BOOK
    // ---------------------------------------------------
    static async getCountryDailyBooking(req, res) {
        const { hotel, year, comparison, month, country } = req.query;

        var currentTime = new Date();
        var currYear = year > 0 ? year : currentTime.getFullYear();
        var prevYear = comparison > 0 ? Number(comparison) : currentTime.getFullYear() - 1;
        var nextYear = currentTime.getFullYear() + 1

        // list 1 years before current year
        const startCurrYear = currYear - 1;
        const curryearList = [];

        for (let tahun = startCurrYear; tahun <= currYear; tahun++) {
            if (tahun != 2020 && tahun != 2021) {
                curryearList.push(tahun);
            }
        }

        curryearList.push(nextYear)

        const yearsBeforeCurrYear = '[' + curryearList.join(', ') + ']'

        console.log(">>>>>>>>>>>>>>>> bikin kondisi baru disini >>>>>>>>>>>>>>>>>>>>>>")
        let whereClause = {};

        if (year != '' && comparison == '') {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear)
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        }


        if (hotel != '') {
            whereClause['room_class'] = hotel;
        }

        if (country != '') {
            whereClause['country'] = country;
        }

        const attributes = [];
        const years = JSON.parse(yearsBeforeCurrYear);

        for (let i = 0; i < years.length; i++) {
            const year = years[i];

            const attribute = [
                Sequelize.fn(
                    'SUM',
                    Sequelize.literal(
                        "CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " +
                        year +
                        ' THEN `rn` ELSE 0 END'
                    )
                ),
                'room_night_' + year,
            ];

            attributes.push(attribute);
        }

        console.log(whereClause, ">>>>>>>>>>>>>>> ISI KONDISI >>>>>>>>>>>>>>>> ")


        if (year != '' && comparison == '') {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        [Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), 'stay_month'],
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'room_night'],
                    ],
                    where: whereClause,
                    group: [
                        Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')),
                    ],
                    order: [
                        [Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), 'ASC'],
                    ],
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        [Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), 'stay_month'],
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + prevYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + prevYear,
                        ],
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + currYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + currYear,
                        ],
                    ],
                    where: whereClause,
                    group: [
                        Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')),
                    ],
                    order: [
                        [Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), 'ASC'],
                    ],
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        [Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), 'stay_month'],
                        ...attributes,
                    ],
                    where: whereClause,
                    group: [
                        Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')),
                    ],
                    order: [
                        [Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), 'ASC'],
                    ],
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        }
    };

    // ---------------------------------------------------
    // COUNTRY TOP RATE CODES
    // ---------------------------------------------------
    static async getCountryTopRateCode(req, res) {
        const { hotel, year, comparison, month, country } = req.query;

        var currentTime = new Date();
        var currYear = year > 0 ? year : currentTime.getFullYear();
        var prevYear = comparison > 0 ? Number(comparison) : currentTime.getFullYear() - 1;
        var nextYear = currentTime.getFullYear() + 1

        // list 1 years before current year
        const startCurrYear = currYear - 1;
        const curryearList = [];

        for (let tahun = startCurrYear; tahun <= currYear; tahun++) {
            if (tahun != 2020 && tahun != 2021) {
                curryearList.push(tahun);
            }
        }

        curryearList.push(nextYear)

        const yearsBeforeCurrYear = '[' + curryearList.join(', ') + ']'

        console.log(">>>>>>>>>>>>>>>> bikin kondisi baru disini >>>>>>>>>>>>>>>>>>>>>>")
        let whereClause = {};

        if (year != '' && comparison == '') {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear)
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        }


        if (hotel != '') {
            whereClause['room_class'] = hotel;
        }


        if (country != '') {
            whereClause['country'] = country;
        }

        const include_option = [
            {
                model: RateCode,
                as: "ratecode",
                attributes: [],
                required: false
            }
        ]

        const attributes = [];
        const years = JSON.parse(yearsBeforeCurrYear);

        for (let i = 0; i < years.length; i++) {
            const year = years[i];

            const attribute = [
                Sequelize.fn(
                    'SUM',
                    Sequelize.literal(
                        "CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " +
                        year +
                        ' THEN `rn` ELSE 0 END'
                    )
                ),
                'room_night_' + year,
            ];

            attributes.push(attribute);
        }

        console.log(whereClause, ">>>>>>>>>>>>>>> ISI KONDISI >>>>>>>>>>>>>>>> ")


        if (year != '' && comparison == '') {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'ratecode.rate_description2',
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'room_night'],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        'ratecode.rate_description2',
                    ],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'ratecode.rate_description2',
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + prevYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + prevYear,
                        ],
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + currYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + currYear,
                        ],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        'ratecode.rate_description2',
                    ],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'ratecode.rate_description2',
                        ...attributes,
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        'ratecode.rate_description2',
                    ],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        }
    };

    // ---------------------------------------------------
    // COUNTRY TOP LENGTH OF STAY
    // ---------------------------------------------------
    static async getCountryTopLengthStay(req, res) {
        const { hotel, year, comparison, month, country } = req.query;

        var currentTime = new Date();
        var currYear = year > 0 ? year : currentTime.getFullYear();
        var prevYear = comparison > 0 ? Number(comparison) : currentTime.getFullYear() - 1;
        var nextYear = currentTime.getFullYear() + 1

        // list 1 years before current year
        const startCurrYear = currYear - 1;
        const curryearList = [];

        for (let tahun = startCurrYear; tahun <= currYear; tahun++) {
            if (tahun != 2020 && tahun != 2021) {
                curryearList.push(tahun);
            }
        }

        curryearList.push(nextYear)

        const yearsBeforeCurrYear = '[' + curryearList.join(', ') + ']'

        console.log(">>>>>>>>>>>>>>>> bikin kondisi baru disini >>>>>>>>>>>>>>>>>>>>>>")
        let whereClause = {};

        if (year != '' && comparison == '') {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('los'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear)
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('los'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('los'), { [Op.ne]: '' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('los'), { [Op.ne]: '' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('los'), { [Op.ne]: '' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('los'), { [Op.ne]: '' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        }


        if (hotel != '') {
            whereClause['room_class'] = hotel;
        }


        if (country != '') {
            whereClause['country'] = country;
        }

        const include_option = [
            {
                model: RateCode,
                as: "ratecode",
                attributes: [],
                required: false
            }
        ]

        const attributes = [];
        const years = JSON.parse(yearsBeforeCurrYear);

        for (let i = 0; i < years.length; i++) {
            const year = years[i];

            const attribute = [
                Sequelize.fn(
                    'SUM',
                    Sequelize.literal(
                        "CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " +
                        year +
                        ' THEN `rn` ELSE 0 END'
                    )
                ),
                'room_night_' + year,
            ];

            attributes.push(attribute);
        }

        console.log(whereClause, ">>>>>>>>>>>>>>> ISI KONDISI >>>>>>>>>>>>>>>> ")


        if (year != '' && comparison == '') {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'los',
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'room_night'],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        'los',
                    ],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'los',
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + prevYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + prevYear,
                        ],
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + currYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + currYear,
                        ],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        'los',
                    ],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'los',
                        ...attributes,
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        'los',
                    ],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        }
    };

    // ---------------------------------------------------
    // COUNTRY TOP BOOKING LEAD TIME
    // ---------------------------------------------------
    static async getCountryTopBookingLeadTime(req, res) {
        const { hotel, year, comparison, month, country } = req.query;

        var currentTime = new Date();
        var currYear = year > 0 ? year : currentTime.getFullYear();
        var prevYear = comparison > 0 ? Number(comparison) : currentTime.getFullYear() - 1;
        var nextYear = currentTime.getFullYear() + 1

        // list 1 years before current year
        const startCurrYear = currYear - 1;
        const curryearList = [];

        for (let tahun = startCurrYear; tahun <= currYear; tahun++) {
            if (tahun != 2020 && tahun != 2021) {
                curryearList.push(tahun);
            }
        }

        curryearList.push(nextYear)

        const yearsBeforeCurrYear = '[' + curryearList.join(', ') + ']'

        console.log(">>>>>>>>>>>>>>>> bikin kondisi baru disini >>>>>>>>>>>>>>>>>>>>>>")
        let whereClause = {};

        if (year != '' && comparison == '') {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('lead_days'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), year)
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('lead_days'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), year),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('lead_days'), { [Op.ne]: '' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('lead_days'), { [Op.ne]: '' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('lead_days'), { [Op.ne]: '' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('lead_days'), { [Op.ne]: '' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        }


        if (hotel != '') {
            whereClause['room_class'] = hotel;
        }


        if (country != '') {
            whereClause['country'] = country;
        }

        const include_option = [
            {
                model: RateCode,
                as: "ratecode",
                attributes: [],
                required: false
            }
        ]

        const attributes = [];
        const years = JSON.parse(yearsBeforeCurrYear);

        for (let i = 0; i < years.length; i++) {
            const year = years[i];

            const attribute = [
                Sequelize.fn(
                    'SUM',
                    Sequelize.literal(
                        "CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " +
                        year +
                        ' THEN `rn` ELSE 0 END'
                    )
                ),
                'room_night_' + year,
            ];

            attributes.push(attribute);
        }

        console.log(whereClause, ">>>>>>>>>>>>>>> ISI KONDISI >>>>>>>>>>>>>>>> ")


        if (year != '' && comparison == '') {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'lead_days',
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'room_night'],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        'lead_days',
                    ],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'lead_days',
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + prevYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + prevYear,
                        ],
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + currYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + currYear,
                        ],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        'lead_days',
                    ],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'lead_days',
                        ...attributes,
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        'lead_days',
                    ],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        }
    };

    // ---------------------------------------------------
    // COUNTRY TOP CITIES
    // ---------------------------------------------------
    static async getCountryTopCity(req, res) {
        const { hotel, year, comparison, month, country } = req.query;

        var currentTime = new Date();
        var currYear = year > 0 ? year : currentTime.getFullYear();
        var prevYear = comparison > 0 ? Number(comparison) : currentTime.getFullYear() - 1;
        var nextYear = currentTime.getFullYear() + 1

        // list 1 years before current year
        const startCurrYear = currYear - 1;
        const curryearList = [];

        for (let tahun = startCurrYear; tahun <= currYear; tahun++) {
            if (tahun != 2020 && tahun != 2021) {
                curryearList.push(tahun);
            }
        }

        curryearList.push(nextYear)

        const yearsBeforeCurrYear = '[' + curryearList.join(', ') + ']'

        console.log(">>>>>>>>>>>>>>>> bikin kondisi baru disini >>>>>>>>>>>>>>>>>>>>>>")
        let whereClause = {};

        if (year != '' && comparison == '') {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('city'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear)
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('city'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('city'), { [Op.ne]: '' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('city'), { [Op.ne]: '' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('city'), { [Op.ne]: '' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('city'), { [Op.ne]: '' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        }


        if (hotel != '') {
            whereClause['room_class'] = hotel;
        }


        if (country != '') {
            whereClause['country'] = country;
        }

        const include_option = [
            {
                model: RateCode,
                as: "ratecode",
                attributes: [],
                required: false
            }
        ]

        const attributes = [];
        const years = JSON.parse(yearsBeforeCurrYear);

        for (let i = 0; i < years.length; i++) {
            const year = years[i];

            const attribute = [
                Sequelize.fn(
                    'SUM',
                    Sequelize.literal(
                        "CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " +
                        year +
                        ' THEN `rn` ELSE 0 END'
                    )
                ),
                'room_night_' + year,
            ];

            attributes.push(attribute);
        }

        console.log(whereClause, ">>>>>>>>>>>>>>> ISI KONDISI >>>>>>>>>>>>>>>> ")


        if (year != '' && comparison == '') {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'city',
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'room_night'],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        'city',
                    ],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'city',
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + prevYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + prevYear,
                        ],
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + currYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + currYear,
                        ],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        'city',
                    ],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'city',
                        ...attributes,
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        'city',
                    ],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        }
    };

    // ---------------------------------------------------
    // COUNTRY TOP SUPPORTERS
    // ---------------------------------------------------
    static async getCountryTopSupporter(req, res) {
        const { hotel, year, comparison, month, country } = req.query;

        var currentTime = new Date();
        var currYear = year > 0 ? year : currentTime.getFullYear();
        var prevYear = comparison > 0 ? Number(comparison) : currentTime.getFullYear() - 1;
        var nextYear = currentTime.getFullYear() + 1

        // list 1 years before current year
        const startCurrYear = currYear - 1;
        const curryearList = [];

        for (let tahun = startCurrYear; tahun <= currYear; tahun++) {
            if (tahun != 2020 && tahun != 2021) {
                curryearList.push(tahun);
            }
        }

        curryearList.push(nextYear)

        const yearsBeforeCurrYear = '[' + curryearList.join(', ') + ']'

        console.log(">>>>>>>>>>>>>>>> bikin kondisi baru disini >>>>>>>>>>>>>>>>>>>>>>")
        let whereClause = {};

        if (year != '' && comparison == '') {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear)
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.ne]: '' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.ne]: '' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.ne]: '' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.ne]: '' }),
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        }


        if (hotel != '') {
            whereClause['room_class'] = hotel;
        }


        if (country != '') {
            whereClause['country'] = country;
        }

        const include_option = [
            {
                model: RateCode,
                as: "ratecode",
                attributes: [],
                required: false
            }
        ]

        const attributes = [];
        const years = JSON.parse(yearsBeforeCurrYear);

        for (let i = 0; i < years.length; i++) {
            const year = years[i];

            const attribute = [
                Sequelize.fn(
                    'SUM',
                    Sequelize.literal(
                        "CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " +
                        year +
                        ' THEN `rn` ELSE 0 END'
                    )
                ),
                'room_night_' + year,
            ];

            attributes.push(attribute);
        }

        console.log(whereClause, ">>>>>>>>>>>>>>> ISI KONDISI >>>>>>>>>>>>>>>> ")


        if (year != '' && comparison == '') {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'agent_name',
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'room_night'],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        'agent_name',
                    ],
                    order: [
                        [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'agent_name',
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + prevYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + prevYear,
                        ],
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + currYear + " THEN `rn` ELSE 0 END")
                            ),
                            "room_night_" + currYear,
                        ],
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        'agent_name',
                    ],
                    order: [
                        [Sequelize.fn(
                            "SUM",
                            Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + currYear + " THEN `rn` ELSE 0 END")
                        ), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        'agent_name',
                        ...attributes,
                    ],
                    include: include_option,
                    where: whereClause,
                    group: [
                        'agent_name',
                    ],
                    order: [
                        [Sequelize.fn(
                            "SUM",
                            Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + currYear + " THEN `rn` ELSE 0 END")
                        ), 'DESC']
                    ],
                    limit: 10,
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        }
    };

    // ---------------------------------------------------
    // COUNTRY SUMMARY
    // ---------------------------------------------------
    static async getCountrySummary(req, res) {
        const { hotel, year, comparison, month, country } = req.query;

        var currentTime = new Date();
        var currYear = year > 0 ? year : currentTime.getFullYear();
        var prevYear = comparison > 0 ? Number(comparison) : currentTime.getFullYear() - 1;
        var nextYear = currentTime.getFullYear() + 1

        console.log(">>>>>>>>>>>>>>>> bikin kondisi baru disini >>>>>>>>>>>>>>>>>>>>>>")
        let whereClause = {};

        if (year != '' && comparison == '') {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear)
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else if (year == '' && comparison != '' || year != '' && comparison != '') {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        } else {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    [Op.and]: [
                        Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), month)
                    ]
                };
            }
        }


        if (hotel != '') {
            whereClause['room_class'] = hotel;
        }


        if (country != '') {
            whereClause['country'] = country;
        }

        const include_option = [
            {
                model: RateCode,
                as: "ratecode",
                attributes: [],
                required: false
            }
        ]

        console.log(whereClause, ">>>>>>>>>>>>>>> ISI KONDISI >>>>>>>>>>>>>>>> ")


        try {

            if (year != '' && comparison == '') {
                const result = await Master.findAll({
                    attributes: [
                        [Sequelize.fn('year', Sequelize.fn('str_to_date', Sequelize.col('stay_date'), '%d/%m/%Y')), 'stay_year'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${currYear} THEN REPLACE(rn, ',', '') ELSE 0 END`)), 'XRN_MTD_CURRYEAR'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${currYear} THEN REPLACE(room_rev_usd, ',', '') ELSE 0 END`)), 'XRREV_MTD_CURRYEAR'],
                    ],
                    include: include_option,

                    where: whereClause,
                    group: [
                        Sequelize.fn('year', Sequelize.fn('str_to_date', Sequelize.col('stay_date'), '%d/%m/%Y'))
                    ],
                    order: [
                        [Sequelize.fn('year', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), 'ASC']
                    ],
                });

                // Manipulasi hasil query untuk menghasilkan output yang diinginkan
                const output = result.map(item => {
                    // console.log(XPU23BEFORE23, " <<<<<< SETIAP ITEM")
                    const stay_year = item.dataValues.stay_year;
                    const XRN_MTD_CURR_YEAR = Number(item.dataValues.XRN_MTD_CURRYEAR);
                    const XRREV_MTD_CURR_YEAR = Number(item.dataValues.XRREV_MTD_CURRYEAR);

                    const XARR_MTD_CURR_YEAR = (XRREV_MTD_CURR_YEAR / XRN_MTD_CURR_YEAR).toFixed(2);

                    const dynamicKey = `XRN_MTD_CURR_${stay_year}`;
                    const dynamicKey2 = `XRREV_MTD_CURR_${stay_year}`;
                    const dynamicKey3 = `XARR_MTD_CURR_${stay_year}`;

                    return {
                        stay_year,
                        [dynamicKey]: XRN_MTD_CURR_YEAR,
                        [dynamicKey2]: XRREV_MTD_CURR_YEAR,
                        [dynamicKey3]: XARR_MTD_CURR_YEAR
                    }
                })

                const modifiedData = output.map(item => {
                    const modifiedItem = {};

                    for (let key in item) {
                        const modifiedKey = key.replace(/(CURR_)/g, "");
                        modifiedItem[modifiedKey] = item[key];
                    }

                    return modifiedItem;
                });

                res.status(200).json(modifiedData)

            } else if (year == '' && comparison != '' || year != '' && comparison != '') {
                const result = await Master.findAll({
                    attributes: [
                        [Sequelize.fn('year', Sequelize.fn('str_to_date', Sequelize.col('stay_date'), '%d/%m/%Y')), 'stay_year'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${prevYear} THEN REPLACE(rn, ',', '') ELSE 0 END`)), 'XRN_MTD_PREVYEAR'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${currYear} THEN REPLACE(rn, ',', '') ELSE 0 END`)), 'XRN_MTD_CURRYEAR'],

                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${prevYear} THEN REPLACE(room_rev_usd, ',', '') ELSE 0 END`)), 'XRREV_MTD_PREVYEAR'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${currYear} THEN REPLACE(room_rev_usd, ',', '') ELSE 0 END`)), 'XRREV_MTD_CURRYEAR'],
                    ],
                    include: include_option,

                    where: whereClause,
                    group: Sequelize.literal('YEAR(STR_TO_DATE(stay_date, "%d/%m/%Y"))'),
                    order: Sequelize.literal('YEAR(STR_TO_DATE(stay_date, "%d/%m/%Y")) ASC'),
                });

                // Manipulasi hasil query untuk menghasilkan output yang diinginkan
                const output = result.map(item => {
                    // console.log(XPU23BEFORE23, " <<<<<< SETIAP ITEM")
                    const stay_year = item.dataValues.stay_year;
                    const XRN_MTD_COMP_YEAR = Number(item.dataValues.XRN_MTD_PREVYEAR);
                    const XRN_MTD_CURR_YEAR = Number(item.dataValues.XRN_MTD_CURRYEAR);

                    const XRREV_MTD_COMP_YEAR = Number(item.dataValues.XRREV_MTD_PREVYEAR);
                    const XRREV_MTD_CURR_YEAR = Number(item.dataValues.XRREV_MTD_CURRYEAR);

                    const XARR_MTD_COMP_YEAR = (XRREV_MTD_COMP_YEAR / XRN_MTD_COMP_YEAR).toFixed(2);
                    const XARR_MTD_CURR_YEAR = (XRREV_MTD_CURR_YEAR / XRN_MTD_CURR_YEAR).toFixed(2);

                    const dynamicKey = `XRN_MTD_COMP_${stay_year}`;
                    const dynamicKey2 = `XRREV_MTD_COMP_${stay_year}`;
                    const dynamicKey3 = `XARR_MTD_COMP_${stay_year}`;
                    const dynamicKey4 = `XRN_MTD_CURR_${stay_year}`;
                    const dynamicKey5 = `XRREV_MTD_CURR_${stay_year}`;
                    const dynamicKey6 = `XARR_MTD_CURR_${stay_year}`;

                    return {
                        stay_year,
                        [dynamicKey]: XRN_MTD_COMP_YEAR,
                        [dynamicKey2]: XRREV_MTD_COMP_YEAR,
                        [dynamicKey3]: XARR_MTD_COMP_YEAR,
                        [dynamicKey4]: XRN_MTD_CURR_YEAR,
                        [dynamicKey5]: XRREV_MTD_CURR_YEAR,
                        [dynamicKey6]: XARR_MTD_CURR_YEAR
                    }

                })

                const filteredOutput = output.map(item => {
                    const filteredItem = { ...item };

                    if (filteredItem.XRN_MTD_COMP_YEAR === 0 || isNaN(parseFloat(filteredItem.XRN_MTD_COMP_YEAR))) {
                        delete filteredItem.XRN_MTD_COMP_YEAR;
                    }

                    if (filteredItem.XRREV_MTD_COMP_YEAR === 0 || isNaN(parseFloat(filteredItem.XRREV_MTD_COMP_YEAR))) {
                        delete filteredItem.XRREV_MTD_COMP_YEAR;
                    }

                    if (filteredItem.XARR_MTD_COMP_YEAR === 'NaN') {
                        delete filteredItem.XARR_MTD_COMP_YEAR;
                    }

                    if (filteredItem.XRN_MTD_CURR_YEAR === 0 || isNaN(parseFloat(filteredItem.XRN_MTD_CURR_YEAR))) {
                        delete filteredItem.XRN_MTD_CURR_YEAR;
                    }

                    if (filteredItem.XRREV_MTD_CURR_YEAR === 0 || isNaN(parseFloat(filteredItem.XRREV_MTD_CURR_YEAR))) {
                        delete filteredItem.XRREV_MTD_CURR_YEAR;
                    }

                    if (filteredItem.XARR_MTD_CURR_YEAR === 'NaN') {
                        delete filteredItem.XARR_MTD_CURR_YEAR;
                    }

                    return filteredItem;
                });

                const modifiedData = filteredOutput.map(item => {
                    const modifiedItem = {};

                    for (let key in item) {
                        const value = item[key];

                        if (value !== "NaN" && value !== 0 && value !== false) {
                            modifiedItem[key] = value;
                        }
                    }

                    return modifiedItem;
                });

                const modifiedData1 = modifiedData.map(item => {
                    const modifiedItem = {};

                    for (let key in item) {
                        const modifiedKey = key.replace(/(COMP_|CURR_)/g, "");
                        modifiedItem[modifiedKey] = item[key];
                    }

                    return modifiedItem;
                });

                res.status(200).json(modifiedData1)

            } else {
                const result = await Master.findAll({
                    attributes: [
                        [Sequelize.fn('year', Sequelize.fn('str_to_date', Sequelize.col('stay_date'), '%d/%m/%Y')), 'stay_year'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${prevYear} THEN REPLACE(rn, ',', '') ELSE 0 END`)), 'XRN_MTD_PREVYEAR'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${currYear} THEN REPLACE(rn, ',', '') ELSE 0 END`)), 'XRN_MTD_CURRYEAR'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${nextYear} THEN REPLACE(rn, ',', '') ELSE 0 END`)), 'XRN_MTD_NEXTYEAR'],

                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${prevYear} THEN REPLACE(room_rev_usd, ',', '') ELSE 0 END`)), 'XRREV_MTD_PREVYEAR'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${currYear} THEN REPLACE(room_rev_usd, ',', '') ELSE 0 END`)), 'XRREV_MTD_CURRYEAR'],
                        [Sequelize.fn('SUM', Sequelize.literal(`CASE WHEN YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) = ${nextYear} THEN REPLACE(room_rev_usd, ',', '') ELSE 0 END`)), 'XRREV_MTD_NEXTYEAR'],
                    ],
                    include: include_option,

                    where: whereClause,
                    group: Sequelize.literal('YEAR(STR_TO_DATE(stay_date, "%d/%m/%Y"))'),
                    order: Sequelize.literal('YEAR(STR_TO_DATE(stay_date, "%d/%m/%Y")) ASC'),
                });


                // Manipulasi hasil query untuk menghasilkan output yang diinginkan
                const output = result.map(item => {
                    // console.log(XPU23BEFORE23, " <<<<<< SETIAP ITEM")
                    const stay_year = item.dataValues.stay_year;
                    const XRN_MTD_PREV_YEAR = Number(item.dataValues.XRN_MTD_PREVYEAR);
                    const XRN_MTD_CURR_YEAR = Number(item.dataValues.XRN_MTD_CURRYEAR);
                    const XRN_MTD_NEXT_YEAR = Number(item.dataValues.XRN_MTD_NEXTYEAR);

                    const XRREV_MTD_PREV_YEAR = Number(item.dataValues.XRREV_MTD_PREVYEAR);
                    const XRREV_MTD_CURR_YEAR = Number(item.dataValues.XRREV_MTD_CURRYEAR);
                    const XRREV_MTD_NEXT_YEAR = Number(item.dataValues.XRREV_MTD_NEXTYEAR);

                    const XARR_MTD_PREV_YEAR = (XRREV_MTD_PREV_YEAR / XRN_MTD_PREV_YEAR).toFixed(2);
                    const XARR_MTD_CURR_YEAR = (XRREV_MTD_CURR_YEAR / XRN_MTD_CURR_YEAR).toFixed(2);
                    const XARR_MTD_NEXT_YEAR = (XRREV_MTD_NEXT_YEAR / XRN_MTD_NEXT_YEAR).toFixed(2);

                    const dynamicKey = `XRN_MTD_PREV_${stay_year}`;
                    const dynamicKey2 = `XRREV_MTD_PREV_${stay_year}`;
                    const dynamicKey3 = `XARR_MTD_PREV_${stay_year}`;
                    const dynamicKey4 = `XRN_MTD_CURR_${stay_year}`;
                    const dynamicKey5 = `XRREV_MTD_CURR_${stay_year}`;
                    const dynamicKey6 = `XARR_MTD_CURR_${stay_year}`;
                    const dynamicKey7 = `XRN_MTD_NEXT_${stay_year}`;
                    const dynamicKey8 = `XRREV_MTD_NEXT_${stay_year}`;
                    const dynamicKey9 = `XARR_MTD_NEXT_${stay_year}`;

                    return {
                        stay_year,
                        [dynamicKey]: XRN_MTD_PREV_YEAR,
                        [dynamicKey2]: XRREV_MTD_PREV_YEAR,
                        [dynamicKey3]: XARR_MTD_PREV_YEAR,
                        [dynamicKey4]: XRN_MTD_CURR_YEAR,
                        [dynamicKey5]: XRREV_MTD_CURR_YEAR,
                        [dynamicKey6]: XARR_MTD_CURR_YEAR,
                        [dynamicKey7]: XRN_MTD_NEXT_YEAR,
                        [dynamicKey8]: XRREV_MTD_NEXT_YEAR,
                        [dynamicKey9]: XARR_MTD_NEXT_YEAR
                    }

                })


                const filteredOutput = output.map(item => {
                    const filteredItem = { ...item };

                    if (filteredItem.XRN_MTD_PREV_YEAR === 0 || isNaN(parseFloat(filteredItem.XRN_MTD_PREV_YEAR))) {
                        delete filteredItem.XRN_MTD_PREV_YEAR;
                    }

                    if (filteredItem.XRREV_MTD_PREV_YEAR === 0 || isNaN(parseFloat(filteredItem.XRREV_MTD_PREV_YEAR))) {
                        delete filteredItem.XRREV_MTD_PREV_YEAR;
                    }

                    if (filteredItem.XARR_MTD_PREV_YEAR === 'NaN') {
                        delete filteredItem.XARR_MTD_PREV_YEAR;
                    }

                    if (filteredItem.XRN_MTD_CURR_YEAR === 0 || isNaN(parseFloat(filteredItem.XRN_MTD_CURR_YEAR))) {
                        delete filteredItem.XRN_MTD_CURR_YEAR;
                    }

                    if (filteredItem.XRREV_MTD_CURR_YEAR === 0 || isNaN(parseFloat(filteredItem.XRREV_MTD_CURR_YEAR))) {
                        delete filteredItem.XRREV_MTD_CURR_YEAR;
                    }

                    if (filteredItem.XARR_MTD_CURR_YEAR === 'NaN') {
                        delete filteredItem.XARR_MTD_CURR_YEAR;
                    }

                    if (filteredItem.XRN_MTD_NEXT_YEAR === 0 || isNaN(parseFloat(filteredItem.XRN_MTD_NEXT_YEAR))) {
                        delete filteredItem.XRN_MTD_NEXT_YEAR;
                    }

                    if (filteredItem.XRREV_MTD_NEXT_YEAR === 0 || isNaN(parseFloat(filteredItem.XRREV_MTD_NEXT_YEAR))) {
                        delete filteredItem.XRREV_MTD_NEXT_YEAR;
                    }


                    if (filteredItem.XARR_MTD_NEXT_YEAR === 'NaN') {
                        delete filteredItem.XARR_MTD_NEXT_YEAR;
                    }

                    return filteredItem;
                });


                const filteredYears = [2019, 2020, 2021]; // Daftar tahun yang ingin Anda filter

                const filteredData = filteredOutput.filter(item => !filteredYears.includes(item.stay_year));

                const modifiedData = filteredData.map(item => {
                    const modifiedItem = {};

                    for (let key in item) {
                        const value = item[key];

                        if (value !== "NaN" && value !== 0 && value !== false) {
                            modifiedItem[key] = value;
                        }
                    }

                    return modifiedItem;
                });

                const modifiedData1 = modifiedData.map(item => {
                    const modifiedItem = {};

                    for (let key in item) {
                        const modifiedKey = key.replace(/(PREV_|CURR_|NEXT_)/g, "");
                        modifiedItem[modifiedKey] = item[key];
                    }

                    return modifiedItem;
                });

                res.status(200).json(modifiedData1)
            }

        } catch (error) {
            console.log(error);
        }
    };


    // ---------------------------------------------------
    // REVTOFB ROOM REVENUE
    // ---------------------------------------------------
    static async getRoomRevenue(req, res) {
        const { hotel, year, comparison, country } = req.query;

        var currentTime = new Date();
        var currYear = year > 0 ? year : currentTime.getFullYear();
        var prevYear = comparison > 0 ? Number(comparison) : currentTime.getFullYear() - 1;
        var nextYear = currentTime.getFullYear() + 1

        // list 1 years before current year
        const startCurrYear = currYear - 1;
        const curryearList = [];

        for (let tahun = startCurrYear; tahun <= currYear; tahun++) {
            if (tahun != 2020 && tahun != 2021) {
                curryearList.push(tahun);
            }
        }

        curryearList.push(nextYear)

        const yearsBeforeCurrYear = '[' + curryearList.join(', ') + ']'

        console.log(">>>>>>>>>>>>>>>> bikin kondisi baru disini >>>>>>>>>>>>>>>>>>>>>>")
        let whereClause = {};

        if (year != '' && comparison == '') {
            whereClause = {
                market_code: {
                    [Op.notIn]: ['HSU', '']
                },
                [Op.and]: [
                    Sequelize.where(Sequelize.col('room_rev_usd'), { [Op.ne]: '' }),
                    Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear)
                ]
            };
        } else if (year != '' && comparison != '') {
            whereClause = {
                market_code: {
                    [Op.notIn]: ['HSU', '']
                },
                [Op.and]: [
                    Sequelize.where(Sequelize.col('room_rev_usd'), { [Op.ne]: '' }),
                    Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                ]
            };
        } else {
            whereClause = {
                market_code: {
                    [Op.notIn]: ['HSU', '']
                },
                [Op.and]: [
                    Sequelize.where(Sequelize.col('room_rev_usd'), { [Op.ne]: '' }),
                    Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                ]
            };
        }


        if (hotel != '') {
            whereClause['room_class'] = hotel;
        }

        if (country != '') {
            whereClause['country'] = country;
        }

        const attributes = [];
        const years = JSON.parse(yearsBeforeCurrYear);

        for (let i = 0; i < years.length; i++) {
            const year = years[i];

            const attribute = [
                Sequelize.fn(
                    'SUM',
                    Sequelize.literal(
                        "CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " +
                        year +
                        ' THEN `rn` ELSE 0 END'
                    )
                ),
                'room_night_' + year,
            ];

            attributes.push(attribute);
        }

        console.log(whereClause, ">>>>>>>>>>>>>>> ISI KONDISI >>>>>>>>>>>>>>>> ")


        if (year != '' && comparison == '') {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        [Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), 'stay_month'],
                        [Sequelize.fn('SUM', Sequelize.col('room_rev_usd')), 'room_revenue'],
                    ],
                    where: whereClause,
                    group: [
                        Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')),
                    ],
                    order: [
                        [Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), 'ASC'],
                    ],
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        [Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), 'stay_month'],
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + prevYear + " THEN `room_rev_usd` ELSE 0 END")
                            ),
                            "room_revenue_" + prevYear,
                        ],
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + currYear + " THEN `room_rev_usd` ELSE 0 END")
                            ),
                            "room_revenue_" + currYear,
                        ],
                    ],
                    where: whereClause,
                    group: [
                        Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')),
                    ],
                    order: [
                        [Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), 'ASC'],
                    ],
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        [Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), 'stay_month'],
                        ...attributes,
                    ],
                    where: whereClause,
                    group: [
                        Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')),
                    ],
                    order: [
                        [Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), 'ASC'],
                    ],
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        }
    };

    // ---------------------------------------------------
    // REVTOFB FB REVENUE
    // ---------------------------------------------------
    static async getFBRevenue(req, res) {
        const { hotel, year, comparison, country } = req.query;

        var currentTime = new Date();
        var currYear = year > 0 ? year : currentTime.getFullYear();
        var prevYear = comparison > 0 ? Number(comparison) : currentTime.getFullYear() - 1;
        var nextYear = currentTime.getFullYear() + 1

        // list 1 years before current year
        const startCurrYear = currYear - 1;
        const curryearList = [];

        for (let tahun = startCurrYear; tahun <= currYear; tahun++) {
            if (tahun != 2020 && tahun != 2021) {
                curryearList.push(tahun);
            }
        }

        curryearList.push(nextYear)

        const yearsBeforeCurrYear = '[' + curryearList.join(', ') + ']'

        console.log(">>>>>>>>>>>>>>>> bikin kondisi baru disini >>>>>>>>>>>>>>>>>>>>>>")
        let whereClause = {};

        if (year != '' && comparison == '') {
            whereClause = {
                market_code: {
                    [Op.notIn]: ['HSU', '']
                },
                [Op.and]: [
                    Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), currYear)
                ]
            };
        } else if (year != '' && comparison != '') {
            whereClause = {
                market_code: {
                    [Op.notIn]: ['HSU', '']
                },
                [Op.and]: [
                    Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) IN ('" + prevYear + "', '" + currYear + "')"),
                ]
            };
        } else {
            whereClause = {
                market_code: {
                    [Op.notIn]: ['HSU', '']
                },
                [Op.and]: [
                    Sequelize.literal("YEAR(STR_TO_DATE(stay_date, '%d/%m/%Y')) NOT IN ('2020,2021')"),
                ]
            };
        }


        if (hotel != '') {
            whereClause['room_class'] = hotel;
        }

        if (country != '') {
            whereClause['country'] = country;
        }

        const attributes = [];
        const years = JSON.parse(yearsBeforeCurrYear);

        for (let i = 0; i < years.length; i++) {
            const year = years[i];

            const attribute = [
                Sequelize.fn(
                    'SUM',
                    Sequelize.literal(
                        "CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " +
                        year +
                        ' THEN `rn` ELSE 0 END'
                    )
                ),
                'room_night_' + year,
            ];

            attributes.push(attribute);
        }

        console.log(whereClause, ">>>>>>>>>>>>>>> ISI KONDISI >>>>>>>>>>>>>>>> ")


        if (year != '' && comparison == '') {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        [Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), 'stay_month'],
                        [Sequelize.fn('SUM', Sequelize.col('fb_rev_usd')), 'fb_revenue'],
                    ],
                    where: whereClause,
                    group: [
                        Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')),
                    ],
                    order: [
                        [Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), 'ASC'],
                    ],
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else if ((year != '' && comparison != '') || (year == '' && comparison != '')) {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        [Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), 'stay_month'],
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + prevYear + " THEN `fb_rev_usd` ELSE 0 END")
                            ),
                            "fb_revenue_" + prevYear,
                        ],
                        [
                            Sequelize.fn(
                                "SUM",
                                Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + currYear + " THEN `fb_rev_usd` ELSE 0 END")
                            ),
                            "fb_revenue_" + currYear,
                        ],
                    ],
                    where: whereClause,
                    group: [
                        Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')),
                    ],
                    order: [
                        [Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), 'ASC'],
                    ],
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        } else {
            try {
                const bookings = await Master.findAll({
                    attributes: [
                        [Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), 'stay_month'],
                        ...attributes,
                    ],
                    where: whereClause,
                    group: [
                        Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')),
                    ],
                    order: [
                        [Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), 'ASC'],
                    ],
                    raw: true
                });
                res.status(200).json(bookings);
            } catch (error) {
                console.log(error);
            }
        }
    };

    // ---------------------------------------------------
    // RESERVATION PICKUP POSITION
    // ---------------------------------------------------
    static async getRsvPickupPostion(req, res) {
        const { hotel, year, month, group, segment, description, country } = req.query;

        var currentTime = new Date();
        const currMonth = currentTime.getMonth() + 1;
        let currYear = currentTime.getFullYear()
        let nextYear = currYear + 1;

        let whereClause = {};
        let whereClauseSegment = {}

        if (year == '') {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    group_status: {
                        [Op.notIn]: ['LOS', 'PEN', 'PRO']
                    },
                    room_type: {
                        [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('insert_date'), '%d/%m/%Y')), currMonth),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('insert_date'), '%d/%m/%Y')), currYear),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), {
                            [Op.in]: [2019, 2023, 2024]
                        }),
                    ]
                };
            } else {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    group_status: {
                        [Op.notIn]: ['LOS', 'PEN', 'PRO']
                    },
                    room_type: {
                        [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('insert_date'), '%d/%m/%Y')), currYear),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), {
                            [Op.in]: [2019, 2023, 2024]
                        }),
                    ]
                };
            }
        } else {
            if (month == '') {
                whereClause = {
                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    group_status: {
                        [Op.notIn]: ['LOS', 'PEN', 'PRO']
                    },
                    room_type: {
                        [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                    },
                    [Op.and]: [
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('insert_date'), '%d/%m/%Y')), year),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), {
                            [Op.in]: [2019, 2023, 2024]
                        }),
                    ]
                };
            } else {
                whereClause = {

                    market_code: {
                        [Op.notIn]: ['HSU', '']
                    },
                    group_status: {
                        [Op.notIn]: ['LOS', 'PEN', 'PRO']
                    },
                    room_type: {
                        [Op.notIn]: ['PM', 'PS', 'PR', 'PV', '']
                    },

                    [Op.and]: [
                        Sequelize.where(
                            Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('insert_date'), '%d/%m/%Y')),
                            year
                        ),
                        Sequelize.where(
                            Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('insert_date'), '%d/%m/%Y')),
                            month
                        ),
                        {
                            [Op.or]: [
                                {
                                    [Op.and]: [
                                        Sequelize.where(
                                            Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')),
                                            { [Op.eq]: year }
                                        ),
                                        Sequelize.where(
                                            Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')),
                                            { [Op.eq]: month }
                                        ),
                                    ],
                                },
                                {
                                    [Op.and]: [
                                        Sequelize.where(
                                            Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')),
                                            { [Op.eq]: nextYear }
                                        ),
                                        Sequelize.where(
                                            Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')),
                                            { [Op.eq]: month }
                                        ),
                                    ],
                                },
                            ],
                        },
                    ],
                };
            }
        }

        if (hotel != '') {
            whereClause['room_class'] = hotel;
        }

        if (group != '') { // filter marketsegment.group sesudah where
            whereClause['group'] = {
                [Op.and]: [
                    Sequelize.where(Sequelize.col('marketsegment.group'), { [Op.eq]: group }),
                ]
            };
        }

        if (segment != '') {
            whereClause['segment'] = {
                [Op.and]: [
                    Sequelize.where(Sequelize.col('marketsegment.segment'), { [Op.eq]: segment }),
                ]
            };
        }

        if (description != '') {
            whereClause['description'] = {
                [Op.and]: [
                    Sequelize.where(Sequelize.col('marketsegment.description'), { [Op.eq]: description }),
                ]
            };
        }

        if (country != '') {
            whereClause['country'] = country;
        }

        const include_option = [
            {
                model: MarketSegment,
                as: 'marketsegment',
                attributes: [],  // just make relation not show column
                required: false,  //left outer join; true : inner join
                where: whereClauseSegment
            }
        ]

        if (description != '' || segment != '') {
            include_option[0].required = true
        }


        // console.log(whereClause, ">>>>>>>>>>>>>>>> isi kondisi sekarang >>>>>>>>>>>>>>>>>>>>>>")

        const cyear = year == '' ? currYear : year;

        try {
            const bookings = await Master.findAll({
                attributes: [
                    [Sequelize.literal("RIGHT(STR_TO_DATE(stay_date, '%d/%m/%Y'), 2)"), "day"],
                    [
                        Sequelize.fn(
                            "SUM",
                            Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + cyear + " THEN `rn` ELSE 0 END")
                        ),
                        "room_night_" + cyear,
                    ],
                    [
                        Sequelize.fn(
                            "SUM",
                            Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + nextYear + " THEN `rn` ELSE 0 END")
                        ),
                        "room_night_" + nextYear,
                    ],
                ],
                include: include_option,
                where: whereClause,
                group: [
                    Sequelize.literal("RIGHT(STR_TO_DATE(stay_date, '%d/%m/%Y'), 2)"),
                ],
                order: [
                    [Sequelize.literal("RIGHT(STR_TO_DATE(stay_date, '%d/%m/%Y'), 2)"), 'ASC'],
                    [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                ],
                raw: true
            });
            res.status(200).json(bookings);
        } catch (error) {
            console.log(error);
        }

    };

    // ---------------------------------------------------
    // RESERVATION MTD PICKUP
    // ---------------------------------------------------
    static async getRsvMtdPickup(req, res) {
        const { hotel, year, month, group, segment, description, country } = req.query;

        var currentTime = new Date();
        const currMonth = currentTime.getMonth() + 1;
        var currYear = currentTime.getFullYear()
        var nextYear = currYear + 1;


        //    console.log(nextYear, ">>>>>>>>>>>>>>>> isi nextyear >>>>>>>>>>>>>>>>>>>>>>")
        let whereClause = {};
        let whereClauseSegment = {}

        if (year == '') {
            if (month == '') {
                whereClause = {
                    [Op.and]: [
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('insert_date'), '%d/%m/%Y')), currMonth),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('insert_date'), '%d/%m/%Y')), currYear),
                        Sequelize.where(Sequelize.col('market_code'), {
                            [Op.notIn]: ['HSU', '']
                        }),
                        Sequelize.where(Sequelize.col('group_status'), {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), {
                            [Op.in]: [2019, 2023, 2024]
                        }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), {
                            [Op.notIn]: [2020, 2021]
                        })
                    ]
                };
            } else {
                whereClause = {
                    [Op.and]: [
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('insert_date'), '%d/%m/%Y')), currYear),
                        Sequelize.where(Sequelize.col('market_code'), {
                            [Op.notIn]: ['HSU', '']
                        }),
                        Sequelize.where(Sequelize.col('group_status'), {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), {
                            [Op.in]: [2019, 2023, 2024]
                        }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), {
                            [Op.notIn]: [2020, 2021]
                        })
                    ]
                };
            }

        } else {
            if (month == '') {
                whereClause = {
                    [Op.and]: [
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('insert_date'), '%d/%m/%Y')), year),
                        Sequelize.where(Sequelize.col('market_code'), {
                            [Op.notIn]: ['HSU', '']
                        }),
                        Sequelize.where(Sequelize.col('group_status'), {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), {
                            [Op.in]: [2019, 2023, 2024]
                        }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), {
                            [Op.notIn]: [2020, 2021]
                        })
                    ]
                };
            } else {
                whereClause = {

                    [Op.and]: [

                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('insert_date'), '%d/%m/%Y')), year),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('insert_date'), '%d/%m/%Y')), month),
                        Sequelize.where(Sequelize.col('market_code'), {
                            [Op.notIn]: ['HSU', '']
                        }),
                        Sequelize.where(Sequelize.col('group_status'), {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), {
                            [Op.in]: [2019, 2023, 2024]
                        }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), {
                            [Op.notIn]: [2020, 2021]
                        })
                    ]
                };
            }
        }


        if (hotel != '') {
            whereClause['room_class'] = hotel;
        }

        if (group != '') { // filter marketsegment.group sesudah where
            whereClause['group'] = {
                [Op.and]: [
                    Sequelize.where(Sequelize.col('marketsegment.group'), { [Op.eq]: group }),
                ]
            };
        }

        if (segment != '') {
            whereClause['segment'] = {
                [Op.and]: [
                    Sequelize.where(Sequelize.col('marketsegment.segment'), { [Op.eq]: segment }),
                ]
            };
        }

        if (description != '') {
            whereClause['description'] = {
                [Op.and]: [
                    Sequelize.where(Sequelize.col('marketsegment.description'), { [Op.eq]: description }),
                ]
            };
        }

        if (country != '') {
            whereClause['country'] = country;
        }

        const include_option = [
            {
                model: MarketSegment,
                as: 'marketsegment',
                attributes: [],  // just make relation not show column
                required: false,  //left outer join; true : inner join
                where: whereClauseSegment
            }
        ]

        if (description != '' || segment != '') {
            include_option[0].required = true
        }

        const cyear = year == '' ? currYear : year;

        try {
            const bookings = await Master.findAll({
                attributes: [
                    [Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), 'stay_month'],
                    [
                        Sequelize.fn(
                            "SUM",
                            Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + cyear + " THEN `rn` ELSE 0 END")
                        ),
                        "room_night_" + cyear,
                    ],
                    [
                        Sequelize.fn(
                            "SUM",
                            Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + nextYear + " THEN `rn` ELSE 0 END")
                        ),
                        "room_night_" + nextYear,
                    ],
                ],
                include: include_option,
                where: whereClause,
                group: [
                    Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')),
                ],
                order: [
                    [Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), 'ASC'],
                ],
                raw: true
            });
            res.status(200).json(bookings);
        } catch (error) {
            console.log(error);
        }

    };

    // ---------------------------------------------------
    // RESERVATION TOP COUNTRIES
    // ---------------------------------------------------
    static async getRsvTopCountry(req, res) {
        const { hotel, year, month, group, segment, description, country } = req.query;

        var currentTime = new Date();
        const currMonth = currentTime.getMonth() + 1;
        var currYear = currentTime.getFullYear()
        var nextYear = currYear + 1;


        //  console.log(nextYear, ">>>>>>>>>>>>>>>> isi nextyear >>>>>>>>>>>>>>>>>>>>>>")
        let whereClause = {};
        let whereClauseSegment = {}

        if (year == '') {
            if (month == '') {
                whereClause = {
                    [Op.and]: [
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('insert_date'), '%d/%m/%Y')), currMonth),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('insert_date'), '%d/%m/%Y')), currYear),
                        Sequelize.where(Sequelize.col('market_code'), {
                            [Op.notIn]: ['HSU', '']
                        }),
                        Sequelize.where(Sequelize.col('group_status'), {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), {
                            [Op.in]: [2019, 2023, 2024]
                        }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), {
                            [Op.notIn]: [2020, 2021]
                        })
                    ]
                };
            } else {
                whereClause = {
                    [Op.and]: [
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('insert_date'), '%d/%m/%Y')), currYear),
                        Sequelize.where(Sequelize.col('market_code'), {
                            [Op.notIn]: ['HSU', '']
                        }),
                        Sequelize.where(Sequelize.col('group_status'), {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), {
                            [Op.in]: [2019, 2023, 2024]
                        }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), {
                            [Op.notIn]: [2020, 2021]
                        })
                    ]
                };
            }

        } else {
            if (month == '') {
                whereClause = {
                    [Op.and]: [
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('insert_date'), '%d/%m/%Y')), year),
                        Sequelize.where(Sequelize.col('market_code'), {
                            [Op.notIn]: ['HSU', '']
                        }),
                        Sequelize.where(Sequelize.col('group_status'), {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), {
                            [Op.in]: [2019, 2023, 2024]
                        }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), {
                            [Op.notIn]: [2020, 2021]
                        })
                    ]
                };
            } else {
                whereClause = {

                    [Op.and]: [

                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('insert_date'), '%d/%m/%Y')), year),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('insert_date'), '%d/%m/%Y')), month),
                        Sequelize.where(Sequelize.col('market_code'), {
                            [Op.notIn]: ['HSU', '']
                        }),
                        Sequelize.where(Sequelize.col('group_status'), {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), {
                            [Op.in]: [2019, 2023, 2024]
                        }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), {
                            [Op.notIn]: [2020, 2021]
                        })
                    ]
                };
            }
        }


        if (hotel != '') {
            whereClause['room_class'] = hotel;
        }

        if (group != '') { // filter marketsegment.group sesudah where
            whereClause['group'] = {
                [Op.and]: [
                    Sequelize.where(Sequelize.col('marketsegment.group'), { [Op.eq]: group }),
                ]
            };
        }

        if (segment != '') {
            whereClause['segment'] = {
                [Op.and]: [
                    Sequelize.where(Sequelize.col('marketsegment.segment'), { [Op.eq]: segment }),
                ]
            };
        }

        if (description != '') {
            whereClause['description'] = {
                [Op.and]: [
                    Sequelize.where(Sequelize.col('marketsegment.description'), { [Op.eq]: description }),
                ]
            };
        }

        if (country != '') {
            whereClause['country'] = country;
        }

        const include_option = [
            {
                model: MarketSegment,
                as: 'marketsegment',
                attributes: [],  // just make relation not show column
                required: false,  //left outer join; true : inner join
                where: whereClauseSegment
            }
        ]

        if (description != '' || segment != '') {
            include_option[0].required = true
        }

        const cyear = year == '' ? currYear : year;

        try {
            const bookings = await Master.findAll({
                attributes: [
                    'country',
                    [
                        Sequelize.fn(
                            "SUM",
                            Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + cyear + " THEN `rn` ELSE 0 END")
                        ),
                        "room_night_" + cyear,
                    ],
                    [
                        Sequelize.fn(
                            "SUM",
                            Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + nextYear + " THEN `rn` ELSE 0 END")
                        ),
                        "room_night_" + nextYear,
                    ],
                ],
                include: include_option,
                where: whereClause,
                group: ["country"],
                order: [
                    [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                ],
                limit: 10,
                raw: true
            });
            res.status(200).json(bookings);
        } catch (error) {
            console.log(error);
        }

    };

    // ---------------------------------------------------
    // RESERVATION TOP LENGTH OF STAY
    // ---------------------------------------------------
    static async getRsvTopLengthStay(req, res) {
        const { hotel, year, month, group, segment, description, country } = req.query;

        var currentTime = new Date();
        const currMonth = currentTime.getMonth() + 1;
        var currYear = currentTime.getFullYear()
        var nextYear = currYear + 1;


        //    console.log(nextYear, ">>>>>>>>>>>>>>>> isi nextyear >>>>>>>>>>>>>>>>>>>>>>")
        let whereClause = {};
        let whereClauseSegment = {}

        if (year == '') {
            if (month == '') {
                whereClause = {
                    [Op.and]: [
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('insert_date'), '%d/%m/%Y')), currMonth),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('insert_date'), '%d/%m/%Y')), currYear),
                        Sequelize.where(Sequelize.col('market_code'), {
                            [Op.notIn]: ['HSU', '']
                        }),
                        Sequelize.where(Sequelize.col('group_status'), {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), {
                            [Op.in]: [2019, 2023, 2024]
                        }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), {
                            [Op.notIn]: [2020, 2021]
                        })
                    ]
                };
            } else {
                whereClause = {
                    [Op.and]: [
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('insert_date'), '%d/%m/%Y')), currYear),
                        Sequelize.where(Sequelize.col('market_code'), {
                            [Op.notIn]: ['HSU', '']
                        }),
                        Sequelize.where(Sequelize.col('group_status'), {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), {
                            [Op.in]: [2019, 2023, 2024]
                        }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), {
                            [Op.notIn]: [2020, 2021]
                        })
                    ]
                };
            }

        } else {
            if (month == '') {
                whereClause = {
                    [Op.and]: [
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('insert_date'), '%d/%m/%Y')), year),
                        Sequelize.where(Sequelize.col('market_code'), {
                            [Op.notIn]: ['HSU', '']
                        }),
                        Sequelize.where(Sequelize.col('group_status'), {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), {
                            [Op.in]: [2019, 2023, 2024]
                        }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), {
                            [Op.notIn]: [2020, 2021]
                        })
                    ]
                };
            } else {
                whereClause = {

                    [Op.and]: [

                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('insert_date'), '%d/%m/%Y')), year),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('insert_date'), '%d/%m/%Y')), month),
                        Sequelize.where(Sequelize.col('market_code'), {
                            [Op.notIn]: ['HSU', '']
                        }),
                        Sequelize.where(Sequelize.col('group_status'), {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), {
                            [Op.in]: [2019, 2023, 2024]
                        }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), {
                            [Op.notIn]: [2020, 2021]
                        })
                    ]
                };
            }
        }


        if (hotel != '') {
            whereClause['room_class'] = hotel;
        }

        if (group != '') { // filter marketsegment.group sesudah where
            whereClause['group'] = {
                [Op.and]: [
                    Sequelize.where(Sequelize.col('marketsegment.group'), { [Op.eq]: group }),
                ]
            };
        }

        if (segment != '') {
            whereClause['segment'] = {
                [Op.and]: [
                    Sequelize.where(Sequelize.col('marketsegment.segment'), { [Op.eq]: segment }),
                ]
            };
        }

        if (description != '') {
            whereClause['description'] = {
                [Op.and]: [
                    Sequelize.where(Sequelize.col('marketsegment.description'), { [Op.eq]: description }),
                ]
            };
        }

        if (country != '') {
            whereClause['country'] = country;
        }

        const include_option = [
            {
                model: MarketSegment,
                as: 'marketsegment',
                attributes: [],  // just make relation not show column
                required: false,  //left outer join; true : inner join
                where: whereClauseSegment
            }
        ]

        if (description != '' || segment != '') {
            include_option[0].required = true
        }

        const cyear = year == '' ? currYear : year;

        try {
            const bookings = await Master.findAll({
                attributes: [
                    'los',
                    [
                        Sequelize.fn(
                            "SUM",
                            Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + cyear + " THEN `rn` ELSE 0 END")
                        ),
                        "room_night_" + cyear,
                    ],
                    [
                        Sequelize.fn(
                            "SUM",
                            Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + nextYear + " THEN `rn` ELSE 0 END")
                        ),
                        "room_night_" + nextYear,
                    ],
                ],
                include: include_option,
                where: whereClause,
                group: ["los"],
                order: [
                    [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                ],
                limit: 10,
                raw: true
            });
            res.status(200).json(bookings);
        } catch (error) {
            console.log(error);
        }

    };

    // ---------------------------------------------------
    // RESERVATION TOP BOOKING LEAD TIME
    // ---------------------------------------------------
    static async getRsvTopLeadTime(req, res) {
        const { hotel, year, month, group, segment, description, country } = req.query;

        var currentTime = new Date();
        const currMonth = currentTime.getMonth() + 1;
        var currYear = currentTime.getFullYear()
        var nextYear = currYear + 1;


        //   console.log(nextYear, ">>>>>>>>>>>>>>>> isi nextyear >>>>>>>>>>>>>>>>>>>>>>")
        let whereClause = {};
        let whereClauseSegment = {}

        if (year == '') {
            if (month == '') {
                whereClause = {
                    [Op.and]: [
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('insert_date'), '%d/%m/%Y')), currMonth),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('insert_date'), '%d/%m/%Y')), currYear),
                        Sequelize.where(Sequelize.col('market_code'), {
                            [Op.notIn]: ['HSU', '']
                        }),
                        Sequelize.where(Sequelize.col('group_status'), {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), {
                            [Op.in]: [2019, 2023, 2024]
                        }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), {
                            [Op.notIn]: [2020, 2021]
                        })
                    ]
                };
            } else {
                whereClause = {
                    [Op.and]: [
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('insert_date'), '%d/%m/%Y')), currYear),
                        Sequelize.where(Sequelize.col('market_code'), {
                            [Op.notIn]: ['HSU', '']
                        }),
                        Sequelize.where(Sequelize.col('group_status'), {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), {
                            [Op.in]: [2019, 2023, 2024]
                        }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), {
                            [Op.notIn]: [2020, 2021]
                        })
                    ]
                };
            }

        } else {
            if (month == '') {
                whereClause = {
                    [Op.and]: [
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('insert_date'), '%d/%m/%Y')), year),
                        Sequelize.where(Sequelize.col('market_code'), {
                            [Op.notIn]: ['HSU', '']
                        }),
                        Sequelize.where(Sequelize.col('group_status'), {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), {
                            [Op.in]: [2019, 2023, 2024]
                        }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), {
                            [Op.notIn]: [2020, 2021]
                        })
                    ]
                };
            } else {
                whereClause = {

                    [Op.and]: [

                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('insert_date'), '%d/%m/%Y')), year),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('insert_date'), '%d/%m/%Y')), month),
                        Sequelize.where(Sequelize.col('market_code'), {
                            [Op.notIn]: ['HSU', '']
                        }),
                        Sequelize.where(Sequelize.col('group_status'), {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), {
                            [Op.in]: [2019, 2023, 2024]
                        }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), {
                            [Op.notIn]: [2020, 2021]
                        })
                    ]
                };
            }
        }


        if (hotel != '') {
            whereClause['room_class'] = hotel;
        }

        if (group != '') { // filter marketsegment.group sesudah where
            whereClause['group'] = {
                [Op.and]: [
                    Sequelize.where(Sequelize.col('marketsegment.group'), { [Op.eq]: group }),
                ]
            };
        }

        if (segment != '') {
            whereClause['segment'] = {
                [Op.and]: [
                    Sequelize.where(Sequelize.col('marketsegment.segment'), { [Op.eq]: segment }),
                ]
            };
        }

        if (description != '') {
            whereClause['description'] = {
                [Op.and]: [
                    Sequelize.where(Sequelize.col('marketsegment.description'), { [Op.eq]: description }),
                ]
            };
        }

        if (country != '') {
            whereClause['country'] = country;
        }

        const include_option = [
            {
                model: MarketSegment,
                as: 'marketsegment',
                attributes: [],  // just make relation not show column
                required: false,  //left outer join; true : inner join
                where: whereClauseSegment
            }
        ]

        if (description != '' || segment != '') {
            include_option[0].required = true
        }

        const cyear = year == '' ? currYear : year;

        try {
            const bookings = await Master.findAll({
                attributes: [
                    'lead_days',
                    [
                        Sequelize.fn(
                            "SUM",
                            Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + cyear + " THEN `rn` ELSE 0 END")
                        ),
                        "room_night_" + cyear,
                    ],
                    [
                        Sequelize.fn(
                            "SUM",
                            Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + nextYear + " THEN `rn` ELSE 0 END")
                        ),
                        "room_night_" + nextYear,
                    ],
                ],
                include: include_option,
                where: whereClause,
                group: ["lead_days"],
                order: [
                    [Sequelize.fn('SUM', Sequelize.col('rn')), 'DESC']
                ],
                limit: 10,
                raw: true
            });
            res.status(200).json(bookings);
        } catch (error) {
            console.log(error);
        }

    };

    // ---------------------------------------------------
    // RESERVATION TOP MARKET SEGMENTS
    // ---------------------------------------------------
    static async getRsvTopMarketSegment(req, res) {
        const { hotel, year, month, group, segment, description, country } = req.query;

        var currentTime = new Date();
        const currMonth = currentTime.getMonth() + 1;
        var currYear = currentTime.getFullYear()
        var nextYear = currYear + 1;


        //     console.log(nextYear, ">>>>>>>>>>>>>>>> isi nextyear >>>>>>>>>>>>>>>>>>>>>>")
        let whereClause = {};
        let whereClauseSegment = {}

        if (year == '') {
            if (month == '') {
                whereClause = {
                    [Op.and]: [
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('insert_date'), '%d/%m/%Y')), currMonth),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('insert_date'), '%d/%m/%Y')), currYear),
                        Sequelize.where(Sequelize.col('market_code'), {
                            [Op.notIn]: ['HSU', '']
                        }),
                        Sequelize.where(Sequelize.col('group_status'), {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), {
                            [Op.in]: [2019, 2023, 2024]
                        }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), {
                            [Op.notIn]: [2020, 2021]
                        })
                    ]
                };
            } else {
                whereClause = {
                    [Op.and]: [
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('insert_date'), '%d/%m/%Y')), currYear),
                        Sequelize.where(Sequelize.col('market_code'), {
                            [Op.notIn]: ['HSU', '']
                        }),
                        Sequelize.where(Sequelize.col('group_status'), {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), {
                            [Op.in]: [2019, 2023, 2024]
                        }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), {
                            [Op.notIn]: [2020, 2021]
                        })
                    ]
                };
            }

        } else {
            if (month == '') {
                whereClause = {
                    [Op.and]: [
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('insert_date'), '%d/%m/%Y')), year),
                        Sequelize.where(Sequelize.col('market_code'), {
                            [Op.notIn]: ['HSU', '']
                        }),
                        Sequelize.where(Sequelize.col('group_status'), {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), {
                            [Op.in]: [2019, 2023, 2024]
                        }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), {
                            [Op.notIn]: [2020, 2021]
                        })
                    ]
                };
            } else {
                whereClause = {

                    [Op.and]: [

                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('insert_date'), '%d/%m/%Y')), year),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('insert_date'), '%d/%m/%Y')), month),
                        Sequelize.where(Sequelize.col('market_code'), {
                            [Op.notIn]: ['HSU', '']
                        }),
                        Sequelize.where(Sequelize.col('group_status'), {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), {
                            [Op.in]: [2019, 2023, 2024]
                        }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), {
                            [Op.notIn]: [2020, 2021]
                        })
                    ]
                };
            }
        }


        if (hotel != '') {
            whereClause['room_class'] = hotel;
        }

        if (group != '') { // filter marketsegment.group sesudah where
            whereClause['group'] = {
                [Op.and]: [
                    Sequelize.where(Sequelize.col('marketsegment.group'), { [Op.eq]: group }),
                ]
            };
        }

        if (segment != '') {
            whereClause['segment'] = {
                [Op.and]: [
                    Sequelize.where(Sequelize.col('marketsegment.segment'), { [Op.eq]: segment }),
                ]
            };
        }

        if (description != '') {
            whereClause['description'] = {
                [Op.and]: [
                    Sequelize.where(Sequelize.col('marketsegment.description'), { [Op.eq]: description }),
                ]
            };
        }

        if (country != '') {
            whereClause['country'] = country;
        }

        const include_option = [
            {
                model: MarketSegment,
                as: 'marketsegment',
                attributes: [],  // just make relation not show column
                required: false,  //left outer join; true : inner join
                where: whereClauseSegment
            }
        ]

        if (description != '' || segment != '') {
            include_option[0].required = true
        }

        const cyear = year == '' ? currYear : year;

        try {
            const bookings = await Master.findAll({
                attributes: [
                    'marketsegment.segment',
                    [
                        Sequelize.fn(
                            "SUM",
                            Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + cyear + " THEN `rn` ELSE 0 END")
                        ),
                        "room_night_" + cyear,
                    ],
                    [
                        Sequelize.fn(
                            "SUM",
                            Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + nextYear + " THEN `rn` ELSE 0 END")
                        ),
                        "room_night_" + nextYear,
                    ],
                ],
                include: include_option,
                where: whereClause,
                group: [
                    'marketsegment.segment',
                ],
                order: [
                    [Sequelize.fn(
                        "SUM",
                        Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + cyear + " THEN `rn` ELSE 0 END")
                    ), 'DESC']
                ],
                limit: 10,
                raw: true
            });
            res.status(200).json(bookings);
        } catch (error) {
            console.log(error);
        }

    };

    // ---------------------------------------------------
    // RESERVATION TOP SUPPORTERS
    // ---------------------------------------------------
    static async getRsvTopSupporter(req, res) {
        const { hotel, year, month, group, segment, description, country } = req.query;

        var currentTime = new Date();
        const currMonth = currentTime.getMonth() + 1;
        var currYear = currentTime.getFullYear()
        var nextYear = currYear + 1;


        //   console.log(nextYear, ">>>>>>>>>>>>>>>> isi nextyear >>>>>>>>>>>>>>>>>>>>>>")
        let whereClause = {};
        let whereClauseSegment = {}

        if (year == '') {
            if (month == '') {
                whereClause = {
                    [Op.and]: [
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('insert_date'), '%d/%m/%Y')), currMonth),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('insert_date'), '%d/%m/%Y')), currYear),
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.col('market_code'), {
                            [Op.notIn]: ['HSU', '']
                        }),
                        Sequelize.where(Sequelize.col('group_status'), {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), {
                            [Op.in]: [2019, 2023, 2024]
                        }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), {
                            [Op.notIn]: [2020, 2021]
                        })
                    ]
                };
            } else {
                whereClause = {
                    [Op.and]: [
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('insert_date'), '%d/%m/%Y')), currYear),
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.col('market_code'), {
                            [Op.notIn]: ['HSU', '']
                        }),
                        Sequelize.where(Sequelize.col('group_status'), {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), {
                            [Op.in]: [2019, 2023, 2024]
                        }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), {
                            [Op.notIn]: [2020, 2021]
                        })
                    ]
                };
            }

        } else {
            if (month == '') {
                whereClause = {
                    [Op.and]: [
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('insert_date'), '%d/%m/%Y')), year),
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.col('market_code'), {
                            [Op.notIn]: ['HSU', '']
                        }),
                        Sequelize.where(Sequelize.col('group_status'), {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), {
                            [Op.in]: [2019, 2023, 2024]
                        }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), {
                            [Op.notIn]: [2020, 2021]
                        })
                    ]
                };
            } else {
                whereClause = {

                    [Op.and]: [

                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('insert_date'), '%d/%m/%Y')), year),
                        Sequelize.where(Sequelize.fn('MONTH', Sequelize.fn('STR_TO_DATE', Sequelize.col('insert_date'), '%d/%m/%Y')), month),
                        Sequelize.where(Sequelize.col('agent_name'), { [Op.ne]: '' }),
                        Sequelize.where(Sequelize.col('market_code'), {
                            [Op.notIn]: ['HSU', 'PF', '']
                        }),
                        Sequelize.where(Sequelize.col('group_status'), {
                            [Op.notIn]: ['LOS', 'PEN', 'PRO']
                        }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), {
                            [Op.in]: [2019, 2023, 2024]
                        }),
                        Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), {
                            [Op.notIn]: [2020, 2021]
                        })
                    ]
                };
            }
        }


        if (hotel != '') {
            whereClause['room_class'] = hotel;
        }

        if (group != '') { // filter marketsegment.group sesudah where
            whereClause['group'] = {
                [Op.and]: [
                    Sequelize.where(Sequelize.col('marketsegment.group'), { [Op.eq]: group }),
                ]
            };
        }

        if (segment != '') {
            whereClause['segment'] = {
                [Op.and]: [
                    Sequelize.where(Sequelize.col('marketsegment.segment'), { [Op.eq]: segment }),
                ]
            };
        }

        if (description != '') {
            whereClause['description'] = {
                [Op.and]: [
                    Sequelize.where(Sequelize.col('marketsegment.description'), { [Op.eq]: description }),
                ]
            };
        }

        if (country != '') {
            whereClause['country'] = country;
        }

        const include_option = [
            {
                model: MarketSegment,
                as: 'marketsegment',
                attributes: [],  // just make relation not show column
                required: false,  //left outer join; true : inner join
                where: whereClauseSegment
            }
        ]

        if (description != '' || segment != '') {
            include_option[0].required = true
        }

        const cyear = year == '' ? currYear : year;

        try {
            const bookings = await Master.findAll({
                attributes: [
                    'agent_name',
                    [
                        Sequelize.fn(
                            "SUM",
                            Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + cyear + " THEN `rn` ELSE 0 END")
                        ),
                        "room_night_" + cyear,
                    ],
                    [
                        Sequelize.fn(
                            "SUM",
                            Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + nextYear + " THEN `rn` ELSE 0 END")
                        ),
                        "room_night_" + nextYear,
                    ],
                ],
                include: include_option,
                where: whereClause,
                group: [
                    'agent_name',
                ],
                order: [
                    [Sequelize.fn(
                        "SUM",
                        Sequelize.literal("CASE WHEN YEAR(STR_TO_DATE(`stay_date`, '%d/%m/%Y')) = " + cyear + " THEN `rn` ELSE 0 END")
                    ), 'DESC']
                ],
                limit: 10,
                raw: true
            });
            res.status(200).json(bookings);
        } catch (error) {
            console.log(error);
        }
    };

    // ----------------------------------------------------------
    // GET LIST AGENTS BY ROOM CLASS, YEAR, AND MARKET SEGMENTS
    // ----------------------------------------------------------
    static async getListAgents(req, res) {
        const { hotel, year, description } = req.query;

        var currentTime = new Date();
        const currMonth = currentTime.getMonth() + 1;
        var currYear = currentTime.getFullYear()
        var nextYear = currYear + 1;


        //   console.log(nextYear, ">>>>>>>>>>>>>>>> isi nextyear >>>>>>>>>>>>>>>>>>>>>>")
        let whereClause = {};
        let whereClauseSegment = {}

        if (year == '') {
            whereClause = {
                [Op.and]: [
                    Sequelize.where(Sequelize.col('agent_name'), { [Op.ne]: '' }),
                    Sequelize.where(Sequelize.col('market_code'), {
                        [Op.notIn]: ['HSU', '']
                    }),
                    Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), {
                        [Op.notIn]: [2020, 2021]
                    })
                ]
            };
        } else {
            whereClause = {
                [Op.and]: [
                    Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('insert_date'), '%d/%m/%Y')), year),
                    Sequelize.where(Sequelize.col('agent_name'), { [Op.ne]: '' }),
                    Sequelize.where(Sequelize.col('market_code'), {
                        [Op.notIn]: ['HSU', '']
                    }),
                    Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), {
                        [Op.in]: [2019, 2023, 2024]
                    }),
                    Sequelize.where(Sequelize.fn('YEAR', Sequelize.fn('STR_TO_DATE', Sequelize.col('stay_date'), '%d/%m/%Y')), {
                        [Op.notIn]: [2020, 2021]
                    })
                ]
            };
        }

        if (hotel != '') {
            whereClause['room_class'] = hotel;
        }

        if (description != '') {
            whereClause['description'] = {
                [Op.and]: [
                    Sequelize.where(Sequelize.col('marketsegment.description'), { [Op.eq]: description }),
                ]
            };
        }

        const include_option = [
            {
                model: MarketSegment,
                as: 'marketsegment',
                attributes: [],  // just make relation not show column
                required: false,  //left outer join; true : inner join
                where: whereClauseSegment
            }
        ]

        if (description != '') {
            include_option[0].required = true
        }

        const cyear = year == '' ? currYear : year;

        try {
            const agents = await Master.findAll({
                attributes: [
                    'agent_name',
                ],
                include: include_option,
                where: whereClause,
                group: [
                    'agent_name',
                ],
                order: [
                    ['agent_name', 'ASC']
                ],
                raw: true
            });

            // Modifikasi hasil sebelum mengirimkan respons
            const modifiedAgents = agents.map(agent => {
                return { "name": agent.agent_name , "value": agent.agent_name };
            });

            res.status(200).json(modifiedAgents);
        } catch (error) {
            console.log(error);
        }

    };










    // TEST API 

    static async testApi(req, res) {
        try {
            const { hotel, year, month, group, segment, desc, country } = req.query

            //! INI SUDAH JALAN
            const data = await Master.findAll({
                attributes: ['room_rev_usd'],
            })
            console.log(data)

            const dataBaru = data.map(item => {
                const number = Number(item.dataValues.room_rev_usd)

                const temp = []

                if (number > 0) {
                    return (number)
                } else {
                    return (0)
                }
            })



            // const test = data.map(item => {
            //     // console.log(item.dataValues.room_rev_usd)
            //     let stringData = item.dataValues.room_rev_usd
            //     let stringBaru = stringData.replace(/,/g, "")
            //     console.log(Number(stringBaru))

            // })
            // res.status(200).json(data)
            //! INI SUDAH JALAN

        } catch (error) {
            console.log(error)
        }
    }
}

module.exports = DashboardController
