const { Dining, Outlet, Setting, Member } = require('../db/models/index');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const Sequelize = require('sequelize');
const { Op } = require('sequelize');
const { calculateEarnPoint, calculateBonusPoint, calculateDoublePoint, calculateSocialEventPoint, calculatePointPlus, calculateRamadanPointPlus } = require('../utils/pointCalculator');

function convertUTCToLocal(utcDate, offset) {
    if (!(utcDate instanceof Date)) {
        throw new TypeError('The first argument must be a Date object.');
    }
    if (typeof offset !== 'number') {
        throw new TypeError('The second argument must be a number.');
    }

    // Convert the UTC time to the local time
    return new Date(utcDate.getTime() + offset * 60 * 60 * 1000);
}

class DiningController {

    static async getDinings(req, res, next) {
        const { page, per_page, filter, location } = req.query;
        const perPageInt = parseInt(per_page, 10);
        const offset = (page - 1) * per_page;

        const uppercaseLocation = location ? location.toUpperCase() : '';

        try {

            // Membuat objek where condition
            const whereCondition = {
                is_approved: 0, // Kondisi baru untuk is_approved = 0
                is_deleted: 0   // Tambahkan kondisi is_deleted = 0
            };

            if (filter) {
                whereCondition[Op.or] = [
                    {
                        member_id: {
                            [Op.like]: `%${filter}%`
                        }
                    },
                    {
                        member_name: {
                            [Op.like]: `%${filter}%`
                        }
                    },
                    {
                        check_number: {
                            [Op.like]: `%${filter}%`
                        }
                    }
                ];
            }

            if (uppercaseLocation) {
                whereCondition.location = uppercaseLocation;
            }

            const dinings = await Dining.findAndCountAll({
                attributes: ['id', 'dining_date', 'outlet_id', 'member_id', 'member_name', 'total_bill', 'bookeeping', 'point_type_id', 'total_bill_rp', 'earned_point', 'check_number', 'location', 'bill_image', 'is_approved', 'approvedBy', 'approvedAt', 'createdBy', 'createdAt'],
                offset: offset,
                limit: perPageInt,
                where: whereCondition,
                include: [
                    {
                        model: Outlet,
                        attributes: ['outlet_name'], // Atur atribut yang ingin Anda ambil dari model Outlet
                        as: 'outlet' // Sesuaikan dengan nama asosiasi antara Dining dan Outlet
                    }
                ],
                order: [
                    ['createdAt', 'DESC'],
                    ['member_id', 'ASC'],   // Order kedua: member_id ASC
                    ['check_number', 'ASC'] // Order ketiga: check_number ASC
                ],
            });

            const getPointTypeName = (pointTypeId) => {
                switch (pointTypeId) {
                    case 1: return 'Normal Point';
                    case 2: return 'Bonus Point';
                    case 3: return 'Double Point';
                    default: return 'No Point Type';
                }
            };

            // Ekstrak data dan jumlah total dari hasil query
            // const data = dinings.rows;
            const formattedDinings = dinings.rows.map(dining => ({
                id: dining.id,
                dining_date: dining.dining_date,
                outlet_id: dining.outlet_id,
                outlet_name: dining.outlet?.outlet_name, // Mengakses kolom outlet_name dari model Outlet
                member_id: dining.member_id,
                member_name: dining.member_name,
                total_bill: dining.total_bill,
                bookeeping: dining.bookeeping,
                point_type_id: dining.point_type_id,
                point_type_name: getPointTypeName(dining.point_type_id),
                total_bill_rp: dining.total_bill_rp,
                earned_point: dining.earned_point,
                check_number: dining.check_number,
                location: dining.location,
                is_attachment: dining.bill_image !== null && dining.bill_image !== '' ? 1 : 0, // Menentukan apakah transaksi memiliki gambar atau tidak
                is_approved: dining.is_approved,
                approvedBy: dining.approvedBy,
                approvedAt: dining.approvedAt,
                createdBy: dining.createdBy,
                createdAt: dining.createdAt,
            }));

            const totalCount = dinings.count;

            res.status(200).json({
                data: formattedDinings,
                totalData: totalCount,
            });
        } catch (error) {
            console.log(error);
        }

    }


    static async validateCheckNumber(req, res) {
        const { diningId, checkNumber, region } = req.query;

        try {
            const diningRecords = await Dining.findAll({
                where: {
                    check_number: checkNumber,
                    location: region,
                    is_deleted: 0
                }
            });


            const diningIds = diningRecords.map(record => record.dataValues.id);
            const count = diningIds.length;
            let isDuplicate = false;

            if (count > 1) {
                isDuplicate = true
            } else if (count === 1 && diningIds.includes(Number(diningId))) {
                isDuplicate = false
            } else if (count === 1 && !diningIds.includes(Number(diningId))) {
                isDuplicate = true
            }
            // Jika hanya ada satu hasil dan id-nya sama dengan diningId, bukan duplikat
            //const isDuplicate = count > 1 || (count === 1 && diningIds.includes(diningId));

            res.status(200).json({ isDuplicate });
        } catch (error) {
            console.log(error)
        }
    }

    static async getEarnPointsByMemberId(req, res) {
        const memberId = req.params.memberId;

        try {

            const eranings = await Dining.findAll({
                attributes: ['member_id', 'dining_date', 'createdAt', 'check_number',
                    [Sequelize.literal('0'), 'room_usd'],
                    [Sequelize.literal('0'), 'room_point'],
                    [Sequelize.fn('SUM', Sequelize.col('total_bill')), 'fb_usd'],
                    [Sequelize.fn('SUM', Sequelize.col('earned_point')), 'fb_point'],
                    [Sequelize.fn('SUM', Sequelize.col('earned_point')), 'total_points']
                ],
                include: [
                    {
                        model: Outlet,
                        attributes: ['outlet_name'], // Atur atribut yang ingin Anda ambil dari model Outlet
                        as: 'outlet' // Sesuaikan dengan nama asosiasi antara Dining dan Outlet
                    }
                ],
                where: {
                    member_id: memberId,
                    is_approved: 1
                },
                group: ['member_id', 'outlet_id', 'dining_date', 'total_bill', 'earned_point', 'createdAt', 'check_number'],
                order: [
                    ['dining_date', 'ASC']
                ],
            });

            res.status(200).json(eranings);
        } catch (error) {
            console.log(error);
        }
    };


    static async checkFirstDining(req, res) {
        const memberId = req.params.memberId;

        try {
            const dining = await Dining.findOne({
                attributes: ['member_id', 'dining_date', 'createdAt', 'check_number', 'is_first_dining'],
                where: {
                    member_id: memberId,
                    is_approved: 1
                }
            });

            res.status(200).json(dining);
        } catch (error) {
            console.log(error);
        }
    };

    static async getTotalPoint(req, res) {
        const { memberId, totalBill, totalBillRp, bookeeping, outletId, pointType, region } = req.query;

        try {

            // Get member level
            const member = await Member.findOne({ where: { member_id: memberId } });
            if (!member) {
                return res.status(404).json({ status: 404, msg: "Settings not found" });
            }
            const member_level = member.member_level;

            let earn_point = 0;

            switch (Number(pointType)) {
                case 1: // normal point
                    earn_point = await calculateEarnPoint(member_level, totalBill);
                    break;

                case 2: // bonus point
                    // Bonus poin hanya dihitung jika region adalah Jakarta
                    if (region === 'JAKARTA') {
                        earn_point = await calculateBonusPoint(member_level, totalBillRp, bookeeping, outletId);
                    } else {
                        earn_point = await calculateEarnPoint(member_level, totalBill);
                    }
                    break;

                case 3: // double point
                    earn_point = await calculateDoublePoint(member_level, totalBill);
                    break;
                case 4: // social event point
                    earn_point = await calculateSocialEventPoint(totalBillRp);
                    break;
                case 5: // point plus
                    earn_point = await calculatePointPlus(totalBillRp, outletId);
                    break;
                case 6: // point plus
                    earn_point = await calculateRamadanPointPlus(totalBillRp, outletId);
                    break;
                default:
                    earn_point = 0; // Jika pointType tidak sesuai dengan 1, 2, atau 3
                    break;
            }


            // if (Number(pointType) === 1) {
            //     earn_point = await calculateEarnPoint(member_level, totalBill);
            // } else if (Number(pointType) === 2) {
            //     // Bonus poin hanya dihitung jika region adalah Jakarta
            //     if (region === 'JAKARTA') {
            //         earn_point = await calculateBonusPoint(member_level, totalBillRp, bookeeping, outletId);
            //     } else {
            //         // Jika bukan Jakarta, poin tetap 0 (atau bisa diisi logika lain jika ada)
            //         earn_point = await calculateEarnPoint(member_level, totalBill);;
            //     }
            // } else if (Number(pointType) === 3) {
            //     earn_point = await calculateDoublePoint(member_level, totalBill);
            // }
            res.status(200).json(earn_point);
        } catch (error) {
            console.log(error);
        }
    };


    static async createDining(req, res) {
        const { dining_date, outlet_id, member_id, member_name, point_type_id, total_bill, total_bill_rp, bookeeping, earned_point, check_number, location, remarks, bill_image, bill_url, username } = req.body;
        // using UTC time
        const nowUTC = new Date(); // Current UTC time
        const offset = 8; // Bali (WITA) is UTC+8
        const localDateTime = convertUTCToLocal(nowUTC, offset);

        try {
            // Get member level
            // const member = await Member.findOne({ where: { member_id: member_id } });
            // if (!member) {
            //     return res.status(404).json({ status: 404, msg: "Settings not found" });
            // }
            // const member_level = member.member_level;

            // Calculate earn_point using the external function
            // const earn_point = await calculateEarnPoint(member_level, total_bill);

            await Dining.create({
                dining_date: dining_date,
                outlet_id: outlet_id,
                member_id: member_id,
                member_name: member_name,
                point_type_id: point_type_id,
                total_bill: total_bill,
                total_bill_rp: total_bill_rp,
                bookeeping: bookeeping,
                earned_point: earned_point,
                check_number: check_number,
                location: location,
                remarks: remarks,
                bill_image: bill_image,
                bill_url: bill_url,
                createdAt: localDateTime,
                createdBy: username,
                updatedAt: localDateTime,
                updatedBy: username,
            });
            res.json({ status: 200, msg: "Add new Dining successfully" });
        } catch (error) {
            console.log(error);
        }
    };

    static async detailDining(req, res) {

        const diningId = req.params.id;
        try {
            const dining = await Dining.findOne({
                include: ["outlet", "member"],
                where: {
                    id: diningId
                }
            });
            res.status(200).json(dining);
        } catch (error) {
            console.log(error);
        }
    };

    static async updateDining(req, res) {
        const diningId = req.params.id;
        const { dining_date, outlet_id, member_id, member_name, point_type_id, total_bill, total_bill_rp, bookeeping, earned_point, check_number, location, remarks, bill_image, bill_url, username } = req.body


        // using UTC time
        const nowUTC = new Date(); // Current UTC time
        const offset = 8; // Bali (WITA) is UTC+8
        const localDateTime = convertUTCToLocal(nowUTC, offset);


        //cek apakah Role sdh ada
        const dining = await Dining.findAll({
            where: {
                id: diningId
            }
        });

        if (!dining[0]) return res.status(400).json({ status: 400, msg: "Dining is not exist" });

        try {
            // Get member level
            // const member = await Member.findOne({ where: { member_id: member_id } });
            // if (!member) {
            //     return res.status(404).json({ status: 404, msg: "Settings not found" });
            // }
            // const member_level = member.member_level;

            // Calculate earn_point using the external function
            // const earn_point = await calculateEarnPoint(member_level, total_bill)

            await Dining.update(
                {
                    dining_date: dining_date,
                    outlet_id: outlet_id,
                    member_id: member_id,
                    member_name: member_name,
                    point_type_id: point_type_id,
                    total_bill: total_bill,
                    total_bill_rp: total_bill_rp,
                    bookeeping: bookeeping,
                    earned_point: earned_point,
                    check_number: check_number,
                    location: location,
                    remarks: remarks,
                    bill_image: bill_image,
                    bill_url: bill_url,
                    updatedAt: localDateTime,
                    updatedBy: username
                },
                {
                    where: {
                        id: diningId
                    }
                });

            res.json({ status: 200, msg: "Update Dining is successfully" });
        } catch (error) {
            console.log(error);
        }
    };

    static async approveDining(req, res) {
        const diningId = req.params.id;
        const { is_approved, is_first_dining, username } = req.body;

        // using UTC time
        const nowUTC = new Date(); // Current UTC time
        const offset = 8; // Bali (WITA) is UTC+8
        const localDateTime = convertUTCToLocal(nowUTC, offset);

        //cek apakah Role sdh ada
        const dining = await Dining.findAll({
            where: {
                id: diningId
            }
        });

        if (!dining[0]) return res.status(400).json({ status: 400, msg: "Dining is not exist" });

        try {
            await Dining.update(
                {
                    is_approved: is_approved,
                    is_first_dining: is_first_dining,
                    approvedAt: localDateTime,
                    approvedBy: username
                },
                {
                    where: {
                        id: diningId
                    }
                });

            res.json({ status: 200, msg: "Approve Dining is successfully" });
        } catch (error) {
            console.log(error);
        }
    };

    static async deleteDining(req, res) {
        const diningId = req.params.id;

        // Cek apakah dining dengan ID tersebut ada
        const dining = await Dining.findOne({ where: { id: diningId } });

        if (!dining) return res.status(400).json({ status: 400, msg: "Dining does not exist" });

        try {

            // Update field is_deleted menjadi 1
            await Dining.update(
                { is_deleted: 1 },
                { where: { id: diningId } }
            );

            res.json({ status: 200, msg: "Delete Dining is successfully" });
        } catch (error) {
            console.log(error);
        }
    };
}


module.exports = DiningController







