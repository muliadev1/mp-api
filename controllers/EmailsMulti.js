const { Member, Reedem, ReedemsPoint, MemberVoucher } = require('../db/models/index');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const Sequelize = require('sequelize');

const { chromium } = require('playwright');

const handlebars = require("handlebars");
const fs = require('fs');

// Import nodemailer
const nodemailer = require('nodemailer');
const Imap = require('node-imap');
const { simpleParser } = require('mailparser');

// Email configuration
const senderEmail = 'support@muliaprivileges.com';
const senderPassword = 'BlueFish@2024';

// SMTP (sending) server details
const smtpServer = 'smtp.titan.email';
const smtpPort = 465;

// IMAP (receiving) server details
const imapServer = 'imap.titan.email';
const imapPort = 993;

// Create a nodemailer transporter using SMTP
const transporter = nodemailer.createTransport({
  host: smtpServer,
  port: smtpPort,
  secure: true,
  auth: {
    user: senderEmail,
    pass: senderPassword,
  },
});

function convertUTCToLocal(utcDate, offset) {
  if (!(utcDate instanceof Date)) {
    throw new TypeError('The first argument must be a Date object.');
  }
  if (typeof offset !== 'number') {
    throw new TypeError('The second argument must be a number.');
  }

  // Convert the UTC time to the local time
  return new Date(utcDate.getTime() + offset * 60 * 60 * 1000);
}

// Define BCC emails if needed
const bccEmails = ['novan.arifianto@themulia.com']; // Replace with actual BCC emails

const approve_image_Url = `${process.env.API_HOST}/assets/uploads/APPROVE.png`;
const approved_image_Url = `${process.env.API_HOST}/assets/uploads/APPROVED.png`;

const body = '';


const generateVoucher = (prefix) => {
  const currentDate = new Date();
  const currentYear = currentDate.getFullYear().toString();

  const uniqueNumber = Math.floor(Math.random() * 1000000); // Angka acak antara 0 dan 99999999
  const newvoucher = prefix + "-" + uniqueNumber.toString().padStart(8, '0') + currentYear + "MP"; // Menambahkan nol di depan jika angka kurang dari 8 digit

  return newvoucher;
}

async function sendEmailAndAppend() {
  const recipientEmail = 'softhouse@themulia.com';
  const subject = 'Testing email script';
  const body = 'This is a test email sent from a Node.js script.';

  const emailTemplateSource = fs.readFileSync("assets/email/testEmail.html", "utf8");
  const emailTemplate = handlebars.compile(emailTemplateSource);
  // Define the data to be used in the email template
  const data = {
    url: "https://muliaprivileges.com",
  };

  // Render the email template with the data
  const emailHtml = emailTemplate(data);
  try {

    // Create the email options
    const mailOptions = {
      from: senderEmail,
      to: recipientEmail,
      subject: subject,
      html: emailHtml,
    };

    // Send the email
    const info = await transporter.sendMail(mailOptions);
    console.log('Email sent successfully.');
    console.log('Info object:', info);

    // Append the sent email to the "Sent" folder using IMAP
    const imap = new Imap({
      user: senderEmail,
      password: senderPassword,
      host: imapServer,
      port: imapPort,
      tls: true,
    });

    imap.once('ready', () => {
      imap.openBox('Sent', true, (err) => {
        if (err) {
          console.error('Error opening "Sent" folder:', err);
          imap.end();
          return;
        }

        // Create the email message as MIMEText
        const emailMessage = `From: ${senderEmail}\r\nTo: ${recipientEmail}\r\nSubject: ${subject}\r\n\r\n${body}`;

        // Append the sent email to the "Sent" folder
        imap.append(emailMessage, { mailbox: 'Sent' }, (appendErr) => {
          if (appendErr) {
            console.error('Error appending email to "Sent" folder:', appendErr);
          } else {
            console.log('Email appended to "Sent" folder.');
          }
          imap.end();
        });
      });
    });

    imap.once('error', (imapErr) => {
      console.error('IMAP Error:', imapErr);
    });

    imap.connect();
  } catch (error) {
    console.error('Error sending email:', error);
  }
}


// SEND EMAIL USING SENDINBLUE
const SendActivationEmail = async (memberData) => {

  const subject = 'Muliaprivileges.com - Your Activation Account!';

  // Load the email template
  const emailTemplateSource = fs.readFileSync("assets/email/EmailRequestOTP.html", "utf8");

  // Compile the template using Handlebars.js
  const emailTemplate = handlebars.compile(emailTemplateSource);
  // Define the data to be used in the email template
  const data = {
    firstName: memberData.first,
    lastName: memberData.last,
    password: memberData.temp_password
  };

  // Render the email template with the data
  const emailHtml = emailTemplate(data);

  // set receipient email
  const recipientEmail = memberData.primary_email;

  try {
    // Create the email options
    const mailOptions = {
      from: `Team Support Muliaprivileges.com <${senderEmail}>`,
      to: `${data.firstName} ${data.lastName} <${recipientEmail}>`,
      // cc: ['cc_email1@example.com', 'cc_email2@example.com'],
      subject: subject,
      html: emailHtml,
    };

    // Send the email
    const info = await transporter.sendMail(mailOptions);
    console.log('Email sent successfully.');
    console.log('Info object:', info);

    // Append the sent email to the "Sent" folder using IMAP
    const imap = new Imap({
      user: senderEmail,
      password: senderPassword,
      host: imapServer,
      port: imapPort,
      tls: true,
    });

    imap.once('ready', () => {
      imap.openBox('Sent', true, (err) => {
        if (err) {
          console.error('Error opening "Sent" folder:', err);
          imap.end();
          return;
        }

        // Create the email message as MIMEText
        const emailMessage = `From: ${senderEmail}\r\nTo: ${recipientEmail}\r\nSubject: ${subject}\r\n\r\n${body}`;

        // Append the sent email to the "Sent" folder
        imap.append(emailMessage, { mailbox: 'Sent' }, (appendErr) => {
          if (appendErr) {
            console.error('Error appending email to "Sent" folder:', appendErr);
          } else {
            console.log('Email appended to "Sent" folder.');
          }
          imap.end();
        });
      });
    });

    imap.once('error', (imapErr) => {
      console.error('IMAP Error:', imapErr);
    });

    imap.connect();
  } catch (error) {
    console.error('Error sending email:', error);
  }
}

// send email notification
const SendRedemptionNotification = async (email, name) => {
  // Define the data to be used in the email template
  const data = {
    email: email,
    name: name
  };

  const subject = 'Item redemption notification for ' + data.name;
  // set receipient email
  const recipientEmail = data.email;

  // Load the email template
  const emailTemplateSource = fs.readFileSync("assets/email/RedemptionNotification.html", "utf8");

  // Compile the template using Handlebars.js
  const emailTemplate = handlebars.compile(emailTemplateSource);

  // Render the email template with the data
  const emailHtml = emailTemplate(data);


  try {
    // Create the email options
    const mailOptions = {
      from: `Team Support Muliaprivileges.com <${senderEmail}>`,
      to: `${data.name} <${recipientEmail}>`,
      subject: subject,
      html: emailHtml,
    };

    // Send the email
    const info = await transporter.sendMail(mailOptions);
    console.log('Email sent successfully.');
    console.log('Info object:', info);

    // Append the sent email to the "Sent" folder using IMAP
    const imap = new Imap({
      user: senderEmail,
      password: senderPassword,
      host: imapServer,
      port: imapPort,
      tls: true,
    });

    imap.once('ready', () => {
      imap.openBox('Sent', true, (err) => {
        if (err) {
          console.error('Error opening "Sent" folder:', err);
          imap.end();
          return;
        }

        // Create the email message as MIMEText
        const emailMessage = `From: ${senderEmail}\r\nTo: ${recipientEmail}\r\nSubject: ${subject}\r\n\r\n${body}`;

        // Append the sent email to the "Sent" folder
        imap.append(emailMessage, { mailbox: 'Sent' }, (appendErr) => {
          if (appendErr) {
            console.error('Error appending email to "Sent" folder:', appendErr);
          } else {
            console.log('Email appended to "Sent" folder.');
          }
          imap.end();
        });
      });
    });

    imap.once('error', (imapErr) => {
      console.error('IMAP Error:', imapErr);
    });

    imap.connect();
  } catch (error) {
    console.error('Error sending email:', error);
  }
}

// send email notification
const SendMissingNotification = async (email, name) => {
  // Define the data to be used in the email template
  const data = {
    email: email,
    name: name
  };

  // email subject
  const subject = 'Missing points notification for ' + data.name;

  // set receipient email
  const recipientEmail = data.email;

  // Load the email template
  const emailTemplateSource = fs.readFileSync("assets/email/MissingNotification.html", "utf8");

  // Compile the template using Handlebars.js
  const emailTemplate = handlebars.compile(emailTemplateSource);

  // Render the email template with the data
  const emailHtml = emailTemplate(data);

  try {

    // Create the email options
    const mailOptions = {
      from: `Team Support Muliaprivileges.com <${senderEmail}>`,
      to: `${data.name} <${recipientEmail}>`,
      subject: subject,
      html: emailHtml,
    };

    // Send the email
    const info = await transporter.sendMail(mailOptions);
    console.log('Email sent successfully.');
    console.log('Info object:', info);

    // Append the sent email to the "Sent" folder using IMAP
    const imap = new Imap({
      user: senderEmail,
      password: senderPassword,
      host: imapServer,
      port: imapPort,
      tls: true,
    });

    imap.once('ready', () => {
      imap.openBox('Sent', true, (err) => {
        if (err) {
          console.error('Error opening "Sent" folder:', err);
          imap.end();
          return;
        }

        // Create the email message as MIMEText
        const emailMessage = `From: ${senderEmail}\r\nTo: ${recipientEmail}\r\nSubject: ${subject}\r\n\r\n${body}`;

        // Append the sent email to the "Sent" folder
        imap.append(emailMessage, { mailbox: 'Sent' }, (appendErr) => {
          if (appendErr) {
            console.error('Error appending email to "Sent" folder:', appendErr);
          } else {
            console.log('Email appended to "Sent" folder.');
          }
          imap.end();
        });
      });
    });

    imap.once('error', (imapErr) => {
      console.error('IMAP Error:', imapErr);
    });

    imap.connect();
  } catch (error) {
    console.error('Error sending email:', error);
  }
}

const SendResetPasswordEmail = async (memberData) => {
  // Define the data to be used in the email template
  const data = {
    email: memberData.primary_email,
    firstName: memberData.first,
    lastName: memberData.last,
    password: memberData.temp_password,
    host: process.env.MP_HOST
  };

  // email subject
  const subject = 'Muliaprivileges.com - Reset Your Password!';

  // Load the email template
  const emailTemplateSource = fs.readFileSync("assets/email/ResetForgotPassword.html", "utf8");

  // set receipient email
  const recipientEmail = data.email;

  // Compile the template using Handlebars.js
  const emailTemplate = handlebars.compile(emailTemplateSource);

  // Render the email template with the data
  const emailHtml = emailTemplate(data);

  try {
    // Create the email options
    const mailOptions = {
      from: `Team Support Muliaprivileges.com <${senderEmail}>`,
      to: `${data.firstName} ${data.lastName} <${recipientEmail}>`,
      subject: subject,
      html: emailHtml,
    };

    // Send the email
    const info = await transporter.sendMail(mailOptions);
    console.log('Email sent successfully.');
    console.log('Info object:', info);

    // Append the sent email to the "Sent" folder using IMAP
    const imap = new Imap({
      user: senderEmail,
      password: senderPassword,
      host: imapServer,
      port: imapPort,
      tls: true,
    });

    imap.once('ready', () => {
      imap.openBox('Sent', true, (err) => {
        if (err) {
          console.error('Error opening "Sent" folder:', err);
          imap.end();
          return;
        }

        // Create the email message as MIMEText
        const emailMessage = `From: ${senderEmail}\r\nTo: ${recipientEmail}\r\nSubject: ${subject}\r\n\r\n${body}`;

        // Append the sent email to the "Sent" folder
        imap.append(emailMessage, { mailbox: 'Sent' }, (appendErr) => {
          if (appendErr) {
            console.error('Error appending email to "Sent" folder:', appendErr);
          } else {
            console.log('Email appended to "Sent" folder.');
          }
          imap.end();
        });
      });
    });

    imap.once('error', (imapErr) => {
      console.error('IMAP Error:', imapErr);
    });

    imap.connect();
  } catch (error) {
    console.error('Error sending email:', error);
  }
}

const SendRedemptionRequestEmail = async (memberData) => {
  // Define the data to be used in the email template
  const data = {
    member_id: memberData.id,
    member_name: memberData.name,
    member_email: memberData.email,
    redemption_date: memberData.redemption_date,
    items: memberData.items,
    previous_balance: memberData.previous_balance,
    claimed_points: memberData.claimed_points,
    remaining_points: memberData.remaining_points,
    surcharge_type_id: memberData.surcharge_type_id,
    surcharge_type_name: memberData.surcharge_type_name,
    surcharge_points: memberData.surcharge_points,
    voucher_location: memberData.voucher_location,
    is_loyalty: memberData.is_loyalty,
    is_revenue: memberData.is_revenue,
    is_financial: memberData.is_financial,
    is_resident: memberData.is_resident,
    approve_status_id: memberData.approve_status_id,
    is_manual: memberData.is_manual,
    username: memberData.username,
  };

  // email subject
  const subject = 'Item redemption request for ' + data.member_name;

  // Load the email template
  const emailTemplateSource = data.surcharge_type_id === 0 ? fs.readFileSync("assets/email/RedemptionRequestMulti.html", "utf8") : fs.readFileSync("assets/email/RedemptionRequestMultiSurcharge.html", "utf8");

  // Compile the template using Handlebars.js
  const emailTemplate = handlebars.compile(emailTemplateSource);

  // Render the email template with the data
  const emailHtml = emailTemplate(data);

  // sender
  const sender = data.member_email;

  // set receipient email
  const recipientEmail = 'support@muliaprivileges.com';
  let ccEmail = null;

  if (data.location === 'BALI') {
    // set cc email
    ccEmail = 'muliaprivilege@themulia.com';
  } else {
    // set cc email
    ccEmail = 'muliaprivilege@hotelmuliasenayan.com';
  }

  try {
    // Create the email options
    const mailOptions = {
      from: `${data.member_name} <${senderEmail}>`,
      to: `Team Support Muliaprivileges.com <${recipientEmail}>`,
      cc: [`Muliaprivileges.com <${ccEmail}>`],
      bcc: bccEmails,
      subject: subject,
      html: emailHtml,
      replyTo: `${data.member_name} <${sender}>`, // Menambahkan Reply-To header agar server tahu alamat pengirim sebenarnya
    };

    // Send the email
    const info = await transporter.sendMail(mailOptions);
    console.log('Email sent successfully.');
    console.log('Info object:', info);

    // Append the sent email to the "Sent" folder using IMAP
    const imap = new Imap({
      user: senderEmail,
      password: senderPassword,
      host: imapServer,
      port: imapPort,
      tls: true,
    });

    imap.once('ready', () => {
      imap.openBox('Sent', true, (err) => {
        if (err) {
          console.error('Error opening "Sent" folder:', err);
          imap.end();
          return;
        }

        // Create the email message as MIMEText
        const emailMessage = `From: ${senderEmail}\r\nTo: ${recipientEmail}\r\nSubject: ${subject}\r\n\r\n${body}`;

        // Append the sent email to the "Sent" folder
        imap.append(emailMessage, { mailbox: 'Sent' }, (appendErr) => {
          if (appendErr) {
            console.error('Error appending email to "Sent" folder:', appendErr);
          } else {
            console.log('Email appended to "Sent" folder.');
          }
          imap.end();
        });
      });
    });

    imap.once('error', (imapErr) => {
      console.error('IMAP Error:', imapErr);
    });

    imap.connect();
  } catch (error) {
    console.error('Error sending email:', error);
  }
}


const SendVoucherRedeemptionEmail = async (memberData) => {
  // Define the data to be used in the email template
  const data = {
    member_id: memberData.id,
    member_name: memberData.name,
    member_email: memberData.email,
    item_id: memberData.reedem_id,
    item_name: memberData.reedem_name,
    beneficiary: memberData.beneficiary,
    comments: memberData.comments,
    voucher_no: memberData.voucher_no,
    date_expired: memberData.date_expired,
    is_used: memberData.is_used,
    outlet_name: memberData.outlet_name,
    createdBy: memberData.createdBy,
    updatedBy: memberData.updatedBy,
    createdAt: memberData.createdAt,
    updatedAt: memberData.updatedAt,
  };


  const subject = 'Voucher Redeemption request for ' + data.member_name;

  // Load the email template
  const emailTemplateSource = fs.readFileSync("assets/email/VoucherRedemption.html", "utf8");

  // Compile the template using Handlebars.js
  const emailTemplate = handlebars.compile(emailTemplateSource);

  // Render the email template with the data
  const emailHtml = emailTemplate(data);

  // sender
  const sender = data.member_email;

  // set receipient email
  const recipientEmail = 'support@muliaprivileges.com';
  let ccEmail = null;

  if (data.location === 'BALI') {
    // set cc email
    ccEmail = 'muliaprivilege@themulia.com';
  } else {
    // set cc email
    ccEmail = 'muliaprivilege@hotelmuliasenayan.com';
  }

  try {
    // Create the email options
    const mailOptions = {
      from: `${data.member_name} <${senderEmail}>`,
      to: `Team Support Muliaprivileges.com <${recipientEmail}>`,
      cc: [`Muliaprivileges.com <${ccEmail}>`],
      bcc: bccEmails,
      subject: subject,
      html: emailHtml,
      replyTo: `${data.member_name} <${sender}>`, // Menambahkan Reply-To header agar server tahu alamat pengirim sebenarnya
    };

    // Send the email
    const info = await transporter.sendMail(mailOptions);
    console.log('Email sent successfully.');
    console.log('Info object:', info);

    // Append the sent email to the "Sent" folder using IMAP
    const imap = new Imap({
      user: senderEmail,
      password: senderPassword,
      host: imapServer,
      port: imapPort,
      tls: true,
    });

    imap.once('ready', () => {
      imap.openBox('Sent', true, (err) => {
        if (err) {
          console.error('Error opening "Sent" folder:', err);
          imap.end();
          return;
        }

        // Create the email message as MIMEText
        const emailMessage = `From: ${senderEmail}\r\nTo: ${recipientEmail}\r\nSubject: ${subject}\r\n\r\n${body}`;

        // Append the sent email to the "Sent" folder
        imap.append(emailMessage, { mailbox: 'Sent' }, (appendErr) => {
          if (appendErr) {
            console.error('Error appending email to "Sent" folder:', appendErr);
          } else {
            console.log('Email appended to "Sent" folder.');
          }
          imap.end();
        });
      });
    });

    imap.once('error', (imapErr) => {
      console.error('IMAP Error:', imapErr);
    });

    imap.connect();
  } catch (error) {
    console.error('Error sending email:', error);
  }
}


const SendRewardClaimForm = async (memberData) => {
  // Define the data to be used in the email template
  let total_claimed = 0;
  total_claimed = parseInt(memberData.claimed_points) + parseInt(memberData.surcharge_points);
  const data = {
    member_id: memberData.id,
    member_name: memberData.name,
    member_email: memberData.email,
    redemption_date: memberData.redemption_date,
    items: memberData.items,
    previous_balance: memberData.previous_balance,
    number_of_points: memberData.claimed_points,
    total_climed_points: total_claimed,
    remaining_points: memberData.remaining_points,
    surcharge_type_id: memberData.surcharge_type_id,
    surcharge_type_name: memberData.surcharge_type_name,
    surcharge_points: memberData.surcharge_points,
    reedem_point_id: memberData.reedem_point_id,
    location: memberData.voucher_location,
    is_loyalty: memberData.is_loyalty,
    is_revenue: memberData.is_revenue,
    is_financial: memberData.is_financial,
    is_resident: memberData.is_resident,
    approve_status_id: memberData.approve_status_id,
    token: memberData.token,
    approve_link: process.env.MP_HOST,
    approve_image_link: approve_image_Url,
    approved_image_link: approved_image_Url
  };

  const subject = 'Muliaprivileges Reward Claim Form for ' + data.member_name;

  // Load the email template
  const emailTemplateSource = memberData.surcharge_type_id === 0 ? fs.readFileSync("assets/email/RewardClaimFormMulti_Loyalty.html", "utf8") : fs.readFileSync("assets/email/RewardClaimFormMulti_Loyalty_Surcharge.html", "utf8");

  // Compile the template using Handlebars.js
  const emailTemplate = handlebars.compile(emailTemplateSource);

  // Render the email template with the data
  const emailHtml = emailTemplate(data);

  // sender
  const sender = data.member_email;

  let recipientEmail = null;
  let ccEmail = null;

  console.log(data.location, ">>>>>>> ISI LOCATION >>>>>>>>>>>>>>>")

  if (data.location === 'BALI') {
    // set receipient email
    recipientEmail = 'fellicia.wijaya@themulia.com';
    // set cc email
    ccEmail = 'muliaprivilege@themulia.com';
  } else {
    // set receipient email
    recipientEmail = 'muliaprivilege@hotelmulia.com';
    // set cc email
    ccEmail = 'hanny.maharani@hotelmulia.com';
  }

  try {
    // Create the email options
    const mailOptions = {
      from: `${data.member_name} <${senderEmail}>`,
      to: `Loyalty Manager Muliaprivileges <${recipientEmail}>`,
      cc: [`Team of Loyalty Muliaprivileges <${ccEmail}>`],
      bcc: bccEmails,
      subject: subject,
      html: emailHtml,
      replyTo: `${data.member_name} <${sender}>`, // Menambahkan Reply-To header agar server tahu alamat pengirim sebenarnya
    };

    // Send the email
    const info = await transporter.sendMail(mailOptions);
    console.log('Email sent successfully.');
    console.log('Info object:', info);

    // Append the sent email to the "Sent" folder using IMAP
    const imap = new Imap({
      user: senderEmail,
      password: senderPassword,
      host: imapServer,
      port: imapPort,
      tls: true,
    });

    imap.once('ready', () => {
      imap.openBox('Sent', true, (err) => {
        if (err) {
          console.error('Error opening "Sent" folder:', err);
          imap.end();
          return;
        }

        // Create the email message as MIMEText
        const emailMessage = `From: ${senderEmail}\r\nTo: ${recipientEmail}\r\nSubject: ${subject}\r\n\r\n${body}`;

        // Append the sent email to the "Sent" folder
        imap.append(emailMessage, { mailbox: 'Sent' }, (appendErr) => {
          if (appendErr) {
            console.error('Error appending email to "Sent" folder:', appendErr);
          } else {
            console.log('Email appended to "Sent" folder.');
          }
          imap.end();
        });
      });
    });

    imap.once('error', (imapErr) => {
      console.error('IMAP Error:', imapErr);
    });

    imap.connect();
  } catch (error) {
    console.error('Error sending email:', error);
  }
}


const SendSurchargeClaimForm = async (memberData) => {
  // Define the data to be used in the email template
  let total_claimed = 0;
  total_claimed = parseInt(memberData.total_points) + parseInt(memberData.surcharge_points);
  const data = {
    member_id: memberData.id,
    member_name: memberData.name,
    member_email: memberData.email,
    reedem_id: memberData.reedem_id,
    reedem_name: memberData.reedem_name,
    reedem_type: memberData.reedem_type,
    location: memberData.location,
    redemption_date: memberData.redemption_date,
    previous_balance: memberData.previous_balance,
    remaining_points: memberData.remaining_points,
    required_points: memberData.required_points,
    qty_points: memberData.qty_points,
    number_of_points: memberData.total_points,
    surcharge_type_id: memberData.surcharge_type_id,
    surcharge_type_name: memberData.surcharge_type_name,
    surcharge_points: memberData.surcharge_points,
    total_climed_points: total_claimed,
    beneficiary: memberData.beneficiary,
    comments: memberData.comments,
    reedem_point_id: memberData.reedem_point_id,
    is_addsurcharge_loyalty: memberData.is_addsurcharge_loyalty,
    is_addsurcharge_revenue: memberData.is_addsurcharge_revenue,
    is_addsurcharge_financial: memberData.is_addsurcharge_financial,
    is_addsurcharge_resident: memberData.is_addsurcharge_resident,
    addsurcharge_status_id: memberData.addsurcharge_status_id,
    token: memberData.token,
    approve_link: process.env.MP_HOST,
    approve_image_link: approve_image_Url,
    approved_image_link: approved_image_Url
  };


  const subject = 'Muliaprivileges Reward Claim Form for ' + data.member_name;

  // Load the email template
  const emailTemplateSource = fs.readFileSync("assets/email/RewardClaimForm_Loyalty_Surcharge.html", "utf8");

  // Compile the template using Handlebars.js
  const emailTemplate = handlebars.compile(emailTemplateSource);

  // Render the email template with the data
  const emailHtml = emailTemplate(data);

  // sender
  const sender = data.member_email;

  let recipientEmail = null;
  let ccEmail = null;

  console.log(data.location, ">>>>>>> ISI LOCATION >>>>>>>>>>>>>>>")

  if (data.location === 'BALI') {
    // set receipient email
    recipientEmail = 'fellicia.wijaya@themulia.com';
    // set cc email
    ccEmail = 'muliaprivilege@themulia.com';
  } else {
    // set receipient email
    recipientEmail = 'muliaprivilege@hotelmulia.com';
    // set cc email
    ccEmail = 'hanny.maharani@hotelmulia.com';
  }

  try {
    // Create the email options
    const mailOptions = {
      from: `${data.member_name} <${senderEmail}>`,
      to: `Loyalty Manager Muliaprivileges <${recipientEmail}>`,
      cc: [`Team of Loyalty Muliaprivileges <${ccEmail}>`],
      bcc: bccEmails,
      subject: subject,
      html: emailHtml,
      replyTo: `${data.member_name} <${sender}>`, // Menambahkan Reply-To header agar server tahu alamat pengirim sebenarnya
    };

    // Send the email
    const info = await transporter.sendMail(mailOptions);
    console.log('Email sent successfully.');
    console.log('Info object:', info);

    // Append the sent email to the "Sent" folder using IMAP
    const imap = new Imap({
      user: senderEmail,
      password: senderPassword,
      host: imapServer,
      port: imapPort,
      tls: true,
    });

    imap.once('ready', () => {
      imap.openBox('Sent', true, (err) => {
        if (err) {
          console.error('Error opening "Sent" folder:', err);
          imap.end();
          return;
        }

        // Create the email message as MIMEText
        const emailMessage = `From: ${senderEmail}\r\nTo: ${recipientEmail}\r\nSubject: ${subject}\r\n\r\n${body}`;

        // Append the sent email to the "Sent" folder
        imap.append(emailMessage, { mailbox: 'Sent' }, (appendErr) => {
          if (appendErr) {
            console.error('Error appending email to "Sent" folder:', appendErr);
          } else {
            console.log('Email appended to "Sent" folder.');
          }
          imap.end();
        });
      });
    });

    imap.once('error', (imapErr) => {
      console.error('IMAP Error:', imapErr);
    });

    imap.connect();
  } catch (error) {
    console.error('Error sending email:', error);
  }
}

const SendEmailFromLoyaltyToRevenue = async (memberData) => {
  // Define the data to be used in the email template
  const data = {
    member_id: memberData.member_id,
    member_name: memberData.full_name,
    member_email: memberData.email,
    items: memberData.items,
    location: memberData.location,
    redemption_date: memberData.redemption_date,
    previous_balance: memberData.previous_balance,
    remaining_points: memberData.remaining_points,
    number_of_points: memberData.claimed_point,
    reedem_point_id: memberData.id,
    surcharge_type_id: memberData.surcharge_type_id,
    surcharge_type_name: memberData.surcharge_type_name,
    surcharge_points: memberData.surcharge_points,
    total_climed_points: memberData.total_climed_points,
    is_loyalty: memberData.is_loyalty,
    is_revenue: memberData.is_revenue,
    is_financial: memberData.is_financial,
    is_resident: memberData.is_resident,
    is_manual: memberData.is_manual,
    approve_status_id: memberData.approve_status_id,
    token: memberData.token,
    approve_link: process.env.MP_HOST,
    approve_image_link: approve_image_Url,
    approved_image_link: approved_image_Url
  };

  const subject = 'Muliaprivileges Reward Claim Form for ' + data.member_name;

  // Load the email template
  const emailTemplateSource = memberData.surcharge_type_id === 0 ? fs.readFileSync("assets/email/RewardClaimFormMulti_Revenue.html", "utf8") : fs.readFileSync("assets/email/RewardClaimFormMulti_Revenue_Surcharge.html", "utf8");

  // Compile the template using Handlebars.js
  const emailTemplate = handlebars.compile(emailTemplateSource);

  // Render the email template with the data
  const emailHtml = emailTemplate(data);

  let sender = null;
  let recipientEmail = null;
  let ccEmail = null;

  if (data.location === 'BALI') {
    // sender
    sender = 'fellicia.wijaya@themulia.com';
    // set receipient email
    recipientEmail = 'bagus.artadi@themulia.com';
    // set cc email
    ccEmail = 'cecilia.yunita@themulia.com';
  } else {
    // sender
    sender = 'muliaprivilege@hotelmulia.com';
    // set receipient email
    recipientEmail = 'dyah.saptorini@hotelmulia.com';
    // set cc email
    ccEmail = 'angela.hadisiswojo@hotelmulia.com';
  }

  try {
    // Create the email options
    const mailOptions = {
      from: `Loyalty Manager Muliaprivileges <${senderEmail}>`,
      to: `Director of Revenue Management <${recipientEmail}>`,
      cc: [`Team of Revenue Management <${ccEmail}>`],
      bcc: bccEmails,
      subject: subject,
      html: emailHtml,
      replyTo: `Loyalty Manager Muliaprivileges <${sender}>`, // Menambahkan Reply-To header agar server tahu alamat pengirim sebenarnya
    };

    // Send the email
    const info = await transporter.sendMail(mailOptions);
    console.log('Email sent successfully.');
    console.log('Info object:', info);

    // Append the sent email to the "Sent" folder using IMAP
    const imap = new Imap({
      user: senderEmail,
      password: senderPassword,
      host: imapServer,
      port: imapPort,
      tls: true,
    });

    imap.once('ready', () => {
      imap.openBox('Sent', true, (err) => {
        if (err) {
          console.error('Error opening "Sent" folder:', err);
          imap.end();
          return;
        }

        // Create the email message as MIMEText
        const emailMessage = `From: ${senderEmail}\r\nTo: ${recipientEmail}\r\nSubject: ${subject}\r\n\r\n${body}`;

        // Append the sent email to the "Sent" folder
        imap.append(emailMessage, { mailbox: 'Sent' }, (appendErr) => {
          if (appendErr) {
            console.error('Error appending email to "Sent" folder:', appendErr);
          } else {
            console.log('Email appended to "Sent" folder.');
          }
          imap.end();
        });
      });
    });

    imap.once('error', (imapErr) => {
      console.error('IMAP Error:', imapErr);
    });

    imap.connect();
  } catch (error) {
    console.error('Error sending email:', error);
  }
}

const SendEmailSurchargeFromLoyaltyToRevenue = async (memberData) => {
  // Define the data to be used in the email template
  const data = {
    member_id: memberData.member_id,
    member_name: memberData.full_name,
    member_email: memberData.email,
    items: memberData.items,
    location: memberData.location,
    redemption_date: memberData.redemption_date,
    previous_balance: memberData.previous_balance,
    remaining_points: memberData.remaining_points,
    number_of_points: memberData.claimed_point,
    reedem_point_id: memberData.id,
    surcharge_type_id: memberData.surcharge_type_id,
    surcharge_type_name: memberData.surcharge_type_name,
    surcharge_points: memberData.surcharge_points,
    total_climed_points: memberData.total_climed_points,
    is_addsurcharge_loyalty: memberData.is_addsurcharge_loyalty,
    is_addsurcharge_revenue: memberData.is_addsurcharge_revenue,
    is_addsurcharge_financial: memberData.is_addsurcharge_financial,
    is_addsurcharge_resident: memberData.is_addsurcharge_resident,
    is_manual: memberData.is_manual,
    addsurcharge_status_id: memberData.addsurcharge_status_id,
    token: memberData.token,
    approve_link: process.env.MP_HOST,
    approve_image_link: approve_image_Url,
    approved_image_link: approved_image_Url
  };

  const subject = 'Muliaprivileges Reward Claim Form for ' + data.member_name;

  // Load the email template
  const emailTemplateSource = fs.readFileSync("assets/email/RewardClaimFormMulti_Revenue_Surcharge.html", "utf8");

  // Compile the template using Handlebars.js
  const emailTemplate = handlebars.compile(emailTemplateSource);

  // Render the email template with the data
  const emailHtml = emailTemplate(data);

  let sender = null;
  let recipientEmail = null;
  let ccEmail = null;

  if (data.location === 'BALI') {
    // sender
    sender = 'fellicia.wijaya@themulia.com';
    // set receipient email
    recipientEmail = 'bagus.artadi@themulia.com';
    // set cc email
    ccEmail = 'cecilia.yunita@themulia.com';
  } else {
    // sender
    sender = 'muliaprivilege@hotelmulia.com';
    // set receipient email
    recipientEmail = 'dyah.saptorini@hotelmulia.com';
    // set cc email
    ccEmail = 'angela.hadisiswojo@hotelmulia.com';
  }

  try {
    // Create the email options
    const mailOptions = {
      from: `Loyalty Manager Muliaprivileges <${senderEmail}>`,
      to: `Director of Revenue Management <${recipientEmail}>`,
      cc: [`Team of Revenue Management <${ccEmail}>`],
      bcc: bccEmails,
      subject: subject,
      html: emailHtml,
      replyTo: `Loyalty Manager Muliaprivileges <${sender}>`, // Menambahkan Reply-To header agar server tahu alamat pengirim sebenarnya
    };

    // Send the email
    const info = await transporter.sendMail(mailOptions);
    console.log('Email sent successfully.');
    console.log('Info object:', info);

    // Append the sent email to the "Sent" folder using IMAP
    const imap = new Imap({
      user: senderEmail,
      password: senderPassword,
      host: imapServer,
      port: imapPort,
      tls: true,
    });

    imap.once('ready', () => {
      imap.openBox('Sent', true, (err) => {
        if (err) {
          console.error('Error opening "Sent" folder:', err);
          imap.end();
          return;
        }

        // Create the email message as MIMEText
        const emailMessage = `From: ${senderEmail}\r\nTo: ${recipientEmail}\r\nSubject: ${subject}\r\n\r\n${body}`;

        // Append the sent email to the "Sent" folder
        imap.append(emailMessage, { mailbox: 'Sent' }, (appendErr) => {
          if (appendErr) {
            console.error('Error appending email to "Sent" folder:', appendErr);
          } else {
            console.log('Email appended to "Sent" folder.');
          }
          imap.end();
        });
      });
    });

    imap.once('error', (imapErr) => {
      console.error('IMAP Error:', imapErr);
    });

    imap.connect();
  } catch (error) {
    console.error('Error sending email:', error);
  }
}

const SendEmailFromRevenueToFinancial = async (memberData) => {
  // Define the data to be used in the email template
  const data = {
    member_id: memberData.member_id,
    member_name: memberData.full_name,
    member_email: memberData.email,
    items: memberData.items,
    location: memberData.location,
    redemption_date: memberData.redemption_date,
    previous_balance: memberData.previous_balance,
    remaining_points: memberData.remaining_points,
    number_of_points: memberData.claimed_point,
    reedem_point_id: memberData.id,
    surcharge_type_id: memberData.surcharge_type_id,
    surcharge_type_name: memberData.surcharge_type_name,
    surcharge_points: memberData.surcharge_points,
    total_climed_points: memberData.total_climed_points,
    is_loyalty: memberData.is_loyalty,
    is_revenue: memberData.is_revenue,
    is_financial: memberData.is_financial,
    is_resident: memberData.is_resident,
    is_manual: memberData.is_manual,
    approve_status_id: memberData.approve_status_id,
    token: memberData.token,
    approve_link: process.env.MP_HOST,
    approve_image_link: approve_image_Url,
    approved_image_link: approved_image_Url
  };

  const subject = 'Muliaprivileges Reward Claim Form for ' + data.member_name;

  // Load the email template
  const emailTemplateSource = memberData.surcharge_type_id === 0 ? fs.readFileSync("assets/email/RewardClaimFormMulti_Finance.html", "utf8") : fs.readFileSync("assets/email/RewardClaimFormMulti_Finance_Surcharge.html", "utf8");

  // Compile the template using Handlebars.js
  const emailTemplate = handlebars.compile(emailTemplateSource);

  // Render the email template with the data
  const emailHtml = emailTemplate(data);

  let sender = null;
  let recipientEmail = null;
  let ccEmail = null;

  if (data.location === 'BALI') {
    // sender
    sender = 'bagus.artadi@themulia.com';

    // set receipient email
    recipientEmail = 'reynaldo.yanga@themulia.com';

    // set cc email
    ccEmail = 'barry.panjaya@themulia.com';
  } else {
    // sender
    sender = 'dyah.saptorini@hotelmulia.com';

    // set receipient email
    recipientEmail = 'diaz.patria@hotelmulia.com';
  }


  try {
    // Create the email options
    const mailOptions = {
      from: `Director of Revenue Management <${senderEmail}>`,
      to: `Financial Controller <${recipientEmail}>`,
      ...(ccEmail ? { cc: [`Team of Financial Controller <${ccEmail}>`] } : {}),
      bcc: bccEmails,
      subject: subject,
      html: emailHtml,
      replyTo: `Director of Revenue Management <${sender}>`, // Menambahkan Reply-To header agar server tahu alamat pengirim sebenarnya

    };

    // Send the email
    const info = await transporter.sendMail(mailOptions);
    console.log('Email sent successfully.');
    console.log('Info object:', info);

    // Append the sent email to the "Sent" folder using IMAP
    const imap = new Imap({
      user: senderEmail,
      password: senderPassword,
      host: imapServer,
      port: imapPort,
      tls: true,
    });

    imap.once('ready', () => {
      imap.openBox('Sent', true, (err) => {
        if (err) {
          console.error('Error opening "Sent" folder:', err);
          imap.end();
          return;
        }

        // Create the email message as MIMEText
        const emailMessage = `From: ${senderEmail}\r\nTo: ${recipientEmail}\r\nSubject: ${subject}\r\n\r\n${body}`;

        // Append the sent email to the "Sent" folder
        imap.append(emailMessage, { mailbox: 'Sent' }, (appendErr) => {
          if (appendErr) {
            console.error('Error appending email to "Sent" folder:', appendErr);
          } else {
            console.log('Email appended to "Sent" folder.');
          }
          imap.end();
        });
      });
    });

    imap.once('error', (imapErr) => {
      console.error('IMAP Error:', imapErr);
    });

    imap.connect();
  } catch (error) {
    console.error('Error sending email:', error);
  }
}

const SendEmailSurchargeFromRevenueToFinancial = async (memberData) => {
  // Define the data to be used in the email template
  const data = {
    member_id: memberData.member_id,
    member_name: memberData.full_name,
    member_email: memberData.email,
    items: memberData.items,
    location: memberData.location,
    redemption_date: memberData.redemption_date,
    previous_balance: memberData.previous_balance,
    remaining_points: memberData.remaining_points,
    number_of_points: memberData.claimed_point,
    reedem_point_id: memberData.id,
    surcharge_type_id: memberData.surcharge_type_id,
    surcharge_type_name: memberData.surcharge_type_name,
    surcharge_points: memberData.surcharge_points,
    total_climed_points: memberData.total_climed_points,
    is_addsurcharge_loyalty: memberData.is_addsurcharge_loyalty,
    is_addsurcharge_revenue: memberData.is_addsurcharge_revenue,
    is_addsurcharge_financial: memberData.is_addsurcharge_financial,
    is_addsurcharge_resident: memberData.is_addsurcharge_resident,
    is_manual: memberData.is_manual,
    addsurcharge_status_id: memberData.addsurcharge_status_id,
    token: memberData.token,
    approve_link: process.env.MP_HOST,
    approve_image_link: approve_image_Url,
    approved_image_link: approved_image_Url
  };

  const subject = 'Muliaprivileges Reward Claim Form for ' + data.member_name;

  // Load the email template
  const emailTemplateSource = fs.readFileSync("assets/email/RewardClaimFormMulti_Finance_Surcharge.html", "utf8");

  // Compile the template using Handlebars.js
  const emailTemplate = handlebars.compile(emailTemplateSource);

  // Render the email template with the data
  const emailHtml = emailTemplate(data);

  let sender = null;
  let recipientEmail = null;
  let ccEmail = null;

  if (data.location === 'BALI') {
    // sender
    sender = 'bagus.artadi@themulia.com';

    // set receipient email
    recipientEmail = 'reynaldo.yanga@themulia.com';

    // set cc email
    ccEmail = 'barry.panjaya@themulia.com';
  } else {
    // sender
    sender = 'dyah.saptorini@hotelmulia.com';

    // set receipient email
    recipientEmail = 'diaz.patria@hotelmulia.com';
  }


  try {
    // Create the email options
    const mailOptions = {
      from: `Director of Revenue Management <${senderEmail}>`,
      to: `Financial Controller <${recipientEmail}>`,
      ...(ccEmail ? { cc: [`Team of Financial Controller <${ccEmail}>`] } : {}),
      bcc: bccEmails,
      subject: subject,
      html: emailHtml,
      replyTo: `Director of Revenue Management <${sender}>`, // Menambahkan Reply-To header agar server tahu alamat pengirim sebenarnya

    };

    // Send the email
    const info = await transporter.sendMail(mailOptions);
    console.log('Email sent successfully.');
    console.log('Info object:', info);

    // Append the sent email to the "Sent" folder using IMAP
    const imap = new Imap({
      user: senderEmail,
      password: senderPassword,
      host: imapServer,
      port: imapPort,
      tls: true,
    });

    imap.once('ready', () => {
      imap.openBox('Sent', true, (err) => {
        if (err) {
          console.error('Error opening "Sent" folder:', err);
          imap.end();
          return;
        }

        // Create the email message as MIMEText
        const emailMessage = `From: ${senderEmail}\r\nTo: ${recipientEmail}\r\nSubject: ${subject}\r\n\r\n${body}`;

        // Append the sent email to the "Sent" folder
        imap.append(emailMessage, { mailbox: 'Sent' }, (appendErr) => {
          if (appendErr) {
            console.error('Error appending email to "Sent" folder:', appendErr);
          } else {
            console.log('Email appended to "Sent" folder.');
          }
          imap.end();
        });
      });
    });

    imap.once('error', (imapErr) => {
      console.error('IMAP Error:', imapErr);
    });

    imap.connect();
  } catch (error) {
    console.error('Error sending email:', error);
  }
}

const SendEmailFromFinancialToResident = async (memberData) => {
  // Define the data to be used in the email template
  const data = {
    member_id: memberData.member_id,
    member_name: memberData.full_name,
    member_email: memberData.email,
    items: memberData.items,
    location: memberData.location,
    redemption_date: memberData.redemption_date,
    previous_balance: memberData.previous_balance,
    remaining_points: memberData.remaining_points,
    number_of_points: memberData.claimed_point,
    reedem_point_id: memberData.id,
    surcharge_type_id: memberData.surcharge_type_id,
    surcharge_type_name: memberData.surcharge_type_name,
    surcharge_points: memberData.surcharge_points,
    total_climed_points: memberData.total_climed_points,
    is_loyalty: memberData.is_loyalty,
    is_revenue: memberData.is_revenue,
    is_financial: memberData.is_financial,
    is_resident: memberData.is_resident,
    is_manual: memberData.is_manual,
    approve_status_id: memberData.approve_status_id,
    token: memberData.token,
    approve_link: process.env.MP_HOST,
    approve_image_link: approve_image_Url,
    approved_image_link: approved_image_Url
  };

  const subject = 'Muliaprivileges Reward Claim Form for ' + data.member_name;

  // Load the email template
  const emailTemplateSource = memberData.surcharge_type_id === 0 ? fs.readFileSync("assets/email/RewardClaimFormMulti_Resident.html", "utf8") : fs.readFileSync("assets/email/RewardClaimFormMulti_Resident_Surcharge.html", "utf8");

  // Compile the template using Handlebars.js
  const emailTemplate = handlebars.compile(emailTemplateSource);

  // Render the email template with the data
  const emailHtml = emailTemplate(data);

  let sender = null;
  let recipientEmail = null;
  let ccEmail = null;

  if (data.location === 'BALI') {
    // sender
    sender = 'reynaldo.yanga@themulia.com';

    // set receipient email
    recipientEmail = 'adam.bardetta@themulia.com';

    // set cc email
    ccEmail = 'sonny.koentjoro@themulia.com';
  } else {
    // sender
    sender = 'diaz.patria@hotelmulia.com';

    // set receipient email
    recipientEmail = 'martino.tanor@hotelmulia.com';
  }

  try {
    // Create the email options
    const mailOptions = {
      from: `Financial Controller <${senderEmail}>`,
      to: `General Manager <${recipientEmail}>`,
      ...(ccEmail ? { cc: [`Hotel Manager <${ccEmail}>`] } : {}),
      bcc: bccEmails,
      subject: subject,
      html: emailHtml,
      replyTo: `Financial Controller <${sender}>`, // Menambahkan Reply-To header agar server tahu alamat pengirim sebenarnya
    };

    // Send the email
    const info = await transporter.sendMail(mailOptions);
    console.log('Email sent successfully.');
    console.log('Info object:', info);

    // Append the sent email to the "Sent" folder using IMAP
    const imap = new Imap({
      user: senderEmail,
      password: senderPassword,
      host: imapServer,
      port: imapPort,
      tls: true,
    });

    imap.once('ready', () => {
      imap.openBox('Sent', true, (err) => {
        if (err) {
          console.error('Error opening "Sent" folder:', err);
          imap.end();
          return;
        }

        // Create the email message as MIMEText
        const emailMessage = `From: ${senderEmail}\r\nTo: ${recipientEmail}\r\nSubject: ${subject}\r\n\r\n${body}`;

        // Append the sent email to the "Sent" folder
        imap.append(emailMessage, { mailbox: 'Sent' }, (appendErr) => {
          if (appendErr) {
            console.error('Error appending email to "Sent" folder:', appendErr);
          } else {
            console.log('Email appended to "Sent" folder.');
          }
          imap.end();
        });
      });
    });

    imap.once('error', (imapErr) => {
      console.error('IMAP Error:', imapErr);
    });

    imap.connect();
  } catch (error) {
    console.error('Error sending email:', error);
  }
}

const SendEmailSurchargeFromFinancialToResident = async (memberData) => {
  // Define the data to be used in the email template
  const data = {
    member_id: memberData.member_id,
    member_name: memberData.full_name,
    member_email: memberData.email,
    items: memberData.items,
    location: memberData.location,
    redemption_date: memberData.redemption_date,
    previous_balance: memberData.previous_balance,
    remaining_points: memberData.remaining_points,
    number_of_points: memberData.claimed_point,
    reedem_point_id: memberData.id,
    surcharge_type_id: memberData.surcharge_type_id,
    surcharge_type_name: memberData.surcharge_type_name,
    surcharge_points: memberData.surcharge_points,
    total_climed_points: memberData.total_climed_points,
    is_addsurcharge_loyalty: memberData.is_addsurcharge_loyalty,
    is_addsurcharge_revenue: memberData.is_addsurcharge_revenue,
    is_addsurcharge_financial: memberData.is_addsurcharge_financial,
    is_addsurcharge_resident: memberData.is_addsurcharge_resident,
    is_manual: memberData.is_manual,
    addsurcharge_status_id: memberData.addsurcharge_status_id,
    token: memberData.token,
    approve_link: process.env.MP_HOST,
    approve_image_link: approve_image_Url,
    approved_image_link: approved_image_Url
  };

  const subject = 'Muliaprivileges Reward Claim Form for ' + data.member_name;

  // Load the email template
  const emailTemplateSource = fs.readFileSync("assets/email/RewardClaimFormMulti_Resident_Surcharge.html", "utf8");

  // Compile the template using Handlebars.js
  const emailTemplate = handlebars.compile(emailTemplateSource);

  // Render the email template with the data
  const emailHtml = emailTemplate(data);

  let sender = null;
  let recipientEmail = null;
  let ccEmail = null;

  if (data.location === 'BALI') {
    // sender
    sender = 'reynaldo.yanga@themulia.com';

    // set receipient email
    recipientEmail = 'adam.bardetta@themulia.com';

    // set cc email
    ccEmail = 'sonny.koentjoro@themulia.com';
  } else {
    // sender
    sender = 'diaz.patria@hotelmulia.com';

    // set receipient email
    recipientEmail = 'martino.tanor@hotelmulia.com';
  }

  try {
    // Create the email options
    const mailOptions = {
      from: `Financial Controller <${senderEmail}>`,
      to: `General Manager <${recipientEmail}>`,
      ...(ccEmail ? { cc: [`Hotel Manager <${ccEmail}>`] } : {}),
      bcc: bccEmails,
      subject: subject,
      html: emailHtml,
      replyTo: `Financial Controller <${sender}>`, // Menambahkan Reply-To header agar server tahu alamat pengirim sebenarnya
    };

    // Send the email
    const info = await transporter.sendMail(mailOptions);
    console.log('Email sent successfully.');
    console.log('Info object:', info);

    // Append the sent email to the "Sent" folder using IMAP
    const imap = new Imap({
      user: senderEmail,
      password: senderPassword,
      host: imapServer,
      port: imapPort,
      tls: true,
    });

    imap.once('ready', () => {
      imap.openBox('Sent', true, (err) => {
        if (err) {
          console.error('Error opening "Sent" folder:', err);
          imap.end();
          return;
        }

        // Create the email message as MIMEText
        const emailMessage = `From: ${senderEmail}\r\nTo: ${recipientEmail}\r\nSubject: ${subject}\r\n\r\n${body}`;

        // Append the sent email to the "Sent" folder
        imap.append(emailMessage, { mailbox: 'Sent' }, (appendErr) => {
          if (appendErr) {
            console.error('Error appending email to "Sent" folder:', appendErr);
          } else {
            console.log('Email appended to "Sent" folder.');
          }
          imap.end();
        });
      });
    });

    imap.once('error', (imapErr) => {
      console.error('IMAP Error:', imapErr);
    });

    imap.connect();
  } catch (error) {
    console.error('Error sending email:', error);
  }
}

const SendVoucherFromResidentToMember = async (memberData) => {

  console.log(memberData, ">>>>>>>>>>> ISI member data sebelum send voucher >>>>>>>>>>>>")

  // Define the data to be used in the email template
  const subject = 'Muliaprivileges Reward Claim Voucher Form for ' + memberData.full_name;
  const voucherFiles = [];

  // ==================================================
  // set date_expired in 6 month  min 1 day from today
  // ==================================================
  const today = new Date();
  const sixMonthsFromToday = new Date(today);
  sixMonthsFromToday.setMonth(sixMonthsFromToday.getMonth() + 6);
  // Kurangi 1 hari
  sixMonthsFromToday.setDate(sixMonthsFromToday.getDate() - 1);
  // ================================ end ===============================

  // Format the date
  const formatDate = (date) => {
    return new Intl.DateTimeFormat('en-US', { month: 'short', day: 'numeric', year: 'numeric' }).format(date);
  };

  const formattedIssueDate = formatDate(today);
  const formattedExpiredDate = formatDate(sixMonthsFromToday);

  const generateAndSaveVoucher = async (item) => {

    let prefix = null;
    let voucherTemplate = null;

    switch (item.redem_type_id) {
      case 1: // Room Redemption
        voucherTemplate = memberData.location === 'BALI' ? "voucher_room_reedemption.html" : "voucher_room_reedemption_hms.html"
        prefix = "RM"
        break;
      case 2: // Food & Beverage
        voucherTemplate = memberData.location === 'BALI' ? "voucher_food_beverage.html" : "voucher_food_beverage_hms.html"
        prefix = "FB"
        break;
      case 3: // SPA
        voucherTemplate = memberData.location === 'BALI' ? "voucher_spa.html" : "voucher_spa_hms.html"
        prefix = "SP"
        break;
      case 4: // Airport Transfer
        voucherTemplate = memberData.location === 'BALI' ? "voucher_air_transfer.html" : "voucher_air_transfer_hms.html"
        prefix = "AT"
        break;
      default:
      // code block
    }

    const voucherNo = generateVoucher(prefix);

    const response = await fetch(process.env.API_HOST + `/generate-qrcode?voucher_no=${voucherNo}`);
    if (!response.ok) {
      throw new Error('Network response was not ok');
    }

    const dataqr = await response.json();
    const fileUrl = `${process.env.API_HOST}/assets/qr-code/${dataqr.fileName}`;

    const data = {
      voucher_no: voucherNo,
      date_expired: formattedExpiredDate,
      qrcode_link: fileUrl,
      member_id: memberData.member_id,
      member_name: memberData.full_name,
      member_email: memberData.email,
      reedem_id: item.redem_id,
      reedem_name: item.redem_name,
      redem_type_id: item.redem_type_id,
      reedem_type: item.redem_type,
      location: memberData.location,
      redemption_date: formattedIssueDate,
      previous_balance: memberData.previous_balance,
      remaining_points: memberData.remaining_points,
      required_points: item.required_points,
      qty_points: item.qty_points,
      number_of_points: memberData.claimed_point, // total_reedem_points
      surcharge_type_id: memberData.surcharge_type_id,
      surcharge_type_name: memberData.surcharge_type_name,
      surcharge_points: memberData.surcharge_points,
      total_climed_points: memberData.total_climed_points,
      beneficiary: item.beneficiary,
      comments: item.comments,
      reedem_point_id: memberData.id,
      is_loyalty: memberData.is_loyalty,
      is_revenue: memberData.is_revenue,
      is_financial: memberData.is_financial,
      is_resident: memberData.is_resident,
      is_manual: memberData.is_manual,
      approve_status_id: memberData.approve_status_id,
      token: memberData.token,
      username: memberData.username
    };

    const voucher = await MemberVoucher.findOne({
      attributes: ['id', 'member_id', 'voucher_no', 'is_used', 'createdAt', 'createdBy', 'updatedAt', 'updatedBy'],
      where: {
        voucher_no: voucherNo
      }
    });

    if (!voucher) {
      try {
        await MemberVoucher.create({
          reedem_id: item.redem_id,
          reedem_name: item.redem_name,
          member_id: memberData.member_id,
          reedem_point_id: memberData.id,
          beneficiary: item.beneficiary,
          comments: item.comments,
          voucher_no: voucherNo,
          date_expired: sixMonthsFromToday,
          is_used: 0,
          qrcode_url: dataqr.fileName,
          createdAt: Sequelize.fn('NOW'),
          createdBy: memberData.username,
          updatedAt: Sequelize.fn('NOW'),
          updatedBy: memberData.username,
        });
        console.log('Add Member Voucher is successfully');
      } catch (error) {
        console.log(error);
        console.log(`Duplicate voucher prevented for: ${voucherNo}`);
      }
    }


    const attachmentTemplateVoucher = fs.readFileSync(`assets/voucher/${voucherTemplate}`, "utf8");
    const attachmentTemplate = handlebars.compile(attachmentTemplateVoucher);
    const attachmentContent = attachmentTemplate(data);

    const newVoucherFilename = `voucher_${voucherNo}.png`;
    const newVoucherFilePath = `assets/voucher/${newVoucherFilename}`;

    try {
      const puppeteer = require('puppeteer-core');
      const chromium = require('@sparticuz/chromium'); //used for linux


      const browser = await puppeteer.launch({
        // used for windows
        // executablePath: 'C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe',

        // used for linux
        executablePath: await chromium.executablePath(),
        headless: false,
        args: [
          '--no-sandbox',
          '--disable-setuid-sandbox',
          '--disable-dev-shm-usage',
          '--disable-gpu',
        ],
      });

      const page = await browser.newPage();
      await page.setViewport({
        width: 1200,
        height: 800,
        deviceScaleFactor: 1,
      });

      await page.setContent(attachmentContent, { waitUntil: 'networkidle0' });
      await page.screenshot({ path: newVoucherFilePath, fullPage: true });
      await browser.close();
      console.log('PNG voucher created:', newVoucherFilePath);
      voucherFiles.push({ filename: newVoucherFilename, path: newVoucherFilePath, contentType: 'image/png' });
    } catch (error) {
      console.error("Error creating PNG:", error);
    }
  };


  // Only generate vouchers if is_manual is not 1
  if (memberData.is_manual === 0 || memberData.is_manual === "0") {
    for (const item of memberData.items) {
      const totalQty = Number(item.qty_points); // Pastikan qty_points di-cast ke angka
      for (let i = 0; i < totalQty; i++) {
        // Update generateAndSaveVoucher to accept the current item
        await generateAndSaveVoucher(item);
      }
    }
  }

  // Load the email template
  const emailTemplateSource = memberData.is_manual === 1 ? fs.readFileSync("assets/email/EmailWithoutVoucherToMember.html", "utf8") : fs.readFileSync("assets/email/EmailVoucherToMember.html", "utf8");

  // Compile the template using Handlebars.js
  const emailTemplate = handlebars.compile(emailTemplateSource);

  // Render the email template with the data
  const emailHtml = emailTemplate({
    member_id: memberData.member_id,
    member_name: memberData.full_name,
    member_email: memberData.email,
    location: memberData.location,
    redemption_date: memberData.redemption_date,
    previous_balance: memberData.previous_balance,
    remaining_points: memberData.remaining_points,
    number_of_points: memberData.claimed_point,
    total_climed_points: memberData.total_climed_points,
    reedem_point_id: memberData.id,
    is_loyalty: memberData.is_loyalty,
    is_revenue: memberData.is_revenue,
    is_financial: memberData.is_financial,
    is_resident: memberData.is_resident,
    approve_status_id: memberData.approve_status_id,
    token: memberData.token,
    username: memberData.username
  });

  let sender = null;
  let ccEmail = null;

  if (memberData.location === 'BALI') {
    // sender
    sender = 'adam.bardetta@themulia.com';
    // set cc email
    ccEmail = 'muliaprivilege@themulia.com';
  } else {
    // sender
    sender = 'martino.tanor@hotelmulia.com ';
    // set cc email
    ccEmail = 'muliaprivilege@hotelmuliasenayan.com';
  }

  // set receipient email
  const recipientEmail = memberData.member_email;

  try {
    // Create the email options
    const mailOptions = {
      from: `Support Muliaprivileges <${senderEmail}>`,
      to: `${memberData.member_name} <${recipientEmail}>`,
      ...(ccEmail ? { cc: [`Muliaprivileges <${ccEmail}>`] } : {}),
      bcc: bccEmails,
      subject: subject,
      html: emailHtml,
    };

    mailOptions["attachments"] = voucherFiles;

    // Send the email
    const info = await transporter.sendMail(mailOptions);
    console.log('Email sent successfully.');
    console.log('Info object:', info);


    // Append the sent email to the "Sent" folder using IMAP
    const imap = new Imap({
      user: senderEmail,
      password: senderPassword,
      host: imapServer,
      port: imapPort,
      tls: true,
    });

    imap.once('ready', () => {
      imap.openBox('Sent', true, (err) => {
        if (err) {
          console.error('Error opening "Sent" folder:', err);
          imap.end();
          return;
        }

        // Create the email message as MIMEText
        const emailMessage = `From: ${senderEmail}\r\nTo: ${recipientEmail}\r\nSubject: ${subject}\r\n\r\n${body}`;

        // Append the sent email to the "Sent" folder
        imap.append(emailMessage, { mailbox: 'Sent' }, (appendErr) => {
          if (appendErr) {
            console.error('Error appending email to "Sent" folder:', appendErr);
          } else {
            console.log('Email appended to "Sent" folder.');
          }
          imap.end();
        });
      });
    });

    imap.once('error', (imapErr) => {
      console.error('IMAP Error:', imapErr);
    });

    imap.connect();
  } catch (error) {
    console.error('Error sending email:', error);
  }
}

const SendVoucherSurchargeFromResidentToMember = async (memberData) => {

  console.log(memberData, ">>>>>>>>>>> ISI member data sebelum send voucher >>>>>>>>>>>>")

  // Define the data to be used in the email template
  const subject = 'Muliaprivileges Reward Claim Voucher Form for ' + memberData.full_name;
  const voucherFiles = [];

  // ==================================================
  // set date_expired in 6 month  min 1 day from today
  // ==================================================
  const today = new Date();
  const sixMonthsFromToday = new Date(today);
  sixMonthsFromToday.setMonth(sixMonthsFromToday.getMonth() + 6);
  // Kurangi 1 hari
  sixMonthsFromToday.setDate(sixMonthsFromToday.getDate() - 1);
  // ================================ end ===============================

  // Format the date
  const formatDate = (date) => {
    return new Intl.DateTimeFormat('en-US', { month: 'short', day: 'numeric', year: 'numeric' }).format(date);
  };

  const formattedIssueDate = formatDate(today);
  const formattedExpiredDate = formatDate(sixMonthsFromToday);

  const generateAndSaveVoucher = async (item) => {

    let prefix = null;
    let voucherTemplate = null;

    switch (item.redem_type_id) {
      case 1: // Room Redemption
        voucherTemplate = memberData.location === 'BALI' ? "voucher_room_reedemption.html" : "voucher_room_reedemption_hms.html"
        prefix = "RM"
        break;
      case 2: // Food & Beverage
        voucherTemplate = memberData.location === 'BALI' ? "voucher_food_beverage.html" : "voucher_food_beverage_hms.html"
        prefix = "FB"
        break;
      case 3: // SPA
        voucherTemplate = memberData.location === 'BALI' ? "voucher_spa.html" : "voucher_spa_hms.html"
        prefix = "SP"
        break;
      case 4: // Airport Transfer
        voucherTemplate = memberData.location === 'BALI' ? "voucher_air_transfer.html" : "voucher_air_transfer_hms.html"
        prefix = "AT"
        break;
      default:
      // code block
    }

    const voucherNo = generateVoucher(prefix);

    const response = await fetch(process.env.API_HOST + `/generate-qrcode?voucher_no=${voucherNo}`);
    if (!response.ok) {
      throw new Error('Network response was not ok');
    }

    const dataqr = await response.json();
    const fileUrl = `${process.env.API_HOST}/assets/qr-code/${dataqr.fileName}`;

    const data = {
      voucher_no: voucherNo,
      date_expired: formattedExpiredDate,
      qrcode_link: fileUrl,
      member_id: memberData.member_id,
      member_name: memberData.full_name,
      member_email: memberData.email,
      reedem_id: item.redem_id,
      reedem_name: item.redem_name,
      redem_type_id: item.redem_type_id,
      reedem_type: item.reedem_type,
      location: memberData.location,
      redemption_date: formattedIssueDate,
      previous_balance: memberData.previous_balance,
      remaining_points: memberData.remaining_points,
      required_points: item.required_points,
      qty_points: item.qty_points,
      number_of_points: memberData.claimed_point, // total_reedem_points
      surcharge_type_id: memberData.surcharge_type_id,
      surcharge_type_name: memberData.surcharge_type_name,
      surcharge_points: memberData.surcharge_points,
      total_climed_points: memberData.total_climed_points,
      beneficiary: item.beneficiary,
      comments: item.comments,
      reedem_point_id: memberData.id,
      is_loyalty: memberData.is_loyalty,
      is_revenue: memberData.is_revenue,
      is_financial: memberData.is_financial,
      is_resident: memberData.is_resident,
      is_manual: memberData.is_manual,
      approve_status_id: memberData.approve_status_id,
      token: memberData.token,
      username: memberData.username
    };

    const voucher = await MemberVoucher.findAll({
      attributes: ['id', 'member_id', 'voucher_no', 'is_used', 'createdAt', 'createdBy', 'updatedAt', 'updatedBy'],
      where: {
        voucher_no: voucherNo
      }
    });

    if (!voucher[0]) {
      try {
        await MemberVoucher.create({
          reedem_id: item.redem_id,
          reedem_name: item.redem_name,
          member_id: memberData.member_id,
          reedem_point_id: memberData.id,
          beneficiary: item.beneficiary,
          comments: item.comments,
          voucher_no: voucherNo,
          date_expired: sixMonthsFromToday,
          is_used: 0,
          qrcode_url: dataqr.fileName,
          createdAt: Sequelize.fn('NOW'),
          createdBy: memberData.username,
          updatedAt: Sequelize.fn('NOW'),
          updatedBy: memberData.username,
        });
        console.log('Add Member Voucher is successfully');
      } catch (error) {
        console.log(error);
      }
    }

    const attachmentTemplateVoucher = fs.readFileSync(`assets/voucher/${voucherTemplate}`, "utf8");
    const attachmentTemplate = handlebars.compile(attachmentTemplateVoucher);
    const attachmentContent = attachmentTemplate(data);

    const newVoucherFilename = `voucher_${voucherNo}.png`;
    const newVoucherFilePath = `assets/voucher/${newVoucherFilename}`;

    try {
      const puppeteer = require('puppeteer-core');
      const chromium = require('@sparticuz/chromium'); //used for linux


      const browser = await puppeteer.launch({
        // used for windows
        // executablePath: 'C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe',

        // used for linux
        executablePath: await chromium.executablePath(),
        headless: false,
        args: [
          '--no-sandbox',
          '--disable-setuid-sandbox',
          '--disable-dev-shm-usage',
          '--disable-gpu',
        ],
      });

      const page = await browser.newPage();
      await page.setViewport({
        width: 1200,
        height: 800,
        deviceScaleFactor: 1,
      });

      await page.setContent(attachmentContent, { waitUntil: 'networkidle0' });
      await page.screenshot({ path: newVoucherFilePath, fullPage: true });
      await browser.close();
      console.log('PNG voucher created:', newVoucherFilePath);
      voucherFiles.push({ filename: newVoucherFilename, path: newVoucherFilePath, contentType: 'image/png' });
    } catch (error) {
      console.error("Error creating PNG:", error);
    }
  };


  // Only generate vouchers if is_manual is not 1
  if (memberData.is_manual === 0 || memberData.is_manual === "0") {
    for (const item of memberData.items) {
      const totalQty = Number(item.qty_points); // Pastikan qty_points di-cast ke angka
      for (let i = 0; i < totalQty; i++) {
        // Update generateAndSaveVoucher to accept the current item
        await generateAndSaveVoucher(item);
      }
    }
  }

  // Load the email template
  const emailTemplateSource = memberData.is_manual === 1 ? fs.readFileSync("assets/email/EmailWithoutVoucherToMember.html", "utf8") : fs.readFileSync("assets/email/EmailVoucherToMember.html", "utf8");

  // Compile the template using Handlebars.js
  const emailTemplate = handlebars.compile(emailTemplateSource);

  // Render the email template with the data
  const emailHtml = emailTemplate({
    member_id: memberData.member_id,
    member_name: memberData.full_name,
    member_email: memberData.email,
    reedem_id: item.redem_id,
    reedem_name: item.redem_name,
    redem_type_id: item.redem_type_id,
    reedem_type: item.redem_type,
    location: memberData.location,
    redemption_date: memberData.redemption_date,
    previous_balance: memberData.previous_balance,
    remaining_points: memberData.remaining_points,
    required_points: item.required_points,
    qty_points: item.qty_points,
    number_of_points: memberData.claimed_point,
    total_climed_points: memberData.total_climed_points,
    beneficiary: item.beneficiary,
    comments: item.comments,
    reedem_point_id: memberData.id,
    is_loyalty: memberData.is_loyalty,
    is_revenue: memberData.is_revenue,
    is_financial: memberData.is_financial,
    is_resident: memberData.is_resident,
    approve_status_id: memberData.approve_status_id,
    token: memberData.token,
    username: memberData.username
  });

  let sender = null;
  let ccEmail = null;

  if (memberData.location === 'BALI') {
    // sender
    sender = 'adam.bardetta@themulia.com';
    // set cc email
    ccEmail = 'muliaprivilege@themulia.com';
  } else {
    // sender
    sender = 'martino.tanor@hotelmulia.com ';
    // set cc email
    ccEmail = 'muliaprivilege@hotelmuliasenayan.com';
  }

  // set receipient email
  const recipientEmail = memberData.member_email;

  try {
    // Create the email options
    const mailOptions = {
      from: `Support Muliaprivileges <${senderEmail}>`,
      to: `${memberData.member_name} <${recipientEmail}>`,
      ...(ccEmail ? { cc: [`Muliaprivileges <${ccEmail}>`] } : {}),
      bcc: bccEmails,
      subject: subject,
      html: emailHtml,
    };

    mailOptions["attachments"] = voucherFiles;

    // Send the email
    const info = await transporter.sendMail(mailOptions);
    console.log('Email sent successfully.');
    console.log('Info object:', info);


    // Append the sent email to the "Sent" folder using IMAP
    const imap = new Imap({
      user: senderEmail,
      password: senderPassword,
      host: imapServer,
      port: imapPort,
      tls: true,
    });

    imap.once('ready', () => {
      imap.openBox('Sent', true, (err) => {
        if (err) {
          console.error('Error opening "Sent" folder:', err);
          imap.end();
          return;
        }

        // Create the email message as MIMEText
        const emailMessage = `From: ${senderEmail}\r\nTo: ${recipientEmail}\r\nSubject: ${subject}\r\n\r\n${body}`;

        // Append the sent email to the "Sent" folder
        imap.append(emailMessage, { mailbox: 'Sent' }, (appendErr) => {
          if (appendErr) {
            console.error('Error appending email to "Sent" folder:', appendErr);
          } else {
            console.log('Email appended to "Sent" folder.');
          }
          imap.end();
        });
      });
    });

    imap.once('error', (imapErr) => {
      console.error('IMAP Error:', imapErr);
    });

    imap.connect();
  } catch (error) {
    console.error('Error sending email:', error);
  }
}


const SendMissingRequestEmail = async (memberData) => {
  // Define the data to be used in the email template
  const data = {
    member_id: memberData.id,
    member_name: memberData.name,
    member_email: memberData.email,
    confirmation_no: memberData.confirmation_no,
    hotel: memberData.hotel,
    checkin: memberData.checkin,
    checkout: memberData.checkout,
    attach_file: memberData.attach_file,
    attach_url: memberData.attach_url,
    comments: memberData.comments,
    username: memberData.username
  };

  const subject = 'Missing ponts notification for ' + data.member_name;

  // Load the email template
  const emailTemplateSource = fs.readFileSync("assets/email/MissingRequest.html", "utf8");

  // Compile the template using Handlebars.js
  const emailTemplate = handlebars.compile(emailTemplateSource);

  // Render the email template with the data
  const emailHtml = emailTemplate(data);

  // sender
  const sender = data.member_email;

  // set receipient email
  const recipientEmail = senderEmail;

  const attachmentContent = fs.readFileSync(data.attach_file);

  try {
    // Create the email options
    const mailOptions = {
      from: `${data.member_name} <${senderEmail}>`,
      to: `Team Support Muliaprivileges.com <${recipientEmail}>`,
      subject: subject,
      html: emailHtml,
      replyTo: `${data.member_name} <${sender}>`,
      attachments: [],
    };

    // Check if attachmentContent is available
    if (attachmentContent) {
      mailOptions.attachments.push({
        filename: data.attach_file, // Ganti dengan nama file attachment
        content: attachmentContent,
        encoding: 'base64', // Encoding attachment (base64 umumnya digunakan)
      });
    }

    // Send the email
    const info = await transporter.sendMail(mailOptions);
    console.log('Email sent successfully.');
    console.log('Info object:', info);

    // Append the sent email to the "Sent" folder using IMAP
    const imap = new Imap({
      user: senderEmail,
      password: senderPassword,
      host: imapServer,
      port: imapPort,
      tls: true,
    });

    imap.once('ready', () => {
      imap.openBox('Sent', true, (err) => {
        if (err) {
          console.error('Error opening "Sent" folder:', err);
          imap.end();
          return;
        }

        // Create the email message as MIMEText
        const emailMessage = `From: ${senderEmail}\r\nTo: ${recipientEmail}\r\nSubject: ${subject}\r\n\r\n${body}`;

        // Append the sent email to the "Sent" folder
        imap.append(emailMessage, { mailbox: 'Sent' }, (appendErr) => {
          if (appendErr) {
            console.error('Error appending email to "Sent" folder:', appendErr);
          } else {
            console.log('Email appended to "Sent" folder.');
          }
          imap.end();
        });
      });
    });

    imap.once('error', (imapErr) => {
      console.error('IMAP Error:', imapErr);
    });

    imap.connect();
  } catch (error) {
    console.error('Error sending email:', error);
  }
}

// send enroll notifications
const SendEnrollNotifEmail = async (memberData) => {

  let memberLevel = null;

  switch (memberData.member_level) {
    case 'MPS':
      memberLevel = "Silver"
      break;
    case 'MPG':
      memberLevel = "Gold"
      break;
    case 'MPD':
      memberLevel = "Diamond"
      break;
    default:
    // code block
  }

  // Define the data to be used in the email template
  const data = {
    fullName: memberData.first + ' ' + memberData.last,
    email: memberData.primary_email,
    memberId: memberData.member_id,
    memberLevel: memberLevel,
    memberPoints: memberData.available_points,
    cardLink: "https://themulia.net/mp-card/?pl=849046h49a4cl46o47346h46t46c46x47h4dh46b00&g=" + memberData.first + "%20" + memberData.last + "&id=" + memberData.member_id + "&ti=" + memberLevel
  };

  const subject = 'Congratulations, ' + memberData.first + '! You Are Now A Mulia Privilege Member!';

  // Load the email template
  const emailTemplateSource = fs.readFileSync("assets/email/EnrollNotificationToMember.html", "utf8");

  // Compile the template using Handlebars.js
  const emailTemplate = handlebars.compile(emailTemplateSource);

  // Render the email template with the data
  const emailHtml = emailTemplate(data);

  // set receipient email
  const recipientEmail = data.email;

  try {
    // Create the email options
    const mailOptions = {
      from: `Team Support Muliaprivileges.com <${senderEmail}>`,
      to: `${data.fullName} <${recipientEmail}>`,
      subject: subject,
      html: emailHtml
    };

    // Send the email
    const info = await transporter.sendMail(mailOptions);
    console.log('Email sent successfully.');
    console.log('Info object:', info);

    // Append the sent email to the "Sent" folder using IMAP
    const imap = new Imap({
      user: senderEmail,
      password: senderPassword,
      host: imapServer,
      port: imapPort,
      tls: true,
    });

    imap.once('ready', () => {
      imap.openBox('Sent', true, (err) => {
        if (err) {
          console.error('Error opening "Sent" folder:', err);
          imap.end();
          return;
        }

        // Create the email message as MIMEText
        const emailMessage = `From: ${senderEmail}\r\nTo: ${recipientEmail}\r\nSubject: ${subject}\r\n\r\n${body}`;

        // Append the sent email to the "Sent" folder
        imap.append(emailMessage, { mailbox: 'Sent' }, (appendErr) => {
          if (appendErr) {
            console.error('Error appending email to "Sent" folder:', appendErr);
          } else {
            console.log('Email appended to "Sent" folder.');
          }
          imap.end();
        });
      });
    });

    imap.once('error', (imapErr) => {
      console.error('IMAP Error:', imapErr);
    });

    imap.connect();
  } catch (error) {
    console.error('Error sending email:', error);
  }
}


class EmailMultiController {

  static async activeMember(req, res) {
    const { email, username } = req.body;

    // using UTC time
    const nowUTC = new Date(); // Current UTC time
    const offset = 8; // Bali (WITA) is UTC+8
    const localDateTime = convertUTCToLocal(nowUTC, offset);

    //cek apakah member sdh ada
    const member = await Member.findOne({
      attributes: ['id', 'first', 'last', 'primary_email', 'temp_password', 'city', 'country', 'member_id'],
      where: {
        primary_email: email
      }
    });

    if (!member) return res.status(400).json({ msg: "Member is not exist" });

    try {
      // cancel reedem points berdasarkan id
      await Member.update(
        {
          is_active: 1, // set reedem points as cancel
          updatedAt: localDateTime,
          updatedBy: username
        },
        {
          where: {
            member_id: member.member_id
          }
        });
      res.json({ msg: "Activation member is successfully" });
    } catch (error) {
      console.log(error);
    }

  };

  static async activationAccount(req, res) {
    const email = req.body.email;

    //cek apakah member sdh ada
    const member = await Member.findOne({
      attributes: ['id', 'first', 'last', 'primary_email', 'temp_password', 'city', 'country', 'member_id'],
      where: {
        primary_email: email
      }
    });

    if (!member) return res.status(400).json({ msg: "Member is not exist" });

    try {

      // call email sender function 
      await SendActivationEmail(member);

      res.status(200).json({ msg: 'We have sent an email regarding member activation, please check your incoming email and press the "Activate My Account" button to activate your account.' })
    } catch (error) {
      console.error(error)
      res.status(500).json({ msg: 'An error occurred' })
    }
  };



  static async resetPassword(req, res) {
    const email = req.body.email;

    //cek apakah member sdh ada
    const member = await Member.findOne({
      attributes: ['id', 'first', 'last', 'primary_email', 'temp_password', 'city', 'country'],
      where: {
        primary_email: email
      }
    });

    if (!member) return res.status(400).json({ msg: "Member is not exist" });

    try {
      // call email sender function 
      await SendResetPasswordEmail(member)
      res.status(200).json({ message: 'Reset password successfully' })
    } catch (error) {
      console.error(error)
      res.status(500).json({ message: 'An error occurred' })
    }
  };

  static async redemptionRequestMulti(req, res) {

    const {
      reedem_point_id,
      member_id,
      PreviousBalance,
      PointClaimedToday,
      RemainingBalance,
      surcharge_type_id,
      surcharge_points,
      voucher_location,
      is_manual,
      username,
      items,
      token } = req.body;


    try {

      //cek apakah member sdh ada
      const member = await Member.findOne({
        attributes: ['id', 'first', 'last', 'primary_email', 'total_points', 'reedem_points', 'available_points'],
        where: {
          member_id: member_id
        }
      });

      if (!member) return res.status(400).json({ msg: "Member is not exist" });

      //get latest record
      const latestReedemPoint = await ReedemsPoint.findOne({
        attributes: ['id', 'previous_balance', 'remaining_points', 'is_loyalty', 'is_revenue', 'is_financial', 'is_resident', 'approve_status_id', 'createdAt'],
        where: {
          id: reedem_point_id
        },
      });

      if (latestReedemPoint) {
        // ID terakhir
        const latestId = latestReedemPoint.id;

        // Lakukan sesuatu dengan ID terakhir
        console.log("ID terakhir:", latestId);
      } else {
        // Tidak ada rekaman yang ditemukan
        console.log("Tidak ada rekaman yang ditemukan");
      }

      const reedemDate = new Intl.DateTimeFormat('en-US', { month: 'short', day: '2-digit', year: 'numeric' }).format(latestReedemPoint.createdAt);
     
      let surchargeType = null;
      switch (surcharge_type_id) {
        case 0:
          surchargeType = '';
          break;
        case 1:
          surchargeType = 'High Season Surcharge';
          break;
        case 2:
          surchargeType = 'Peak Season Surcharge';
          break
        default:
          surchargeType = '';
          break;
      }

      const dataMember = {
        id: member_id,
        name: member.first + ' ' + member.last,
        email: member.primary_email,
        redemption_date: reedemDate,
        items: items,
        claimed_points: PointClaimedToday,
        previous_balance: PreviousBalance,
        remaining_points: RemainingBalance,
        reedem_point_id: latestReedemPoint.id,
        surcharge_type_id: surcharge_type_id,
        surcharge_type_name: surchargeType,
        surcharge_points: surcharge_points,
        voucher_location: voucher_location,
        is_loyalty: latestReedemPoint.is_loyalty,
        is_revenue: latestReedemPoint.is_revenue,
        is_financial: latestReedemPoint.is_financial,
        is_resident: latestReedemPoint.is_resident,
        approve_status_id: latestReedemPoint.approve_status_id,
        is_manual: is_manual,
        token: token,
        username: username
      }

      console.log(dataMember, ">>>>>>>>>>> ISI DATA MEMBER SEKARANG >>>>>>>>>>>>>>>>>")
      // call email sender function 
      await SendRedemptionRequestEmail(dataMember)

      await SendRewardClaimForm(dataMember)

      await SendRedemptionNotification(dataMember.email, dataMember.name)

      res.status(200).json({ message: 'Redemption Request is successfully' })
    } catch (error) {
      console.error(error)
      res.status(500).json({ message: 'An error occurred' })
    }
  };

  static async surchargeRequest(req, res) {

    const { redeem_point_id, member_id, reedem_id, previous_balance, required_points, qty_points, total_points, remaining_points, surcharge_type_id, surcharge_points, beneficiary, comments, is_manual,
      is_addsurcharge_financial, is_addsurcharge_loyalty, is_addsurcharge_resident, is_addsurcharge_revenue, addsurcharge_status_id, createdAt, createdBy, username } = req.body;

    try {

      //cek apakah member sdh ada
      const member = await Member.findOne({
        attributes: ['id', 'first', 'last', 'primary_email', 'total_points', 'reedem_points', 'available_points'],
        where: {
          member_id: member_id
        }
      });

      if (!member) return res.status(400).json({ msg: "Member is not exist" });

      //cek data reedem
      const reedem = await Reedem.findOne({
        attributes: ['id', 'reedem_name', 'point', 'location', 'description', 'reedem_type', 'createdAt'],
        where: {
          id: reedem_id
        }
      });


      const reedemDate = new Intl.DateTimeFormat('en-US', { month: 'short', day: '2-digit', year: 'numeric' }).format(createdAt);

      let surchargeType = null;
      switch (surcharge_type_id) {
        case 0:
          surchargeType = '';
          break;
        case 1:
          surchargeType = 'High Season Surcharge';
          break;
        case 2:
          surchargeType = 'Peak Season Surcharge';
          break
        default:
          surchargeType = '';
          break;
      }

      const dataMember = {
        id: member_id,
        name: member.first + ' ' + member.last,
        email: member.primary_email,
        reedem_id: reedem_id,
        reedem_name: reedem.reedem_name,
        reedem_type: reedem.reedem_type,
        location: reedem.location,
        redemption_date: reedemDate,
        required_points: required_points,
        qty_points: qty_points,
        total_points: total_points,
        surcharge_type_id: surcharge_type_id,
        surcharge_type_name: surchargeType,
        surcharge_points: surcharge_points,
        beneficiary: beneficiary,
        previous_balance: previous_balance,
        remaining_points: remaining_points,
        comments: comments,
        reedem_point_id: redeem_point_id,
        is_addsurcharge_loyalty: is_addsurcharge_loyalty,
        is_addsurcharge_revenue: is_addsurcharge_revenue,
        is_addsurcharge_financial: is_addsurcharge_financial,
        is_addsurcharge_resident: is_addsurcharge_resident,
        addsurcharge_status_id: addsurcharge_status_id,
        is_manual: is_manual,
        token: token
      }

      console.log(dataMember, ">>>>>>>>>>> ISI DATA MEMBER SEKARANG >>>>>>>>>>>>>>>>>")
      // call email sender function 
      // await SendRedemptionRequestEmail(dataMember)

      await SendSurchargeClaimForm(dataMember)

      // await SendRedemptionNotification(dataMember.email, dataMember.name)

      res.status(200).json({ message: 'Surcharge Request is successfully' })
    } catch (error) {
      console.error(error)
      res.status(500).json({ message: 'An error occurred' })
    }
  };


  static async voucherRedemption(req, res) {

    const { reedem_id, reedem_name, member_id, member_name, reedem_point_id, beneficiary,
      comments, voucher_no, date_expired, is_used, outlet_name, createdBy, updatedBy, createdAt, updatedAt } = req.body;
    try {

      //cek apakah member sdh ada
      const member = await Member.findOne({
        attributes: ['id', 'primary_email'],
        where: {
          member_id: member_id
        }
      });

      if (!member) return res.status(400).json({ msg: "Member is not exist" });

      // Function to format date to YYYY-MM-DD HH:mm:ss
      function formatDateTime(date) {
        const d = new Date(date);
        const year = d.getFullYear();
        const month = String(d.getMonth() + 1).padStart(2, '0');
        const day = String(d.getDate()).padStart(2, '0');
        const hours = String(d.getHours()).padStart(2, '0');
        const minutes = String(d.getMinutes()).padStart(2, '0');
        const seconds = String(d.getSeconds()).padStart(2, '0');
        return `${year}-${month}-${day} ${hours}:${minutes}:${seconds}`;
      }

      // Format updatedAt
      const formattedUpdatedAt = formatDateTime(updatedAt);

      const dataMember = {
        id: member_id,
        name: member_name,
        email: member.primary_email,
        reedem_id: reedem_id,
        reedem_name: reedem_name,
        reedem_point_id: reedem_point_id,
        beneficiary: beneficiary,
        comments: comments,
        voucher_no: voucher_no,
        date_expired: date_expired,
        is_used: is_used,
        outlet_name: outlet_name,
        createdBy: createdBy,
        updatedBy: updatedBy,
        createdAt: createdAt,
        updatedAt: formattedUpdatedAt
      }

      console.log(dataMember, ">>>>>>>>>>> ISI DATA MEMBER SEKARANG >>>>>>>>>>>>>>>>>")
      // call email sender function 
      await SendVoucherRedeemptionEmail(dataMember)

      res.status(200).json({ message: 'Voucher Redemption is successfully' })
    } catch (error) {
      console.error(error)
      res.status(500).json({ message: 'An error occurred' })
    }
  };

  static async sendEmailToRevenue(req, res) {
    const { id, member_id, items, location, full_name, email, redemption_date, total_reedem_points,
      previous_balance, remaining_points, surcharge_type_id, surcharge_type_name, surcharge_points, total_climed_points,
      is_cancel, is_loyalty, is_revenue, is_financial, is_resident, is_manual, approve_status_id, username, token } = req.body;


    try {

      const dataMember = {
        id: id,
        member_id: member_id,
        items: items,
        location: location,
        full_name: full_name,
        email: email,
        redemption_date: redemption_date,
        previous_balance: previous_balance,
        claimed_point: total_reedem_points,
        remaining_points: remaining_points,
        surcharge_type_id: surcharge_type_id,
        surcharge_type_name: surcharge_type_name,
        surcharge_points: surcharge_points,
        total_climed_points: total_climed_points,
        is_cancel: is_cancel,
        is_loyalty: is_loyalty,
        is_revenue: is_revenue,
        is_financial: is_financial,
        is_resident: is_resident,
        is_manual: is_manual,
        approve_status_id: approve_status_id,
        username: username,
        token: token
      }

      console.log(dataMember, ">>>>>>>>>>> ISI DATA MEMBER SEKARANG >>>>>>>>>>>>>>>>>")
      // call email sender function 
      await SendEmailFromLoyaltyToRevenue(dataMember)

      res.status(200).json({ message: 'Send email to Revenue is successfully' })
    } catch (error) {
      console.error(error)
      res.status(500).json({ message: 'An error occurred' })
    }
  };

  static async sendEmailSurchargeToRevenue(req, res) {
    const { id, member_id, items, location, full_name, email, redemption_date, total_reedem_points,
      previous_balance, remaining_points, surcharge_type_id, surcharge_type_name, surcharge_points, total_climed_points,
      is_cancel, is_addsurcharge_loyalty, is_addsurcharge_revenue, is_addsurcharge_financial, is_addsurcharge_resident, is_manual, addsurcharge_status_id, username, token } = req.body;

    try {

      const dataMember = {
        id: id,
        member_id: member_id,
        items: items,
        location: location,
        full_name: full_name,
        email: email,
        redemption_date: redemption_date,
        previous_balance: previous_balance,
        claimed_point: total_reedem_points,
        remaining_points: remaining_points,
        surcharge_type_id: surcharge_type_id,
        surcharge_type_name: surcharge_type_name,
        surcharge_points: surcharge_points,
        total_climed_points: total_climed_points,
        is_cancel: is_cancel,
        is_addsurcharge_loyalty: is_addsurcharge_loyalty,
        is_addsurcharge_revenue: is_addsurcharge_revenue,
        is_addsurcharge_financial: is_addsurcharge_financial,
        is_addsurcharge_resident: is_addsurcharge_resident,
        is_manual: is_manual,
        addsurcharge_status_id: addsurcharge_status_id,
        username: username,
        token: token
      }

      console.log(dataMember, ">>>>>>>>>>> ISI DATA MEMBER SEKARANG >>>>>>>>>>>>>>>>>")
      // call email sender function 
      await SendEmailSurchargeFromLoyaltyToRevenue(dataMember)

      res.status(200).json({ message: 'Send email to Revenue is successfully' })
    } catch (error) {
      console.error(error)
      res.status(500).json({ message: 'An error occurred' })
    }
  };



  static async sendEmailToFinancial(req, res) {
    const { id, member_id, items, location, full_name, email, redemption_date, total_reedem_points,
      previous_balance, remaining_points, surcharge_type_id, surcharge_type_name, surcharge_points, total_climed_points,
      is_cancel, is_loyalty, is_revenue, is_financial, is_resident, is_manual, approve_status_id, username, token } = req.body;

    try {

      const dataMember = {
        id: id,
        member_id: member_id,
        items: items,
        location: location,
        full_name: full_name,
        email: email,
        redemption_date: redemption_date,
        previous_balance: previous_balance,
        claimed_point: total_reedem_points,
        remaining_points: remaining_points,
        surcharge_type_id: surcharge_type_id,
        surcharge_type_name: surcharge_type_name,
        surcharge_points: surcharge_points,
        total_climed_points: total_climed_points,
        is_cancel: is_cancel,
        is_loyalty: is_loyalty,
        is_revenue: is_revenue,
        is_financial: is_financial,
        is_resident: is_resident,
        is_manual: is_manual,
        approve_status_id: approve_status_id,
        username: username,
        token: token
      }

      console.log(dataMember, ">>>>>>>>>>> ISI DATA MEMBER SEKARANG >>>>>>>>>>>>>>>>>")
      // call email sender function 
      await SendEmailFromRevenueToFinancial(dataMember)

      res.status(200).json({ message: 'Send email to Financial is successfully' })
    } catch (error) {
      console.error(error)
      res.status(500).json({ message: 'An error occurred' })
    }
  };

  static async sendEmailSurchargeToFinancial(req, res) {
    const { id, member_id, items, location, full_name, email, redemption_date, total_reedem_points,
      previous_balance, remaining_points, surcharge_type_id, surcharge_type_name, surcharge_points, total_climed_points,
      is_cancel, is_addsurcharge_loyalty, is_addsurcharge_revenue, is_addsurcharge_financial, is_addsurcharge_resident, is_manual, addsurcharge_status_id, username, token } = req.body;

    try {

      const dataMember = {
        id: id,
        member_id: member_id,
        items: items,
        location: location,
        full_name: full_name,
        email: email,
        redemption_date: redemption_date,
        previous_balance: previous_balance,
        claimed_point: total_reedem_points,
        remaining_points: remaining_points,
        surcharge_type_id: surcharge_type_id,
        surcharge_type_name: surcharge_type_name,
        surcharge_points: surcharge_points,
        total_climed_points: total_climed_points,
        is_cancel: is_cancel,
        is_addsurcharge_loyalty: is_addsurcharge_loyalty,
        is_addsurcharge_revenue: is_addsurcharge_revenue,
        is_addsurcharge_financial: is_addsurcharge_financial,
        is_addsurcharge_resident: is_addsurcharge_resident,
        is_manual: is_manual,
        addsurcharge_status_id: addsurcharge_status_id,
        username: username,
        token: token
      }

      console.log(dataMember, ">>>>>>>>>>> ISI DATA MEMBER SEKARANG >>>>>>>>>>>>>>>>>")
      // call email sender function 
      await SendEmailSurchargeFromRevenueToFinancial(dataMember)

      res.status(200).json({ message: 'Send email to Financial is successfully' })
    } catch (error) {
      console.error(error)
      res.status(500).json({ message: 'An error occurred' })
    }
  };


  static async sendEmailToResident(req, res) {
    const { id, member_id, items, location, full_name, email, redemption_date, total_reedem_points,
      previous_balance, remaining_points, surcharge_type_id, surcharge_type_name, surcharge_points, total_climed_points,
      is_cancel, is_loyalty, is_revenue, is_financial, is_resident, is_manual, approve_status_id, username, token } = req.body;

    try {

      const dataMember = {
        id: id,
        member_id: member_id,
        items: items,
        location: location,
        full_name: full_name,
        email: email,
        redemption_date: redemption_date,
        previous_balance: previous_balance,
        claimed_point: total_reedem_points,
        remaining_points: remaining_points,
        surcharge_type_id: surcharge_type_id,
        surcharge_type_name: surcharge_type_name,
        surcharge_points: surcharge_points,
        total_climed_points: total_climed_points,
        is_cancel: is_cancel,
        is_loyalty: is_loyalty,
        is_revenue: is_revenue,
        is_financial: is_financial,
        is_resident: is_resident,
        is_manual: is_manual,
        approve_status_id: approve_status_id,
        username: username,
        token: token
      }

      console.log(dataMember, ">>>>>>>>>>> ISI DATA MEMBER SEKARANG >>>>>>>>>>>>>>>>>")
      // call email sender function 
      await SendEmailFromFinancialToResident(dataMember)

      res.status(200).json({ message: 'Send email to Resident is successfully' })
    } catch (error) {
      console.error(error)
      res.status(500).json({ message: 'An error occurred' })
    }
  };

  static async sendEmailSurchargeToResident(req, res) {
    const { id, member_id, items, location, full_name, email, redemption_date, total_reedem_points,
      previous_balance, remaining_points, surcharge_type_id, surcharge_type_name, surcharge_points, total_climed_points,
      is_cancel, is_addsurcharge_loyalty, is_addsurcharge_revenue, is_addsurcharge_financial, is_addsurcharge_resident, is_manual, addsurcharge_status_id, username, token } = req.body;

    try {

      const dataMember = {
        id: id,
        member_id: member_id,
        items: items,
        location: location,
        full_name: full_name,
        email: email,
        redemption_date: redemption_date,
        previous_balance: previous_balance,
        claimed_point: total_reedem_points,
        remaining_points: remaining_points,
        surcharge_type_id: surcharge_type_id,
        surcharge_type_name: surcharge_type_name,
        surcharge_points: surcharge_points,
        total_climed_points: total_climed_points,
        is_cancel: is_cancel,
        is_addsurcharge_loyalty: is_addsurcharge_loyalty,
        is_addsurcharge_revenue: is_addsurcharge_revenue,
        is_addsurcharge_financial: is_addsurcharge_financial,
        is_addsurcharge_resident: is_addsurcharge_resident,
        is_manual: is_manual,
        addsurcharge_status_id: addsurcharge_status_id,
        username: username,
        token: token
      }

      console.log(dataMember, ">>>>>>>>>>> ISI DATA MEMBER SEKARANG >>>>>>>>>>>>>>>>>")
      // call email sender function 
      await SendEmailSurchargeFromFinancialToResident(dataMember)

      res.status(200).json({ message: 'Send email to Resident is successfully' })
    } catch (error) {
      console.error(error)
      res.status(500).json({ message: 'An error occurred' })
    }
  };

  static async sendVoucherSurchargeToMember(req, res) {
    const { id, member_id, items, location, full_name, email, redemption_date, total_reedem_points,
      previous_balance, remaining_points, surcharge_type_id, surcharge_type_name, surcharge_points, total_climed_points,
      is_cancel, is_addsurcharge_loyalty, is_addsurcharge_revenue, is_addsurcharge_financial, is_addsurcharge_resident, is_manual, addsurcharge_status_id, username, token } = req.body;

    try {

      const dataMember = {
        id: id,
        member_id: member_id,
        items: items,
        location: location,
        full_name: full_name,
        email: email,
        redemption_date: redemption_date,
        previous_balance: previous_balance,
        claimed_point: total_reedem_points,
        remaining_points: remaining_points,
        surcharge_type_id: surcharge_type_id,
        surcharge_type_name: surcharge_type_name,
        surcharge_points: surcharge_points,
        total_climed_points: total_climed_points,
        is_cancel: is_cancel,
        is_addsurcharge_loyalty: is_addsurcharge_loyalty,
        is_addsurcharge_revenue: is_addsurcharge_revenue,
        is_addsurcharge_financial: is_addsurcharge_financial,
        is_addsurcharge_resident: is_addsurcharge_resident,
        is_manual: is_manual,
        addsurcharge_status_id: addsurcharge_status_id,
        username: username,
        token: token
      }

      console.log(dataMember, ">>>>>>>>>>> ISI DATA MEMBER SEKARANG >>>>>>>>>>>>>>>>>")
      // call email sender function 
      await SendVoucherSurchargeFromResidentToMember(dataMember)

      res.status(200).json({ message: 'Send email to Resident is successfully' })
    } catch (error) {
      console.error(error)
      res.status(500).json({ message: 'An error occurred' })
    }
  };

  static async sendVoucherToMember(req, res) {
    const { id, member_id, items, location, full_name, email, redemption_date, total_reedem_points,
      previous_balance, remaining_points, surcharge_type_id, surcharge_type_name, surcharge_points, total_climed_points,
      is_cancel, is_loyalty, is_revenue, is_financial, is_resident, is_manual, approve_status_id, username, token } = req.body;

    try {

      const dataMember = {
        id: id,
        member_id: member_id,
        items: items,
        location: location,
        full_name: full_name,
        email: email,
        redemption_date: redemption_date,
        previous_balance: previous_balance,
        claimed_point: total_reedem_points,
        remaining_points: remaining_points,
        surcharge_type_id: surcharge_type_id,
        surcharge_type_name: surcharge_type_name,
        surcharge_points: surcharge_points,
        total_climed_points: total_climed_points,
        is_cancel: is_cancel,
        is_loyalty: is_loyalty,
        is_revenue: is_revenue,
        is_financial: is_financial,
        is_resident: is_resident,
        is_manual: is_manual,
        approve_status_id: approve_status_id,
        username: username,
        token: token
      }

      console.log(dataMember, ">>>>>>>>>>> ISI DATA MEMBER SEKARANG >>>>>>>>>>>>>>>>>")
      // call email sender function 
      await SendVoucherFromResidentToMember(dataMember)

      res.status(200).json({ message: 'Send email to Resident is successfully' })
    } catch (error) {
      console.error(error)
      res.status(500).json({ message: 'An error occurred' })
    }
  };

  static async missingRequest(req, res) {
    const { member_id, confirmation_no, guest_name, hotel, checkin_date, checkout_date, attach_file, attach_url, comments, username } = req.body;

    //cek apakah member sdh ada
    const member = await Member.findOne({
      attributes: ['id', 'first', 'last', 'primary_email'],
      where: {
        member_id: member_id
      }
    });

    if (!member) return res.status(400).json({ msg: "Member is not exist" });


    const dataMember = {
      id: member_id,
      name: guest_name,
      confirmation_no: confirmation_no,
      email: member.primary_email,
      hotel: hotel,
      checkin: checkin_date,
      checkout: checkout_date,
      attach_file: attach_file,
      attach_url: attach_url,
      comments: comments,
      username: username
    }

    try {
      // call email sender function 
      await SendMissingRequestEmail(dataMember)

      await SendMissingNotification(dataMember.email, dataMember.name)

      res.status(200).json({ message: 'Missing Request is successfully' })
    } catch (error) {
      console.error(error)
      res.status(500).json({ message: 'An error occurred' })
    }
  };

  static async enrollNotifications(req, res) {
    const { email } = req.body;

    //cek apakah member sdh ada
    const member = await Member.findOne({
      attributes: ['id', 'first', 'last', 'primary_email', 'member_id', 'member_level', 'available_points'],
      where: {
        primary_email: email
      }
    });

    if (!member) return res.status(400).json({ msg: "Member is not exist" });

    try {
      // call email sender function 
      await SendEnrollNotifEmail(member)

      res.status(200).json({ msg: 'Send email enroll notifications is successfully.' })
    } catch (error) {
      console.error(error)
      res.status(500).json({ msg: 'An error occurred' })
    }
  };


  static async testmail(req, res) {
    // Call the function to send the email and append it to the "Sent" folder
    try {
      await sendEmailAndAppend();
      res.status(200).json({ message: 'Send email is successfully.' })
    } catch (error) {
      console.error(error)
      res.status(500).json({ msg: 'An error occurred' })
    }
  }


}

module.exports = EmailMultiController
