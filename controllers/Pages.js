const { Page, PageLang, PageContent, PageContentLang, Language, PageTerm, PageFaq } = require('../db/models/index');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const Sequelize = require('sequelize');

class PageController {
    static async getPages_pages(req, res) {
        let { page, pageSize } = req.query

        //! Calculate starting index and number of records to retrieve
        const startIndex = (Number(page) - 1) * Number(pageSize);
        const endIndex = Number(page) * Number(pageSize);
        try {
            const pages = await Page.findAll({
                offset: startIndex,
                limit: Number(pageSize)
            });
            //! Retrieve total count of records for pagination info
            const totalCount = await Page.count();
            //! Calculate total number of pages
            const totalPages = Math.ceil(totalCount / Number(pageSize));
            res.status(200).json({
                contents: pages,
                totalRecords: totalCount,
                currentPage: Number(page),
                totalPages: totalPages
            });
        } catch (error) {
            console.log(error);
        }
    };

    static async getPages(req, res) {
        try {
            const page = await Page.findAll({
            });
            res.status(200).json(page);
        } catch (error) {
            console.log(error);
        }
    };


    static async getPagesLangs(req, res) {
        const langId = req.params.language;
        try {
            const page = await Page.findAll({
                attributes: ['id', 'page_name', 'page_address', 'image_name', 'image_url', 'is_active'],
                where: { is_deleted: 0 }, // hanya yang tidak didelete yang di ambil
                include: [
                    {
                        model: PageLang,
                        as: 'pagelangs',
                        where: { language_id: langId },
                        required: false, // Tetap ambil Page walaupun PageLang tidak ditemukan
                    },
                ],
            });
            res.status(200).json(page);
        } catch (error) {
            console.log(error);
        }
    };

    static async getPagesMenus(req, res) {
        try {
            const pages = await Page.findAll({
            });
            res.status(200).json(pages);
        } catch (error) {
            console.log(error);
        }
    };

    static async createPage(req, res) {
        const { page_name, page_title, page_description, short_description, page_address, image_name, image_url, is_active, username } = req.body;
        //cek apakah language sdh ada
        const page = await Page.findAll({
            where: {
                page_name: page_name
            }
        });

        if (page[0]) return res.status(400).json({ msg: "Page is alredy exist" });

        try {
            await Page.create({
                page_name: page_name,
                page_title: page_title,
                short_description: short_description,
                page_description: page_description,
                page_address: page_address,
                image_name: image_name,
                image_url: image_url,
                is_active: is_active,
                createdAt: Sequelize.fn('NOW'),
                createdBy: username,
                updatedAt: Sequelize.fn('NOW'),
                updatedBy: username
            });
            res.json({ msg: "Add new page successfully" });
        } catch (error) {
            console.log(error);
        }
    };

    static async createPageLang(req, res) {
        const langId = req.params.language;
        const { page_name, page_title, page_description, short_description, page_address, image_name, image_url, is_active, username } = req.body;

        // Cek apakah halaman sudah ada
        const page = await Page.findOne({ where: { page_name } });

        if (page) return res.status(400).json({ msg: "Page already exists" });

        try {
            // Simpan data ke tabel 'pages'
            const newPage = await Page.create({
                page_name: page_name,
                page_title: page_title,
                short_description: short_description,
                page_description: page_description,
                page_address: page_address,
                image_name: image_name,
                image_url: image_url,
                is_active: is_active,
                createdAt: Sequelize.fn('NOW'),
                createdBy: username,
                updatedAt: Sequelize.fn('NOW'),
                updatedBy: username
            });

            // Simpan data ke tabel 'page_langs'
            await PageLang.create({
                page_id: newPage.id, // Ambil ID dari Page yang baru dibuat
                language_id: langId, // Gunakan parameter langId
                page_title: page_title,
                short_description: short_description,
                page_description: page_description,
                createdAt: Sequelize.fn('NOW'),
                createdBy: username,
                updatedAt: Sequelize.fn('NOW'),
                updatedBy: username
            });

            res.json({ msg: "Add new page successfully" });
        } catch (error) {
            console.log(error);
        }
    };

    static async detailPage(req, res) {

        const pageId = req.params.id;
        try {

            const page = await Page.findOne({
                where: { id: pageId },
                include: [
                    {
                        model: PageContent,
                        as: 'pagecontent',
                        // ... options for Task model query
                    },
                ],
            });

            res.status(200).json(page);
        } catch (error) {
            console.log(error);
        }
    };

    static async detailPageLang(req, res) {

        const pageId = req.params.id;
        const langId = req.params.language;
        try {

            const page = await Page.findOne({
                attributes: ['id', 'page_name', 'page_address', 'image_name', 'image_url', 'is_active'],
                where: { id: pageId, is_deleted: 0 },
                include: [
                    {
                        model: PageLang,
                        as: 'pagelangs',
                        where: { language_id: langId },
                        required: false, // Tetap ambil Page walaupun PageLang tidak ditemukan
                    },
                ],
            });

            res.status(200).json(page);
        } catch (error) {
            console.log(error);
        }
    };

    static async detailPageSlug(req, res) {

        const slug = req.params.slug;
        console.log('Received slug:', slug);

        try {

            const page = await Page.findOne({
                where: { page_address: slug },
                include: [
                    {
                        model: PageContent,
                        as: 'pagecontent',
                        // ... options for Task model query
                    },
                ],
            });
            if (!page) {
                console.log(`No page found for slug: ${slug}`);  // Log if no page is found
                return res.status(404).json({ message: 'Page not found' });
            }

            console.log('Page found:', page);
            res.status(200).json(page);
        } catch (error) {
            console.log(error);
        }
    };

    static async updatePage(req, res) {
        const pageId = req.params.id;
        const { page_name, page_title, page_description, short_description, page_address, image_name, image_url, is_active, username } = req.body;
        //cek apakah language sdh ada
        const page = await Page.findAll({
            where: {
                id: pageId
            }
        });

        if (!page[0]) return res.status(400).json({ msg: "Page is not exist" });

        try {

            await Page.update(
                {
                    page_name: page_name,
                    page_title: page_title,
                    short_description: short_description,
                    page_description: page_description,
                    page_address: page_address,
                    image_name: image_name,
                    image_url: image_url,
                    is_active: is_active,
                    updatedAt: Sequelize.fn('NOW'),
                    updatedBy: username
                },
                {
                    where: {
                        id: pageId
                    }
                });

            res.json({ msg: "Update Page is successfully" });
        } catch (error) {
            console.log(error);
        }
    };

    static async updatePageLang(req, res) {
        const pageId = req.params.id;
        const langId = req.params.language;
        const { page_name, page_title, page_description, short_description, page_address, image_name, image_url, is_active, username } = req.body;

        try {

            // 1. Cek apakah page ada
            const page = await Page.findOne({ where: { id: pageId } });
            if (!page) return res.status(400).json({ msg: "Page does not exist" });

            // 2. Jika langId = 1, update tabel `pages`
            if (langId === "1") {
                await Page.update(
                    {
                        page_name: page_name,
                        page_title: page_title,
                        short_description: short_description,
                        page_description: page_description,
                        page_address: page_address,
                        image_name: image_name,
                        image_url: image_url,
                        is_active: is_active,
                        updatedAt: Sequelize.fn('NOW'),
                        updatedBy: username
                    },
                    { where: { id: pageId } }
                );
            } else {
                // jika langId bukan 1
                await Page.update(
                    {
                        page_name: page_name,
                        page_address: page_address,
                        image_name: image_name,
                        image_url: image_url,
                        is_active: is_active,
                        updatedAt: Sequelize.fn('NOW'),
                        updatedBy: username
                    },
                    { where: { id: pageId } }
                );
            }

            // 3. Cek apakah page_langs sudah ada berdasarkan `page_id` dan `language_id`
            const pageLang = await PageLang.findOne({
                where: { page_id: pageId, language_id: langId }
            });

            if (pageLang) {
                // Jika sudah ada, lakukan update
                await PageLang.update(
                    {
                        page_title: page_title,
                        short_description: short_description,
                        page_description: page_description,
                        updatedAt: Sequelize.fn('NOW'),
                        updatedBy: username
                    },
                    { where: { page_id: pageId, language_id: langId } }
                );
            } else {
                // Jika belum ada, lakukan insert (create)
                await PageLang.create({
                    page_id: pageId,
                    language_id: langId,
                    page_title: page_title,
                    short_description: short_description,
                    page_description: page_description,
                    createdAt: Sequelize.fn('NOW'),
                    updatedAt: Sequelize.fn('NOW'),
                    createdBy: username,
                    updatedBy: username
                });
            }


            res.json({ msg: "Update Page and PageLangs is successfully" });
        } catch (error) {
            console.log(error);
            res.status(500).json({ msg: "Internal Server Error" });
        }
    };

    static async deletePage(req, res) {
        const pageId = req.params.id;
        //cek apakah language sdh ada
        const page = await Page.findAll({
            where: {
                id: pageId
            }
        });

        if (!page[0]) return res.status(400).json({ msg: "Page is not exist" });

        try {

            await Page.destroy(
                {
                    where: {
                        id: pageId
                    }
                });

            res.json({ msg: "Delete Page is successfully" });
        } catch (error) {
            console.log(error);
        }
    };

    static async deletePageLang(req, res) {
        const pageId = req.params.id;

        try {
            // Cek apakah page ada
            const page = await Page.findOne({ where: { id: pageId } });

            if (!page) {
                return res.status(400).json({ msg: "Page does not exist" });
            }

            // Update is_deleted menjadi 1 di tabel Page
            await Page.update(
                { is_deleted: 1 },
                { where: { id: pageId } }
            );

            // Update is_deleted menjadi 1 di tabel PageLang
            await PageLang.update(
                { is_deleted: 1 },
                { where: { page_id: pageId } }
            );


            res.json({ msg: "Delete Page is successfully" });
        } catch (error) {
            console.log(error);
        }
    };
    // page content >>>>>>>>>>>>>>>>>>>>>>>>>
    static async getPageContents(req, res) {
        try {
            const contents = await PageContent.findAll({
                attributes: ['id', 'page_id', 'page_title', 'page_sub_title', 'page_link', 'page_content', 'image_name', 'image_url', 'language_id'],
                include: ["page"],
            });

            res.status(200).json(contents);
        } catch (error) {
            console.log(error);
        }
    };

    static async getPageContentsLang(req, res) {
        const langId = req.params.language;
        try {
            const contents = await PageContent.findAll({
                attributes: [
                    'id', 'page_id', 'page_link', 'image_name', 'image_url'
                ],
                where: { is_deleted: 0 },
                include: [
                    {
                        model: Page,
                        as: "page",
                        attributes: ['page_name'],
                        required: false
                    },
                    {
                        model: PageContentLang, // Menghubungkan dengan tabel PageContentLang
                        as: "pagecontentlangs",
                        attributes: ['page_content_id', 'page_title', 'page_sub_title', 'page_content', 'language_id'],
                        where: { language_id: langId }, // Filter berdasarkan language_id
                        required: false // Tetap tampilkan PageContent meskipun PageContentLang tidak ada
                    }
                ],
                order: [['id', 'ASC']] // Urutkan berdasarkan id
            });

            res.status(200).json(contents);
        } catch (error) {
            console.log(error);
        }
    };

    static async detailPageContent(req, res) {
        const contId = req.params.id;

        try {
            const pcontent = await PageContent.findOne({
                attributes: ['id', 'page_id', 'page_title', 'page_sub_title', 'page_link', 'page_content', 'image_name', 'image_url', 'language_id'],
                where: {
                    id: contId
                }
            });

            res.status(200).json(pcontent);
        } catch (error) {
            console.log(error);
        }
    };

    static async detailPageContentByPageLang(req, res) {
        const pageId = req.params.pageId;
        const langId = req.params.langId;
        try {
            const pcontent = await PageContent.findOne({
                attributes: ['id', 'page_id', 'page_title', 'page_sub_title', 'page_link', 'page_content', 'image_name', 'image_url', 'language_id'],
                where: {
                    page_id: pageId,
                    language_id: langId
                }
            });

            res.status(200).json(pcontent);
        } catch (error) {
            console.log(error);
        }
    };

    static async detailPageContentLang(req, res) {
        const contentId = req.params.id;
        const langId = req.params.language;
        try {

            const page = await PageContent.findOne({
                attributes: ['id', 'page_id', 'page_link', 'image_name', 'image_url'],
                where: { id: contentId, is_deleted: 0 },
                include: [
                    {
                        model: PageContentLang,
                        as: 'pagecontentlangs',
                        where: { language_id: langId },
                        required: false, // Tetap ambil Page walaupun PageLang tidak ditemukan
                    },
                ],
            });

            res.status(200).json(page);
        } catch (error) {
            console.log(error);
        }

    };


    static async createPageContent(req, res) {
        const pageId = req.params.pageId;
        const langId = req.params.language;
        const { page_title, page_sub_title, page_link, page_content, image_name, image_url, username } = req.body;
        //cek apakah page content sdh ada
        const pcontent = await PageContent.findAll({
            attributes: ['id', 'page_title', 'page_content'],
            where: {
                page_title: page_title
            }
        });

        if (pcontent[0]) return res.status(400).json({ msg: "Page content is alredy exist" });

        try {
            await PageContent.create({
                page_id: pageId,
                page_title: page_title,
                page_sub_title: page_sub_title,
                page_link: page_link,
                page_content: page_content,
                image_name: image_name,
                image_url: image_url,
                language_id: langId,
                createdAt: Sequelize.fn('NOW'),
                createdBy: username,
                updatedAt: Sequelize.fn('NOW'),
                updatedBy: username
            });
            res.json({ msg: "Add new page content successfully" });
        } catch (error) {
            console.log(error);
        }
    };

    static async createPageContentLang(req, res) {
        const langId = req.params.language;
        const { page_id, page_title, page_sub_title, page_link, page_content, image_name, image_url, username } = req.body;

        // Cek apakah page content sudah ada
        const page = await PageContent.findOne({ where: { page_title } });
        if (page) return res.status(400).json({ msg: "Page content already exists" });

        try {

            // Simpan data ke tabel 'pagecontents'
            const newPage = await PageContent.create({
                page_id: page_id,
                page_title: page_title,
                page_sub_title: page_sub_title,
                page_link: page_link,
                page_content: page_content,
                image_name: image_name,
                image_url: image_url,
                createdAt: Sequelize.fn('NOW'),
                createdBy: username,
                updatedAt: Sequelize.fn('NOW'),
                updatedBy: username
            });

            // Simpan data ke tabel 'page_langs'
            await PageContentLang.create({
                page_content_id: newPage.id, // Ambil ID dari Page yang baru dibuat
                page_title: page_title,
                page_sub_title: page_sub_title,
                page_content: page_content,
                language_id: langId, // Gunakan parameter langId
                createdAt: Sequelize.fn('NOW'),
                createdBy: username,
                updatedAt: Sequelize.fn('NOW'),
                updatedBy: username
            });

            res.json({ msg: "Add new page content successfully" });
        } catch (error) {
            console.log(error);
        }
    };

    static async updatePageContent(req, res) {
        const contentId = req.params.contentId;
        const langId = req.params.langId;
        const { page_title, page_sub_title, page_link, page_content, image_name, image_url, username } = req.body;
        //cek apakah page content sdh ada
        const pcontent = await PageContent.findOne({
            attributes: ['id', 'page_title', 'page_content'],
            where: {
                id: contentId
            }
        });

        if (!pcontent) return res.status(400).json({ msg: "Page content is not exist" });
        const pcontentId = pcontent.id;

        try {

            await PageContent.update(
                {
                    page_title: page_title,
                    page_sub_title: page_sub_title,
                    page_link: page_link,
                    page_content: page_content,
                    image_name: image_name,
                    image_url: image_url,
                    updatedAt: Sequelize.fn('NOW'),
                    updatedBy: username
                },
                {
                    where: {
                        id: pcontentId
                    }
                });

            res.json({ msg: "Update Page content is successfully" });
        } catch (error) {
            console.log(error);
        }
    };

    static async updatePageContentLang(req, res) {
        const contentId = req.params.id;
        const langId = req.params.language;
        const { page_id, page_title, page_sub_title, page_link, page_content, image_name, image_url, username } = req.body;

        try {

            // 1. Cek apakah page ada
            const pageContent = await PageContent.findOne({ where: { id: contentId } });
            if (!pageContent) return res.status(400).json({ msg: "Page content does not exist" });
            // 2. Jika langId = 1, update tabel `pagecontent`
            if (langId === "1") {
                await PageContent.update(
                    {
                        page_id: page_id,
                        page_title: page_title,
                        page_sub_title: page_sub_title,
                        page_link: page_link,
                        page_content: page_content,
                        image_name: image_name,
                        image_url: image_url,
                        updatedAt: Sequelize.fn('NOW'),
                        updatedBy: username
                    },
                    { where: { id: contentId } }
                );
            } else {
                // jika langId bukan 1
                await PageContent.update(
                    {
                        page_id: page_id,
                        page_link: page_link,
                        image_name: image_name,
                        image_url: image_url,
                        updatedAt: Sequelize.fn('NOW'),
                        updatedBy: username
                    },
                    { where: { id: contentId } }
                );
            }


            // 3. Cek apakah pagecontentlangs sudah ada berdasarkan `page_content_id` dan `language_id`
            const pageContentLang = await PageContentLang.findOne({
                where: { page_content_id: contentId, language_id: langId }
            });

            if (pageContentLang) {
                // Jika sudah ada, lakukan update
                await PageContentLang.update(
                    {
                        page_title: page_title,
                        page_sub_title: page_sub_title,
                        page_content: page_content,
                        updatedAt: Sequelize.fn('NOW'),
                        updatedBy: username
                    },
                    { where: { page_content_id: contentId, language_id: langId } }
                );
            } else {
                // Jika belum ada, lakukan insert (create)
                await PageContentLang.create({
                    page_content_id: contentId,
                    page_title: page_title,
                    page_sub_title: page_sub_title,
                    page_content: page_content,
                    language_id: langId,
                    createdAt: Sequelize.fn('NOW'),
                    updatedAt: Sequelize.fn('NOW'),
                    createdBy: username,
                    updatedBy: username
                });
            }

            res.json({ msg: "Update Page content is successfully" });
        } catch (error) {
            console.log(error);
        }
    };

    static async deletePageContent(req, res) {
        const contId = req.params.id;
        //cek apakah page content sdh ada
        const pcontent = await PageContent.findAll({
            attributes: ['id', 'page_title', 'page_content'],
            where: {
                id: contId
            }
        });

        if (!pcontent[0]) return res.status(400).json({ msg: "Page content is not exist" });
        const pcontentId = pcontent[0].id;
        try {

            await PageContent.destroy(
                {
                    where: {
                        id: pcontentId
                    }
                });

            res.json({ msg: "Delete Page content is successfully" });
        } catch (error) {
            console.log(error);
        }
    };

    static async deletePageContentLang(req, res) {
        const contId = req.params.id;
        try {

            // Cek apakah PageContent ada
            const pcontent = await PageContent.findOne({ where: { id: contId } });

            if (!pcontent) {
                return res.status(400).json({ msg: "Page Content does not exist" });
            }

            // Update is_deleted menjadi 1 di tabel PageContent
            await PageContent.update(
                { is_deleted: 1 },
                { where: { id: contId } }
            );

            // Update is_deleted menjadi 1 di tabel PageContentLang
            await PageContentLang.update(
                { is_deleted: 1 },
                { where: { page_content_id: contId } }
            );

            res.json({ msg: "Delete Page content is successfully" });
        } catch (error) {
            console.log(error);
        }
    };


    // page term >>>>>>>>>>>>>>>>>>>>>>>>>
    static async getPageTerm(req, res) {
        const pageId = req.params.pageId;
        const langId = req.params.langId;

        try {
            const pterm = await PageTerm.findOne({
                attributes: ['id', 'term_title', 'term_description'],
                where: {
                    page_id: pageId,
                    language_id: langId
                }
            });

            res.status(200).json(pterm);
        } catch (error) {
            console.log(error);
        }
    };

    static async createPageTerm(req, res) {
        const pageId = req.params.pageId;
        const langId = req.params.langId;
        const { term_title, term_description } = req.body;
        //cek apakah page term sdh ada
        const pterm = await PageTerm.findAll({
            attributes: ['id', 'term_title', 'term_description'],
            where: {
                page_id: pageId,
                language_id: langId
            }
        });

        if (pterm[0]) return res.status(400).json({ msg: "Page term is alredy exist" });

        try {
            await PageTerm.create({
                page_id: pageId,
                term_title: term_title,
                term_description: term_description,
                language_id: langId
            });
            res.json({ msg: "Add new page term successfully" });
        } catch (error) {
            console.log(error);
        }
    };

    static async updatePageTerm(req, res) {
        const pageId = req.params.pageId;
        const langId = req.params.langId;
        const { term_title, term_description } = req.body;
        //cek apakah page term sdh ada
        const pterm = await PageTerm.findAll({
            attributes: ['id', 'term_title', 'term_description'],
            where: {
                page_id: pageId,
                language_id: langId
            }
        });

        if (!pterm[0]) return res.status(400).json({ msg: "Page term is not exist" });
        const ptermId = pterm[0].id;
        try {

            await PageTerm.update(
                {
                    term_title: term_title,
                    term_description: term_description
                },
                {
                    where: {
                        id: ptermId
                    }
                });

            res.json({ msg: "Update Page term is successfully" });
        } catch (error) {
            console.log(error);
        }
    };

    static async deletePageTerm(req, res) {
        const pageId = req.params.pageId;
        const langId = req.params.langId;
        //cek apakah page term sdh ada
        const pterm = await PageTerm.findAll({
            attributes: ['id', 'term_title', 'term_description'],
            where: {
                page_id: pageId,
                language_id: langId
            }
        });

        if (!pterm[0]) return res.status(400).json({ msg: "Page term is not exist" });
        const ptermId = pterm[0].id;
        try {

            await PageTerm.destroy(
                {
                    where: {
                        id: ptermId
                    }
                });

            res.json({ msg: "Delete Page term is successfully" });
        } catch (error) {
            console.log(error);
        }
    };

    // page faqs >>>>>>>>>>>>>>>>>>>>>>>>>
    static async getPageFaq(req, res) {
        const pageId = req.params.pageId;
        const langId = req.params.langId;

        try {
            const pfaq = await PageFaq.findOne({
                attributes: ['id', 'faqs_title', 'faqs_description'],
                where: {
                    page_id: pageId,
                    language_id: langId
                }
            });

            res.status(200).json(pfaq);
        } catch (error) {
            console.log(error);
        }
    };

    static async createPageFaq(req, res) {
        const pageId = req.params.pageId;
        const langId = req.params.langId;
        const { faqs_title, faqs_description } = req.body;
        //cek apakah page faqs sdh ada
        const pfaqs = await PageFaq.findAll({
            attributes: ['id', 'faqs_title', 'faqs_description'],
            where: {
                page_id: pageId,
                language_id: langId
            }
        });

        if (pfaqs[0]) return res.status(400).json({ msg: "Page faqs is alredy exist" });

        try {
            await PageFaq.create({
                page_id: pageId,
                faqs_title: faqs_title,
                faqs_description: faqs_description,
                language_id: langId
            });
            res.json({ msg: "Add new page faqs successfully" });
        } catch (error) {
            console.log(error);
        }
    };

    static async updatePageFaq(req, res) {
        const pageId = req.params.pageId;
        const langId = req.params.langId;
        const { faqs_title, faqs_description } = req.body;
        //cek apakah page faqs sdh ada
        const pfaqs = await PageFaq.findAll({
            attributes: ['id', 'faqs_title', 'faqs_description'],
            where: {
                page_id: pageId,
                language_id: langId
            }
        });

        if (!pfaqs[0]) return res.status(400).json({ msg: "Page faqs is not exist" });

        const pfaqId = pfaqs[0].id;
        try {

            await PageFaq.update(
                {
                    faqs_title: faqs_title,
                    faqs_description: faqs_description
                },
                {
                    where: {
                        id: pfaqId
                    }
                });

            res.json({ msg: "Update Page faqs is successfully" });
        } catch (error) {
            console.log(error);
        }
    };

    static async deletePageFaq(req, res) {
        const pageId = req.params.pageId;
        const langId = req.params.langId;
        //cek apakah page faqs sdh ada
        const pfaq = await PageFaq.findAll({
            attributes: ['id', 'faqs_title', 'faqs_description'],
            where: {
                page_id: pageId,
                language_id: langId
            }
        });

        if (!pfaq[0]) return res.status(400).json({ msg: "Page faqs is not exist" });
        const pfaqId = pterm[0].id;
        try {

            await PageFaq.destroy(
                {
                    where: {
                        id: pfaqId
                    }
                });

            res.json({ msg: "Delete Page faqs is successfully" });
        } catch (error) {
            console.log(error);
        }
    };
}


module.exports = PageController

