const { ExtraRoomPoint } = require('../db/models/index');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const Sequelize = require('sequelize');


class ExtrapointController {
    static async getExtrapointsUsedPages(req, res) {
        let { page, pageSize } = req.query

        //! Calculate starting index and number of records to retrieve
        const startIndex = (Number(page) - 1) * Number(pageSize);
        const endIndex = Number(page) * Number(pageSize);
        try {
            const langs = await ExtraRoomPoint.findAll({
                offset: startIndex,
                limit: Number(pageSize)
            });
            //! Retrieve total count of records for pagination info
            const totalCount = await ExtraRoomPoint.count();
            //! Calculate total number of pages
            const totalPages = Math.ceil(totalCount / Number(pageSize));
            res.status(200).json({
                data: langs,
                totalRecords: totalCount,
                currentPage: Number(page),
                totalPages: totalPages
            });
        } catch (error) {
            console.log(error);
        }
    };

    static async getExtrapoints(req, res) {
        try {
            const extpoints = await ExtraRoomPoint.findAll({
            });
            res.status(200).json(extpoints);
        } catch (error) {
            console.log(error);
        }
    };

    static async createExtrapoint(req, res) {
        const { room_type_code, room_type_name, percentage_3_months, percentage_6_months, percentage_9_months, username } = req.body;
        //cek apakah language sdh ada
        const extpoint = await ExtraRoomPoint.findAll({
            where: {
                room_type_code: room_type_code
            }
        });

        if (extpoint[0]) return res.status(409).json({ msg: "Extrapoint is alredy exist" });

        try {
            await ExtraRoomPoint.create({
                room_type_code: room_type_code,
                room_type_name: room_type_name,
                percentage_3_months: percentage_3_months,
                percentage_6_months: percentage_6_months,
                percentage_9_months: percentage_9_months,
                createdAt: Sequelize.fn('NOW'),
                createdBy: username,
                updatedAt: Sequelize.fn('NOW'),
                updatedBy: username,
            });
            res.json({ status: 200, msg: "Add new Extrapoint successfully" });
        } catch (error) {
            console.log(error);
        }
    };

    static async detailExtrapoint(req, res) {

        const extpointId = req.params.id;
        try {
            const extpoint = await ExtraRoomPoint.findOne({
                where: {
                    id: extpointId
                }
            });
            res.status(200).json(extpoint);
        } catch (error) {
            console.log(error);
        }
    };

    static async updateExtrapoint(req, res) {
        const extpointId = req.params.id;
        const { percentage_3_months, percentage_6_months, percentage_9_months, username } = req.body;
        //cek apakah language sdh ada
        const extpoint = await ExtraRoomPoint.findAll({
            attributes: ['id', 'room_type_code', 'room_type_name'],
            where: {
                id: extpointId
            }
        });

        if (!extpoint[0]) return res.status(400).json({ status: 400, msg: "Extrapoint is not exist" });

        try {

            await ExtraRoomPoint.update(
                {
                    percentage_3_months: percentage_3_months,
                    percentage_6_months: percentage_6_months,
                    percentage_9_months: percentage_9_months,
                    updatedBy: username,
                    updatedAt: Sequelize.fn('NOW')
                },
                {
                    where: {
                        id: extpointId
                    }
                });

            res.json({ status: 200, msg: "Update Extrapoint is successfully" });
        } catch (error) {
            console.log(error);
        }
    };

    static async deleteExtrapoint(req, res) {
        const extpointId = req.params.id;
        //cek apakah language sdh ada
        const extpoint = await ExtraRoomPoint.findAll({
            where: {
                id: extpointId
            }
        });

        if (!extpoint[0]) return res.status(400).json({ msg: "Extrapoint is not exist" });

        try {

            await ExtraRoomPoint.destroy(
                {
                    where: {
                        id: extpointId
                    }
                });

            res.json({ msg: "Delete Extrapoint is successfully" });
        } catch (error) {
            console.log(error);
        }
    };
}


module.exports = ExtrapointController







