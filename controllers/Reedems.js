const { Reedem, ReedemLang, ReedemsPoint, Member } = require('../db/models/index');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const Sequelize = require('sequelize');
const { Op, where, col, fn, literal } = require('sequelize');


class ReedemController {
    static async getReedems(req, res) {
        try {
            // console.log("MASUK")
            const reedems = await Reedem.findAll({
                order: [
                    ['order_no', 'ASC']
                ],
            });

            res.status(200).json(reedems);
        } catch (error) {
            console.log(error);
        }
    };

    static async getReedemsLang(req, res) {
        const langId = req.params.language;
        try {
            const reedems = await Reedem.findAll({
                attributes: ['id', 'reedem_name', 'redem_type_id', 'reedem_type', 'location', 'point', 'image_name', 'image_url', 'order_no', 'is_active'],
                where: { is_deleted: 0 },
                include: [
                    {
                        model: ReedemLang,
                        as: 'reedemlangs',
                        where: { language_id: langId },
                        required: false, // Tetap ambil Page walaupun PageLang tidak ditemukan
                    },
                ],
            });
            res.status(200).json(reedems);
        } catch (error) {
            console.log(error);
        }
    };

    static async getCatalogues(req, res) {
        const { location } = req.query;
        try {
            const reedems = await Reedem.findAll({
                where: {
                    location: location
                }
            });
            res.status(200).json(reedems);
        } catch (error) {
            console.log(error);
        }
    };


    static async getRedeemptions(req, res, next) {
        const { page, per_page, filter, location } = req.query;
        const perPageInt = parseInt(per_page, 10);
        const offset = (page - 1) * per_page;

        const uppercaseLocation = location ? location.toUpperCase() : '';

        try {

            // Membuat objek where condition
            const whereCondition = {
                is_manual: 1,
            };

            if (filter) {
                whereCondition[Op.or] = [
                    {
                        member_id: {
                            [Op.like]: `%${filter}%`
                        }
                    },
                    {
                        beneficiary: {
                            [Op.like]: `%${filter}%`
                        }
                    },
                    {
                        comments: {
                            [Op.like]: `%${filter}%`
                        }
                    }
                ];
            }

            const dataRedeems = await ReedemsPoint.findAndCountAll({
                attributes: [
                    'id', 'member_id',
                    'reedem_id',
                    'previous_balance', 'required_points', 'qty_points', 'total_reedem_points',
                    'surcharge_type_id', 'surcharge_points', 'remaining_points',
                    [Sequelize.literal(`CASE 
                        WHEN surcharge_type_id = 1 THEN 'High session surcharge' 
                        WHEN surcharge_type_id = 2 THEN 'Peak session surcharge' 
                        ELSE '' END`), 'surcharge_type_name'],
                    'beneficiary', 'comments', 'createdBy', 'createdAt', 'updatedBy', 'updatedAt',
                    [Sequelize.col('members.name_on_card'), 'name_on_card'],
                    [Sequelize.col('reedems.reedem_name'), 'reedem_name'],
                    [Sequelize.col('reedems.location'), 'location'],
                ],
                offset: offset,
                limit: perPageInt,
                where: whereCondition,
                include: [
                    {
                        model: Member,
                        attributes: ["name_on_card"],
                        // required: true,
                        as: 'members' // Sesuaikan dengan nama asosiasi antara Dining dan Outlet
                    },
                    {
                        model: Reedem,
                        // required: true,
                        as: 'reedems',
                        attributes: ["reedem_name", "location"],
                        where: { location: uppercaseLocation }

                    },
                ],
                order: [['createdAt', 'DESC']],
            });

            const data = dataRedeems.rows;
            const totalCount = dataRedeems.count;

            res.status(200).json({
                data: data,
                totalData: totalCount,
            });
        } catch (error) {
            console.log(error);
        }

    }

    static async getReedemsFrontend(req, res) {
        try {
            const reedems = await Reedem.findAll({
                where: {
                    is_active: 1
                },
                order: [
                    ['order_no', 'ASC']
                ],
            });
            res.status(200).json({ data: reedems });
        } catch (error) {
            console.log(error);
        }
    };

    static async createReedem(req, res) {
        const { point, image_name, image_url, location, order_no, reedem_name, redem_type_id, reedem_type, description, username, short_desc, is_active } = req.body;

        try {
            await Reedem.create({
                point: point,
                image_name: image_name,
                image_url: image_url,
                location: location,
                order_no: order_no,
                reedem_name: reedem_name,
                redem_type_id: redem_type_id,
                reedem_type: reedem_type,
                description: description,
                createdBy: username,
                updatedBy: username,
                short_desc: short_desc,
                is_active: is_active,
                createdAt: Sequelize.fn('NOW'),
                updatedAt: Sequelize.fn('NOW'),
            });
            res.json({ status: 200, msg: "Add new page successfully" });
        } catch (error) {
            console.log(error);
        }
    };

    static async createReedemLang(req, res) {
        const langId = req.params.language;
        const { reedem_name, redem_type_id, reedem_type, location, point, image_name, image_url, order_no, description, short_desc, is_active, username } = req.body;

        // Cek apakah halaman sudah ada
        const reedem = await Reedem.findOne({ where: { reedem_name } });

        if (reedem) return res.status(400).json({ msg: "Reedem already exists" });

        try {
            // Simpan data ke tabel 'pages'
            const newReedem = await Reedem.create({
                reedem_name: reedem_name,
                redem_type_id: redem_type_id,
                reedem_type: reedem_type,
                location: location,
                point: point,
                image_name: image_name,
                image_url: image_url,
                order_no: order_no,
                description: description,
                short_desc: short_desc,
                is_active: is_active,
                createdAt: Sequelize.fn('NOW'),
                createdBy: username,
                updatedAt: Sequelize.fn('NOW'),
                updatedBy: username
            });

            // Simpan data ke tabel 'reedemlangs'
            await ReedemLang.create({
                reedem_id: newReedem.id, // Ambil ID dari Page yang baru dibuat
                reedem_name: reedem_name,
                description: description,
                short_desc: short_desc,
                language_id: langId, // Gunakan parameter langId
                createdAt: Sequelize.fn('NOW'),
                createdBy: username,
                updatedAt: Sequelize.fn('NOW'),
                updatedBy: username
            });

            res.json({ msg: "Add new reedem successfully" });
        } catch (error) {
            console.log(error);
        }
    };

    static async detailReedem(req, res) {

        const reedemId = req.params.id;
        try {
            const reedems = await Reedem.findOne({
                where: {
                    id: reedemId
                }
            });
            res.status(200).json({ status: 200, data: reedems });
        } catch (error) {
            console.log(error);
        }
    };

    static async detailReedemLang(req, res) {

        const reedemId = req.params.id;
        const langId = req.params.language;
        try {

            const redeem = await Reedem.findOne({
                attributes: ['id', 'redem_type_id', 'reedem_type', 'location', 'point', 'image_name', 'image_url', 'order_no', 'is_active'],
                where: { id: reedemId, is_deleted: 0 },
                include: [
                    {
                        model: ReedemLang,
                        as: 'reedemlangs',
                        where: { language_id: langId },
                        required: false, // Tetap ambil Page walaupun PageLang tidak ditemukan
                    },
                ],
            });

            res.status(200).json(redeem);
        } catch (error) {
            console.log(error);
        }

    };

    static async updateReedem(req, res) {
        const reedemId = req.params.id;
        const { point, image_name, image_url, location, order_no, reedem_name, redem_type_id, reedem_type, description, short_desc, username, is_active } = req.body;
        //cek apakah Reedem sdh ada
        const reedem = await Reedem.findAll({
            where: {
                id: reedemId
            }
        });

        if (!reedem[0]) return res.status(400).json({ status: 400, msg: "Reedem is not exist" });

        try {

            await Reedem.update(
                {
                    point: point,
                    image_name: image_name,
                    image_url: image_url,
                    location: location,
                    order_no: order_no,
                    reedem_name: reedem_name,
                    redem_type_id: redem_type_id,
                    reedem_type: reedem_type,
                    description: description,
                    short_desc: short_desc,
                    is_active: is_active,
                    updatedAt: Sequelize.fn('NOW'),
                    updatedBy: username
                },
                {
                    where: {
                        id: reedemId
                    }
                });

            res.json({ status: 200, msg: "Update Reedem is successfully" });
        } catch (error) {
            console.log(error);
        }
    };

    static async updateReedemLang(req, res) {
        const reedemId = req.params.id;
        const langId = req.params.language;
        const { reedem_name, redem_type_id, reedem_type, location, point, image_name, image_url, order_no, description, short_desc, is_active, username } = req.body;

        try {
            // 1. Cek apakah reedem sudah ada
            const reedem = await Reedem.findOne({ where: { id: reedemId } });
            if (!reedem) return res.status(400).json({ msg: "Reedem does not exist" });

            // 2. Jika langId = 1, update tabel `pages`
            if (langId === "1") {
                await Reedem.update(
                    {
                        reedem_name: reedem_name,
                        redem_type_id: redem_type_id,
                        reedem_type: reedem_type,
                        location: location,
                        point: point,
                        image_name: image_name,
                        image_url: image_url,
                        order_no: order_no,
                        description: description,
                        short_desc: short_desc,
                        is_active: is_active,
                        updatedAt: Sequelize.fn('NOW'),
                        updatedBy: username
                    },
                    { where: { id: reedemId } }
                );
            } else {
                // jika langId bukan 1
                await Reedem.update(
                    {
                        redem_type_id: redem_type_id,
                        reedem_type: reedem_type,
                        location: location,
                        point: point,
                        image_name: image_name,
                        image_url: image_url,
                        order_no: order_no,
                        is_active: is_active,
                        updatedAt: Sequelize.fn('NOW'),
                        updatedBy: username
                    },
                    { where: { id: reedemId } }
                );
            }

            // 3. Cek apakah page_langs sudah ada berdasarkan `page_id` dan `language_id`
            const reedemLang = await ReedemLang.findOne({
                where: { reedem_id: reedemId, language_id: langId }
            });

            if (reedemLang) {
                // Jika sudah ada, lakukan update
                await ReedemLang.update(
                    {
                        reedem_name: reedem_name,
                        description: description,
                        short_desc: short_desc,
                        updatedAt: Sequelize.fn('NOW'),
                        updatedBy: username
                    },
                    { where: { reedem_id: reedemId, language_id: langId } }
                );
            } else {
                // Jika belum ada, lakukan insert (create)
                await ReedemLang.create({
                    reedem_id: reedemId,
                    reedem_name: reedem_name,
                    description: description,
                    short_desc: short_desc,
                    language_id: langId,
                    createdAt: Sequelize.fn('NOW'),
                    updatedAt: Sequelize.fn('NOW'),
                    createdBy: username,
                    updatedBy: username
                });
            }

            res.json({ status: 200, msg: "Update Reedem is successfully" });
        } catch (error) {
            console.log(error);
        }
    };

    static async deleteReedem(req, res) {
        const reedemId = req.params.id;
        //cek apakah Reedem sdh ada
        const reedems = await Reedem.findAll({
            where: {
                id: reedemId
            }
        });

        if (!reedems[0]) return res.status(400).json({ status: 400, msg: "Reedem is not exist" });

        try {

            await Reedem.destroy(
                {
                    where: {
                        id: reedemId
                    }
                });

            res.json({ status: 200, msg: "Delete Reedem is successfully" });
        } catch (error) {
            console.log(error);
        }
    };

    static async deleteReedemLang(req, res) {
        const reedemId = req.params.id;

        try {

            // Cek apakah page ada
            const reedem = await Reedem.findOne({ where: { id: reedemId } });

            if (!reedem) {
                return res.status(400).json({ msg: "Reedem does not exist" });
            }

            // Update is_deleted menjadi 1 di tabel Reedem
            await Reedem.update(
                { is_deleted: 1 },
                { where: { id: reedemId } }
            );

            // Update is_deleted menjadi 1 di tabel ReedemLang
            await ReedemLang.update(
                { is_deleted: 1 },
                { where: { reedem_id: reedemId } }
            );

            res.json({ status: 200, msg: "Delete Reedem is successfully" });
        } catch (error) {
            console.log(error);
        }
    };
}


module.exports = ReedemController







