const { RedemType } = require('../db/models/index');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const Sequelize = require('sequelize');


class RedemtypeController {
    static async getRedemtypesUsedPages(req, res) {
        let { page, pageSize } = req.query

        //! Calculate starting index and number of records to retrieve
        const startIndex = (Number(page) - 1) * Number(pageSize);
        const endIndex = Number(page) * Number(pageSize);
        try {
            const langs = await RedemType.findAll({
                offset: startIndex,
                limit: Number(pageSize)
            });
              //! Retrieve total count of records for pagination info
              const totalCount = await RedemType.count();
              //! Calculate total number of pages
              const totalPages = Math.ceil(totalCount / Number(pageSize));
            res.status(200).json({
                data: langs,
                totalRecords: totalCount,
                currentPage: Number(page),
                totalPages: totalPages });
        } catch (error) {
            console.log(error);
        }
    };

    static async getRedemtypes(req, res) {
        try {
            const redem = await RedemType.findAll({
            });
            res.status(200).json(redem);
        } catch (error) {
            console.log(error);
        }
    };

    static async createRedemtype(req, res) {
        const { redem_type, username } = req.body;
        //cek apakah language sdh ada
        const redem = await RedemType.findAll({
            where: {
                redem_type: redem_type
            }
        });

        if (redem[0]) return res.status(400).json({ msg: "Redem type is alredy exist" });

        try {
            await RedemType.create({
                redem_type: redem_type,
                createdAt: Sequelize.fn('NOW'),
                createdBy: username,
                updatedAt: Sequelize.fn('NOW'), 
                updatedBy: username,
            });
            res.json({ msg: "Add new Redem type successfully" });
        } catch (error) {
            console.log(error);
        }
    };

    static async detailRedemtype(req, res) {

        const redemId = req.params.id;
        try {
            const redem = await RedemType.findOne({
                where: {
                    id: redemId
                }
            });
            res.status(200).json(redem);
        } catch (error) {
            console.log(error);
        }
    };

    static async updateRedemtype (req, res) {
        const redemId = req.params.id;
        const { redem_type, username } = req.body;
        //cek apakah language sdh ada
        const redem = await RedemType.findAll({
            attributes: ['id', 'redem_type'],
            where: {
                id: redemId
            }
        });
    
        if (!redem[0]) return res.status(400).json({ msg: "Redem type is not exist" });
    
        try {
    
            await redem_type.update(
                {
                    redem_type: redem_type,
                    updatedBy: username,
                    updatedAt: Sequelize.fn('NOW')
                },
                {
                    where: {
                        id: redemId
                    }
                });
    
            res.json({ msg: "Update Redem type is successfully" });
        } catch (error) {
            console.log(error);
        }
    };

    static async deleteRedemtype(req, res) {
        const redemId = req.params.id;
        //cek apakah language sdh ada
        const redem = await RedemType.findAll({
            where: {
                id: redemId
            }
        });
    
        if (!redem[0]) return res.status(400).json({ msg: "Redem type is not exist" });
    
        try {
    
            await RedemType.destroy(
                {
                    where: {
                        id: redemId
                    }
                });
    
            res.json({ msg: "Delete redem type is successfully" });
        } catch (error) {
            console.log(error);
        }
    };
}


module.exports = RedemtypeController







