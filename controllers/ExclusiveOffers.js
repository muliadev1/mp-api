const { ExclusiveOffer, ExclusiveOfferLang } = require('../db/models/index');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const Sequelize = require('sequelize');

class ExclusiveOffersController {
    static async getExclusiveOffers(req, res) {
        try {
            // console.log("MASUK")
            const offer = await ExclusiveOffer.findAll(
                {
                    order: [
                        ['order_no', 'ASC']
                    ],
                }
            );

            res.status(200).json(offer);
        } catch (error) {
            console.log(error);
        }
    };

    static async getExclusiveOffersLang(req, res) {
        const langId = req.params.language;
        try {
            const offer = await ExclusiveOffer.findAll({
                attributes: ['id', 'category', 'hotel', 'destination', 'image_name', 'image_url', 'order_no', 'url_page', 'is_active', 'is_booking', 'is_homepage'],
                where: { is_deleted: 0 },
                include: [
                    {
                        model: ExclusiveOfferLang,
                        as: 'offerlangs',
                        where: { language_id: langId },
                        required: false, // Tetap ambil Page walaupun PageLang tidak ditemukan
                    },
                ],
                order: [
                    ['order_no', 'ASC']
                ],
            });

            res.status(200).json(offer);
        } catch (error) {
            console.log(error);
        }
    };

    static async getExclusiveOffersFrontend(req, res) {
        try {
            const offers = await ExclusiveOffer.findAll({
                order: [
                    ['destination', 'DESC'],
                    ['order_no', 'ASC']
                ],
            });
            res.status(200).json({ data: offers });
        } catch (error) {
            console.log(error);
        }
    };

    static async getExclusiveOffersByDestination(req, res) {
        const destId = req.params.destId;
        try {
            // console.log("MASUK")
            const offers = await ExclusiveOffer.findAll({
                where: {
                    destination: destId
                },
                order: [
                    ['order_no', 'ASC']
                ],
            });
            res.status(200).json({ status: 200, data: offers });
        } catch (error) {
            console.log(error);
        }
    };



    static async createExclusiveOffers(req, res) {
        const { destination, hotel, name, category, description, url_page, image_name, image_url, order_no, is_booking, is_active, is_homepage, username } = req.body;

        try {
            await ExclusiveOffer.create({
                destination: destination,
                hotel: hotel,
                image_name: image_name,
                image_url: image_url,
                name: name,
                category: category,
                description: description,
                url_page: url_page,
                order_no: order_no,
                is_booking: is_booking,
                is_active: is_active,
                is_homepage: is_homepage,
                createdAt: Sequelize.fn('NOW'),
                createdBy: username,
                updatedAt: Sequelize.fn('NOW'),
                updatedBy: username,
            });
            res.json({ status: 200, message: "Add new Exclusive Offer successfully" });
        } catch (error) {
            console.log(error);
        }
    };

    static async createExclusiveOffersLang(req, res) {
        const langId = req.params.language;
        const { name, category, destination, hotel, description, image_name, image_url, url_page, order_no, is_booking, is_active, is_homepage, username } = req.body;

        // Cek apakah halaman sudah ada
        const offer = await ExclusiveOffer.findOne({ where: { name } });
        
        if (offer) return res.status(400).json({ msg: "Offer already exists" });

        try {

            // Simpan data ke tabel 'pages'
            const newOffer = await ExclusiveOffer.create({
                name: name,
                category: category,
                destination: destination,
                hotel: hotel,
                description: description,
                image_name: image_name,
                image_url: image_url,
                url_page: url_page,
                order_no: order_no,
                is_booking: is_booking,
                is_active: is_active,
                is_homepage: is_homepage,
                createdAt: Sequelize.fn('NOW'),
                createdBy: username,
                updatedAt: Sequelize.fn('NOW'),
                updatedBy: username
            });


            // Simpan data ke tabel 'offerlangs'
            await ExclusiveOfferLang.create({
                exclusive_offer_id: newOffer.id, // Ambil ID dari Page yang baru dibuat
                name: name,
                description: description,
                language_id: langId, // Gunakan parameter langId
                createdAt: Sequelize.fn('NOW'),
                createdBy: username,
                updatedAt: Sequelize.fn('NOW'),
                updatedBy: username
            });

            res.json({ status: 200, message: "Add new Exclusive Offer successfully" });
        } catch (error) {
            console.log(error);
        }
    };

    static async detailExclusiveOffers(req, res) {
        const offerId = req.params.id;
        try {
            const offers = await ExclusiveOffer.findOne({
                where: {
                    id: offerId
                }
            });
            res.status(200).json({ status: 200, data: offers });
        } catch (error) {
            console.log(error);
        }
    };

    static async detailExclusiveOffersLang(req, res) {

        const offerId = req.params.id;
        const langId = req.params.language;
        try {

            const offer = await ExclusiveOffer.findOne({
                attributes: ['id', 'category', 'hotel', 'destination', 'image_name', 'image_url', 'order_no', 'url_page', 'is_active', 'is_booking', 'is_homepage'],
                where: { id: offerId, is_deleted: 0 },
                include: [
                    {
                        model: ExclusiveOfferLang,
                        as: 'offerlangs',
                        where: { language_id: langId },
                        required: false, // Tetap ambil Page walaupun PageLang tidak ditemukan
                    },
                ],
            });

            res.status(200).json(offer);
        } catch (error) {
            console.log(error);
        }
    };

    static async updateExclusiveOffers(req, res) {
        const offerId = req.params.id;
        const { destination, hotel, name, category, description, image_name, image_url, url_page, order_no, is_booking, is_active, is_homepage, username } = req.body;
        //cek apakah ExclusiveOffer sdh ada
        //console.log(req);
        const offer = await ExclusiveOffer.findAll({
            where: {
                id: offerId
            }
        });

        if (!offer[0]) return res.status(400).json({ msg: "offer is not exist" });

        try {

            await ExclusiveOffer.update(
                {
                    destination: destination,
                    hotel: hotel,
                    image_name: image_name,
                    image_url: image_url,
                    name: name,
                    category: category,
                    description: description,
                    url_page: url_page,
                    order_no: order_no,
                    is_booking: is_booking,
                    is_active: is_active,
                    is_homepage: is_homepage,
                    updatedAt: Sequelize.fn('NOW'),
                    updatedBy: username,
                },
                {
                    where: {
                        id: offerId
                    }
                });

            res.json({ status: 200, msg: "Update offer is successfully" });
        } catch (error) {
            console.log(error);
        }
    };


    static async updateExclusiveOffersLang(req, res) {
        const offerId = req.params.id;
        const langId = req.params.language;
        const { name, category, destination, hotel, description, image_name, image_url, url_page, order_no, is_booking, is_active, is_homepage, username } = req.body;

        try {

            // 1. Cek apakah offer ada
            const offer = await ExclusiveOffer.findOne({ where: { id: offerId } });
            if (!offer) return res.status(400).json({ msg: "Offer does not exist" });

            // 2. Jika langId = 1, update tabel `pages`
            if (langId === "1") {
                await ExclusiveOffer.update(
                    {
                        name: name,
                        category: category,
                        hotel: hotel,
                        destination: destination,
                        description: description,
                        url_page: url_page,
                        image_name: image_name,
                        image_url: image_url,
                        order_no: order_no,
                        is_booking: is_booking,
                        is_active: is_active,
                        is_homepage: is_homepage,
                        updatedAt: Sequelize.fn('NOW'),
                        updatedBy: username
                    },
                    { where: { id: offerId } }
                );
            } else {
                // jika langId bukan 1
                await ExclusiveOffer.update(
                    {
                        category: category,
                        hotel: hotel,
                        destination: destination,
                        url_page: url_page,
                        image_name: image_name,
                        image_url: image_url,
                        order_no: order_no,
                        is_booking: is_booking,
                        is_active: is_active,
                        is_homepage: is_homepage,
                        updatedAt: Sequelize.fn('NOW'),
                        updatedBy: username
                    },
                    { where: { id: offerId } }
                );
            }

            // 3. Cek apakah offerlangs sudah ada berdasarkan `exclusive_offer_id` dan `language_id`
            const offerLang = await ExclusiveOfferLang.findOne({
                where: { exclusive_offer_id: offerId, language_id: langId }
            });

            if (offerLang) {
                // Jika sudah ada, lakukan update
                await ExclusiveOfferLang.update(
                    {
                        name: name,
                        description: description,
                        updatedAt: Sequelize.fn('NOW'),
                        updatedBy: username
                    },
                    { where: { exclusive_offer_id: offerId, language_id: langId } }
                );
            } else {
                // Jika belum ada, lakukan insert (create)
                await ExclusiveOfferLang.create({
                    exclusive_offer_id: offerId,
                    name: name,
                    description: description,
                    language_id: langId,
                    createdAt: Sequelize.fn('NOW'),
                    updatedAt: Sequelize.fn('NOW'),
                    createdBy: username,
                    updatedBy: username
                });
            }

            res.json({ status: 200, msg: "Update offer is successfully" });
        } catch (error) {
            console.log(error);
        }
    };

    static async deleteExclusiveOffers(req, res) {
        const offerId = req.params.id;
        //cek apakah offer sdh ada
        const offers = await ExclusiveOffer.findAll({
            where: {
                id: offerId
            }
        });

        if (!offers[0]) return res.status(400).json({ msg: "Offer is not exist" });

        try {

            await ExclusiveOffer.destroy(
                {
                    where: {
                        id: offerId
                    }
                });

            res.json({ status: 200, msg: "Delete offer is successfully" });
        } catch (error) {
            console.log(error);
        }
    };

    static async deleteExclusiveOffersLang(req, res) {
        const offerId = req.params.id;

        try {

            // Cek apakah page ada
            const page = await ExclusiveOffer.findOne({ where: { id: offerId } });

            if (!page) {
                return res.status(400).json({ msg: "Offer does not exist" });
            }

            // Update is_deleted menjadi 1 di tabel ExclusiveOffer
            await ExclusiveOffer.update(
                { is_deleted: 1 },
                { where: { id: offerId } }
            );

            // Update is_deleted menjadi 1 di tabel ExclusiveOfferLang
            await ExclusiveOfferLang.update(
                { is_deleted: 1 },
                { where: { exclusive_offer_id: offerId } }
            );

            res.json({ status: 200, msg: "Delete offer is successfully" });
        } catch (error) {
            console.log(error);
        }
    };
}


module.exports = ExclusiveOffersController







