const { Member, PointAdjustment, sequelize } = require('../db/models/index');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const Sequelize = require('sequelize');
const { Op, where, col, fn, literal } = require('sequelize');

function convertUTCToLocal(utcDate, offset) {
    if (!(utcDate instanceof Date)) {
        throw new TypeError('The first argument must be a Date object.');
    }
    if (typeof offset !== 'number') {
        throw new TypeError('The second argument must be a number.');
    }

    // Convert the UTC time to the local time
    return new Date(utcDate.getTime() + offset * 60 * 60 * 1000);
}

class AdjustmentController {

    static async getPointAdjustment(req, res) {

        const memberId = req.params.memberId;
        try {
            const points = await PointAdjustment.findAll({
                where: {
                    member_id: memberId
                }
            });
            res.status(200).json(points);
        } catch (error) {
            console.log(error);
        }
    };

    static async createAdjustmentPoint(req, res) {
        var memberId = req.params.memberId;
        const { prevTotalPoints, totalPoints, prevReedemPoints, reedemPoints, prevAvailablePoints, availablePoints, remarks, username } = req.body

        // using UTC time
        const nowUTC = new Date(); // Current UTC time
        const offset = 8; // Bali (WITA) is UTC+8
        const localDateTime = convertUTCToLocal(nowUTC, offset);

        //cek apakah member sdh ada
        const member = await Member.findOne({
            where: {
                member_id: memberId
            }
        });

        if (!member) return res.status(400).json({ msg: "Sorry , members doesn't exist" });

        try {
            await PointAdjustment.create({
                member_id: memberId,
                prev_total_points: prevTotalPoints,
                total_points: totalPoints,
                prev_reedem_points: prevReedemPoints,
                reedem_points: reedemPoints,
                prev_available_points: prevAvailablePoints,
                available_points: availablePoints,
                remarks: remarks,
                createdAt: localDateTime,
                createdBy: username,
                updatedAt: localDateTime,
                updatedBy: username,
            });
            res.json({ msg: "Adjustment point is successfully" });
        } catch (error) {
            console.log(error);
        }
    };

    static async updateMemberPoints(req, res) {
        const memberId = req.params.memberId;
        const { totalPoints, reedemPoints, availablePoints, username } = req.body; // berdasarkan data yang diselect di enroll modal

        // using UTC time
        const nowUTC = new Date(); // Current UTC time
        const offset = 8; // Bali (WITA) is UTC+8
        const localDateTime = convertUTCToLocal(nowUTC, offset);

        //cek apakah member sdh ada
        const member = await Member.findAll({
            attributes: ['id', 'first', 'last', 'primary_email', 'primary_mobilephone', 'city', 'country', 'reedem_points', 'total_points', 'available_points'],
            where: {
                member_id: memberId
            }
        });

        if (!member[0]) return res.status(400).json({ msg: "Member is not exist" });

        try {

            await Member.update(
                {
                    total_points: totalPoints,
                    reedem_points: reedemPoints,
                    available_points: availablePoints,
                    updatedAt: localDateTime,
                    updatedBy: username
                },
                {
                    where: {
                        member_id: memberId
                    }
                });

            res.json({ msg: "Update point member is successfully" });
        } catch (error) {
            console.log(error);
        }

    };

    static async getAdjustments(req, res) {

        let { dateFrom, dateTo, username } = req.query

        const dateObjFrom = new Date(dateFrom);
        const yearFrom = dateObjFrom.getFullYear();
        const monthFrom = dateObjFrom.getMonth() + 1;
        const dayFrom = dateObjFrom.getDate();

        const dateObjTo = new Date(dateTo);
        const yearTo = dateObjTo.getFullYear();
        const monthTo = dateObjTo.getMonth() + 1;
        const dayTo = dateObjTo.getDate();

        let whereClause = {
            [Op.and]: [
                {
                    [Op.or]: [
                        {
                            [Op.and]: [
                                Sequelize.where(Sequelize.fn('YEAR', Sequelize.col('PointAdjustment.createdAt')), yearFrom),
                                Sequelize.where(Sequelize.fn('MONTH', Sequelize.col('PointAdjustment.createdAt')), monthFrom),
                                Sequelize.where(Sequelize.fn('DAY', Sequelize.col('PointAdjustment.createdAt')), {
                                    [Op.between]: [dayFrom, dayTo],
                                }),
                            ],
                        },
                        {
                            [Op.and]: [
                                Sequelize.where(Sequelize.fn('YEAR', Sequelize.col('PointAdjustment.createdAt')), yearTo),
                                Sequelize.where(Sequelize.fn('MONTH', Sequelize.col('PointAdjustment.createdAt')), monthTo),
                                Sequelize.where(Sequelize.fn('DAY', Sequelize.col('PointAdjustment.createdAt')), {
                                    [Op.between]: [dayFrom, dayTo],
                                }),
                            ],
                        },
                    ],
                },
            ],
        };

        // Menambahkan filter untuk 'createdBy' hanya jika username diberikan
        if (username) {
            whereClause[Op.and].push({
                '$PointAdjustment.createdBy$': username,
            });
        }


        try {
            const adjustment = await PointAdjustment.findAll({

                attributes: [
                    'id',
                    'member_id',
                    [Sequelize.col('members.name_on_card'), 'name_on_card'],
                    'prev_total_points',
                    'total_points',
                    'prev_reedem_points',
                    'reedem_points',
                    'prev_available_points',
                    'available_points',
                    'remarks',
                    'createdBy',
                    [Sequelize.fn('DATE_FORMAT', Sequelize.col('PointAdjustment.createdAt'), '%m/%d/%Y'), 'createdAt'],
                    'updatedBy',
                    [Sequelize.fn('DATE_FORMAT', Sequelize.col('PointAdjustment.updatedAt'), '%m/%d/%Y'), 'updatedAt']
                ],
                include: [
                    {
                        model: Member,
                        as: "members",
                        attributes: ["name_on_card"]
                    },
                ],
                where: whereClause,
                order: [
                    ['member_id', 'ASC'],
                ],
            });


            res.status(200).json(adjustment);
        } catch (error) {
            console.log(error);
        }
    };
}

module.exports = AdjustmentController