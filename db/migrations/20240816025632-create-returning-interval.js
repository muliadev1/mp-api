'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('ReturningIntervals', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      member_id: {
        type: Sequelize.STRING
      },
      name_on_card: {
        type: Sequelize.STRING
      },
      first_stay: {
        type: Sequelize.DATE
      },
      thirty_days: {
        type: Sequelize.STRING
      },
      sixty_days: {
        type: Sequelize.STRING
      },
      ninety_days: {
        type: Sequelize.STRING
      },
      one_hunred_twenty_days: {
        type: Sequelize.STRING
      },
      one_hunred_fifty_days: {
        type: Sequelize.STRING
      },
      one_hunred_eighty_days: {
        type: Sequelize.STRING
      },
      two_hunred_ten_days: {
        type: Sequelize.STRING
      },
      two_hunred_forty_days: {
        type: Sequelize.STRING
      },
      two_hunred_seventy_days: {
        type: Sequelize.STRING
      },
      three_hundred_days: {
        type: Sequelize.STRING
      },
      three_hundred_thirty_days: {
        type: Sequelize.STRING
      },
      three_hundred_sixty_days: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('ReturningIntervals');
  }
};