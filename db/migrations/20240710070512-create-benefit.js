'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('Benefits', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      qualification_silver: {
        type: Sequelize.STRING
      },
      qualification_gold: {
        type: Sequelize.STRING
      },
      qualification_diamond: {
        type: Sequelize.STRING
      },
      earning_points_silver: {
        type: Sequelize.STRING
      },
      earning_points_gold: {
        type: Sequelize.STRING
      },
      earning_points_diamond: {
        type: Sequelize.STRING
      },
      extra_privileges_silver: {
        type: Sequelize.STRING
      },
      extra_privileges_gold: {
        type: Sequelize.STRING
      },
      extra_privileges_diamond: {
        type: Sequelize.STRING
      },
      general_toc: {
        type: Sequelize.STRING
      },
      createdBy: {
        type: Sequelize.STRING
      },
      updatedBy: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('Benefits');
  }
};