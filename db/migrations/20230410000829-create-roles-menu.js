'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('RolesMenus', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      role_id: {
        type: Sequelize.INTEGER,
        references: {
          model: "Roles",
          key: "id"
        },
        onUpdate: "cascade",
        onDelete: "cascade"
      },
      module_id: {
        type: Sequelize.INTEGER,
        references: {
          model: "Modules",
          key: "id"
        },
        onUpdate: "cascade",
        onDelete: "cascade"
      },
      menu_id: {
        type: Sequelize.INTEGER,
        references: {
          model: "Menus",
          key: "id"
        },
        onUpdate: "cascade",
        onDelete: "cascade"
      },
      order_no: {
        type: Sequelize.INTEGER
      },
      is_insert: {
        type: Sequelize.INTEGER
      },
      is_update: {
        type: Sequelize.INTEGER
      },
      is_delete: {
        type: Sequelize.INTEGER
      },
      is_read: {
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('RolesMenus');
  }
};