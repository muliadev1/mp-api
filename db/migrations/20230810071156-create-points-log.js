'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('PointsLogs', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      confirmation_no: {
        type: Sequelize.INTEGER
      },
      member_id: {
        type: Sequelize.STRING
      },
      first_name: {
        type: Sequelize.STRING
      },
      last_name: {
        type: Sequelize.STRING
      },
      arrival_date: {
        type: Sequelize.DATE
      },
      departure_date: {
        type: Sequelize.DATE
      },
      room_class: {
        type: Sequelize.STRING
      },
      room_usd: {
        type: Sequelize.INTEGER
      },
      fb_usd: {
        type: Sequelize.INTEGER
      },
      room_point: {
        type: Sequelize.INTEGER
      },
      fb_point: {
        type: Sequelize.INTEGER
      },
      total_points: {
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('PointsLogs');
  }
};