'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('MemberLogs', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      member_id: {
        type: Sequelize.STRING
      },
      first_name: {
        type: Sequelize.STRING
      },
      last_name: {
        type: Sequelize.STRING
      },
      name_on_card: {
        type: Sequelize.STRING
      },
      birth_date: {
        type: Sequelize.DATE
      },
      member_level: {
        type: Sequelize.STRING
      },
      joined_date: {
        type: Sequelize.DATE
      },
      exp_date: {
        type: Sequelize.DATE
      },
      exp_level_date: {
        type: Sequelize.DATE
      },
      primary_email: {
        type: Sequelize.STRING
      },
      total_points: {
        type: Sequelize.DOUBLE
      },
      reedem_points: {
        type: Sequelize.DOUBLE
      },
      available_points: {
        type: Sequelize.DOUBLE
      },
      createdBy: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('MemberLogs');
  }
};