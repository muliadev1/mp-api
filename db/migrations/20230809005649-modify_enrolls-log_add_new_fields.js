'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    return Promise.all([
      queryInterface.addColumn(
        'EnrollslLogs', // table name
        'room_class', // new field name
        {
          type: Sequelize.STRING,
          allowNull: true,
        },
        'room_point', // new field name
        {
          type: Sequelize.INTEGER,
          allowNull: true,
        },
        'fb_point', // new field name
        {
          type: Sequelize.INTEGER,
          allowNull: true,
        },
        'total_points', // new field name
        {
          type: Sequelize.INTEGER,
          allowNull: true,
        },
        'updatedAt', // new field name
        {
          type: Sequelize.DATE,
          allowNull: true,
        },
        'updatedBy', // new field name
        {
          type: Sequelize.STRING,
          allowNull: true,
        },
      )
    ]);
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
  }
};
