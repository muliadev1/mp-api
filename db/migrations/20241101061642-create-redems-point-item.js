'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('RedemsPointItems', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      redem_point_id: {
        type: Sequelize.INTEGER
      },
      redem_id: {
        type: Sequelize.INTEGER
      },
      required_points: {
        type: Sequelize.INTEGER
      },
      qty_points: {
        type: Sequelize.INTEGER
      },
      total_redem_points: {
        type: Sequelize.INTEGER
      },
      beneficiary: {
        type: Sequelize.STRING
      },
      comments: {
        type: Sequelize.STRING
      },
      createdBy: {
        type: Sequelize.STRING
      },
      updatedBy: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('RedemsPointItems');
  }
};