'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('Menus', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      menu_code: {
        type: Sequelize.STRING
      },
      menu_name: {
        type: Sequelize.STRING
      },
      module_id: {
        type: Sequelize.INTEGER,
        references: {
          model: "Modules",
          key: "id"
        },
        onUpdate: "cascade",
        onDelete: "cascade"
      },
      order_no: {
        type: Sequelize.INTEGER
      },
      url: {
        type: Sequelize.STRING
      },
      is_trx: {
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('Menus');
  }
};