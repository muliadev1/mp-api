'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('ReedemPointDetails', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      reedem_main_id: {
        type: Sequelize.INTEGER
      },
      member_id: {
        type: Sequelize.STRING
      },
      stay_date: {
        type: Sequelize.DATE
      },
      confirmation_no: {
        type: Sequelize.STRING
      },
      reedem_points: {
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('ReedemPointDetails');
  }
};