'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('Members', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      name_on_card: {
        type: Sequelize.STRING
      },
      first: {
        allowNull: false,
        type: Sequelize.STRING
      },
      middle: {
        type: Sequelize.STRING
      },
      last: {
        type: Sequelize.STRING
      },
      title: {
        type: Sequelize.STRING
      },
      gender: {
        type: Sequelize.STRING
      },
      birth_date: {
        type: Sequelize.STRING
      },
      city: {
        type: Sequelize.STRING
      },
      countryid: {
        type: Sequelize.STRING
      },
      country: {
        type: Sequelize.STRING
      },
      member_id: {
        type: Sequelize.STRING
      },
      member_level: {
        type: Sequelize.STRING
      },
      joined_date: {
        type: Sequelize.STRING
      },
      exp_date: {
        type: Sequelize.STRING
      },
      exp_level_date: {
        type: Sequelize.STRING
      },
      total_points: {
        type: Sequelize.STRING
      },
      reedem_points: {
        type: Sequelize.STRING
      },
      available_points: {
        type: Sequelize.STRING
      },
      primary_email: {
        allowNull: false,
        type: Sequelize.STRING
      },
      other_email: {
        type: Sequelize.STRING
      },
      mailist: {
        type: Sequelize.STRING
      },
      primary_mobilephone: {
        type: Sequelize.STRING
      },
      temp_password: {
        type: Sequelize.STRING
      },
      password: {
        type: Sequelize.STRING
      },
      refresh_token: {
        type: Sequelize.STRING
      },
      sts_member: {
        type: Sequelize.STRING
      },
      notifications: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      createdBy: {
        type: Sequelize.STRING
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedBy: {
        type: Sequelize.STRING
      },
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('Members');
  }
};