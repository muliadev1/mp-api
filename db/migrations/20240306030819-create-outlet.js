'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('Outlets', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      outlet_name: {
        type: Sequelize.STRING
      },
      cuisine: {
        type: Sequelize.STRING
      },
      opening_hour: {
        type: Sequelize.STRING
      },
      breakfast: {
        type: Sequelize.STRING
      },
      lunch: {
        type: Sequelize.STRING
      },
      dinner: {
        type: Sequelize.STRING
      },
      snack: {
        type: Sequelize.STRING
      },
      sunday_brunch: {
        type: Sequelize.STRING
      },
      sunday_lunch: {
        type: Sequelize.STRING
      },
      all_day_dining: {
        type: Sequelize.STRING
      },
      afternoon_tea: {
        type: Sequelize.STRING
      },
      createdBy: {
        type: Sequelize.STRING
      },
      updatedBy: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('Outlets');
  }
};