'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('ReedemsPoints', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      member_id: {
        type: Sequelize.STRING
      },
      reedem_id: {
        type: Sequelize.INTEGER,
        references: {
          model: "Reedems",
          key: "id"
        },
        onUpdate: "cascade",
        onDelete: "cascade"
      },
      total_reedem_points: {
        type: Sequelize.INTEGER
      },
      comments: {
        type: Sequelize.STRING
      },
      is_cancel: {
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      createdBy: {
        type: Sequelize.STRING
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedBy: {
        type: Sequelize.STRING
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('ReedemsPoints');
  }
};