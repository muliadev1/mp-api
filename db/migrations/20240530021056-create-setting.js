'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('Settings', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      room_amount: {
        type: Sequelize.INTEGER
      },
      room_point: {
        type: Sequelize.INTEGER
      },
      fb_amount: {
        type: Sequelize.INTEGER
      },
      fb_point: {
        type: Sequelize.INTEGER
      },
      hotel_pay_to_loyalty: {
        type: Sequelize.DECIMAL
      },
      loyalty_pay_to_hotel: {
        type: Sequelize.DECIMAL
      },
      system_profit_percentage: {
        type: Sequelize.DECIMAL
      },
      system_profit_amount: {
        type: Sequelize.DECIMAL
      },
      complimentary_stay_nights: {
        type: Sequelize.INTEGER
      },
      dividing_factors: {
        type: Sequelize.DECIMAL
      },
      createdBy: {
        type: Sequelize.STRING
      },
      updatedBy: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('Settings');
  }
};