'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('PointAdjustments', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      member_id: {
        type: Sequelize.STRING
      },
      prev_total_points: {
        type: Sequelize.DOUBLE
      },
      total_points: {
        type: Sequelize.DOUBLE
      },
      prev_reedem_points: {
        type: Sequelize.DOUBLE
      },
      reedem_points: {
        type: Sequelize.DOUBLE
      },
      prev_available_points: {
        type: Sequelize.DOUBLE
      },
      available_points: {
        type: Sequelize.DOUBLE
      },
      remarks: {
        type: Sequelize.STRING
      },
      createdBy: {
        type: Sequelize.STRING
      },
      updatedBy: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('PointAdjustments');
  }
};