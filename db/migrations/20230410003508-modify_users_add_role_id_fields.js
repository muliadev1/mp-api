'use strict';

//** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    return Promise.all([
      queryInterface.addColumn(
        'Users', // table name
        'role_id', // new field name
        {
          type: Sequelize.INTEGER,
          allowNull: true,
        },
      ),
    ]);
  },

  async down (queryInterface, Sequelize) {
     // logic for reverting the changes
    return Promise.all([
      queryInterface.removeColumn('Users', 'role_id'),
    ]);
  }
};
