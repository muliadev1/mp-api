'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('InitialPoints', {
      id: {
        autoIncrement: true,
        type: Sequelize.INTEGER
      },
      member_id: {
        type: Sequelize.STRING,
        allowNull: false,
        primaryKey: true,
        unique: true
      },
      checkin_date: {
        type: Sequelize.STRING
      },
      checkout_date: {
        type: Sequelize.STRING
      },
      property: {
        type: Sequelize.STRING
      },
      room_usd: {
        type: Sequelize.INTEGER
      },
      fb_usd: {
        type: Sequelize.INTEGER
      },
      init_point: {
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('InitialPoints');
  }
};