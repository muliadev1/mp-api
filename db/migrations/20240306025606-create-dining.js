'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('Dinings', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      dining_date: {
        type: Sequelize.DATE
      },
      outlet_id: {
        type: Sequelize.INTEGER
      },
      member_id: {
        type: Sequelize.STRING
      },
      member_name: {
        type: Sequelize.STRING
      },
      total_bill: {
        type: Sequelize.DOUBLE
      },
      earned_point: {
        type: Sequelize.DOUBLE
      },
      check_number: {
        type: Sequelize.STRING
      },
      createdBy: {
        type: Sequelize.STRING
      },
      updatedBy: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('Dinings');
  }
};