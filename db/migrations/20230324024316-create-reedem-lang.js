'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('ReedemLangs', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      reedem_id: {
        type: Sequelize.INTEGER,
        references: {
          model: "Reedems",
          key: "id"
        },
        onUpdate: "cascade",
        onDelete: "cascade"
      },
      language_id: {
        type: Sequelize.INTEGER,
        references: {
          model: "Languages",
          key: "id"
        },
        onUpdate: "cascade",
        onDelete: "cascade"
      },
      description: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('ReedemLangs');
  }
};