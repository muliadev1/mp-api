'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('PageFaqs', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      page_id: {
        type: Sequelize.INTEGER,
        references: {
          model: "Pages",
          key: "id"
        },
        onUpdate: "cascade",
        onDelete: "cascade"
      },
      faqs_title: {
        type: Sequelize.STRING
      },
      faqs_description: {
        type: Sequelize.STRING
      },
      language_id: {
        type: Sequelize.INTEGER,
        references: {
          model: "Languages",
          key: "id"
        },
        onUpdate: "cascade",
        onDelete: "cascade"
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('PageFaqs');
  }
};