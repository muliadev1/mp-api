'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('RateCodes', {
      id: {
        type: Sequelize.INTEGER,
      },
      rate_code: {
        type: Sequelize.STRING,
        allowNull: false,
        primaryKey: true,
        unique: true
      },
      rate_description: {
        type: Sequelize.STRING
      },
      rate_description2: {
        type: Sequelize.STRING
      },
      card: {
        type: Sequelize.STRING
      },
      segment: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('RateCodes');
  }
};