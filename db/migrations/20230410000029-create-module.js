'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('Modules', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      module_code: {
        type: Sequelize.STRING
      },
      module_name: {
        type: Sequelize.STRING
      },
      order_no: {
        type: Sequelize.INTEGER
      },
      url: {
        type: Sequelize.STRING
      },
      is_parent: {
        type: Sequelize.INTEGER
      },
      is_trx: {
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('Modules');
  }
};