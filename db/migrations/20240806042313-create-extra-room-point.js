'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('ExtraRoomPoints', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      room_type_code: {
        type: Sequelize.STRING
      },
      room_type_name: {
        type: Sequelize.STRING
      },
      percentage_3_months: {
        type: Sequelize.DECIMAL
      },
      percentage_6_months: {
        type: Sequelize.DECIMAL
      },
      percentage_9_months: {
        type: Sequelize.DECIMAL
      },
      createdBy: {
        type: Sequelize.STRING
      },
      updatedBy: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('ExtraRoomPoints');
  }
};