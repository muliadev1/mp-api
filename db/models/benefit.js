'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Benefit extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  Benefit.init({
    benefit_id: DataTypes.INTEGER,
    qualification_silver: DataTypes.STRING,
    qualification_gold: DataTypes.STRING,
    qualification_diamond: DataTypes.STRING,
    earning_points_silver: DataTypes.STRING,
    earning_points_gold: DataTypes.STRING,
    earning_points_diamond: DataTypes.STRING,
    extra_privileges_silver: DataTypes.STRING,
    extra_privileges_gold: DataTypes.STRING,
    extra_privileges_diamond: DataTypes.STRING,
    general_toc: DataTypes.STRING,
    language_id: DataTypes.INTEGER,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Benefit',
  });
  return Benefit;
};