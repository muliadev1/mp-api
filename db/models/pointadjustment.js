'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class PointAdjustment extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      PointAdjustment.belongsTo(models.Member, { as: 'members', foreignKey: 'member_id' });
    }
  }
  PointAdjustment.init({
    member_id: DataTypes.STRING,
    prev_total_points: DataTypes.DOUBLE,
    total_points: DataTypes.DOUBLE,
    prev_reedem_points: DataTypes.DOUBLE,
    reedem_points: DataTypes.DOUBLE,
    prev_available_points: DataTypes.DOUBLE,
    available_points: DataTypes.DOUBLE,
    remarks: DataTypes.STRING,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING,
    createdAt: {
      type: DataTypes.DATE
    },
    updatedAt: {
      type: DataTypes.DATE
    },
  }, {
    sequelize,
    modelName: 'PointAdjustment',
    timestamps: false
  });
  return PointAdjustment;
};