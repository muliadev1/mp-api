'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Menu extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Menu.belongsTo(models.Module, { as: 'module', foreignKey: 'module_id' });
      Menu.hasMany(models.RolesMenu, { as: 'rolesmenu', foreignKey: 'menu_id' });
    }
  }
  Menu.init({
    menu_code: DataTypes.STRING,
    menu_name: DataTypes.STRING,
    module_id: DataTypes.INTEGER,
    order_no: DataTypes.INTEGER,
    url: DataTypes.STRING,
    is_trx: DataTypes.INTEGER,
    createdBy: {
      type: DataTypes.STRING
    },
    updatedBy: {
      type: DataTypes.STRING
    }
  }, {
    sequelize,
    modelName: 'Menu',
  });
  return Menu;
};