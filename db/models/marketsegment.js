'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class MarketSegment extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      MarketSegment.hasMany(models.Master, { as: 'master', foreignKey: 'market_code' });
    }
  }
  MarketSegment.init({
    group: DataTypes.STRING,
    segment: DataTypes.STRING,
    code: DataTypes.STRING,
    description: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'MarketSegment',
  });
  return MarketSegment;
};