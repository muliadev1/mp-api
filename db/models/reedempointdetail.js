'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class ReedemPointDetail extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      ReedemPointDetail.belongsTo(models.ReedemPointMain, { as: 'reedempointmain', foreignKey: 'reedem_main_id' });
    }
  }
  ReedemPointDetail.init({
    reedem_main_id: DataTypes.INTEGER,
    member_id: DataTypes.STRING,
    confirmation_no: DataTypes.STRING,
    reedem_points: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'ReedemPointDetail',
  });
  return ReedemPointDetail;
};