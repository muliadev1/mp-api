'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Language extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Language.hasMany(models.PageContent, { as: 'pagecontents', foreignKey: 'language_id' })
      Language.hasMany(models.PageTerm)
      Language.hasMany(models.PageFaq)
    }
  }
  Language.init({
    language_name: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notNull: {
          msg: "Language name is required."
        },
        notEmpty: {
          msg: "Language name is required"
        }
      }
    }, 
    language_code: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notNull: {
          msg: "Language code is required."
        },
        notEmpty: {
          msg: "Language code is required"
        }
      }
    },
    iso_code: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notNull: {
          msg: "Iso code is required."
        },
        notEmpty: {
          msg: "Iso code is required"
        }
      }
    },
    createdBy: {
      type: DataTypes.STRING
    },
    updatedBy: {
      type: DataTypes.STRING
    }
  }, {
    sequelize,
    modelName: 'Language',
  });
  return Language;
};