'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class PageLang extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      PageLang.belongsTo(models.Page, { as: 'page', foreignKey: 'page_id' });
    }
  }
  PageLang.init({
    page_id: DataTypes.INTEGER,
    page_title: DataTypes.STRING,
    short_description: DataTypes.STRING,
    page_description: DataTypes.STRING,
    language_id: DataTypes.INTEGER,
    is_deleted: DataTypes.INTEGER,
    createdBy: {
      type: DataTypes.STRING
    },
    updatedBy: {
      type: DataTypes.STRING
    }
  }, {
    sequelize,
    modelName: 'PageLang',
  });
  return PageLang;
};