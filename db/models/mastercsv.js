'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class MasterCSV extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
        }
    }
    MasterCSV.init({
        stay_date: DataTypes.STRING,
        confirmation_no: DataTypes.INTEGER,
        rn: DataTypes.INTEGER,
        bookkeeping: DataTypes.STRING,
        room_rev: DataTypes.STRING,
        fb_rev: DataTypes.STRING,
        other_rev: DataTypes.STRING,
        market_code: DataTypes.STRING,
        market_group: DataTypes.STRING,
        rate_code: DataTypes.STRING,
        currency: DataTypes.STRING,
        rate_amount: DataTypes.STRING,
        share_amount: DataTypes.STRING,
        room_dep: DataTypes.INTEGER,
        adults: DataTypes.INTEGER,
        children: DataTypes.INTEGER,
        firstname: DataTypes.STRING,
        lastname: DataTypes.STRING,
        source_name: DataTypes.STRING,
        agent_name: DataTypes.STRING,
        company_name: DataTypes.STRING,
        qq_room: DataTypes.STRING,
        qs: DataTypes.STRING,
        group_status: DataTypes.STRING,
        resv_status: DataTypes.STRING,
        packages: DataTypes.STRING,
        title: DataTypes.STRING,
        passport: DataTypes.STRING,
        local_id: DataTypes.STRING,
        cancelation_date: DataTypes.STRING,
        insert_date: DataTypes.STRING,
        lead_days: DataTypes.STRING,
        arrival_date: DataTypes.STRING,
        departure_date: DataTypes.STRING,
        los: DataTypes.INTEGER,
        no_show: DataTypes.INTEGER,
        rtc: DataTypes.STRING,
        room_type: DataTypes.STRING,
        room_class: DataTypes.STRING,
        nationality: DataTypes.STRING,
        country: DataTypes.STRING,
        city: DataTypes.STRING,
        mp: DataTypes.STRING,
        mpenroll_date: DataTypes.STRING,
        mp_level: DataTypes.STRING,
        mp_enroll: DataTypes.STRING,
        wcc: DataTypes.STRING,
        wcc_enroll: DataTypes.STRING,
        sc: DataTypes.STRING,
        sc_enroll: DataTypes.STRING,
        birthday: DataTypes.STRING,
        mailist: DataTypes.STRING,
        email: DataTypes.STRING,
        telephone: DataTypes.STRING,
        room_rev_usd: DataTypes.STRING,
        room_rev_idr: DataTypes.STRING,
        fb_rev_usd: DataTypes.STRING,
        fb_rev_idr: DataTypes.STRING,
        other_rev_usd: DataTypes.STRING,
        other_rev_idr: DataTypes.STRING,
        flag: DataTypes.INTEGER,
        temp_points: DataTypes.STRING,
        region: DataTypes.STRING,
        createdBy: {
            type: DataTypes.STRING
        },
        updatedBy: {
            type: DataTypes.STRING
        }
    }, {
        sequelize,
        modelName: 'MasterCSV',
        tableName: 'masters_temp_csv',
    });
    return MasterCSV;
};