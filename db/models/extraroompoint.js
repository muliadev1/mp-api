'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class ExtraRoomPoint extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  ExtraRoomPoint.init({
    room_type_code: DataTypes.STRING,
    room_type_name: DataTypes.STRING,
    percentage_3_months: DataTypes.DECIMAL,
    percentage_6_months: DataTypes.DECIMAL,
    percentage_9_months: DataTypes.DECIMAL,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'ExtraRoomPoint',
  });
  return ExtraRoomPoint;
};