'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class PageContent extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      PageContent.hasMany(models.PageContentLang, { as: 'pagecontentlangs', foreignKey: 'page_content_id' });
      PageContent.belongsTo(models.Page, { as: 'page', foreignKey: 'page_id' });
      PageContent.belongsTo(models.Language, { as: 'language', foreignKey: "language_id" })
    }
  }
  PageContent.init({
    page_id: DataTypes.INTEGER,
    page_title: DataTypes.STRING,
    page_sub_title: DataTypes.STRING,
    page_link: DataTypes.STRING,
    page_content: DataTypes.STRING,
    image_name: DataTypes.STRING,
    image_url: DataTypes.STRING,
    language_id: DataTypes.INTEGER,
    is_deleted: DataTypes.INTEGER,
    createdBy: {
      type: DataTypes.STRING
    },
    updatedBy: {
      type: DataTypes.STRING
    }
  }, {
    sequelize,
    modelName: 'PageContent',
  });
  return PageContent;
};