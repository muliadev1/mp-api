'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Setting extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  Setting.init({
    room_amount: DataTypes.INTEGER,
    room_point_silver: DataTypes.INTEGER,
    room_point_gold: DataTypes.INTEGER,
    room_point_diamond: DataTypes.INTEGER,
    fb_amount: DataTypes.INTEGER,
    fb_point_silver: DataTypes.INTEGER,
    fb_point_gold: DataTypes.INTEGER,
    fb_point_diamond: DataTypes.INTEGER,
    other_amount: DataTypes.INTEGER,
    other_point_silver: DataTypes.INTEGER,
    other_point_gold: DataTypes.INTEGER,
    other_point_diamond: DataTypes.INTEGER,
    hotel_pay_to_loyalty: DataTypes.DECIMAL,
    loyalty_pay_to_hotel: DataTypes.DECIMAL,
    system_profit_percentage: DataTypes.DECIMAL,
    system_profit_amount: DataTypes.DECIMAL,
    complimentary_stay_nights: DataTypes.INTEGER,
    dividing_factors: DataTypes.DECIMAL,
    bookeeping: DataTypes.DECIMAL,
    bookeeping_jkt: DataTypes.INTEGER,
    hotel_level_1: DataTypes.DECIMAL,
    hotel_level_2: DataTypes.DECIMAL,
    hotel_level_3: DataTypes.DECIMAL,
    hotel_level_4: DataTypes.DECIMAL,
    hotel_level_5: DataTypes.DECIMAL,
    hotel_level_6: DataTypes.DECIMAL,
    hotel_level_7: DataTypes.DECIMAL,
    hotel_level_8: DataTypes.DECIMAL,
    hotel_level_9: DataTypes.DECIMAL,
    hotel_level_10: DataTypes.DECIMAL,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Setting',
  });
  return Setting;
};