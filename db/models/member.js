'use strict';
const { Integer } = require('read-excel-file');
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Member extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      // Member.belongsTo(models.MemberType, { foreignKey: "membertype_id" })
      // Member.hasMany(models.Master, { as: 'master', foreignKey: 'member_id' });
      //Member.hasMany(models.Master, { as: 'master', foreignKey: 'mp' });

      Member.hasOne(models.Master, { foreignKey: 'mp', sourceKey: 'member_id', as: 'masters' });

      Member.hasMany(models.EnrollsLog, { foreignKey: 'member_id', sourceKey: 'member_id', as: 'enrollLogs' });

      Member.hasMany(models.SecondPointLog, { foreignKey: 'member_id', sourceKey: 'member_id', as: 'secondpointLogs' });

      Member.hasMany(models.ReedemsPoint, { foreignKey: 'member_id', sourceKey: 'member_id', as: 'reedemspoints' });

      Member.hasMany(models.MemberVoucher, { foreignKey: 'member_id', sourceKey: 'member_id', as: 'membervouchers' });

      Member.hasMany(models.PointBuyPromo, { foreignKey: 'member_id', sourceKey: 'member_id', as: 'pointbuypromos' });

      Member.hasMany(models.UpgradeLog, { as: 'upgradelogs', foreignKey: 'member_id' });

      Member.hasMany(models.MemberLog, { as: 'memberlogs', foreignKey: 'member_id' });

      Member.hasMany(models.PointAdjustment, { foreignKey: 'member_id', sourceKey: 'member_id', as: 'pointadjustments' });

      Member.belongsTo(models.Company, { foreignKey: 'company_id', as: 'companies' });


      Member.hasOne(models.InitialPoint, {
        foreignKey: 'member_id', // This should be the foreign key that links the two tables
        sourceKey: 'member_id',
        as: 'initialPoint', // This alias is optional, but it can be useful
      });
    }
  }
  Member.init({
    name_on_card: DataTypes.STRING,
    first: DataTypes.STRING,
    middle: DataTypes.STRING,
    last: DataTypes.STRING,
    title: DataTypes.STRING,
    gender: DataTypes.STRING,
    birth_date: DataTypes.DATE,
    city: DataTypes.STRING,
    countryid: DataTypes.STRING,
    country: DataTypes.STRING,
    source: DataTypes.INTEGER,
    member_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
    },
    member_type_id: DataTypes.INTEGER,
    company_id: DataTypes.INTEGER,
    member_level: DataTypes.STRING,
    joined_date: DataTypes.DATE,
    exp_date: DataTypes.DATE,
    exp_level_date: DataTypes.DATE,
    total_points: DataTypes.STRING,
    reedem_points: DataTypes.STRING,
    available_points: DataTypes.STRING,
    primary_email: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notNull: {
          msg: "Email is required."
        },
        notEmpty: {
          msg: "Email is required"
        }
      }
    },
    other_email: DataTypes.STRING,
    is_mailist: DataTypes.INTEGER,
    primary_mobilephone: DataTypes.STRING,
    sales_person: DataTypes.STRING,
    sales_email: DataTypes.STRING,
    temp_password: DataTypes.STRING,
    password: DataTypes.STRING,
    refresh_token: DataTypes.STRING,
    is_active: DataTypes.INTEGER,
    activate_date: {
      type: DataTypes.DATE,
      allowNull: true,
    },
    notifications: DataTypes.INTEGER,
    is_first_enroll: DataTypes.STRING,
    region: DataTypes.STRING,
    is_deleted: DataTypes.INTEGER,
    is_reset: DataTypes.INTEGER,
    createdBy: {
      type: DataTypes.STRING
    },
    updatedBy: {
      type: DataTypes.STRING
    },
    createdAt: {
      type: DataTypes.DATE
    },
    updatedAt: {
      type: DataTypes.DATE
    },
  }, {
    sequelize,
    modelName: 'Member',
    timestamps: false
  });
  return Member;
};