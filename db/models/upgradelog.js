'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class UpgradeLog extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      UpgradeLog.belongsTo(models.Member, { as: 'members', foreignKey: 'member_id' });
    }
  }
  UpgradeLog.init({
    member_id: DataTypes.STRING,
    member_level: DataTypes.STRING,
    member_next_level: DataTypes.STRING,
    bonus_points: DataTypes.INTEGER,
    location: DataTypes.STRING,
    createdAt: DataTypes.DATE,
    createdBy: DataTypes.STRING,
    updatedAt: {
      type: DataTypes.DATE
    },
    updatedBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'UpgradeLog',
    timestamps: false
  });
  return UpgradeLog;
};