'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class ReedemsPoint extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      ReedemsPoint.hasMany(models.RedemsPointItem, { 
        foreignKey: 'redem_point_id', 
        sourceKey: 'id',
        as: 'items' 
      });
 
      ReedemsPoint.belongsTo(models.Reedem, { as: 'reedems', foreignKey: 'reedem_id' });

      ReedemsPoint.belongsTo(models.Member, { foreignKey: 'member_id', targetKey: 'member_id', as: 'members' });

      // ReedemsPoint.hasMany(models.MemberVoucher, { foreignKey: 'reedem_id', targetKey: 'reedem_id', as: 'memberVouchers', constraints: false });
      

      ReedemsPoint.hasMany(models.MemberVoucher, {
        as: 'memberVouchers',
        foreignKey: 'reedem_point_id'
      });
      
    }
  }
  ReedemsPoint.init({
    member_id: DataTypes.STRING,
    reedem_id: DataTypes.INTEGER,
    previous_balance:  DataTypes.INTEGER,
    required_points:  DataTypes.INTEGER,
    qty_points: DataTypes.INTEGER,
    total_reedem_points: DataTypes.INTEGER,
    remaining_points: DataTypes.INTEGER,
    beneficiary: DataTypes.STRING,
    comments: DataTypes.STRING,
    surcharge_type_id: DataTypes.INTEGER,
    surcharge_points: DataTypes.INTEGER,
    location: DataTypes.STRING,
    is_cancel: DataTypes.INTEGER,
    is_loyalty: DataTypes.INTEGER,
    is_revenue: DataTypes.INTEGER,
    is_financial: DataTypes.INTEGER,
    is_resident: DataTypes.INTEGER,
    is_manual: DataTypes.INTEGER,
    approve_status_id: DataTypes.INTEGER,
    is_addsurcharge_loyalty: DataTypes.INTEGER,
    is_addsurcharge_revenue: DataTypes.INTEGER,
    is_addsurcharge_financial: DataTypes.INTEGER,
    is_addsurcharge_resident: DataTypes.INTEGER,
    addsurcharge_status_id: DataTypes.INTEGER,
    createdBy: {
      type: DataTypes.STRING
    },
    updatedBy: {
      type: DataTypes.STRING
    },
    createdAt: {
      type: DataTypes.DATE
    },
    updatedAt: {
      type: DataTypes.DATE
    },
  }, {
    sequelize,
    modelName: 'ReedemsPoint',
    timestamps: false
  });
  return ReedemsPoint;
};