'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class PageTerm extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      PageTerm.belongsTo(models.Page, { foreignKey: "page_id" })
      PageTerm.belongsTo(models.Language, { foreignKey: "language_id" })
    }
  }
  PageTerm.init({
    page_id: DataTypes.INTEGER,
    term_title: DataTypes.STRING,
    term_description: DataTypes.STRING,
    language_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'PageTerm',
  });
  return PageTerm;
};