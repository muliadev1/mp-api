'use strict'
const { Model } = require('sequelize')
module.exports = (sequelize, DataTypes) => {
  class EnrollsLog extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      EnrollsLog.belongsTo(models.Member, {
        foreignKey: 'member_id',
        targetKey: 'member_id',
        as: 'members',
      })
    }
  }
  EnrollsLog.init(
    {
      confirmation_no: DataTypes.INTEGER,
      member_id: DataTypes.STRING,
      first_name: DataTypes.STRING,
      last_name: DataTypes.STRING,
      arrival_date: DataTypes.DATE,
      departure_date: DataTypes.DATE,
      room_class: DataTypes.STRING,
      room_idr: DataTypes.INTEGER,
      room_usd: DataTypes.INTEGER,
      fb_idr: DataTypes.INTEGER,
      fb_usd: DataTypes.INTEGER,
      room_point: DataTypes.INTEGER,
      fb_point: DataTypes.INTEGER,
      total_points: DataTypes.INTEGER,
      point_promotion: DataTypes.STRING,
      is_new_enroll: DataTypes.INTEGER,
      location: DataTypes.STRING,
      createdAt: {
        type: DataTypes.DATE,
      },
      createdBy: {
        type: DataTypes.STRING,
      },
      updatedAt: {
        type: DataTypes.DATE,
      },
      updatedBy: {
        type: DataTypes.STRING,
      },
    },
    {
      sequelize,
      modelName: 'EnrollsLog',
      timestamps: false,
    }
  )
  return EnrollsLog
}
