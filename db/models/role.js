'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Role extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Role.hasMany(models.RolesMenu, { as: 'rolesmenu', foreignKey: 'role_id' });
      Role.hasMany(models.User, { as: 'user', foreignKey: 'role_id' });
    }
  }
  Role.init({
    role_name: DataTypes.STRING,
    role_description: DataTypes.STRING,
    is_active: DataTypes.INTEGER,
    createdBy: {
      type: DataTypes.STRING
    },
    updatedBy: {
      type: DataTypes.STRING
    }
  }, {
    sequelize,
    modelName: 'Role',
  });
  return Role;
};