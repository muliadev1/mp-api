'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class OfferLang extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      OfferLang.belongsTo(models.Language, { foreignKey: "language_id" })
      OfferLang.belongsTo(models.ExclusiveOffer, { foreignKey: "offer_id" })
    }
  }
  OfferLang.init({
    offer_id: DataTypes.INTEGER,
    language_id: DataTypes.INTEGER,
    title: DataTypes.STRING,
    description: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'OfferLang',
  });
  return OfferLang;
};