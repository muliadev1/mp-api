'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class PageContentLang extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      PageContentLang.belongsTo(models.PageContent, { as: 'pagecontent', foreignKey: 'page_content_id' });
      PageContentLang.belongsTo(models.Language, { as: 'language', foreignKey: "language_id" })
    }
  }
  PageContentLang.init({
    page_content_id: DataTypes.INTEGER,
    page_title: DataTypes.STRING,
    page_sub_title: DataTypes.STRING,
    page_content: DataTypes.STRING,
    language_id: DataTypes.INTEGER,
    is_deleted: DataTypes.INTEGER,
    createdBy: {
      type: DataTypes.STRING
    },
    updatedBy: {
      type: DataTypes.STRING
    }
  }, {
    sequelize,
    modelName: 'PageContentLang',
  });
  return PageContentLang;
};