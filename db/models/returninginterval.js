'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class ReturningInterval extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  ReturningInterval.init({
    member_id: DataTypes.STRING,
    name_on_card: DataTypes.STRING,
    first_stay: DataTypes.DATE,
    thirty_days: DataTypes.STRING,
    sixty_days: DataTypes.STRING,
    ninety_days: DataTypes.STRING,
    one_hundred_twenty_days: DataTypes.STRING,
    one_hundred_fifty_days: DataTypes.STRING,
    one_hundred_eighty_days: DataTypes.STRING,
    two_hundred_ten_days: DataTypes.STRING,
    two_hundred_forty_days: DataTypes.STRING,
    two_hundred_seventy_days: DataTypes.STRING,
    three_hundred_days: DataTypes.STRING,
    three_hundred_thirty_days: DataTypes.STRING,
    three_hundred_sixty_days: DataTypes.STRING,
    more_three_hundred_sixty_days: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'ReturningInterval',
  });
  return ReturningInterval;
};