'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class PointBuyPromo extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      PointBuyPromo.belongsTo(models.Member, { as: 'member', foreignKey: 'member_id' });
    }
  }
  PointBuyPromo.init({
    member_id: DataTypes.STRING,
    member_name: DataTypes.STRING,
    purchase_date: DataTypes.DATE,
    property: DataTypes.STRING,
    check_number: DataTypes.STRING,
    total_point: DataTypes.INTEGER,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING,
    createdAt: {
      type: DataTypes.DATE
    },
    updatedAt: {
      type: DataTypes.DATE
    },
  }, {
    sequelize,
    modelName: 'PointBuyPromo',
    timestamps: false
  });
  return PointBuyPromo;
};

