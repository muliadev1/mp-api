'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Outlet extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Outlet.hasMany(models.Dining, { as: 'dining', foreignKey: 'outlet_id' });
    }
  }
  Outlet.init({
    outlet_name: DataTypes.STRING,
    cuisine: DataTypes.STRING,
    opening_hour: DataTypes.STRING,
    breakfast: DataTypes.STRING,
    lunch: DataTypes.STRING,
    dinner: DataTypes.STRING,
    snack: DataTypes.STRING,
    sunday_brunch: DataTypes.STRING,
    sunday_lunch: DataTypes.STRING,
    all_day_dining: DataTypes.STRING,
    afternoon_tea: DataTypes.STRING,
    late_night: DataTypes.STRING,
    entertainment: DataTypes.STRING,
    region: DataTypes.STRING,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Outlet',
  });
  return Outlet;
};