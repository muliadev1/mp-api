'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class MemberVoucher extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      MemberVoucher.belongsTo(models.Member, { foreignKey: 'member_id', targetKey: 'member_id', as: 'members' });
      MemberVoucher.belongsTo(models.Reedem, { as: 'reedems', foreignKey: 'reedem_id' });
      // MemberVoucher.belongsTo(models.ReedemsPoint, { foreignKey: 'reedem_id', targetKey: 'reedem_id', as: 'reedemspoints' });
      MemberVoucher.belongsTo(models.ReedemsPoint, {
        as: 'reedemspoints',
        foreignKey: 'reedem_point_id'
      });
    }
  }
  MemberVoucher.init({
    reedem_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'ReedemsPoints', // Nama tabel ReedemPoint
        key: 'id'
      }
    },
    reedem_name: DataTypes.STRING,
    member_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'Members', // Nama tabel member
        key: 'id'
      }
    },
    reedem_point_id: DataTypes.INTEGER,
    beneficiary: DataTypes.STRING,
    comments: DataTypes.STRING,
    voucher_no: DataTypes.STRING,
    date_expired: DataTypes.DATE,
    qrcode_url: DataTypes.STRING,
    is_used: DataTypes.INTEGER,
    outlet_name: DataTypes.STRING,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING,
    createdAt: {
      type: DataTypes.DATE
    },
    updatedAt: {
      type: DataTypes.DATE
    },
  }, {
    sequelize,
    modelName: 'MemberVoucher',
    timestamps: false
  });
  return MemberVoucher;
};