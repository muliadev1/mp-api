'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Page extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Page.hasMany(models.PageLang, { as: 'pagelangs', foreignKey: 'page_id' });
      Page.hasMany(models.PageContent, { as: 'pagecontent', foreignKey: 'page_id' });
      Page.hasMany(models.Tag, { as: 'tags', foreignKey: 'page_id' });
    }
  }
  Page.init({
    page_name: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notNull: {
          msg: "Page name is required."
        },
        notEmpty: {
          msg: "Page name is required"
        }
      }
    },
    page_title:DataTypes.STRING,
    short_description: DataTypes.STRING,
    page_description: DataTypes.STRING,
    page_address: DataTypes.STRING,
    image_name: DataTypes.STRING,
    image_url: DataTypes.STRING,
    is_active: DataTypes.INTEGER,
    is_deleted: DataTypes.INTEGER,
    createdBy: {
      type: DataTypes.STRING
    },
    updatedBy: {
      type: DataTypes.STRING
    }
  }, {
    sequelize,
    modelName: 'Page',
  });
  return Page;
};