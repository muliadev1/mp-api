'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class RolesMenu extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      RolesMenu.belongsTo(models.Role, { as: 'role', foreignKey: 'role_id' });
      RolesMenu.belongsTo(models.Module, { as: 'module', foreignKey: 'module_id' });
      RolesMenu.belongsTo(models.Menu, { as: 'menu', foreignKey: 'menu_id' }); 
    }
  }
  RolesMenu.init({
    role_id: DataTypes.INTEGER,
    module_id: DataTypes.INTEGER,
    menu_id: DataTypes.INTEGER,
    order_no: DataTypes.INTEGER,
    is_insert: DataTypes.INTEGER,
    is_update: DataTypes.INTEGER,
    is_delete: DataTypes.INTEGER,
    is_read: DataTypes.INTEGER,
    is_cancel: DataTypes.INTEGER,
    is_search: DataTypes.INTEGER,
    is_loyalty: DataTypes.INTEGER,
    is_revenue: DataTypes.INTEGER,
    is_finance: DataTypes.INTEGER,
    is_resident: DataTypes.INTEGER,
  }, {
    sequelize,
    modelName: 'RolesMenu',
  });
  return RolesMenu;
};