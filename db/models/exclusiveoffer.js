'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class ExclusiveOffer extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      ExclusiveOffer.hasMany(models.ExclusiveOfferLang, { as: 'offerlangs', foreignKey: 'exclusive_offer_id' });
    }
  }
  ExclusiveOffer.init({
    destination: DataTypes.STRING,
    hotel: DataTypes.STRING,
    image_name: DataTypes.STRING,
    image_url: DataTypes.STRING,
    name: DataTypes.STRING,
    category: DataTypes.INTEGER,
    description: DataTypes.STRING,
    url_page: DataTypes.STRING,
    order_no: DataTypes.INTEGER,
    is_booking: DataTypes.INTEGER,
    is_active: DataTypes.INTEGER,
    is_homepage: DataTypes.INTEGER,
    is_deleted: DataTypes.INTEGER,
    createdBy: {
      type: DataTypes.STRING
    },
    updatedBy: {
      type: DataTypes.STRING
    }
  }, {
    sequelize,
    modelName: 'ExclusiveOffer',
  });
  return ExclusiveOffer;
};