'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Tag extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Tag.belongsTo(models.Page, { as: 'page', foreignKey: 'page_id' });
      Tag.hasMany(models.TagTranslation, { as: 'translations', foreignKey: 'tag_id' });
    }
  }
  Tag.init({
    tag_name: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notNull: {
          msg: "Tag name is required."
        },
        notEmpty: {
          msg: "Tag name is required"
        }
      }
    },
    page_id:DataTypes.INTEGER,
    is_delete: DataTypes.INTEGER,
    createdBy: {
      type: DataTypes.STRING
    },
    updatedBy: {
      type: DataTypes.STRING
    }
  }, {
    sequelize,
    modelName: 'Tag',
  });
  return Tag;
};