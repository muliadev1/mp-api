'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class RedemsPointItem extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      RedemsPointItem.belongsTo(models.ReedemsPoint, { 
        foreignKey: 'redem_point_id',  // Kolom di RedemsPointItem yang menjadi FK
        targetKey: 'id',               // Kolom di ReedemsPoint yang menjadi PK
        as: 'reedemspoint'             // Alias untuk asosiasi
      });
    }
  }
  RedemsPointItem.init({
    redem_point_id: DataTypes.INTEGER,
    redem_id: DataTypes.INTEGER,
    redem_name: DataTypes.STRING,
    redem_type_id: DataTypes.INTEGER,
    redem_type: DataTypes.STRING,
    redem_location: DataTypes.STRING,
    required_points: DataTypes.INTEGER,
    qty_points: DataTypes.INTEGER,
    total_redem_points: DataTypes.INTEGER,
    beneficiary: DataTypes.STRING,
    comments: DataTypes.STRING,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING,
    createdAt: {
      type: DataTypes.DATE
    },
    updatedAt: {
      type: DataTypes.DATE
    },
  }, {
    sequelize,
    modelName: 'RedemsPointItem',
    timestamps: false
  });
  return RedemsPointItem;
};



