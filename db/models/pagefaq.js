'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class PageFaq extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      PageFaq.belongsTo(models.Page, { foreignKey: "page_id" })
      PageFaq.belongsTo(models.Language, { foreignKey: "language_id" })
    }
  }
  PageFaq.init({
    page_id: DataTypes.INTEGER,
    faqs_title: DataTypes.STRING,
    faqs_description: DataTypes.STRING,
    language_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'PageFaq',
  });
  return PageFaq;
};