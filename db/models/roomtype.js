'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class RoomType extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      RoomType.hasMany(models.Master, { as: 'master', foreignKey: 'room_type' });
    }
  }
  RoomType.init({
    room_type_code: DataTypes.STRING,
    room_type: DataTypes.STRING,
    room_type_description: DataTypes.STRING,
    createdBy: {
      type: DataTypes.STRING
    },
    updatedBy: {
      type: DataTypes.STRING
    }
  }, {
    sequelize,
    modelName: 'RoomType',
  });
  return RoomType;
};