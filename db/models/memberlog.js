'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class MemberLog extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      MemberLog.belongsTo(models.Member, { as: 'members', foreignKey: 'member_id' });
    }
  }
  MemberLog.init({
    member_id: DataTypes.STRING,
    first_name: DataTypes.STRING,
    last_name: DataTypes.STRING,
    name_on_card: DataTypes.STRING,
    birth_date: DataTypes.DATE,
    member_level: DataTypes.STRING,
    joined_date: DataTypes.DATE,
    exp_date: DataTypes.DATE,
    exp_level_date: DataTypes.DATE,
    primary_email: DataTypes.STRING,
    total_points: DataTypes.DOUBLE,
    reedem_points: DataTypes.DOUBLE,
    available_points: DataTypes.DOUBLE,
    logs_location: DataTypes.STRING,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING,
    createdAt: {
      type: DataTypes.DATE
    },
    updatedAt: {
      type: DataTypes.DATE
    },
  }, {
    sequelize,
    modelName: 'MemberLog',
    timestamps: false,
  });
  return MemberLog;
};