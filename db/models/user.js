'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      User.belongsTo(models.Role, { as: 'role', foreignKey: 'role_id' });
    }
  }
  User.init({
    title_name: DataTypes.STRING,
    first_name: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notNull: {
          msg: "First name is required."
        },
        notEmpty: {
          msg: "First name is required"
        }
      }
    },
    last_name: DataTypes.STRING,
    username: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notNull: {
          msg: "Username is required."
        },
        notEmpty: {
          msg: "Username is required"
        }
      }
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notNull: {
          msg: "Password is required."
        },
        notEmpty: {
          msg: "Password is required"
        }
      }
    },
    role_id: DataTypes.INTEGER,
    refresh_token: DataTypes.STRING,
    email: DataTypes.STRING,
    phone: DataTypes.STRING,
    image_name: DataTypes.STRING,
    image_url: DataTypes.STRING,
    region:DataTypes.STRING,
    outlet_id:DataTypes.INTEGER,
    outlet_name:DataTypes.STRING,
    createdBy: {
      type: DataTypes.STRING
    },
    updatedBy: {
      type: DataTypes.STRING
    }
  }, {
    sequelize,
    modelName: 'User',
  });
  return User;
};