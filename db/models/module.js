'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Module extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Module.hasMany(models.Menu, { as: 'menu', foreignKey: 'module_id' });
      Module.hasMany(models.RolesMenu, { as: 'rolesmenu', foreignKey: 'module_id' });
    }
  }
  Module.init({
    module_code: DataTypes.STRING,
    module_name: DataTypes.STRING,
    order_no: DataTypes.INTEGER,
    url: DataTypes.STRING,
    is_parent: DataTypes.INTEGER,
    is_trx: DataTypes.INTEGER,
    createdBy: {
      type: DataTypes.STRING
    },
    updatedBy: {
      type: DataTypes.STRING
    }
  }, {
    sequelize,
    modelName: 'Module',
  });
  return Module;
};