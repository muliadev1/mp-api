'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class ReedemPointMain extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      ReedemPointMain.hasMany(models.ReedemPointDetail, { as: 'reedempointdetail', foreignKey: 'reedem_main_id' });
    }
  }
  ReedemPointMain.init({
    reedem_id: DataTypes.INTEGER,
    total_reedem_points: DataTypes.INTEGER,
    commands: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'ReedemPointMain',
  });
  return ReedemPointMain;
};