'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class MissingPointEmail extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  MissingPointEmail.init({
    member_id: DataTypes.STRING,
    confirmation_no: DataTypes.STRING,
    guest_name: DataTypes.STRING,
    property: DataTypes.STRING,
    checkin: DataTypes.DATE,
    checkout: DataTypes.DATE,
    file: DataTypes.STRING,
    url: DataTypes.STRING,
    comment: DataTypes.TEXT,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'MissingPointEmail',
  });
  return MissingPointEmail;
};