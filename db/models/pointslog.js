'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class PointsLog extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  PointsLog.init({
    confirmation_no: DataTypes.INTEGER,
    member_id: DataTypes.STRING,
    first_name: DataTypes.STRING,
    last_name: DataTypes.STRING,
    arrival_date: DataTypes.DATE,
    departure_date: DataTypes.DATE,
    room_class: DataTypes.STRING,
    room_usd: DataTypes.INTEGER,
    fb_usd: DataTypes.INTEGER,
    room_point: DataTypes.INTEGER,
    fb_point: DataTypes.INTEGER,
    total_points: DataTypes.INTEGER,
    createdAt: {
      type: DataTypes.DATE
    },
    createdBy: {
      type: DataTypes.STRING
    },
    updatedAt: {
      type: DataTypes.DATE
    },
    updatedBy: {
      type: DataTypes.STRING
    },
  }, {
    sequelize,
    modelName: 'PointsLog',
    timestamps: false
  });
  return PointsLog;
};