'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Reedem extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Reedem.hasMany(models.ReedemLang, { as: 'reedemlangs', foreignKey: 'reedem_id' });

      Reedem.hasMany(models.ReedemsPoint, { as: 'reedemspoints', foreignKey: 'reedem_id' });

      Reedem.hasMany(models.MemberVoucher, { as: 'membervouchers', foreignKey: 'reedem_id' });

      Reedem.belongsTo(models.RedemType, { as: 'redemtypes', foreignKey: 'redem_type_id' });
    }
  }
  Reedem.init({
    reedem_name: DataTypes.STRING,
    redem_type_id: DataTypes.INTEGER,
    reedem_type: DataTypes.STRING,
    location: DataTypes.STRING,
    point: DataTypes.INTEGER,
    image_name: DataTypes.STRING,
    image_url: DataTypes.STRING,
    short_desc: DataTypes.STRING,
    description: DataTypes.STRING,
    order_no: DataTypes.INTEGER,
    is_active: DataTypes.INTEGER,
    is_deleted: DataTypes.INTEGER,
    createdBy: {
      type: DataTypes.STRING
    },
    updatedBy: {
      type: DataTypes.STRING
    },
    short_desc:DataTypes.STRING,
  }, {
    sequelize,
    modelName: 'Reedem',
  });
  return Reedem;
};