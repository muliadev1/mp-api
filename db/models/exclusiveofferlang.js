'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class ExclusiveOfferLang extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      ExclusiveOfferLang.belongsTo(models.ExclusiveOffer, { as: 'offers', foreignKey: 'exclusive_offer_id' });
      ExclusiveOfferLang.belongsTo(models.Language, { as: 'language', foreignKey: "language_id" })
    }
  }
  ExclusiveOfferLang.init({
    exclusive_offer_id: DataTypes.INTEGER,
    name: DataTypes.STRING,
    description: DataTypes.STRING,
    language_id: DataTypes.INTEGER,
    is_deleted: DataTypes.INTEGER,
    createdBy: {
      type: DataTypes.STRING
    },
    updatedBy: {
      type: DataTypes.STRING
    }
  }, {
    sequelize,
    modelName: 'ExclusiveOfferLang',
  });
  return ExclusiveOfferLang;
};