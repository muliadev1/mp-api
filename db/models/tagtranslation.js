'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class TagTranslation extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      TagTranslation.belongsTo(models.Tag, { as: 'tags', foreignKey: 'tag_id' });
    }
  }
  TagTranslation.init({
    tag_id:DataTypes.INTEGER,
    tag_translation: DataTypes.STRING,
    language_id: DataTypes.INTEGER,
    is_delete: DataTypes.INTEGER,
    createdBy: {
      type: DataTypes.STRING
    },
    updatedBy: {
      type: DataTypes.STRING
    }
  }, {
    sequelize,
    modelName: 'TagTranslation',
  });
  return TagTranslation;
};