'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Package_Sub extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  Package_Sub.init({
    package_id: DataTypes.INTEGER,
    room_id: DataTypes.INTEGER,
    room_name: DataTypes.STRING,
    room_price: DataTypes.DOUBLE,
    total_price: DataTypes.DOUBLE,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Package_Sub',
  });
  return Package_Sub;
};