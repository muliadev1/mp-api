'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class InitialPoint extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      InitialPoint.hasMany(models.Master, { as: 'master', foreignKey: 'mp' });
    }
  }
  InitialPoint.init({
    member_id: DataTypes.STRING,
    checkin_date: DataTypes.STRING,
    checkout_date: DataTypes.STRING,
    property: DataTypes.STRING,
    room_usd: DataTypes.INTEGER,
    fb_usd: DataTypes.INTEGER,
    init_point: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'InitialPoint',
  });
  return InitialPoint;
};