'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class MemberType extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      // MemberType.hasMany(models.Member)
    }
  }
  MemberType.init({
    member_type: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'MemberType',
  });
  return MemberType;
};