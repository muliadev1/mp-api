'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class ReedemLang extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      ReedemLang.belongsTo(models.Language, { foreignKey: "language_id" })
      ReedemLang.belongsTo(models.Reedem, { as: 'reedems', foreignKey: 'reedem_id' });

    }
  }
  ReedemLang.init({
    reedem_id: DataTypes.INTEGER,
    reedem_name: DataTypes.STRING,
    short_desc: DataTypes.STRING,
    description: DataTypes.STRING,
    language_id: DataTypes.INTEGER,
    is_deleted: DataTypes.INTEGER,
    createdBy: {
      type: DataTypes.STRING
    },
    updatedBy: {
      type: DataTypes.STRING
    }
  }, {
    sequelize,
    modelName: 'ReedemLang',
  });
  return ReedemLang;
};