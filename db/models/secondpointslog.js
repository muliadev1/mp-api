'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class SecondPointLog extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      SecondPointLog.belongsTo(models.Member, { foreignKey: 'member_id', targetKey: 'member_id', as: 'members' });
    }
  }
  SecondPointLog.init({
    member_id: DataTypes.STRING,
    first_name: DataTypes.STRING,
    last_name: DataTypes.STRING,
    total_points: DataTypes.INTEGER,
    createdAt: {
      type: DataTypes.DATE
    },
    createdBy: {
      type: DataTypes.STRING
    },
    updatedAt: {
      type: DataTypes.DATE
    },
    updatedBy: {
      type: DataTypes.STRING
    },
  }, {
    sequelize,
    modelName: 'SecondPointLog',
    timestamps: false,
  });
  return SecondPointLog;
};