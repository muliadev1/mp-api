'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class PointDailyTransaction extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  PointDailyTransaction.init({
    inquiry_date: DataTypes.DATE,
    opening_balance: DataTypes.FLOAT,
    new_enroll_bali: DataTypes.FLOAT,
    new_enroll_jakarta: DataTypes.FLOAT,
    existing_enroll_bali: DataTypes.FLOAT,
    existing_enroll_jakarta: DataTypes.FLOAT,
    purchase_point_bali: DataTypes.FLOAT,
    purchase_point_jakarta: DataTypes.FLOAT,
    bonus_point_bali: DataTypes.FLOAT,
    bonus_point_jakarta: DataTypes.FLOAT,
    dining_point_bali: DataTypes.FLOAT,
    dining_point_jakarta: DataTypes.FLOAT,
    redeem_point_bali: DataTypes.FLOAT,
    redeem_point_jakarta: DataTypes.FLOAT,
    closing_balance: DataTypes.FLOAT,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING,
    createdAt: {
      type: DataTypes.DATE
    },
    updatedAt: {
      type: DataTypes.DATE
    },
  }, {
    sequelize,
    modelName: 'PointDailyTransaction',
    timestamps: false
  });
  return PointDailyTransaction;
};