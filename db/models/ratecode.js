'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class RateCode extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      RateCode.hasMany(models.Master, { as: 'master', foreignKey: 'rate_code' });
    }
  }
  RateCode.init({
    rate_code: DataTypes.STRING,
    rate_description: DataTypes.STRING,
    rate_description2: DataTypes.STRING,
    card: DataTypes.STRING,
    segment: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'RateCode',
  });
  return RateCode;
};