'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Dining extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Dining.belongsTo(models.Outlet, { as: 'outlet', foreignKey: 'outlet_id' });
      Dining.belongsTo(models.Member, { as: 'member', foreignKey: 'member_id' });
    }
  }
  Dining.init({
    dining_date: DataTypes.DATE,
    outlet_id: DataTypes.INTEGER,
    member_id: DataTypes.STRING,
    member_name: DataTypes.STRING,
    total_bill: DataTypes.DOUBLE,
    total_bill_rp: DataTypes.DOUBLE,
    bookeeping: DataTypes.DOUBLE,
    point_type_id: DataTypes.INTEGER,
    earned_point: DataTypes.DOUBLE,
    check_number: DataTypes.STRING,
    location: DataTypes.STRING,
    remarks: DataTypes.STRING,
    bill_image: DataTypes.STRING,
    bill_url: DataTypes.STRING,
    is_first_dining: DataTypes.INTEGER,
    is_approved: DataTypes.INTEGER,
    is_deleted: DataTypes.INTEGER,
    approvedBy: DataTypes.STRING,
    approvedAt: DataTypes.DATE,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING,
    createdAt: {
      type: DataTypes.DATE
    },
    updatedAt: {
      type: DataTypes.DATE
    },
  }, {
    sequelize,
    modelName: 'Dining',
    timestamps: false
  });
  return Dining;
};