'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class ReedemsPointLog extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  ReedemsPointLog.init({
    member_id: DataTypes.STRING,
    reedem_id: DataTypes.INTEGER,
    total_reedem_points: DataTypes.INTEGER,
    comments: DataTypes.STRING,
    surcharge_type_id: DataTypes.INTEGER,
    surcharge_points: DataTypes.INTEGER,
    createdAt: {
      type: DataTypes.DATE
    },
    createdBy: {
      type: DataTypes.STRING
    },
    updatedAt: {
      type: DataTypes.DATE
    },
    updatedBy: {
      type: DataTypes.STRING
    },
  }, {
    sequelize,
    modelName: 'ReedemsPointLog',
    timestamps: false
  });
  return ReedemsPointLog;
};