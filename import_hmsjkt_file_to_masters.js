const { now } = require('moment');
const mysql = require('mysql');
const xlsx = require('xlsx');

const db = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "123456",
    database: "muliadb"
})


// Mendapatkan tanggal saat ini
let currentDate = new Date();
currentDate.setDate(currentDate.getDate() - 1);

// Mengambil komponen tanggal, bulan, dan tahun dari tanggal saat ini
let day = currentDate.getDate(); // Mendapatkan hari (1-31)
let month = currentDate.getMonth() + 1; // Mendapatkan bulan (0-11), ditambah 1 karena bulan dimulai dari 0
let year = currentDate.getFullYear(); // Mendapatkan tahun (e.g., 2024)



// Format tanggal sesuai dengan format yang diinginkan (misalnya YYYY-MM-DD)
let formattedDate = `${day.toString().padStart(2, '0')}/${month.toString().padStart(2, '0')}/${year}`;

console.log(formattedDate, "isi tanggal")

// Format tanggal sesuai dengan format yang diinginkan (misalnya YYYY-MM-DD)
// let stayDate = `${lastyear}-${lastmonth.toString().padStart(2, '0')}-${lastday.toString().padStart(2, '0')}`;

// get exchange rate berdasarkan stayDate atau tanggal sebelumnya
// let bookkeepingValue=0;
// let sqlExc = "SELECT * FROM Masters m WHERE m.stay_date = ? AND bookkeeping > 0 LIMIT 1";

// db.query(sqlExc, [formattedDate], (err, results, fields) => {
//     if (err) {
//         return console.error(err.message);
//     }

//     // Mengekstrak nilai bookkeeping dari hasil query
//     if (results.length > 0) {
//         bookkeepingValue = results[0].bookkeeping;
//         console.log("Exchange rate: " + bookkeepingValue);
//     } else {
//         console.log("Tidak ada hasil yang ditemukan untuk tanggal tersebut.");
//     }
// });

// db.end();

let workbook = xlsx.readFile('D:/Projects/MP Data_Departure 18 Mar 2024.xlsx')
//let workbook = xlsx.readFile('D:/Projects/basedCO.xlsx')
let worksheet = workbook.Sheets[workbook.SheetNames[0]]
let range = xlsx.utils.decode_range(worksheet["!ref"])

console.log(range, ">>>>>> isi range")

for (let row = range.s.r + 1; row <= range.e.r; row++) {
    let data = []

    // Menentukan kolom yang ingin dimasukkan ke dalvam tabel
    let confirmation_no = worksheet[xlsx.utils.encode_cell({ r: row, c: 0 })]?.v;
    let exchange_rate = worksheet[xlsx.utils.encode_cell({ r: row, c: 37 })]?.v;
    let market_code = worksheet[xlsx.utils.encode_cell({ r: row, c: 3 })]?.v;
    let market_group = worksheet[xlsx.utils.encode_cell({ r: row, c: 4 })]?.v;
    let rate_code = worksheet[xlsx.utils.encode_cell({ r: row, c: 5 })]?.v;
    let currency = worksheet[xlsx.utils.encode_cell({ r: row, c: 6 })]?.v;
    let adult = worksheet[xlsx.utils.encode_cell({ r: row, c: 8 })]?.v;
    let child = worksheet[xlsx.utils.encode_cell({ r: row, c: 9 })]?.v;
    let first_name = worksheet[xlsx.utils.encode_cell({ r: row, c: 10 })]?.v;
    let last_name = worksheet[xlsx.utils.encode_cell({ r: row, c: 11 })]?.v;
    let source_name = worksheet[xlsx.utils.encode_cell({ r: row, c: 12 })]?.v;
    let agent_name = worksheet[xlsx.utils.encode_cell({ r: row, c: 14 })]?.v;
    let company_name = worksheet[xlsx.utils.encode_cell({ r: row, c: 15 })]?.v;
    // let room_no = worksheet[xlsx.utils.encode_cell({ r: row, c: 16 })]?.v;
    let group_status = worksheet[xlsx.utils.encode_cell({ r: row, c: 17 })]?.v;
    let resvation_status = worksheet[xlsx.utils.encode_cell({ r: row, c: 18 })]?.v;
    let title = worksheet[xlsx.utils.encode_cell({ r: row, c: 19 })]?.v;
    let insert_date = convertExcelDateToJSDate(worksheet[xlsx.utils.encode_cell({ r: row, c: 20 })]?.v);
    let arrival_date = convertExcelDateToJSDate(worksheet[xlsx.utils.encode_cell({ r: row, c: 21 })]?.v);
    let departure_date = convertExcelDateToJSDate(worksheet[xlsx.utils.encode_cell({ r: row, c: 22 })]?.v);

    let los = worksheet[xlsx.utils.encode_cell({ r: row, c: 23 })]?.v;
    let room_type = worksheet[xlsx.utils.encode_cell({ r: row, c: 24 })]?.v;
    let room_class = worksheet[xlsx.utils.encode_cell({ r: row, c: 25 })]?.v;
    let nationality = worksheet[xlsx.utils.encode_cell({ r: row, c: 26 })]?.v;
    let country = worksheet[xlsx.utils.encode_cell({ r: row, c: 27 })]?.v;
    let city = worksheet[xlsx.utils.encode_cell({ r: row, c: 28 })]?.v;
    let member_id = worksheet[xlsx.utils.encode_cell({ r: row, c: 29 })]?.v;
    let member_level = worksheet[xlsx.utils.encode_cell({ r: row, c: 30 })]?.v;
    let birth_date = convertExcelDateToJSDate(worksheet[xlsx.utils.encode_cell({ r: row, c: 31 })]?.v);
    let email = worksheet[xlsx.utils.encode_cell({ r: row, c: 32 })]?.v;
    let phone = worksheet[xlsx.utils.encode_cell({ r: row, c: 33 })]?.v;
    let room_idr = worksheet[xlsx.utils.encode_cell({ r: row, c: 38 })]?.v;
    let room_usd = worksheet[xlsx.utils.encode_cell({ r: row, c: 40 })]?.v;
    let fb_idr = worksheet[xlsx.utils.encode_cell({ r: row, c: 41 })]?.v;
    let fb_usd = worksheet[xlsx.utils.encode_cell({ r: row, c: 43 })]?.v;

    //COT, PKG, RCK, VCO, LST 
    let room_point = 0;
    let fb_point = 0;

    if (market_group === 'COT') {
        room_point = room_usd * 5
        fb_point = fb_usd * 0.5
    } else if (market_group === 'PKG') {
        room_point = room_usd * 5
        fb_point = fb_usd * 0.5
    } else if (market_group === 'VCO') {
        room_point = room_usd * 5
        fb_point = fb_usd * 0.5
    } else if (market_group === 'VCO') {
        room_point = room_usd * 5
        fb_point = fb_usd * 0.5
    }


    // Memasukkan nilai kolom ke dalam array data
    data.push(formattedDate);
    data.push(confirmation_no);
    data.push(los);
    data.push(exchange_rate);
    data.push(market_code);
    data.push(market_group);
    data.push(rate_code);
    data.push(currency);
    data.push(adult);
    data.push(child);
    data.push(first_name);
    data.push(last_name);
    data.push(source_name);
    data.push(agent_name);
    data.push(company_name);
    // data.push(room_no);
    data.push(group_status);
    data.push(resvation_status);
    data.push(title);
    data.push(insert_date);
    data.push(arrival_date);
    data.push(departure_date);
    data.push(los);
    data.push(room_type);
    data.push(room_class);
    data.push(nationality);
    data.push(country);
    data.push(city);
    data.push(member_id);
    data.push(member_level);
    data.push(birth_date);
    data.push(email);
    data.push(phone);
    data.push(room_idr);
    data.push(room_usd);
    data.push(fb_idr);
    data.push(fb_usd);
    data.push('JAKARTA');
    data.push('SYS');
    data.push('SYS');

    //kalkulasi point berdasarkan marget group
    // From Room and FB Revenue: COT, PKG, RCK, VCO, LST 



    // From FB Revenue: OTA, WHL



    console.log(data)

    // insert to table
    let sql = "INSERT INTO masters_new(stay_date,confirmation_no,rn,bookkeeping,market_code,market_group,rate_code,currency,adults,children,firstname,lastname,source_name,agent_name,company_name,group_status,resv_status,title,insert_date,arrival_date,departure_date,los,room_type,room_class,nationality,country,city, mp,mp_level,birthday,email,telephone,room_rev_idr,room_rev_usd,fb_rev_idr,fb_rev_usd,region,createdBy,updatedBy) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"
    db.query(sql, data, (err, results, fields) => {
        if (err) {
            return console.error(err.message)
        }
        console.log("Member ID:" + results.insertId)
    })

    // ambail nilai total points dan reedem points
    let totalPoint = 0
    let totalReedem = 0
    let availablePoint = 0
    let sql2 = "SELECT total_points, reedem_points from members where member_id = ?"
    db.query(sql2, [member_id], (err, results, fields) => {
        if (err) {
            return console.error(err.message)
        }
        if (results.length > 0) {
            totalPoint = results[0].total_points + room_point + fb_point;
            totalReedem = results[0].reedem_points;
            availablePoint = totalPoint - totalReedem;
            // Update tabel members
            let updateSql = "UPDATE members SET total_points = ?, available_points = ? WHERE member_id = ?";
            let updateValues = [totalPoint, availablePoint, member_id];
            db.query(updateSql, updateValues, (err, result) => {
                if (err) {
                    return console.error(err.message);
                }
                console.log("Tabel members berhasil diperbarui.");
            });
            console.log("Member ID : ---> ", member_id)
        } else {
            console.log("Member ID tidak ketemu.");
        }
    })


}

// // Tutup koneksi ke database setelah selesai
db.end();

// Fungsi untuk mengonversi tanggal Excel ke tipe data tanggal JavaScript
function convertExcelDateToJSDate(excelDate) {
    // Konversi nilai Excel ke tipe data tanggal JavaScript
    // return new Date((excelDate - (25567 + 1)) * 86400 * 1000); // Sesuai dengan formula konversi tanggal Excel ke JavaScript
    let jsDate = new Date((excelDate - 25569) * 86400 * 1000);

    // Mendapatkan komponen tanggal, bulan, dan tahun dari tanggal JavaScript
    let year = jsDate.getFullYear();
    let month = (jsDate.getMonth() + 1).toString().padStart(2, '0');
    let day = jsDate.getDate().toString().padStart(2, '0');

    // Menggabungkan komponen tanggal, bulan, dan tahun dalam format yang diinginkan
    let formattedDate = `${year}-${month}-${day}`;

    return formattedDate;
}

