require('dotenv').config();

const express = require('express');
const { PDFDocument, StandardFonts, rgb } = require('pdf-lib');
const axios = require('axios');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const QRCode = require('qrcode');
const fs = require('fs');
const multer = require('multer');
const path = require('path');
const cors = require('cors');
var jwt = require('jsonwebtoken');

const responseEnhancer = require('express-response-formatter');

const port = process.env.PORT || 3009;

const app = express();

require('dotenv').config(); // Load environment variables

/**
 * Expose
 */

module.exports = {
  app
};

app.use(express.json());



app.use(cors({
  origin: ["http://localhost:3000"],
  methods: ['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS'],
  allowedHeaders: ["Content-Type", "Authorization"],
  credentials: true
}));

// Tangani preflight request secara eksplisit
app.options('*', cors()); 

// Middleware untuk melayani file statis dari direktori "public/uploads"
app.use('/uploads', express.static(path.join(__dirname, 'public/uploads')));

// Tentukan root path proyek
global.appRoot = path.resolve(__dirname);

// Middleware untuk menghidangkan file statis dari folder "assets"
app.use('/assets', express.static(path.join(global.appRoot, 'assets')));

app.use('/assets/bill', express.static(path.join(__dirname, 'assets/bill')));

// app.set('views', path.join(__dirname, 'views'));
// app.set('view engine', 'pug');

// Set up view engine (EJS)
app.set('view engine', 'ejs');
app.set('views', path.join(global.appRoot, 'views'));


app.use(logger('dev'));
app.use(express.json());

app.use(cookieParser());
//app.use(bodyParser.urlencoded({ extended: true }));
//app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, 'public')));
app.use(responseEnhancer());

// Buat folder "assets/qr-code" jika belum ada
const qrCodeDir = path.join(__dirname, 'assets/qr-code');
if (!fs.existsSync(qrCodeDir)) {
  fs.mkdirSync(qrCodeDir);
}

// Endpoint untuk membuat QR Code
app.get('/generate-qrcode', async (req, res) => {
  // const voucher_no = req.params.voucher_no;

  const { voucher_no } = req.query;

  if (!voucher_no) {
    return res.status(400).send('voucher_no parameter is required');
  }

  try {
    // Generate QR code
    const qrCodeDataURL = await QRCode.toDataURL(voucher_no);


    // Remove the data URL prefix to get the base64-encoded image
    const base64Data = qrCodeDataURL.replace(/^data:image\/png;base64,/, '');
    const imgBuffer = Buffer.from(base64Data, 'base64');

    // Tentukan nama file untuk menyimpan QR code
    const fileName = `qrcode_${voucher_no}.png`;
    const filePath = path.join(qrCodeDir, fileName);

    // Tulis buffer ke file
    fs.writeFileSync(filePath, imgBuffer);
    console.log({
      fileName,
      filePath
    })
    // Kirim respons sukses dalam format JSON
    res.status(200).json({
      message: 'QR code generated successfully',
      fileName: fileName,
    });
  } catch (error) {
    console.error(error);
    res.status(500).send('Error generating QR code');
  }
});


app.post('/generate-receipt-pdf', async (req, res) => {

  const voucherInfo = req.body.voucherInfo;  // Get voucher info from request body
  const userLogin = req.body.username;

  try {
    const formatDate = (dateString) => {
      const date = new Date(dateString);
      return date.toLocaleDateString('en-US', {
        year: 'numeric',
        month: 'short',
        day: 'numeric'
      });
    };

    const formatDateTime = (dateString) => {
      const date = new Date(dateString);
      return date.toLocaleString('en-US', {
        year: 'numeric',
        month: 'short',
        day: 'numeric',
        hour: '2-digit',
        minute: '2-digit'
      });
    };

    const pdfDoc = await PDFDocument.create();

    // Embed the Times Roman font
    const helveticaBoldFont = await pdfDoc.embedFont(StandardFonts.HelveticaBold)
    const helveticaNormalFont = await pdfDoc.embedFont(StandardFonts.Helvetica)

    // Add a blank page to the document
    const page = pdfDoc.addPage()

    // Get the width and height of the page
    const { width, height } = page.getSize()

    // Set the title text
    const title = 'Voucher Redemption Receipt';
    const titlefontSize = 20;

    // Calculate the width of the title text
    const titletextWidth = helveticaBoldFont.widthOfTextAtSize(title, titlefontSize);

    // Calculate the x position to center the title
    const titleX = (width - titletextWidth) / 2;

    // Draw a string of text toward the top of the page
    page.drawText('Voucher Redemption Receipt', {
      x: titleX,
      y: height - 4 * titlefontSize,
      size: titlefontSize,
      font: helveticaBoldFont,
      color: rgb(0, 0, 0),
    })

    // Set the subtitle text
    const subtitle = `Voucher Number: ${voucherInfo[0].voucher_no}`;
    const subtitlefontSize = 12;

    // Calculate the width of the title text
    const subtitletextWidth = helveticaNormalFont.widthOfTextAtSize(subtitle, subtitlefontSize);

    // Calculate the x position to center the title
    const subtitleX = (width - subtitletextWidth) / 2;

    // Draw the subtitle text below the title
    const subtitleY = height - 8 * subtitlefontSize;
    page.drawText(subtitle, {
      x: subtitleX,
      y: subtitleY,
      size: subtitlefontSize,
      font: helveticaNormalFont,
      color: rgb(0, 0, 0),
    })

    // Draw a line below the subtitle
    const lineY = subtitleY - 10; // Position the line 10 units below the subtitle
    page.drawLine({
      start: { x: 50, y: lineY }, // Start the line 50 units from the left edge
      end: { x: width - 50, y: lineY }, // End the line 50 units from the right edge
      thickness: 1, // Line thickness
      color: rgb(0, 0, 0), // Line color
    });

    // Fetch image as ArrayBuffer
    const imageUrl = `${process.env.API_HOST}/assets/voucher/voucher_${voucherInfo[0].voucher_no}.png`;
    const response = await axios.get(imageUrl, { responseType: 'arraybuffer' });

    const imageBytes = response.data;

    // Embed the image
    const pngImage = await pdfDoc.embedPng(imageBytes);

    // Get image dimensions
    const pngDims = pngImage.scale(0.5); // Scale down if needed

    // Calculate the Y position for the image just below the line
    const imageY = lineY - pngDims.height - 10; // Position 10 units below the line

    // Draw the image
    page.drawImage(pngImage, {
      x: 10,
      y: imageY,
      width: pngDims.width,
      height: pngDims.height,
    });

    // tampilkan detail informasi voucher
    // Set information details
    const memberInfo = [
      `Member ID: ${voucherInfo[0].member_id}`,
      `Member Name: ${voucherInfo[0].members?.name_on_card}`,
      `Beneficiary: ${voucherInfo[0].beneficiary}`,
      `Expiry Date: ${formatDate(voucherInfo[0].date_expired)}`,
      `Item Name: ${voucherInfo[0].reedem_name}`,
      `Outlet Name: ${voucherInfo[0].outlet_name}`,
      `User Scanner: ${voucherInfo[0].updatedBy}`,
      `Date/Time Scanner: ${formatDateTime(voucherInfo[0].updatedAt)}`,
      `Status Voucher: ${voucherInfo[0].is_used ? 'Used' : 'Not Used'}`,
    ];

    const infoFontSize = 10;
    const lineSpacing = 15; // Space between lines

    // Start position for member info
    let infoY = imageY - lineSpacing;

    // Draw each piece of member info text
    memberInfo.forEach(info => {
      page.drawText(info, {
        x: 50,
        y: infoY,
        size: infoFontSize,
        font: helveticaNormalFont,
        color: rgb(0, 0, 0),
      });
      infoY -= lineSpacing; // Move down for the next line of text
    });

    // Add footer
    const footerY = 50; // Y position for the footer
    const footerLineY = footerY + 10; // Y position for the line
    const footerFontSize = 10;

    // Draw the footer line
    page.drawLine({
      start: { x: 50, y: footerLineY },
      end: { x: width - 50, y: footerLineY },
      thickness: 1,
      color: rgb(0, 0, 0),
    });

    // Add "Printed on" text
    const printedOnText = `Printed on: ${formatDateTime(new Date())}`;
    page.drawText(printedOnText, {
      x: 50,
      y: footerY,
      size: footerFontSize,
      font: helveticaNormalFont,
      color: rgb(0, 0, 0),
    });

    // Add "Printed by" text
    const updatedByText = `Printed by: ${userLogin}`;
    const updatedByTextWidth = helveticaNormalFont.widthOfTextAtSize(updatedByText, footerFontSize);
    page.drawText(updatedByText, {
      x: width - 50 - updatedByTextWidth,
      y: footerY,
      size: footerFontSize,
      font: helveticaNormalFont,
      color: rgb(0, 0, 0),
    });

    // Specify the path to save the file
    const filePath = path.join(__dirname, 'assets/receipt', `receipt_${voucherInfo[0].voucher_no}.pdf`);
    const pdfBytes = await pdfDoc.save();
    fs.writeFileSync(filePath, pdfBytes);

    res.status(200).json({ message: 'PDF generated successfully', filePath: filePath });
  } catch (error) {
    console.log(error)
  }
});

const authenticateToken = (req, res, next) => {
  const authHeader = req.headers['authorization'];

  if (!authHeader) {
    return res.status(401).json({ message: 'Unauthorized: No token provided' });
  }

  const token = authHeader;

  jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, decoded) => {
    //if(err) return res.sendStatus(403);
    //req.username = decoded.username;
    next();
  });
};


//!==============================================
// Konfigurasi multer
const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    const uploadPath = path.join(__dirname, 'assets/bill'); // Tentukan folder tujuan

    // Create the folder if it doesn't exist
    if (!fs.existsSync(uploadPath)) {
      fs.mkdirSync(uploadPath, { recursive: true });
    }

    cb(null, uploadPath);
  },
  filename: (req, file, cb) => {
    // Beri nama file unik dengan timestamp
    const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9);
    const ext = path.extname(file.originalname); // Ekstensi file
    cb(null, `${file.fieldname}-${uniqueSuffix}${ext}`);
  },
});

// Middleware multer
const upload = multer({ 
  storage, 
  limits: { fileSize: 5 * 1024 * 1024 } // Maksimal 5MB
});

app.use(bodyParser.json({ limit: '5mb' }));
app.use(bodyParser.urlencoded({ limit: '5mb', extended: true }));

// Endpoint untuk upload file
app.post('/api/upload', authenticateToken, upload.single('billImage'), (req, res) => {
  try {
    if (!req.file) {
      console.error('No file received');
      return res.status(400).json({ message: 'No file uploaded' });
    }
    const filePath = `/assets/bill/${req.file.filename}`;
    const fileName = req.file.filename;
    res.status(200).json({ message: 'File uploaded successfully', filePath, fileName });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Error uploading file' });
  }
});

//!==============================================

// Bootstrap routes
require('./routes/index.js')(app);

app.listen(port);

console.log('API app started on port ' + port);














