const { now } = require('moment');
const mysql = require('mysql');
const xlsx = require('xlsx');

const db = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "123456",
    database: "muliadb"
})

let workbook = xlsx.readFile('D:/Projects/MP Points as of 03042024.xlsx')
//let workbook = xlsx.readFile('D:/Projects/basedCO.xlsx')
let worksheet = workbook.Sheets[workbook.SheetNames[0]]
let range = xlsx.utils.decode_range(worksheet["!ref"])

console.log(range, ">>>>>> isi range")

for (let row = range.s.r + 1; row <= range.e.r; row++) {
    let data = []

    // Menentukan kolom yang ingin dimasukkan ke dalvam tabel
    let first = worksheet[xlsx.utils.encode_cell({ r: row, c: 0 })]?.v;
    let last = worksheet[xlsx.utils.encode_cell({ r: row, c: 2 })]?.v;
    let title = worksheet[xlsx.utils.encode_cell({ r: row, c: 3 })]?.v;
    let birth_date = convertExcelDateToJSDate(worksheet[xlsx.utils.encode_cell({ r: row, c: 4 })]?.v);
    let address_type = worksheet[xlsx.utils.encode_cell({ r: row, c: 5 })]?.v;
    let city = worksheet[xlsx.utils.encode_cell({ r: row, c: 10 })]?.v;
    let countryid = worksheet[xlsx.utils.encode_cell({ r: row, c: 11 })]?.v;
    let country = worksheet[xlsx.utils.encode_cell({ r: row, c: 12 })]?.v;
    let state = worksheet[xlsx.utils.encode_cell({ r: row, c: 13 })]?.v;
    let province = worksheet[xlsx.utils.encode_cell({ r: row, c: 14 })]?.v;
    let zip_code = worksheet[xlsx.utils.encode_cell({ r: row, c: 15 })]?.v;
    let membership_card_no = worksheet[xlsx.utils.encode_cell({ r: row, c: 16 })]?.v;
    let membership_level = worksheet[xlsx.utils.encode_cell({ r: row, c: 17 })]?.v;
    let joined_date = convertExcelDateToJSDate(worksheet[xlsx.utils.encode_cell({ r: row, c: 18 })]?.v);
    let avail_points_2024 = worksheet[xlsx.utils.encode_cell({ r: row, c: 19 })]?.v;
    let EXP_PTS_2024 = convertExcelDateToJSDate(worksheet[xlsx.utils.encode_cell({ r: row, c: 20 })]?.v); 
    let primary_email = worksheet[xlsx.utils.encode_cell({ r: row, c: 21 })]?.v;
   
    // Memasukkan nilai kolom ke dalam array data
    data.push(first);
    data.push(last);
    data.push(title);
    data.push(birth_date);
    data.push(address_type);
    data.push(city);
    data.push(countryid);
    data.push(country);
    data.push(state);
    data.push(province);
    data.push(zip_code);
    data.push(membership_card_no);
    data.push(membership_level);
    data.push(joined_date);
    data.push(EXP_PTS_2024);
    data.push(avail_points_2024);
    data.push(primary_email);
   

    // console.log(data)

    // insert to table
    let sql = "INSERT INTO acc_summary_temp(first,last,title,birth_date,address_type,city,countryid,country,state,province,zip_code,membership_card_no,membership_level,joined_date,EXP_PTS_2024,avail_points_2024,primary_email) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"
    db.query(sql, data, (err, results, fields) => {
        if (err) {
            return console.error(err.message)
        }
        console.log("Member ID:" + results.insertId)
    })
}

// // Tutup koneksi ke database setelah selesai
db.end();

// Fungsi untuk mengonversi tanggal Excel ke tipe data tanggal JavaScript
function convertExcelDateToJSDate(excelDate) {
    // Konversi nilai Excel ke tipe data tanggal JavaScript
    // return new Date((excelDate - (25567 + 1)) * 86400 * 1000); // Sesuai dengan formula konversi tanggal Excel ke JavaScript
    let jsDate = new Date((excelDate - 25569) * 86400 * 1000);

    // Mendapatkan komponen tanggal, bulan, dan tahun dari tanggal JavaScript
    let year = jsDate.getFullYear();
    let month = (jsDate.getMonth() + 1).toString().padStart(2, '0');
    let day = jsDate.getDate().toString().padStart(2, '0');

    // Menggabungkan komponen tanggal, bulan, dan tahun dalam format yang diinginkan
    let formattedDate = `${year}-${month}-${day}`;

    return formattedDate;
}

