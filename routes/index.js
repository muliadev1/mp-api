'use strict';
const { Master } = require('../db/models/index');
const jwt = require('jsonwebtoken');
const refreshToken = require('../controllers/RefreshToken');
const verifyToken = require('../middleware/VerifyToken');
const fs = require('fs');
const { Sequelize } = require('sequelize');
const upload = require('../middleware/upload');
// const { generateQRCode } = require('../middleware/qrCodeGenerator')

const mysql = require('mysql')
const multer = require('multer')
const path = require('path');
const csv = require('fast-csv');
const util = require('util');


const excelController = require("../controllers/excel.controller");
const csvController = require("../controllers/csv.controller");
const HomeController = require('../controllers/Home');
const UserController = require('../controllers/Users');
const LanguageController = require('../controllers/Languages');

const ExtrapointController = require('../controllers/Extrapoints');
const ExtracreditController = require('../controllers/Extracredits');

const RoomtypeController = require('../controllers/Roomtypes');

const PageController = require('../controllers/Pages');
const MemberController = require('../controllers/Members');
const CompanyController = require('../controllers/Companies');
const MemberLogController = require('../controllers/MemberLogs');
const MasterController = require('../controllers/Masters');
const ReedemController = require('../controllers/Reedems');
const ReedemPointController = require('../controllers/ReedemPoints');
const ReedemLangController = require('../controllers/ReedemLang');
const ExclusiveOffersController = require('../controllers/ExclusiveOffers');
const OfferLangController = require('../controllers/OfferLang')
const ReservationController = require('../controllers/Reservations')
const RoleController = require('../controllers/Roles');
const RoleMenuController = require('../controllers/RoleMenus');
const ModuleController = require('../controllers/Modules');
const MenuController = require('../controllers/Menus');
const MissingPointController = require('../controllers/MissingPointEmail');

const DashboardController = require('../controllers/Dashboards');
const EmailController = require('../controllers/Emails');
const EmailMultiController = require('../controllers/EmailsMulti');

const OutletController = require('../controllers/Outlets');
const DiningController = require('../controllers/Dinings');
const SettingController = require('../controllers/Settings');
const QrCodeController = require('../controllers/QrCodes');

const PointController = require('../controllers/Points');
const BenefitController = require('../controllers/Benefits');
const AdjustmentController = require('../controllers/Adjustments');
const RedemtypeController = require('../controllers/Redemtypes');
const ContactController = require('../controllers/Contacts');
const TagController = require('../controllers/Tags');
const PointtypeController = require('../controllers/Pointtype');

module.exports = function (app) {
    // home
    app.get('/', HomeController.index);
    // users
    app.get('/users', verifyToken, UserController.getUsers);
    app.get('/users/list-account', verifyToken, UserController.getListAccount);
    app.post('/users', UserController.createUser);
    app.get('/users/:id', verifyToken, UserController.detailUser);
    app.put('/users/:id', verifyToken, UserController.updateUser);
    app.delete('/users/:id', verifyToken, UserController.deleteUser);

    app.post('/login', UserController.Login);
    app.post('/login-email', UserController.LoginEmail);
    app.get('/token', refreshToken);
    app.delete('/logout', UserController.Logout);

    // qr code
    app.get('/qrcodes', verifyToken, QrCodeController.getVouchers);
    app.get('/qrcodes/:voucherNo', verifyToken, QrCodeController.getVoucher);
    app.put('/qrcodes/update', verifyToken, QrCodeController.updateIsUsed);
    app.put('/qrcodes/beneficiary/:id', verifyToken, QrCodeController.updateVoucher);


    //languages
    app.get('/languages', verifyToken, LanguageController.getLanguages);
    app.post('/languages', verifyToken, LanguageController.createLanguage);
    app.get('/languages/:id', verifyToken, LanguageController.detailLanguage);
    app.put('/languages/:id', verifyToken, LanguageController.updateLanguage);
    app.delete('/languages/:id', verifyToken, LanguageController.deleteLanguage);

    //tags
    app.get('/tags', verifyToken, TagController.getTags);
    app.post('/tags', verifyToken, TagController.createTag);
    app.get('/tags/:id', verifyToken, TagController.detailTag);
    app.put('/tags/:id', verifyToken, TagController.updateTag);
    app.delete('/tags/:id', verifyToken, TagController.deleteTag);

    app.get('/tagstranslations', verifyToken, TagController.getTranslations);
    app.post('/tagstranslations', verifyToken, TagController.upInsertTranslations);

    // redemtype
    app.get('/redemtypes', verifyToken, RedemtypeController.getRedemtypes);
    app.post('/redemtypes', verifyToken, RedemtypeController.createRedemtype);
    app.get('/redemtypes/:id', verifyToken, RedemtypeController.detailRedemtype);
    app.put('/redemtypes/:id', verifyToken, RedemtypeController.updateRedemtype);
    app.delete('/redemtypes/:id', verifyToken, RedemtypeController.deleteRedemtype);

    //pointtypes
    app.get('/pointtypes', verifyToken, PointtypeController.getPointtypes);

    // extra points
    app.get('/extrapoints', verifyToken, ExtrapointController.getExtrapoints);
    app.post('/extrapoints', verifyToken, ExtrapointController.createExtrapoint);
    app.get('/extrapoints/:id', verifyToken, ExtrapointController.detailExtrapoint);
    app.put('/extrapoints/:id', verifyToken, ExtrapointController.updateExtrapoint);
    app.delete('/extrapoints/:id', verifyToken, ExtrapointController.deleteExtrapoint);

    // extra credits
    app.get('/extracredits', verifyToken, ExtracreditController.getExtracredits);
    app.post('/extracredits', verifyToken, ExtracreditController.createExtracredit);
    app.get('/extracredits/:id', verifyToken, ExtracreditController.detailExtracredit);
    app.put('/extracredits/:id', verifyToken, ExtracreditController.updateExtracredit);
    app.delete('/extracredits/:id', verifyToken, ExtracreditController.deleteExtracredit);

    // roomtypes
    app.get('/roomtypes', verifyToken, RoomtypeController.getRoomtypes);

    
    //pages
    app.get('/pages', verifyToken, PageController.getPages);
    app.get('/pages-menus', verifyToken, PageController.getPagesMenus);
    app.post('/pages', verifyToken, PageController.createPage);
    app.get('/pages/content', verifyToken, PageController.getPageContents);
    app.get('/pages/slug/:slug', verifyToken, PageController.detailPageSlug);
    app.get('/pages/:id', verifyToken, PageController.detailPage);
    app.put('/pages/:id', verifyToken, PageController.updatePage);
    app.delete('/pages/:id', verifyToken, PageController.deletePage);

    // PAGES LANGS
    app.get('/pages/langs/:language', verifyToken, PageController.getPagesLangs);
    app.get('/pages/:id/langs/:language', verifyToken, PageController.detailPageLang);
    app.put('/pages/:id/langs/:language', verifyToken, PageController.updatePageLang);
    app.post('/pages/langs/:language', verifyToken, PageController.createPageLang);
    app.delete('/pages/langs/:id', verifyToken, PageController.deletePageLang);


    //page contents
    app.get('/pages/content/:id', verifyToken, PageController.detailPageContent);
    app.get('/pages/content/:pageId/:langId', verifyToken, PageController.detailPageContentByPageLang);
    app.post('/pages/content/:pageId/:langId', verifyToken, PageController.createPageContent);
    app.put('/pages/content/:contentId/:langId', verifyToken, PageController.updatePageContent);
    app.delete('/pages/content/:id', verifyToken, PageController.deletePageContent);

    //PAGECONTENT LANGS
    app.get('/pages/content-langs/:language', verifyToken, PageController.getPageContentsLang);
    app.get('/pages/:id/content-langs/:language', verifyToken, PageController.detailPageContentLang);
    app.post('/pages/content-langs/:language', verifyToken, PageController.createPageContentLang);
    app.put('/pages/:id/content-langs/:language', verifyToken, PageController.updatePageContentLang);
    app.delete('/pages/content-langs/:id', verifyToken, PageController.deletePageContentLang);


    //page terms
    app.get('/pages/term/:pageId/:langId', verifyToken, PageController.getPageTerm);
    app.post('/pages/term/:pageId/:langId', verifyToken, PageController.createPageTerm);
    app.put('/pages/term/:pageId/:langId', verifyToken, PageController.updatePageTerm);
    app.delete('/pages/term/:pageId/:langId', verifyToken, PageController.deletePageTerm);

    //page faqs
    app.get('/pages/faq/:pageId/:langId', verifyToken, PageController.getPageFaq);
    app.post('/pages/faq/:pageId/:langId', verifyToken, PageController.createPageFaq);
    app.put('/pages/faq/:pageId/:langId', verifyToken, PageController.updatePageFaq);
    app.delete('/pages/faq/:pageId/:langId', verifyToken, PageController.deletePageFaq);

    //enrolls logs
    app.post('/members/enrolllogs', verifyToken, MemberController.createEnrollLogs);
    app.post('/members/2ndenrolllogs', verifyToken, MemberController.create2ndEnrollLogs);
    app.get('/members/enrolllogs/totalpoints/:memberId', verifyToken, MemberController.getTotalPointsByMemberId);
    app.post('/members/upgradelogs', verifyToken, MemberController.createUpgradeLogs);
    
    // point daily transaction
    app.post('/members/dailytransaction', verifyToken, MemberController.createDailyTransaction);
    app.put('/members/dailytransaction/:currentDate', verifyToken, MemberController.updateDailyTransaction);
    app.put('/members/dailytransaction/updateinitbalance/:id', verifyToken, MemberController.updateInitBalance);

    // adjustments
    app.get('/adjustments/pointadjustments/:memberId', verifyToken, AdjustmentController.getPointAdjustment);
    app.post('/adjustments/create-adjustment-point/:memberId', verifyToken, AdjustmentController.createAdjustmentPoint);
    app.put('/adjustments/update-member-points/:memberId', verifyToken, AdjustmentController.updateMemberPoints);
    app.get('/adjustments/list-adjustment', verifyToken, AdjustmentController.getAdjustments);

    //membertypes    
    app.get('/membertypes', verifyToken, MemberController.getMembertypes);

    // companies
    app.get('/companies', verifyToken, CompanyController.getCompanies);
    app.post('/companies', verifyToken, CompanyController.createCompany);
    app.get('/list-companies', verifyToken, CompanyController.getListCompanies);
    app.get('/companies/getCompany/:id', verifyToken, CompanyController.getCompany);
    app.put('/companies/updatecompany/:id', verifyToken, CompanyController.updateCompany);
    app.delete('/companies/:id', verifyToken, CompanyController.deleteCompany);

    //contacts
    app.get('/contacts/getbycompany/:companyId', verifyToken, ContactController.getContactsByCompany);
    app.post('/contacts', verifyToken, ContactController.createContact);
    app.put('/contacts/:id', verifyToken, ContactController.updateContact);

    //members
    app.get('/members/enrollMember', verifyToken, MemberController.getEnrollMember);
    app.post('/members', verifyToken, MemberController.createMember);
    app.post('/fb-member', verifyToken, MemberController.createFbMember);
    app.post('/members/create-member-logs/:memberId', verifyToken, MemberController.createMemberLogs);
    app.get('/members', verifyToken, MemberController.getMembers);
    app.get('/members/points', verifyToken, MemberController.getMembershipPoints);
    app.get('/members/selects', verifyToken, MemberController.getSelectMembers);
    app.get('/members/level', verifyToken, MemberController.getMembersLevel);
    app.get('/members/level-detail/:memberId', verifyToken, MemberController.getMembersLevelDetail);
    app.get('/members/tiers', verifyToken, MemberController.getListTierProperty);
    app.get('/members/detailtiers', verifyToken, MemberController.getListDetailTierProperty);
    app.get('/members/passwords', verifyToken, MemberController.getPasswords);
    app.post('/members/login', MemberController.Login);
    app.get('/members/enrolls', verifyToken, MemberController.getListEnroll);
    app.get('/members/list-points', verifyToken, MemberController.getListPoints);
    app.get('/members/expirydate', verifyToken, MemberController.getListExpiryDate);
    app.get('/members/birthday', verifyToken, MemberController.getListBirthday);
    app.get('/members/checkpointtransaction', verifyToken, MemberController.getPointTransaction);

    app.get('/members/summarydailytransaction', verifyToken, MemberController.getSummaryDailyTransaction);
    app.get('/members/summarypointcalculation', verifyToken, MemberController.getSummaryPointCalculation);

    app.get('/members/returnback', verifyToken, MemberController.getMemberReturnBack);
    app.get('/members/summarypoints', verifyToken, MemberController.getSummaryPoints);

    app.get('/members/summaryenrolls', verifyToken, MemberController.getSummaryEnrolls);
    app.get('/members/detailnewenrolls', verifyToken, MemberController.getDetailNewEnrolls);
    app.get('/members/detailexistingenrolls', verifyToken, MemberController.getDetailExistingEnrolls);
    app.get('/members/summarypurchase', verifyToken, MemberController.getSummaryPurchase);
    app.get('/members/detailpurchase', verifyToken, MemberController.getDetailPurchase);
    app.get('/members/summarybonus', verifyToken, MemberController.getSummaryBonus);
    app.get('/members/detailbonus', verifyToken, MemberController.getDetailBonus);
    app.get('/members/summarydining', verifyToken, MemberController.getSummaryDining);
    app.get('/members/detaildining', verifyToken, MemberController.getDetailDining);
    app.get('/members/summaryredeem', verifyToken, MemberController.getSummaryRedeem);
    app.get('/members/detailredeem', verifyToken, MemberController.getDetailRedeem);

    app.get('/members/summaryreturningguest', verifyToken, MemberController.getSummaryReturningGuest);
    app.get('/members/detailreturningguest', verifyToken, MemberController.getDetailReturningGuest);
    app.get('/members/summaryreturningguestreport', verifyToken, MemberController.getSummaryReturningGuestReport);

    app.get('/members/visitinterval', verifyToken, MemberController.getVisitInterval);
    app.get('/members/returningguestinterval', verifyToken, MemberController.getReturningGuestInterval);

    app.get('/members/initbalance', verifyToken, MemberController.getInitBalance);
    app.get('/members/openbalance', verifyToken, MemberController.getOpenBalance);
    app.get('/members/closebalance', verifyToken, MemberController.getCloseBalance);
    app.get('/members/redemption', verifyToken, MemberController.getListRedemption);
    app.get('/members/redemption-manual', verifyToken, MemberController.getListRedemptionManual);
    app.get('/members/dining', verifyToken, MemberController.getListDining);
    app.get('/members/voucher', verifyToken, MemberController.getListVoucher);
    app.get('/members/vouchersused', verifyToken, MemberController.getVouchersUsed);
    app.put('/members/change-password/:id', MemberController.changePassword);
    app.get('/members/:id', verifyToken, MemberController.getProfile);
    app.put('/members/:id', verifyToken, MemberController.updateProfile);
    app.put('/members/deleted/:id', verifyToken, MemberController.deleteMember);

    app.get('/members/getmember/:id', verifyToken, MemberController.getMember);
    app.put('/members/updatemember/:id', verifyToken, MemberController.updateMember);
    app.put('/members/updatepassword/:id', verifyToken, MemberController.updatePassword);
    app.put('/members/generatepassword/:email', verifyToken, MemberController.generatePassword);
    app.put('/members/generatepasswordFb/:email', verifyToken, MemberController.generatePasswordFb);
    app.put('/members/initpoints/:memberId', verifyToken, MemberController.initPoints);
    app.put('/members/updatepoints/:memberId', verifyToken, MemberController.updatePoints);
    app.put('/members/updateexpdate/:memberId', verifyToken, MemberController.updateExpDate);
    app.put('/members/updatebirthdate/:memberId', verifyToken, MemberController.updateBirthDate);
    app.put('/members/adjustpoints/:memberId', verifyToken, MemberController.adjustPoints);
    app.put('/members/updatetotalpoints/:memberId', verifyToken, MemberController.updateTotalPoints);
    app.put('/members/updatelevelpoints/:memberId', verifyToken, MemberController.updateLevelPoints);
    app.put('/members/replacepoints/:memberId', verifyToken, MemberController.replacePoints);
    app.put('/members/updatereedempoints/:memberId', verifyToken, MemberController.updateReedemPoints);
    app.put('/members/updatenotif/:memberId', verifyToken, MemberController.updateNotif);
    app.put('/members/cancel-reedem/:id', verifyToken, MemberController.cancelReedemPoints);
    app.put('/members/cancel-voucher/:id', verifyToken, MemberController.cancelVoucher);
    app.put('/members/updateisfirstenroll/:memberId', verifyToken, MemberController.updateIsFirstEnroll);

    app.post('/members/insertpointlogs', verifyToken, MemberController.createPointLogs);
    app.put('/members/updatenlolllogs/:confNo', verifyToken, MemberController.updateEnrollLogs);

    app.get('/members/select-list-member/:filter', verifyToken, MemberController.getListMembersByName);

    app.get('/members/profile/:memberId', verifyToken, MemberController.getProfileByMemberId);
    app.get('/members/check-status/:memberId', verifyToken, MemberController.getStatusByMemberId);
    app.post('/members/reward-claim', verifyToken, MemberController.rewardClaim);
    app.post('/members/reward-claim-multi', verifyToken, MemberController.rewardClaimMulti);
    app.post('/members/reward-claim-items', verifyToken, MemberController.rewardClaimItems);
    app.get('/members/reedem-points/:memberId', verifyToken, MemberController.getReedemPoints);
    app.get('/members/check-email/:email', verifyToken, MemberController.getProfileByEmail);
    app.get('/members/:firstName/:lastName', verifyToken, MemberController.getProfileByName);

    app.get('/members/scanners', verifyToken, MemberController.getListScanner);

    app.get('/members/check-voucher-used-status/:memberId/:redemPointId', verifyToken, MemberController.getVoucherUsedStatus);

    app.put('/members/reset-points/:memberId', verifyToken, MemberController.resetPointByMemberId);
    app.get('/memberlogs/points/:memberId', verifyToken, MemberLogController.getPointLogs);

    // members WCC
    app.get('/members-wcc', MemberController.getMembersWcc);
    app.get('/members-wcc/:memberId', MemberController.getDetailMemberWcc);
    
    //masters
    app.get('/masters', verifyToken, MasterController.getMasters);
    app.get('/masters/enroll-member', verifyToken, MasterController.getEnrolls);
    app.get('/masters/opera-member', verifyToken, MasterController.getOperas);
    app.get('/masters/needpoint-member', verifyToken, MasterController.getNeedPoint);
    app.get('/masters/autopoint-member', verifyToken, MasterController.getAutoPoint);
    app.get('/masters/autopoint-wcc-member', verifyToken, MasterController.getAutoWccPoint);
    app.get('/masters/needextend-member', verifyToken, MasterController.getNeedExtend);
    app.get('/masters/list-master', verifyToken, MasterController.getListMaster);
    app.get('/masters/laststay', verifyToken, MasterController.getListLastStay);
    app.get('/masters/detail-member', verifyToken, MasterController.getDetailMember);
    app.get('/masters/detail-member-opera', verifyToken, MasterController.getDetailMemberOpera);
    app.get('/masters/detail-member-transaction', verifyToken, MasterController.getDetailMemberTrx);
    app.put('/masters/update-member/:confno', verifyToken, MasterController.updateMemberByConfNo);
    app.put('/masters/update-wcc-member/:confno', verifyToken, MasterController.updateWCCMemberByConfNo);
    app.put('/masters/update-master/:email', verifyToken, MasterController.updateMemberByEmail);
    app.get('/masters/total-points/:confno/:region/:level', verifyToken, MasterController.getTotalPointsByConfNo);
    app.get('/masters/calculate-points/:confno/:memberId', verifyToken, MasterController.calculateTotalPoints);
    app.put('/masters/release-points/:confno', verifyToken, MasterController.releasePointByConfNo);
    app.post('/masters/initpoints', verifyToken, MasterController.initPoints);
    app.get('/masters/check-point-logs/:confNo', verifyToken, MasterController.getPointLogsByConfNo);
    app.get('/masters/departure_date/:deptDate/:region', verifyToken, MasterController.getDepartureDate);
    

    // upload csv file
    app.post("/import-csv", verifyToken, csvController.uploadCsv);

    // upload csv file
    app.post("/import-excel", verifyToken, excelController.uploadExcel);

    
    //Reedem
    app.get('/reedems', verifyToken, ReedemController.getReedems);
    app.get('/reedems/catalogues', verifyToken, ReedemController.getCatalogues);
    app.get('/reedems-frontend', verifyToken, ReedemController.getReedemsFrontend);
    app.get('/redeemptions', verifyToken, ReedemController.getRedeemptions);
    app.post('/reedems', verifyToken, ReedemController.createReedem);
    app.get('/reedems/:id', verifyToken, ReedemController.detailReedem);
    app.put('/reedems/:id', verifyToken, ReedemController.updateReedem);
    app.delete('/reedems/:id', verifyToken, ReedemController.deleteReedem);

    //REDEEMLANG
    app.get('/reedems/langs/:language', verifyToken, ReedemController.getReedemsLang);
    app.get('/reedems/:id/langs/:language', verifyToken, ReedemController.detailReedemLang);
    app.put('/reedems/:id/langs/:language', verifyToken, ReedemController.updateReedemLang);
    app.post('/reedems/langs/:language', verifyToken, ReedemController.createReedemLang);
    app.delete('/reedems/langs/:id', verifyToken, ReedemController.deleteReedemLang);

    //reedemptions
    app.get('/reedemslang/:langId', verifyToken, ReedemLangController.getReedemLangs);
    app.get('/reedemslang/detail/:reedemId/:langId', verifyToken, ReedemLangController.detailReedemLang);
    app.get('/reedemslang/byid/:id', verifyToken, ReedemLangController.byIdReedemLang);
    app.post('/reedemslang/:reedemId/:langId', verifyToken, ReedemLangController.createReedemLang);
    app.put('/reedemslang/:reedemId/:langId', verifyToken, ReedemLangController.updateReedemLang);
    // app.delete('/reedemslang/:reedemId/:langId',verifyToken,  ReedemLangController.deleteReedemLang);
    app.delete('/reedemslang/delete/:id', verifyToken, ReedemLangController.deleteReedemLangById);

    //ExclusiveOffer
    app.get('/exclusiveoffers', verifyToken, ExclusiveOffersController.getExclusiveOffers);
    app.get('/exclusiveoffers-frontend', verifyToken, ExclusiveOffersController.getExclusiveOffersFrontend);
    app.post('/exclusiveoffers', verifyToken, ExclusiveOffersController.createExclusiveOffers);
    app.get('/exclusiveoffers/:id', verifyToken, ExclusiveOffersController.detailExclusiveOffers);
    app.put('/exclusiveoffers/:id', verifyToken, ExclusiveOffersController.updateExclusiveOffers);
    app.delete('/exclusiveoffers/:id', verifyToken, ExclusiveOffersController.deleteExclusiveOffers);

    app.get('/exclusiveoffers/destination/:destId', verifyToken, ExclusiveOffersController.getExclusiveOffersByDestination);


    //ExclusiveOfferLang
    app.get('/exclusiveoffers/langs/:language', verifyToken, ExclusiveOffersController.getExclusiveOffersLang);
    app.get('/exclusiveoffers/:id/langs/:language', verifyToken, ExclusiveOffersController.detailExclusiveOffersLang);
    app.put('/exclusiveoffers/:id/langs/:language', verifyToken, ExclusiveOffersController.updateExclusiveOffersLang);
    app.post('/exclusiveoffers/langs/:language', verifyToken, ExclusiveOffersController.createExclusiveOffersLang);
    app.delete('/exclusiveoffers/langs/:id', verifyToken, ExclusiveOffersController.deleteExclusiveOffersLang);


    //offerlang
    app.get('/offerlang/:langId', verifyToken, OfferLangController.getOfferLang);
    // app.get('/offerlang/detail/:offerId/:langId',verifyToken, OfferLangController.detailOfferLang);
    app.get('/offerlang/detail/:Id', verifyToken, OfferLangController.getOfferLangById);
    app.post('/offerlang/:offerId/:langId', verifyToken, OfferLangController.createOfferLang);
    app.put('/offerlang/:offerId/:langId', verifyToken, OfferLangController.updateOfferLang);
    // app.delete('/offerlang/:offerId/:langId',verifyToken,  OfferLangController.deleteOfferLang);
    app.delete('/offerlang/:id', verifyToken, OfferLangController.deleteOfferLang);


    //profile
    app.get('/profile/myreservations/:memberId', verifyToken, ReservationController.getResevByMemberId);
    app.get('/profile/mypoints/:memberId', verifyToken, ReservationController.getPointsByMemberId);
    app.get('/profile/mypoints/detail/:confNo', verifyToken, ReservationController.getPointDetailByConfNo);
    app.get('/profile/myinitpoints/:memberId', verifyToken, ReservationController.getInitPointsByMemberId);
    app.get('/profile/mysecondpoints/:memberId', verifyToken, ReservationController.getSecondPointsByMemberId);
    app.get('/profile/total-points/:memberId', verifyToken, ReservationController.getTotalPointsByMemberId);
    app.get('/profile/list-total-points', verifyToken, ReservationController.getTotalPoints);
    app.get('/profile/upgradepoints/:memberId', verifyToken, ReservationController.getUpgradePointsByMemberId);

    // reedem ponts
    app.get('/reedempoints', verifyToken, ReedemPointController.getRedeemPoints);
    app.get('/reedempoints/pending-approve', verifyToken, ReedemPointController.getRedemPendingApprove);
    app.get('/reedempoints/mypoints/detail/:id', verifyToken, ReedemPointController.getReedemPointDetailById);
    app.post('/reedempoints/reedempointlogs', verifyToken, ReedemPointController.createReedemPointLogs);
    app.put('/reedempoints/reedempoint/:reedemId', verifyToken, ReedemPointController.updateReedemPoint);
    app.get('/reedempoints/totalpoints/:memberId', verifyToken, ReedemPointController.getTotalReedemPoints);
    app.get('/reedempoints/get-latest-id', verifyToken, ReedemPointController.getLatestReedemPoints);
    app.put('/reedempoints/set-is-loyalty/:id', verifyToken, ReedemPointController.updateIsLoyalty);
    app.put('/reedempoints/set-is-revenue/:id', verifyToken, ReedemPointController.updateIsRevenue);
    app.put('/reedempoints/set-is-financial/:id', verifyToken, ReedemPointController.updateIsFinancial);
    app.put('/reedempoints/set-is-resident/:id', verifyToken, ReedemPointController.updateIsResident);

    app.put('/reedempoints/set-is-surcharge-loyalty/:id', verifyToken, ReedemPointController.updateIsSurchargeLoyalty);
    app.put('/reedempoints/set-is-surcharge-revenue/:id', verifyToken, ReedemPointController.updateIsSurchargeRevenue);
    app.put('/reedempoints/set-is-surcharge-financial/:id', verifyToken, ReedemPointController.updateIsSurchargeFinancial);
    app.put('/reedempoints/set-is-surcharge-resident/:id', verifyToken, ReedemPointController.updateIsSurchargeResident);

    app.delete('/reedempoints/:id', verifyToken, ReedemPointController.deleteRedeemPoint);

    // redem point items
    app.get('/reedempointitems/:id', verifyToken, ReedemPointController.getReedemPointItemsById);
    app.put('/redeempointitems/beneficiary/:id', verifyToken, ReedemPointController.updateIsSurchargeResident);


    //missing point email
    app.get('/missingpointemail', verifyToken, MissingPointController.getPointEmail);
    app.post('/missingpointemail', verifyToken, MissingPointController.createPointEmail);
    app.get('/missingpointemail/:id', verifyToken, MissingPointController.getPointEmailById);
    app.put('/missingpointemail/:id', verifyToken, MissingPointController.updatePointEmail);
    app.delete('/missingpointemail/:id', verifyToken, MissingPointController.deletePointEmail);

    // Loyalty setup
    app.get('/settings', verifyToken, SettingController.getSettings);
    app.get('/settings/:id', verifyToken, SettingController.detailSetting);
    app.put('/settings/:id', verifyToken, SettingController.updateSetting);

    // Benefit setup
    app.get('/benefits/:id', verifyToken, BenefitController.detailBenefit);
    app.put('/benefits/:id', verifyToken, BenefitController.updateBenefit);

    // BENEFIT LANGS
    app.get('/benefits/:id/langs/:language', verifyToken, BenefitController.detailBenefitLang);
    app.put('/benefits/:id/langs/:language', verifyToken, BenefitController.updateBenefitLang);

    //roles
    app.get('/roles', verifyToken, RoleController.getRoles);
    app.post('/roles', verifyToken, RoleController.createRole);
    app.get('/roles/:id', verifyToken, RoleController.detailRole);
    app.put('/roles/:id', verifyToken, RoleController.updateRole);
    app.delete('/roles/:id', verifyToken, RoleController.deleteRole);

    //modules
    app.get('/modules', verifyToken, ModuleController.getModules);
    app.post('/modules', verifyToken, ModuleController.createModule);
    app.get('/modules/:id', verifyToken, ModuleController.detailModule);
    app.put('/modules/:id', verifyToken, ModuleController.updateModule);
    app.delete('/modules/:id', verifyToken, ModuleController.deleteModule);

    //menus
    app.get('/menus', verifyToken, MenuController.getMenus);
    app.get('/menus/module/:id', verifyToken, MenuController.getMenusByModule);
    app.post('/menus', verifyToken, MenuController.createMenu);
    app.get('/menus/:id', verifyToken, MenuController.detailMenu);
    app.put('/menus/:id', verifyToken, MenuController.updateMenu);
    app.delete('/menus/:id', verifyToken, MenuController.deleteMenu);
    app.get('/menus/roles/:roleId/:moduleId/:menuId', verifyToken, MenuController.checkRoleMenu);

    //Role Menus
    app.get('/role-menus/:roleId', verifyToken, RoleMenuController.getRoleMenus);
    app.post('/role-menus/:roleId', verifyToken, RoleMenuController.createRoleMenu);
    app.get('/role-menus/detail/:id', verifyToken, RoleMenuController.detailRoleMenu);
    app.put('/role-menus/update/:id', verifyToken, RoleMenuController.updateRoleMenu);
    app.delete('/role-menus/delete/:id', verifyToken, RoleMenuController.deleteRoleMenu);

    //emails
    app.put('/set-active-member', EmailController.activeMember);
    app.post('/activation-account', EmailController.activationAccount);
    app.post('/activation-account-fb', EmailController.activationAccountFb);
    app.post('/welcome-member', EmailController.welcomeMember);
    app.post('/reset-password', EmailController.resetPassword);
    app.post('/redemption-request', EmailController.redemptionRequest);
    app.post('/surcharge-request', EmailController.surchargeRequest);
    app.post('/voucher-redemption', EmailController.voucherRedemption);
    app.post('/missing-request', EmailController.missingRequest);
    app.post('/enroll-notification', verifyToken, EmailController.enrollNotifications);
    app.post('/send-email-revenue', EmailController.sendEmailToRevenue);
    app.post('/send-email-financial', EmailController.sendEmailToFinancial);
    app.post('/send-email-resident', EmailController.sendEmailToResident);
    app.post('/send-member-voucher', EmailController.sendVoucherToMember);

    app.post('/send-email-surcharge-revenue', EmailController.sendEmailSurchargeToRevenue);
    app.post('/send-email-surcharge-financial', EmailController.sendEmailSurchargeToFinancial);
    app.post('/send-email-surcharge-resident', EmailController.sendEmailSurchargeToResident);
    app.post('/send-email-surcharge-member', EmailController.sendEmailSurchargeToMember);

    // emails multi
    app.post('/send-email-revenue-multi', EmailMultiController.sendEmailToRevenue);
    app.post('/send-email-financial-multi', EmailMultiController.sendEmailToFinancial);
    app.post('/send-email-resident-multi', EmailMultiController.sendEmailToResident);
    app.post('/send-member-voucher-multi', EmailMultiController.sendVoucherToMember);

    app.post('/send-email-surcharge-revenue-multi', EmailMultiController.sendEmailSurchargeToRevenue);
    app.post('/send-email-surcharge-financial-multi', EmailMultiController.sendEmailSurchargeToFinancial);
    app.post('/send-email-surcharge-resident-multi', EmailMultiController.sendEmailSurchargeToResident);
    app.post('/send-email-surcharge-member-multi', EmailMultiController.sendVoucherSurchargeToMember);

    app.post('/test-email', EmailController.testmail);

    // EMAILS MULTI
    app.post('/redemption-request-multi', EmailMultiController.redemptionRequestMulti);

    // GENERATE VOUCHER MANUAL
    app.post('/generate-voucher-manual', EmailController.generateVoucherManual);  // execute directly in postman
    // UPDATE VOUCHER WITHOUT SEND EMAIL TO MEMBER
    app.post('/recreate-voucher', EmailController.RecreateVoucher);  
    

    //MUlia Dashboard
    // Month To Date (MTD)
    app.get('/mtd/testapi', verifyToken, DashboardController.testApi)
    app.get('/mtd/dailybookings', verifyToken, DashboardController.getMtdDailyBooking);
    app.get('/mtd/topcountries', verifyToken, DashboardController.getMtdTopCountries);
    app.get('/mtd/toplengthstay', verifyToken, DashboardController.getMtdTopLengthStay);
    app.get('/mtd/topbookleadtime', verifyToken, DashboardController.getMtdTopBookingLeadTime);
    app.get('/mtd/topmarketsegment', verifyToken, DashboardController.getMtdTopMarketSegment);
    app.get('/mtd/topsupporters', verifyToken, DashboardController.getMtdTopSupporters);
    app.get('/mtd/summary', verifyToken, DashboardController.getMtdSummary);

    // Year To Date (YTD)
    app.get('/ytd/monthlybookings', verifyToken, DashboardController.getYtdMonthlyBooking);
    app.get('/ytd/topcountries', verifyToken, DashboardController.getYtdTopCountries);
    app.get('/ytd/toplengthstay', verifyToken, DashboardController.getYtdTopLengthStay);
    app.get('/ytd/topbookleadtime', verifyToken, DashboardController.getYtdTopBookingLeadTime);
    app.get('/ytd/topmarketsegment', verifyToken, DashboardController.getYtdTopMarketSegment);
    app.get('/ytd/topsupporters', verifyToken, DashboardController.getYtdTopSupporters);
    app.get('/ytd/summary', verifyToken, DashboardController.getYtdSummary);

    // Forecast
    app.get('/forecast', verifyToken, DashboardController.getForeCast);
    app.get('/forecast/monthlybookings', verifyToken, DashboardController.getForecastMonthlyBooking);
    app.get('/forecast/pickupselectedyear', verifyToken, DashboardController.getForecastPickupSelectedYear);
    app.get('/forecast/pickupprevselectedyear', verifyToken, DashboardController.getForecastPickupPreviousSelectedYear);
    app.get('/forecast/summary', verifyToken, DashboardController.getForecastSummary);

    // Group
    app.get('/group/prospect', verifyToken, DashboardController.getProspect);
    app.get('/group/dailyactdeften', verifyToken, DashboardController.getDailyActDefTen);
    app.get('/group/dailyactdeftengroup', verifyToken, DashboardController.getDailyActDefTenGroup);
    app.get('/group/monthlyprospect', verifyToken, DashboardController.getMonthlyProspect);
    app.get('/group/monthlyactdeften', verifyToken, DashboardController.getMonthlyActDefTen);

    //WHL (YTD)
    app.get('/whl/mtd/dailybookings', verifyToken, DashboardController.getWhlMtdDailyBooking);
    app.get('/whl/ytd/dailybookings', verifyToken, DashboardController.getWhlDailyBooking);
    app.get('/whl/topsegmentations', verifyToken, DashboardController.getWhlTopSegmentation);
    app.get('/whl/toplengthstay', verifyToken, DashboardController.getWhlTopLengthStay);
    app.get('/whl/topbookingleadtime', verifyToken, DashboardController.getWhlTopBookingLeadTime);
    app.get('/whl/toproomtypes', verifyToken, DashboardController.getWhlTopRoomType);
    app.get('/whl/topsupporters', verifyToken, DashboardController.getWhlTopSupporter);
    app.get('/whl/summary', verifyToken, DashboardController.getWhlSummary);

    //Luxury Escape 
    app.get('/le/mtd/dailybookings', verifyToken, DashboardController.getLeMtdDailyBooking);
    app.get('/le/ytd/dailybookings', verifyToken, DashboardController.getLeDailyBooking);
    app.get('/le/topcountries', verifyToken, DashboardController.getLeTopCountry);
    app.get('/le/toplengthstay', verifyToken, DashboardController.getLeTopLengthStay);
    app.get('/le/topbookingleadtime', verifyToken, DashboardController.getLeTopBookingLeadTime);
    app.get('/le/toproomtypes', verifyToken, DashboardController.getLeTopRoomType);
    app.get('/le/topnationality', verifyToken, DashboardController.getLeTopNationality);
    app.get('/le/summary', verifyToken, DashboardController.getLeSummary);

    //OTA Exclude LE
    app.get('/otaexle/mtd/dailybookings', verifyToken, DashboardController.getOtaExLeMtdDailyBooking);
    app.get('/otaexle/ytd/dailybookings', verifyToken, DashboardController.getOtaExLeDailyBooking);
    app.get('/otaexle/topcountries', verifyToken, DashboardController.getOtaExLeTopCountry);
    app.get('/otaexle/toplengthstay', verifyToken, DashboardController.getOtaExLeTopLengthStay);
    app.get('/otaexle/topbookingleadtime', verifyToken, DashboardController.getOtaExLeTopBookingLeadTime);
    app.get('/otaexle/toproomtypes', verifyToken, DashboardController.getOtaExLeTopRoomType);
    app.get('/otaexle/topsupporters', verifyToken, DashboardController.getOtaExLeTopSupporter);
    app.get('/otaexle/summary', verifyToken, DashboardController.getOtaExLeSummary);

    //DIRECT
    app.get('/direct/mtd/dailybookings', verifyToken, DashboardController.getDirectMtdDailyBooking);
    app.get('/direct/ytd/dailybookings', verifyToken, DashboardController.getDirectDailyBooking);
    app.get('/direct/topratecodes', verifyToken, DashboardController.getDirectTopRateCode);
    app.get('/direct/toplengthstay', verifyToken, DashboardController.getDirectTopLengthStay);
    app.get('/direct/topbookingleadtime', verifyToken, DashboardController.getDirectTopBookingLeadTime);
    app.get('/direct/toproomtypes', verifyToken, DashboardController.getDirectTopRoomType);
    app.get('/direct/topsupporters', verifyToken, DashboardController.getDirectTopSupporter);
    app.get('/direct/summary', verifyToken, DashboardController.getDirectSummary);

    //MP
    app.get('/mp/mtd/dailybookings', verifyToken, DashboardController.getMpMtdDailyBooking);
    app.get('/mp/ytd/dailybookings', verifyToken, DashboardController.getMpDailyBooking);
    app.get('/mp/topratecodes', verifyToken, DashboardController.getMpTopRateCode);
    app.get('/mp/toplengthstay', verifyToken, DashboardController.getMpTopLengthStay);
    app.get('/mp/topbookingleadtime', verifyToken, DashboardController.getMpTopBookingLeadTime);
    app.get('/mp/toproomtypes', verifyToken, DashboardController.getMpTopRoomType);
    app.get('/mp/topcountries', verifyToken, DashboardController.getMpTopCountry);
    app.get('/mp/summary', verifyToken, DashboardController.getMpSummary);

    //Country
    app.get('/country/mtd/dailybookings', verifyToken, DashboardController.getCountryMtdDailyBooking);
    app.get('/country/ytd/dailybookings', verifyToken, DashboardController.getCountryDailyBooking);
    app.get('/country/topratecodes', verifyToken, DashboardController.getCountryTopRateCode);
    app.get('/country/toplengthstay', verifyToken, DashboardController.getCountryTopLengthStay);
    app.get('/country/topbookingleadtime', verifyToken, DashboardController.getCountryTopBookingLeadTime);
    app.get('/country/topcities', verifyToken, DashboardController.getCountryTopCity);
    app.get('/country/topsupporters', verifyToken, DashboardController.getCountryTopSupporter);
    app.get('/country/summary', verifyToken, DashboardController.getCountrySummary);

    //RevToFB
    app.get('/revenue/roomrevenue', verifyToken, DashboardController.getRoomRevenue);
    app.get('/revenue/fbrevenue', verifyToken, DashboardController.getFBRevenue);

    //Reservation
    app.get('/reservation/pickup-position', verifyToken, DashboardController.getRsvPickupPostion);
    app.get('/reservation/mtd-pickup', verifyToken, DashboardController.getRsvMtdPickup);
    app.get('/reservation/top-countries', verifyToken, DashboardController.getRsvTopCountry);
    app.get('/reservation/top-length-stay', verifyToken, DashboardController.getRsvTopLengthStay);
    app.get('/reservation/top-lead-time', verifyToken, DashboardController.getRsvTopLeadTime);
    app.get('/reservation/top-market-segments', verifyToken, DashboardController.getRsvTopMarketSegment);
    app.get('/reservation/top-supporters', verifyToken, DashboardController.getRsvTopSupporter);

    // agents
    app.get('/agents', verifyToken, DashboardController.getListAgents);

    // outlets
    app.get('/outlets', verifyToken, OutletController.getOutlets);
    app.get('/outlets/:region', verifyToken, OutletController.getOutletsByRegion);
    app.post('/outlets', verifyToken, OutletController.createOutlet);
    app.get('/outlets/:id', verifyToken, OutletController.detailOutlet);
    app.put('/outlets/:id', verifyToken, OutletController.updateOutlet);
    app.delete('/outlets/:id', verifyToken, OutletController.deleteOutlet);

    // dinings
    app.get('/dinings', verifyToken, DiningController.getDinings);
    app.post('/dinings', verifyToken, DiningController.createDining);
    app.get('/dinings/total-points', verifyToken, DiningController.getTotalPoint);
    app.get('/dinings/validate-check-number', verifyToken, DiningController.validateCheckNumber);
    app.get('/dinings/check-first-dining/:memberId', verifyToken, DiningController.checkFirstDining);
    app.get('/dinings/:id', verifyToken, DiningController.detailDining);
    app.put('/dinings/:id', verifyToken, DiningController.updateDining);
    app.delete('/dinings/:id', verifyToken, DiningController.deleteDining);
    app.put('/dinings/approve/:id', verifyToken, DiningController.approveDining);
    app.get('/dinings/earned-points/:memberId', verifyToken, DiningController.getEarnPointsByMemberId);

    // app.post('/dinings/:id/billImage', [verifyToken, upload.single('file')], DiningController.updateBillImage);


    // buy points
    app.get('/points', verifyToken, PointController.getPoints);
    app.put('/points/updateExpDateMember/:memberId', verifyToken, PointController.updateMember);
    app.post('/points', verifyToken, PointController.createPoint);
    app.put('/points/:id', verifyToken, PointController.updatePoint);
    app.get('/points/:id', verifyToken, PointController.detailPoint);
    app.get('/points/purchase/:memberId', verifyToken, PointController.getPurchasePointsByMemberId);

};
