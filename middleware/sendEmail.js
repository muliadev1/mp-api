// sendEmail.js
const nodemailer = require('nodemailer');
const fs = require('fs');
const util = require('util');

const readFileAsync = util.promisify(fs.readFile);


async function sendEmail() {
  try {
    // Read the email template file
    const emailTemplate = await readFileAsync('../assets/email/notification_email_template.html', 'utf-8'); 

    // Create a SMTP transporter
    let transporter = nodemailer.createTransport({
      host: 'smtp.office365.com', // Replace with your SMTP server address
      port: 587,
      secure: false, // Set to true if your SMTP server requires SSL/TLS
      auth: {
        user: 'support@muliaprivileges.com', // Replace with your SMTP username
        pass: 'Newuser**2023', // Replace with your SMTP password
      },
    });

    // Define email content with placeholders
    const mailOptions = {
        from: 'support@muliaprivileges.com', // Replace with your email address
        to: 'iwan_lembong@yahoo.com', // Replace with recipient's email address
        subject: 'Notification from Next.js App',
        html: emailTemplate.replace('{{ recipientName }}', 'Iwan Lembong').replace('{{ notificationMessage }}', 'This is an email sent from a Next.js app using SMTP and nodemailer.'),
      };
  
    // Send email
    let info = await transporter.sendMail(mailOptions);

    console.log('Email sent:', info.messageId);
  } catch (error) {
    console.error('Error sending email:', error);
  }
}

module.exports = sendEmail;