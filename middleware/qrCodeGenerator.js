const QRCode = require('qrcode');
const fs = require('fs');
const path = require('path');

async function generateQRCode(voucherNo) {
  try {
    // Generate QR code
    const qrCodeDataURL = await QRCode.toDataURL(voucherNo);

    // Remove the data URL prefix to get the base64-encoded image
    const base64Data = qrCodeDataURL.replace(/^data:image\/png;base64,/, '');
    const imgBuffer = Buffer.from(base64Data, 'base64');

    // Tentukan path ke folder "assets/qr-code"
    const qrCodeDir = path.join(global.appRoot, 'assets/qr-code');
    if (!fs.existsSync(qrCodeDir)) {
      fs.mkdirSync(qrCodeDir, { recursive: true });
    }

    // Tentukan nama file untuk menyimpan QR code
    const fileName = `qrcode_${voucherNo}.png`;
    const filePath = path.join(qrCodeDir, fileName);

    // Tulis buffer ke file
    fs.writeFileSync(filePath, imgBuffer);

    // Kembalikan path file
    return filePath;
  } catch (error) {
    console.error(error);
    throw new Error('Error generating QR code');
  }
}

module.exports = { generateQRCode };