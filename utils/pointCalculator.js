// utils/pointCalculator.js

const { Setting } = require('../db/models/index')

/**
 * Fungsi umum untuk menghitung poin
 * @param {string} member_level - Level anggota ('MPS', 'MPG', 'MPD')
 * @param {number} total_bill - Total tagihan (dalam USD)
 * @param {string} type - Jenis perhitungan ('fb' untuk F&B atau 'room' untuk Room point)
 * @returns {number} earn_point - Poin yang diperoleh
 */

async function calculatePoint(member_level, total_bill, type) {
  try {
    // Fetch settings data
    const setting = await Setting.findOne({ where: { id: 1 } })
    if (!setting) {
      throw new Error('Settings not found')
    }

    // Tentukan multiplier (fb_point atau room_point) berdasarkan level anggota
    let point_value = 0
    let amount_value = 0

    if (type === 'fb') {
      // F&B point settings
      switch (member_level) {
        case 'MPS':
          point_value = parseFloat(setting.fb_point_silver)
          break
        case 'MPG':
          point_value = parseFloat(setting.fb_point_gold)
          break
        case 'MPD':
          point_value = parseFloat(setting.fb_point_diamond)
          break
        default:
          point_value = parseFloat(setting.fb_point_silver)
          break
      }
      amount_value = parseFloat(setting.fb_amount)
    } else if (type === 'room') {
      // Room point settings
      switch (member_level) {
        case 'MPS':
          point_value = parseFloat(setting.room_point_silver)
          break
        case 'MPG':
          point_value = parseFloat(setting.room_point_gold)
          break
        case 'MPD':
          point_value = parseFloat(setting.room_point_diamond)
          break
        default:
          point_value = parseFloat(setting.room_point_silver)
          break
      }
      amount_value = parseFloat(setting.room_amount)
    } else {
      throw new Error("Invalid point type. Use 'fb' or 'room'.")
    }

    // Validasi nilai settings
    if (isNaN(point_value) || isNaN(amount_value) || amount_value === 0) {
      throw new Error('Invalid settings values')
    }

    // Hitung multiplier dan earn_point
    let multiplier = point_value / amount_value
    let total_point = Math.floor(multiplier * total_bill)

    //return return_point
    return { total_point, message: 'Calculating normal point is succesfully' }
  } catch (error) {
    console.error(`Error calculating ${type} point:`, error.message)
    throw error
  }
}

async function calculate_DoublePoint(member_level, total_bill, type) {
  try {
    // Fetch settings data
    const setting = await Setting.findOne({ where: { id: 1 } })
    if (!setting) {
      throw new Error('Settings not found')
    }

    // Tentukan multiplier (fb_point atau room_point) berdasarkan level anggota
    let point_value = 0
    let amount_value = 0

    if (type === 'fb') {
      // F&B point settings
      switch (member_level) {
        case 'MPS':
          point_value = parseFloat(setting.fb_point_silver)
          break
        case 'MPG':
          point_value = parseFloat(setting.fb_point_gold)
          break
        case 'MPD':
          point_value = parseFloat(setting.fb_point_diamond)
          break
        default:
          point_value = parseFloat(setting.fb_point_silver)
          break
      }
      amount_value = parseFloat(setting.fb_amount)
    } else if (type === 'room') {
      // Room point settings
      switch (member_level) {
        case 'MPS':
          point_value = parseFloat(setting.room_point_silver)
          break
        case 'MPG':
          point_value = parseFloat(setting.room_point_gold)
          break
        case 'MPD':
          point_value = parseFloat(setting.room_point_diamond)
          break
        default:
          point_value = parseFloat(setting.room_point_silver)
          break
      }
      amount_value = parseFloat(setting.room_amount)
    } else {
      throw new Error("Invalid point type. Use 'fb' or 'room'.")
    }

    // Validasi nilai settings
    if (isNaN(point_value) || isNaN(amount_value) || amount_value === 0) {
      throw new Error('Invalid settings values')
    }

    // Hitung multiplier dan earn_point
    let multiplier = point_value / amount_value
    let return_point = Math.floor(multiplier * total_bill)
    let total_point = Math.floor(return_point * 2)

    return { total_point, message: 'Calculating double point is succesfully' }
    //return double_point
  } catch (error) {
    console.error(`Error calculating ${type} point:`, error.message)
    throw error
  }
}
async function calculate_BonusPoint(
  member_level,
  total_bill_rp,
  bookeeping,
  outletId
) {
  let multiplier_bonus = 1 // Default multiplier
  let multiplier_normal = 0
  let point_value = 0
  let amount_value = 0

  let normal_point = 0
  let bonus_point = 0
  let total_point = 0

  try {
    // Fetch settings data
    const setting = await Setting.findOne({ where: { id: 1 } })
    if (!setting) {
      throw new Error('Settings not found')
    }

    // F&B point settings
    switch (member_level) {
      case 'MPS':
        point_value = parseFloat(setting.fb_point_silver)
        break
      case 'MPG':
        point_value = parseFloat(setting.fb_point_gold)
        break
      case 'MPD':
        point_value = parseFloat(setting.fb_point_diamond)
        break
      default:
        point_value = parseFloat(setting.fb_point_silver)
        break
    }
    amount_value = parseFloat(setting.fb_amount)

    multiplier_normal = point_value / amount_value

    // Konversi total_bill_rp ke number jika perlu
    total_bill_rp = parseFloat(total_bill_rp)
    // console.log('total_bill_rp:', total_bill_rp, 'Type:', typeof total_bill_rp);
    outletId = parseInt(outletId)

    if (outletId === 19 || outletId === 20) {
      // CASCADE LOUNGE & CJ'S BAR
      min_total_bill = 500000 // Batas minimal untuk kategori ini
      if (total_bill_rp < min_total_bill) {
        return {
          total_point: 0,
          message: `The minimum total bill for Bonus Point is ${min_total_bill.toLocaleString(
            'en-US'
          )} rupiah.`,
        }
      }
    } else if (outletId >= 14 && outletId <= 18) {
      min_total_bill = 1000000 // Batas minimal untuk kategori ini
      if (total_bill_rp < min_total_bill) {
        return {
          total_point: 0,
          message: `The minimum total bill for Bonus Point is ${min_total_bill.toLocaleString(
            'en-US'
          )} rupiah.`,
        }
      }
    }

    if (outletId === 19 || outletId === 20) {
      // CASCADE LOUNGE & CJ'S BAR
      if (total_bill_rp >= 500000 && total_bill_rp <= 999999) {
        multiplier_bonus = 1.1 // 10%
        normal_point =
          total_bill_rp - 500000 > 0
            ? Math.floor(
                ((total_bill_rp - 500000) / bookeeping) * multiplier_normal
              )
            : 0
        bonus_point = Math.floor(
          parseFloat(500000 / bookeeping) * multiplier_bonus * multiplier_normal
        )
      } else if (total_bill_rp >= 1000000 && total_bill_rp <= 2999999) {
        multiplier_bonus = 1.15 // 15%
        normal_point =
          total_bill_rp - 1000000 > 0
            ? Math.floor(
                ((total_bill_rp - 1000000) / bookeeping) * multiplier_normal
              )
            : 0
        bonus_point = Math.floor(
          parseFloat(1000000 / bookeeping) *
            multiplier_bonus *
            multiplier_normal
        )
      } else if (total_bill_rp >= 3000000 && total_bill_rp <= 4999999) {
        multiplier_bonus = 1.23 // 23%
        normal_point =
          total_bill_rp - 3000000 > 0
            ? Math.floor(
                ((total_bill_rp - 3000000) / bookeeping) * multiplier_normal
              )
            : 0
        bonus_point = Math.floor(
          parseFloat(3000000 / bookeeping) *
            multiplier_bonus *
            multiplier_normal
        )
      } else if (total_bill_rp >= 5000000) {
        multiplier_bonus = 1.3 // 30%
        normal_point =
          total_bill_rp - 5000000 > 0
            ? Math.floor(
                ((total_bill_rp - 5000000) / bookeeping) * multiplier_normal
              )
            : 0
        bonus_point = Math.floor(
          parseFloat(5000000 / bookeeping) *
            multiplier_bonus *
            multiplier_normal
        )
      }
    } else if (outletId >= 14 && outletId <= 18) {
      if (total_bill_rp >= 1000000 && total_bill_rp <= 1999999) {
        multiplier_bonus = 1.1 // 10%
        normal_point =
          total_bill_rp - 1000000 > 0
            ? Math.floor(
                ((total_bill_rp - 1000000) / bookeeping) * multiplier_normal
              )
            : 0
        bonus_point = Math.floor(
          parseFloat(1000000 / bookeeping) *
            multiplier_bonus *
            multiplier_normal
        )
      } else if (total_bill_rp >= 2000000 && total_bill_rp <= 3499999) {
        multiplier_bonus = 1.18 // 18%
        normal_point =
          total_bill_rp - 2000000 > 0
            ? Math.floor(
                ((total_bill_rp - 2000000) / bookeeping) * multiplier_normal
              )
            : 0
        bonus_point = Math.floor(
          parseFloat(2000000 / bookeeping) *
            multiplier_bonus *
            multiplier_normal
        )
      } else if (total_bill_rp >= 3500000 && total_bill_rp <= 4999999) {
        multiplier_bonus = 1.25 // 25%
        normal_point =
          total_bill_rp - 3500000 > 0
            ? Math.floor(
                ((total_bill_rp - 3500000) / bookeeping) * multiplier_normal
              )
            : 0
        bonus_point = Math.floor(
          parseFloat(3500000 / bookeeping) *
            multiplier_bonus *
            multiplier_normal
        )
      } else if (total_bill_rp >= 5000000) {
        multiplier_bonus = 1.35 // 35%
        normal_point =
          total_bill_rp - 5000000 > 0
            ? Math.floor(
                ((total_bill_rp - 5000000) / bookeeping) * multiplier_normal
              )
            : 0
        bonus_point = Math.floor(
          parseFloat(5000000 / bookeeping) *
            multiplier_bonus *
            multiplier_normal
        )
      }
    }

    total_point = normal_point + bonus_point

    return { total_point, message: 'Calculating bonus point is succesfully' }
  } catch (error) {
    console.error(`Error calculating bonus point:`, error.message)
    throw error
  }
}

async function calculate_SocialEventPoint(total_bill_rp) {
  let total_point = 0
  let min_total_bill = 50000000 // Batas minimal untuk kategori ini

  try {
    // Konversi total_bill_rp ke number jika perlu
    total_bill_rp = parseFloat(total_bill_rp)

    if (total_bill_rp < min_total_bill) {
      return {
        total_point: 0,
        message: `The minimum total bill for Social Event Point is ${min_total_bill.toLocaleString(
          'en-US'
        )} rupiah.`,
      }
    }

    if (total_bill_rp >= 50000000 && total_bill_rp <= 199999999) {
      total_point = 6000
    } else if (total_bill_rp >= 200000000 && total_bill_rp <= 499000000) {
      total_point = 15000
    } else if (total_bill_rp >= 500000000 && total_bill_rp <= 1000000000) {
      total_point = 30000
    } else if (total_bill_rp >= 1000000000) {
      total_point = 30000
    } else {
      total_point = 0
    }

    //return total_point
    return {
      total_point,
      message: 'Calculating social event point is succesfully',
    }
  } catch (error) {
    console.error(`Error calculating bonus point:`, error.message)
    throw error
  }
}

async function calculate_PointPlus(total_bill_rp, outletId) {
  let total_point = 0

  try {
    // Konversi total_bill_rp ke number jika perlu
    total_bill_rp = parseFloat(total_bill_rp)
    // console.log('total_bill_rp:', total_bill_rp, 'Type:', typeof total_bill_rp);
    outletId = parseInt(outletId)

    if (outletId === 19 || outletId === 20) {
      // CASCADE LOUNGE & CJ'S BAR
      min_total_bill = 500000 // Batas minimal untuk kategori ini
      if (total_bill_rp < min_total_bill) {
        return {
          total_point: 0,
          message: `The minimum total bill for Point Plus is ${min_total_bill.toLocaleString(
            'en-US'
          )} rupiah.`,
        }
      }
    } else if (outletId >= 14 && outletId <= 18) {
      min_total_bill = 1000000 // Batas minimal untuk kategori ini
      if (total_bill_rp < min_total_bill) {
        return {
          total_point: 0,
          message: `The minimum total bill for Point Plus is ${min_total_bill.toLocaleString(
            'en-US'
          )} rupiah.`,
        }
      }
    }

    if (outletId === 19 || outletId === 20) {
      // CASCADE LOUNGE & CJ'S BAR
      if (total_bill_rp >= 500000 && total_bill_rp <= 999999) {
        total_point = 1000
      } else if (total_bill_rp >= 1000000 && total_bill_rp <= 2999999) {
        total_point = 3000
      } else if (total_bill_rp >= 3000000 && total_bill_rp <= 4999999) {
        total_point = 5000
      } else if (total_bill_rp >= 5000000) {
        total_point = 8000
      }
    } else if (outletId >= 14 && outletId <= 18) {
      if (total_bill_rp >= 1000000 && total_bill_rp <= 1999999) {
        total_point = 2000
      } else if (total_bill_rp >= 2000000 && total_bill_rp <= 3499999) {
        total_point = 4000
      } else if (total_bill_rp >= 3500000 && total_bill_rp <= 4999999) {
        total_point = 7000
      } else if (total_bill_rp >= 5000000) {
        total_point = 10000
      }
    }

    return { total_point, message: 'Calculating point plus is succesfully' }
  } catch (error) {
    console.error(`Error calculating pint plus:`, error.message)
    throw error
  }
}

async function calculate_RamadanPointPlus(total_bill_rp, outletId) {
  let total_point = 0

  try {
    // Konversi total_bill_rp ke number jika perlu
    total_bill_rp = parseFloat(total_bill_rp)
    // console.log('total_bill_rp:', total_bill_rp, 'Type:', typeof total_bill_rp);
    outletId = parseInt(outletId)
    if (outletId === 20) {
      // CJ'S BAR
      return {
        total_point: 0,
        message: `This outlet doesn't have Ramadan Point Plus.`,
      }
    } else if (outletId === 19) {
      // CASCADE LOUNGE
      min_total_bill = 500000 // Batas minimal untuk kategori ini
      if (total_bill_rp < min_total_bill) {
        return {
          total_point: 0,
          message: `The minimum total bill for Ramadan Point Plus is ${min_total_bill.toLocaleString(
            'en-US'
          )} rupiah.`,
        }
      }
    } else if (outletId >= 14 && outletId <= 18) {
      min_total_bill = 1000000 // Batas minimal untuk kategori ini
      if (total_bill_rp < min_total_bill) {
        return {
          total_point: 0,
          message: `The minimum total bill for Ramadan Point Plus is ${min_total_bill.toLocaleString(
            'en-US'
          )} rupiah.`,
        }
      }
    }

    if (outletId === 19) {
      // CASCADE LOUNGE
      if (total_bill_rp >= 500000 && total_bill_rp <= 999999) {
        total_point = 1000
      } else if (total_bill_rp >= 1000000 && total_bill_rp <= 2999999) {
        total_point = 3000
      } else if (total_bill_rp >= 3000000 && total_bill_rp <= 4999999) {
        total_point = 5000
      } else if (total_bill_rp >= 5000000) {
        total_point = 8000
      }
    } else if (outletId >= 14 && outletId <= 18) {
      if (total_bill_rp >= 1000000 && total_bill_rp <= 1999999) {
        total_point = 2500
      } else if (total_bill_rp >= 2000000 && total_bill_rp <= 3499999) {
        total_point = 5000
      } else if (total_bill_rp >= 3500000 && total_bill_rp <= 4999999) {
        total_point = 8750
      } else if (total_bill_rp >= 5000000) {
        total_point = 12500
      }
    }

    return {
      total_point,
      message: 'Calculating ramadan point plus is succesfully',
    }
  } catch (error) {
    console.error(`Error calculating pint plus:`, error.message)
    throw error
  }
}

/**
 * Fungsi untuk menghitung F&B point
 */
async function calculateEarnPoint(member_level, total_bill) {
  const levelStr = String(member_level)
  const billNum = Number(total_bill)
  return calculatePoint(levelStr, billNum, 'fb')
}

/**
 * Fungsi untuk menghitung Room point
 */
async function calculateRoomPoint(member_level, total_bill) {
  const levelStr = String(member_level)
  const billNum = Number(total_bill)
  return calculatePoint(levelStr, billNum, 'room')
}

async function calculateBonusPoint(
  member_level,
  total_bill_rp,
  bookeeping,
  outletId
) {
  return calculate_BonusPoint(member_level, total_bill_rp, bookeeping, outletId)
}

async function calculateDoublePoint(member_level, total_bill) {
  return calculate_DoublePoint(member_level, total_bill, 'fb')
}

async function calculateSocialEventPoint(total_bill_rp) {
  return calculate_SocialEventPoint(total_bill_rp)
}

async function calculatePointPlus(total_bill_rp, outletId) {
  return calculate_PointPlus(total_bill_rp, outletId)
}

async function calculateRamadanPointPlus(total_bill_rp, outletId) {
  return calculate_RamadanPointPlus(total_bill_rp, outletId)
}

module.exports = {
  calculateEarnPoint,
  calculateRoomPoint,
  calculateBonusPoint,
  calculateDoublePoint,
  calculateSocialEventPoint,
  calculatePointPlus,
  calculateRamadanPointPlus,
}
