const mysql = require('mysql');

// Buat koneksi ke database MySQL
const connection = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: '123456',
  database: 'muliadb'
});

// Buka koneksi ke database
connection.connect((err) => {
  if (err) {
    console.error('Error connecting to database: ' + err.stack);
    return;
  }
  console.log('Connected to database');
  
  // Perintah SQL untuk membaca semua record dari tabel acc_summary_temp
  const selectSql = `SELECT * FROM acc_summary_temp`;

  // Eksekusi perintah SQL untuk membaca record dari tabel acc_summary_temp
  connection.query(selectSql, (selectError, selectResults, selectFields) => {
    if (selectError) {
      console.error('Error reading records from acc_summary_temp: ' + selectError.message);
      connection.end(); // Tutup koneksi jika ada kesalahan membaca record
      return;
    }

    // Fungsi rekursif untuk mengolah setiap record
    processRecord(selectResults, 0);
  });

});

function processRecord(records, index) {
  if (index >= records.length) {
    // Tutup koneksi ke database setelah semua operasi selesai
    connection.end();
    return;
  }

  const row = records[index];
  console.log('Processing record ' + (index + 1) + ' of ' + records.length);

  // Periksa apakah member_id ada di tabel members
  const checkMemberSql = `SELECT * FROM members WHERE member_id = ?`;
  connection.query(checkMemberSql, [row.membership_card_no], (checkError, checkResults, checkFields) => {
    if (checkError) {
      console.error('Error checking member ID ' + row.membership_card_no + ' in members table: ' + checkError.message);
      processRecord(records, index + 1); // Lanjutkan ke record berikutnya setelah menangani kesalahan
      return;
    }

    // Jika member_id ditemukan di tabel members, update field is_found menjadi 1 di tabel acc_summary_temp
    if (checkResults.length > 0) {
      const updateSql = `UPDATE members SET total_points = ?, reedem_points = ?, available_points = ? WHERE member_id = ?`;
      connection.query(updateSql, [row.avail_points_2024, 0, row.avail_points_2024, row.membership_card_no], (updateError, updateResults, updateFields) => {
        if (updateError) {
          console.error('Error updating members for member ID ' + row.membership_card_no + ': ' + updateError.message);
        } else {
          console.log('Updated members for member ID ' + row.membership_card_no);
        }
        processRecord(records, index + 1); // Lanjutkan ke record berikutnya setelah menyelesaikan operasi saat ini
      });
    } else {
      console.log('Member ID ' + row.membership_card_no + ' not found in members table');
      const updateSql2 = `UPDATE acc_summary_temp SET is_not_found = 1 WHERE membership_card_no = ?`;
      connection.query(updateSql2, [row.membership_card_no], (updateError, updateResults, updateFields) => {
        if (updateError) {
          console.error('Error updating is_not_found for member ID ' + row.membership_card_no + ': ' + updateError.message);
        } else {
          console.log('Updated is_not_found for member ID ' + row.membership_card_no);
        }
        processRecord(records, index + 1); // Lanjutkan ke record berikutnya setelah menyelesaikan operasi saat ini
      });
    }
  });
}