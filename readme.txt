#### Sequelize
1. in the root project create new folder called "db"
2. in the rott project create another file called .sequelizerc in the root folder and update like below
    
    const path = require("path");
    module.exports = {
        config: path.resolve("db/config", "config.json"),
        "models-path": path.resolve("db", "models"),
        "migrations-path": path.resolve("db", "migrations"),
        "seeders-path": path.resolve("db", "seeders"),
    };

2. on the command promt, run "sequelize init" to create these folders under /database folder.

3. after that go to folder db/config and open config.json file, adjust the file like below

{
  "development": {
    "username": "root",
    "password": "123456",
    "database": "muliadb",
    "host": "127.0.0.1",
    "dialect": "mysql"
  },
  "test": {
    "username": "root",
    "password": null,
    "database": "database_test",
    "host": "127.0.0.1",
    "dialect": "mysql"
  },
  "production": {
    "username": "root",
    "password": null,
    "database": "database_production",
    "host": "127.0.0.1",
    "dialect": "mysql"
  }
}

4. create migrate and model , like sample models below

npx sequelize-cli model:generate --name Language --attributes language_name:string,language_code:string,iso_code:string

npx sequelize-cli model:generate --name Page --attributes page_name:string,page_title:string,page_description:string,page_address:string,image_name:string,image_url:string

npx sequelize-cli model:generate --name PageContent --attributes page_id:integer,page_title:string,page_content:string,image_name:string,image_url:string,language_id:integer

npx sequelize-cli model:generate --name PageTerm --attributes page_id:integer,term_title:string,term_description:string,language_id:integer

npx sequelize-cli model:generate --name PageFaq --attributes page_id:integer,faqs_title:string,faqs_description:string,language_id:integer

npx sequelize-cli model:generate --name Reedem --attributes point:integer,image_name:string,image_url:string,location:string,reedem_name:string

npx sequelize-cli model:generate --name ReedemLang --attributes reedem_id:integer,language_id:integer,description:string

npx sequelize-cli model:generate --name ExclusiveOffer --attributes destination:string,hotel:string,image_name:string,image_url:string,name:string

npx sequelize-cli model:generate --name OfferLang --attributes offer_id:integer,language_id:integer,title:string,description:string

npx sequelize-cli migration:create --name modify_users_add_new_fields (berguna dalam menambah fields baru pada tabel yang sudah ada tanpa menghilangkan data pada database)

npx sequelize-cli migration:create --name modify_reedems_add_new_fields

npx sequelize-cli migration:create --name modify_reedemslangs_add_new_fields

npx sequelize-cli migration:create --name modify_exclusiveoffers_add_new_fields

npx sequelize-cli model:generate --name MemberVoucher --attributes member_id:string,voucher_no:string,date_expaired:date,is_used:integer,createdBy:string,updatedBy:string


npx sequelize-cli model:generate --name Member --attributes title:string,first_name:string,last_name:string,gender:string,birth_date:string,language:date,membership_no:string,membertype_id:integer,
email:string,password:string,phone:string,nationality:string,country_residence:string,address:string,join_date:date,points:integer

npx sequelize-cli model:generate --name Role --attributes role_name:string,role_description:string,is_active:integer

npx sequelize-cli model:generate --name Module --attributes module_code:string,module_name:string,order_no:integer,url:string,is_parent:integer,is_trx:integer

npx sequelize-cli model:generate --name Menu --attributes menu_code:string,menu_name:string,module_id:integer,order_no:integer,url:string,is_trx:integer

npx sequelize-cli model:generate --name RolesMenu --attributes role_id:integer,module_id:integer,menu_id:integer,order_no:integer,is_insert:integer,is_update:integer,is_delete:integer,is_read:integer

npx sequelize-cli model:generate --name MissingPointEmail --attributes confirmation_no:string,guest_name:string,property:string,checkin:date,checkout:date,file:string,comment:text


npx sequelize-cli migration:create --name modify_users_add_role_id_fields

npx sequelize-cli model:generate --name ReedemsPoint --attributes member_id:string,reedem_id:integer,total_reedem_points:integer,comments:string

npx sequelize-cli model:generate --name MarketSegment --attributes group:string,segment:string,code:string,description:string

npx sequelize-cli model:generate --name RateCode --attributes rate_code:string,rate_description:string,rate_description2:string,card:string,segment:string

npx sequelize-cli model:generate --name RoomType --attributes room_type_code:string,room_type:string,room_type_description:string

npx sequelize-cli migration:create --name create_initial_points

npx sequelize-cli model:generate --name InitialPoint --attributes member_id:string,arrival_date:date,departure_date:date,property:string,room_usd:integer,fb_usd:integer,init_point:integer

npx sequelize-cli model:generate --name EnrollsLog --attributes confirmation_no:integer,member_id:string,first_name:string,last_name:string,arrival_date:date,departure_date:date,room_usd:integer,fb_usd:integer

npx sequelize-cli model:generate --name PointsLog --attributes confirmation_no:integer,member_id:string,first_name:string,last_name:string,arrival_date:date,departure_date:date,room_class:string,room_usd:integer,fb_usd:integer,room_point:integer,fb_point:integer,total_points:integer

npx sequelize-cli model:generate --name ReedemsPointLog --attributes member_id:string,reedem_id:integer,total_reedem_points:integer,comments:string

npx sequelize-cli migration:create --name modify_enrolls-log_add_new_fields

npx sequelize-cli model:generate --name Dining --attributes dining_date:date,outlet_id:integer,member_id:string,member_name:string,total_bill:double,earned_point:double,check_number:string,createdBy:string,updatedBy:string

npx sequelize-cli model:generate --name Outlet --attributes outlet_name:string,cuisine:string,opening_hour:string,breakfast:string,lunch:string,dinner:string,snack:string,sunday_brunch:string,sunday_lunch:string,all_day_dining:string,afternoon_tea:string,createdBy:string,updatedBy:string

npx sequelize-cli model:generate --name Setting --attributes room_amount:integer,room_point:integer,fb_amount:integer,fb_point:integer,hotel_pay_to_loyalty:decimal,loyalty_pay_to_hotel:decimal,system_profit_percentage:decimal,system_profit_amount:decimal,complimentary_stay_nights:integer,dividing_factors:decimal,createdBy:string,updatedBy:string

5. after command above, on the folder db/migrations and db/models automaticly create mogrations files and models files

6. on the command fromt runj "npx sequelize-cli db:migrate" to update table to database


